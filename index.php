<?php
// On sécurise les variables SESSIONS
ini_set('session.use_trans_sid', 0);
session_start();
session_regenerate_id();

// On empêche l'affichage des erreurs sauf en dev
if(!in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1', '10.0.2.2')))
	error_reporting(0);
else
    error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED); // certaines librairies ne sont pas compatibles avec les "stricts standards" de php 5.4 du coup il faut le désactiver

require_once('inc/essentials.php'); // Fonction critiques
require_once('inc/Request.class.php');
Request::start();
require_once('inc/autoloader.php'); // Chargement des objets
require_once('inc/log.php'); // Système de logging
require_once('inc/bdd_racine.php'); // Connexion à la bdd
require_once('inc/conf.php'); // Config CAS et Payutc
require_once('inc/func.php'); // Fonctions usuelles
require_once('inc/payutc_tools.php');

$user = NULL;
/* juste pour que la variable $user existe,
   elle sera modifiée plus tard et est toujours une instance de la classe UtilisateurSite
*/
require_once('inc/login.php');

header('Content-Type: text/html; charset=UTF-8');

$user = load_user_for_session($user);
Request::redirectToTarget(); // si on vient de se logger il est possible qu'on veuille retourner vers la page d'origine
$conid = $user->get_id();

// création du panier de commande
require_once('inc/Panier.php');
Request::$panier = new Panier($db, $_SESSION);
Request::$panier->setUser($user);

Request::load();

$hasdroit = Droits::select('COUNT(*)')->where('ID = Page.ID')
    ->where('User=?', $user);
$page = Page::select('psp.Acces')
    ->select('(CASE WHEN ('.$hasdroit->get_sql().')=1 THEN 1 ELSE 0 END)',
             'HasDroit')
    ->where('Module LIKE ? AND Section LIKE ?', Request::$module, Request::$section)
    ->addSubquery($hasdroit)
    ->rawExecute()->fetch();

switch($page['Acces']){
	case 'public':
        Request::includeSection();
        break;

	case 'private':
		if($page['HasDroit'] == 1){
			if(isset($_SESSION['con-lastPrivateActive']) && time() - $_SESSION['con-lastPrivateActive'] < 60*15){
				$_SESSION['con-lastPrivateActive'] = time();
                Request::inject();
				Request::includeSection();
			} else {
                Request::handlePrivate();
			}
		}
		elseif ($user->is_logged())
            Request::handle404();
        else
            Request::handleAuth();
	break;

	case 'staff':
		if($user->is_logged() && $user->Staff) {
            Request::inject();
            Request::includeSection();
        } elseif ($user->is_logged())
                Request::handle404();
        else
            Request::handleAuth();
	break;

	case 'member':
		if($user->is_logged()) {
            Request::inject();
            Request::includeSection();
        } else
            Request::handleAuth();
	break;

	default:
        Request::handle404();
}

// Fermeture de la connexion à la bdd
mysql_close();
?>
