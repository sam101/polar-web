#!/usr/bin/env python
# -*- coding:utf-8 -*-

# cf README

from urllib import urlencode
from urlparse import urljoin
from urllib2 import urlopen, URLError
from json import loads
import tarfile
from wget import download
from hashlib import md5
from shutil import rmtree
import os
import os.path

import settings

def is_up_to_date():
    # On recherche la version courante
    try:
        version = open('current_version', 'r').read()
    except IOError:
        with open('current_version', 'w') as f:
            f.write('')
        version = ''

    # Puis la dernière version disponible
    url = urljoin(settings.site, 'applis/update')
    params = urlencode({'appli' : settings.app_name})
    try:
        res = urlopen(url+'?'+params)
    except URLError as error:
        return 'fail'# on lance le programme quand même...

    try:
        remote = loads(res.read())
    except ValueError:
        return 'parse failed'

    return remote['hash'] == version

def update():
    # On télécharge le fichier archive
    path = download(urljoin(settings.site, 'applis_store/') + settings.app_name + '.tar.gz')
    c = open(path, 'rb')

    if (os.path.isdir('appli')):
        # On supprime les anciens fichiers de sauvegarde
        rmtree('backup', True)
        # On sauvegarde les anciens fichiers (au cas où les nouveaux plantent)
        os.rename('appli', 'backup')
        os.rename('current_version', 'backup/current_version')

        updated = True # Aka le dossier backup existe
    else:
        updated = False

    # On stocke le hash md5 de la nouvelle archive
    hash = md5(c.read()).hexdigest()
    with open('current_version', 'w') as f:
        f.write(hash)

    tar = tarfile.open(path, 'r:gz')
    tar.extractall('appli')

    return updated

def rollback():
    # On stocke les fichiers fautifs dans failed
    rmtree('failed', True)
    # On sauvegarde les anciens fichiers (au cas où les nouveaux plantent)
    os.rename('appli', 'failed')
    os.rename('failed/current_version', 'current_version')

    # On remet les anciens fichiers à la place
    os.rename('backup', 'appli')

# le module doit définir une fonction run qui lance l'appli

if __name__ == '__main__':
    if not(is_up_to_date()):
        print "= Mise à jour de l'application ="
        updated = update()
    else:
        print "= L'application est à jour ="
        updated = False

    try:
        import appli
        appli.run()

    except Exception:
        if updated:
            # On revient à l'état d'avant et on se pend
            rollback()
            reload(appli)
            appli.run()

