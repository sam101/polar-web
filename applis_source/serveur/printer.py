# -*- coding:utf-8 -*-

import unicodedata
from escpos import *
import settings
from threading import Lock
from socket import error as socket_error

printing = Lock()

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

def print_poster_soutenance(num, nom, code, branche):
    printing.acquire()

    try:
        p = printer.Network(settings.ip_printer)
    except socket_error:
        return False

    p.hw('init')
    p.text(constants.TXT_ALIGN_CT)
    p.image('logo-header.png')

    p.text('\x1d\x21\x00') # taille 1x
    p.text('\n')
    p.text(constants.TXT_ALIGN_LT)
    p.text(remove_accents(nom))
    p.text('\n')
    p.text('\n')
    p.text(constants.TXT_ALIGN_CT)

    # on veut imprimer un code barre CODE128 mais c'est la galère
    p.text(constants.BARCODE_TXT_OFF)
    p.text(constants.BARCODE_HEIGHT)
    p.text(constants.BARCODE_WIDTH)
    p.text('\x1d\x6b\x49\x0c') # type CODE128 + 12 octects de données
    p.text('\x7b\x42') # CODE A
    p.text(str(code))
    p.text('\n')

    p.text('\x1d\x62\x01') # text smoothing
    p.text('\x1d\x21\x44') # taille 5x
    if branche == 'Autre':
        p.text(str(num))
    else:
        p.text(str(branche)+'-'+str(num))
    p.cut()

    printing.release() # c'est fini !
    return True

def print_commande(num, nom, type, code):
    # On attend déjà que les autres impressions soient finies
    printing.acquire()

    try:
        p = printer.Network(settings.ip_printer)
    except socket_error:
        return False

    p.hw('init')
    p.text(constants.TXT_ALIGN_CT)
    p.image('logo-header.png')

    p.text('\x1d\x21\x00') # taille 1x
    p.text('\n\n')
    p.text(constants.TXT_ALIGN_LT)
    p.text(remove_accents(nom))
    p.text('\n')
    p.text(remove_accents(type))
    p.text('\n\n')
    p.text(constants.TXT_ALIGN_CT)

    # on veut imprimer un code barre CODE128 mais c'est la galère
    p.text(constants.BARCODE_TXT_OFF)
    p.text(constants.BARCODE_HEIGHT)
    p.text(constants.BARCODE_WIDTH)
    p.text('\x1d\x6b\x49\x0c') # type CODE128 + 12 octects de données
    p.text('\x7b\x42') # CODE A
    p.text(str(code))
    p.text('\n')

    p.text('\x1d\x62\x01') # text smoothing
    p.text('\x1d\x21\x44') # taille 5x
    p.text(str(num))
    p.cut()

    printing.release() # c'est fini !
    return True
