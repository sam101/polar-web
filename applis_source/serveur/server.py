# -*- coding:utf-8 -*-

from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from waitress import serve
import json
import settings
from printer import print_commande, print_poster_soutenance

class CrossSiteResponse(Response):
    """Le serveur va tourner sur le PC caisse du Polar.
    Pour des raisons de sécurité firefox n'autorise pas la lecture de la réponse d'une requête ajax faites sur un autre domaine.
    On va préciser les en-têtes Origin et Access-Control-Allow-Origin pour autoriser la caisse à les lire.
    cf https://developer.mozilla.org/en-US/docs/HTTP/Access_control_CORS
    """
    def __init__(self, body, *args, **kwargs):
        kwargs['body'] = json.dumps(body)
        super(CrossSiteResponse, self).__init__(*args, **kwargs)
        self.headers['Access-Control-Allow-Origin'] = '*'
        self.content_type = 'application/json'

def do_print_commande(request):
    numero = int(request.POST.get('id'))
    nom = request.POST.get('nom')
    type = request.POST.get('type')
    code = request.POST.get('code')
    print_commande(numero, nom, type, code)

    return CrossSiteResponse(True)

def do_print_soutenance(request):
    numero = int(request.POST.get('id'))
    nom = request.POST.get('nom')
    code = request.POST.get('code')
    branche = request.POST.get('branche')
    print_poster_soutenance(numero, nom, code, branche)

    return CrossSiteResponse(True)

def run():
    config = Configurator()

    config.add_route('print_commande', '/print_commande')
    config.add_view(do_print_commande, route_name='print_commande')

    config.add_route('print_soutenance', '/print_soutenance')
    config.add_view(do_print_soutenance, route_name='print_soutenance')

    app = config.make_wsgi_app()
    serve(app, port=8111)

if __name__ == '__main__':
    run()

