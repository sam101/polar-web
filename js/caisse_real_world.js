/*
Interactions avec le monde réel
On utilise un petit serveur http qui tourne sur le PC caisse
*/

var capabilities = {}
var server_addr = 'http://127.0.0.1:8080/'; // en prod comme en dev c'est toujours le pc sur lequel le navigateur tourne qui nous intéresse
var currentCarteEtu = false;

function realWorldError(titre, contenu) {
    $('#erreur').removeClass('invisible')
        .popover({placement: 'right',
                  trigger: 'hover',
                  html: true,
                  content: contenu,
                  title: titre,
                 });
}

function resetServeurCaisse() {
    return $.get(server_addr+'reset');
}

function checkCapabilities(callback) {
    $.ajax(server_addr+'capabilities',
           {'success': callback,
            'timeout': 1000,
            'dataType': 'json',
            'error': function(xhr, status, error) {
                callback({'error': status});
            }
           });
}

function openTheCaisse() {
    $.get(server_addr+'caisse');
}

function pollCarteEtu() {
    $.ajax({ url: server_addr+'smartcard', success: function(data){
        if (data) {
            if (data.added) {
                currentCarteEtu = data.etu;
                if (modePaiement == 'payutc') {
                    $('#modal-validation .valider').removeAttr('disabled').click();
                } else {
                    $('#payutc').click();
                }
            }
        }
    }, complete: pollCarteEtu, timeout: 70000 });
}

function printBillet(id, nom, type, code) {
    $.post(server_addr+'print_billet', {'id' : id, 'nom' : nom,
                                        'type' : type, 'code' : code});
}

$(function() {
    checkCapabilities(function(data) {
        if (data.error)
            realWorldError('Serveur non connecté', 'Le serveur de caisse ne semble pas connecté, merci de redémarrer le PC caisse');
        else {
            capabilities = data;
            resetServeurCaisse();
            // on attend les cartes étu
            pollCarteEtu();
            // on signale les appareils manquant au permanencier
            var manquants = '';
            if (!capabilities.smartcard)
                manquants += 'Le lecteur de carte étudiant n\'est pas connecté<br/><br/>';
            if (!capabilities.printer)
                manquants += 'L\'imprimante thermique n\'est pas connectée<br/><br/>';
            if (manquants) {
                manquants += 'Après connection merci de relancer la caisse (F5 ou clic sur "Erreur")';
                realWorldError('Appareils manquants', manquants);
            }
        }
    });
});