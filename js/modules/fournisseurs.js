$(function() {
    $("#btn-add-fournisseur").click(function() {
        $("#details-fournisseur").hide();
        $("#add-fournisseur").removeClass('hidden');
    });

    $("#btn-add-ticket").click(function() {
        $("#add-ticket").removeClass('hidden');
    });

    $("#btn-add-contact").click(function() {
        $("#tr-add-contact").removeClass('hidden');
    });

    $("#btn-cancel-ticket").click(function() {
        $("#add-ticket").addClass('hidden');
        $("#add-ticket form")[0].reset();
    });

    $("#input-file-ticket").change(function() {
        $("#control-nom-fichier").removeClass("hidden");
        var input = $("#input-nom-fichier-ticket");
        if (input.val() == '')
            input.val($(this).val());
    });
});