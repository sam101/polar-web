$(function() {
	$("#login").autocomplete(autocomplete_url,{
		matchContains:1,
		mustMatch:false,
		delay: 100,
		formatItem: function(row, i, max) {
			return row[0]+" - "+row[1]+" "+row[2];
		},
		formatResult: function(row) {
			return row[0];
		}
	});

    $('#panier-btn').click(function(e) {
        e.preventDefault();
        $('#users-loading').show();
        var commande = $("#form-panier input[name=Commande]").val();
        $.getJSON(url,
                  {'findUser':'', 'Commande':commande},
                  function(data) {
                      var options = '';
                      for (var i = 0; i < data.length; i++) {
                          options += '<option value="'+data[i].id+'">'+data[i].nom+' ('+data[i].mail+')</option>';
                      }
                      $('#select-users').html(options);
                      $('#users-loading').hide();
                      $('#form-panier').show();
                  });
    });
});