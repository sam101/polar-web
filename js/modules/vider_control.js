var etats = new Array('Sauvegarde des données', 'Cryptage', 'Envoi', 'Nettoyage');
var params = new Array('BackupDB', 'EncryptDB', 'UploadDB', 'CleanDB');
var i = 0;
var finished = false;

function next(){
    if(i < params.length){
        $('#encours').append('<li>'+etats[i]+'... <img src=\"'+icone+'\" /></li>');
        $.ajax({
            url: url+'?'+params[i],
            success: function(data){
                if(data == 'ok'){
                    $('#encours li:last').html('<li>'+etats[i]+'... OK !</li>');
                    i++;
                    next();
                }
                else {
                    $('#encours li:last').html('<li>'+etats[i]+'... '+data+ '</li>');
                    $('#finished').html('<span style=\"color:red\"><b>La sauvegarde a échoué. Veuillez déposer un ticket au webmaster, en indiquant l\'étape où la sauvegarde s\'est arrêtée et le message d\'erreur affiché.</b></span>');
                }
            }
        });
    }
    else{
        $('#finished').html('La sauvegarde est terminée, vous pouvez quitter cette page.');
        finished = true;
    }
}

$(document).ready(function() {
    next();

    $(window).bind('beforeunload', function(){
        if(!finished)
            return 'ATTENTION : la sauvegarde n\'est pas terminée ou ne s\'est pas terminée correctement.';
    });
});