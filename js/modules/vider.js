$(document).ready(function(){
    $("#monnaie input").keyup(function(e){
        var total = 0;
        total += parseInt($("#100e").val())*100;
        total += parseInt($("#50e").val())*50;
        total += parseInt($("#20e").val())*20;
        total += parseInt($("#10e").val())*10;
        total += parseInt($("#5e").val())*5;
        total += parseInt($("#2e").val())*2;
        total += parseInt($("#1e").val())*1;
        total += parseInt($("#50c").val())*0.5;
        total += parseInt($("#20c").val())*0.2;
        total += parseInt($("#10c").val())*0.1;
        total += parseInt($("#5c").val())*0.05;
        total += parseInt($("#2c").val())*0.02;
        total += parseInt($("#1c").val())*0.01;
        $("#montantpratique").html(total.toFixed(2) + " &euro;");
    });

    $("#boutonAnnuler").click(function(){
        window.location = racine;
    });

    $("#otc").click(function(){
        openTheCaisse();
    });
    $("#formCaisse").submit(function(){
        var valeur = prompt("Combien font 6 fois 7 ?\n\n(Ce test nécessitant au moins A à MT90 permet d\'éviter de valider la caisse par erreur.)");
        return (valeur == 42);
    });

});