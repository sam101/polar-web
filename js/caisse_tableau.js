//Gestion du tableau d'articles

var nbLignes = 0;
var ligneCourante;
var colCourante;

function ajouterLigne(){ //Rajoute une ligne au tableau des articles
	nbLignes++;
	
	// Ajout de la ligne dans le tableau
	$("#tableau_caisse > tbody").append('<tr>\
		<td class="code selectable">\
			<input type="text" size="4" class="input-invisible input-mini"/>\
		</td>\
		<td class="designation selectable">\
			<input type="text" size="42" class="input-invisible input-designation"/>\
		</td>\
		<td class="qte selectable">\
			<input type="text" min="1" value="1" size="4" class="input-invisible input-mini"/>\
		</td>\
		<td class="prix"></td>\
		<td class="prix_asso invisible"></td>\
		<td class="remise"></td>\
		<td class="sous-total"></td>\
		<td class="sous-total_asso invisible"></td>\
		<td class="param invisible"></td>\
		<td class="ok invisible"></td>\
		<td class="colonne_suppr"></td>\
	</tr>');
	
	// Ajout du bouton supprimer sur l'avant-dernière ligne
	if(nbLignes>1)
		getLigne(nbLignes-1).find(".colonne_suppr").html('<i class="icon icon-remove supprimer_ligne" ></i>').tooltip({title:'Supprimer cette ligne (F4)'});
	
	// Handler de l'événement "clic sur une case"
	getLigne(nbLignes).find("td.selectable").click(function(e){
		e.stopPropagation();
		var ligne = $("#tableau_caisse tr").index($(this).parent());
		var colonne = $(this).attr("class").split(" ");
		selectCase(ligne,colonne[0]); // Sélection de la case
	});
	
	// Handler de l'événement "focus sur un champ" (sert pour la navigation au clavier)
	getLigne(nbLignes).find("input").focus(function(e){
		$(this).parent().click(); // On clique sur la case du tableau correspondante
		return false;
	});
	
	// Handler de l'événement "clic sur supprimer"
	getLigne(nbLignes-1).find(".supprimer_ligne").click(function(e){
		e.stopPropagation();
		var ligne = $("#tableau_caisse tr").index($(this).parent().parent());
		// Hook pour que les articles puissent savoir qu'ils ont été supprimés
		$(document).trigger("articleSupprime", [ligne]);
		supprimerLigne(ligne);
		selectCase(nbLignes,"code");
	});
	
	//Ajout de l'autocomplétion aux champs à l'aide des données de recupdata.js. Il est désactivé (9999) tant que le champ n'est pas sélectionné
	getInput(nbLignes,"designation").autocomplete(prod_designation_filled, {
		matchContains: true,
		minChars:9999
	});
}

// Supprime la ligne indiquée (eh ouais.)
function supprimerLigne(ligne){
	if(ligneCourante == ligne){ //Si on est sur la ligne à supprimer
		ligneCourante = 0;
		colCourante = "code";
	}
	else if(ligneCourante > ligne) //Si on est sur une ligne sous la ligne à supprimer
		ligneCourante--;
	getLigne(ligne).remove();
	nbLignes--;
	majTotal();
}

// Entraîne la sélection d'une case du tableau
function selectCase(ligne,colonne){
	// Si on n'est pas sur une case valide la fonction échoue
	if(!getCase(ligne, colonne))
		return false;

	// Si on n'a pas le droit de sélectionner la case, idem
	if(!getCase(ligne, colonne).attr("class") || !getCase(ligne, colonne).hasClass("selectable"))
		return false;

	if(ligneCourante == ligne && colCourante == colonne)
		return true; //Si on est déjà sur la case la fonction réussit d'office

	if(!deselect()){ //Si une autre case est déjà sélectionnée et qu'on n'arrive pas à la déselectionner
		getInput(ligneCourante,colCourante).select(); //On resélectionne le texte de la case sélectionnée
		return false; //et la fonction échoue
	}
	//Sélection proprement dite : cadre, sélection du texte du champ, activation de l'autocomplete, mise à jour des variables
    
	getCase(ligne,colonne).addClass("selectionne");
	getInput(ligne,colonne).setOptions({minChars:0});
	getInput(ligne,colonne).select();
	ligneCourante = ligne;
	colCourante = colonne;
	return true;
}

//Entraîne la déselection d'une case
function deselect(){
	if(inputValide()){ //Sinon on vérifie que le contenu du champ est valide pour pouvoir le désélectionner
		getInput(ligneCourante,colCourante).setOptions({minChars:9999}); //Désactivation de l'autocomplete
		getCase(ligneCourante,colCourante).removeClass("selectionne");
	}
	else
		return false;

	ligneCourante = 0;
	colCourante = "code";
	return true;
}

// Achtung, cette fonction n'insère pas l'article de façon synchrone
function insertArticle(code){
    if(getCase(ligneCourante, 'ok').html() == code) //pour éviter les boucles
        return true;
	getInput(ligneCourante, "code").val(code);
	
	// Si l'article a besoin d'un numéro de commande et qu'on n'en a pas déjà un
	// On stocke pour pouvoir éventuellement supprimer
    var ligneDerniereCommande = ligneCourante;
	if(prod_requireparam[code]){
		if(getInput(ligneCourante,"qte").attr("readonly") == undefined){
			// sera appelé directement si on a déjà le code ou plus tard si on recherche la commande
            var fail = function() {
                if(prod_requireparamobligatoire[code]){
				    alert("Un numéro de commande est obligatoire pour cet article.");
				    ajouterLigne();
				    supprimerLigne(ligneDerniereCommande);
                    selectCase(nbLignes, 'code');
				    return false;
                }
                return insertArticleEnd(code);
            }
            var callback = function(idCommande, raison) {
                // Si idCommande est bien un numéro
			    if(!isNaN(idCommande) && idCommande > 0) {
                    searchingCommande = false; // on veut juste afficher le commande trouvée
                    if (!displayCommande(idCommande)) // la commande n'existe pas ou est déjà payée
                        return fail();
			    } else
                    return fail();
            } // fin callback
			if(getCase(ligneCourante,"param").html() == "") {
                searchCommande(code, callback);
                return false;
            }
			else
                return callback(parseInt(getCase(ligneCourante,"param").html()));
		}
	} else {
        return insertArticleEnd(code);
    }
}

function insertArticleEnd(code) {
	// Code JS spécifique à l'article
	if(prod_codejs[code] != "")
		eval(prod_codejs[code]);

	// Mise à jour de la désignation
	if(getCase(ligneCourante, "param").html() == "")
		getInput(ligneCourante,"designation").val(prod_designation[code]);
	else
		getInput(ligneCourante,"designation").val(prod_designation[code] + " n°" + getCase(ligneCourante, "param").html());

	// Mise à jour des prix
	majPrix(ligneCourante);

	//Si on a renseigné la dernière ligne : ajout d'une ligne
	if(ligneCourante == nbLignes) {
		ajouterLigne();
    }
    getCase(ligneCourante, 'ok').html(code);
    if (getCase(ligneCourante, 'param').html() != '') {
        selectCase(nbLignes, "code");
    } else {
        selectCase(ligneCourante, 'qte');
    }
    return true;
}

//Vérifie si le contenu du champ sélectionné est valide, et si c'est bon : met à jour la ligne
function inputValide(){
	var input = getInput(ligneCourante, colCourante);

	// Vérification d'un champ code
	if(colCourante == "code" && !(input.val() == "" && ligneCourante == nbLignes) && ligneCourante > 0){ // On peut avoir ce champ vide si c'est la dernière ligne
		var code = input.val();
		
		// Si c'est un code barre (avec support du checksum en lettres)
		if(code.length >= 10){
		  // Code EAN13
			for(var i=0;i<prod_ean13.length;i++)
				if(prod_ean13[i] != undefined && prod_ean13[i] == code)
					code = i;
					
			// Si le code fait toujours 10 ou 13 caractères, c'est un code perso
			if((code.length == 10 || code.length == 13) && !isNaN(code.substr(0, 10))){
			  getCase(ligneCourante, "param").html(parseInt(code.substr(4,6), 10)); // 6 derniers chiffres : param
			  code = parseInt(code.substr(0,4), 10); // 4 premiers chiffres : code article
			  input.val(code);
		  }
	  }
		else if(!isNaN(code))
			code = parseInt(code);
		
		// Si on n'a pas le produit dans le tableau, c'est pas bon
		if(isNaN(code) || prod_designation[code] == undefined){
			alert("Produit non trouvé");
			// On essaie de corriger à partir du champ désignation
			if(getInput(ligneCourante,"designation").val() != ""){
				code = trouverCode(getInput(ligneCourante,"designation").val());
				if(code != -1)
					input.val(code);
			}
			else {
				input.val("");
				getCase(ligneCourante, "param").html(""); // On enlève ce qu'on aurait pu mettre avec un code barre
			}
			
			return false;
		}
		
		// Valide
		else
			return insertArticle(code);
	}
	
	// Vérification d'un champ désignation
	else if(colCourante == "designation" && !(input.val() == "" && ligneCourante == nbLignes)){ //On peut avoir ce champ vide si c'est la dernière ligne
        var ok = getCase(ligneCourante, 'ok').html();
        if (ok != undefined && ok != '') // hack, il faut recoder toute la caisse en angular js
            return true;

		var designation = input.val();
		// On recherche la valeur du champ dans le tableau prod_designation
		var code = trouverCode(designation);
		
		// Non valide
		if(code == -1){
			alert("Produit non trouvé");
			if(!isNaN(parseInt(getInput(ligneCourante,"code").val())))
				input.val(prod_designation[parseInt(getInput(ligneCourante,"code").val())]);
			return false;
		}
		// Valide
		else
			return insertArticle(code);
	}
	
	// Vérification d'un champ quantité
	else if(colCourante == "qte"){
		var qte = input.val();

		if(qte.match(/^[0-9+*]+$/)){
			input.val(eval(qte));
			if(getInput(ligneCourante,"code").val() != "")
				majPrix(ligneCourante); // màj des prix
		}
		else { // Non valide
			alert("Quantité invalide");
			input.val(1);

			if(getInput(ligneCourante,"code").val() != "")
				majPrix(ligneCourante); // màj des prix

			return false;
		}
	}
	return true; // On retourne true : l'input est valide
}

// Retourne le code d'un produit selon sa designation, -1 si non trouvé
function trouverCode(designation){
	for(var i=0;i<prod_designation.length;i++)
		if(prod_designation[i] != undefined && prod_designation[i].toLowerCase() == designation.toLowerCase())
			return i;

	return -1;
}

// Met à jour les colonnes "prix unitaire", "remise" et "sous-total" d'une certaine ligne, puis le total global
function majPrix(ligne){
	var code = parseInt(getInput(ligne,"code").val());
	var qte = parseInt(getInput(ligne,"qte").val());

    if (isNaN(code) || isNaN(qte))
        return;

	getCase(ligne,"prix").html(prod_prix[code].toFixed(2));
	getCase(ligne,"prix_asso").html(prod_prix_asso[code].toFixed(2));
	
	//Calcul de la remise (si on n'a pas coché "tarif asso")
	var remise = 0;
	if(!tarifAsso){
		if(prod_palier2[code] != 0 && prod_palier2[code] <= qte)
			remise = prod_remise2[code];
		else if(prod_palier1[code] != 0 && prod_palier1[code] <= qte)
			remise = prod_remise1[code];
	}
	
	if(remise != 0)
		getCase(ligne,"remise").html("-"+remise*100+"%");
	else
		getCase(ligne,"remise").html("");
	
	//Sous total
	var sousTotal = arrondi(qte * prod_prix[code] * (1-remise));
	var sousTotal_asso = arrondi(qte * prod_prix_asso[code] * (1-remise));
	getCase(ligne,"sous-total").html(sousTotal.toFixed(2));
	getCase(ligne,"sous-total_asso").html(sousTotal_asso.toFixed(2));
	
	//Total
	majTotal();
}

// Met à jour le total
function majTotal(){
	//Parcourt toutes les lignes pour faire la somme des sous-totaux
	var total = 0, totalAsso = 0;
	for(i=1;i<=nbLignes;i++){
		if(getCase(i,"sous-total").html() != "")
			total += parseFloat(getCase(i,"sous-total").html());
		if(getCase(i,"sous-total_asso").html() != "")
			totalAsso += parseFloat(getCase(i,"sous-total_asso").html());
	}		
	
	//Si on n'a pas coché "tarif asso" :
	if(!tarifAsso){
		//$("#avantRemiseBDE").html(""); //montant "avant remise bde" vide
		$("#total").html(total.toFixed(2)); //montant "total" = somme des sous-totaux
	}
	//Si on l'a coché :
	else {
		//$("#avantRemiseBDE").html(total.toFixed(2)+" &euro;"); //montant "avant remise bde" = somme des sous-totaux
		$("#total").html(totalAsso.toFixed(2)); //montant "total" prend en compte la remise
	}
}

// Met à jour toutes les lignes (et donc le total)
function majTout(){
	for(var i=1;i<=nbLignes;i++) {
		majPrix(i);
    }
}

function arrondi(num){
	return Math.round(num*100)/100;
}

//Renvoie un pointeur DOM vers une ligne
function getLigne(ligne){
	return $("#tableau_caisse tr:eq("+ligne+")");
}

//Renvoie un pointeur DOM vers une case
function getCase(ligne,colonne){	
	return $("#tableau_caisse tr:eq("+ligne+") td."+colonne);
}

//Renvoie un pointeur DOM vers un champ
function getInput(ligne,colonne){
	return getCase(ligne, colonne).children("input");
}
