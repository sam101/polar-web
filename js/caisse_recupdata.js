//Récupération des données sur les articles et les associations

var prod_designation = new Array(); //Tableau des désignations en fonction du code de l'article
var prod_prix = new Array(); //Tableau des prix en fonction du code de l'article, prix non cotisants
var prod_prix_asso = new Array(); //Tableau des prix pour assos
var prod_requireparam = new Array(); // Les articles qui ont besoin d'un numéro de commande
var prod_requireparamobligatoire = new Array(); // Ceux où c'est obligatoire
var prod_palier1 = new Array();
var prod_remise1 = new Array();
var prod_palier2 = new Array();
var prod_remise2 = new Array();
var prod_codejs = new Array();
var prod_ean13 = new Array();
var banques = new Array();

var prod_designation_filled = new Array(); //idem

$(document).ready(function(){ //Après chargement de la page
	//Récupération des articles
	$.get(racine+"caisse/index_control?articles",function(xml){
		
		//Pour chaque article on renseigne les tableaux
		$(xml).find("article").each(function(){
			var code = parseInt($(this).find("code").text());
			prod_designation[code] = $(this).find("designation").text();
			prod_prix[code] = parseFloat($(this).find("prix").text());
			prod_prix_asso[code] = parseFloat($(this).find("prixasso").text());
			prod_requireparam[code] = parseFloat($(this).find("requireparam").text());
			prod_requireparamobligatoire[code] = parseFloat($(this).find("requireparamobligatoire").text());
			prod_palier1[code] = parseInt($(this).find("palier1").text());
			prod_remise1[code] = parseFloat($(this).find("remise1").text());
			prod_palier2[code] = parseInt($(this).find("palier2").text());
			prod_remise2[code] = parseFloat($(this).find("remise2").text());
			prod_codejs[code] = $(this).find("codejs").text();
			prod_ean13[code] = $(this).find("ean13").text();
		});
		
		//Remplissage des tableaux utilisés par l'autocompletion
		var j=0;
		for(var i=0;i<prod_designation.length;i++)
			if(prod_designation[i] != undefined){
				prod_designation_filled[j] = prod_designation[i];
				j++;
			}
		
		//Ajout de la première ligne
		$("#tableau_boutons").removeClass("invisible");
		$("#tableau_caisse").removeClass("invisible");
		ajouterLigne();
		selectCase(1,"code");
		majTotal();
	});
});
