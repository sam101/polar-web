function ajouterCode(type, zone, code, code2) {
  var input = document.getElementById(zone);
  var scroll = document.getElementById(zone).scrollTop;
  input.focus();

  // Cas de IE
  if(typeof(document.selection) != 'undefined') {
    // On insére le code
    selection = document.selection.createRange();

    if(type == 1) {
      selection.text = ' ' + code + ' ';
      selection.select();
    }
    else if(type == 2) {
      selection.text = '<detecteurdedebut>' + code + selection.text + code2 + '<detecteurdefin>';

      var cobaye = input.value;
      input.value = input.value.replace(/<detecteurdedebut>/g, '');
      input.value = input.value.replace(/<detecteurdefin>/g, '');

      // On récupére la position de début et de fin de la sélection (à l'attaaaaaaaaaaaaaque mouarf)
      var regex_d_assaut = new RegExp('<detecteurdedebut>', 'gi');
      var attaque_debut = cobaye.split(regex_d_assaut);

      if(attaque_debut.length == 1) {
        var position_debut = code.length;
        regex_d_assaut = new RegExp('<detecteurdefin>', 'gi');
        var attaque_fin = attaque_debut[0].split(regex_d_assaut);
        var longueur_texte = attaque_fin[0].length - code.length - code2.length;
        var position_fin = attaque_fin[0].length - code2.length;
      }
      else {
        var position_debut = attaque_debut[0].length + code.length;
        regex_d_assaut = new RegExp('<detecteurdefin>', 'gi');
        var attaque_fin = attaque_debut[1].split(regex_d_assaut);
        var longueur_texte = attaque_fin[0].length - code.length - code2.length;
        var position_fin = attaque_fin[0].length - code2.length + attaque_debut[0].length;
      }

      var selection_fantome = input.createTextRange();
      selection_fantome.moveStart('character', position_debut);
      selection_fantome.moveEnd('character', -input.value.length + position_fin);
      selection_fantome.select();
    }

    document.getElementById(zone).scrollTop = scroll;
    smCode(zone);
  }

  // Cas de Mozilla, Netscape etc
  else if(typeof(input.selectionStart) != 'undefined') {
    // On insére le code
    var debut_selection = input.selectionStart;
    var fin_selection = input.selectionEnd;
    var texte = input.value.substring(debut_selection, fin_selection);

    if(type == 1) {
      input.value = input.value.substr(0, debut_selection) + ' ' + code + ' ' + input.value.substr(fin_selection);
      input.selectionStart = debut_selection + code.length + 2;
      input.selectionEnd = debut_selection + code.length + 2;
    }
    else if(type == 2) {
      input.value = input.value.substr(0, debut_selection) + code + texte + code2 + input.value.substr(fin_selection);
      input.selectionStart = debut_selection + code.length;
      input.selectionEnd = debut_selection + code.length + texte.length;
    }

    document.getElementById(zone).scrollTop = scroll;
    smCode(zone);
  }
}

function ajouterLien(zone) {
  var lien = '';
  var texte_lien = '';

  lien = prompt('Saisir l\'adresse du lien : ', 'http://');

  if(lien != null) {
    texte_lien = prompt('Saisir le texte affiché en place du lien.', '');

    if(texte_lien != null)
      ajouterCode(1, zone, '<lien adresse="'+lien+'">'+texte_lien+'</lien>', '');
  }
}

function ajouterImage(zone) {
  var adresse = '';

  adresse = prompt('Saisir l\'URL de l\'image désirée', 'http://');

  if(adresse != null)
    ajouterCode(1, zone, '<image adresse="'+adresse+'" />', '');
}

function smCode(zone) {
  document.getElementById('apercu').scrollTop = document.getElementById(zone).scrollTop;
  var contenu = document.getElementById(zone).value;

  var regex = new Array();
  regex[1] = /:\)/g;
  regex[2] = /=\)/g;
  regex[3] = /:D/g;
  regex[4] = /;\)/g;
  regex[5] = /\^\^/g;
  regex[6] = /:P/g;
  regex[7] = /\|D/g;
  regex[8] = /:\|/g;
  regex[9] = /:\(/g;
  regex[10] = /:s/g;
  regex[11] = /:\/ /g;
  regex[12] = /:\\/g;
  regex[13] = /:\$/g;
  regex[14] = /::zz::/g;
  regex[15] = /::yeah::/g;
  regex[16] = /\(l\)/g;
  regex[17] = /\(B\)/g;
  regex[18] = /:@/g;
  regex[19] = /:O/g;
  regex[20] = /:'\(/g;
  regex[21] = /\(L\)/g;
  regex[22] = /::666::/g;
  regex[23] = /::i::/g;
  regex[24] = /-->/g;
  regex[25] = /&lt;lien adresse="(.+)"&gt;/g;
  regex[26] = /&lt;\/lien&gt;/g;
  regex[27] = /&lt;image adresse="(.+)" \/&gt;/g;
  regex[28] = /&lt;gras&gt;/g;
  regex[29] = /&lt;\/gras&gt;/g;
  regex[30] = /&lt;italique&gt;/g;
  regex[31] = /&lt;\/italique&gt;/g;
  regex[32] = /&lt;souligne&gt;/g;
  regex[33] = /&lt;\/souligne&gt;/g;
  regex[34] = /&lt;barre&gt;/g;
  regex[35] = /&lt;\/barre&gt;/g;

  var decodeur = new Array();
  decodeur[1] = ':)';
  decodeur[2] = '=)';
  decodeur[3] = ':D';
  decodeur[4] = ';)';
  decodeur[5] = '^^';
  decodeur[6] = ':P';
  decodeur[7] = '|D';
  decodeur[8] = ':|';
  decodeur[9] = ':(';
  decodeur[10] = ':s';
  decodeur[11] = ':/';
  decodeur[12] = ':\\';
  decodeur[13] = ':$';
  decodeur[14] = '::zz::';
  decodeur[15] = '::yeah::';
  decodeur[16] = '(l)';
  decodeur[17] = '(B)';
  decodeur[18] = ':@';
  decodeur[19] = ':O';
  decodeur[20] = ':\'(';
  decodeur[21] = '(L)';
  decodeur[22] = '::666::';
  decodeur[23] = '::i::';
  decodeur[24] = '-->';
  decodeur[25] = '&lt;lien adresse="(.+)"&gt;§<a href="$1">§';
  decodeur[26] = '&lt;/lien&gt;§</a>';
  decodeur[27] = '&lt;image adresse="(.+)" \/&gt;§<img src="$1" alt="Image utilisateur" />§';
  decodeur[28] = '&lt;gras&gt;§<span class="gras">';
  decodeur[29] = '&lt;/gras&gt;§</span>';
  decodeur[30] = '&lt;italique&gt;§<span class="italique">';
  decodeur[31] = '&lt;/italique&gt;§</span>';
  decodeur[32] = '&lt;souligne&gt;§<span class="souligne">';
  decodeur[33] = '&lt;/souligne&gt;§</span>';
  decodeur[34] = '&lt;barre&gt;§<span class="barre">';
  decodeur[35] = '&lt;/barre&gt;§</span>';

  while(contenu.search(/</) != -1)
    contenu = contenu.replace(/</, '&lt;');

  while(contenu.search(/>/) != -1)
    contenu = contenu.replace(/>/, '&gt;');

  while(contenu.search(/\n/) != -1)
    contenu = contenu.replace(/\n/, '<br />');

  for(i = 1; i < decodeur.length; i++) {
    while(contenu.search(regex[i]) != -1) {
      if(decodeur[i].split('§').length == 1)
        contenu = contenu.replace(decodeur[i], '<img class="smiley" src="/polar/styles/0/smileys/'+i+'.gif" alt="Smiley '+i+'" />');
      else if(decodeur[i].split('§').length == 2) {
        balises = decodeur[i].split('§');
        contenu = contenu.replace(balises[0], balises[1]);
      }
      else if(decodeur[i].split('§').length == 3) {
        balises = decodeur[i].split('§');
        contenu = contenu.replace(new RegExp(balises[0], 'g'), balises[1]);
      }
    }
  }

  document.getElementById('apercu').innerHTML = contenu;
}

function testerApercu(checked) {
  if(checked == true)
    document.getElementById('cadre-apercu').style.display = '';
  else
    document.getElementById('cadre-apercu').style.display = 'none';
}

function modifierCadre(type) {
  if(type == 'plus') {
    document.getElementById('LDTcode').style.height = (parseInt(document.getElementById('LDTcode').style.height.split('p')[0]) + 50) + 'px';
    document.getElementById('apercu').style.height = (parseInt(document.getElementById('LDTcode').style.height.split('p')[0]) + 50) + 'px';
  }
  else if(type == 'moins') {
    if(parseInt(document.getElementById('LDTcode').style.height.split('p')[0]) > 200) {
      document.getElementById('LDTcode').style.height = (parseInt(document.getElementById('LDTcode').style.height.split('p')[0]) - 50) + 'px';
      document.getElementById('apercu').style.height = (parseInt(document.getElementById('LDTcode').style.height.split('p')[0]) - 50) + 'px';
    }
  }
}
