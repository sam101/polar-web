
var PdfJsViewer = function(div, opts) {

    this._opts = {
        minScale: 0.5,
        maxScale: 5,
        width: 900,
        height: 900,
        heightScaled:true,
        multipage: false
    }

    this._intOpts = {
        scale: 1.0,
        canvas: null,
        ctx: null,
        pdfDoc: null,
        heightScale: 1.0,
        pageNum: 1,
        pageNumSpan: null,
        pageCountSpan: null
    }

    {
        //
        // Disable workers to avoid yet another cross-origin issue (workers need the URL of
        // the script to be loaded, and currently do not allow cross-origin scripts)
        //
        PDFJS.disableWorker = true;

        for (var i in opts) {
            this._opts[i] = opts[i];
        }

        var obj = this;

        var pdfHeaderDiv = document.createElement('div');
        pdfHeaderDiv.style.backgroundColor = '#ccc';
        pdfHeaderDiv.style.borderBottom = '2px solid #aaa';
        pdfHeaderDiv.style.paddingTop = '3px';
        pdfHeaderDiv.style.width = '100%';
        pdfHeaderDiv.style.height = '22px';

        var pdfZoomButtons = document.createElement('div');

        var pdfZoomIn = document.createElement('button');
        pdfZoomIn.innerHTML = '+';
        pdfZoomIn.onclick = function() {obj.zoomIn(); return false;};
        pdfZoomButtons.appendChild(pdfZoomIn);

        var pdfZoomOut = document.createElement('button');
        pdfZoomOut.innerHTML = '-';
        pdfZoomOut.onclick = function() {obj.zoomOut(); return false;};
        pdfZoomButtons.appendChild(pdfZoomOut);



        if (this._opts.multipage) {
            pdfZoomButtons.style.cssFloat = "left";
            pdfZoomButtons.style.marginLeft = "20px";

            var pdfPageButtons = document.createElement('div');
            pdfPageButtons.style.cssFloat = "right";
            pdfPageButtons.style.marginRight = "20px";

            var pdfSpanPageNode = document.createElement('span');
            pdfSpanPageNode.style.marginRight = "5px";
            pdfSpanPageNode.style.color = "black";

            this._intOpts.pageNumSpan = document.createElement('span');
            this._intOpts.pageCountSpan = document.createElement('span');

            pdfSpanPageNode.appendChild(document.createTextNode("Page "));
            pdfSpanPageNode.appendChild(this._intOpts.pageNumSpan);
            pdfSpanPageNode.appendChild(document.createTextNode("/"));
            pdfSpanPageNode.appendChild(this._intOpts.pageCountSpan);

            pdfPageButtons.appendChild(pdfSpanPageNode);

            var pdfPagePrevious = document.createElement('button');
            pdfPagePrevious.innerHTML = '<-';
            pdfPagePrevious.onclick = function() {obj.previousPage(); return false;};
            pdfPageButtons.appendChild(pdfPagePrevious);

            var pdfPageNext = document.createElement('button');
            pdfPageNext.innerHTML = '->';
            pdfPageNext.onclick = function() {obj.nextPage(); return false;};
            pdfPageButtons.appendChild(pdfPageNext);



            pdfHeaderDiv.appendChild(pdfPageButtons);

        }

        pdfHeaderDiv.appendChild(pdfZoomButtons);
        div.appendChild(pdfHeaderDiv);

        var pdfContent = document.createElement('div');
        pdfContent.style.width = this._opts.width + 'px';
        pdfContent.style.height = this._opts.height - 22 + 'px';
        pdfContent.style.overflow = 'auto';

        var pdfCanvas = document.createElement('canvas');
        pdfCanvas.style.border = '1px solid black';
        pdfCanvas.style.margin = '0 auto';
        pdfContent.appendChild(pdfCanvas);


        div.style.width = this._opts.width + 'px';
        div.style.height = this._opts.height + 'px';
        div.style.textAlign = 'center';

        div.appendChild(pdfContent);

        this._intOpts.canvas = pdfCanvas;
        this._intOpts.ctx = pdfCanvas.getContext('2d');
    }



    //
    // Get page info from document, resize canvas accordingly, and render page
    //
    this.renderPage = function() {
        var obj = this;
        // Using promise to fetch the page
        obj._intOpts.pdfDoc.getPage(this._intOpts.pageNum).then(function(page) {
            var trueScale = obj._intOpts.scale * obj._intOpts.heightScale;
            var viewport = page.getViewport(trueScale);
            obj._intOpts.canvas.height = viewport.height;
            obj._intOpts.canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
              canvasContext: obj._intOpts.ctx,
              viewport: viewport
            };
            page.render(renderContext);
        });
        
        if(this._opts.multipage) {
            this._intOpts.pageNumSpan.textContent = this._intOpts.pageNum;
            this._intOpts.pageCountSpan.textContent = this._intOpts.pdfDoc.numPages;
        }
    }

    //
    // Zoom In
    //
    this.zoomIn = function() {
      if (this._intOpts.scale >= this._opts.maxScale)
        return;
      this._intOpts.scale *= 1.25;
      this.renderPage();
    }

    //
    // Zoom Out
    //
    this.zoomOut = function() {
      if (this._intOpts.scale <= this._opts.minScale)
        return;
      this._intOpts.scale *= 0.8;
      this.renderPage();
    }

    this.previousPage = function() {
        if (this._intOpts.pageNum <= 1) {
            return;
        }
        this._intOpts.pageNum--;
        this.renderPage();
    }

    this.nextPage = function() {
        if (this._intOpts.pageNum >= this._intOpts.pdfDoc.numPages) {
            return;
        }
        this._intOpts.pageNum++;
        this.renderPage();
    }


    //
    // Asynchronously download PDF as an ArrayBuffer
    //

    this.loadPdf = function(url) {
        var obj = this;
        PDFJS.getDocument(url).then(function getPdfHelloWorld(_pdfDoc) {
            obj._intOpts.pdfDoc = _pdfDoc;

            if (obj._opts.heightScaled) {
                obj._intOpts.pdfDoc.getPage(1).then(function(page) {
                    var viewport = page.getViewport(1);
                    obj._intOpts.heightScale = obj._opts.height / viewport.height * 0.99;
                });
            }

            obj.renderPage();

        });
    }

}