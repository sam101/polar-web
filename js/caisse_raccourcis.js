//Gestion des raccourcis clavier

$(document).ready(function(){
	
	//Raccourcis toujours activés (boutons de paiement)
	$(window).keydown(function(event){
		if(!$("#tableau_boutons").hasClass("invisible")){
			switch(event.keyCode){
				case 114: //F3
					$("#btn-facture").click();
					return false;
				break;
				case 117: //F6
    				$("#btn-search-commande").click();
					return false;
				break;
				case 119: //F8
					if(!$("#especes").attr("disabled"))
						$("#especes").click();
					return false;
				break;
				case 120: //F9
					if(!$("#cheque").attr("disabled"))
						$("#cheque").click();
					return false;
				break;
				case 121: //F10
					if(!$("#cb").attr("disabled"))
						$("#cb").click();
					return false;
				break;
				case 118: //F7
					if(!$("#moneo").attr("disabled"))
						$("#moneo").click();
					return false;
				break;
				case 123: //F12
					if(!$("#assos").attr("disabled"))
						$("#assos").click();
					return false;
				break;
			}
		}
		
		//Raccourcis du tableau d'articles
		if(modePaiement == '' && !searchingCommande)
			switch(event.keyCode){
				case 37: //flèche gauche
					var colDest = getCase(ligneCourante,colCourante).prev().attr("class").split(" ");
					selectCase(ligneCourante,colDest[0]);
					$(".ac_results").css("display","none");
					return false;
				break;
				case 38: //flèche haut
					if($(".ac_results:visible").length == 0){
						if(ligneCourante>1)
							selectCase(ligneCourante-1,colCourante);
						return false;
					}
				break;
				case 39: //flèche droite
					var colDest = getCase(ligneCourante,colCourante).next().attr("class").split(" ");
					selectCase(ligneCourante,colDest[0]);
					$(".ac_results").css("display","none");
					return false;
				break;
				case 40: //flèche bas
					if($(".ac_results:visible").length == 0){
						if(ligneCourante<nbLignes)
							selectCase(ligneCourante+1,colCourante);
						return false;
					}
				break;
				case 13: //Entrée
				// Séléction intelligente de la case suivante
				if(colCourante == "code") {
					if(getInput(ligneCourante,"code").val() == "")
						selectCase(ligneCourante,"designation");
					else
						selectCase(ligneCourante,"qte");
                }
				else if(colCourante == "designation") {
					selectCase(ligneCourante,"qte");
                }
				else {
					var tmp = ligneCourante;
					if(!selectCase(ligneCourante+1,"code"))
						selectCase(tmp,"qte");
				}
				return false;
				break;
				case 115: //F4
					getLigne(ligneCourante).find(".supprimer_ligne").click();
					return false;
				break;
			}
		
        if (searchingCommande) {
            switch(event.keyCode) {
            case 40: // fleche bas
                if (!$('#modal-commandes .search').hasClass('invisible')) {
                    var lignes = $('#modal-commandes .select-table tbody tr');
                    $(lignes[ligneCommande]).removeClass('selectionne');
                    ligneCommande++;
                    if (ligneCommande >= lignes.length)
                        ligneCommande = 0;
                    $(lignes[ligneCommande]).addClass('selectionne');
                }
                return false;
            case 38: //fleche haut
                if (!$('#modal-commandes .search').hasClass('invisible')) {
                    var lignes = $('#modal-commandes .select-table tbody tr');
                    $(lignes[ligneCommande]).removeClass('selectionne');
                    ligneCommande--;
                    if (ligneCommande < 0)
                        ligneCommande = 0;
                    $(lignes[ligneCommande]).addClass('selectionne');
                }
                return false;
            case 13: //Entrée
                var selected = $('#modal-commandes .select-table tbody .selectionne td:first-child');
                if (currentCommande) {
                    doActionCommande(currentCommande);
                } else if (searchCode != 0 && $.isFunction(searchCallback)) {
                    var cb = searchCallback;
                    searchCallback = null;
                    $('#modal-commandes').modal('hide');
                    cb(parseInt(selected.html()), 'validated');
                } else {
                    var commande = parseInt(selected.html());
                    if(!isNaN(commande)) {
                        currentCommande = commande;
                        displayCommande(currentCommande);
                    }
                }
                return false;
            case 8: //Retour
                if ($('#modal-commandes .search').hasClass('invisible') && !onlyDisplay) {
                    $('#modal-commandes .search').removeClass('invisible');
                    $('#modal-commandes .infos').addClass('invisible');
                    $('#btn-retour-commandes').addClass('invisible');
                    $('#btn-display-infos-commande').removeClass('invisible');
                    $('#btn-valider-infos-commande').addClass('invisible');
                    $('#input-search-command').select();
                    currentCommande = 0;
                    return false;
                }
                return true;
            }
        }

		//Si on est dans l'étape du choix de paiement
		if (modePaiement != '')
			switch(event.keyCode){
				case 13: //Entrée
                    // on valide si on est au dernier input ou on sélectionne le prochain input
                    var inputs = $('#modal-validation .'+modePaiement+' input:visible, select:visible');
                    
				    if(inputs.length == 0 || (inputs.last().is(':focus') && !$("#modal-validation .valider").attr("disabled"))) {
				    		$("#modal-validation .valider").click();
                        return false;
                    }
                    
                    for (var i = 0; i < inputs.length; ++i) {
                        if($(inputs[i]).is(':focus')) {
                            $(inputs[i+1]).focus();
                            break;
                        }
                    }
					return false;
				break;
			}
	});
	
});
