//Gestion du choix de paiement et de la validation de la vente

var MONTANT_MIN_CB = 1;
var modePaiement = "";
var tarifAsso = false;
var asso = 0;
var venteValidee = false;

function otc(){
  var ev = document.createEvent("Events");
  ev.initEvent("openTheCaisse", true, false);
  document.dispatchEvent(ev);
}

//helper
var fmt_number = function(number) {
    if (number >= 10)
        return number.toString();
    else
        return "0" + number.toString();
}

var getFormattedDate = function() {
	  var newDate = new Date();
	  dateString = newDate.getFullYear() + "-";
 	  dateString += fmt_number(newDate.getMonth() + 1) + "-";
	  dateString += fmt_number(newDate.getDate())  + " ";
	  dateString += fmt_number(newDate.getHours()) + ":";
	  dateString += fmt_number(newDate.getMinutes()) + ":";
	  dateString += fmt_number(newDate.getSeconds());

	  return dateString;
}

var ajaxFail = function(xhr) {
    if (xhr.status == 403) {
        alert("Vous n'êtes plus connecté à la caisse; la page va être réchargée.");
        document.location.reload();
    }
}

$(document).ready(function(){
	// Javascript prototype : remove French accent from a string
	String.prototype.stringNoAccent = function() {
		var x=this.toString();
		x = x.replace(/[\xC0-\xC5]/g,"A");
		x = x.replace(/[\xE0-\xE5]/g,"a");
		x = x.replace(/[\xC8-\xCB]/g,"E");
		x = x.replace(/[\xE8-\xEB]/g,"e");
		x = x.replace(/[\xCC-\xCF]/g,"I");
		x = x.replace(/[\xEC-\xEF]/g,"i");
		x = x.replace(/[\xD2-\xD6]/g,"O");
		x = x.replace(/[\xF2-\xF6]/g,"o");
		x = x.replace(/[\xD9-\xDC]/g,"U");
		x = x.replace(/[\xF9-\xFC]/g,"u");
		x = x.replace(/[\xC7-\xC7]/g,"C");
		x = x.replace(/[\xE7-\xE7]/g,"c");
		x = x.replace(/[\xC6-\xC6]/g,"AE");
		x = x.replace(/[\xE6-\xE6]/g,"ae");
		return x;
	};

	Array.prototype.in_array = function(p_val) {
		for(var i = 0, l = this.length; i < l; i++)
			if(this[i] == p_val)
				return true;
	    return false;
	}
    Array.prototype.last = function() {
        return this[this.length-1];
    }

	// Quand on clique sur un des boutons paiement :
	$('.bouton-paiement').click(function(e){
		e.stopPropagation();
        if ($(this).hasClass('disabled') || modePaiement != '')
            return;
        if($(this).attr('id') == 'erreur') {
            document.location.reload();
            return;
        }
		if(deselect()){ //Si rien n'était sélectionné ou si la désélection se passe bien
			$(".ac_results").css("display", "none"); //On fait disparaitre la liste d'autocompletion au cas où

			// Moyen de paiement sélectionné
			var newModePaiement = $(this).attr("id");

			// Hook pour que les articles puissent savoir que le mode de paiement a été choisi
			for(var i=1;i<=nbLignes;i++)
				getLigne(i).trigger("modePaiementChoisi", [newModePaiement]);

			//On vérifie que le total n'est pas nul
			if(parseFloat($("#total").html()) == 0){
				alert("Y'a rien à payer !");
				selectCase(1,"code");
			}
		 	// On vérifie le total est ok niveau plancher CB
			else if(newModePaiement == "cb" && parseFloat($("#total").html()) < MONTANT_MIN_CB){
				alert("Montant minimum pour CB : " + MONTANT_MIN_CB + " euros");
				selectCase(1,"code");
			}
			else {
				modePaiement = newModePaiement;

                // payutc : si pas de carte passée on désactive le bouton valider
                if (modePaiement == 'payutc') {
                    if (currentCarteEtu)
                        $("#modal-validation .valider").removeAttr("disabled");
                    else
                        $("#modal-validation .valider").attr('disabled', 'disabled');
                }

		        $("#modal-validation .modal-page").addClass("invisible");
				$("#modal-validation ." + modePaiement).removeClass("invisible");
				$("#modal-validation").modal("show");

                // On vide le cache au cas où on a validé une nouvelle asso
                // depuis la dernière vente asso
                $('#nom-asso').flushCache();

                // On met le bouton concerné en vert
		        $("#"+modePaiement).removeClass("btn-info");
		        $("#"+modePaiement).addClass("btn-success");
			}
		}
		else
			getInput(ligneCourante,colCourante).select();
	});

    // Quand on affiche le modal
    $("#modal-validation").on('shown', function() {
        // si carte étu présente on valide direct
        if(modePaiement == 'payutc' && currentCarteEtu) {
            $("#modal-validation .valider").click();
        }
		// Pré-remplissage de la calculette à rendu
		$("#especes-donne").val(parseFloat($("#total").html())).keyup();
        $("#modal-validation ."+modePaiement+" input:visible:first").focus().select();
    });

	//Quand on change la valeur du bouton radio "tarif asso"
	$("input[name=tarifasso]").change(function(){
		// Désactivation de la remise
		if(!this.checked){
			// On échange les colonnes
			$(".sous-total_asso").addClass("invisible");
			$(".sous-total").removeClass("invisible");
			$(".prix_asso").addClass("invisible");
			$(".prix").removeClass("invisible");

			$("#tableau_boutons button").removeAttr("disabled");

			tarifAsso = false;
		}
		// Activation de la remise
		else {
			// On échange les colonnes
			$(".sous-total_asso").removeClass("invisible");
			$(".sous-total").addClass("invisible");
			$(".prix_asso").removeClass("invisible");
			$(".prix").addClass("invisible");

			$("#tableau_boutons button:not(#assos)").attr("disabled", "disabled");

			tarifAsso = true;
		}
		majTout();
	});

	// Mise à jour du champ "rendu"
	$("#modal-validation .especes .donne").keyup(function(){
		var donne = parseFloat($(this).val().replace(",","."));
		if(isNaN(donne))
			donne = 0;
		var rendu = donne - parseFloat($("#total").html());
		if(rendu>0)
			$("#modal-validation .especes .rendu").html(rendu.toFixed(2));
		else
			$("#modal-validation .especes .rendu").html("-");
	});

    $("#modal-validation").on('hidden', function() {
        if(venteValidee) {
            venteValidee = false;
            part_reset();
        }

        // reset carte étu
        currentCarteEtu = false;

        // On ràz le modal quand on le cache (annulation ou commande validée)
		$("#modal-validation .modal-page").addClass("invisible");
        // bouton de paiement
		$("#"+modePaiement).addClass("btn-info").removeClass("btn-success");
        // Selecteur de compte asso
        $("#compte-asso").parents('.control-group').addClass('invisible');
        $('input[name=tarifasso]').parents('.control-group').addClass('invisible');
        $('#quota-compte-asso').parents('.control-group').addClass('invisible');
        $("#nom-asso").val('');

		tarifAsso = false;
        asso = 0;
		$(".sous-total_asso").addClass("invisible");
		$(".sous-total").removeClass("invisible");
		$(".prix_asso").addClass("invisible");
		$(".prix").removeClass("invisible");

        // On ràz tous les inputs
		$("#modal-validation input").val("");

		$("#modal-validation .valider").removeAttr("disabled");
		$("#modal-validation .revenir").removeAttr("disabled");
		modePaiement = "";
		selectCase(1,"code");
        getInput(1, 'code').select();

        // Vu qu'on a peut être modifié le tarif asso on recalcule tout
		majTout();
    });

    // Marque les commandes comme payées
    var finalizeCommandes = function() {
        if (!venteValidee)
            return;

        var date = getFormattedDate();

        for (var i=1; i<=nbLignes; i++) {
            // On sélectionne les commandes qui sont des billets et on les envoie pour l'impression
            var num = parseInt(getCase(i, "param").html());
            if (!isNaN(num)) {
                var commande = getCommande(num);
                commande.paiement = date;
                if (commande.commande_type == 'Billet')
                    printBillet(num, commande.prenom+' '+commande.nom, commande.type, commande.codebarre);
            }
        }
    }

    var part_reset = function() {
		while(nbLignes>0)
			supprimerLigne(nbLignes);
        currentCarteEtu = false;
		ajouterLigne();
		$("#btn-facture").removeAttr("checked");
		selectCase(1,"code");
    };

	//Bouton "valider"
	$("#modal-validation .valider").click(function(){
        if (venteValidee) { // On a un IDVente a traiter
            var champFacture = $('#modal-validation input[name=nom-facture]');
            if (champFacture.length >= 1) {
                var nomFacture = champFacture.val();
                if (nomFacture == '') {
                    champFacture.focus();
                    return;
                } else {
					window.open(racine+"caisse/facture_control?id="+venteValidee+"&nom="+nomFacture,"idfact");
                }
            }

            part_reset();
            venteValidee = false;
            $('#modal-validation').modal('hide');
            return;
        }

		if(modePaiement == '' || $("#modal-validation .valider").attr("disabled"))
            return;
        $('#modal-validation .working').removeClass('invisible');
        $('#modal-validation .'+modePaiement).addClass('invisible');
        // on désactive les boutons
		$("#modal-validation .valider").attr("disabled", "disabled");
		$("#modal-validation .revenir").attr("disabled", "disabled");
        // on désactive la fermeture du modal
        $("#modal-validation").modal({keyboard:false, backdrop:'static'});

		//parametres POST
		var postparams = {};
		postparams["total"] = parseFloat($("#total").html());
		if($("#btn-facture:checked").length == 1)
			postparams["facture"] = true;
		else
			postparams["facture"] = false;

		if(modePaiement == 'assos' && postparams["facture"] == 1){
			postparams["facture"] = "0";
			alert("Les associations recoivent leur facture \340 la fin du semestre. Il est impossible d'imprimer la facture d'une vente.");
		}
		postparams["modepaiement"] = modePaiement;
		if(modePaiement == "assos"){
			postparams["representant"] = $("#client-asso").val();
			postparams["asso"] = $('#compte-asso').val();
		}
		else {
			postparams["representant"] = "";
			postparams["asso"] = "";
		}
		postparams["motdepasse"] = $("#modal-validation ."+modePaiement+" .mot-de-passe").val();
		postparams["banque"] = $("#cheque-banque").val();
		postparams["emetteur"] = $("#cheque-emetteur").val();
		if(tarifAsso)
			postparams["tarif"] = 'asso';
		else
			postparams["tarif"] = 'normal';

        if(modePaiement == 'payutc') {
            if (currentCarteEtu)
                postparams["idetu"] = currentCarteEtu;
            else
                return;
        }

		for(var i=1;i<=nbLignes-1;i++){
			postparams["article_"+i] = parseInt(getInput(i,"code").val())+'';
			postparams["quantite_"+i] = parseInt(getInput(i,"qte").val())+'';
			postparams["param_"+i] = parseInt(getCase(i,"param").html())+'';

			if(!tarifAsso)
				postparams["prix_"+i] = getCase(i,"sous-total").html();
			else
				postparams["prix_"+i] = getCase(i,"sous-total_asso").html();
		}

		// AJAAAAAAAAAX
		$.post(racine+"caisse/index_control", postparams, function(text){
            var data = {};
            try {
                data = JSON.parse(text);
            } catch (SyntaxError) {
                data = {error:99, message:'Erreur inconnue, merci de contacter le webmaster'};
            }
            if (!data.error && !data.idvente) {
                data = {error:99, message:'Erreur inconnue, merci de contacter le webmaster'};
            }

			if(data.error){ //Problème
                alert('Paiement refusé\n'+data.message);

				// On refait apparaître les boutons de paiement
                if (modePaiement == 'payutc')
                    currentCarteEtu = false;
                else
				    $("#modal-validation .valider").removeAttr("disabled");
				$("#modal-validation .revenir").removeAttr("disabled");
				$("#modal-validation ."+modePaiement).removeClass("invisible");
				$("#modal-validation .working").addClass("invisible");

				if(data.error == 1) //mauvais mot de passe asso
					$("#modal-validation .assos .mot-de-passe").focus();
			}
			else { //Vente confirmée
				// Ouverture du tiroir caisse si besoin
				if(modePaiement == "especes")
					openTheCaisse();

				// Hook pour que les articles puissent savoir que la vente a été validée
				$(document).trigger("venteConfirmee");
                venteValidee = data.idvente;
                finalizeCommandes();

				$("#modal-validation .valider").removeAttr("disabled");
				$("#modal-validation .revenir").removeAttr("disabled");
				$("#modal-validation .working").addClass("invisible");
				$("#modal-validation .ok").removeClass("invisible");

				var arendre = "";
				var texte = "Vente enregistrée.<br/><br/>";
                if (data.message)
                    texte += data.message + "<br/><br/>";
				if(postparams["modepaiement"] == "especes" &&
                   $("#modal-validation .especes .rendu").html() != "") {
					texte += "À rendre : "+$("#modal-validation .especes .rendu").html()+".\n\n";
                }
				if(postparams["facture"] && modePaiement != 'assos'){
                    texte += 'À quel nom doit être la facture ?<br/><input type="text" name="nom-facture" autocomplete="off" />';
				}
				$('#modal-validation .ok .message').html(texte);

                // Si on a un input pour la facture on focus
                $('#modal-validation input[name=nom-facture]').focus();
			}
		}, 'text').fail(ajaxFail);
	});

    // On fixe la hauteur du modal de validation pour pouvoir voir
    // le montant de la vente
    $('#modal-validation').css('top', $('#tableau_caisse').offset().top);

    var setComptes = function(comptes) {
        $this = $('#compte-asso');
        if (comptes == undefined)
            $this.html('<option value="">-Choisis une asso-</option>');
        else {
            $this.html('');
            comptes.forEach(function(compte) {
                $this.append('<option value="'+compte.id+'" data-quota="'+compte.quota+'" data-tarif="'+compte.tarif_asso+'" data-en-cours="'+compte.en_cours+'">'+compte.nom+'</option>');
            });
            $this.change();
        }
        return $this;
    };

    $('#compte-asso').change(function() {
        // droit au tarif asso ?
        if($('#compte-asso option[value='+$(this).attr('value')+']').attr('data-tarif') == 'true') {
            $('input[name=tarifasso]').attr('checked', 'checked')
                .removeAttr('disabled').change();
        } else {
            $('input[name=tarifasso]').removeAttr('checked')
                .attr('disabled', 'disabled').change();
        }
        // quota
        var quota = $('#compte-asso option[value='+$(this).attr('value')+']').attr('data-quota');
        $('#modal-validation .valider').removeAttr('disabled');
        if (quota == 'null') {
            $('#quota-compte-asso').html('Aucun');
        } else {
            quota = parseFloat(quota);
            var en_cours = parseFloat($('#compte-asso option[value='+$(this).attr('value')+']').attr('data-en-cours'));
            $('#quota-compte-asso').html(quota.toFixed(2)+' €');
            if (en_cours >= quota) {
                $('#quota-compte-asso').append(' (Dépassé)');
                $('#modal-validation .valider').attr('disabled', 'disabled');
            }
        }
        $('#client-asso').focus();
    });

    $('#nom-asso').autocomplete(racine+'assos/ajax',
                                {minChars: 1,
                                 extraParams: {action: 'search'},
                                 cacheLength: 30,
                                 delay: 100,
                                 autoFill: true
                                })
        .result(function(e, data, f) {
            $.get(racine+'assos/ajax',
                  {action: 'infos',
                   nom: data[0]},
                  'json')
                .done(function(data) {
                    setComptes(data.comptes).parents('.control-group')
                        .removeClass('invisible');
                    $('#compte-asso').focus();
                    $('input[name=tarifasso]').parents('.control-group')
                        .removeClass('invisible');
                    $('#quota-compte-asso').parents('.control-group')
                        .removeClass('invisible');
                })
                .fail(ajaxFail);
        });
});
