var commandesCache = {} // id -> {nom, type, payé, prêt, retiré, cache_timestamp}
var searchingCommande = false;
var currentCommande = 0;
var ligneCommande = 0;
var searchCode = 0;
var searchCallback = null;
var onlyDisplay = false;

String.prototype.titleize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
}

function insertCommande(commande) {
    // on vérifie que la commande ne soit pas déjà dans la liste des articles
    var done = false;
    for (var i=1; i <= nbLignes; i++) {
        if (done)
            return false
        if (getCase(i, "param").html() == commande.id)
            done = true;
    }

    if (commande.paiement) //déjà payé = osef
        return false;

    // on vérifie que la billetterie ne soit pas complète
    // ajax sync :-(
    if (commande.commande_type == 'Billet') {
        var pass = false;
        $.ajax(racine+'commandes/ajax', {cache : false,
                                         data : {action: 'valider_billetterie',
                                                 id: commande.id},
                                         dataType : 'json',
                                         async : false,
                                         success : function(data) {
                                             if (data[0] == 'OK')
                                                 pass = true;
                                         }});
        if (!pass) {
            alert("Désolé, la billetterie est complète");
            return false;
        }
    }
    else if (commande.commande_type == 'Panier') {
        insertPanier(commande);
        return false;
    }

    var ligneDerniereCommande = ligneCourante;
    /* Si on vient de la recherche de commande (F6) on est peut être sur
       une ligne déjà remplie donc il faut en créer une autre */
    if(!searchCode && !onlyDisplay) {
        //selectCase(nbLignes, 'code');
        getInput(ligneCourante, 'code').val(commande.article);
    }

	// Mise à jour des paramètres dans le tableau
	getInput(ligneCourante,"qte").val(commande.quantite);
	getCase(ligneCourante,"param").html(commande.id);

	// On désactive l'édition de l'article
	getInput(ligneCourante,"qte").attr("readonly", "readonly");
	getInput(ligneCourante,"code").attr("readonly", "readonly");
	getInput(ligneCourante,"designation").attr("readonly", "readonly");
	getCase(ligneCourante,"code").removeClass("selectable");
	getCase(ligneCourante,"designation").removeClass("selectable");
	getCase(ligneCourante,"qte").removeClass("selectable");

    insertArticleEnd(commande.article);

    return true;
}

function insertPanier(commande) {
    if (commande.commande_type != 'Panier')
        return;

    $.get(racine+'commandes/ajax',
          {action:'search', panier:commande.id, search:''})
        .done(function(data) {
            for (var i=0; i<data.length; i++) {
                commande = data[i];
                commandesCache[commande.id] = commande;
                insertCommande(commande);
                selectCase(nbLignes, "code");
            }
        })
        .fail(function(xhr) {
            ajaxFail(xhr);
        });    
}

var doActionCommande = function(num) {
    if (num.id != undefined) // on peut passer un objet commande directement
        var commande = num;
    else
        var commande = getCommande(num);

    if (commande == null) {
        $('#modal-commandes').modal('hide');
        return false;
    }
    if (!commande.paiement) {
        $('#modal-commandes').modal('hide');
        return insertCommande(commande);
    }
    if (commande.paiement && commande.pret) {
        $.get(racine+'commandes/ajax',
              {action:'retirer', id:commande.id})
            .done(function(data) {
                if (data[0] == 'OK') {
                    commande.retrait = getFormattedDate();
                    $('#modal-commandes').modal('hide');
                }
                else
                    alert(data[0]);
            })
            .fail(function(xhr) {
                ajaxFail(xhr);
            });
        return false;
    }
    return true;
}

$(document).ready(function() {
    $('#modal-commandes').on('hidden', function() {
        searchingCommande = false;
        currentCommande = 0;
        onlyDisplay = false;
        if (searchCode != 0 && $.isFunction(searchCallback))
            searchCallback('hidden');
        searchCode = 0;
        searchCallback = null;
        $('#input-search-commande').val('');
        $('#modal-commandes .search').removeClass('invisible');
        $('#modal-commandes .infos').addClass('invisible');
        $('#commandes-titre').html('Rechercher une commande');
        $('#btn-display-infos-commande').addClass('invisible');
        $('#btn-retour-commandes').addClass('invisible');
        $('#btn-valider-infos-commande').addClass('invisible');
        $('#modal-commandes .select-table tbody').html('');
	    getInput(ligneCourante, colCourante).select();
    });

    $('#modal-commandes').on('shown', function() {
        searchingCommande = true;
        //$('#modal-commandes').focus();
        setTimeout(function() {
            $('#input-search-commande').filter(':visible').focus().select();
        }, 0);
    });

    $('#btn-valider-infos-commande').click(function() {
        var event = $.Event('keydown', {keyCode:13});
        $(window).triggerHandler(event);
    });
    $('#btn-display-infos-commande').click(function() {
        var event = $.Event('keydown', {keyCode:13});
        $(window).triggerHandler(event);
    });
    $('#btn-retour-commandes').click(function() {
        var event = $.Event('keydown', {keyCode:8});
        $(window).triggerHandler(event);
    });

    $('#input-search-commande').keyup(function(event) {
        if([13, 38, 40].in_array(event.keyCode))
            return;

        var val = $('#input-search-commande').val();
        if(val != '' && val.length >= 3) {
            if (val.length == 10 && /\d{10}/.test(val)) { // code barre
                val = val.substring(4, 10);
            }
            // Numéro | Nom | Type | État
            $.get(racine+'commandes/ajax', {action:'search',
                                            search:val,
                                            code:searchCode,
                                            limit:8})
                .done(function(data) {
                    var rows = '';
                    for (i=0; i<data.length; i++) {
                        var commande = data[i];
                        commandesCache[commande.id] = commande;
                        var etat = 'En attente de paiement';
                        if (commande.paiement)
                            etat = 'En attente de réalisation';
                        if (commande.pret)
                            etat = 'Prêt';
                        if (commande.retrait && commande.commande_type != 'Poster' && commande.commande_type != 'Badges' && commande.commande_type != 'Plastification')
                            etat = 'Déjà Retiré';
                        if (commande.nom == null)
                            var nom = 'Non assignée';
                        else
                            var nom = commande.prenom+' '+commande.nom;
                        rows += '<tr><td>'+commande.id+'</td><td>'+nom+'</td><td>'+commande.type+'</td><td>'+etat+'</td></tr>';
                    }
                    $('#modal-commandes .select-table tbody').html(rows);
                    $('#modal-commandes .select-table tbody tr').click(lineClicked);
                    ligneCommande = 0;
                    if (rows != '') {
                        // On pré-sélectionne le premier
                        $('#modal-commandes .select-table tbody tr:first-child').addClass('selectionne');
                        // Boutons d'action
                        $('#btn-display-infos-commande').removeClass('invisible');
                    } else {
                        $('#btn-display-infos-commande').addClass('invisible');
                    }

                })
                .fail(function(xhr) {
                    $('#modal-commandes .select-table tbody').html('');
                    ajaxFail(xhr);
                });
        } else {
            $('#modal-commandes .select-table tbody').html('');
        }
    });
});

function getCommande(id) {
    if (commandesCache[id] == undefined) {
        $.ajax(racine+'commandes/ajax',
               {async: false,
                method: 'GET',
                data :{action:'search',
                       search:id,
                       limit:1},
                success: function(data) {
                    if (data.length == 1 && data[0].id == id) {
                        commandesCache[data[0].id] = data[0];
                    }
                },
                error: function(xhr) {
                    ajaxFail(xhr);
                }});
    }
    return commandesCache[id];
}

function displayCommande(id) {
    var commande = getCommande(id);
    if (commande != undefined && commande.nom == null) {
        return doActionCommande(commande);
    }

    if (!searchingCommande) {
        currentCommande = id;
        searchingCommande = true;
        onlyDisplay = true;
    }
    $('#modal-commandes').modal('show');
    $('#modal-commandes .search').addClass('invisible');

    $('#btn-display-infos-commande').addClass('invisible');

    var p = $('#modal-commandes .infos').removeClass('invisible');
    if (commande == undefined) {
        p.html('<h3>La commande ' + id + ' n\'existe pas</h3>');
        return false;
    }

    $('#btn-retour-commandes').removeClass('invisible');
    $('#btn-valider-infos-commande').removeClass('invisible');

    p.html('<h3>'+commande.prenom.titleize()+' '+commande.nom.titleize()+'</h3>'+
           '<h4>'+commande.type+'</h4>'+
           '<p>Commandé le '+commande.commande+'<br/>');
    var action = 'Payer';
    if(commande.paiement)
        p.append('Payée le '+commande.paiement+'<br/>');
    else
        p.append('Non payée<br/>');
    if (commande.pret && commande.paiement) {
        p.append('Faite le '+commande.pret+'<br/>');
        action = 'Retirer';
    } if (commande.retrait && commande.paiement) {
        action = false;
    }
    p.append('</p>');
    $('#commandes-titre').html('Info commande n° '+id);
    if (action)
        $('#btn-valider-infos-commande').html(action+' <span class="raccourcis">Entrée</span>');
    else
        $('#btn-valider-infos-commande').addClass('invisible');

    return true;
}

function searchCommande(code, callback) {
    searchCode = code;
    searchCallback = callback;
    $('#modal-commandes').modal('show');
}

function lineClicked() {
    var lignes = $('#modal-commandes .select-table tbody tr');
    $(lignes[ligneCommande]).removeClass('selectionne');

    for (var i = 0; i<lignes.length; i++) {
        if (lignes[i] == this) { // il faut comparer directement les éléments DOM, si on passe par jquery ça ne marche pas
            ligneCommande = i;
            break;
        }
    }
    $(lignes[ligneCommande]).addClass('selectionne');

    // On envoie un évènement 'Enter', ça nous évite d'avoir à recher le numéro de commande sélectionné
    var e = jQuery.Event("keydown");
    e.which = e.keyCode = 13; // Entrée
    $(document).trigger(e);
}