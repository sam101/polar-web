-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Serveur: sql.mde.utc:3306
-- Généré le : Ven 23 Mars 2012 à 14:38
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.10-1~dotdeb.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `polar`
--

-- --------------------------------------------------------

--
-- Structure de la table `polar_annales`
--

CREATE TABLE `polar_annales` (
  `Code` varchar(4) NOT NULL DEFAULT '',
  `NbPages` int(3) DEFAULT NULL,
  `EnVente` int(1) DEFAULT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `polar_annales_statistiques`
--

CREATE TABLE `polar_annales_statistiques` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Debut` datetime NOT NULL,
  `Fin` datetime NOT NULL,
  `NbAnnales` int(5) NOT NULL,
  `NbPagesTheoriques` int(5) NOT NULL,
  `NbPagesReel` int(5) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_annales_sujets`
--

CREATE TABLE `polar_annales_sujets` (
  `sujet_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sujet_uv` varchar(4) NOT NULL,
  `sujet_type` enum('t','u','v','p','q','m','f') NOT NULL,
  `sujet_semestre` varchar(1) NOT NULL,
  `sujet_annee` tinyint(4) NOT NULL,
  `sujet_corrige` tinyint(4) NOT NULL,
  `sujet_pages` tinyint(4) NOT NULL DEFAULT '0',
  `sujet_info` varchar(50) NOT NULL,
  `sujet_commentaire` varchar(50) NOT NULL COMMENT 'Ce commentaire apparaît sur le sommaire',
  `sujet_ecarte` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Mettre de coté le sujet',
  PRIMARY KEY (`sujet_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_annales_uvs`
--

CREATE TABLE `polar_annales_uvs` (
  `uv_code` varchar(7) NOT NULL,
  `uv_enseignee` enum('Inconnu','A','P','AP','Rempl','Fin') NOT NULL DEFAULT 'Inconnu',
  `uv_info` varchar(200) NOT NULL,
  `uv_enVente` tinyint(1) NOT NULL DEFAULT '0',
  `uv_modif` datetime NOT NULL,
  PRIMARY KEY (`uv_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `polar_annales_pages`
--

CREATE TABLE `polar_annales_pages` (
  `pages_code` varchar(7) NOT NULL,
  `pages_types` varchar(7) NOT NULL,
  `pages_nbrPages` int(11) NOT NULL,
  PRIMARY KEY (`pages_code`,`pages_types`)
)

--
-- Structure de la table `polar_assos`
--

CREATE TABLE `polar_assos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MailAsso` varchar(50) NOT NULL,
  `Asso` varchar(70) NOT NULL,
  `President` varchar(50) NOT NULL,
  `MailPresident` varchar(50) DEFAULT NULL,
  `TelPresident` varchar(20) DEFAULT NULL,
  `Tresorier` varchar(50) NOT NULL,
  `MailTresorier` varchar(50) DEFAULT NULL,
  `TelTresorier` varchar(20) DEFAULT NULL,
  `MotDePasse` varchar(128) NOT NULL,
  `MotDePasseSecurise` tinyint(1) NOT NULL DEFAULT '0',
  `DateCreation` datetime NOT NULL,
  `Etat` enum('Actif','DefautPaiement','AttenteActivation','Clos','Supprime') NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_assos_conventions`
--

CREATE TABLE `polar_assos_conventions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Asso` varchar(50) NOT NULL,
  `Date` datetime NOT NULL,
  `But` varchar(1000) NOT NULL,
  `PolarSengage` text NOT NULL,
  `AssoSengage` text NOT NULL,
  `TypeSignataire` varchar(50) NOT NULL,
  `NomSignataire` varchar(50) NOT NULL,
  `Attestation` int(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_assos_factures`
--

CREATE TABLE `polar_assos_factures` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Asso` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Montant` float NOT NULL,
  `Encaisse` int(1) NOT NULL,
  `Cheque` int(15) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_assos_paiement`
--

CREATE TABLE `polar_assos_paiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Asso` varchar(50) NOT NULL,
  `Date` datetime NOT NULL,
  `Convention` int(11) NOT NULL,
  `But` varchar(150) NOT NULL,
  `Montant` float NOT NULL,
  `Detail` varchar(500) NOT NULL,
  `TypeSignataire` varchar(50) NOT NULL,
  `Signataire` varchar(50) NOT NULL,
  `Cheque` int(15) NOT NULL,
  `DateVirement` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Asso` (`Asso`),
  KEY `Convention` (`Convention`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_billetterie`
--

CREATE TABLE `polar_billetterie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(100) NOT NULL,
  `Date` datetime NOT NULL,
  `Asso` int(11) DEFAULT NULL,
  `Places` int(11) DEFAULT NULL,
  `Preinscription` tinyint(1) NOT NULL,
  `Contact` varchar(300) DEFAULT NULL,
  `InfosPreinscription` text,
  `CodesBarres` text,
  `CBOrientation` enum('p','l') DEFAULT NULL,
  `CBType` varchar(15) DEFAULT NULL,
  `CBWidth` int(4) DEFAULT NULL,
  `CBHeight` int(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_billetterie_organisateurs`
--

CREATE TABLE `polar_billetterie_organisateurs` (
  `Billetterie` int(11) NOT NULL,
  `User` int(11) NOT NULL,
  PRIMARY KEY (`Billetterie`,`User`),
  KEY `User` (`User`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_billetterie_tarifs`
--

CREATE TABLE `polar_billetterie_tarifs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Evenement` int(11) NOT NULL,
  `Intitule` varchar(300) CHARACTER SET latin1 NOT NULL,
  `TypeCommande` int(11) NOT NULL,
  `RequireUTC` tinyint(1) NOT NULL,
  `RequireBDE` tinyint(1) NOT NULL,
  `Prive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Evenement` (`Evenement`),
  KEY `TypeCommande` (`TypeCommande`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- --------------------------------------------------------

--
-- Structure de la table `polar_caisses`
--

CREATE TABLE IF NOT EXISTS `polar_caisses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Mouvement` int(11) NOT NULL,
  `Details` int(11) NOT NULL,
  `TheoriqueCB` double NOT NULL,
  `ReelCB` double NOT NULL,
  `TheoriqueMoneo` double NOT NULL,
  `ReelMoneo` double NOT NULL,
  `TheoriquePayutc` double NOT NULL,
  `ReelPayutc` double NOT NULL,
  `TheoriqueEspeces` double NOT NULL,
  `ReelEspeces` double NOT NULL,
  `EspecesApres` double NOT NULL,
  `DernierIDVente` int(11) NOT NULL,
  `Commentaire` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Mouvement` (`Mouvement`),
  UNIQUE KEY `Details` (`Details`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_articles`
--

CREATE TABLE `polar_caisse_articles` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CodeCaisse` int(3) NOT NULL,
  `EnVente` int(1) NOT NULL,
  `Actif` int(1) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Secteur` int(2) NOT NULL,
  `PrixAchat` float NOT NULL,
  `PrixVente` float NOT NULL,
  `PrixVenteAsso` float NOT NULL,
  `TVA` float NOT NULL,
  `Palier1` int(5) NOT NULL,
  `Remise1` float NOT NULL,
  `Palier2` int(5) NOT NULL,
  `Remise2` float NOT NULL,
  `StockInitial` int(11) DEFAULT NULL,
  `Stock` int(11) DEFAULT NULL,
  `SeuilAlerte` int(11) DEFAULT NULL,
  `CodeJS` text NOT NULL,
  `Photo` varchar(50) NOT NULL,
  `EAN13` varchar(13) DEFAULT NULL,
  `Auteur` int(11) DEFAULT NULL,
  `Fournisseur` varchar(200) DEFAULT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CodeCaisse` (`CodeCaisse`),
  KEY `Secteur` (`Secteur`),
  KEY `Auteur` (`Auteur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_cheques`
--

CREATE TABLE `polar_caisse_cheques` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `NumVente` bigint(20) NOT NULL,
  `Date` datetime NOT NULL,
  `Numero` int(11) DEFAULT NULL,
  `Banque` varchar(50) NOT NULL,
  `Montant` float NOT NULL,
  `Emetteur` varchar(50) NOT NULL,
  `Ordre` varchar(50) NOT NULL,
  `PEC` int(1) NOT NULL,
  `DateEncaissement` datetime DEFAULT NULL,
  `Motif` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NumVente` (`NumVente`),
  KEY `Numero` (`Numero`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_cheques_manuels`
--

CREATE TABLE `polar_caisse_cheques_manuels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Client` varchar(150) NOT NULL,
  `Date` datetime NOT NULL,
  `Location` int(11) NOT NULL,
  `Montant` float NOT NULL,
  `Detail` varchar(500) NOT NULL,
  `Cheque` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Location` (`Location`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_coffre`
--

CREATE TABLE `polar_caisse_coffre` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `User` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Billets` float NOT NULL,
  `Pieces` float NOT NULL,
  `Echange` enum('u','o','n') NOT NULL DEFAULT 'u',
  `Commentaire` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `User` (`User`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_details`
--

CREATE TABLE IF NOT EXISTS `polar_caisse_details` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `c1` int(15) NOT NULL,
  `c2` int(15) NOT NULL,
  `c5` int(15) NOT NULL,
  `c10` int(15) NOT NULL,
  `c20` int(15) NOT NULL,
  `c50` int(15) NOT NULL,
  `e1` int(15) NOT NULL,
  `e2` int(15) NOT NULL,
  `e5` int(15) NOT NULL,
  `e10` int(15) NOT NULL,
  `e20` int(15) NOT NULL,
  `e50` int(15) NOT NULL,
  `e100` int(15) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_detail`
--

CREATE TABLE `polar_caisse_detail` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `1c` int(15) NOT NULL,
  `2c` int(15) NOT NULL,
  `5c` int(15) NOT NULL,
  `10c` int(15) NOT NULL,
  `20c` int(15) NOT NULL,
  `50c` int(15) NOT NULL,
  `1e` int(15) NOT NULL,
  `2e` int(15) NOT NULL,
  `5e` int(15) NOT NULL,
  `10e` int(15) NOT NULL,
  `20e` int(15) NOT NULL,
  `50e` int(15) NOT NULL,
  `100e` int(15) NOT NULL,
  `CBTheorique` float NOT NULL,
  `CBReel` float NOT NULL,
  `MoneoTheorique` float NOT NULL,
  `MoneoReel` float NOT NULL,
  `TotalAvantTransfert` float NOT NULL,
  `VersCaisse` float NOT NULL,
  `VersCoffre` float NOT NULL,
  `TotalApresTransfert` float NOT NULL,
  `DernierIDVente` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_erreurs`
--

CREATE TABLE `polar_caisse_erreurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `user` int(11) NOT NULL,
  `erreur` float NOT NULL,
  `diff_cb` float NOT NULL,
  `diff_moneo` float NOT NULL,
  `cheques_nok` int(10) NOT NULL,
  `commentaire` varchar(1500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_ip`
--

CREATE TABLE `polar_caisse_ip` (
  `ip` varchar(15) NOT NULL,
  `Type` varchar(500) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_secteurs`
--

CREATE TABLE `polar_caisse_secteurs` (
  `Secteur` varchar(50) NOT NULL,
  `Code` int(2) NOT NULL,
  PRIMARY KEY (`Secteur`,`Code`),
  KEY `Code` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_ventes`
--

CREATE TABLE `polar_caisse_ventes` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IDVente` bigint(20) NOT NULL,
  `Article` bigint(20) NOT NULL,
  `Quantite` int(15) NOT NULL,
  `Date` datetime NOT NULL,
  `Finalise` int(1) NOT NULL,
  `Asso` int(15) DEFAULT NULL,
  `Client` varchar(50) NOT NULL,
  `Facture` int(1) NOT NULL,
  `PrixFacture` float NOT NULL,
  `MontantTVA` float NOT NULL,
  `Tarif` enum('normal','asso') NOT NULL,
  `MoyenPaiement` varchar(15) NOT NULL,
  `Permanencier` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Article` (`Article`),
  KEY `IDVente` (`IDVente`),
  KEY `Permanencier` (`Permanencier`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_ventes_global`
--

CREATE TABLE `polar_caisse_ventes_global` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IDVente` bigint(20) NOT NULL,
  `Article` bigint(20) NOT NULL,
  `Quantite` int(15) NOT NULL,
  `Date` datetime NOT NULL,
  `Finalise` int(1) NOT NULL,
  `Asso` int(15) DEFAULT NULL,
  `Client` varchar(50) NOT NULL,
  `Facture` int(1) NOT NULL,
  `PrixFacture` float NOT NULL,
  `MontantTVA` float NOT NULL,
  `Tarif` enum('normal','asso') NOT NULL,
  `MoyenPaiement` varchar(15) NOT NULL,
  `Permanencier` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Article` (`Article`),
  KEY `Permanencier` (`Permanencier`),
  KEY `IDVente` (`IDVente`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_caisse_virement`
--

CREATE TABLE `polar_caisse_virement` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `Emetteur` varchar(50) NOT NULL,
  `Destinataire` varchar(50) NOT NULL,
  `Montant` float NOT NULL,
  `Effectue` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_commandes`
--

CREATE TABLE `polar_commandes` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` int(11) NOT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Prenom` varchar(50) DEFAULT NULL,
  `Mail` varchar(50) DEFAULT NULL,
  `Asso` int(11) DEFAULT NULL,
  `DateCommande` datetime DEFAULT NULL,
  `IPCommande` varchar(20) DEFAULT NULL,
  `DatePaiement` datetime DEFAULT NULL,
  `IDVente` bigint(20) DEFAULT NULL,
  `DatePrete` datetime DEFAULT NULL,
  `IDPreparateur` int(11) DEFAULT NULL,
  `DateRetrait` datetime DEFAULT NULL,
  `IDRetrait` int(11) DEFAULT NULL,
  `DateRetour` datetime DEFAULT NULL,
  `IDRetour` int(11) DEFAULT NULL,
  `Termine` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IDVente` (`IDVente`),
  KEY `IDPreparateur` (`IDPreparateur`),
  KEY `IDRetrait` (`IDRetrait`),
  KEY `IDRetour` (`IDRetour`),
  KEY `Type` (`Type`),
  KEY `Asso` (`Asso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_commandes_contenu`
--

CREATE TABLE `polar_commandes_contenu` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IDCommande` bigint(20) NOT NULL,
  `Detail` varchar(150) DEFAULT NULL,
  `Quantite` int(5) DEFAULT NULL,
  `Article` bigint(20) DEFAULT NULL,
  `PrixTTC` float DEFAULT NULL,
  `PrixUnitaire` float DEFAULT NULL,
  `TVA` float DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDCommande` (`IDCommande`),
  KEY `Article` (`Article`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_commandes_types`
--

CREATE TABLE `polar_commandes_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(40) NOT NULL,
  `CodeProduit` int(15) NOT NULL,
  `Actif` int(1) NOT NULL,
  `RequireCommande` int(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CodeProduit` (`CodeProduit`),
  KEY `Nom` (`Nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_evenementiel`
--

CREATE TABLE `polar_evenementiel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `Lieu` varchar(50) NOT NULL,
  `Titre` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Auteur` int(11) NOT NULL,
  `Creation` datetime NOT NULL,
  `Photo` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DeposePar` (`Auteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_evenementiel_participants`
--

CREATE TABLE `polar_evenementiel_participants` (
  `Evenement` int(5) NOT NULL,
  `Participant` int(5) NOT NULL,
  PRIMARY KEY (`Evenement`,`Participant`),
  KEY `Evenement` (`Evenement`),
  KEY `Participant` (`Participant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_manuels`
--

CREATE TABLE `polar_manuels` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `uv` varchar(15) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `Actif` tinyint(4) NOT NULL,
  `CD` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_manuels_flotte`
--

CREATE TABLE `polar_manuels_flotte` (
  `Manuel` int(3) NOT NULL,
  `Serie` int(3) NOT NULL,
  `Numero` int(3) NOT NULL,
  `Etat` enum('stock','location','rendu','perdu') NOT NULL,
  PRIMARY KEY (`Manuel`,`Serie`,`Numero`,`Etat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_msdnaa`
--

CREATE TABLE `polar_msdnaa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `envoye` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `login` (`login`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_news`
--

CREATE TABLE `polar_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `news` longtext NOT NULL,
  `date` datetime NOT NULL,
  `Etat` enum('online','offline') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auteur` (`auteur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_notifications`
--

CREATE TABLE `polar_notifications` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ActionID` varchar(50) NOT NULL,
  `Date` datetime NOT NULL,
  `Destinataire` int(11) NOT NULL,
  `Icone` varchar(50) NOT NULL,
  `Texte` varchar(500) NOT NULL,
  `Chemin` varchar(100) NOT NULL,
  `Etat` enum('non-lu','lu','archive') NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Destinataire` (`Destinataire`),
  KEY `ActionID` (`ActionID`),
  KEY `Etat` (`Etat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_parametres`
--

CREATE TABLE `polar_parametres` (
  `Nom` varchar(50) NOT NULL,
  `Valeur` text NOT NULL,
  PRIMARY KEY (`Nom`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_perms`
--

CREATE TABLE `polar_perms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Jour` int(5) NOT NULL,
  `Creneau` int(5) NOT NULL,
  `Permanencier` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Permanencier` (`Permanencier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_perms_troc`
--

CREATE TABLE `polar_perms_troc` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Semaine` varchar(7) NOT NULL,
  `Jour` int(5) NOT NULL,
  `Creneau` int(5) NOT NULL,
  `Donneur` int(11) NOT NULL,
  `Receveur` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Donneur` (`Donneur`),
  KEY `Receveur` (`Receveur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_postit`
--

CREATE TABLE `polar_postit` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Module` varchar(50) NOT NULL,
  `Section` varchar(50) DEFAULT NULL,
  `Auteur` int(11) NOT NULL,
  `Public` int(1) NOT NULL,
  `Date` datetime NOT NULL,
  `Message` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Auteur` (`Auteur`),
  KEY `Module` (`Module`),
  KEY `Section` (`Section`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_screen`
--

CREATE TABLE `polar_screen` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `Mail` varchar(50) NOT NULL,
  `Nom` varchar(25) NOT NULL,
  `Message` varchar(150) NOT NULL,
  `Actif` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_screen_photos`
--

CREATE TABLE `polar_screen_photos` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Adresse` varchar(60) NOT NULL,
  `Actif` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_screen_videos`
--

CREATE TABLE `polar_screen_videos` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Adresse` varchar(60) NOT NULL,
  `Actif` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_securite_droits`
--

CREATE TABLE `polar_securite_droits` (
  `ID` int(15) NOT NULL,
  `User` int(15) NOT NULL,
  PRIMARY KEY (`ID`,`User`),
  KEY `User` (`User`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_securite_pages`
--

CREATE TABLE `polar_securite_pages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(150) NOT NULL,
  `Menu` varchar(150) DEFAULT NULL,
  `Acces` enum('public','member','staff','private') NOT NULL,
  `Module` varchar(50) DEFAULT NULL,
  `Section` varchar(50) DEFAULT NULL,
  `Postit` enum('module','section') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Module` (`Module`),
  KEY `Section` (`Section`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_sondages`
--

CREATE TABLE `polar_sondages` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(100) NOT NULL,
  `Date` datetime NOT NULL,
  `Par` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Par` (`Par`),
  KEY `polar_sondages_ibfk_1` (`Par`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_sondages_jetons`
--

CREATE TABLE `polar_sondages_jetons` (
  `Sondage` int(15) NOT NULL,
  `Utilisateur` int(15) NOT NULL,
  PRIMARY KEY (`Sondage`,`Utilisateur`),
  KEY `Utilisateur` (`Utilisateur`),
  KEY `polar_sondages_jetons_ibfk_1` (`Sondage`),
  KEY `polar_sondages_jetons_ibfk_2` (`Utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_sondages_questions`
--

CREATE TABLE `polar_sondages_questions` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Sondage` int(10) NOT NULL,
  `Question` varchar(100) NOT NULL,
  `Type` enum('unique','multiple','libre') NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Sondage` (`Sondage`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_sondages_reponses`
--

CREATE TABLE `polar_sondages_reponses` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Question` int(10) NOT NULL,
  `Reponse` varchar(100) NOT NULL,
  `NbVotes` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Question` (`Question`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_sondages_reponses_libres`
--

CREATE TABLE `polar_sondages_reponses_libres` (
  `Question` int(11) NOT NULL,
  `Reponse` text NOT NULL,
  PRIMARY KEY (`Question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_tickets`
--

CREATE TABLE `polar_tickets` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `category` mediumint(9) NOT NULL,
  `opened` datetime NOT NULL,
  `status` enum('new','active','closed') NOT NULL DEFAULT 'new',
  `Salt` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`),
  KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_tickets_categories`
--

CREATE TABLE `polar_tickets_categories` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Objets` text NOT NULL,
  `template` text NOT NULL,
  `acces` enum('public','member','staff','private') NOT NULL,
  `GenererCheque` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_tickets_cheques`
--

CREATE TABLE `polar_tickets_cheques` (
  `Ticket` mediumint(9) NOT NULL,
  `Cheque` int(11) NOT NULL,
  UNIQUE KEY `Ticket` (`Ticket`,`Cheque`),
  KEY `Cheque` (`Cheque`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_tickets_messages`
--

CREATE TABLE `polar_tickets_messages` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `ticket` mediumint(9) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `sent` datetime NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket` (`ticket`),
  KEY `sender` (`sender`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_tickets_responsables`
--

CREATE TABLE `polar_tickets_responsables` (
  `Categorie` mediumint(15) NOT NULL,
  `Utilisateur` int(15) NOT NULL,
  PRIMARY KEY (`Categorie`,`Utilisateur`),
  KEY `Utilisateur` (`Utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_utilisateurs`
--

CREATE TABLE `polar_utilisateurs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IPClient` varchar(20) NOT NULL,
  `DateCreation` datetime NOT NULL,
  `Login` varchar(8) NOT NULL,
  `MotDePasse` varchar(128) NOT NULL,
  `MotDePasseSecurise` tinyint(1) NOT NULL DEFAULT '0',
  `Email` varchar(50) NOT NULL,
  `Staff` int(1) NOT NULL DEFAULT '0',
  `Bureau` int(1) NOT NULL DEFAULT '0',
  `Ancien` int(1) NOT NULL DEFAULT '0',
  `Responsable` int(2) DEFAULT NULL,
  `Poste` varchar(150) DEFAULT NULL,
  `Presentation` text NOT NULL,
  `Nom` varchar(150) NOT NULL,
  `Prenom` varchar(150) NOT NULL,
  `Sexe` enum('m','f') DEFAULT NULL,
  `Telephone` varchar(10) DEFAULT NULL,
  `Newsletter` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Login` (`Login`),
  KEY `Responsable` (`Responsable`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_utilisateurs_new`
--

CREATE TABLE `polar_utilisateurs_new` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(8) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Verification` varchar(32) NOT NULL,
  `Creation` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_wiki`
--

CREATE TABLE `polar_wiki` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Page` int(5) NOT NULL,
  `Categorie` int(3) NOT NULL,
  `Titre` varchar(200) NOT NULL,
  `Contenu` text NOT NULL,
  `Auteur` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Categorie` (`Categorie`),
  KEY `Auteur` (`Auteur`),
  KEY `Page` (`Page`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `polar_wiki_categories`
--

CREATE TABLE `polar_wiki_categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `polar_wallpaper`
--

CREATE TABLE `polar_wallpaper` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ImageFileName` varchar(100) NOT NULL,
  `ThumbnailFileName` varchar(100) NOT NULL,
  `Date` datetime NOT NULL,
  `UserCreatorID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserCreatorID` (`UserCreatorID`)
);

-- --------------------------------------------------------

--
-- Table structure for table `polar_wallpaper_storage`
--

CREATE TABLE `polar_wallpaper_storage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Year` smallint(6) NOT NULL,
  `Week` smallint(6) NOT NULL,
  `ImageFileName` varchar(100) NOT NULL,
  `ThumbnailFileName` varchar(100) NOT NULL,
  `Date` datetime NOT NULL,
  `UserCreatorID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserCreatorID` (`UserCreatorID`)
);

-- --------------------------------------------------------

--
-- Table structure for table `polar_wallpaper_votes`
--

CREATE TABLE `polar_wallpaper_votes` (
  `UserID` int(11) NOT NULL,
  `WallpaperID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `WallpaperID` (`WallpaperID`)
);


--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `polar_assos_factures`
--
ALTER TABLE `polar_assos_factures`
  ADD CONSTRAINT `polar_assos_factures_ibfk_1` FOREIGN KEY (`Asso`) REFERENCES `polar_assos` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_assos_paiement`
--
ALTER TABLE `polar_assos_paiement`
  ADD CONSTRAINT `polar_assos_paiement_ibfk_1` FOREIGN KEY (`Convention`) REFERENCES `polar_assos_conventions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_billetterie`
--
ALTER TABLE `polar_billetterie`
  ADD CONSTRAINT `polar_billetterie_ibfk_2` FOREIGN KEY (`Asso`) REFERENCES `polar_assos` (`ID`);

--
-- Contraintes pour la table `polar_billetterie_organisateurs`
--
ALTER TABLE `polar_billetterie_organisateurs`
  ADD CONSTRAINT `polar_billetterie_organisateurs_ibfk_1` FOREIGN KEY (`Billetterie`) REFERENCES `polar_billetterie` (`ID`),
  ADD CONSTRAINT `polar_billetterie_organisateurs_ibfk_2` FOREIGN KEY (`User`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_billetterie_tarifs`
--
ALTER TABLE `polar_billetterie_tarifs`
  ADD CONSTRAINT `polar_billetterie_tarifs_ibfk_1` FOREIGN KEY (`Evenement`) REFERENCES `polar_billetterie` (`ID`),
  ADD CONSTRAINT `polar_billetterie_tarifs_ibfk_2` FOREIGN KEY (`TypeCommande`) REFERENCES `polar_commandes_types` (`ID`);

--
-- Contraintes pour la table `polar_caisses`
--
ALTER TABLE `polar_caisses`
  ADD CONSTRAINT `polar_caisses_ibfk_2` FOREIGN KEY (`Details`) REFERENCES `polar_caisse_details` (`ID`),
  ADD CONSTRAINT `polar_caisses_ibfk_3` FOREIGN KEY (`Mouvement`) REFERENCES `polar_caisse_coffre` (`ID`);

--
-- Contraintes pour la table `polar_caisse_articles`
--
ALTER TABLE `polar_caisse_articles`
  ADD CONSTRAINT `polar_caisse_articles_ibfk_1` FOREIGN KEY (`Secteur`) REFERENCES `polar_caisse_secteurs` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_caisse_articles_ibfk_2` FOREIGN KEY (`Auteur`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_caisse_coffre`
--
ALTER TABLE `polar_caisse_coffre`
  ADD CONSTRAINT `polar_caisse_coffre_ibfk_1` FOREIGN KEY (`User`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_caisse_erreurs`
--
ALTER TABLE `polar_caisse_erreurs`
  ADD CONSTRAINT `polar_caisse_erreurs_ibfk_1` FOREIGN KEY (`user`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_caisse_ventes`
--
ALTER TABLE `polar_caisse_ventes`
  ADD CONSTRAINT `polar_caisse_ventes_ibfk_2` FOREIGN KEY (`Permanencier`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_caisse_ventes_ibfk_3` FOREIGN KEY (`Article`) REFERENCES `polar_caisse_articles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_caisse_ventes_ibfk_4` FOREIGN KEY (`Asso`) REFERENCES `polar_assos_comptes` (`ID`);

--
-- Contraintes pour la table `polar_caisse_ventes_global`
--
ALTER TABLE `polar_caisse_ventes_global`
  ADD CONSTRAINT `polar_caisse_ventes_global_ibfk_1` FOREIGN KEY (`Article`) REFERENCES `polar_caisse_articles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_caisse_ventes_global_ibfk_2` FOREIGN KEY (`Asso`) REFERENCES `polar_assos_comptes` (`ID`);

--
-- Contraintes pour la table `polar_commandes`
--
ALTER TABLE `polar_commandes`
  ADD CONSTRAINT `polar_commandes_ibfk_1` FOREIGN KEY (`IDPreparateur`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_commandes_ibfk_2` FOREIGN KEY (`IDRetrait`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_commandes_ibfk_3` FOREIGN KEY (`IDRetour`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_commandes_ibfk_4` FOREIGN KEY (`Type`) REFERENCES `polar_commandes_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_commandes_ibfk_5` FOREIGN KEY (`Asso`) REFERENCES `polar_assos` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_commandes_contenu`
--
ALTER TABLE `polar_commandes_contenu`
  ADD CONSTRAINT `polar_commandes_contenu_ibfk_1` FOREIGN KEY (`IDCommande`) REFERENCES `polar_commandes` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_evenementiel`
--
ALTER TABLE `polar_evenementiel`
  ADD CONSTRAINT `polar_evenementiel_ibfk_1` FOREIGN KEY (`Auteur`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_evenementiel_participants`
--
ALTER TABLE `polar_evenementiel_participants`
  ADD CONSTRAINT `polar_evenementiel_participants_ibfk_2` FOREIGN KEY (`Participant`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_evenementiel_participants_ibfk_3` FOREIGN KEY (`Evenement`) REFERENCES `polar_evenementiel` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `polar_manuels_flotte`
--
ALTER TABLE `polar_manuels_flotte`
  ADD CONSTRAINT `polar_manuels_flotte_ibfk_1` FOREIGN KEY (`Manuel`) REFERENCES `polar_manuels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_news`
--
ALTER TABLE `polar_news`
  ADD CONSTRAINT `polar_news_ibfk_1` FOREIGN KEY (`auteur`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_notifications`
--
ALTER TABLE `polar_notifications`
  ADD CONSTRAINT `polar_notifications_ibfk_1` FOREIGN KEY (`Destinataire`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_perms`
--
ALTER TABLE `polar_perms`
  ADD CONSTRAINT `polar_perms_ibfk_1` FOREIGN KEY (`Permanencier`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_perms_troc`
--
ALTER TABLE `polar_perms_troc`
  ADD CONSTRAINT `polar_perms_troc_ibfk_1` FOREIGN KEY (`Donneur`) REFERENCES `polar_utilisateurs` (`ID`),
  ADD CONSTRAINT `polar_perms_troc_ibfk_2` FOREIGN KEY (`Receveur`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_postit`
--
ALTER TABLE `polar_postit`
  ADD CONSTRAINT `polar_postit_ibfk_1` FOREIGN KEY (`Module`) REFERENCES `polar_securite_pages` (`Module`),
  ADD CONSTRAINT `polar_postit_ibfk_2` FOREIGN KEY (`Section`) REFERENCES `polar_securite_pages` (`Section`),
  ADD CONSTRAINT `polar_postit_ibfk_3` FOREIGN KEY (`Auteur`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Contraintes pour la table `polar_securite_droits`
--
ALTER TABLE `polar_securite_droits`
  ADD CONSTRAINT `polar_securite_droits_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `polar_securite_pages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_securite_droits_ibfk_2` FOREIGN KEY (`User`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_sondages`
--
ALTER TABLE `polar_sondages`
  ADD CONSTRAINT `polar_sondages_ibfk_1` FOREIGN KEY (`Par`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_sondages_ibfk_2` FOREIGN KEY (`Par`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION;

--
-- Contraintes pour la table `polar_sondages_jetons`
--
ALTER TABLE `polar_sondages_jetons`
  ADD CONSTRAINT `polar_sondages_jetons_ibfk_1` FOREIGN KEY (`Sondage`) REFERENCES `polar_sondages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_sondages_jetons_ibfk_2` FOREIGN KEY (`Utilisateur`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_sondages_jetons_ibfk_4` FOREIGN KEY (`Utilisateur`) REFERENCES `polar_utilisateurs` (`ID`),
  ADD CONSTRAINT `polar_sondages_jetons_ibfk_5` FOREIGN KEY (`Sondage`) REFERENCES `polar_sondages` (`ID`) ON DELETE CASCADE;

--
-- Contraintes pour la table `polar_sondages_questions`
--
ALTER TABLE `polar_sondages_questions`
  ADD CONSTRAINT `polar_sondages_questions_ibfk_1` FOREIGN KEY (`Sondage`) REFERENCES `polar_sondages` (`ID`) ON DELETE CASCADE;

--
-- Contraintes pour la table `polar_sondages_reponses`
--
ALTER TABLE `polar_sondages_reponses`
  ADD CONSTRAINT `polar_sondages_reponses_ibfk_2` FOREIGN KEY (`Question`) REFERENCES `polar_sondages_questions` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_sondages_reponses_ibfk_3` FOREIGN KEY (`Question`) REFERENCES `polar_sondages_questions` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_tickets`
--
ALTER TABLE `polar_tickets`
  ADD CONSTRAINT `polar_tickets_ibfk_1` FOREIGN KEY (`category`) REFERENCES `polar_tickets_categories` (`id`);

--
-- Contraintes pour la table `polar_tickets_cheques`
--
ALTER TABLE `polar_tickets_cheques`
  ADD CONSTRAINT `polar_tickets_cheques_ibfk_1` FOREIGN KEY (`Cheque`) REFERENCES `polar_caisse_cheques` (`ID`),
  ADD CONSTRAINT `polar_tickets_cheques_ibfk_2` FOREIGN KEY (`Ticket`) REFERENCES `polar_tickets` (`id`);

--
-- Contraintes pour la table `polar_tickets_messages`
--
ALTER TABLE `polar_tickets_messages`
  ADD CONSTRAINT `polar_tickets_messages_ibfk_3` FOREIGN KEY (`ticket`) REFERENCES `polar_tickets` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `polar_tickets_responsables`
--
ALTER TABLE `polar_tickets_responsables`
  ADD CONSTRAINT `polar_tickets_responsables_ibfk_2` FOREIGN KEY (`Utilisateur`) REFERENCES `polar_utilisateurs` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `polar_tickets_responsables_ibfk_3` FOREIGN KEY (`Categorie`) REFERENCES `polar_tickets_categories` (`id`);

--
-- Contraintes pour la table `polar_utilisateurs`
--
ALTER TABLE `polar_utilisateurs`
  ADD CONSTRAINT `polar_utilisateurs_ibfk_1` FOREIGN KEY (`Responsable`) REFERENCES `polar_caisse_secteurs` (`Code`);

--
-- Contraintes pour la table `polar_wiki`
--
ALTER TABLE `polar_wiki`
  ADD CONSTRAINT `polar_wiki_ibfk_1` FOREIGN KEY (`Categorie`) REFERENCES `polar_wiki_categories` (`ID`),
  ADD CONSTRAINT `polar_wiki_ibfk_2` FOREIGN KEY (`Auteur`) REFERENCES `polar_utilisateurs` (`ID`);


--
-- Constraints for table `polar_wallpaper`
--
ALTER TABLE `polar_wallpaper`
  ADD CONSTRAINT `polar_wallpaper_ibfk_1` FOREIGN KEY (`UserCreatorID`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Constraints for table `polar_wallpaper_storage`
--
ALTER TABLE `polar_wallpaper_storage`
  ADD CONSTRAINT `polar_wallpaper_storage_ibfk_1` FOREIGN KEY (`UserCreatorID`) REFERENCES `polar_utilisateurs` (`ID`);

--
-- Constraints for table `polar_wallpaper_votes`
--
ALTER TABLE `polar_wallpaper_votes`
  ADD CONSTRAINT `polar_wallpaper_votes_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `polar_utilisateurs` (`ID`),
  ADD CONSTRAINT `polar_wallpaper_votes_ibfk_2` FOREIGN KEY (`WallpaperID`) REFERENCES `polar_wallpaper` (`ID`) ON DELETE CASCADE;
