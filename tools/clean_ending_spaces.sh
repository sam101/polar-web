#!/bin/sh

POLAR_DIR=`dirname "$0"`
POLAR_DIR="$POLAR_DIR/.."

cd $POLAR_DIR

FILES=`find . -type f \( -name "*.php" -o -name "*.css" -o -name "*.js" -o -name "*.html" \)`

for f in $FILES ; do
    sed -E -i '' -e 's/[[:blank:]]+$//g' "${f}"
done