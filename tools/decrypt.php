<?php
$fichierSource = '../Documents/Backups/Sauvegarde_20110209.sql.gz.aes';
$fichierDestination = '../Documents/Backups/Sauvegarde_20110209.sql.gz';

require("../inc/backup.php");

// Lecture de l'archivage
if(!$fp = fopen($fichierSource, "rb"))
	die("Erreur d'ouverture de l'archive crptyée.");
$archive = '';
while (!feof($fp))
  $archive .= fread($fp, 8192);
fclose($fp);

if(strlen($archive) == 0)
	die("Erreur de lecture de l'archive cryptée.");

// Cryptage
require_once('../lib/Crypt/AES.php');
$aes = new Crypt_AES(CRYPT_AES_MODE_CBC);
$aes->setKey($clecryptage);
$aes->setKeyLength(256);
$output = $aes->decrypt($archive);

// Écriture des données cryptées
if(!$fp = fopen($fichierDestination, "wb"))
	die("Erreur d'ouverture du fichier destination.");
if(fwrite($fp, $output) === FALSE)
	die("Erreur d'écriture.");
fclose($fp);

echo 'ok';
