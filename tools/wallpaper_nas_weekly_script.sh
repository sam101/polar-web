#!/bin/sh

MAGIC_NUMBER=4210146 # Don't change it
WEEKLY_PHP_SCRIPT_URL="http://wwwassos.utc.fr/polar/wallpaper/weekly_script"
TMP_SAVE_FILE="/tmp/wallpaper_polar_file.tmp"
WALLPAPER_IMAGE_FILE="/mnt/HD_a2/Clones/polar.jpg"
NB_SEC_SLEEP=300

CONTINUE=true

while $CONTINUE ; do
	http_code=`wget --post-data="magic_number=$MAGIC_NUMBER" --server-response "$WEEKLY_PHP_SCRIPT_URL" -O "$TMP_SAVE_FILE" 2>&1 | awk '/^  HTTP/{print $2}'`

    case $http_code in

    000)
        echo "Connection error, next try in $NB_SEC_SLEEP secs";
        sleep "$NB_SEC_SLEEP";
        ;;

    200)
        CONTINUE=false;
        echo "Everything ok";
        ;;
    404)
        echo "Error, file not found" 1>&2
        exit
        ;;
    500)
        cat "$TMP_SAVE_FILE" 1>&2;
        echo 1>&2
        exit
        ;;
    *)
        echo "Unkwown error" 1>&2
        exit
        ;;
    esac
done

mv "$TMP_SAVE_FILE" "$WALLPAPER_IMAGE_FILE"