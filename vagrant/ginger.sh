#!/usr/bin/env bash

git clone --recursive https://github.com/simde-utc/ginger.git
cd ginger
composer install

cp /vagrant/vagrant/ginger/buildtime-conf.xml ./
cp /vagrant/vagrant/ginger/build.properties ./
cp /vagrant/vagrant/ginger/config.php ./

mysql --user=root --password=root -e "CREATE DATABASE ginger CHARACTER SET utf8;"

chmod +x vendor/propel/propel1/generator/bin/phing.php
./propel-gen
./propel-gen insert-sql

mysql --user=root --password=root ginger < /vagrant/vagrant/ginger/data.sql

ln -s /home/vagrant/ginger/web /var/www/ginger

cd