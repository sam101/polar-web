-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 22 Février 2014 à 16:47
-- Version du serveur: 5.5.35-0ubuntu0.13.10.2
-- Version de PHP: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Contenu de la table `tj_app_fun_afu`
--

INSERT INTO `tj_app_fun_afu` (`afu_id`, `app_id`, `fun_id`, `afu_service`, `afu_inserted`, `afu_removed`) VALUES
(1, 2, 3, 'GESARTICLE', '2013-07-20 16:53:19', NULL),
(2, 2, 3, 'ADMINRIGHT', '2013-07-21 22:03:03', NULL),
(3, 3, 3, 'POSS3', '2013-07-24 00:00:00', NULL),
(4, 2, 3, 'STATS', '2013-09-30 00:00:00', NULL),
(5, 4, 3, 'WEBSALE', '2013-12-24 13:23:00', NULL),
(6, 1, NULL, 'MYACCOUNT', '2013-12-26 18:33:14', NULL),
(7, 1, NULL, 'WEBSALECONFIRM', '2013-12-26 18:33:14', NULL),
(8, 1, NULL, 'RELOAD', '2013-12-26 18:33:42', NULL);

--
-- Contenu de la table `tj_usr_fun_ufu`
--

INSERT INTO `tj_usr_fun_ufu` (`ufu_id`, `usr_id`, `fun_id`, `ufu_service`, `ufu_inserted`, `ufu_removed`) VALUES
(2, 10505, 3, 'ADMINRIGHT', '2013-07-11 00:00:00', '2013-08-27 17:46:11'),
(6, 10505, 3, 'GESARTICLE', '2013-07-22 00:24:43', '2013-08-27 22:59:36'),
(7, 10505, 3, 'ADMINRIGHT', '2013-07-22 00:24:43', '2013-08-27 17:46:11'),
(8, 10505, 3, 'ADMINRIGHT', '2013-07-22 00:24:43', '2013-08-27 17:46:11'),
(9, 10505, 3, 'ADMINRIGHT', '2013-07-22 00:25:25', '2013-08-27 17:46:11'),
(10, 10505, 3, 'ADMINRIGHT', '2013-07-22 00:25:25', NULL),
(11, 10505, 3, 'GESARTICLE', '2013-07-22 00:50:01', '2013-08-27 22:59:36'),
(12, 10506, 3, 'POSS3', '2013-07-22 18:55:46', '2013-07-22 18:56:47'),
(13, 10505, 3, 'POSS3', '2013-07-22 23:53:28', NULL),
(14, 10506, 3, 'GESARTICLE', '2013-07-24 15:33:59', NULL),
(15, 10506, 3, 'POSS3', '2013-07-26 00:52:23', NULL),
(16, 10505, 3, 'GESARTICLE', '2013-08-27 17:51:39', '2013-08-27 22:59:36'),
(17, 10505, 3, 'GESARTICLE', '2013-08-27 22:55:31', NULL),
(18, 10505, 3, 'STATS', '2013-09-30 00:00:00', NULL);

--
-- Contenu de la table `ts_user_usr`
--

INSERT INTO `ts_user_usr` (`usr_id`, `usr_pwd`, `usr_firstname`, `usr_lastname`, `usr_nickname`, `usr_adult`, `usr_mail`, `usr_credit`, `img_id`, `usr_temporary`, `usr_fail_auth`, `usr_blocked`, `usr_msg_perso`) VALUES
(9447, '81dc9bdb52d04dc20036dbd8313ed055', 'Matthieu', 'GUFFROY', 'mguffroy', 1, 'matthieu.guffroy@etu.utc.fr', 156, NULL, 0, 0, 0, 'Hello World Ceci est un message super long ...... Plus de 54caracteres il parrait !'),
(10505, NULL, 'Florent', 'THEVENET', 'fthevene', 1, 'fthevene@etu.utc.fr', 29200, NULL, 0, 0, 0, 'Bonjour');

--
-- Contenu de la table `t_application_app`
--

INSERT INTO `t_application_app` (`app_id`, `app_url`, `app_key`, `app_name`, `app_desc`, `app_creator`, `app_lastuse`, `app_created`, `app_removed`) VALUES
(1, 'http://localhost:8080/casper/', 'f168cb7b134427016b5991516256bfd3', 'casper', '', 'fthevene', '2014-01-06 09:25:51', '2013-12-26 18:30:18', NULL),
(5, 'http://localhost:8080/scoobydoo', '505de22823d71378d5be80177dc521c3', 'scoobydoo', '', 'fthevene', '0000-00-00 00:00:00', '2013-07-20 16:01:14', NULL),
(2, 'http://localhost:8080/polar', '0ebd397e19e763170069a8ab81a83c71', 'polar-site-web', '', 'fthevene', '2014-02-14 22:19:25', '2013-07-20 16:07:58', NULL),
(3, 'http://localhost:8080/polar/caisse', '56e8481dcaf3ab41f0effef172a8d4ef', 'polar-caisse', '', 'fthevene', '2013-12-26 18:34:10', '2013-07-24 16:05:18', NULL),
(4, 'http://localhost:8080/polar/', 'b022f8cf6d6c536162b26c6474f4b638', 'polar-websale', '', 'fthevene', '2014-02-16 11:29:52', '2013-12-25 19:50:52', NULL);

--
-- Contenu de la table `t_fundation_fun`
--

INSERT INTO `t_fundation_fun` (`fun_id`, `fun_name`, `fun_removed`) VALUES
(1, 'BDE UTC', 0),
(2, 'PICASSO', 0),
(3, 'POLAR', 0);

--
-- Contenu de la table `t_message_msg`
--

INSERT INTO `t_message_msg` (`usr_id`, `fun_id`, `msg_perso`) VALUES
(NULL, NULL, 'Il y a une vie après les cours');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
