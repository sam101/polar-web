#!/usr/bin/env bash

git clone https://github.com/feuloren/logreader.git
cd logreader
npm config set registry http://registry.npmjs.org/
npm install
#nohup node main.js --http_ip=0.0.0.0 --port=1337 &
