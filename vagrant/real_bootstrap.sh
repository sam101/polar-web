 #!/usr/bin/env bash

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -q -y --force-yes vim git apache2 php5 mysql-server mysql-server-5.5 mysql-common phpmyadmin php5-curl curl nmap libsqlite3-dev rubygems nodejs npm php5-xdebug

# Config de mysql
mysql --user=root -e "UPDATE mysql.user SET password=PASSWORD('root') WHERE user='root'; FLUSH PRIVILEGES;"
mysql --user=root --password=root -e "CREATE DATABASE polar CHARACTER SET utf8;"
mysql --user=root --password=root polar < /vagrant/vagrant/data.sql
# Puis phpmyadmin
DEBCONF_DB_OVERRIDE='File{/vagrant/vagrant/config.dat}' dpkg-reconfigure -fnoninteractive phpmyadmin

# Rendre phpmyadmin accessible
ln -s /usr/share/phpmyadmin/ /var/www/phpmyadmin
# Rendre le site du Polar accessible
ln -s /vagrant /var/www/polar
ln -s /vagrant/cas /var/www/cas

# On installe composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Ginger (outils cotisant)
echo "Configuration de Ginger"
/bin/bash /vagrant/vagrant/ginger.sh
# Payutc
echo "Configuration de Payutc"
/bin/bash /vagrant/vagrant/payutc.sh
# CatchMail
echo "Installation de MailCatcher"
/bin/bash /vagrant/vagrant/catchmail.sh
# LogReader
echo "Installation de LogReader"
/bin/bash /vagrant/vagrant/logreader.sh

# XDebug
echo "Configuration de XDebug"
echo "xdebug.remote_enable = On" >> /etc/php5/apache2/conf.d/xdebug.ini
echo "xdebug.remote_connect_back = On" >> /etc/php5/apache2/conf.d/xdebug.ini
echo "xdebug.remote_log = /tmp/php5-xdebug.log" >> /etc/php5/apache2/conf.d/xdebug.ini
echo "xdebug.profiler_enable_trigger = On" >> /etc/php5/apache2/conf.d/xdebug.ini

# Options php
echo "display_errors = On" >> /etc/php5/apache2/php.ini
echo "display_startup_errors = On" >> /etc/php5/apache2/php.ini
echo "error_reporting = E_ALL" >> /etc/php5/apache2/php.ini

# Fichiers de config pour le site
cp /vagrant/vagrant/htaccess /vagrant/.htaccess
cp /vagrant/vagrant/bdd_racine.php /vagrant/inc
cp /vagrant/vagrant/conf.php /vagrant/inc

cp /vagrant/vagrant/polar /etc/apache2/sites-available
a2ensite polar

a2dissite default
a2dissite default-ssl

a2enmod rewrite
service apache2 restart
