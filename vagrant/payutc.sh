#!/usr/bin/env bash

# D'abord le serveur
git clone --recursive https://github.com/payutc/server.git
cd server
composer install

mkdir logs
chmod -R 777 .

cp /vagrant/vagrant/server/config.inc.php ./

mysql --user=root --password=root -e "CREATE DATABASE payutc CHARACTER SET utf8;"
./db.php migrations:migrate --no-interaction
mysql --user=root --password=root payutc < /vagrant/vagrant/server/data.sql

ln -s /home/vagrant/server/web /var/www/server

cd ..

# Puis casper
git clone --recursive https://github.com/payutc/casper.git
cd casper
composer install

mkdir logs
chmod 777 logs

cp /vagrant/vagrant/casper/config.inc.php ./

ln -s /home/vagrant/casper /var/www/casper

cd ..

# Puis casper
git clone --recursive https://github.com/payutc/scoobydoo.git
cd scoobydoo

mkdir logs
chmod 777 logs

cp /vagrant/vagrant/scoobydoo/config.php ./

ln -s /home/vagrant/scoobydoo /var/www/scoobydoo

