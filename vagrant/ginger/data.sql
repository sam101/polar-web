INSERT INTO `authkey` (`id`, `asso`, `details`, `cle`, `droit_ecriture`, `droit_badges`, `droit_cotisations`, `created_at`, `updated_at`) VALUES
(1, 'payutc', '', 'payutc', 0, 1, 1, '2013-08-29 00:00:00', NULL),
(2, 'polar', '', 'polar', 0, 1, 1, '2013-08-29 00:00:00', NULL);

INSERT INTO `personne` (`id`, `login`, `prenom`, `nom`, `mail`, `type`, `date_naissance`, `is_adulte`, `badge_uid`, `expiration_badge`, `created_at`, `updated_at`) VALUES
(1, 'fthevene', 'Florent', 'Thevenet', 'fthevene@etu.utc.fr', 1, '1993-11-08', 0, 'D6FACC3F', '2014-07-17', '2013-08-29 23:04:09', NULL),
(2, 'johndoe', 'John', 'Doe', 'johndoe@etu.utc.fr', 1, '1990-03-12', 1, 'ABCDEFGH', '2016-09-12', '2013-09-10 00:00:00', NULL),
(3, 'msimatic', 'Marie', 'SIMATIC', 'marie.simatic@etu.utc.fr', 1, NULL, 0, NULL, NULL, '2013-10-26 00:00:00', NULL),
(4, 'michelme', 'Mewen', 'MICHEL', 'mewen.michel@etu.utc.fr', 1, '1990-03-12', 0, NULL, NULL, '2013-10-26 00:00:00', NULL),
(5, 'salepeti', 'Samuel', 'LEPETIT', 'samuel.lepetit@etu.utc.fr', 1, '1990-03-12', 0, NULL, NULL, '2013-10-26 00:00:00', NULL),
(6, 'jennypau', 'Paul', 'JENNY', 'paul.jenny@etu.utc.fr', 1, '1990-03-12', 0, NULL, NULL, '2013-10-26 00:00:00', NULL),
(7, 'alecalve', 'Antoine', 'LE CALVEZ', 'antoine.le-calvez@etu.utc.fr', 1, '1990-03-12', 0, NULL, NULL, '2013-10-26 00:00:00', NULL),
(8, 'mariedam', 'Damien', 'MARIE', 'damien.marie@etu.utc.fr', 1, '1990-03-12', 0, NULL, NULL, '2013-10-26 00:00:00', NULL),
(9, 'gavelant', 'Antoine', 'GAVEL', 'antoine.gavel@etu.utc.fr', 1, '1990-03-12', 0, NULL, NULL, '2013-10-26 00:00:00', NULL);

INSERT INTO `cotisation` (`id`, `personne_id`, `debut`, `fin`, `montant`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2013-08-01', '2015-11-14', 16.00, '2013-08-29 00:00:00', NULL, NULL),
(2, 3, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL),
(3, 4, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL),
(4, 5, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL),
(5, 6, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL),
(6, 7, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL),
(7, 8, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL),
(8, 9, '2013-09-01', '2015-06-30', 20.00, '2013-10-26 00:00:00', '2013-10-26 00:00:00', NULL);
