<?php
// Fichier de config. ATTENTION !
if (!mysql_connect('localhost', 'root', 'root')) {
    Log::critical('Impossible de se connecter à mysql', array('erreur' => mysql_error()));
    die("<b>Site indisponible pour le moment, veuillez nous excuser du dérangement</b>");
}
mysql_select_db('polar');
mysql_query('SET NAMES UTF8');

define('DEBUG', True);

$racine = '/polar/';

try {
    $db = new PolarDB('localhost', 'polar', 'root', 'root');
} catch (PDOException $e) {
    Log::critical('Impossible de se connecter à mysql (PDO)', array('exception' => $e));
    die("<b>Site indisponible pour le moment, veuillez nous excuser du dérangement</b>");
}
PolarObject::$db = $db;
?>
