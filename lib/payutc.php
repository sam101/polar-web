<?php

class PayutcRightsError extends PolarUserError { }
require_once 'lib/json-client/jsonclient/JsonClient.class.php';

class PolarJsonClient  {
    public function __call($name, $arguments) {

    }
}

class Payutc {
    private static $services = array();

    public static function saveTicket($ticket) {
        $_SESSION['payutc_ticket'] = $ticket;
    }

    private static function hasTicket() {
        return isset($_SESSION['payutc_ticket']);
    }

    private static function getTicket() {
        return $_SESSION['payutc_ticket'];
    }

    public static function loadService($service, $app, $log_user=true) {
        global $CONF;

        if (!$CONF['use_payutc'])
            return new PolarJsonClient();

        if (!isset($CONF['payutc_'.$app.'_key']))
            throw new PolarException("Cette application ($app) n'existe pas, merci de vérifier inc/conf.php");

        // pas de client pour la requête actuelle
        if(!isset(self::$services[$service])) {

            self::$services[$service] =  new \JsonClient\AutoJsonClient($CONF['payutc_url'], $service, array(CURLOPT_CAINFO => __DIR__."/TERENA_SSL_CA.pem")); // mettre les params du proxy dans l'array vide
            // Le cookie sert à ne pas avoir à se réauthentifier à chaque requête et à chaque fois qu'on demande un nouveau service
            if (isset($_SESSION['payutc_cookie']))
                self::$services[$service]->cookie = $_SESSION['payutc_cookie'];
            self::$services[$service]->loginApp(array("key" => $CONF['payutc_'.$app.'_key']));
        }

        $client = self::$services[$service];

        $status = $client->getStatus();
        if ($log_user) {
            if (!$status->user) {
                // on s'authentifie aurpès du service
                if(!self::hasTicket()) { // auth cas, normalement l'utilisateur n'aura pas à rentrer son mot de passe;
                    Request::saveAndRedirect($client->getCasUrl() . 'login?service=' . $CONF['polar_url_for_payutc']);
                } else { // on utilise le ticket
                    try {
                        $client->loginCas(array('ticket' => self::getTicket(),
                                                'service' => $CONF['polar_url_for_payutc']));
                    } catch(\JsonClient\JsonException $e) {
                        if ($e) {// $e->code ?
                            throw new PolarUserError("Erreur d'authentification payutc, vous n'avez pas le droit d'accèder au module payutc $service ".$e);
                        }
                    }
                    $_SESSION['payutc_cookie'] = $client->cookie;
                }
            }
        }

        return $client;
    }

    /**
     * Supprime les données stockées en session
     */
    public static function reset() {
        unset($_SESSION['payutc_cookie'], $_SESSION['payutc_ticket']);
    }
}