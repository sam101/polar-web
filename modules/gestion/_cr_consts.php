<?php

$UPLOAD_TMP_DIRECTORY = $racine.'upload/tmp/';
$UPLOAD_DOCS_DIRECTORY = $racine.'upload/docs/';

$ALLOWED_EXTENSION = array('pdf');

$SIZE_LIMIT = 10 * 1024 * 1024;
$FILE_PREFIX = 'cr';
$OBSOLOTE_TMP_FILE_LIMIT = 2 * 3600;

$PREVIEW_IMAGE_SIZE = array(600, 800);


?>
