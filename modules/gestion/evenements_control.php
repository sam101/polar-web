<?php
if(isset($_GET['Editer'])){
	// Sécurisation des données entrantes
	$date = mysqlSecureText($_POST['date']);
	$heure = mysqlSecureText($_POST['heure']);
	$nom = mysqlSecureText($_POST['nom']);
	$lieu = mysqlSecureText($_POST['lieu']);
	$description = mysqlSecureText($_POST['description']);
	$event = intval($_GET['Editer']);

	if(empty($date) || !preg_match("!^[0-9]{4}-[0-9]{2}-[0-9]{2}!", $date))
		ajouterErreur("saisissez une date");
	if(empty($heure) || !preg_match("!^[0-9]{1,2}:[0-9]{2}!", $heure))
		ajouterErreur("saissisez une heure au format hh:mm");
	if(empty($nom))
		ajouterErreur("saisissez un titre");
	if(empty($lieu))
		ajouterErreur("saisissez un lieu");
	if(empty($description))
		ajouterErreur("saisissez une description");

	if(!hasErreurs()){
		if($event == 0)
			query("INSERT INTO polar_evenementiel (
				Date,
				Lieu,
				Titre,
				Description,
				Auteur,
				Creation
			) VALUES (
				'$date $heure',
				'$lieu',
				'$nom',
				'$description',
				$conid,
				NOW()
			)");
		else
			query("UPDATE polar_evenementiel SET
				Date = '$date $heure',
				Lieu = '$lieu',
				Titre = '$nom',
				Description = '$description',
				Auteur = $conid
				WHERE ID = $event
			");
		echo "<script>alert('Événement enregistré !');window.location='$racine$module/$section';</script>";
	}
	else {
		saveFormData($_POST);
		header("Location: $racine$module/$section?Editer=$event");
	}
}
elseif(isset($_GET['Supprimer'])){
	$event = intval($_GET['Supprimer']);
	query("DELETE FROM polar_evenementiel_participants WHERE Evenement = $event");
	query("DELETE FROM polar_evenementiel WHERE ID = $event");
	header("Location: $racine$module/$section");
}
