<?php

require_once('inc/file_uploader.php');
require_once('_cr_consts.php');


function testFailResult($result) {
    return !isset($result['success']) || $result['success'] != true;
}

/**
 * Classe contenant l'ensemble des actions du fichier control
 * @author Nicolas Pauss
 */
class CrControl {
    public $uploadTmpDirectory;
    public $uploadDocsDirectory;

    public $allowedExtensions;
    public $sizeLimit;
    public $filePrefixName;
    public $thumbnailSuffixName;

    public $obsoleteTmpFileLimit; //s

    //---------------------------------------------
    // Public functions
    //---------------------------------------------

    /**
     * Envoie les fichiers vers le dossier temporaire
     */
    public function uploadFileToTmpDirectory() {
        $uploader = new qqFileUploader($this->allowedExtensions, $this->sizeLimit,
            new FileNameChooserWithPrefixTimeBased($this->filePrefixName));
        return $uploader->handleUpload($this->uploadTmpDirectory);
    }


    /**
     * Supprime un fichier temporaire du dossier temporaire
     */
    public function deleteTmpFile($fileName) {
        if (empty($fileName) || !is_string($fileName)) {
            return array('error' => 'File Url is not valid.');
        }
        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->uploadTmpDirectory.$fileName;

        if (!file_exists($filePath)) {
            return array('error' => "File '$fileName' doesn't exist.");
        }

        if (!@unlink($filePath)) {
            return array('error' => "Unable to delete file '$fileName'. Check that you have the right to delete this file.");
        }

        return array('success' => true);
    }

    /**
     * Supprime les fichiers temporaires obsolètes.
     * @param array $avoidFiles Tableau de fichiers à ne pas supprimer
     */
    public function cleanObsoleteTmpWallpapers(array $avoidFiles = array()) {
        $uploadTmpDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadTmpDirectory;
        if (!is_dir($uploadTmpDirectoryPath)) return true;
        $listFiles = scandir($uploadTmpDirectoryPath);
        $obsoleteFileTime = time() - $this->obsoleteTmpFileLimit;
        foreach ($listFiles as $file) {
            if (in_array($file, $avoidFiles)) {
                continue;
            }
            if (startsWith($file, $this->filePrefixName)) {
                $filePath = $uploadTmpDirectoryPath.$file;
                if (filemtime($filePath) < $obsoleteFileTime) {
                    if(!@unlink($filePath)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    public function addNewCr($fileName, $day, $month, $year) {
        $docName = $this->getDocsFileName($year, $month, $day);
        $docPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadDocsDirectory.$docName;
        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->uploadTmpDirectory.$fileName;
        $result = $this->moveTmpToDocs($filePath, $docPath);
        if (testFailResult($result)) {
            return $result;
        }
        return array('success' => true, 'docName' => $docName);
    }


    //---------------------------------------------
    // Private functions
    //---------------------------------------------

    private function getDocsFileName($year, $month, $day) {
        $monthStr = ($month < 10) ? '0'.$month : $month;
        $dayStr = ($day < 10) ? '0'.$day : $day;
        return 'CR_'.$year.$monthStr.$dayStr.'.pdf';
    }



    private function moveTmpToDocs($tmpFilePath, $voteFilePath) {
        if(!rename($tmpFilePath, $voteFilePath)) {
            return array('error' => 'le nouveau compte rendu n\'a pu être déplacé');
        }
        return array('success' => true);
    }

}


$crControl = new CrControl();

$crControl->uploadTmpDirectory = $UPLOAD_TMP_DIRECTORY;
$crControl->uploadDocsDirectory = $UPLOAD_DOCS_DIRECTORY;

$crControl->allowedExtensions = $ALLOWED_EXTENSION;
$crControl->sizeLimit = $SIZE_LIMIT;
$crControl->filePrefixName = $FILE_PREFIX;

$crControl->obsoleteTmpFileLimit = $OBSOLOTE_TMP_FILE_LIMIT; //s

if (isset($_GET['Upload'])) {

    $crControl->cleanObsoleteTmpWallpapers();
    $result = $crControl->uploadFileToTmpDirectory();
    echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);


} else if (isset($_GET['Clean'])) {

    $fileName = @$_POST['fileName'];
    $crControl->cleanObsoleteTmpWallpapers();
    $result = $crControl->deleteTmpFile($fileName);
    echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);


} else if (isset($_GET['NewCR'])) {

    $fileName = @$_POST['fileName'];
    $day = intval($_POST['ep-day']);
    $month = intval($_POST['ep-month']);
    $year = intval($_POST['ep-year']);

    $result = $crControl->addNewCr($fileName, $day, $month, $year);

    if (testFailResult($result)) {
        ajouterErreur($result['error'].'. Veuillez renouveler l\'opération ou contacter le webmaster');
    } else {
        $_SESSION['ep-docName'] = $result['docName'];
    }

    $crControl->cleanObsoleteTmpWallpapers();

    header("Location: $racine$module/$section");
} else {
    header("Location: $racine$module/$section");
}


?>
