<?php
$titrePage = "Gestion des événements";
if(isset($_GET['Editer'])){
	use_jquery_ui();
	addFooter('
	<script>
		$(document).ready(function() {
			$("#champdate").datepicker({ dateFormat: \'yy-mm-dd\' });
		});
	</script>');
	require("inc/header.php");
	$event = intval($_GET['Editer']);
	$req = query("SELECT *, DATE(Date) AS Jour, DATE_FORMAT(Date, '%H:%i') AS Heure FROM polar_evenementiel WHERE ID = $event LIMIT 1");
	$res = mysql_fetch_assoc($req);

	$nom = getFormData('nom');
	$lieu = getFormData('lieu');
	$date = getFormData('date');
	$heure = getFormData('heure');
	$description = getFormData('description');

	if(empty($nom)) $nom = $res['Titre'];
	if(empty($lieu)) $lieu = $res['Lieu'];
	if(empty($date)) $date = $res['Jour'];
	if(empty($heure)) $heure = $res['Heure'];
	if(empty($description)) $description = $res['Description'];
	?>
	<h1>Ajouter un événement</h1>
	<p><a href="<?php echo "$racine$module/$section"; ?>" title="Retour">Retour</a></p>
	<form method="post" enctype="multipart/form-data" action="<?php echo "$racine$module/$section"; ?>_control?Editer=<?php echo $event; ?>">
	<?php afficherErreurs(); ?>
	 <table>
	   <tr>
	     <td>Nom de l'événement :</td>
	     <td><input type="text" class="in-texte" name="nom" value="<?php echo $nom; ?>" /></td>
	   </tr>
	   <tr>
	     <td>Lieu de l'événement :</td>
	     <td><input type="text" class="in-texte" name="lieu" value="<?php echo $lieu; ?>" /></td>
	   </tr>
	   <tr>
	     <td>Date :</td>
	     <td><input type="text" class="in-texte" name="date" id="champdate" value="<?php echo $date; ?>" /></td>
	   </tr>
	   <tr>
	     <td>Heure :</td>
	     <td><input type="text" class="in-texte" name="heure" value="<?php echo $heure; ?>" /> <small>Du style 18:30.</td>
	   </tr>
	   <tr>
	     <td>Description :</td>
	     <td><textarea class="in-texte" name="description" style="width:300px;height:80px;"><?php echo $description; ?></textarea></td>
	   </tr>
	   <tr>
	     <td colspan="2"><input type="submit" value="Enregistrer" class="btn" /></td>
	   </tr>
	 </table>
	</form>
	<?php
	require("inc/footer.php");
}
else {
	require("inc/header.php");
	?>
	<h1>Liste des Evenements</h1>
	<p><a href="<?php echo "$racine$module/$section"; ?>?Editer" title="Ajouter">Ajouter un événement</a></p>
	<?php
	$req = query("SELECT * FROM polar_evenementiel ORDER BY Date ASC");

	echo '<table class="datatables table table-bordered table-striped table-condensed">
	<tr>
	<th>Date</th>
	<th>Nom</th>
	<th>Lieu</th>
	<th>S</th>
	</tr>';

	while($donnees = mysql_fetch_assoc($req)){
		echo '<tr>
			<td>'.$donnees['Date'].'</td>
			<td><a href="'.$racine.$module.'/'.$section.'?Editer='.$donnees['ID'].'" title="Editer l\'événement">'.$donnees['Titre'].'</a></td>
			<td>'.$donnees['Lieu'].'</td>
			<td><a href="'.$racine.$module.'/'.$section.'_control?Supprimer='.$donnees['ID'].'" title=""><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="Supprimer" /></a></td>
			</tr>';
	}

	echo '</table>';
	require("inc/footer.php");
}
?>
