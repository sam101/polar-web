<?php
$titrePage = 'Gestion des permanences';
require_once('inc/header.php');
?>
<h1>Mise à jour des paramètres</h1>
<?php
  if(isset($_SESSION['mm-erreur'])) {
	echo '<p>
  <img src="',$racine,'styles/',$design,'/icones/exclamation.png" alt="" /> &nbsp;<span class="erreur-script">',htmlentities($_SESSION['mm-erreur']),'</span></p>';
	unset($_SESSION['mm-erreur']);
  }

  $req=query('SELECT Nom, Prenom, (SELECT Valeur FROM polar_parametres WHERE Nom LIKE \'max_perms\') AS MaxPerms,
		(SELECT Valeur FROM polar_parametres WHERE Nom LIKE \'max_permanenciers\') AS MaxPermanenciers,
		(SELECT Valeur FROM polar_parametres WHERE Nom LIKE \'modifier_perms\') AS ModifierPerms
		FROM polar_utilisateurs WHERE ID='.intval($_SESSION['con-id']));
	$param=mysql_fetch_array($req);
	$max_perms=$param['MaxPerms'];
	$modifier_perms=$param['ModifierPerms'];
	$max_permanenciers=$param['MaxPermanenciers'];
 ?>

<form id="formulaire-news" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
  <table>
	<tr>
	  <td style="font-size:12px;">Nombre maximal de permanences autorisées :</td>
	  <td><input type="text" size="2" name="mm-max" value="<?php echo $max_perms; ?>"/></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Possibilité de modifier son inscription :</td>
	  <td>
		<select name="mm-modif">
			<option value="0" <?php if($modifier_perms==0) echo 'selected="selected"'; ?>>Non</option>
			<option value="1" <?php if($modifier_perms==1) echo 'selected="selected"'; ?>>Oui</option>
		</select>
	</tr>
	<tr>
	  <td style="font-size:12px;">Nombre maximal de personnes par permanence :</td>
	  <td><input type="text" size="2" name="mm-maxpers" value="<?php echo $max_permanenciers; ?>"/></td>
	</tr>
	<tr>
	  <td></td>
	  <td><input class="btn" name="ModifierParametres" type="submit" value="Modifier les données !" /></td>
	</tr>
  </table>
</form>

<h1>Inscrire un permanencier</h1>
<p>ATTENTION : Aucun contrôle ne sera fait sur le nombre maximal de personnes sur la permanence sélectionnée !</p>
<?php
	$sql='SELECT id, nom, prenom FROM polar_utilisateurs WHERE Staff=1 ORDER BY nom asc';
	$req=query($sql);
?>
 <form id="formulaire-news" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
 <table>
	<tr>
	  <td style="font-size:12px;">Permanencier :</td>
	  <td><?php
	  echo '<select name="ep-permanencier">';
				while($data=mysql_fetch_array($req)) echo '<option value="'.$data[0].'">'.$data[1].' '.$data[2].'</option>';
			echo '</select>';
	  ?></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Jour :</td>
	  <td><select name="ep-jour">
	  <option value="0">Lundi</option>
	  <option value="1">Mardi</option>
	  <option value="2">Mercredi</option>
	  <option value="3">Jeudi</option>
	  <option value="4">Vendredi</option>
	  </select></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Créneau :</td>
	  <td><select name="ep-creneau">
	  <?php
		require_once("modules/membres/_perms.php");
		for($i=0;$i<9;$i++)
			echo '<option value="'.$i.'">'.$creneaux[$i][0].' à '.$creneaux[$i][1].'</option>';
	  ?>
	  </select></td>
	</tr>
	<tr>
	  <td></td>
	  <td><input class="btn" name="AjouterPermanence" type="submit" value="Inscrire !" /></td>
	</tr>
  </table>
 </form>

<h1>Désinscrire un permanencier</h1>
<p>ATTENTION : Aucun contrôle ne sera fait sur cette désinscription !</p>
<?php
	$sql='SELECT id, nom, prenom FROM polar_utilisateurs WHERE Staff=1 ORDER BY nom asc';
	$req=query($sql);
?>
 <form id="formulaire-news" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
 <table>
	<tr>
	  <td style="font-size:12px;">Permanencier :</td>
	  <td><?php
	  echo '<select name="ep-permanencier">';
				while($data=mysql_fetch_array($req)) echo '<option value="'.$data[0].'">'.$data[1].' '.$data[2].'</option>';
			echo '</select>';
	  ?></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Jour :</td>
	  <td><select name="ep-jour">
	  <option value="0">Lundi</option>
	  <option value="1">Mardi</option>
	  <option value="2">Mercredi</option>
	  <option value="3">Jeudi</option>
	  <option value="4">Vendredi</option>
	  </select></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Créneau :</td>
	  <td><select name="ep-creneau">
	  <?php
		for($i=0;$i<9;$i++)
			echo '<option value="'.$i.'">'.$creneaux[$i][0].' à '.$creneaux[$i][1].'</option>';
  		?>
	  </select></td>
	</tr>
	<tr>
	  <td></td>
	  <td><input class="btn" name="SupprimerPermanence" type="submit" value="Désinscrire !" /></td>
	</tr>
  </table>
 </form>
 <h1>Réinitialiser le planning</h1>
 <p>Cette fonction permet, en fin de semestre, de supprimer toutes les entrées du tableau des permanences. A utiliser avec précaution !</p>
 <input type=submit value="RAZ Perms" class="btn" onClick="<?php echo 'if(confirm(\'Voulez-vous vraiment réinitialiser le planning des permanences ?\')) location.href= \''.$racine.$module.'/'.$section.'_control?RAZ\';'; ?>"/>
<?php
	require_once('inc/footer.php');
?>


