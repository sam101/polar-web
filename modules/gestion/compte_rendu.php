<?php

require_once('inc/file_uploader.php');
require_once('_cr_consts.php');

$titrePage = 'Envoyer un compte rendu sur le site';

$prevDay = time() - (24*60*60);

$day = (getFormData('ep-day') === false) ? @date('d', $prevDay) : getFormData('ep-day');
$month = (getFormData('ep-month') === false) ? @date('m', $prevDay) : getFormData('ep-month');
$year = (getFormData('ep-year') === false) ? @date('Y', $prevDay) : getFormData('ep-year');

$additionalDiv = <<<EOT
    <div id="cr_date_div">
        <span style="font-weight:bold">Date du compte rendu :</span>
        <input type="text" size="2" id="day" name="ep-day" value="{$day}" />
        <input type="text" size="2" id="month" name="ep-month" value="{$month}" />
        <input type="text" size="4" id="year" name="ep-year" value="{$year}" />
    </div>
EOT;


$controlFile = $racine.$module.'/'.$section.'_control';

$file_uploader = new FileUploader($controlFile. '?Upload', $ALLOWED_EXTENSION);

$pdf_viewer = new PdfDocumentViewer($controlFile. '?NewCR', $file_uploader,
    $PREVIEW_IMAGE_SIZE, true, 'pdf_viewer', 'pdf_viewer', $additionalDiv);


$file_uploader->registerDocumentViewer('pdf', $pdf_viewer);

addFooter($file_uploader->getJs());
addHeaders($file_uploader->getCss());
addFooter($pdf_viewer->getJs());
addHeaders($pdf_viewer->getCss());


addHeaders(<<<EOT

    <style type="text/css">
        .qq-upload-button {
            margin: 0 auto;
        }

        #cr_send_div {
            text-align: center;
            margin: 0 auto;
            margin-top: 20px;
        }

        #pdf_viewer_submit {
            margin-top:20px;
        }

        #cr_date_div {
            margin-bottom:10px;
        }

    </style>
EOT
);

addFooter(<<<EOT

    <script type="text/javascript" src="{$racine}js/jquery.watermark.min.js"></script>
    <script type="text/javascript">

    var file_should_clean = true;

    function cleanTmpFile() {
        var controlFile = '${controlFile}?Clean';
        var tmpFileName = $('#pdf_viewer_file_name').val();
        if(!tmpFileName) {
            return {noFile:true};
        }

        var responseText = $.ajax({
            type: "POST",
            url: controlFile,
            cache: false,
            async: false,
            data: { fileName: tmpFileName },
            timeout: 5000,
        }).responseText;

        //console.log("cleanTmpFile responseText = " + responseText);
        try {
            response = eval("(" + responseText + ")");
        } catch(err){
            response = {};
        }
        return response;
    }

    $(document).ready(function(){
        $(window).bind('beforeunload', function(){
            if (file_should_clean) {
                cleanTmpFile();
            }
        });


        $('#pdf_viewer_form').submit(function() {
            file_should_clean = false;
            return true;
        });

        $('#day').watermark('JJ');
        $('#month').watermark('MM');
        $('#year').watermark('AAAA');
    });

    </script>

EOT
);


require('inc/header.php');
?>
<h1>Envoyer un compte rendu sur le site</h1>

<?php
afficherErreurs();

if (isset($_SESSION['ep-docName'])) {
    $docLink = 'http://'.$_SERVER['HTTP_HOST'].$racine.'upload/docs/'.$_SESSION['ep-docName'];
?>
    <p id="success_doc_name">
        <span style="color:green">Le document a bien été envoyé sur le serveur, il est maintenant accessible à l'adresse suivante :</span><br/>
        <a href="<?=$docLink?>"><?=$docLink?></a>
    </p>
<?php
    unset($_SESSION['ep-docName']);
} else {
?>




<div id="cr_information">
    <p>
    Vous pouvez sur cette page envoyer le compte rendu des réunions hebdomadaires.
    </p>
</div>

<div id="cr_send_div">

    <?php
    echo $file_uploader->getDiv();
    echo $pdf_viewer->getDiv();
    ?>

</div>

<?php
}
?>

<?php
require('inc/footer.php');
?>