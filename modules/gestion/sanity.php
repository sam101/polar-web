<?php
// pour le moment on fait juste un check des transactions payutc en attente
require_once 'inc/header.php';
?>

<pre><?php

$websale = Payutc::loadService('WEBSALE', 'web', false);

$paniers = PanierWeb::select()->where('Etat = "sent"');
foreach($paniers as $panier) {
    echo $panier . ' (' . $panier->TransactionID . ') (' . $panier->Mail . ') : ';
    try {
        $infos = $websale->getTransactionInfo(array('fun_id' => $CONF['payutc_fundation'],
                                                    'tra_id' => $panier->TransactionID));
    } catch (\JsonClient\JsonException $e) {
        echo 'Erreur (' . $e->getMessage() . ")\n";
        continue;
    }
    switch ($infos->status) {
    case 'V':
        // si la commande est validée chez payutc mais sent chez nous c'est mauvais signe
        echo "Validé (V)";
        echo '<a href="'.urlTo('commander', 'paiement_control', 'retour='.$panier).'" target="_blank">Retour payutc</a>';
        break;
    case 'W':
        echo "En attente (W)";
        break;
    case 'A':
        echo "Annulé (A)";
        break;
    }
    foreach($panier->getCommandes() as $commande) {
        echo ' <a href="'.urlTo('commandes', 'gestion', 'Commande='.$commande).'" target="_blank">' . $commande . '</a>';
    }
    echo "\n";
}
?></pre>

<?php
require_once 'inc/footer.php';
?>