<?php

require_once("_perms.php");

function regroupPerms($permData, $jours, $creneaux) {
    if (empty($permData)) {
        return array();
    }

    $permExploded = explode(' ', $permData);
    $isRegrouped = array_fill(0, count($permExploded), false);
    $permsRet = array();
    foreach ($permExploded as $index1 => $perm1) {
        if ($isRegrouped[$index1]) {
            continue;
        }
        $explodedPerm1 = explode(',', $perm1);
        $jourIndex1 = intval($explodedPerm1[0]);
        $creneauIndex1 = intval($explodedPerm1[1]);
        $jour = $jours[$jourIndex1];
        $debCreneau = $creneaux[$creneauIndex1][0];

        foreach($permExploded as $index2 => $perm2) {
            $explodedPerm2 = explode(',', $perm2);
            $jourIndex2 = intval($explodedPerm2[0]);
            $creneauIndex2 = intval($explodedPerm2[1]);

            if (($jourIndex1 === $jourIndex2) && (($creneauIndex1 + 1) === $creneauIndex2)) {
                $creneauIndex1 = $creneauIndex2;
                $isRegrouped[$index2] = true;
            }
        }

        $finCreneau = $creneaux[$creneauIndex1][1];

        $permsRet[] = array($jour, array($debCreneau, $finCreneau));
    }
    return $permsRet;
}


$categorie = isset($_GET['cat']) ? $_GET['cat'] : 'tous';
$membres = Utilisateur::select("Utilisateur.*, 
                GROUP_CONCAT(DISTINCT CONCAT_WS(',', Jour, Creneau) ORDER BY Jour, Creneau SEPARATOR ' ') AS Perms")
    ->leftJoin('Permanence',  'Utilisateur.ID = Permanence.Permanencier')
    ->groupBy('Utilisateur.ID')
    ->order('Utilisateur.Nom');
switch($categorie) {
	case 'permanenciers':
		$titrePage = 'Les permanenciers du Polar';
		$presentation = 'Un permanencier, ça sait tout faire... Ca relie, ça imprime, ça photocopie, ça vend et ça tient la caisse !';
        $membres->where("Bureau=0 AND Staff=1 AND Ancien=0 AND (Poste IS NULL OR Poste = '')");
	break;

	case 'responsables':
		$titrePage = 'Les responsables du Polar';
		$presentation = 'Les responsables sont ceux qui s\'occupent de faire vivre les activités du Polar et d\'enseigner aux permanenciers l\'art de polariser... et bien !';
		$membres->where("Bureau=0 AND Staff=1 AND Ancien=0 AND Poste IS NOT NULL AND Poste != ''");
	break;

	case 'bureau':
		$titrePage = ' Le bureau du Polar';
		$presentation = "Le bureau c'est l'organe vital du Polar : ils ont le pouvoir des sous !";
		$membres->where("Bureau=1 AND Staff=1 AND Ancien=0");
	break;

	case 'anciens':
		$titrePage = 'Les anciens du Polar';
		$presentation = 'Ils ont fait vivre le Polar en leur temps...';
		$membres->where("Ancien=1 AND Staff=0");
	break;

	default:
		$titrePage = "L'&eacute;quipe du Polar";
		$presentation = 'Voici l\'équipe du polar au grand complet.';
		$membres->where("Ancien=0 AND Staff=1");
	break;
}

use_jquery_ui();
addHeaders('<style type="text/css">
table.membres {
	width:100%;
	border-collapse:collapse;
	font-size:12px;
	margin-top: 2em;
}
table.membres tr td.photo {
	width:140px;
	background:#fafafa;
	border:1px solid #d0d6e7;
}
table.membres tr td.presentation {
	background:#fafafa;
	border:1px solid #d0d6e7;
	vertical-align:top;
	padding:10px 0px 0px 5px;
	text-align:left;
}
table.membres tr td.presentation div.boutons {
	float:right;
	margin-top:-7px;
	padding-right:3px;
}
table.membres tr td.perms {
	background:#fafafa;
	border:1px solid #d0d6e7;
	vertical-align:top;
	padding:10px 0px 0px 5px;
	text-align:left;
}
</style>');
addFooter('<script>
$(document).ready(function() {
	$("#dialog").dialog({ width: 700, autoOpen: false });
	$(".boutmessage").click(function(){
		$("#idmembre").val($(this).attr("utilisateur"));
		$("#messageto").html(\'<h2>Vous allez envoyer un message &agrave; \'+$(this).attr("prenomutil")+\' \'+$(this).attr("nomutil")+\'</h2>\');
		$("#idtextarea").val(\'Votre message ici...\');
		$("#dialog").dialog("open");
		})
 	});
</script>');

require('inc/header.php');

echo "<h1>$titrePage</h1>";
echo '<p>',$presentation,'<br/>Voir aussi :
<a href="',urlSection(),'" title="Les membres du Polar">l&rsquo;&eacute;quipe au grand complet</a> -
<a href="',urlSection('cat=responsables'),'" title="Les responsables du Polar">les responsables</a> -
<a href="',urlSection('cat=bureau'),'" title="Le bureau du Polar">le bureau</a> -
<a href="',urlSection('cat=anciens'), '" title="Les anciens du Polar">les anciens</a>
</p>';


foreach($membres as $membre){
    if($membre->Poste == '')
        $poste = "Permanencier";
    else
        $poste = $membre->Poste;
    echo '<table class="membres">';
    $dernierUtilisateur = $membre->get_id();
    echo '<tr><td class="photo">';

    $url = urlTo($membre->getPhoto());
    echo '<img src="'.$url.'" alt="" />';

    echo '</td>
        <td class="presentation">';

    if($user->is_logged())
        echo '<div class="boutons"><img class="boutmessage" utilisateur="'.$membre->get_id().'" nomutil="'.$membre->Nom.'" prenomutil="'.$membre->Prenom.'" src="'.urlStyle('icones/mail.png').'" alt="Envoyer un message" /></div>';


    echo '<strong>',$membre->Nom,' ',$membre->Prenom,'</strong> : <em>',$poste,'</em>
        <p><strong>';
    if($membre->Sexe == 'f')
        echo 'Elle';
    else
        echo 'Il';
    echo ' se pr&eacute;sente :</strong><br/><em>'.nl2br(entities($membre->Presentation)).'</em></p>
        </td>
        </tr>';

    // Affichage des coordonnées/permanences
    if($user->is_logged() && $user->Staff){
        echo '<tr><td colspan="2" class="perms">';


        echo '
            <strong>Mail : </strong>'.$membre->Email.'<br />
            ';

        if($isStaff && !is_null($membre->Telephone))
            echo '<strong>Portable : </strong>'.$membre->Telephone.'<br />';

        $perms = regroupPerms($membre->Perms, $jours, $creneaux);
        $isFirst = true;
        foreach ($perms as $perm) {
            if ($isFirst) {
                echo '<strong>Permanences : </strong>';
            } else {
                echo ' et ';
            }
            echo $perm[0], ' de ', $perm[1][0], ' à ', $perm[1][1];
            $isFirst = false;
        }
        echo '</td></tr>';
    }
    echo '</table>';

} // Fin de la boucle sur les membres

echo '<div id="dialog" title="Envoyer un message">
<form style="margin:0px;" method="post" enctype="multipart/form-data" action="'.$racine.$module.'/'.$section.'_control">
<input type="hidden" name="idmembre" id="idmembre" />
<span id="messageto"></span>
<textarea name="message" id="idtextarea" style="width:650px;height:300px;">Votre message ici...</textarea>
<input type="submit" name="EnvoyerMessage" value="Envoyer !" />
</form>
</div>';
require('inc/footer.php');
?>


