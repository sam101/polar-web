<?php
$titrePage = "Mes assos";
use_jquery_ui();
use_datatables();
addFooter('
	<script>
	$(document).ready(function() {
		$("#assosselect").change(function(e){$("#formassos").submit();});
$(".change-password").click(function() {
var compte = $(this).attr("data-compte-id");
$("#change-password").modal("show");
$("#pass-compte-id").val(compte);
});
$(".activer-compte").click(function() {
var compte = $(this).attr("data-compte-id");
$("#activer-compte").modal("show");
$("#reactiver-compte-id").val(compte);
});
	});
$("#btn-new-compte").click(function() {
$("#new-compte").modal("show");
});
	</script>');
require("inc/header.php");
?>
<h1>Mes assos</h1>

<form action="<?php echo urlSection() ?>" method="get" id="formassos">
<p>
Association :
	<select name="asso" id="assosselect">
<?php
// Liste des assos auquel le gars a accès
$selectAsso = 0;
$assoVoulue = !empty($_GET['asso']) ? intval($_GET['asso']) : 0;
$assos = Asso::selectActives()->where('MailPresident LIKE ? OR MailTresorier LIKE ?', $user->Email, $user->Email);
foreach($assos as $asso){
	echo '<option value="'.$asso->get_id().'"';
	// Si pas d'assos choisie (une fois) ou qu'on est sur l'assos choisie
	if($asso->get_id() == $assoVoulue || ($assoVoulue == 0 && $selectAsso == 0)){
		echo ' selected="selected"';
		$current_asso = $asso;
		$selectAsso = $asso->get_id();
	}
	echo '>'.$asso->Asso.'</option>';
}
?>
		</select>
		<input type="submit" class="btn" value="Afficher" />
	</p>
	</form>
	<?php echo afficherErreurs(); ?>
<?php
if($selectAsso > 0){
	echo $current_asso->format_from_attributes('
	<h2>{{Asso}}</h2>
    <p>Président : {{President}} ({{MailPresident}}/{{TelPresident}})</p>
	<p>Trésorier : {{Tresorier}} ({{MailTresorier}}/{{TelTresorier}})</p>
	<p>État du compte : {{Etat}}</p>');
?>
<?php
$req = query("SELECT * FROM polar_assos_factures WHERE Asso = $selectAsso AND Encaisse = 0");
if(mysql_num_rows($req) >0){
?>
	<h2>Factures à payer</h2>
	<table class="classement-donnees">
		<tr>
			<th>Date</th>
			<th>Montant</th>
			<th></th>
		</tr>
		<?php
		while($data = mysql_fetch_assoc($req)){
			$hasfactures = true;
			echo '<tr>
				<td>'.$data['Date'].'</td>
				<td>'.$data['Montant'].'</td>
				<td>';
					//<a href="'.$racine.$module.'/'.$section.'_control?EncaisserVirement='.$data['ID'].'" title="Encaisser par virement !"><img src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></a>
				echo '</td>
			</tr>';
		}
		?>
	</table>
<?
}
?>
	<h2>Achats non facturés</h2>
	<table class="datatables table table-bordered table-striped">
		<thead>
			<tr><th>Date</th><th>Article</th><th>Compte</th><th>Quantité</th><th>Prix unitaire</th><th>Montant</th></tr>
		</thead>
		<tbody>
<?php
    $achats = OldVente::select('OldVente.*, Article.Nom, Article.PrixVente, Article.PrixVenteAsso, CompteAsso.Nom as NomCompte')
    ->join('Article', 'OldVente.Article = Article.ID')
    ->join('CompteAsso', 'CompteAsso.ID = OldVente.Asso')
    ->where('Finalise=0 AND facture=0 AND CompteAsso.Asso=?', $current_asso);

    $total = 0;
    foreach($achats as $achat) {
        echo '<tr>';
        echo '<td>'.$achat->Date.'</td>';
        echo '<td>'.$achat->Nom.'</td>';
        echo '<td>'.$achat->NomCompte.'</td>';
        echo '<td>'.$achat->Quantite.'</td>';
        if($achat->Tarif == 'asso' && $achat->PrixVenteAsso != NULL)
            $prixVente = $achat->PrixVenteAsso;
        else
            $prixVente = $achat->PrixVente;
        echo '<td>'.formatPrix(round($prixVente, 2)).' €</td>';
        $montant = round($achat->PrixFacture, 2);
        echo '<td>'.formatPrix($montant).' €</td>';
        $total += $montant;
        echo '</tr>';
    }
?>
		</tbody>
	</table>

	<p style="clear:right;"><b>En-cours total : <?php echo $total; ?>€</b></p>

	<h2><a name="mdp"></a>Changer le mot de passe du compte</h2>
	<form action="<?php echo urlControl('MotPasse='.$current_asso) ?>" method="post" id="formassos">
	<label>Nouveau mot de passe : <input type="password" name="mdp" class="in-texte"/></label>
	<label>Confirmer : <input type="password" name="mdp2" class="in-texte" /></label>
	<input type="submit" value="Valider" class="btn" />
	</form>

    <button id="btn-new-compte" class="btn btn-primary" style="float:right;">Créer un sous-compte</button>
    <h2>Sous-comptes</h2>
    <table class="table table-condensed table-striped table-bordered">
      <thead>
        <tr>
          <th>Nom</th>
          <th>État</th>
          <th>Quota</th>
          <th>Activer</th>
          <th>Clôturer</th>
          <th>Modifier le Mot de Passe</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($current_asso->getComptes()->select('CompteAsso.*') as $compte): ?>
        <tr>
          <td><?php echo $compte->Nom ?></td>
          <td><?php echo $compte->DateActivation ? 'Actif' : 'Clos' ?></td>
          <td><?php echo $compte->Quota ? formatPrix($compte->Quota).' €' : 'Aucun' ?></td>
          <?php if($compte->DateActivation): ?>
          <td></td>
          <td><a href="<?php echo urlControl('cloturer='.$compte->get_id()) ?>"><i class="icon-remove"></i></a></td>
          <?php else: ?>
          <td><a data-compte-id="<?php echo $compte->get_id() ?>" class="activer-compte"><i class="icon-check"></i></a></td>
          <td></td>
          <?php endif; ?>
          <td><a data-compte-id="<?php echo $compte->get_id() ?>" class="change-password"><i class="icon-edit"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

    <!-- Changer le mot de passe d'un sous compte -->

    <div id="change-password" class="modal hide">
      <div class="modal-header"><h3>Modifier le mot de passe d'un compte</h3></div>
      <div class="modal-body">
        <form method="post" action="<?php echo urlControl() ?>" class="form-horizontal">
        <div class="control-group">
          <input type="hidden" id="pass-compte-id" name="pass-compte-id" value="" />
          <label class="control-label" for="pass-new1">Nouveau mot de passe</label>
          <div class="controls">
            <input type="password" id="pass-new1" name="pass-new1"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="pass-new2">Nouveau mot de passe</label>
            <div class="controls">
              <input type="password" id="pass-new2" name="pass-new2" placeholder="Confirmation"/>
              </div>
            </div>
          <div class="control-group">
            <label class="control-label" for="pass-old">Mot de passe asso</label>
            <div class="controls">
              <input type="password" id="pass-old" name="pass-old"/>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Enregistrer</button>
              </div>
            </div>  
        </form>
      </div>
    </div>

    <!-- Reactiver un sous-compte -->

    <div id="activer-compte" class="modal hide">
      <div class="modal-header"><h3>Activer un sous-compte</h3></div>
      <div class="modal-body">
        <form method="post" action="<?php echo urlControl() ?>" class="form-horizontal">
        <div class="control-group">
          <label class="control-label" for="quota">Quota</label>
          <div class="controls">
            <input type="text" id="quota" name="quota" placeholder="Laisser vide pour pas de quota"/>
          </div>
        </div>
        <div class="control-group">
          <input type="hidden" id="reactiver-compte-id" name="reactiver-compte-id" value="" />
          <label class="control-label" for="pass-new1">Nouveau mot de passe</label>
          <div class="controls">
            <input type="password" id="pass-new1" name="pass-new1"/>
          </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="pass-new2">Nouveau mot de passe</label>
            <div class="controls">
              <input type="password" id="pass-new2" name="pass-new2" placeholder="Confirmation"/>
              </div>
            </div>
          <div class="control-group">
            <label class="control-label" for="pass-old">Mot de passe asso</label>
            <div class="controls">
              <input type="password" id="pass-old" name="pass-old"/>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Activer</button>
              </div>
            </div>  
        </form>
      </div>
    </div>

    <!-- Nouveau sous-compte -->

    <div id="new-compte" class="modal hide">
      <div class="modal-header"><h3>Créer un compte</h3></div>
      <div class="modal-body">
        <form method="post" action="<?php echo urlControl() ?>" class="form-horizontal">
        <div class="control-group">
          <input type="hidden" id="asso-id" name="asso-id" value="<?php echo $current_asso->get_id() ?>" />
          <label class="control-label" for="nom-compte">Nom du compte</label>
          <div class="controls">
            <input type="text" id="nom-compte" name="nom-compte"/>
            </div>
          </div>
        <div class="control-group">
          <label class="control-label" for="quota">Quota</label>
          <div class="controls">
            <input type="text" id="quota" name="quota" placeholder="Laisser vide pour pas de quota"/>
            </div>
          </div>
        <div class="control-group">
          <label class="control-label" for="pass-new1">Mot de passe</label>
          <div class="controls">
            <input type="password" id="pass-new1" name="pass-new1"/>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="pass-new2">Mot de passe</label>
            <div class="controls">
              <input type="password" id="pass-new2" name="pass-new2" placeholder="Confirmation"/>
              </div>
            </div>
          <div class="control-group">
            <label class="control-label" for="pass-old">Mot de passe asso</label>
            <div class="controls">
              <input type="password" id="pass-old" name="pass-old"/>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Enregistrer</button>
              </div>
            </div>  
        </form>
      </div>
    </div>
<?php 
}
else {
	echo "<p>Vous n'êtes déclaré président ou trésorier d'aucune association.<br />Si vous pensez qu'il s'agit d'une erreur, créez un ticket dans Ma Hotline !</p>";
}
require('inc/footer.php');
?>

