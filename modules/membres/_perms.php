<?php
// Créneaux des permanences
$jours = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi');
$creneaux = array(
	array("9h", "10h15"),
	array("10h", "11h15"),
	array("11h15", "12h30"),
	array("12h15", "13h15"),
	array("13h", "14h15"),
	array("14h", "15h15"),
	array("15h15", "16h30"),
	array("16h15", "17h30"),
	array("17h30", "18h45")
);
