<?php
if(isset($_POST['ModifierMembre'])){
    try {
        $membre = Utilisateur::getById(intval($_POST['mp-id']));
    } catch (UnknowObject $e) {
        throw new PolarUserError("Le membre '".mysqlSecureText($_POST['mp-id'])."' n'existe pas");
    }

	$membre->Staff = intval($_POST['mp-staff']);
	$membre->Bureau = intval($_POST['mp-bureau']);
	$membre->Ancien = intval($_POST['mp-ancien']);
	$membre->Responsable = empty($_POST['mp-resp']) ? NULL : intval($_POST['mp-resp']);
	$membre->Poste = $_POST['mp-poste'];
    if(empty($presentation))
        $membre->Presentation = "Ce membre n'a pas encore de présentation";
    else
	$membre->Presentation = $_POST['mp-presentation'];
	$membre->Nom = $_POST['mp-nom'];
	$membre->Prenom = $_POST['mp-prenom'];

	$sexe = $_POST['mp-sexe'];
	// Vérification du sexe
	if(empty($sexe) || ($sexe != 'm' && $sexe != 'f'))
		$membre->Sexe = NULL;
	else
		$membre->Sexe = $sexe;
    $membre->save();

    // staff =  on ajoute le droit de vente payutc
    if ($membre->Staff)
        PayutcTools::addRight($membre, 'POSS3');
    else
        PayutcTools::removeRight($membre, 'POSS3');

    if ($membre->Bureau)
        PayutcTools::addRight($membre, 'STATS');
    else
        PayutcTools::addRight($membre, 'STATS');

    redirectOk();
}
else if(isset($_GET['AEU'])) {
	$autor = mysql_fetch_assoc($req);
	$id = intval($_GET['AEU']);
	$poste = getPoste($id);
	if($poste != 'Aucun') {
		require_once('inc/tcpdf.php');

		$sql = query("SELECT Prenom, Nom FROM polar_utilisateurs WHERE ID=$id");
		$membre = mysql_fetch_assoc($sql);

		$sql = query("SELECT Valeur AS President FROM polar_parametres WHERE NOM like 'prez'");
		$prez = mysql_fetch_assoc($sql);

		$pdf = new PolarPDF("ATTESTATION D'ACTIVITÉ EXTRA-UNIVERSITAIRE");

		$pdf->SetFontSize(12);
		$output = '<p align="right">Compiègne, le '.date("d/m/Y").'</p>';

		$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
		$pdf->Ln(10);


		$html = '<p>Je soussigné, '.unhtmlentities($prez['President']).', Président du Polar UTC,
		certifie que '.unhtmlentities($membre['Prenom']).' '.unhtmlentities($membre['Nom']).'
		est bien membre du Polar à ce jour, et ce depuis le début du semestre,
		en qualité de : '.unhtmlentities($poste).'.</p>
		<p>Pour rappel, les membres du Polar ont plusieurs responsabilités :</p>
		<ul>
			<li>Deux heures minimum de bénévolat par semaine</li>
			<li>Service aux étudiants : impression de rapports, posters...</li>
			<li>Gestion financière : encaissement des ventes.</li>
		</ul>
		<p>Attestation faite pour faire valoir ce que de droit.</p>';

		$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
		$pdf->Ln(10);

		$pdf->Signature("prez");

		$file = 'Documents/AEU_'.$id.'.pdf';
		$pdf->Output($file, 'F');
		header('Location: '.$racine.$file);
	}
} else if (isset($_GET['PayutcSync'])) {
    /* fait un synchronisation 'simple' vers payutc
     * les utilisateurs ont le droit POSS3
     * les membres du bureau ont les droits STATS et GESARTICLE en plus
     * on ajoute simplement les droits, pas de remove pour éviter de parcourir
     * les 3500 utilisateurs du site
     */
    $admin = Payutc::loadService('ADMINRIGHT', 'manager');

    $membres = Utilisateur::select()->where('Staff = 1 OR Bureau = 1');
    echo '<ul>';
    foreach($membres as $membre) {
        if ($membre->Login != '') { // user extérieur = pas de vente payutc...
            echo '<li>'.$membre->Login;
            if (PayutcTools::addRight($membre, 'POSS3', $admin)) {
                if ($membre->Bureau) {
                    PayutcTools::addRight($membre, 'STATS', $admin);
                    PayutcTools::addRight($membre, 'GESARTICLE', $admin);
                    echo ' (Bureau)';
                }
            } else {
                echo '- Echec !';
            }
            echo '</li>';
        }
    }
    echo '</ul>';
} else {
    throw new PolarUserError('Action invalide');
}
?>
