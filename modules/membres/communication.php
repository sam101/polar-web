<?php
addFooter('<script type="text/javascript">
$(document).ready(function(){
	$("#frm").submit(function(event){
		$("#submitter").val("Envoi en cours...").attr("disabled", "disabled");
	});
});
</script>');
require("inc/header.php");
?>
<h1>Envoi de mail</h1>
<p>Cette fonctionnalité permet l'envoi automatique de mails aux différents groupes</p>
<form method="post" action="<?php echo $racine.$module."/".$section; ?>_control" id="frm">
	<table>
		<tr>
			<td>Groupe</td>
			<td>
				<select name="ep-groupe">
					<option value="bureau">Bureau</option>
					<option value="responsables">Bureau et Responsables</option>
					<option value="staff">Staff</option>
					<option value="anciens">Anciens</option>
					<option value="tous">Tous les inscrits</option>
					<option value="assos">Assos</option>
				</select>
				<label><input type="checkbox" name="forcer" value="1" />Envoyer aussi aux membres non inscrits aux nouvelles</label>
			</td>
		</tr>
		<tr>
			<td>Sujet</td>
			<td>
				[Le Polar] <input name="ep-sujet" class="text" size="50">
			</td>
		</tr>
		<tr>
			<td>Message</td>
			<td>
				<textarea name="ep-message" rows="20" cols="75">Votre texte ici.</textarea>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input class="btn" id="submitter" type="submit" value="Envoyer !" />
			</td>
		</tr>
	</table>
</form>
<?php
require("inc/footer.php");
?>
