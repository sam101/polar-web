<?php
$titrePage = "Mes commandes";
use_jquery_ui();
use_datatables();
addFooter('
	<script>
	function infos(id, type, commande, paye, pret, retrait, uvs){
		$("#com-id").html(id);
		$("#com-type").html(type);
		$("#com-uv").html(uvs);
		$("#com-commande").html(commande);
		$("#com-paiement").html(paye);
		$("#com-pret").html(pret);
		$("#com-retrait").html(retrait);
		$("#dialog").dialog("open");
	}

	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
	});
	</script>');
require("inc/header.php");
?>
<h1>Mes commandes</h1>

<table class="datatables table table-bordered table-striped">
	<thead>
		<tr><th>ID</th><th>Type</th><th>Commande</th><th>Paiement</th><th>Prêt</th><th>Retrait</th><th>Retour</th><th>Infos</th></tr>
	</thead>
	<tbody>
<?php
$mail = mysqlSecureText($_SESSION['con-email']);
$req = query("SELECT pct.Nom AS TypeCommande, pc.ID,DATE(DateCommande),DateCommande,DATE(DatePaiement),DatePaiement,DATE(DatePrete),DatePrete,DATE(DateRetrait),DateRetrait,DATE(DateRetour),DateRetour,Detail FROM polar_commandes pc
	INNER JOIN polar_commandes_types pct ON pc.type=pct.ID
	LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
	WHERE Mail LIKE '$mail' ORDER BY pc.ID DESC");
	$lastID = 0;
	$ligne = "";
	while($data=mysql_fetch_assoc($req)){
		if($data['ID'] != $lastID){
			// Ecriture de la fin de la ligne, sauf pour la première ligne
			if($lastID != 0){
				// On vire la dernière virgule
				$ligne = substr($ligne, 0, -2);

				// Fin de la ligne
				$ligne .= '\');" style="cursor:pointer;" src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></td>';
				$ligne .= '</tr>';
			}
			// Affichage de la ligne précédente
			echo $ligne;

			// Nouvelle ligne
			$lastID = $data['ID'];
			$ligne = '<tr>';
			$ligne .= '<td>'.$data['ID'].'</td>';
			$ligne .= '<td>'.$data['TypeCommande'].'</td>';
			$ligne .= '<td>'.$data['DATE(DateCommande)'].'</td>';
			$ligne .= '<td>'.$data['DATE(DatePaiement)'].'</td>';
			$ligne .= '<td>'.$data['DATE(DatePrete)'].'</td>';
			$ligne .= '<td>'.$data['DATE(DateRetrait)'].'</td>';
			$ligne .= '<td>'.$data['DATE(DateRetour)'].'</td>';
			$ligne .= '<td><img title="Infos" onclick="infos(';
			$ligne .= "'".$data['ID']."','".$data['TypeCommande']."','".$data['DateCommande']."','".$data['DatePaiement']."','".$data['DatePrete']."','".$data['DateRetrait']."','";
		}
		$ligne .= $data['Detail'].", ";
	}
?>
	</tbody>
</table>

<div id="dialog" title="Détail de la commande">
	<table>
		<tr>
			<th>ID</th>
			<td id="com-id"></td>
		</tr>
		<tr>
			<th>Type</th>
			<td id="com-type"></td>
		</tr>
		<tr>
			<th>Contenu</th>
			<td id="com-uv"></td>
		</tr>
		<tr>
			<th>Date de commande</th>
			<td id="com-commande"></td>
		<tr>
			<th>Date de paiement</th>
			<td id="com-paiement"></td>
		</tr>
		<tr>
			<th>Date prête</th>
			<td id="com-pret"></td>
		</tr>
		<tr>
			<th>Date de retrait</th>
			<td id="com-retrait"></td>
		</tr>
	</table>
</div>
<?php
require('inc/footer.php');
?>

