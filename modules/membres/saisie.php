<?php
$titrePage = 'Ajouter un membre';

addHeaders('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />');
addFooter('
   <script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="'.$racine.'js/jquery.bgiframe.min.js"></script>
   <script type="text/javascript">
   $(document).ready(function() {
		$("#login").autocomplete("'.$racine.'utils/login_autocomplete",{
			matchContains:1,
			mustMatch:false,
			delay: 100,
			formatItem: function(row, i, max) {
				return row[0]+" - "+row[1]+" "+row[2];
			},
			formatResult: function(row) {
				return row[0];
			}
		});
   });
   </script>');
require('inc/header.php');
?>

<h1>Ajouter un membre</h1>
<p>Cette page permet de saisir rapidement la liste des membres inscrits sur les JDA. Le login sera validé et l'étudiant recevra par e-mail un mot de passe pour se connecter au site du Polar. Le mail mentionne explicitement la Journée des Associations.</p>
<?php
afficherErreurs();
if(!empty($_SESSION['jda-success'])){
	echo '<p style="color:green;">OK !</p>';
	unset($_SESSION['jda-success']);
}
?>
<form id="formulaire" method="post" action="<?php echo $racine.$module."/".$section; ?>_control">
<p>Login : <input type="text" name="login" class="autofocus" id="login" size="50" class="in-texte" />
	<input type="submit" value="Inscrire !" class="btn" />
</p>
</form>
<?php
require('inc/footer.php');
?>
