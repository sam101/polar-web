<?php
if(isset($_POST['login'])){
	$login = mysqlSecureText($_POST['login']);

  try {
      $new_user = Utilisateur::create_from_ginger($ginger, $login);
      if ($new_user->get_id() === null) {
          $new_user->save();
      }

      if (!isCotisant($login))
      $message_cotisant = "Attention : il est nécessaire d'être cotisant au BDE-UTC pour être permanencier au Polar.<br/>
Nous t'invitons à contacter le BDE-UTC pour payer ta cotisation.<br/><br/>";
      else
      $message_cotisant = "";

    query("INSERT INTO polar_jda (login, date) VALUES ('$login', NOW())");

    $message = "Bonjour " . $new_user->Prenom . ",<br /><br />
Suite à ton inscription a la journée des associations, nous t'informons que les amphis de présentation auront lieu la semaine prochaine mardi 25 et jeudi 27 (au choix !) à 18h45 en FA104. Ils seront suivis des formations au Polar puis de l'inscription aux permanences.<br /><br />
Pour simplifier la procédure, nous t'avons créé un compte sur le site du Polar.<br /><br />
Tu peux maintenant te connecter sur http://assos.utc.fr/polar avec tes identifiants UTC et profiter de nos services. Après validation lors de notre amphi de présentation, tu y retrouveras également les différents outils du permanencier.<br /><br />
$message_cotisant
N'hésite pas à répondre à ce message si tu as des questions.<br /><br />
En espérant te revoir à un des amphis, on compte sur toi !<br />
L'équipe du Polar";
    $new_user->sendMail('polar@assos.utc.fr','Le Polar', '[Le Polar] Amphis de présentation',$message);
    $_SESSION['jda-success'] = true;
  } catch (ApiException $e) {
      redirectWithErrors('', "Erreur lors de la récupération de l'utilisateur : " . $e->getMessage());
  }

  redirectOk();
}
?>
