<?php
$titrePage = "Ouverture d'un compte MyPolar";
addHeaders('
<script type="text/javascript">
$(document).ready(function(){
	$("#am-login").keyup(function(){
		$("#c-login").val($("#am-login").val());
	});
	$("#domaine").change(function(){
		if($("#domaine").val() == "autre")
			$("#domaine").replaceWith(\'<input type="text" class="in-text" id="domaine" name="domaine" value="" />\');
	});

});
</script>
');
require_once('inc/header.php');
?>
<h1>Cr&eacute;ation d'un compte MyPolar</h1>
<p>Veuillez remplir ce formulaire pour proc&eacute;der &agrave; la cr&eacute;tion d'un compte MyPolar qui vous permettra de :</p>
<ul>
<li>Commander des annales</li>
<li>Commander des posters</li>
<li>R&eacute;server le vid&eacute;oprojecteur</li>
<li>Suivre l&rsquo;ensemble de vos commandes passées sur le site du Polar !</li>
<li>Et plein d&rsquo;autre choses !</li>
</ul>
<p><b>ATTENTION : </b>Tous les champs sont obligatoires.</p>
<br />
<?php afficherErreurs(); ?>
<table>
<form method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
<?php
$loginSaisi = getFormData('am-login');
?>
<tr>
	<td>
		Login :
	</td>
	<td>
		<input type="text" class="in-text" id="am-login" class="autofocus" name="am-login" value="<?php echo $loginSaisi; ?>" />
	</td>
</tr>
<tr>
	<td>
		Courriel :
	</td>
	<td>
		<input type="text" disabled="disabled" class="in-text" id="c-login" value="<?php echo $loginSaisi; ?>" /> @
		<select name="domaine" id="domaine">
			<option value="etu.utc.fr" selected="selected">etu.utc.fr</option>
			<option value="hds.utc.fr">hds.utc.fr</option>
			<option value="escom.fr">escom.fr</option>
			<option value="autre">Autre...</option>
		</select>
		<small>L&rsquo;adresse doit &ecirc;tre une adresse UTC ou ESCOM.</small>
	</td>
</tr>
<tr>
	<td>
		Pr&eacute;nom :
	</td>
	<td>
		<input type="text" class="in-text" name="am-prenom" value="<?php echo getFormData('am-prenom'); ?>" />
	</td>
</tr>
<tr>
	<td>
		Nom :
	</td>
	<td>
		<input type="text" class="in-text" name="am-nom" value="<?php echo getFormData('am-nom'); ?>" />
	</td>
</tr>
<tr>
	<td style="font-size:12px;">
		Mot de passe :
	</td>
	<td>
		<input type="password" class="in-text" name="am-password" />
	</td>
</tr>
<tr>
	<td style="font-size:12px;">
		Confirmer :
	</td>
	<td>
		<input type="password" class="in-text" name="am-confirmer" />
	</td>
</tr>
<tr>
	<td>
	</td>
	<td>
		<small>Vous devrez valider votre adresse mail avant de pouvoir vous connecter.</small><br />
		<input class="btn" type="submit" name="CreerCompte" value="Cr&eacute;er !" />
	</td>
</tr>
</form>
</table>
<?php
require_once('inc/footer.php');
?>
