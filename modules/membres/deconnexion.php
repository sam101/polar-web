<?php
unset($_SESSION['con-pseudo']);
unset($_SESSION['con-connecte']);
unset($_SESSION['con-id']);
unset($_SESSION['con-staff']);
unset($_SESSION['con-derniereactivite']);
unset($_SESSION['con-lastPrivateActive']);
unset($_SESSION['con-method']);
Payutc::reset();
Request::$panier->nettoyer();

if ($user->is_logged_with_cas())
    header("Location: ".$CONF["cas_logout"]);
else
    header("Location: ".urlTo());
?>
