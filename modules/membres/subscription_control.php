<?php
// Formulaire entier ?
if(isset($_POST['am-prenom'], $_POST['am-nom'], $_POST['am-login'], $_POST['am-password'], $_POST['am-confirmer'])){
	// Récupération des données
	$prenom = mysqlSecureText($_POST['am-prenom']);
	$nom = mysqlSecureText($_POST['am-nom']);
	$login = mysqlSecureText($_POST['am-login']);
	$mail = mysqlSecureText($_POST['am-login']."@".$_POST['domaine']);
	$passe = md5(stripslashes($_POST['am-password']));
	$passe2 = md5(stripslashes($_POST['am-confirmer']));


	if(empty($nom))
		ajouterErreur('vous devez saisir votre nom');
	if(empty($prenom))
		ajouterErreur('vous devez saisir votre prénom');
	if(!verifMailUTC($mail))
		ajouterErreur("l'adresse mail saisie est incorrecte");
	if(!verifLoginUTC($login))
		ajouterErreur("le login \"$login\" est incorrect");
	if($passe != $passe2)
		ajouterErreur('les deux mots de passe saisis sont différents');

	$req = query("SELECT COUNT(*) AS nbr FROM polar_utilisateurs WHERE Email LIKE '$mail'");
	$donnees = mysql_fetch_assoc($req);
	if($donnees['nbr'] > 0)
		ajouterErreur('ce compte existe déjà');

	$req = query("SELECT COUNT(*) AS nbr FROM polar_utilisateurs_new WHERE Email LIKE '$mail'");
	$donnees = mysql_fetch_assoc($req);
	if($donnees['nbr'] > 0)
		ajouterErreur("ce compte est en attente d'activation");

	if(hasErreurs()){
		saveFormData($_POST);
		header("Location: $racine$module/$section");
		die();
	}
	else{
		$cle = genererAleatoire(32);
		query("INSERT INTO polar_utilisateurs_new (
			Prenom,
			Nom,
			Login,
			Email,
			Password,
			Verification,
			Creation
		) VALUES(
			'$prenom',
			'$nom',
			'$login',
			'$mail',
			'$passe',
			'$cle',
			NOW()
		)");

		$message = '
		<h1>Bienvenue sur MyPolar !</h1>
		<p>Bonjour '.$prenom.',</p>
		<p>Vous avez r&eacute;cemment cr&eacute;&eacute; un compte sur le site du Polar. Merci de
		<a href="http://assos.utc.fr/polar/membres/subscription_control?ActiverCompte='.$cle.'">cliquer ici</a> pour activer votre compte.
		<p>Nous vous rappelons vos informations de connexions :</p>
		<p>Email : '.$mail.'<br />Mot de passe : <i>Vous seul(e) le connaissez !</i></p>
		<p>Votre mot de passe est crypt&eacute; dans notre base de donn&eacute;. En cas de perte, merci d\'utiliser le formulaire disponible sur le site du Polar.</p>
		<p>En cas de probl&egrave;me de connexion, vous pouvez contacter polar@assos.utc.fr<br />
		En cas de probl&egrave;me concernant l\'utilisation des services, merci d\'utiliser "Ma hotline", pr&eacute;sente sur le site</p>
		<p>L\'&eacute;quipe du Polar</p>
		';
		sendMail('polar@assos.utc.fr','Le Polar',array($mail),'[Le Polar] Activation de votre compte MyPolar',$message);
		echo '<script>
			alert("Votre demande d\'ouverture de compte est prise en compte. Un mail de d\'activation vous a été envoyé.");
			document.location = \''.$racine.'\';
			</script>';
	}
  }
  if(isset($_GET['ActiverCompte'])){
	$cle = mysqlSecureText($_GET['ActiverCompte']);
	$req = query("SELECT * FROM polar_utilisateurs_new WHERE Verification LIKE '$cle'");
	if(mysql_num_rows($req) > 0){
		$data = mysql_fetch_assoc($req);
		$ip = mysqlSecureText($_SERVER['REMOTE_ADDR']);
		$passe = mysqlSecureText($data['Password']);
		$email = mysqlSecureText($data['Email']);
		$nom = mysqlSecureText($data['Nom']);
		$prenom = mysqlSecureText($data['Prenom']);
		$login = mysqlSecureText($data['Login']);
		query("INSERT INTO polar_utilisateurs (
			IPClient,
			DateCreation,
			MotDePasse,
			Login,
			Email,
			Staff,
			Bureau,
			Ancien,
			Presentation,
			Nom,
			Prenom,
			Sexe
		) VALUES(
			'$ip',
			NOW(),
			'$passe',
			'$login',
			'$email',
			0,
			0,
			0,
			'Ce membre n&rsquo;a pas encore de pr&eacute;sentation.',
			'$nom',
			'$prenom',
			'm'
		)");
		$message = '
			<p>Bonjour '.stripslashes($prenom).',</p>
			<h1>Votre compte MyPolar a &eacute;t&eacute; activ&eacute; avec succ&egrave;s !</h1>
			<p>Nous vous rappelons vos informations de connexions :</p>
			<p>Email : '.$email.'<br />Mot de passe : <i>Vous seul(e) le connaissez !</i></p>
			<p>Votre mot de passe est crypt&eacute; dans notre base de donn&eacute;. En cas de perte, merci d\'utiliser le formulaire disponible sur le site du Polar.</p>
			<p>En cas de probl&egrave;me de connexion, vous pouvez contacter polar@assos.utc.fr<br />
			En cas de probl&egrave;me concernant l\'utilisation des services, merci d\'utiliser "Ma hotline", pr&eacute;sente sur le site</p>
			<p>L\'&eacute;quipe du Polar</p>
		';
		sendMail('polar@assos.utc.fr','Le Polar',array($data['Email']),'[Le Polar] Bienvenue sur MyPolar',$message);
		query("DELETE FROM polar_utilisateurs_new WHERE Verification LIKE '$cle'");
		echo '<script>
			alert(\'Votre compte a été activé avec succès, vous pouvez vous connecter dès à présent. Un mail de confirmation vous a été envoyé.\');
			document.location = \''.$racine.'\';
			</script>';
	}
	else{
		die("Impossible de reconnaitre votre cle.");
	}
  }
?>
