<?php
$titrePage = 'Modification du profil';
addHeaders('
<link href="'.$racine.'lib/valums-file-uploader/fileuploader.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="'.$racine.'lib/Jcrop/jquery.Jcrop.css" type="text/css" />');
addFooter('
<script src="'.$racine.'lib/valums-file-uploader/fileuploader.js" type="text/javascript"></script>
<script src="'.$racine.'lib/Jcrop/jquery.Jcrop.min.js" type="text/javascript"></script>
<script>
function updateCoords(c){
	$("#x1").val(c.x);
	$("#y1").val(c.y);
	$("#w").val(c.w);
	$("#h").val(c.h);
};
$(document).ready(function() {
	if($("#file").val() != ""){
		$("#avatarchange").attr("src", '.$racine.' + $("#file").val()).Jcrop({
	    	aspectRatio: 1,
			onSelect: updateCoords,
			setSelect: [$("#x1").val(), $("#y1").val(), $("#x1").val()+$("#w").val(), $("#y2").val()+$("#h").val()],
	    });
		updateCoords();
	}
	else {
		var uploader = new qq.FileUploader({
			element: document.getElementById("file-uploader"),
			action: "'.$racine.$module.'/'.$section.'_control?Upload",
			allowedExtensions: ["jpg", "jpeg", "png"],
			multiple: false,
			sizeLimit: 2 * 1024 * 1024,
			onComplete: function(id, fileName, responseJSON){
				if(responseJSON.success){
					$("#avatarchange").attr("src", '.$racine.' + responseJSON.url).Jcrop({
				    	aspectRatio: 1,
						onSelect: updateCoords,
						setSelect: [50, 50, 150, 150],
				    });
					$("#file").val(responseJSON.url);
					$("#fileid").val(responseJSON.fileid);
					$("#file-uploader").hide();
					updateCoords({x: 50, y: 50, w:100, h:100});
				}
			},
			messages: {
				typeError: "{file} n\'a pas une extension valide, seuls {extensions} sont autorisés.",
	            sizeError: "{file} est trop grand : maximum {sizeLimit}.",
	            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
	            emptyError: "{file} est vide.",
	            onLeave: "Chargement des fichiers en cours. Quitter la page maintenant annulera le chargement."
			},
			template: \'<div class="qq-uploader"><div class="qq-upload-drop-area"><span>Glisser-déposer une image</span></div><div class="qq-upload-button">Choisir une image</div><ul class="qq-upload-list"></ul><div>\',
		});
	}

    $("#regenMdp").click(function() {
        $.ajax({
				url: "'.$racine.$module.'/'.$section.'_control?RegenMdp",
                async: false,
                dataType: "json",
				success: function(data){
					if(data.ok) {
                        alert("Votre nouveau mot de passe a été envoyé par email.");
					} else {
                        alert("Erreur lors de la régénération du mot de passe.");
                    }
				}
			});
        return false;
    });

});
</script>
');
require_once('inc/header.php');

$presentation = stripslashes(getFormData('mp-presentation'));
$tel = getFormData('mp-tel');
$sexe = getFormData('mm-sexe');
$newsletter = getFormData('newsletter');

if(empty($sexe)) $sexe = $user->Sexe;
if(empty($presentation)) $presentation = $user->Presentation;
if(empty($tel)) $tel = $user->Telephone;
if(empty($newsletter)) $newsletter = $user->Newsletter;
?>
<h1>Modification de ton profil</h1>
<?php afficherErreurs(); ?>

<form method="post" id="formulaire-news" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control?ModifProfil'; ?>">
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label" for="avartarchange">Photo</label>
	  		<div class="controls">
				<div id="file-uploader">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
					</noscript>
				</div>
				<img id="avatarchange" src="<?php echo photoURL(); ?>" alt="" /><br />
				<input type="hidden" name="x1" id="x1" value="<?php echo getFormData('x1'); ?>" />
				<input type="hidden" name="y1" id="y1" value="<?php echo getFormData('y1'); ?>" />
				<input type="hidden" name="w" id="w" value="<?php echo getFormData('w'); ?>" />
				<input type="hidden" name="h" id="h" value="<?php echo getFormData('h'); ?>" />
				<input type="hidden" name="file" id="file" value="<?php echo getFormData('file'); ?>" />
				<input type="hidden" name="fileid" id="fileid" value="<?php echo getFormData('fileid'); ?>" />

				<a href="<?php echo $racine.$module.'/'.$section.'_control'; ?>?SupprimerPhoto" title="Supprimer ma photo">Supprimer ma photo</a><br />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="mm-sexe">Sexe</label>
			<div class="controls">
				<select name="mm-sexe" id="mm-sexe">
					<option value="">Indéfini</option>
					<option value="f" <?php if($sexe=='f') echo 'selected="selected" ';?>>Féminin</option>
					<option value="m" <?php if($sexe=='m') echo 'selected="selected" ';?>>Masculin</option>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="mp-tel">Portable</label>
			<div class="controls">
				<input type="text" id="mp-tel" name="mp-tel" class="in-texte" value="<?php echo $tel; ?>" />
				<small>10 chiffres sans points/espaces. Il ne sera visible que par le staff du Polar.</small>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="mp-presentation">Présentation</label>
			<div class="controls">
				<textarea id="mp-presentation" name="mp-presentation"><?php echo $presentation; ?></textarea>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
			<label class="checkbox" for="newsletter">
				<input id="newsletter" type="checkbox" name="newsletter" value="1" <?php if($newsletter==1) echo 'checked="checked" ';?>/> Recevoir les nouvelles
            </label><span class="help-block">Note : les permanenciers recoivent toujours les infos les plus importantes.</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="mdp-new">Nouveau mot de passe</label>
			<div class="controls">
				<input type="password" name="mdp-new" class="in-texte" id="mdp-new"/>
			</div>
		</div>		<div class="control-group">
			<label class="control-label" for="mdp-new2">Nouveau mot de passe</label>
			<div class="controls">
				<input type="password" name="mdp-new2" class="in-texte" id="mdp-new2"/> <span class="help-inline">Confirmation</span>
			</div>
		</div>
<?php if($user->is_logged_with_password()): ?>
		<div class="control-group success">
			<label class="control-label" for="mp-mdp">Mot de passe actuel</label>
			<div class="controls">
				<input type="password" name="mp-mdp" class="in-texte" id="mp-mdp"/>
			</div>
		</div>
<?php endif; ?>
		<div class="control-group">
			<div class="controls">
				<input type="submit" value="Modifier !" class="btn btn-primary"/>
			</div>
		</div>
	</div>
</form>
<?php
require_once('inc/footer.php');
?>
