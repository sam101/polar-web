<?php
if(isset($_SESSION['captcha'], $_POST['ReinitialiserMdp'])) {
	$email = mysqlSecureText($_POST['nm-email']);

	// Vérification du Captcha
	if($_SESSION['captcha'] != $_POST['nm-captcha'])
	 	ajouterErreur("Code de vérification erroné");
	unset($_SESSION['captcha']);

	$req = query("SELECT COUNT(*) AS nbr FROM polar_utilisateurs
		WHERE Email LIKE '$email'");
	$data = mysql_fetch_assoc($req);
	if($data['nbr'] != 1)
	 	ajouterErreur('Compte inexistant ou pas encore activé');


	if(hasErreurs()){
		saveFormData($_POST);
		header("Location: $racine$module/$section");
	}
	else {
		// On réinitialise le mot de passe
		$mdpp = genererAleatoire(8);
		$newmdp = md5($mdpp);
		query("UPDATE polar_utilisateurs SET MotDePasse = '$newmdp', MotDePasseSecurise=0 WHERE Email LIKE '$email'");
		$message = "Bonjour,<br />
Vous avez demandé la réinitialisation de votre mot de passe sur le site du Polar.<br />
Voici vos informations de connexions :<br />
Email : {$_POST['nm-email']}<br />
Mot de passe : $mdpp<br />
N'oubliez pas que vous pouvez modifier dans votre profil sur le site !<br />
En cas de problème de connexion, contactez polar@assos.utc.fr<br />
Cordialement,<br />
L'équipe du Polar";
		sendMail('polar@assos.utc.fr', 'Le Polar', array($_POST['nm-email']), '[Le Polar] Réinitialisation de votre mot de passe MyPolar', $message);
		echo "<script>
alert('Votre nouveau mot de passe vous a été envoyé par mail.');
document.location = '$racine';
</script>";
	}
}
?>
