<?php
$titrePage = "&Eacute;v&eacute;nements";
require("inc/header.php");
?>
<h1>Participer aux activités événementielles du Polar</h1>
<p>Le Polar, ce n'est pas que de l'activité de vente, c'est aussi des soirées au Pic, des actions ponctuelles dans Le Fil et bien d'autres encore !</p>
<p></p>
<?php
$req = query("SELECT pe.*, COUNT(Participant) AS NbParticipants FROM polar_evenementiel pe
	LEFT JOIN polar_evenementiel_participants pep ON pep.Evenement = pe.ID
	GROUP BY pe.ID
	ORDER BY pe.Date ASC");

while($donnees = mysql_fetch_assoc($req)){
	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<tr><th colspan="2"><strong>',$donnees['Titre'],'</strong></th></tr>';
	echo '<tr>
		<td></td>
		<td>';

	echo '<br />
	<p><strong>Date : </strong>',str_replace(" ", " &agrave; ", $donnees['Date']),'</p>
	<p><strong>Lieu : </strong>',$donnees['Lieu'],'</p>
	<p><br />',nl2br($donnees['Description']),'<br /></p>
	<p><em>',$donnees['NbParticipants'],' participants</em></p>';

	echo '
		</td></tr>
		<tr>
			<th colspan="2">
			Ils y participent
		</th></tr>';
	$idEvent = $donnees['ID'];
	$req2=query("SELECT Prenom, Nom, Participant FROM polar_utilisateurs pu
		INNER JOIN polar_evenementiel_participants pep ON pu.ID=pep.Participant
		INNER JOIN polar_evenementiel pe ON pep.Evenement=pe.id
		WHERE pe.ID=$idEvent
		ORDER BY Nom ASC");
	$present = false;
	while($participants = mysql_fetch_assoc($req2)){
		echo '<tr><td colspan="2">';
		echo $participants['Prenom'].' '.$participants['Nom'];
		if($participants['Participant'] == $_SESSION['con-id']){
			echo '<a href="'.$racine.$module.'/'.$section.'_control?Desinscription='.$idEvent.'" title="Me désinscrire !"><img src="'.$racine.'styles/'.$design,'/icones/croix.png" alt="-" /></a>';
			$present = true;
		}
		echo '</td></tr>';
	}
	if($present == false)
		echo '<tr><td colspan="2">
				<a href="'.$racine.$module.'/'.$section.'_control?Inscription='.$idEvent.'">Je participe !</a>
			</td></tr></p>';

	echo '</table>';
}

require("inc/footer.php");
?>
