<?php
require("_perms.php");
$titrePage = 'G&eacute;rer mes permanences';
use_jquery_ui();
addFooter('
<script>
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
		$(".mail").click(function(){
			$("#idmembre").val($(this).attr("util"));
			$("#messageto").html(\'<h2>Vous allez envoyer un message &agrave; \'+$(this).attr("prenomutil")+\' \'+$(this).attr("nomutil")+\'</h2>\');
			$("#idtextarea").val(\'Votre message ici...\');
			$("#dialog").dialog("open");
			return false;
		})
	});');
addHeaders('
</script>
<style type="text/css">
.permanences {
  width:714px;
  margin-top:10px;
  border-collapse:collapse;
  font-size:12px;
}

.permanences th {
  padding:2px;
  border: thin solid #6495ed;
  text-align:center;
  background-color: #D0E3FA;
  text-align:center;
}

.permanences td {
  font-family: sans-serif;
  border: thin solid #6495ed;
  padding: 5px;
  text-align: center;
  background-color: #ffffff;
}
</style>');
require_once('inc/header.php');
?>
<h1>Troc de permanences</h1>
<p><?php afficherErreurs(); ?></p>
<?php
//Récupération des paramètres
if(empty($_GET['semaine']))
	$semaine = strftime("%G-%V");
else
	$semaine = mysqlSecureText($_GET['semaine']);

$week = substr($semaine, 5, 2);
$year = substr($semaine, 0, 4);

if($semaine != strftime("%G-%V"))
	echo '<a href="'.$racine.$module.'/'.$section.'?semaine='.strftime("%G-%V").'" title="Semaine précédente">';
echo 'Cette semaine';
if($semaine != strftime("%G-%V"))
	echo '</a>';
echo ' - ';

if($semaine != strftime("%G-%V", time() + 7*24*3600))
	echo '<a href="'.$racine.$module.'/'.$section.'?semaine='.strftime("%G-%V", time() + 7*24*3600).'" title="Semaine suivante">';
echo 'La semaine prochaine';
if($semaine != strftime("%G-%V", time() + 7*24*3600))
	echo '</a>';

// On va construire le tableau des permanences
// tableau à 40 colonnes : 5*9h d'ouverture
for($i=0;$i<9;$i++){
	for($j=0;$j<5;$j++){
		$tab[$j][$i] = '';
		$mesperms[$j][$i] = '';
	}
}

$req = query("SELECT pp.Permanencier, ppt.Donneur, ppt.Receveur, pu.Prenom, pu.Nom, pu2.Prenom AS ReceveurPrenom, pu2.Nom AS ReceveurNom, pp.Jour, pp.Creneau FROM polar_perms pp
	INNER JOIN polar_utilisateurs pu ON pu.ID = pp.Permanencier
	LEFT JOIN polar_perms_troc ppt ON ppt.Jour = pp.Jour AND ppt.Creneau = pp.Creneau AND ppt.Donneur = pp.Permanencier AND ppt.Semaine LIKE '$semaine'
	LEFT JOIN polar_utilisateurs pu2 ON pu2.ID = ppt.Receveur");
while($data = mysql_fetch_array($req)){
	// On remplit les cellules du tableau
	$case = '<a href="#" class="mail" util="'.$data['Permanencier'].'" prenomutil="'.$data['Prenom'].'" nomutil="'.$data['Nom'].'">'.$data['Prenom'].' '.$data['Nom'].'</a>';

	// S'il y a un donneur, il est possible de prendre cette perm
	if(!empty($data['Donneur'])){
		$case = '<s>'.$case.'</s>';

		// S'il y a un receveur, on affiche son nom
		if(!empty($data['Receveur'])){
			$case .= '<br />'.'<a href="#" class="mail" util="'.$data['Receveur'].'" prenomutil="'.$data['ReceveurPrenom'].'" nomutil="'.$data['ReceveurNom'].'">'.$data['ReceveurPrenom'].' '.$data['ReceveurNom'].'</a>';
			// Si je suis le repreneur, je peux laisser tomber
			if($data['Receveur'] == $conid)
				$case .= ' <a href="'.$racine.$module.'/'.$section.'_control?Relacher&jour='.$data['Jour'].'&creneau='.$data['Creneau'].'&semaine='.$semaine.'" title="Libérer cette perm"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="x" /></a>';;
		}
		// Pas de receveur
		else {
			// Si c'est ma perm, je peux la reprendre
			if($data['Permanencier'] == $conid)
				$case .= ' <a href="'.$racine.$module.'/'.$section.'_control?Reprendre&jour='.$data['Jour'].'&creneau='.$data['Creneau'].'&semaine='.$semaine.'" title="Reprendre ma perm"><img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="+" /></a>';
			// Sinon, je peux me porter volontaire
			else
				$case .= ' <a href="'.$racine.$module.'/'.$section.'_control?Prendre&jour='.$data['Jour'].'&creneau='.$data['Creneau'].'&semaine='.$semaine.'" title="Prendre cette perm"><img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="+" /></a>';
		}
	}
	// Si c'est une perm à moi, je peux la rendre disponible
	elseif($data['Permanencier'] == $conid)
		$case .= ' <a href="'.$racine.$module.'/'.$section.'_control?Lacher&jour='.$data['Jour'].'&creneau='.$data['Creneau'].'&semaine='.$semaine.'" title="Libérer ma perm"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="x" /></a>';

	$case .= '<br />';

	$tab[$data['Jour']][$data['Creneau']] .= $case;
}
echo '<table class="permanences">';
echo '<tr>
	<th></th>
	<th>Lundi '.date('j', strtotime("{$year}W{$week}")).' *</th>
	<th>Mardi '.date('j', strtotime("{$year}W{$week}") + 24*3600).'</th>
	<th>Mercredi '.date('j', strtotime("{$year}W{$week}") + 2*24*3600).'</th>
	<th>Jeudi '.date('j', strtotime("{$year}W{$week}") + 3*24*3600).'</th>
	<th>Vendredi '.date('j', strtotime("{$year}W{$week}") + 4*24*3600).'</th>
</tr>';
for($i=0;$i<9;$i++){
	// $creneaux est dans _perms.php
	echo "<tr><th>{$creneaux[$i][0]} à {$creneaux[$i][1]}</th>";
	for($j=0;$j<5;$j++){
		echo '<td>';
		echo $tab[$j][$i];

		echo '</td>';
	}
	echo '</tr>';
}
echo '</table>';
echo '<p><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="x" /> : libérer une permanence<br />
<img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="x" /> : prendre une permanence</p>';
echo '<p>* : le lundi matin, les créneaux sont décalés de 15 minutes pour s\'adapter aux horaires de cours.</p>';
echo '<div id="dialog" title="Envoyer un message">
	<form style="margin:0px;" method="post" enctype="multipart/form-data" action="'.$racine.$module.'/'.$section.'_control">
	<input type="hidden" name="idmembre" id="idmembre" />
	<span id="messageto"></span>
	<textarea name="message" id="idtextarea" style="width:650px;height:300px;">Votre message ici...</textarea>
	<input type="submit" name="EnvoyerMessage" value="Envoyer !" />
	</form>
	</div>';
require_once('inc/footer.php');
?>
