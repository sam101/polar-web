<?php
$admin = Payutc::loadService('ADMINRIGHT', 'manager');

$titrePage = 'Gérer les membres';
use_datatables();
addFooter('
	<script>
	$(document).ready(function() {
		$("#sResp").change(function(){
			$("#poste").val("Responsable "+$("#sResp :selected").html());
		});
	} );
	</script>');

require('inc/header.php');

echo "<h1>$titrePage</h1>";

if(isset($_GET['ModifierMembre'])){
    $id = intval($_GET['ModifierMembre']);

    $req = query("SELECT * FROM polar_utilisateurs WHERE ID=$id");
    $donnees = mysql_fetch_assoc($req);

	if(isCotisant($donnees['Login']))
		$disabled = "";
	else {
		$disabled = 'disabled="disabled"';
		$donnees['Bureau'] = 0;
		$donnees['Staff'] = 0;
		echo '<p style="color:red;"><b>Non cotisant au BDE.</b></p>';
	}

	?>
	<form id="formulaire-news" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
	  <table>
		<tr>
		  <td>Login : </td>
		  <td>
			<?php echo $donnees['Login']; ?>
			<input type="hidden" name="mp-id" value="<?php echo $donnees['ID']; ?>" />
		  </td>
		</tr>
		<tr>
		  <td>Courriel : </td>
		  <td>
			<?php echo $donnees['Email']; ?>
		  </td>
		</tr>
		<tr>
		  <td>Nom : </td>
		  <td>
			<input type="text" class="in-texte" name="mp-nom" <?php echo 'value="',$donnees['Nom'],'" '; ?>/>
		  </td>
		</tr>
		<tr>
		  <td>Prénom : </td>
		  <td>
			<input type="text" class="in-texte" name="mp-prenom" <?php echo 'value="',$donnees['Prenom'],'" '; ?>/>
		  </td>
		</tr>
		<tr>
		  <td>Sexe : </td>
		  <td>
			<select name="mp-sexe">
				<option value="">Indéfini</option>
				<option value="f" <?php if($donnees['Sexe']=='f') echo 'selected="selected" ';?>>Féminin</option>
				<option value="m" <?php if($donnees['Sexe']=='m') echo 'selected="selected" ';?>>Masculin</option>
			</select>

		  </td>
		</tr>
		<tr>
		  <td>Accès au secteur : </td>
		  <td>
			<select id="sResp" name="mp-resp">
				<option value="0">---</option>
				<?php
				$req = query("SELECT Code, Secteur FROM polar_caisse_secteurs");
				while($secteur = mysql_fetch_assoc($req)){
					echo '<option value="'.$secteur['Code'].'"';
					if($donnees['Responsable'] == $secteur['Code'])
						echo ' selected="selected"';
					echo '>'.$secteur['Secteur'].'</option>';
				}

				?>
			</select>
			<small>Si le membre a des droits sur les articles, il aura accès à ce secteur seulement.</small>
		  </td>
		</tr>
		<tr>
		  <td>Poste : </td>
		  <td>
			<input type="text" class="in-texte" name="mp-poste" id ="poste" value="<?php echo $donnees['Poste']; ?>" /><br />
			<small>Inscrire quelque chose ici pour une personne qui n'est pas dans le bureau lui donne le statut de Responsable.</small><br />
			<?php
			if(getPoste($donnees['ID']) != 'Aucun')
				echo '<a href="'.$racine.$module.'/'.$section.'_control?AEU='.$donnees['ID'].'" title="Modifier">Obtenir une attestation d\'activité extra-universitaire</a>';
			?>
		  </td>
		</tr>
		<tr>
		  <td>Présentation : </td>
		  <td>
			<textarea name="mp-presentation" class="in-texte"><?php echo $donnees['Presentation']; ?></textarea>
		  </td>
		</tr>
		<tr>
		  <td>Staff : </td>
		  <td>
			<select name="mp-staff"<?php echo $disabled; ?>>
				<option value="1" <?php if($donnees['Staff']==1) echo 'selected="selected" ';?>>Oui</option>
				<option value="0" <?php if($donnees['Staff']==0) echo 'selected="selected" ';?>>Non</option>
			</select>
		  </td>
		</tr>
		<tr>
		  <td>Bureau : </td>
		  <td>
			<select name="mp-bureau"<?php echo $disabled; ?>>
				<option value="1" <?php if($donnees['Bureau']==1) echo 'selected="selected" ';?>>Oui</option>
				<option value="0" <?php if($donnees['Bureau']==0) echo 'selected="selected" ';?>>Non</option>
			</select>
		  </td>
		</tr>
		<tr>
		  <td>Ancien : </td>
		  <td>
			<select name="mp-ancien">
				<option value="1" <?php if($donnees['Ancien']==1) echo 'selected="selected" ';?>>Oui</option>
				<option value="0" <?php if($donnees['Ancien']==0) echo 'selected="selected" ';?>>Non</option>
			</select>
		  </td>
		</tr>
		<tr>
		  <td></td>
		  <td>
			<input type="submit" class="btn" name="ModifierMembre" value="Modifier !" />
		  </td>
		</tr>
	  </table>
	</form>
	<?php
}
else {
    afficherErreurs();
	$req = query("SELECT Login, Nom, Prenom, ID,
		(CASE Staff WHEN 1 THEN 'OUI' ELSE 'NON' END) AS IsStaff,
		(CASE Bureau WHEN 1 THEN 'OUI' ELSE 'NON' END) AS IsBureau,
		(CASE Ancien WHEN 1 THEN 'OUI' ELSE 'NON' END) AS IsAncien
		FROM polar_utilisateurs
		ORDER BY Login ASC
	");
	echo '<table class="datatables table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>Login</th>
				<th>Prénom</th>
				<th>Nom</th>
				<th>Staff ?</th>
				<th>Bureau ?</th>
				<th>Ancien ?</th>
				<th>Modifier</th>
			</tr>
		</thead>
		<tbody>';
	while($data = mysql_fetch_assoc($req))
		echo '<tr>
				<td>'.$data['Login'].'</td><td>'.$data['Prenom'].'</td>
				<td>'.$data['Nom'].'</td>
				<td>'.$data['IsStaff'].'</td><td>'.$data['IsBureau'].'</td><td>'.$data['IsAncien'].'</td>
				<td><a href="'.$racine.$module.'/'.$section.'?ModifierMembre='.$data['ID'].'" title="Modifier"><img src="'.$racine.'styles/0/icones/ajouter.png" alt="-" /></a></td>
			</tr>';

	echo '</tbody>
	</table>';
}
require('inc/footer.php');
?>
