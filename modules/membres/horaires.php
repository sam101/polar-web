<?php

//Récupération des paramètres

//On va construire le tableau des permanences
//tableau à 40 colonnes : 5*8h d'ouverture
require("_perms.php");

$req=query("SELECT polar_utilisateurs.id AS IDUser, Prenom, Nom, Jour, Creneau FROM polar_perms INNER JOIN polar_utilisateurs ON polar_perms.Permanencier=polar_utilisateurs.id");
$compteperms = array_fill(0, 5, array_fill(0, 9, 0));
while($data=mysql_fetch_array($req)){
	//Compte des inscriptions
	$compteperms[$data['Jour']][$data['Creneau']]++;
}

  $titrePage = 'Horaires d&rsquo;ouverture';
  addHeaders('<style type="text/css">
thead th {
    text-align:center !important;
    background-color:#f9f9f9;
}

.rougee {
	background-color:#f2dede !important;
    color:#b94a48;
    text-shadow:0 1px 0 rgba(255,255,255,0.5);
    text-align:center !important;
}
.orange {
	background-color:#fcf8e3 !important;
    color:#c09853;
    text-shadow:0 1px 0 rgba(255,255,255,0.5);
    text-align:center !important;
}
.vert {
	background-color:#dff0d8 !important;
    color:#468847;
    text-shadow:0 1px 0 rgba(255,255,255,0.5);
    text-align:center !important;
}
</style>');
  require_once('inc/header.php');
?>
<h1>Ouverture du Polar</h1>
<p>Ce tableau indique le nombre de permanenciers inscrits à une permanence sur chacun des créneaux constituant la semaine. Il ne s'agit pas d'horaires d'ouverture, mais cela en donne une bonne indication...</p>

<?php
	echo '<table class="table table-bordered table-striped">';
	echo '<thead><tr><th></th><th>Lundi *</th><th>Mardi</th><th>Mercredi</th><th>Jeudi</th><th>Vendredi</th></tr></thead><tbody>';
	for($i=0;$i<9;$i++)
	{
		$heure=$creneaux[$i][0];
		$heure2=$creneaux[$i][1];
		echo '<tr><th>'.$heure.' à '.$heure2.'</th>';
		for($j=0;$j<5;$j++)
		{
			if($compteperms[$j][$i] == 0)
				echo '<td class="rougee">0';
			else if($compteperms[$j][$i] == 1)
				echo '<td class="orange">1';
			else if($compteperms[$j][$i] > 1)
				echo '<td class="vert">2+';

			echo "&nbsp;";
			echo '</td>';
		}
		echo '</tr>';
	}
	echo '</tbody></table>';
?>
<p>* : le lundi matin, les créneaux sont décalés de 15 minutes pour s'adapter aux horaires de cours.</p>
<table class="table table-bordered table-striped" style="width:50%;">
<tr>
	<td class="rougee">0</td><td>Aucun permanencier inscrit</td>
</tr>
<tr>
	<td class="orange">1</td><td>1 permanencier inscrit</td>
</tr>
<tr>
	<td class="vert">2+</td><td>Au moins 2 permanenciers inscrits</td>
</tr>
</table>
<?php
	require_once('inc/footer.php');
?>
