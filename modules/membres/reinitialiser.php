<?php
$titrePage = 'Réinitialisation de mot de passe';
require_once('inc/header.php');
$_SESSION['captcha'] = genererAleatoire(6);
?>
<h1>Réinitialisation de mot de passe</h1>
<p><?php afficherErreurs(); ?></p>
<form method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
	<table>
		<tr>
			<td>Email<span class="rouge">*</span> :</td>
			<td>
				<input type="text" class="in-texte" name="nm-email" value="<?php getFormData('nm-email'); ?>" />
				<small>Sous la forme utilisée pour la connexion au site (login@XXX.utc.fr).
			</td>
		</tr>
		<tr>
			<td>
				Recopiez<span class="rouge">*</span> :
			</td>
			<td>
				<input type="text" class="in-texte" name="nm-captcha" />
				<img src="<?php echo $racine.'utils/captcha'; ?>" alt="Recopiez ce texte dans le champ à gauche" />
				<small>Respectez les majuscules/minuscules !</small>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input class="btn" type="submit" name="ReinitialiserMdp" value="R&eacute;initialiser !" />
			</td>
		</tr>
	</table>
</form>
<?php
require_once('inc/footer.php');
?>
