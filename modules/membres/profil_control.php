<?php
if(isset($_GET['ModifProfil'])){
    if ($user->is_logged_with_cas()) {
        if (isset($_GET['test'])) { // on vient d'être authentifié par le CAS
            $_POST = $_SESSION['profil-change'];
            unset($_SESSION['profil-change']);
        } else { // on renvoie vers le cas
            $_SESSION['ReAuthTarget'] = $racine . $module . '/' . $filename .'?ModifProfil&test';
            $_SESSION['profil-change'] = $_POST;
            header('Location: '.$CONF['cas_login']);
            die();
        }
    } else if ($user->is_logged_with_password()) {
        $mdp = $_POST['mp-mdp'];
        // Vérification du mot de passe
        if(!checkMotDePasse($_SESSION['con-id'], $mdp, 'polar_utilisateurs'))
            ajouterErreur("votre mot de passe actuel est incorrect");
    } else {
        header('Location: '.$racine);
        die();
    }

	$presentation = $_POST['mp-presentation'];
	$sexe = $_POST['mm-sexe'];
	$tel = $_POST['mp-tel'];
	$user->Newsletter = isset($_POST['newsletter']);

	// Vérification du téléphone
	if(empty($tel))
		$user->Telephone = NULL;
	else if(!preg_match("!^(06|07)[0-9]{8}$!", $tel))
	 	ajouterErreur("le numéro de téléphone doit être un portable français sur 10 chiffres sans points ni espaces");
 	else
		$user->Telephone = $tel;

	// Vérification du sexe
	if(empty($sexe) || ($sexe != 'm' && $sexe != 'f'))
		$user->Sexe = NULL;
	else
		$user->Sexe = $sexe;

    if (!empty($_POST['mdp-new']) && !empty($_POST['mdp-new2'])) {
        if ($_POST['mdp-new'] != $_POST['mdp-new2']) {
            ajouterErreur("Le mot de passe de confirmation ne correspond pas");
        } else {
            $user->changeMotDePasse($_POST['mdp-new']);
        }
    }

	// Si pas de présentation saisie
    if(empty($presentation))
        $user->Presentation = "Ce membre n'a pas encore de présentation";
    else
        $user->Presentation = $presentation;

	// Fin vérification des données
	if(hasErreurs()){
		saveFormData($_POST);
		header("Location: $racine$module/$section");
		die();
	}

	// Sauvegarde de la photo
	if(!empty($_POST['fileid']) && isset($_POST['x1'], $_POST['y1'], $_POST['w'], $_POST['h'])){
		$x1 = intval($_POST['x1']);
		$y1 = intval($_POST['y1']);
		$w = intval($_POST['w']);
		$h = intval($_POST['h']);
		$fileurl = 'upload/profil/'.$conid.'_'.mysqlSecureText($_POST['fileid']).'.jpg';
		if(is_file($fileurl)){
			// Découpe de l'image originale
			$original = getimagesize($fileurl);
			$img_r = imagecreatefromjpeg($fileurl);

			// Redimensionnement moyen
			$mid = ImageCreateTrueColor(140, 140);
			imagecopyresampled($mid, $img_r, 0, 0, $x1, $y1, 140, 140, $w, $h);
			imagejpeg($mid, 'upload/profil/'.$conid.'_mid.jpg', 90);

			// Redimensionnement petit
			$small = ImageCreateTrueColor(64, 64);
			imagecopyresampled($small, $img_r, 0, 0, $x1, $y1, 64, 64, $w, $h);
			imagejpeg($small, 'upload/profil/'.$conid.'_small.jpg', 90);

			// Suppression de l'original
			unlink($fileurl);
		}
	}

    $user->save();

	echo "<script>alert('Modifications enregistrées !');window.location='$racine$module/$section';</script>";
}
else if(isset($_GET['SupprimerPhoto'])) {
	@unlink('upload/profil/'.$conid.'_mid.jpg');
	@unlink('upload/profil/'.$conid.'_small.jpg');

	header("Location: $racine$module/$section");
}
else if(isset($_GET['Upload'])){
	/**
	 * Handle file uploads via XMLHttpRequest
	 */
	class qqUploadedFileXhr {
	    /**
	     * Save the file to the specified path
	     * @return boolean TRUE on success
	     */
	    function save($path) {
	        $input = fopen("php://input", "r");
	        $temp = tmpfile();
	        $realSize = stream_copy_to_stream($input, $temp);
	        fclose($input);

	        if ($realSize != $this->getSize()){
	            return false;
	        }

	        $target = fopen($path, "w");
	        fseek($temp, 0, SEEK_SET);
	        stream_copy_to_stream($temp, $target);
	        fclose($target);

	        return true;
	    }
	    function getName() {
	        return $_GET['qqfile'];
	    }
	    function getSize() {
	        if (isset($_SERVER["CONTENT_LENGTH"])){
	            return (int)$_SERVER["CONTENT_LENGTH"];
	        } else {
	            throw new Exception('Getting content length is not supported.');
	        }
	    }
	}

	/**
	 * Handle file uploads via regular form post (uses the $_FILES array)
	 */
	class qqUploadedFileForm {
	    /**
	     * Save the file to the specified path
	     * @return boolean TRUE on success
	     */
	    function save($path) {
	        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
	            return false;
	        }
	        return true;
	    }
	    function getName() {
	        return $_FILES['qqfile']['name'];
	    }
	    function getSize() {
	        return $_FILES['qqfile']['size'];
	    }
	}

	class qqFileUploader {
	    private $allowedExtensions = array();
	    private $sizeLimit = 10485760;
	    private $file;

	    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){
	        $allowedExtensions = array_map("strtolower", $allowedExtensions);

	        $this->allowedExtensions = $allowedExtensions;
	        $this->sizeLimit = $sizeLimit;

	        $this->checkServerSettings();

	        if (isset($_GET['qqfile'])) {
	            $this->file = new qqUploadedFileXhr();
	        } elseif (isset($_FILES['qqfile'])) {
	            $this->file = new qqUploadedFileForm();
	        } else {
	            $this->file = false;
	        }
	    }

	    private function checkServerSettings(){
	        $postSize = $this->toBytes(ini_get('post_max_size'));
	        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

	        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
	            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
	            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
	        }
	    }

	    private function toBytes($str){
	        $val = trim($str);
	        $last = strtolower($str[strlen($str)-1]);
	        switch($last) {
	            case 'g': $val *= 1024;
	            case 'm': $val *= 1024;
	            case 'k': $val *= 1024;
	        }
	        return $val;
	    }

	    /**
	     * Returns array('success'=>true) or array('error'=>'error message')
	     */
	    function handleUpload($uploadDirectory, $user, $replaceOldFile = FALSE){
	        if (!is_writable($uploadDirectory)){
	            return array('error' => "Erreur serveur.");
	        }

	        if (!$this->file){
	            return array('error' => 'Pas de fichier.');
	        }

	        $size = $this->file->getSize();

	        if ($size == 0) {
	            return array('error' => 'Le fichier est vide.');
	        }

	        if ($size > $this->sizeLimit) {
	            return array('error' => 'Le fichier ne doit pas dépasser 2Mo.');
	        }

	        $pathinfo = pathinfo($this->file->getName());
			$id = md5(uniqid());
	        $filename = $user."_".$id;
	        $ext = strtolower($pathinfo['extension']);

	        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
	            $these = implode(', ', $this->allowedExtensions);
	            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
	        }

	        if(!$replaceOldFile){
	            /// don't overwrite previous files that were uploaded
	            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
	                $filename .= rand(10, 99);
	            }
	        }

	        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
	            return array('success'=>true, 'url'=> $uploadDirectory.$filename.'.'.$ext, 'ext'=>$ext, 'fileid'=>$id);
	        } else {
	            return array('error'=> 'Could not save uploaded file.' .
	                'The upload was cancelled, or server error encountered');
	        }

	    }
	}

	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$allowedExtensions = array("jpg", "jpeg", "png");
	// max file size in bytes
	$sizeLimit = 2 * 1024 * 1024;

	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
	$result = $uploader->handleUpload('upload/profil/', $conid);

	// Réduire l'image à max 400 pixels de large
	if(isset($result['success'])){
		$original = getimagesize($result['url']);

		if($result['ext'] == 'jpg' || $result['ext'] == 'jpeg')
			$img_r = imagecreatefromjpeg($result['url']);
		elseif($result['ext'] == 'png')
			$img_r = imagecreatefrompng($result['url']);
		else
			die();

		if($original[0] > 500){
			$target_w = 500;
			$target_h = 500 * $original[1] / $original[0];
		}
		else {
			$target_w = $original[0];
			$target_h = $original[1];
		}

		$target = ImageCreateTrueColor($target_w, $target_h);
		imagecopyresampled($target, $img_r, 0, 0, 0, 0,	$target_w, $target_h, $original[0], $original[1]);

		unlink($result['url']);
		$result['url'] = 'upload/profil/'.$conid."_".$result['fileid'].".jpg";
		imagejpeg($target, $result['url'], 80);
	}

	// to pass data through iframe you will need to encode all html tags
	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
}
?>
