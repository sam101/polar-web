<?php
$titrePage = "Bienvenue sur le site du Polar";
require_once('inc/header.php');
?>
<h1>Salut <?php echo $user->Prenom ?></h1>
<p>
Un compte Polar vient d'être créé pour toi<br/><br/>
D'ici tu peux commander des annales d'examen, réserver un vidéoprojecteur pour toi ou ton asso...<br/>
Si tu es marqué comme président ou trésorier d'une association tu pourras consulter la facture en cours et modifier le mot de passe du compte depuis la rubrique <a href="<?php echo urlTo('membres', 'assos') ?>" title="Mes Assos">Membres > Mes assos</a>.<br/><br/>
Utilise les menus déroulants en haut de la page pour accéder aux différentes rubriques.<br/><br/>
Et n'oublie pas : être permanencier c'est assurer deux heures de permanences par semaine au Polar. Tu peux t'inscrire en début de semestre, de préférence aux amphis de présentation, et rendre service aux étudiants pendant un semestre ou plus si affinités.
</p>
<?php
require_once('inc/footer.php');
?>