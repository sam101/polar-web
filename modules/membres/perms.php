<?php
require("_perms.php");
$titrePage = 'G&eacute;rer mes permanences';
use_jquery_ui();
addFooter('
<script>
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
		$(".mail").click(function(){
			$("#idmembre").val($(this).attr("util"));
			$("#messageto").html(\'<h2>Vous allez envoyer un message &agrave; \'+$(this).attr("prenomutil")+\' \'+$(this).attr("nomutil")+\'</h2>\');
			$("#idtextarea").val(\'Votre message ici...\');
			$("#dialog").dialog("open");
			return false;
		})
	});
</script>');
addHeaders('<style type="text/css">
.permanences {
  width:714px;
  margin-top:10px;
  border-collapse:collapse;
  font-size:12px;
}

.permanences th {
  padding:2px;
  border: thin solid #6495ed;
  text-align:center;
  background-color: #D0E3FA;
  text-align:center;
}

.permanences td {
  font-family: sans-serif;
  border: thin solid #6495ed;
  padding: 5px;
  text-align: center;
  background-color: #ffffff;
}
</style>');
require_once('inc/header.php');
?>
<h1>Inscription aux permanences</h1>
<p><?php afficherErreurs(); ?></p>
<?php
//Récupération des paramètres
$req = query("SELECT Login, (SELECT Valeur FROM polar_parametres WHERE Nom LIKE 'max_perms') AS MaxPerms,
	(SELECT Valeur FROM polar_parametres WHERE Nom LIKE 'max_permanenciers') AS MaxPermanenciers,
	(SELECT Valeur FROM polar_parametres WHERE Nom LIKE 'modifier_perms') AS ModifierPerms,
	(SELECT COUNT(*) FROM polar_perms WHERE Permanencier=$conid) AS NbPerms
	FROM polar_utilisateurs WHERE ID=$conid");
$param = mysql_fetch_assoc($req);
$max_perms = $param['MaxPerms'];
$modifier_perms = $param['ModifierPerms'];
$max_permanenciers = $param['MaxPermanenciers'];
$nbperms = $param['NbPerms'];
$restantes = $max_perms - $nbperms;
echo "<p>Tu es inscrit(e) à $nbperms permanence(s). Tu dois encore t'inscrire à $restantes permanence(s) !<br />";
echo "Maximum $max_permanenciers personnes autorisées par permanence !</p>";

// On va construire le tableau des permanences
// tableau à 40 colonnes : 5*9h d'ouverture
for($i=0;$i<9;$i++){
	for($j=0;$j<5;$j++){
		$tab[$j][$i] = '';
		$mesperms[$j][$i] = '';
		$compteperms[$j][$i] = 0;
	}
}

$req = query("SELECT polar_utilisateurs.id AS IDUser, Prenom, Nom, Jour, Creneau FROM polar_perms
	INNER JOIN polar_utilisateurs ON polar_perms.Permanencier = polar_utilisateurs.id");
while($data = mysql_fetch_array($req)){
	// On remplit les cellules du tableau
	$tab[$data['Jour']][$data['Creneau']] .= '<a href="#" class="mail" util="'.$data['IDUser'].'" prenomutil="'.$data['Prenom'].'" nomutil="'.$data['Nom'].'">'.$data['Prenom'].' '.$data['Nom'].'</a><br />';

	// Mes inscriptions
	if($data['IDUser'] == $conid)
		$mesperms[$data['Jour']][$data['Creneau']] = 1;

	// Compte des inscriptions
	$compteperms[$data['Jour']][$data['Creneau']]++;
}
echo '<table class="permanences">';
echo '<tr><th></th><th>Lundi *</th><th>Mardi</th><th>Mercredi</th><th>Jeudi</th><th>Vendredi</th></tr>';
for($i=0;$i<9;$i++){
	// $creneaux est dans _perms.php
	echo "<tr><th>{$creneaux[$i][0]} à {$creneaux[$i][1]}</th>";
	for($j=0;$j<5;$j++){
		echo '<td>';
		echo $tab[$j][$i];

		// Si je suis inscrit, bouton de désinscription
		if($mesperms[$j][$i] && $modifier_perms)
		 	echo '<br /><a href="'.$racine.$module.'/'.$section.'_control?Retirer&jour='.$j.'&creneau='.$i.'" title="Désinscription"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="x" /></a>';
		// Sinon, reste-t-il de la place ?
		else if($compteperms[$j][$i] < $max_permanenciers && $restantes > 0 && $modifier_perms)
			 echo '<br /><a href="'.$racine.$module.'/'.$section.'_control?Ajouter&jour='.$j.'&creneau='.$i.'" title="Inscription"><img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="+" /></a>';
		echo '</td>';
	}
	echo '</tr>';
}
echo '</table>';
echo '<p>* : le lundi matin, les créneaux sont décalés de 15 minutes pour s\'adapter aux horaires de cours.</p>';
echo '<div id="dialog" title="Envoyer un message">
	<form style="margin:0px;" method="post" enctype="multipart/form-data" action="'.$racine.$module.'/'.$section.'_control">
	<input type="hidden" name="idmembre" id="idmembre" />
	<span id="messageto"></span>
	<textarea name="message" id="idtextarea" style="width:650px;height:300px;">Votre message ici...</textarea>
	<input type="submit" name="EnvoyerMessage" value="Envoyer !" />
	</form>
	</div>';
require_once('inc/footer.php');
?>
