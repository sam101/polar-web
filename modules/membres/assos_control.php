<?php

// Les formulaires (reactiver / changer le mot de passe / nouveau compte)
// on les mêmes champs de mot de passe
function getNewMdp() {
    if (!isset($_POST['pass-new1'], $_POST['pass-new2']))
        return false;
    $pass1 = $_POST['pass-new1'];
    if ($pass1 != $_POST['pass-new2'])
        return false;
    else
        return $pass1;
}
function quota() {
    $quota = $_POST['quota'];
    if(empty($quota))
        return NULL;
    else {
        $quota = floatval(str_replace(',', '.', $quota));
        if($quota == 0)
            return NULL;
        else
            return $quota;
    }
}

if(isset($_GET['MotPasse'])){
    /* Changer le mot de passe de l'asso */
	$id = intval($_GET['MotPasse']);
    try {
        $asso = Asso::getById($id);

        $password = $_POST['mdp'];
        $password2 = $_POST['mdp2'];

        if($password != $password2)
            redirectWithErrors("les deux mots de passe ne sont pas identiques");

        $asso->changeMotDePasse($password);
        $asso->save();
        redirectOk('asso='.$asso->get_id(), 'Mot de passe modifié !');
    } catch(UnknownObject $e) {
        redirectWithError('', "Ce compte n'existe pas");
    }
} elseif (isset($_POST['pass-compte-id'])) {
    /* Changer le mot de passe d'un compte */
    try {
        $compte = CompteAsso::getById(intval($_POST['pass-compte-id']));
        if (!$compte->Asso->checkMotDePasse($_POST['pass-old'])) {
            redirectWithErrors('asso='.$compte->get_object_id('Asso'), 'Mot de passe incorrect');
        } else {
            $pass = getNewMdp();
            if ($pass)
                $compte->changeMotDePasse($pass);
            else
                redirectWithErrors('asso='.$compte->get_object_id('Asso'), 'Le mot de passe de confirmation ne correspond pas');

            $compte->save();
            redirectOk('asso='.$compte->get_object_id('Asso'), 'Mot de passe modifié avec succès');
        }
    } catch(UnknownObject $e) {
        redirectWithError('', "Ce compte n'existe pas");
    }
} elseif (isset($_GET['cloturer'])) {
    /* Réactiver un compte */
    try {
        $compte = CompteAsso::getById(intval($_GET['cloturer']));
        $compte->cloturer();
        redirectOk('asso='.$compte->get_object_id('Asso'), 'Compte clôturé');
    } catch(UnknownObject $e) {
        redirectWithError('', "Ce compte n'existe pas");
    }
} elseif (isset($_POST['reactiver-compte-id'], $_POST['quota'])) {
    /* Réactiver un compte */
    try {
        $compte = CompteAsso::getById(intval($_POST['reactiver-compte-id']));
        if ($compte->Asso->checkMotDePasse($_POST['pass-old'])) {
            redirectWithErrors('asso='.$compte->get_object_id('Asso').'&reactiver='.$compte->get_id(),
                               'Mot de passe incorect');
        } else {
            $pass = getNewMdp();
            if ($pass) {
                $compte->Quota = quota();
                $compte->activer($pass);
            }
            else
                redirectWithErrors('asso='.$compte->get_object_id('Asso'), 'Le mot de passe de confirmation ne correspond pas');

            redirectOk('asso='.$compte->get_object_id('Asso'), 'Compte réactivé');
        }
    } catch(UnknownObject $e) {
        redirectWithError('', "Ce compte n'existe pas");
    }
} elseif (isset($_POST['asso-id'], $_POST['nom-compte'], $_POST['quota'])) {
      try {
          $asso = Asso::getById(intval($_POST['asso-id']));
          if (!$asso->checkMotDePasse($_POST['pass-old'])) {
              redirectWithErrors('asso='.$asso->get_id(), 'Mot de passe incorect');
          } else {
              $pass = getNewMdp();
              if ($pass) {
                  $compte = $asso->nouveauCompte($_POST['nom-compte']);
                  $compte->changeMotDePasse($pass);
                  $compte->Quota = quota();

                  $compte->save();
                  redirectOk('asso='.$asso->get_id(), 'Compte créé avec succès !');
              } else
                  redirectWithErrors('asso='.$asso->get_id(), 'Le mot de passe de confirmation ne correspond pas');
          }
      } catch(UnknownObject $e) {
          redirectWithError('', "Ce compte n'existe pas");
      }
  }
