<?php
$titrePage = 'Ma Hotline';
use_jquery_ui();
use_datatables();
addFooter('
	<script language="javascript" type="text/javascript" src="'.$racine.'js/jquery.validate.js"></script>
	<script>
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
		$("#newticketbut").click(function(){
			$("#dialog").dialog("open");
			$("#dialog div").each(function(){
				$(this).hide();
			});
			$("#dialog div#formpage1").show();
		});
		$(".gotobut").each(function(){
			$(this).click(function(){
				$("#dialog div").each(function(){
					$(this).hide();
				});
				$("#dialog div#formpage"+$(this).attr("page")).show();
			});
		});
		$("#addform :radio[name=\'categorie\']").change(function(){
			if($(this).is(":checked")){
				var id = $(this).val();
				$("#objets").html(extras[0][id]);
				$("#extras").html(extras[1][id]);
				$("#description").html(extras[2][id]);
				$("#suivcat").removeAttr("disabled");
				$("#addform").validate();
			}
		});
		$("#addform").validate();
	});
	</script>');
addHeaders('<style type="text/css">
	textarea {
	width: 400px;
	height: 80px;
	}

	#answer label, label.align {
	width: 12em;
	float: left;
	vertical-align: middle;
	text-align: right;
	padding-right: .5em;
	}

	div.message {
	border: 1px solid black;
	width: 45em;
	padding: 1em;
	margin: .5em;
	}

	div.message span.mheader {
	font-style: italic;
	}

	div.message div.mtext {
	padding: 1em;
	}

	.one-col, .two-cols {
	list-style-type:none;
	padding:0;
	padding-right:.1em;
	}

	.two-cols {
	float:left;
	width:49%;
	}

	label input {
		margin-right: .6em;
	}

	label.error {
		color: red;
		font-size: 10px;
	}
	</style>');
require_once('inc/header.php');
if(isset($_GET['Ticket']) && $user->is_logged()){
	$id = intval($_GET['Ticket']);
	if(isset($_GET['Cle']))
		$cle = mysqlSecureText($_GET['Cle']);
	else
		$cle = 0;

	$req = query("SELECT pt.id AS idticket, subject, status, ptc.Nom AS Categorie, opened, Prenom, pu.Nom, pt.author
		FROM polar_tickets pt
		INNER JOIN polar_tickets_categories ptc ON pt.category = ptc.id
		LEFT JOIN polar_utilisateurs pu ON pt.author = pu.Email
		WHERE pt.id = $id
		AND (pt.author = '".$user->Email."' OR MD5(CONCAT(pt.author, pt.Salt)) = '$cle')
		ORDER BY pt.status ASC, pt.opened ASC
		LIMIT 1");
	if(mysql_num_rows($req) == 1){
		$data = mysql_fetch_assoc($req);
		$subject = $data['subject'];
		$status = $data['status'];
		$cat = $data['Categorie'];
		$date = $data['opened'];
		if(empty($data['Nom']))
			$author = $data['author'];
		else
			$author = $data['Prenom'].' '.$data['Nom'].' ('.$data['author'].')';

		echo "<h1>Ticket #$id - $subject</h1>";
		echo '<a href="'.$racine.$module.'/'.$section.'">Retour aux tickets</a>';
		echo "<table class=\"tresume\">
		<tr><th>Auteur</th><td>$author</td>
		<tr><th>Catégorie</th><td>$cat</td>
		<tr><th>Date d'ouverture</th><td>$date</td></tr>
		<tr><th>État</th><td>$status</td>
		</table>";

		// Récup et affichage des messages
		$req = query("SELECT sent, message, Prenom, Nom, sender
			FROM polar_tickets_messages ptm
			LEFT JOIN polar_utilisateurs pu ON pu.Email = ptm.sender
			WHERE ptm.ticket = $id
			ORDER BY ptm.ID ASC");
		while($data = mysql_fetch_assoc($req)){
			if(empty($data['Nom']))
				$auteur = $data['sender'];
			else
				$auteur = $data['Prenom'].' '.$data['Nom'];
			echo '<div class="message">
			<span class="mheader">Écrit par '.$auteur.' le '.$data['sent'].'</span>
			<div class="mtext">'.nl2br($data['message']).'</div>
			</div>';
		}

		// Formulaire de réponse
		echo '<h2>Répondre</h2>';
		$checkboxClose = ($status == 'closed') ? ' checked="checked"' : '';

		echo '<form action="'.$racine.$module.'/'.$section.'_control" method="post" id="answer">
		<label for="message">Message</label><textarea id="message" name="message"></textarea><input type=hidden name="id" value="'.$_GET['Ticket'].'" /><br />
		<label for="close">Clore le ticket</label><input type="checkbox" id="close" name="close" value="true"'.$checkboxClose.' /><br />
		<input type="submit" name="PosterReponse" value="Envoyer la r&eacute;ponse !" />';

		if(isset($_GET['Cle']))
			echo '<input type="hidden" name="cle" value="'.$_GET['Cle'].'" />';

		echo '</form>';
	}
	else
		echo "<h1>Erreur</h1><p>Ticket non trouvé.</p>";
}
// Pour les membres connectés, Hotline !
elseif($user->is_logged()) {
?>
	<h1>Mes tickets</h1>
	<input type="submit" value="Créer un ticket !" class="btn" id="newticketbut" />

	<table class="datatables table table-bordered table-striped">
	<thead><tr><th>ID</th><th>Catégorie</th><th>Sujet</th><th>Date</th><th>Statut</th></tr></thead><tbody>
	<?php
		$req = query("SELECT pt.*, ptc.Nom FROM polar_tickets pt
			INNER JOIN polar_tickets_categories ptc ON pt.category = ptc.id
			WHERE pt.author = '".$user->Email."'
			ORDER BY pt.id DESC");
		while($data = mysql_fetch_assoc($req))
			echo '<tr>
			<td><a href="'.$racine.$module.'/'.$section.'?Ticket='.$data['id'].'">'.$data['id'].'</a></td>
			<td>'.$data['Nom'].'</td>
			<td>'.$data['subject'].'</td>
			<td>'.$data['opened'].'</td>
			<td>'.$data['status'].'</td>
			</tr>';
	?>
	</tbody>
	</table>


	<div id="dialog" title="Créer un nouvel incident">
	<form id="addform" name="formulaire" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
		<div id="formpage1">
			<h3>Bienvenue</h3>
			<p>Cet assistant va vous guider dans la création d'un ticket.</p>
			<p>Attention à bien choisir la catégorie du ticket, qui garantira un traitement dans les meilleurs délais !</p>
			<p><input type="button" value="Commencer" class="btn gotobut" page="2" /></p>
		</div>
		<div id="formpage2" style="display:none;">
			<p>Veuillez choisir la catégorie correspondant à votre ticket :</p>
			<?php
			// On suppose que le module n'est accessible qu'aux member (important !)
			$acces = (!$isStaff) ? "WHERE acces='member' " : "";

			$req = query("SELECT id, Nom, Description, Objets, template FROM polar_tickets_categories ".$acces."ORDER BY Nom ASC");
			echo '<ul class="two-cols">';
			$nbRes = mysql_num_rows($req);
			$i = 0;
			while($data = mysql_fetch_assoc($req)){
				// Liste des objets à choisir pour cette catégorie
				$objetsOutput = '';
				$objets = unserialize($data['Objets']);
				foreach($objets as $objet)
					$objetsOutput .= '<li><label><input type="radio" name="Objet" value="'.$objet.'">'.$objet.'</label></li>';

				// Formulaire correspondant à la catégorie
				$formsOutput = '';
				$forms = unserialize($data['template']);
				foreach($forms as $form){
					if($form['type'] == 'text'){
						$formsOutput .= '<p>
							<label class="align" for="'.$form['nom'].'">'.$form['description'].'</label><input type="text" class="in-texte" id="'.$form['nom'].'" name="'.$form['nom'].'"';
						if($form['required'])
							$formsOutput .= ' class="required"';
						$formsOutput .= ' />
						</p>';
					}
					elseif($form['type'] == 'textarea'){
						$formsOutput .= '<p>
							<label class="align" for="'.$form['nom'].'">'.$form['description'].'</label><textarea id="'.$form['nom'].'" name="'.$form['nom'].'"';
						if($form['required'])
							$formsOutput .= ' class="required"';
						$formsOutput .= '></textarea>
						</p>';
					}
				}

				// Permet de couper la liste en deux colonnes
				if($i == ceil($nbRes/2))
					echo '</ul><ul class="two-cols">';

				// Affichage des catégories (c'est pour ça qu'on est là à la base)
				echo '<li><label><input type="radio" name="categorie" value="'.$data['id'].'">'.$data['Nom'].'</label></li>';

				// On stocke les valeurs à mettre dans le JSON
				$json[0][$data['id']] = $objetsOutput;
				$json[1][$data['id']] = $formsOutput;
				$json[2][$data['id']] = $data['Description'];
				$i++;
			}
			echo '</ul>';

			echo '<script>var extras = '.json_encode($json).'</script>';
			?>
			<p style="clear:both;" id="description"></p>
			<p><input type="button" value="< Précédent" class="btn gotobut" page="1" /> <input type="button" value="Suivant >" class="btn gotobut" id="suivcat" disabled="disabled" page="3" /></p>
		</div>
		<div id="formpage3" style="display:none;">
			<p>Ce ticket concerne :</p>
			<ul class="one-col" id="objets"></ul>

			<p style="clear:both;"><input type="button" value="< Précédent" class="btn gotobut" page="2" /> <input type="button" value="Suivant >" class="btn gotobut" page="4" /></p>
		</div>
		<div id="formpage4" style="display:none;">
			<label class="align" for="subject">Sujet du message</label><input id="subject" name="subject" class="required in-texte" />
			<p id="extras"></p>

			<p style="clear:both;"><input type="button" value="< Précédent" class="btn gotobut" page="3" /> <input type="submit" name="AjouterTicket" value="Envoyer le ticket" class="btn" /></p>
		</div>
	</form>
	</div>
<?php
}
// Pour les autres, formulaire de contact
else {
	$_SESSION['captcha'] = rand(100000,999999);
	?>
	<h1>Comment nous contacter ?</h1>
	<p>Envoyez nous un mail via le formulaire de contact ci-dessous ou passez nous voir au local FE008 ! Si vous avez un compte sur le site du Polar, connectez-vous pour accéder directement à la Hotline.</p>
	<p>&nbsp;</p>
	<h1>Formulaire de contact</h1>
	<p>Un problème ? Une suggestion ? Envoyez-nous un mail via ce formulaire de contact !</p>
	<?php
		echo afficherErreurs();
	?>
<form id="formulaire-news" method="post" action="<?php echo $racine.$module; ?>/index_control">
	<table>
		<tr>
			<th>Votre courriel :</th>
			<td>
				<input type="text" id="id-mail" name="ec-mail" class="in-texte" value="<?php echo getFormData("ec-mail"); ?>" />
			</td>
		</tr>
		<tr>
			<th>Sujet de votre message :</th>
			<td>
				<input type="text" id="id-sujet" name="ec-sujet" class="in-texte" value="<?php echo getFormData("ec-sujet"); ?>" />
			</td>
		</tr>
		<tr>
			<th>Votre question :</th>
			<td>
				<textarea name="ec-message" id="id-message" class="in-texte"><?php echo getFormData("ec-message"); ?></textarea>
			</td>
		</tr>
		<tr>
			<th>Code de sécurité à recopier :</th>
			<td>
				<img src="<?php echo $racine; ?>utils/captcha" alt="Erreur" />
				<input type="text" id="id-captcha" class="in-texte" name="ec-captcha" />
				<input type="hidden" name="ContactForm" />
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input class="btn" type="submit" value="Envoyer la demande" />
			</td>
		</tr>
	</table>
</form>
<?php
}
require_once('inc/footer.php');
?>
