<?php
$titrePage = 'G&eacute;rer les tickets';
use_datatables();
addHeaders('
	<style type="text/css">
		textarea {
			width: 400px;
			height: 80px;
		}

		label {
			width: 14em;
			float: left;
			vertical-align: middle;
			text-align: right;
			padding-right: .5em;
		}

		label.error {
			float: none;
			color: red;
			padding-left: .5em;
			vertical-align: middle;
		}
		div.message {
			border: 1px solid black;
			width: 45em;
			padding: 1em;
			margin: .5em;
		}

		div.message span.mheader {
			font-style: italic;
		}

		div.message div.mtext {
			padding: 1em;
		}
	</style>');
require_once('inc/header.php');
if(isset($_GET['Ticket'])){
	$id = intval($_GET['Ticket']);
	$req = query("SELECT pt.id, pt.subject, pt.status, pt.author, ptc.Nom AS Categorie, pt.category AS CatID, pt.opened, pu.Prenom, pu.Nom, ptc.GenererCheque, pcc.Numero AS Cheque, pcc.Montant, pcc.Motif
		FROM polar_tickets pt
		INNER JOIN polar_tickets_categories ptc ON ptc.id = pt.category
		LEFT JOIN polar_utilisateurs pu ON pu.Email = pt.author
		LEFT JOIN polar_tickets_cheques ptch ON ptch.Ticket = pt.id
		LEFT JOIN polar_caisse_cheques pcc ON pcc.ID = ptch.Cheque
		WHERE pt.id = $id
		LIMIT 1");

	if(mysql_num_rows($req) == 1){
		$data = mysql_fetch_assoc($req);
		$subject = $data['subject'];
		$status = $data['status'];
		$cat = $data['Categorie'];
		$date = $data['opened'];
		$cheque = $data['Cheque'];
		$montant = $data['Montant'];
		$catId = $data['CatID'];
		$genererCheque = $data['GenererCheque'];
		if(empty($data['Nom']))
			$author = $data['author'];
		else
			$author = $data['Prenom'].' '.$data['Nom'].' ('.$data['author'].')';
		$motif = $data['Motif'];

		echo "<h1>Ticket #$id - $subject</h1>";
		echo '<p><a href="'.$racine.$module.'/'.$section.'">Retour aux tickets</a></p>';
		echo "<table class=\"tresume\">
		<tr><th>Auteur</th><td>$author</td>
		<tr><th>Catégorie</th><td>$cat</td>
		<tr><th>Date d'ouverture</th><td>$date</td></tr>
		<tr><th>État</th><td>$status</td>
		</table>";

		// Récup et affichage des messages
		$req = query("SELECT sent, message, Prenom, pu.Nom, ptm.sender
			FROM polar_tickets_messages ptm
			LEFT JOIN polar_utilisateurs pu ON pu.Email = ptm.sender
			WHERE ptm.ticket = $id
			ORDER BY ptm.ID ASC");
		while($data = mysql_fetch_assoc($req)){
			if(empty($data['Nom']))
				$auteur = $data['sender'];
			else
				$auteur = $data['Prenom'].' '.$data['Nom'];
			echo '<div class="message">
			<span class="mheader">Écrit par '.$auteur.' le '.$data['sent'].'</span>
			<div class="mtext">'.nl2br($data['message']).'</div>
			</div>';
		}

		// Formulaire de déplacement
		echo '<h2>Déplacer le ticket</h2>';

		echo '<form action="'.$racine.$module.'/'.$section.'_control" method="post">
		<input type=hidden name="id" value="'.$_GET['Ticket'].'" />
		<select id="deplacer" name="newcategorie">';
		$requin = query("SELECT id, nom FROM polar_tickets_categories");
		while($categorie = mysql_fetch_assoc($requin)){
			if($categorie['id'] != $catId)
				echo '<option value="'.$categorie['id'].'">'.$categorie['nom'].'</option>';
		}

		echo '</select>
		<input type="submit" name="DeplacerTicket" value="Déplacer" class="btn" />
		</form>';

		// Formulaire de réponse
		echo '<h2>Répondre</h2>';
		$checkboxClose = ($status == 'closed') ? ' checked="checked"' : '';

		echo '<form action="'.$racine.$module.'/'.$section.'_control" method="post">
		<label for="message">Message</label><textarea id="message" name="message"></textarea>
		<input type=hidden name="id" value="'.$_GET['Ticket'].'" /><br />';
		if($genererCheque == 1) {
			echo '<p>&nbsp;</p>';
			echo '<p>Si un chèque doit être ébabli, l&rsquo;attestation sera disponible après que les champs ci-dessous soient remplis :</p>';
			echo '<label for="cheque">Num&eacute;ro du ch&egrave;que</label><input class="in-texte" type="text" id="cheque" name="cheque" value="'.$cheque.'" /><br />';
			echo '<label for="montant">Montant accord&eacute;</label><input type="text" class="in-texte" id="montant" name="montant" value="'.$montant.'" /><br />';
			echo '<label for="motif">Motif</label><input type="text" class="in-texte" id="motif" name="motif" value="'.$motif.'" /> <small>Le motif apparaîtra sur l&rsquo;attestation.</small><br />';
			if($cheque > 0 && $montant > 0)
			 	echo '<center><a href="'.$racine.$module.'/'.$section.'_control?GenererPDF='.$id.'" title="Voir l&rsquo;attestation"><img src="'.$racine.'styles/0/icones/pdf.gif" alt="PDF" /></a></center><br />';
		}
		echo '<label for="close">Clore le ticket</label><input type="checkbox" id="close" name="close" value="true"'.$checkboxClose.' /><br />
		<input type="submit" name="AjouterMessage" class="btn" value="Envoyer la r&eacute;ponse !" />
		</form>';
	}
	else
		echo "<h1>Erreur</h1><p>Ticket inexistant ou hors de votre champ d'action.</p>";
}
else{
?>
	<h1>Les tickets qui me sont affectés</h1>
	<table class="datatables table table-bordered table-striped">
		<thead><tr><th>ID</th><th>Catégorie</th><th>Sujet</th><th>Date</th><th>Statut</th></tr></thead><tbody>
		<?php
			$req = query("SELECT pt.*, ptc.Nom FROM polar_tickets pt
				INNER JOIN polar_tickets_categories ptc ON pt.category = ptc.id
				INNER JOIN polar_tickets_responsables ptr ON ptr.Categorie = pt.Category
				WHERE ptr.Utilisateur = $conid
				ORDER BY status,id DESC");
			while($data = mysql_fetch_array($req))
				echo '<tr>
				<td><a href="'.$racine.$module.'/'.$section.'?Ticket='.$data['id'].'">'.$data['id'].'</a></td>
				<td>'.$data['Nom'].'</td>
				<td>'.$data['subject'].'</td>
				<td>'.$data['opened'].'</td>
				<td>'.$data['status'].'</td>
				</tr>';
		?>
		</tbody></table>
 <?php
}
require_once('inc/footer.php');
?>

