<?php
// Ajouter un ticket, pour les utilisateurs connectés
if(isset($_POST['AjouterTicket']) && isset($conid)){
	// Ajout du ticket
	$subject = mysqlSecureText($_POST['subject']);
	$categorie = intval($_POST['categorie']);
	$salt = genererAleatoire(8);

	query("INSERT INTO `polar_tickets` (
		subject,
		category,
		opened,
		status,
		author,
		Salt
	) VALUES (
		'$subject',
		$categorie,
		NOW(),
		'new',
		'".$user->Email."',
		'$salt'
	)
	");
	$id = mysql_insert_id();

	// Gération du message initial
	$message = '';
	foreach($_POST as $key=>$value){
	if(!in_array($key, array('categorie', 'user', 'subject', 'AjouterTicket')) && !empty($value))
		$message .= "$key: $value\n\n";
	}
  $message = mysqlSecureText($message);

	// Ajout du message initial
	query("INSERT INTO `polar_tickets_messages` (
			ticket,
			sender,
			sent,
			message
		) VALUES (
			$id,
			'".$user->Email."',
			NOW(),
			'$message'
		)
	");

	// Prévenir les responsables
	$req = query("SELECT Email FROM polar_utilisateurs pu
		INNER JOIN polar_tickets_responsables ptr ON pu.ID = ptr.Utilisateur
		WHERE ptr.Categorie = $categorie");
	while($data = mysql_fetch_assoc($req)) {
		$msg = "Un utilisateur vient de déposer un ticket dans votre catégorie.<br />
Merci de le traiter sur le site du Polar : http://assos.utc.fr/polar/hotline/liste?Ticket=$id<br />
Le Webmaster.";
		sendMail('polar-hotline@assos.utc.fr', "Le Polar - Hotline", array($data['Email']), "[Le Polar - Hotline] Nouveau ticket", $msg);
	}

	echo '<script>alert("Votre ticket est en cours de traitement !");document.location.href="'.$racine.$module.'/'.$section.'";</script>"';
}
elseif(isset($_POST['PosterReponse'])){
	$id = intval($_POST['id']);

	// S'il poste un mail/clé
	if(isset($_POST['cle']))
		$cle = mysqlSecureText($_POST['cle']);
	else
		$cle = 0;

	// On vérifie que ce mail a les droits sur le ticket
	$req = query("SELECT pt.author
		FROM polar_tickets pt
		WHERE pt.id = $id
		AND (pt.author = '".$user->Email."' OR MD5(CONCAT(pt.author, pt.Salt)) = '$cle')
		LIMIT 1");

	if(mysql_num_rows($req) == 1){
		$res = mysql_fetch_assoc($req);
		$email = $res['author'];

		// Ouverture/fermeture du ticket
		$status = (isset($_POST['close']) && $_POST['close'] == 'true') ? 'closed' : 'active';
		query("UPDATE polar_tickets SET status='$status' WHERE ID = $id LIMIT 1");
		$req = query("SELECT category FROM polar_tickets WHERE ID = $id");
		$data = mysql_fetch_assoc($req);
		$cat = $data['category'];

		// Ajout du message
		if(!empty($_POST['message'])){
			$message = mysqlSecureText($_POST['message']);

			query("INSERT INTO `polar_tickets_messages` (
				`ticket`,
				`sender`,
				`sent`,
				`message`
			) VALUES (
				$id,
				'$email',
				NOW(),
				'$message'
			)");

			//On prévient les responsables
			$req = query("SELECT email, pu.ID AS iduser FROM polar_utilisateurs pu
				INNER JOIN polar_tickets_responsables ptr ON pu.ID = ptr.Utilisateur
				WHERE ptr.Categorie = $cat");
			while($data = mysql_fetch_assoc($req)) {
				$msg = "Un utilisateur vient de poster une réponse sur un ticket de votre catégorie.<br />
Merci de le traiter sur le site du Polar : http://assos.utc.fr/polar/hotline/liste?Ticket=$id<br />
Le Webmaster.";
				sendMail('polar-hotline@assos.utc.fr',"Le Polar - Hotline",array($data['email']),"[Le Polar - Hotline] Nouveau message",$msg);
			}
		}
	}
	if($cle > 0)
		echo "<script>alert('Informations prises en compte avec succès !');document.location.href='$racine$module/$section?Ticket=$id&Cle=$cle';</script>";
	else
		echo "<script>alert('Informations prises en compte avec succès !');document.location.href='$racine$module/$section?Ticket=$id';</script>";
}
elseif(isset($_POST['ContactForm'])){
	$message = nl2br(stripslashes($_POST['ec-message']));
	$captcha = $_POST['ec-captcha'];
	$mail = $_POST['ec-mail'];
	$subject = stripslashes($_POST['ec-sujet']);

	if(!verifMail($mail))
		ajouterErreur("veuillez saisir un courriel valide");

	if(empty($subject))
		ajouterErreur('veuillez saisir un sujet');

	if(empty($message))
		ajouterErreur('veuillez saisir un message');

	if($captcha != $_SESSION['captcha'])
		ajouterErreur('vous avez mal recopié le code de sécurité');

	unset($_SESSION['captcha']);

	if(!hasErreurs()){
		sendMail($mail, 'Contact Site Internet', array('polar@assos.utc.fr'), "[Le Polar - Contact] $subject", $message);
		echo "<script>alert('Votre message a été transmis !');window.location='$racine';</script>";
		die();
	}
	else {
		saveFormData($_POST);
		header("Location: $racine$module/$section");
	}


	/*$subject = mysqlSecureText($_POST['ec-sujet']);
	$message = mysqlSecureText($_POST['ec-message']);
	$mail = mysqlSecureText($_POST['ec-mail']);
	$captcha = $_POST['ec-captcha'];

	if(!verifMail($mail))
		ajouterErreur("veuillez saisir un courriel valide");

	if(empty($subject))
		ajouterErreur('veuillez saisir un sujet');

	if(empty($message))
		ajouterErreur('veuillez saisir un message');

	if($captcha != $_SESSION['captcha'])
		ajouterErreur('vous avez mal recopié le code de sécurité');

	if(!hasErreurs()){
		// Ajout du ticket
		$salt = genererAleatoire(8);
		$categorie = 12;
		query("INSERT INTO `polar_tickets` (
				subject,
				author,
				category,
				opened,
				status,
				Salt
			) VALUES (
				'$subject',
				'$mail',
				$categorie,
				NOW(),
				'new',
				'$salt'
			)
			");
			$id = mysql_insert_id();

			// Ajout du message initial
			query("INSERT INTO `polar_tickets_messages` (
				ticket,
				sender,
				sent,
				message
			) VALUES (
				$id,
				'$mail',
				NOW(),
				'$message'
			)");

			// Envoi du lien à la personne
			$cle = md5($mail.$salt);
			$msg = "Bonjour,<br /><br />
Nous avons bien reçu votre message. Un membre de l'équipe y répondra prochainement et vous recevrez un e-mail pour vous prévenir.<br />
Vous pouvez suivre votre ticket à l'adresse suivante : http://assos.utc.fr/polar/hotline/index?Ticket=$id&Cle=$cle<br />
Cordialement,<br />
L'équipe du Polar";
			sendMail('polar-hotline@assos.utc.fr',"Le Polar - Hotline",array($mail),"[Le Polar - Hotline] Réception de votre message",$msg);

			//Prévenir les responsables
			$req = query("SELECT Email FROM polar_utilisateurs pu
				INNER JOIN polar_tickets_responsables ptr ON pu.ID = ptr.Utilisateur
				WHERE ptr.Categorie = $categorie");
			while($data = mysql_fetch_assoc($req)) {
				$msg = "Un utilisateur vient de déposer un ticket dans votre catégorie.<br />
Merci de le traiter sur le site du Polar : http://assos.utc.fr/polar/hotline/liste?Ticket=$id<br />
Le Webmaster.";
				sendMail('polar-hotline@assos.utc.fr', "Le Polar - Hotline", array($data['Email']), "[Le Polar - Hotline] Nouveau ticket", $msg);
			}

			echo '<script>alert("Votre ticket est en cours de traitement ! Vous allez recevoir un email de confirmation.");document.location.href="'.$racine.$module.'/'.$section.'";</script>"';
			die();
		}
	else {
		unset($_SESSION['captcha']);
		saveFormData($_POST);
	}
	header("Location: $racine$module/$section");*/
}
?>
