<?php
if(isset($_POST['AjouterMessage'])) {
	$id = intval($_POST['id']);
	// On vérifie que ce mail a les droits sur le ticket
	$req = query("SELECT pt.author, pt.Salt, pu.Nom, pu.Prenom
		FROM polar_tickets pt
		INNER JOIN polar_tickets_responsables ptr ON ptr.Categorie = pt.category
		INNER JOIN polar_utilisateurs pu ON pu.Email LIKE pt.author
		WHERE pt.id = $id AND ptr.Utilisateur = ".$user->get_id()."
		LIMIT 1");

	if(mysql_num_rows($req) == 1){
		$res = mysql_fetch_assoc($req);
		$emailAuteur = $res['author'];
		$salt = $res['Salt'];

		// Ouverture/fermeture du ticket
		$status = (isset($_POST['close']) && $_POST['close'] == 'true') ? 'closed' : 'active';
		query("UPDATE polar_tickets SET status='$status' WHERE ID=$id LIMIT 1");

		// Ajout du message
		if(!empty($_POST['message'])){
			// Ajout SQL
			$message = mysqlSecureText($_POST['message']);
			query("INSERT INTO `polar_tickets_messages` (
				`ticket`,
				`sender`,
				`sent`,
				`message`
			) VALUES (
				$id,
				'".$user->Email."',
				NOW(),
				'$message'
			)");

			// On prévient l'auteur du ticket
			$cle = md5($emailAuteur.$salt);
			$msg = "Bonjour,<br /><br />
Une réponse vient d'être ajoutée à l'un de vos tickets sur le site du Polar.<br />
Pour la consulter et répondre, rendez-vous à l'adresse suivante : http://assos.utc.fr/polar/hotline/index?Ticket=$id&Cle=$cle<br /><br />
Cordialement,<br />
L'équipe du Polar.";

			sendMail('polar-hotline@assos.utc.fr','Le Polar - Hotline',array($emailAuteur),"[Le Polar - Hotline] Nouveau message",$msg);
		}

		// Ajout du chèque
		if(!empty($_POST['cheque']) && !empty($_POST['montant']) && !empty($_POST['motif']) && intval($_POST['cheque']) > 0){
			$id = intval($_POST['id']);
			$cheque = intval($_POST['cheque']);
			$montant = (float)($_POST['montant']);
			$nom = mysqlSecureText($res['Nom']);
			$prenom = mysqlSecureText($res['Prenom']);
			$detail = mysqlSecureText($_POST['message']);
			$motif = mysqlSecureText($_POST['motif']);

			$exists = query("SELECT Cheque FROM polar_tickets_cheques WHERE Ticket=$id");
			if(mysql_num_rows($exists) == 0){
				query("INSERT INTO polar_caisse_cheques (Date, Numero, Banque, Montant, Emetteur, Ordre, PEC, Motif) VALUES(
					NOW(),
					$cheque,
					'Societe Generale',
					$montant,
					'Le Polar',
					'$prenom $nom',
					1,
					'$motif'
				)");
				$idCheque = mysql_insert_id();
				query("INSERT INTO polar_tickets_cheques (Cheque,Ticket) VALUES ($idCheque,$id)");

			}
			else {
				$idCheque = mysql_fetch_assoc($exists);
				$idCheque = $idCheque['Cheque'];

				query("UPDATE polar_caisse_cheques SET
					Date = NOW(),
					Numero = $cheque,
					Montant = $montant,
					Motif = '$motif',
					PEC = 1
					WHERE ID = $idCheque");
			}
		}
	}

	echo "<script>alert('Informations prises en compte avec succès !');document.location.href='$racine$module/$section?Ticket=$id';</script>";
}
else if(isset($_POST['DeplacerTicket'])){
	$id = intval($_POST['id']);
	$newcat = intval($_POST['newcategorie']);
	// On vérifie que ce mail a les droits sur le ticket
	$req = query("SELECT pt.author
		FROM polar_tickets pt
		INNER JOIN polar_tickets_responsables ptr ON ptr.Categorie = pt.category
		WHERE pt.id = $id AND ptr.Utilisateur = ".$user->get_id()."
		LIMIT 1");

	if(mysql_num_rows($req) == 1){
		query("UPDATE polar_tickets SET category = $newcat WHERE id = $id LIMIT 1");

		// On prévient les responsables de la nouvelle catégorie
		$req = query("SELECT Email FROM polar_utilisateurs pu
			INNER JOIN polar_tickets_responsables ptr ON pu.ID = ptr.Utilisateur
			WHERE ptr.Categorie = $newcat");
		while($data = mysql_fetch_assoc($req)) {
			$msg = "Un utilisateur vient de déplacer un ticket dans votre catégorie.<br />
Merci de le traiter sur le site du Polar : http://assos.utc.fr/polar/hotline/liste?Ticket=$id<br />
Le Webmaster.";
			sendMail('polar-hotline@assos.utc.fr', "Le Polar - Hotline", array($data['Email']), "[Le Polar - Hotline] Nouveau ticket", $msg);
		}
	}
	echo "<script>alert('Le ticket a été déplacé. Si vous n\'êtes pas administrateur de la nouvelle catégorie, vous allez voir un message d\'erreur.');document.location.href='$racine$module/$section?Ticket=$id';</script>";
}
else if(isset($_GET['GenererPDF'])){
	require_once('inc/tcpdf.php');
	$geste = intval($_GET['GenererPDF']);

	$req_geste = query("SELECT ptcat.Nom, pcc.Numero AS Cheque, pcc.Ordre AS Client, pcc.Montant, UNIX_TIMESTAMP(pt.opened) AS DateDemande, pcc.Motif
	 	FROM polar_tickets pt
		INNER JOIN polar_tickets_cheques ptc ON ptc.Ticket = pt.id
		INNER JOIN polar_caisse_cheques pcc ON pcc.ID = ptc.Cheque
	 	INNER JOIN polar_tickets_categories ptcat ON ptcat.id = pt.category
		WHERE pt.id = $geste");
	$donnees = mysql_fetch_assoc($req_geste);

	$sql = query("SELECT Valeur AS president FROM polar_parametres WHERE Nom LIKE 'prez'");
	$prez = mysql_fetch_assoc($sql);

	$pdf = new PolarPDF("ATTESTATION");

	$pdf->SetFontSize(12);
	$output = '<p align="right">Compiègne, le '.date("d/m/Y").'</p>';

	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);


	$html = '<p>'.unhtmlentities($donnees['Client']).' déclare avoir reçu le chèque suivant :</p>
	<ul>
		<li>Banque : Société Générale</li>
		<li>Numéro : '.$donnees['Cheque'].'</li>
		<li>Montant : '.formatPrix($donnees['Montant']).'€</li>
	</ul>
	correspondant au '.$donnees['Nom'].' n°'.$geste.' demandé le '.date("d/m/Y", $donnees['DateDemande']).'.</p>';
	$html .= '<p>Détails :<br />'.unhtmlentities($donnees['Motif']).'</p>';
	$html .= 'Imprimé le '.date("d/m/Y").' en deux exemplaires originaux.';

	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$pdf->Signature("prez", 'Le bénéficaire,<br />'.$donnees['Client']);
	$file = 'Documents/Attestation_'.$donnees['Client'].'_'.$geste.'.pdf';
	$pdf->Output($file, 'F');
	header('Location: '.$racine.$file);
}
?>
