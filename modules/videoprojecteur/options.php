<?php
$titrePage = 'Gestion du système de location de VP';
require('inc/header.php');
?>
<h1>Gestion du système de location de VP</h1>
 <?php
if(isset($_SESSION['ap-erreur'])) {
echo '<p>
<img src="',$racine,'styles/',$design,'/icones/exclamation.png" alt="" /> &nbsp;<span class="erreur-script">',htmlentities($_SESSION['ap-erreur']),'</span></p>';
unset($_SESSION['ap-erreur']);
}

$req = query("SELECT Valeur FROM polar_parametres WHERE Nom IN ('vp_actif') ORDER BY Nom ASC");
$actif = mysql_result($req, 0);

$req = query("SELECT CodeProduit FROM polar_commandes_types WHERE Nom LIKE 'Videoprojecteur'");
$code = mysql_result($req, 0);

?>

<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
	<table>
		<tr>
			<td>Etat du système :</td>
			<td>
				<select name="ap-etat">
					<option value="0"<?php if($actif == 0) echo ' selected="selected"'; ?>>Inactif</option>
					<option value="1"<?php if($actif == 1) echo ' selected="selected"'; ?>>Actif</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Code de l'article VP Etudiant dans la caisse :</td>
			<td>
				<input type="text" name="ap-articles" size="50" value="<?php echo $code; ?>" />
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input class="btn" type="submit" value="Modifier le système !" /></td>
		</tr>
	</table>
</form>
<?php
require('inc/footer.php');
?>
