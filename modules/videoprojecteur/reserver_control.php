<?php

$req = query("SELECT pca.ID, pct.Actif, pca.PrixVente FROM polar_commandes_types pct
        INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
        WHERE pct.Nom LIKE 'Videoprojecteur' AND pct.Actif = 1 AND pca.Actif = 1 AND pca.EnVente = 1");

if(mysql_num_rows($req) <= 0){
    header('Location: '.$racine);
    exit();
}

if (isset($_GET['Reserver'])) {
    $date = mysqlSecureText($_POST['date']);

    if(!isset($_POST['cgl'])){
        ajouterErreur("vous devez accepter les CGL");
        header("Location: $racine$module/$section?date=$date");
        exit();
    }

    if(!hasErreurs()){
        $id = intval($_SESSION['con-id']);
        $req = query("SELECT Nom, Prenom, Email FROM polar_utilisateurs WHERE id=$id");
        $donnees = mysql_fetch_assoc($req);
        $nom = $donnees['Nom'];
        $prenom = $donnees['Prenom'];
        $mail = $donnees['Email'];
        $ip = mysqlSecureText(get_ip());

        query("INSERT INTO polar_commandes (
                Type,
                Nom,
                Prenom,
                Mail,
                DateCommande,
                DatePrete,
                IPCommande
            ) VALUES(
                (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Videoprojecteur'),
                '$nom',
                '$prenom',
                '$mail',
                NOW(),
                '$date',
                '$ip'
            )
        ");
        $newid = mysql_insert_id();

        query("INSERT INTO polar_commandes_contenu (
                IDCommande,
                Detail
            ) VALUES(
                $newid,
                'vp-etu'
            )
        ");

        if(isset($_POST['hp']))
            query("INSERT INTO polar_commandes_contenu (
                    IDCommande,
                    Detail
                ) VALUES(
                    $newid,
                    'hp-etu'
                )
            ");

        $commande = Commande::getById($newid); // todo
        Request::$panier->addCommande($commande, $user);
        Request::$panier->redirectTo($commande);
    }

} elseif(isset($_GET['Supprimer'])) {
    $commandeID = intval($_GET['Supprimer']);


    $mois_actuel = date("m");
	$an_actuel = date("Y");

	$mois = isset($_GET['mois']) ? intval($_GET['mois']) : $mois_actuel;
	$an = isset($_GET['an']) ? intval($_GET['an']) : $an_actuel;

    $currentTime = mktime(0, 0, 0);

    query("DELETE pc FROM polar_commandes pc
    LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
    INNER JOIN polar_commandes_types pct ON pct.ID=pc.Type
    AND pct.Nom='Videoprojecteur'
    AND pcc.Detail='vp-etu'
    AND pc.ID=$commandeID
    AND pc.Mail = '".$user->Email."'
    AND UNIX_TIMESTAMP(pc.DatePrete) >= $currentTime");

	header("Location: $racine$module/$section?mois=$mois&an=$an");
} else {
	header("Location: $racine$module/$section");
}
?>
