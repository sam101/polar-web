<?php
$titrePage = "Vidéoprojecteur";
require("inc/header.php");
?>
<h1>Pôle vidéoprojecteur</h1>
<img style="float:right;margin-left:15px;" src="<?php echo $racine; ?>upload/videoprojecteur/vp.jpg" width="298" height="255" />
<p style="margin-top: 20px;">Le Polar dispose d'un vidéoprojecteur HD.</p>
<p>La location coûte <b>5&euro;</b> pour les étudiants. Elle est gratuite pour les associations. Des haut-parleurs peuvent être prêtés avec toute location.</p>
<p>Le vidéoprojecteur devra être retiré au Polar <strong class="souligne">après 12h30</strong> le jour de la réservation et être ramené avant 12h30 le jour suivant (le lundi pour une location le vendredi). Il est à noter qu'une caution de 600&euro; est exigée avant la location.</p>
<a href="<?php echo urlTo('videoprojecteur', 'reserver') ?>" class="btn btn-large btn-primary">Réservation</a>
</p>
<?php
require("inc/footer.php");
?>
