<?php
if(isset($_POST['ap-etat'], $_POST['ap-articles'])) {
	// Sécurisation des données entrantes
	$etat = intval($_POST['ap-etat']);
	$code = intval($_POST['ap-articles']);

	// On sauve les paramètres
	query("REPLACE INTO polar_parametres (Nom, Valeur) VALUES
		('vp_actif', $etat)
	");

	// On vire le vieux code s'il a changé
	query("DELETE FROM polar_commandes_types WHERE CodeProduit != $code AND Nom LIKE 'Videoprojecteur'");

	// On ajoute le nouveau codes
	query("INSERT IGNORE INTO polar_commandes_types (Nom,CodeProduit) VALUES ('Videoprojecteur', $code)");

  	header("Location: $racine$module/$section");
}
else
	header('Location: '.$racine);
?>
