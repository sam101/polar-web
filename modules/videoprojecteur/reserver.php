<?php
$titrePage = "Vidéoprojecteur";

addHeaders('<style type="text/css">
table.jours-videoproj {
	border: 1px solid #ccc;
	width: 100%;
	border-collapse:collapse;
	margin-top: 20px;
}
table.jours-videoproj td {
	border: 1px solid #ccc;
	height: 80px;
	text-align: center;
	width: 80px;
}
table.jours-videoproj tr.jours {
	color: #555;
}
table.jours-videoproj tr.jours td, table.jours-videoproj tr.navigation td {
	height: 20px;
}
table.jours-videoproj td.reserve {
	background: #f6cccc;
}
table.jours-videoproj td.today {
	background: #d9e7ed;
}
</style>
');

require("inc/header.php");

$req = query("SELECT pca.ID, pct.Actif, pca.PrixVente FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
	WHERE pct.Nom LIKE 'Videoprojecteur' AND pct.Actif = 1 AND pca.Actif = 1 AND pca.EnVente = 1");

if(mysql_num_rows($req) == 0)
	echo '<p>Ce secteur est actuellement ferm&eacute;. Merci de revenir plus tard !</p>';
else {
	$vpdata = mysql_fetch_assoc($req);
	if((isset($_GET['jour']) && isset($_GET['mois']) && isset($_GET['an'])) || isset($_GET['date'])){
		if(isset($_GET['date']))
			$date = $_GET['date'];
		else
			$date = intval($_GET['an']).'-'.intval($_GET['mois']).'-'.intval($_GET['jour']);

		$login = $user->Login;

		echo "<h1>Réservation pour le $date</h1>";
		echo 'Le prix de la location est actuellement de '.$vpdata['PrixVente'].'€ par jour.<br />';
		echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control?Reserver">';
		echo "La location sera enregistrée pour $login le $date<br>";
		echo '<input type="hidden" name="date" size="12" value="'.$date.'"><br />';
		afficherErreurs();
		echo '<label><input type="checkbox" name="hp" /> Je souhaite également emprunter des haut-parleurs</label><br />';
		echo '<label><input type="checkbox" name="cgl" /> J\'accepte les </label><a href="'.$racine.'upload/videoprojecteur/CGL.pdf" title="" target="_new">Conditions Générales De Location De Matériel</a><br />';
		echo '<input type="submit" value="OK"></form>';
	} // Fin de si un jour est précisé
	else {
?>
<h1>Réservation du vidéoprojecteur du Polar par un étudiant de l'UTC</h1>
<h2>ATTENTION : le vidéoprojecteur se loue à la journée.</h2>
<p>Il devra être retiré au Polar <strong class="souligne">après 12h30</strong> le jour de la réservation et être ramené avant 12h30 le jour suivant (le lundi pour une location le vendredi). Il est à noter qu'une caution de 600 est exigée avant la location.</p>
<?php
	$tab_logins = array();

    $currentTime = mktime(0, 0, 0);
	$currentMonth = date("m");
	$currentYear = date("Y");

    $selectedMonth = isset($_GET['mois']) ? intval($_GET['mois']) : $currentMonth;
    $selectedYear = isset($_GET['an']) ? intval($_GET['an']) : $currentYear;
    $selectedMonth = ($selectedMonth < 1 || $selectedMonth > 12) ? 1 : $selectedMonth;
    $selectedYear = ($selectedYear < 1972 || $selectedYear > 2042) ? $currentYear : $selectedYear;
    $selectedTime = mktime(0, 0, 0, $selectedMonth, 1, $selectedYear);

    $prevTime = strtotime('-1 month', $selectedTime);
    $prevMonth = date('m', $prevTime);
    $prevYear = date('Y', $prevTime);
    $nextTime = strtotime('+1 month', $selectedTime);
    $nextMonth = date('m', $nextTime);
    $nextYear = date('Y', $nextTime);

    $firstDayOfWeek = date('w', $selectedTime);
    $lastDay = date('t', $selectedTime);

    $monthName = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

	$req = query("SELECT pc.ID, pc.Nom, pc.Prenom, pc.Mail, DATE(DatePrete) AS DatePrete, pcc.Detail FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
		INNER JOIN polar_commandes_types pct ON pct.ID=pc.Type
		INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse=pct.CodeProduit
		WHERE pca.Actif=1 AND pct.Nom='Videoprojecteur' AND MONTH(DatePrete) = $selectedMonth AND YEAR(DatePrete) = $selectedYear AND pcc.Detail='vp-etu'");

	// On marque les jours où il y a des réservations
	while($ligne = mysql_fetch_assoc($req)) {
		// recupartion du jour ou il y a la reservation
		$jour_reserve = date("j", strtotime($ligne['DatePrete']));

		// insertion des jours reservé dans le tableau
		$tab_logins[$jour_reserve] = array('Nom' => $ligne["Prenom"]." ".$ligne["Nom"],
                                            'Mail' => $ligne['Mail'],
                                            'ID' => $ligne['ID']);
	}

	echo '
	  <table class="jours-videoproj">
		<tr class="navigation">
			<td colspan="2" style="text-align: left;">
				<a href="'.$racine.$module.'/'.$section.'?mois='.$prevMonth.'&an='.$prevYear.'">< '.$monthName[$prevMonth - 1].' '.$prevYear.'</a>
			</td>
			<td colspan="3">
			  <h3>'.$monthName[$selectedMonth - 1]." ".$selectedYear.'</h3>
			</td>
			<td colspan="2" style="text-align: right;">
				<a href="'.$racine.$module.'/'.$section.'?mois='.$nextMonth.'&an='.$nextYear.'">'.$monthName[$nextMonth - 1].' '.$nextYear.' ></a>
			</td>
		</tr>
	    <tr class="jours">
	      <td>lundi</td>
	      <td>mardi</td>
	      <td>mercredi</td>
	      <td>jeudi</td>
	      <td>vendredi</td>
	      <td>samedi</td>
	      <td>dimanche</td>
	    </tr>';

	// Affichage du calendrier
	$nb_rangees = ($firstDayOfWeek > 5 || $firstDayOfWeek == 0) ? 6 : 5;
	for ($rangee = 0; $rangee < $nb_rangees; $rangee++){
		echo '<tr class="numero">';
		for ($i=1;$i<8;$i++){
        	$thisDay = $i + $rangee*7 + 1 - $firstDayOfWeek;
            $cellTime = mktime(0, 0, 0, $selectedMonth, $thisDay, $selectedYear);
			if($thisDay < 1 || $thisDay > $lastDay) {
				echo '<td></td>';
            } else {
				// si c'est un jour reserve on applique le style reserve
				if(!empty($tab_logins[$thisDay])){
                    $commande = $tab_logins[$thisDay];
					if($i === 5){
						$tab_logins[$thisDay+1] = $commande;
						$tab_logins[$thisDay+2] = $commande;
					}

					echo '<td class="reserve">';
					echo $thisDay;
					echo '<br />';
					if($isStaff) {
						echo $commande['Nom'];
                        echo '<br />';
                    }

                    if (($cellTime >= $currentTime) && ($commande['Mail'] === $user->Email)) {
                        echo '<a href="',$racine,$module,'/',$section,'_control?Supprimer=',$commande['ID'],'&mois=',$selectedMonth,'&an=',$selectedYear,'">';
						echo '<img src="',$racine,'styles/0/icones/supprimer.png" border="0" alt="Supprimer la commande" />';
                        echo '</a>';
                        echo '<br />';
                    }

					echo '</td>';
				} else {                 // sinon on ne met pas de style
					if($cellTime === $currentTime) {
						echo '<td class="today">';
                    } else {
						echo '<td>';
                    }
					echo $thisDay;
					echo '<br />';

					if(($cellTime >= $currentTime) && ($i !== 6) && ($i !== 7)){
						echo '<a href="'.$racine.$module.'/'.$section.'?mois='.$selectedMonth.'&an='.$selectedYear.'&jour='.$thisDay.'">';
						echo '<img src="'.$racine.'styles/0/icones/ajout.png" border="0" alt="Marquer comme reserve" />';
						echo '</a>';
					}
					echo '</td>';
				}
			}
		}
		echo '</tr>';
	}
?>
</table>

<?php
}
} //Fin Section fermée !
require("inc/footer.php");
?>
