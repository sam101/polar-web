<?php
$titrePage = "Le pôle Assos";
require('inc/header.php');
?>
<div class="well">
<h1>Réactiver un compte Assos</h1>
<p>Chaque semestre, les comptes assos doivent être réactivés. Pour cela, choisissez votre association dans la liste suivante et renseignez tous les champs.</p>
<p>Cette page permet de <b>réactiver</b> un compte existant. Pour créer le compte d'une nouvelle association, vous devez vous rendre au Polar et rencontrer un membre du Bureau.</p>
</div>
<?php
echo afficherErreurs();
?>
<div class="text-center">
<div style="display:inline-block;text-align:left;">
<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control" class="form-horizontal">
    <div class="control-group">
      <label class="control-label" for="ep-nom">Nom de l'association</label>
      <div class="controls"><select name="ep-nom" id="ep-nom">
		<option value="">Choisir...</option>
<?php
$nomChoisi = getFormData('ep-nom');
$assos = Asso::select()->where("Etat = 'Clos'")->order('Asso ASC');
foreach($assos as $asso) {
	echo '<option value="'.$asso->get_id().'"';
	if($nomChoisi == $asso->get_id())
	 	echo ' selected="selected"';
	echo '>'.$asso->Asso.'</option>';
}
?>
        </select>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-mail">Adresse mail de l'association</label>
      <div class="controls"><input type="text" id="ep-mail" class="in-texte" name="ep-mail" value="<?php echo getFormData("ep-mail"); ?>" /></div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-president">Nom et prénom du président</label>
      <div class="controls"><input type="text" class="in-texte" name="ep-president" id="ep-president" value="<?php echo getFormData("ep-president"); ?>" /></div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-presidentmail">Mail UTC du président</label>
      <div class="controls">
		<input type="text" class="in-texte" name="ep-presidentmail" id="ep-presidentmail" value="<?php echo getFormData("ep-presidentmail"); ?>" />
		<small>Au format login@etu.utc.fr si possible.</small>
	  </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-presidenttel">Téléphone du président</label>
      <div class="controls"><input type="text" class="in-texte" name="ep-presidenttel" id="ep-presidenttel" value="<?php echo getFormData("ep-presidenttel"); ?>" /></div>
    </div>
<div class="control-group">
      <label class="control-label" for="ep-tresorier">Nom et prénom du trésorier</label>
      <div class="controls"><input type="text" class="in-texte" name="ep-tresorier" id="ep-tresorier" value="<?php echo getFormData("ep-tresorier"); ?>" /></div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-tresoriermail">Mail UTC du trésorier</label>
      <div class="controls">
		<input type="text" class="in-texte" name="ep-tresoriermail" id="ep-tresoriermail" value="<?php echo getFormData("ep-tresoriermail"); ?>" />
		<small>Au format login@etu.utc.fr si possible.</small>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-tresoriertel">Téléphone du trésorier</label>
      <div class="controls"><input type="text" class="in-texte" name="ep-tresoriertel" id="ep-tresoriertel" value="<?php echo getFormData("ep-tresoriertel"); ?>" /></div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-password">Mot de passe souhaité</label>
      <div class="controls"><input type="password" class="in-texte" name="ep-password" id="ep-password"/>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="ep-password2">Confirmer le mot de passe</label>
      <div class="controls"><input type="password" class="in-texte" name="ep-password2" id="ep-password2"/>
      </div>
    </div>
	<div class="control-group">
		<div class="controls">
			<strong>En cliquant sur <i>Envoyer la demande !</i>, je reconnais avoir pris connaissance des termes de la <a href="<?php echo $racine; ?>upload/charte_associations.pdf" target="_new" title="">Convention d'ouverture d'un compte association</a> et les accepte. Le clic tient lieu de validation électronique.<br />
			<span style="color:red;">ATTENTION : les impressions au tarif assos sont désormais limitées à 3500 pages par semestre et par association.</span></strong>
<br/>
        <input type="submit" value="Envoyer la demande !" class="btn btn-primary" />
      </div>
    </div>
</form>
</div>
</div>
<?php
require('inc/footer.php');
?>
