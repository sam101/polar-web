<?php
$titrePage = "Création d'un compte assos";
require('inc/header.php');
?>
<h1>Création d'un compte assos</h1>
<p>Cette page va créer un nouveau compte assos. Le compte devra ensuite être activé par l'assos via la page Compte Assos (site public).</p>
<h2>IMPORTANT : Règles de nommage</h2>
<ol>
	<li><b>Une assos = un compte</b> : vérifier avant tout que l'assos n'a pas déjà un compte (sur la page de <a href="<?php echo $racine.$module; ?>/reactiver" title="">réactivation</a> ainsi que dans la liste des <a href="<?php echo $racine.$module; ?>/comptes" title="">comptes en attente</a> et <a href="<?php echo $racine.$module; ?>/comptes?actifs" title="">actifs</a>)</li>
	<li><b>Pas d'années</b> : le site assure le suivi des factures entre les semestres, donc un compte du type <i>UTCoupe A10</i> est à proscrire</li>
	<li>Préférer les <b>noms courts</b>, par souci de place sur les factures : PSEC, c'est mieux que Pôle Solidarité et Citoyenneté</li>
</ol>
<p>Il convient bien entendu de vérifier que l'association sera un minimum capable de payer ses factures.</p>
<p>&nbsp;</p>
<?php afficherErreurs(); ?>
<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
Nom du compte : <input type="text" name="nom" class="in-texte" value="<?php echo getFormData('nom'); ?>" />
<input type="submit" value="Créer" class="btn" />
</form>
<?php
require('inc/footer.php');
?>
