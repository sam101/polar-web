<?php
function facturerCompteAsso($asso){
	$asso = intval($asso);

	// Création de la facture
	query("INSERT INTO polar_assos_factures (
		Asso,
		`Date`
	) VALUES(
		$asso,
		NOW()
	)");
	$idfact = mysql_insert_id();

	// Passage des ventes comme finalisées avec ID de facture
	query("UPDATE polar_caisse_ventes_global SET Finalise=1, Facture=$idfact WHERE Asso=$asso AND Finalise=0");

	// Calcul du montant de la facture
	$req = query("SELECT SUM(PrixFacture) AS MontantTTC
		FROM polar_caisse_ventes_global
		WHERE Facture = $idfact");
	$data = mysql_fetch_assoc($req);
	$montant = $data['MontantTTC'];

	// Enregistrement du montant
	query("UPDATE polar_assos_factures SET Montant = $montant WHERE ID = $idfact");

	// Génération de la facture
	$chemin = genererFactureAsso($idfact, false);

	// Message du mail
	$msg = "Bonjour,<br /><br />Vous trouverez ci-joint la facture du Polar concernant votre association.";
	$msg .= "Vous pouvez la régler :<br />";
	$msg .= "- en déposant un chèque au Polar pendant les horaires d'ouverture<br />";
	$msg .= "- en faisant parvenir un chèque au Polar par courrier interne<br />";
	$msg .= "- par virement bancaire (demandez-nous notre RIB)<br /><br />";
	$msg .= "Cordialement,<br />L'équipe du Polar";

	$pj = array($chemin);

	$req = query("SELECT MailAsso, MailPresident, MailTresorier FROM polar_assos WHERE ID=$asso");
	$data = mysql_fetch_assoc($req);
	sendMail('polar@assos.utc.fr', 'Le Polar', array($data['MailAsso'], $data['MailPresident'], $data['MailTresorier']),"[Le Polar] Facture du semestre pour votre association", $msg, $pj);
}

function genererFactureAsso($id, $duplicata = true){
	global $racine;

	if(isset($_GET['no_duplicata']))
		$duplicata = false;

	$id = intval($id);

	$reqnom = query("SELECT pa.Asso, UNIX_TIMESTAMP(paf.Date) AS Date FROM polar_assos pa
		INNER JOIN polar_assos_factures paf ON paf.Asso = pa.ID
		WHERE paf.ID = $id");
	$resnom = mysql_fetch_assoc($reqnom);
	$asso = $resnom['Asso'];
	$date = $resnom['Date'];

	$numfact = 'A'.$id.'.'.date('dmY', $date);

	$req = query("SELECT Nom, PrixVenteAsso, PrixVente, PrixFacture, TVA, Quantite, Tarif, Client, DATE(pcvg.Date) AS DateVente FROM polar_caisse_ventes_global pcvg
		INNER JOIN polar_caisse_articles pca ON pca.ID = pcvg.Article
		WHERE pcvg.Facture = $id
		ORDER BY pcvg.Date ASC");

	require_once('inc/tcpdf.php');

	if($duplicata)
		$pdf = new PolarPDF("DUPLICATA DE FACTURE");
	else
		$pdf = new PolarPDF("FACTURE");

	$pdf->SetFontSize(12);
	$output = '<p align="right">'."Facture N°$numfact";
	$output .= ' du '.date("d/m/Y", $date).'<br>';
	$output .= 'Client : '.stripslashes(html_entity_decode($asso, ENT_COMPAT, 'UTF-8')).'</p>';

	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$pdf->SetFontSize(10);
	$output = '<table border="1" cellpadding="4">
	<tr>
		<th width="315"><b>Désignation</b></th>
		<th width="85"><b>Date</b></th>
		<th width="70"><b>PU TTC</b></th>
		<th width="45"><b>Qté</b></th>
		<th width="55"><b>TVA</b></th>
		<th width="75"><b>Total TTC</b></th>
	</tr>';

	// Données
	while($data = mysql_fetch_assoc($req)){
	  if($data['Tarif'] == 'asso' && !empty($data['PrixVenteAsso']))
			$prixVente = $data['PrixVenteAsso'];
		else
			$prixVente = $data['PrixVente'];

		$output .= '<tr>';
		$texte = $data['Nom'];
		$output .= '<td width="315">'.$texte;
		if(!empty($data['Client']))
			$output .= '<br>  '.stripslashes($data['Client']);
		$output .= '</td>';

		$output .= '<td width="85">'.stripslashes($data['DateVente']).'</td>';
		$output .= '<td width="70" align="right">'.number_format($prixVente, 2, ',', ' ').' €</td>';
		$output .= '<td width="45" align="right">'.$data['Quantite'].'</td>';
		$output .= '<td width="55" align="right">'.number_format($data['TVA']*100, 1, ',', ' ').'%</td>';
		$output .= '<td width="75" align="right">'.number_format($data['PrixFacture'], 2, ',', ' ').' &euro;</td>';
		$output .= '</tr>';
	}

	$output .= '</table>';

	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$req = query("SELECT SUM(Prixfacture) AS MontantTTC, SUM(Prixfacture/(1 + TVA)) AS MontantHT
		FROM polar_caisse_ventes_global pcvg
		INNER JOIN polar_caisse_articles pca ON pca.ID = pcvg.Article
		WHERE Facture = $id");
	$data = mysql_fetch_assoc($req);

	$pdf->Image('upload/signature_prez.jpg', 45, '', 60, '', 'JPG', '', '', true, 150);

	$output = '<p align="right"><b>Montant HT : '.number_format($data['MontantHT'], 2, ',', ' ').' €<br>';
	$output .= 'Montant TTC : '.number_format($data['MontantTTC'], 2, ',', ' ').' €</b></p>';
	$pdf->SetFontSize(12);
	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');

	$file = 'Documents/facture_'.$numfact.'_'.date('HmidmY').'.pdf';
	$pdf->Output($file, 'F');

	return $file;
}

function cloturerCompteAsso($id, $corps) {
	$req = query("SELECT MailAsso, MailTresorier, MailPresident FROM polar_assos WHERE ID=$id");
	$donnees = mysql_fetch_assoc($req);
	sendmail('polar@assos.utc.fr', 'Le Polar', array($donnees['MailAsso'], $donnees['MailPresident'], $donnees['MailTresorier']), "[Le Polar] Rappel de renouvellement de compte", $corps);
	query("UPDATE polar_assos SET Etat = 'Clos' WHERE ID=$id");
}

?>
