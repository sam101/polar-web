<?php
if((isset($_POST['ep-convention'], $_POST['ep-montant'], $_POST['ep-detail'], $_POST['ep-cheque'], $_POST['ep-virement']))) {
	$convention=intval($_POST['ep-convention']);
	$montant=(float) $_POST['ep-montant'];
	$detail=mysqlSecureText($_POST['ep-detail']);
	$cheque=intval($_POST['ep-cheque']);
  $virement=mysqlSecureText($_POST['ep-virement']);

  if (($cheque === 0 AND $virement == '') OR ($cheque > 0 AND $virement != '')) {
    ajouterErreur('Remplissez soit la case chèque soit la case virement');
		saveFormData($_POST);
		header("Location: $racine$module/$section");
  } else {
  	//Récupérer le but :
  	$req=query("SELECT Asso, But FROM polar_assos_conventions WHERE id=$convention");
  	$data=mysql_fetch_assoc($req);
  	$nom_asso=mysqlSecureText($data['Asso']);
  	$but=$data['But'];

  	//Signataire ?
  	$req = query("SELECT * FROM polar_assos_conventions WHERE id=$convention");
  	$data = mysql_fetch_assoc($req);
  	$signataire = mysqlSecureText($data['TypeSignataire']);
  	$nom_signataire=mysqlSecureText($data['NomSignataire']);
  	$but=mysqlSecureText($but);

    if ($montant == 0) {
      $Reqcheque = "0";
      $Reqvirement = "NULL";
    }
    else if ($cheque === 0) {
      $Reqcheque = "0";
      $Reqvirement = "'$virement'";
      query("INSERT INTO polar_caisse_virement (
               Date,
               Emetteur,
               Destinataire,
               Montant,
               Effectue
             ) VALUES (
               NOW(),
               'Le Polar',
               '$nom_asso',
               $montant,
               0
             )");
    } else {
      // On ajoute le chèque dans la base
      query("INSERT INTO polar_caisse_cheques (
  	  		NumVente,
  	  		Date,
  	  		Numero,
  	  		Banque,
  	  		Montant,
  	  		Emetteur,
  	  		Ordre,
  	  		PEC
  	  	) VALUES(
  	  		-1,
  	  		NOW(),
  	  		$cheque,
  	  		'Societe Generale',
  	  		$montant,
  	  		'Le Polar',
  	  		'$nom_asso',
  	  		1
  	  	)
  	  ");
      $Reqcheque = $cheque;
      $Reqvirement = "NULL";
    }

  	query("INSERT INTO polar_assos_paiement (
  			Asso,
  			Date,
  			Convention,
  			But,
  			Montant,
  			Detail,
  			TypeSignataire,
  			Signataire,
  			Cheque,
        DateVirement
  		) VALUES(
  			'$nom_asso',
  			NOW(),
  			'$convention',
  			'$but',
  			$montant,
  			'$detail',
  			'$signataire',
  			'$nom_signataire',
  			$Reqcheque,
        $Reqvirement
  		)
  	");

  	query("UPDATE polar_assos_conventions SET Attestation=1 WHERE id=$convention");
    header("Location: $racine$module/$section");
  }
}
else if(isset($_GET['pdf']) && isset($_GET['id'])){
	$convention = intval($_GET['id']);
	$sql = query("SELECT * FROM polar_assos_paiement WHERE id=$convention");
	$donnees = mysql_fetch_assoc($sql);
	require_once('inc/tcpdf.php');
	$pdf=new PolarPDF("Attestation de paiement - Convention n° ".$donnees['Convention']);
	$pdf->SetFontSize(12);
	$output = '<p align="right">Compiègne, le '.date("d/m/Y").'</p>';

	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

  // On détermine d'abord si la convention a été cloturée par un virement ou un chèque
  if ($donnees['Montant'] == 0) {
    $html = '<p>Aucun paiement effectué, en accord avec les modalités de la convention.</p>';
  } else if ($donnees['Cheque'] > 0) {
    $html = '<p>'.$donnees['Asso'].' déclare avoir reçu le chèque suivant :</p>
	  <ul>
		  <li>Banque : Société Générale</li>
		  <li>Numéro : '.$donnees['Cheque'].'</li>
		  <li>Montant : '.formatPrix($donnees['Montant']).'€</li>
	  </ul>';
  } else {
    $html = '<p>'.$donnees['Asso'].' déclare avoir reçu le virement suivant :</p>
    <ul>
      <li>Date : ' . $donnees['DateVirement'] . '</li>
      <li>Montant : ' . formatPrix($donnees['Montant']) . '€</li>
    </ul>';
  }

	$html .= '<p>relatif à la convention n°'.$donnees['Convention'].' : '.$donnees['But'].'</p>
	<p>Détails :</p>
	<p>'.$donnees['Detail'].'</p>';

  $html .= "<p>Cette attestation clôture la convention.</p>";

	$html .= '<p>Imprimé le '.date("d/m/Y").' en deux exemplaires originaux.</p>';

	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$pdf->Signature("prez", 'Pour '.$donnees['Asso'].',<br />'.$donnees['Signataire'].', '.$donnees['TypeSignataire']);
	$file = 'Documents/Attestation_'.$donnees['Asso'].'_'.$donnees['id'].'.pdf';
	$pdf->Output($file, 'F');
	header('Location: '.$racine.$file);
}
?>
