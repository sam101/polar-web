<?php
$titrePage = "Conventions de partenariat spécifiques";
if(isset($_GET['id'])){
	require('inc/header.php');
?>
	<h1>Les conventions de partenariat spécifiques - Détail</h1>
	<p><a href="<?php echo $racine.$module.'/'.$section; ?>">Revenir aux conventions</a></p>
	<?php
		$id = intval($_GET['id']);
		$req = query("SELECT *,DATE(Date) AS DateConvention FROM polar_assos_conventions WHERE id=$id");
		$data = mysql_fetch_assoc($req);
		echo '
		<table class="datatables table table-bordered table-striped table-condensed">
			<tr>
				<th colspan="2">Convention avec '.$data['Asso'].' du '.$data['DateConvention'].'</th>
			</tr>
			<tr>
				<td>But :</td>
				<td>'.$data['But'].'</td>
			</tr>
			<tr>
				<td>Le Polar s\'engage</td>
				<td>'.$data['PolarSengage'].'</td>
			</tr>
			<tr>
				<td>'.$data['Asso'].' s\'engage</td>
				<td>'.$data['AssoSengage'].'</td>
			</tr>
			<tr>
				<td>Signataire</td>
				<td>'.$data['TypeSignataire'].'</td>
			</tr>
			<tr>
				<td>Nom signataire</td>
				<td>'.$data['NomSignataire'].'</td>
			</tr>
			<tr>
				<td>Voir la convention</td>
				<td>
					<a href="'.$racine.$module.'/'.$section.'_control?pdf&id=',$data['ID'],'" title="Voir la convention en PDF"><img src="',$racine,'styles/',$design,'/icones/pdf.gif" alt="-" /></a>
				</td>
			</tr>
		</table>';
		require('inc/footer.php');
}
else {
	use_jquery_ui();
	addFooter('<script>
		  $(document).ready(function() {
			$("#ajouter").dialog({ width: 700, autoOpen: false });
			$("#newbut").click(function(){
				$("#ajouter").dialog("open");
			})
		  });
		  </script>');
	require('inc/header.php');
?>
<h1>Les conventions de partenariat spécifiques</h1>
<input type="button" value="Nouvelle convention" id="newbut" class="btn" />
<div id="ajouter" title="Ajouter une convention de partenariat">
<?php
	$req = query("SELECT ID,Asso FROM polar_assos ORDER BY Asso ASC");
?>
	<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
		<table>
			<tr>
				<td>Asso :</td>
				<td>
					<select name="ep-asso">
					<?php while($data=mysql_fetch_assoc($req)) echo '<option value="'.$data['ID'].'">'.$data['Asso'].'</option>'; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Signataire :</td>
				<td>
					<select name="ep-signataire">
						<option value="0">Président</option>
						<option value="1">Trésorier</option>
					</select>
					<small>Seuls les présidents et trésoriers de l'association peuvent signer une convention.</small>
				</td>
			</tr>
			<tr>
				<td>But :</td>
				<td>
					<input type="text" size="60" class="in-text" name="ep-but" <?php if(isset($_SESSION['ep-but'])) { echo 'value="',$_SESSION['ep-but'],'" '; unset($_SESSION['ep-but']); }?>/>
				</td>
			</tr>
			<tr>
				<td>Le Polar s'engage...</td>
				<td>
					<textarea rows="8" cols="60" name="ep-polarsengage"><?php if(isset($_SESSION['ep-polarsengage'])) { echo $_SESSION['ep-polarsengage']; unset($_SESSION['ep-polarsengage']); }?></textarea>
				</td>
			</tr>
			<tr>
				<td>L'assos s'engage...</td>
				<td>
					<textarea rows="8" cols="60" name="ep-assosengage"><?php if(isset($_SESSION['ep-assosengage'])) { echo $_SESSION['ep-assosengage']; unset($_SESSION['ep-assosengage']); }?></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:right;">
					<input type="submit" value="Ajouter la convention" class="btn" />
				</td>
			</tr>
		</table>
	</form>
</div>
<p>Ces conventions spécifiques sont pour les demandes spécifiques, autres que les comptes Assos : places d'Estu, places de Gala, ou tout autre demande !</p>

<table class="datatables table table-bordered table-striped table-condensed">
	<thead>
		<tr>
			<th>Asso</th>
	        <th>Date</th>
	        <th>But</th>
		</tr>
	</thead>
<?php

	$req = query("SELECT id, Asso, DATE(Date) AS DateConvention, But FROM polar_assos_conventions ORDER BY id DESC");
	if(mysql_num_rows($req) > 0){
		while($donnees = mysql_fetch_assoc($req)) {
			echo '
              <tr>
                <td><a href="'.$racine.$module.'/'.$section.'?id='.$donnees['id'].'">'.$donnees['Asso'].'</a></td>
                <td>',$donnees['DateConvention'],'</td>
                <td>',$donnees['But'],'</td>
              </tr>';
		}
	}
	else{
		echo '<tr>
				<td colspan="3"><span style="font-style:italic;">Il n\'y a aucune convention pour le moment.</span></td>
			</tr>';
	}
	echo '</table>';

	require('inc/footer.php');
}
?>


