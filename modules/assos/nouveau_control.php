<?php
if(isset($_POST['nom'])){
	if(empty($_POST['nom']))
		redirectWithErrors('', "saisissez un nom");
	else {
        $asso = new Asso(array('Asso' => $_POST['nom'],
                               'DateCreation' => raw('NOW()'),
                               'Etat' => 'Clos'));
        $asso->save();
        $asso->nouveauCompte($_POST['nom'])->save();

		redirectOk('', 'Compte créé !');
	}
}
?>
