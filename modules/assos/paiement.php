<?php
$titrePage = "Attestations de paiement";
use_jquery_ui();
addFooter('
	<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker({ dateFormat: \'yy-mm-dd\' });
	});
	</script>');

if(isset($_GET['id'])){
	require('inc/header.php');
?>
    <h1>Les attestations de paiement - Détail</h1>
	<p><a href="<?php echo $racine.$module.'/'.$section; ?>">Revenir aux attestations</a></p>
	<?php
		$id=$_GET['id'];
		$req=query("SELECT * FROM polar_assos_paiement WHERE id=$id");
		$data=mysql_fetch_assoc($req);
		echo '
		<table class="datatables table table-bordered table-striped table-condensed">
			<tr>
				<th colspan="2">Attestation avec '.$data['Asso'].' du '.$data['Date'].'</th></tr>
				<tr>
					<td>Convention / But :</td>
					<td>'.$data['Convention'].' / '.$data['But'].'</td>
				</tr>
				<tr>
					<td>Montant : </td>
					<td>'.$data['Montant'].'</td>
				</tr>';

    if ($data['Montant'] == 0) {
      // On n'affiche rien mais c'est plus simple de faire comme ça :p
    }
    else if ($data['Cheque'] != 0) {
      echo '<tr>
					<td>Chèque : </td>
					<td>'.$data['Cheque'].'</td>
				</tr>
				<tr>';
    } else {
      echo '<tr>
					<td>Date du virement : </td>
					<td>'.$data['DateVirement'].'</td>
				</tr>
				<tr>';
    }
		echo '
				<td>Détail : </td>
					<td>'.$data['Detail'].'</td>
				</tr>
				<tr>
					<td>Signataire</td>
					<td>'.$data['TypeSignataire'].'</td>
				</tr>
				<tr>
					<td>Nom signataire</td>
					<td>'.$data['Signataire'].'</td>
				</tr>
				<tr>
					<td>Voir l\'attestation</td>
					<td><a href="'.$racine.$module.'/'.$section.'_control?pdf&id=',$data['id'],'" title="Voir la convention en PDF"><img src="',$racine,'styles/',$design,'/icones/pdf.gif" alt="-" /></a></td>
				</tr>
		</table>';

	require('inc/footer.php');
}
else {
	use_jquery_ui();
	addFooter('<script>
		  $(document).ready(function() {
			$("#ajouter").dialog({ width: 700, autoOpen: false });
			$("#newbut").click(function(){
				$("#ajouter").dialog("open");
			})
		  });
		  </script>');
	require('inc/header.php');
?>

<h1>Les attestations de paiement</h1>
<?php
echo afficherErreurs();
?>
<input type="button" value="Nouvelle attestation" id="newbut" class="btn" />
<div id="ajouter" title="Ajouter une attestation de paiement">
<?php
	$req = query("SELECT id, Asso, But FROM polar_assos_conventions WHERE Attestation=0 ORDER BY Asso ASC");
?>
	<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
		<table>
			<tr>
				<td>Convention :</td>
              	<td>
					<select name="ep-convention">
					<?php while($data=mysql_fetch_assoc($req)) echo '<option value="'.$data['id'].'">'.$data['Asso'].' - '.$data['But'].'</option>'; ?>
					</select>
				</td>
           	</tr>
            <tr>
              	<td>Montant :</td>
              	<td>
					<input type="text" size="10" class="in-text" name="ep-montant" <?php if(isset($_SESSION['ep-montant'])) { echo 'value="',$_SESSION['ep-montant'],'" '; unset($_SESSION['ep-montant']); }?>/>
				</td>
            </tr>
			<tr>
            <tr>
            <td>Date du virement (si paiement par virement) :</td>
              	<td>
					<input type="text" size="10" class="in-text hasDatePicker" id="datepicker" name="ep-virement" <?php if(isset($_SESSION['ep-virement'])) { echo 'value="',$_SESSION['ep-virement'],'" '; unset($_SESSION['ep-virement']); }?>/>
				</td>
            </tr>
			<tr>
            <td>n° Chèque (si paiement par chèque) :</td>
              	<td>
					<input type="text" size="10" class="in-text" name="ep-cheque" <?php if(isset($_SESSION['ep-cheque'])) { echo 'value="',$_SESSION['ep-cheque'],'" '; unset($_SESSION['ep-cheque']); }?>/>
				</td>
            </tr>
            <tr>
              	<td>Détails :</td>
              	<td>
					<textarea rows="8" cols="60" name="ep-detail"><?php if(isset($_SESSION['ep-detail'])) { echo $_SESSION['ep-detail']; unset($_SESSION['ep-detail']); }?></textarea>
				</td>
            </tr>
            <tr>
              	<td colspan="2" style="text-align:right;">
			  		<input type="submit" value="Ajouter l'attestation" class="btn" />
				</td>
            </tr>
		</table>
	</form>
</div>

<p>Les attestations de paiement clôturent les conventions et permettent de faire un chèque à l'association.</p>

<table class="datatables table table-bordered table-striped table-condensed">
	<tr>
		<th>Asso</th>
		<th>Date</th>
		<th>But</th>
	</tr>

<?php
	$req = query("SELECT id, Asso, DATE(Date) AS DateAttestation, But FROM polar_assos_paiement ORDER BY id DESC");

	if(mysql_num_rows($req) > 0){
		while($donnees = mysql_fetch_assoc($req)) {
			echo '
				<tr>
					<td><a href="'.$racine.$module.'/'.$section.'?id='.$donnees['id'].'">',$donnees['Asso'],'</a></td>
					<td>',$donnees['DateAttestation'],'</td>
					<td>',$donnees['But'],'</td>
				</tr>';
		}
	}
	else{
		echo '<tr>
				<td colspan="3"><span style="font-style:italic;">Il n\'y a aucune attestation pour le moment.</span></td>
			</tr>';
	}
	echo '</table>';

	require('inc/footer.php');
}
?>
