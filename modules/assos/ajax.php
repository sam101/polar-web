<?php

$result = array();

switch($_GET['action']) {
case 'search':
    if (isset($_GET['q'])) {
        $nom = mysqlSecureText($_GET['q']);
        $assos = Asso::selectActives()->where("UPPER(Asso) LIKE UPPER('%$nom%')");
        foreach($assos as $asso) {
            $result[] = $asso->Asso;
        }
        die(implode("\n", $result)); // Stupid autocomplete plugin !
    } else {
        $result[] = 'Terme de recherche manquant';
    }
    break;
case 'infos':
    if (isset($_GET['nom'])) {
        $asso = Asso::select()->where("Asso LIKE ? AND Etat = 'Actif'", $_GET['nom'])->getOne();
        if ($asso == NULL)
            die();

        $result = array('comptes'=>array(),
                        'nom'=>$asso->Asso,
                        'id'=>$asso->get_id(),
                        'etat'=>$asso->Etat);

        $comptes = $asso->getComptesActifs()->select('CompteAsso.*')
            ->select('COALESCE((SELECT SUM(`PrixFacture`) FROM `polar_caisse_ventes`
                       WHERE Asso = CompteAsso.ID AND `Finalise` = 0 ), 0) +
                      COALESCE((SELECT SUM(`PrixFacture`) FROM `polar_caisse_ventes_global`
                       WHERE Asso = CompteAsso.ID AND `Finalise` =0 ), 0)', 'EnCours')
            ->execute(true);
        foreach ($comptes as $compte) {
            $result['comptes'][] = array('id'=>$compte->get_id(),
                                         'nom'=>$compte->Nom,
                                         'quota'=>$compte->Quota,
                                         'en_cours'=>$compte->EnCours,
                                         'tarif_asso'=>$compte->TarifAsso);
        }
        break;
    }
default:
    $result[] = "Action invalide";
}

header('Content-type: text/json');
echo json_encode($result);

?>