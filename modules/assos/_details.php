<?php
try {
    $asso = Asso::getById(intval($_GET['id_asso']));
} catch (UnknownObject $e) {
    throw new PolarUserError('Identifiant d\'asso invalide');
}

addFooter('
	<script type="text/javascript" src="'.$racine.'js/jquery.jeditable.min.js"></script>
	<script>
submit = function(value, settings) {
           $.post("'.urlControl('liveEdit='.$asso->get_id()).'", {id: $(this).attr("id"), value: value}).done(function(data) {
              if (data.error)
                  alert("La modification n\'a pas pu être enregistrée :\n"+data.error);
});

           return(value);
};


function activer_compte(id) {
  $("#activer-compte").modal("show");
  $("#reactiver-compte-id").val(id);
}

$(document).ready(function() {
    $(".edit").editable(submit, {submit : "Enregistrer",
                                 tooltip: "Cliquer pour modifier",
                                 style: "inherit",
                                 width: "none"});
    $("#nouveau-compte").click(function() {
      $("#new-compte").modal("show");
    });
});
	</script>');
?>

<h1>Informations détaillées concernant le compte</h1>
<p>
	<a href="<?php echo "$racine$module/$section"; ?>?actifs">Revenir à la liste des comptes actifs</a> - <a href="<?php echo "$racine$module/$section"; ?>">Revenir aux demandes en cours</a>
</p>

<div class="alert alert-info">
  <strong>Hey Ho !</strong>
  Tu peux modifier certaines infos en cliquant dessus (adresses mail et numéros de téléphone)
</div>

<p>
<?php

echo $asso->format_from_attributes('<table class="table table-bordered table-condensed table-striped">
  <tr>
    <th>ID</th>
    <td>{{ID}}</td>
  </tr>
<tr>
    <th>Nom de l\'asso</th>
    <td>{{Asso}}</td>
  </tr>
<tr>
    <th>Mail</th>
    <td class="edit" id="MailAsso">{{MailAsso}}</td>
  </tr>
<tr>
    <th>Président</t>
    <td>{{President}}</td>
  </tr>
<tr>
    <th>Mail Président</th>
    <td class="edit" id="MailPresident">{{MailPresident}}</td>
  </tr>
<tr>
    <th>Tél Président</th>
    <td class="edit" id="TelPresident">{{TelPresident}}</td>
  </tr>
<tr>
    <th>Trésorier</th>
    <td>{{Tresorier}}</td>
  </tr>
<tr>
    <th>Mail Trésorier</th>
    <td class="edit" id="MailTresorier">{{MailTresorier}}</td>
  </tr>
<tr>
    <th>Tél Trésorier</th>
    <td class="edit" id="TelTresorier">{{TelTresorier}}</td>
  </tr>
<tr>
    <th>État : </td>
    <td>{{Etat}}</td>
  </tr>
<tr>
    <th>Date Création</th>
    <td>{{DateCreation}}</td>
  </tr>
</table>');

?>
	<h1>Achats non facturés</h1>
	<table class="datatables table table-bordered table-striped">
		<thead>
			<tr><th>Date</th><th>Article</th><th>Compte</th><th>Quantité</th><th>Prix unitaire</th><th>Montant</th></tr>
		</thead>
		<tbody>
<?php
$achats = OldVente::select('OldVente.*, Article.Nom, Article.PrixVente, Article.PrixVenteAsso, CompteAsso.Nom as NomCompte')
         ->join('Article', 'OldVente.Article = Article.ID')
         ->join('CompteAsso', 'CompteAsso.ID = OldVente.Asso')
         ->where('Finalise=0 AND facture=0 AND CompteAsso.Asso=?', $asso);

$total = 0;
foreach($achats as $achat) {
	echo '<tr>';
	echo '<td>'.$achat->Date.'</td>';
	echo '<td>'.$achat->Nom.'</td>';
	echo '<td>'.$achat->NomCompte.'</td>';
	echo '<td>'.$achat->Quantite.'</td>';
  	if($achat->Tarif == 'asso' && $achat->PrixVenteAsso != NULL)
		$prixVente = $achat->PrixVenteAsso;
	else
		$prixVente = $achat->PrixVente;
	echo '<td>'.formatPrix(round($prixVente, 2)).' €</td>';
	$montant = round($achat->PrixFacture, 2);
	echo '<td>'.formatPrix($montant).' €</td>';
	$total += $montant;
	echo '</tr>';
}
	?>
		</tbody>
	</table>

	<p style="clear:right;"><b>En-cours total : <?php echo $total; ?>€</b></p>

	<?php if($total > 0){ ?>
		<p>
			<form method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section; ?>_control?facturer">
				<input type="hidden" name="id-asso" value="<?php echo $asso->get_id(); ?>" />
				<input type="submit" value="Facturer !" class="btn" />
			</form>
		</p>
	<?php
}
?>
<h1>Factures</h1>
<p>Les factures sans boutons de règlement sont déjà réglées.</p>
<table class="table table-striped table-bordered table-condensed">
	<tr>
		<th>Date</th>
		<th>Montant</th>
		<th>Voir la facture</th>
		<th>Encaisser par chèque</th>
		<th>Encaisser par virement</th>
	</tr>
<?php
$factures = $asso->getFactures()->order('FactureAsso.Date DESC');

$hasfactures = false;
foreach($factures as $facture) {
	echo '<tr>
		<td>'.$facture->Date.'</td>
		<td>'.formatPrix($facture->Montant).'€</td>
		<td><a href="'.$racine.$module.'/factures_control?ConsulterFacture='.$facture->get_id().'" title="Consulter la facture !"><i class="icon-print"></i></a></td>
		<td>';

	if(empty($facture->Encaisse)) {
		echo '<a title="Encaisser par chèque !" onclick="var x=prompt(\'Numéro du chèque ?\'); if(x != null) {var y=prompt(\'Banque ?\'); if(y != null) location.href= \'',$racine.$module,'/factures_control?EncaisserCheque=',$facture->get_id(),'&Cheque=\'+x+\'&Banque=\'+y;}" style="cursor:pointer;"><i class="icon-inbox"></i></a>';
	  echo '</td>
		<td>';
		echo '<a href="'.$racine.$module.'/'.$section.'_control?EncaisserVirement='.$facture->get_id().'" title="Encaisser par virement !"><i class="icon-share-alt"></i></a>';
	  echo '</td>';
	  $hasfactures = true;
  } else {
    echo "<td></td>\n<td></td>";
  }
	echo '</tr>';
}
?>
</table>
<?php
if($asso->Etat == 'Actif'){
?>
<br />
<h1>Bloquer pour défaut de paiement</h1>
<form name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section; ?>_control?defaut_paiement">
	<table>
		<tr>
			<td>Situation du compte :</td>
			<td>Débloqué : <input type="radio" name="ep-bloque" value="0" <?php if($asso->Etat == 'Actif') echo 'checked="checked"'; ?>/> Bloqué :<input type="radio" name="ep-bloque" value="1" <?php if($asso->Etat== 'DefautPaiement') echo 'checked="checked"'; ?>/></td>
		</tr>
		<tr>
			<td>Message :</td>
			<td>
				<textarea type="text" cols="40" rows="5" name="ep-raison">Bonjour,
Suite à nos diverses relances, la facture de votre association n'a toujours pas été réglée.
En conséquence, le compte de votre association est maintenant bloqué pour défaut de paiement.
Merci de passer au Polar pour régulariser la situation.
Cordialement,
L'équipe du Polar
				</textarea>
				<input type="hidden" name="ep-id" value="<?php echo $asso->get_id(); ?>">
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:right;">
				<input type="submit" value="Modifier !" class="btn" />
			</td>
		</tr>
	</table>
</form>

<h1>Clôturer le compte</h1>
<p>&Agrave; chaque début du semestre, envoie un mail pour indiquer de remplir le formulaire du compte sur le site du Polar.<br />ATTENTION : Le compte est alors désactivé.</p>
<form name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section; ?>_control?suspendre">
	<table>
		<tr>
			<td>Message :</td>
			<td>
				<textarea type="text" cols="40" rows="5" name="ep-raison">Bonjour,

Le semestre se termine, nous venons donc de clôturer le compte de votre association.
Dès que la nouvelle équipe est formée, celle-ci devra réouvrir le compte au Polar à cette adresse : http://assos.utc.fr/polar/assos/reactiver

Merci de leur transmettre cette information.

A bientôt,
L'équipe du Polar</textarea>
				<input type="hidden" name="ep-id" value="<?php echo $asso->get_id(); ?>">
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:right;">
				<input type="submit" value="Envoyer le rappel !" class="btn" />
			</td>
		</tr>
	</table>
</form>
<?php
} // Fin compte actif
elseif($asso->Etat == 'AttenteActivation') {
	echo '<br /><h1>Valider le compte</h1>
	<p>Ce compte est en attente de validation.</p>';
	if($hasfactures)
		echo '<p style="color:red;">ATTENTION : il reste des factures non payées !</p>';
	echo '<form action="'.$racine.$module.'/'.$section.'_control" method="get">
	<input type="hidden" name="Valider" value="'.$asso->get_id().'" />
	<input type="submit" name="Valider le compte" class="btn" />
	</form>';
} ?>
    <h2>Sous-comptes</h2>
    <button class="btn" id="nouveau-compte">Créer un compte</button>
    <table class="table table-condensed table-striped table-bordered">
      <thead>
        <tr>
          <th>Nom</th>
          <th>État</th>
          <th>Quota</th>
          <th>Tarif Asso</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($asso->getComptes()->select('CompteAsso.*') as $compte): ?>
        <tr>
          <td><?php echo $compte->Nom ?></td>
          <td><?php
          if(!is_null($compte->DateActivation))
            echo 'Actif &nbsp;&nbsp;<a href="'.urlControl('desactiverCompte='.$compte->get_id()).'" class="btn"><i class="icon-remove"></i> Clôturer</a>';
          else
          echo 'Clos &nbsp;&nbsp;<a href="#" onclick="activer_compte('.$compte->get_id().')" class="btn"><i class="icon-ok"></i> Activer</a>'; ?></td>
          <td><?php echo $compte->Quota ? formatPrix($compte->Quota).' €' : 'Aucun' ?></td>
          <td><?php
            if($compte->TarifAsso)
              echo 'Oui &nbsp;&nbsp;<a href="'.urlControl('val=0&setTarifAsso='.$compte->get_id()).'" class="btn"><i class="icon-remove"></i> Désactiver</a>';
            else
          echo 'Non &nbsp;&nbsp;<a href="'.urlControl('val=1&setTarifAsso='.$compte->get_id()).'" class="btn"><i class="icon-ok"></i> Activer</a>'; ?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>


    <div id="activer-compte" class="modal hide">
      <div class="modal-header"><h3>Activer un sous-compte</h3></div>
      <div class="modal-body">
        <form method="post" action="<?php echo urlControl('reactiverCompte') ?>" class="form-horizontal">
        <div class="control-group">
          <label class="control-label" for="quota">Quota</label>
          <div class="controls">
            <input type="text" id="quota" name="quota" placeholder="Laisser vide pour pas de quota"/>
          </div>
        </div>
        <div class="control-group">
          <input type="hidden" id="reactiver-compte-id" name="compte" value="" />
          <label class="control-label" for="pass-new1">Nouveau mot de passe</label>
          <div class="controls">
            <input type="password" id="pass-new1" name="pass"/>
          </div>
        </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Activer</button>
              </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Nouveau sous-compte -->

    <div id="new-compte" class="modal hide">
      <div class="modal-header"><h3>Créer un compte</h3></div>
      <div class="modal-body">
        <form method="post" action="<?php echo urlControl('creerCompte') ?>" class="form-horizontal">
        <div class="control-group">
          <input type="hidden" id="new-compte-asso-id" name="asso" value="<?php echo $asso->get_id() ?>" />
          <label class="control-label" for="nom-compte">Nom du compte</label>
          <div class="controls">
            <input type="text" id="nom-compte" name="nom"/>
            </div>
          </div>
        <div class="control-group">
          <label class="control-label" for="quota">Quota</label>
          <div class="controls">
            <input type="text" id="quota" name="quota" placeholder="Laisser vide pour pas de quota"/>
            </div>
          </div>
        <div class="control-group">
          <label class="control-label" for="pass-new">Mot de passe</label>
          <div class="controls">
            <input type="password" id="pass-new" name="pass"/>
            </div>
          </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Enregistrer</button>
              </div>
            </div>
        </form>
      </div>
    </div>