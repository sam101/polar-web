<?php
$titrePage = 'Suivi des factures Assos en cours';
use_datatables();
addFooter('
	<script>
	$(document).ready(function() {
		$(".BoutonEncaisserCheque").each(function(){
			$(this).click(function(){
				var cheque;
				var banque;
				cheque = prompt(\'Veuillez saisir le numéro du chèque\');
				if(cheque){
					banque = prompt(\'Veuillez saisir la banque\');
					if(banque) document.location = \''.$racine.$module.'/'.$section.'_control?EncaisserCheque=\'+$(this).attr("facture")+\'&Cheque=\'+cheque+\'&Banque=\'+banque;
				}
			});
		});
	});
	</script>');
require_once('inc/header.php');
?>
<h1>Les factures Assos en cours</h1>
<form method="post" action="<?php echo $racine.$module.'/'.$section.'_control?relancer'; ?>">
<table class="datatables table table-bordered table-striped">
<thead><tr><th>Date</th><th>Asso</th><th>Montant</th><th>Voir la facture</th><th>Encaisser par chèque</th><th>Encaisser par virement</th><th>Relancer</th></tr></thead>
<tbody>
<?php
	$sql='SELECT polar_assos_factures.*, DATE(polar_assos_factures.Date) AS DateJ, polar_assos.Asso as NomAsso FROM polar_assos_factures INNER JOIN polar_assos WHERE polar_assos.ID=polar_assos_factures.Asso AND Encaisse=0';
	$req=query($sql);
	$total = 0;
	while($data=mysql_fetch_array($req)){
		$total += $data['Montant'];
		echo '<tr><td>'.$data['DateJ'].'</td><td>'.$data['NomAsso'].'</td><td>'.$data['Montant'].'</td>
		<td style="text-align:center;width:30px;"><a href="'.$racine.$module.'/'.$section.'_control?ConsulterFacture='.$data['ID'].'" title="Consulter la facture !"><img src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></a></td>
		<td style="text-align:center;width:30px;"><img title="Encaisser par chèque !" class="BoutonEncaisserCheque" facture="'.$data['ID'].'" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></td>
		<td style="text-align:center;width:30px;"><a href="'.$racine.$module.'/'.$section.'_control?EncaisserVirement='.$data['ID'].'" title="Encaisser par virement !"><img src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></a></td>
    <td style="text-align:center;width:30px;"><input type="checkbox" name="relancer_'.$data['Asso'].'"/></td></tr>';
	}
?>
</tbody>
</table>
<p style="clear:right;"><b>Montant total restant dû : <?php echo $total; ?>€.</b><br/><br/>
Message de relance :<br/>
<textarea name="message-relance">Bonjour,
Nous n'avons pas encore reçu le règlement de la facture de votre association pour ce semestre,
vous pouvez la régler par chèque à déposer au Polar ou par virement, merci de nous prévenir par mail dans ce cas.

Le Trésorier du Polar</textarea><br/>
Relancer les assos sélectionnées : <input type="submit" value="Valider" class="btn"/></p>
</form>
<?php
require_once('inc/footer.php');
?>
