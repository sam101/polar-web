<?php
require_once("modules/assos/_fonctions.php");
if(isset($_GET['ConsulterFacture'])){
	$facture = FactureAsso::getById(intval($_GET['ConsulterFacture']));
	$chemin = $facture->genererPDF(true);
	header("Location: ".urlTo($chemin));
}
else if(isset($_GET['EncaisserVirement'])){
	$facture = FactureAsso::getById(intval($_GET['EncaisserVirement']));
    $facture->encaisserVirement();
    redirectOk();
}
else if(isset($_GET['EncaisserCheque'])){
	$facture = FactureAsso::getById(intval($_GET['EncaisserCheque']));
	$facture->encaisserCheque(intval($_GET['Cheque']), $_GET['Banque']);
    redirectOk();
} else if (isset($_GET['relancer'])) {
  $req = query("SELECT * FROM polar_assos");

  while ($data = mysql_fetch_assoc($req)) {
    if (isset($_POST['relancer_'.$data['ID']])) {
      $message = nl2br(htmlentities($_POST['message-relance'], ENT_COMPAT, 'UTF-8'));
      sendmail('polar@assos.utc.fr', 'Le Polar', array($data['MailAsso'], $data['MailPresident'], $data['MailTresorier']), '[Le Polar] Rappel de facture asso', $message);
    }
  }

  redirectOk('', 'Un mail de relance à été envoyé pour toutes les factures sélectionnées !');
}
?>
