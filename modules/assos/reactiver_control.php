<?php
// Si le formulaire est entier
if((isset($_POST['ep-nom'], $_POST['ep-president'], $_POST['ep-presidentmail'], $_POST['ep-presidenttel'], $_POST['ep-tresorier'], $_POST['ep-tresoriermail'], $_POST['ep-tresoriertel'], $_POST['ep-password'], $_POST['ep-password2'], $_POST['ep-mail']))) {
	// Sécurisation des données entrantes
	$nom = intval($_POST['ep-nom']);
    $mail = mysqlSecureText($_POST['ep-mail']);
	$president = mysqlSecureText($_POST['ep-president']);
	$presidentmail = mysqlSecureText($_POST['ep-presidentmail']);
	$presidenttel = mysqlSecureText(str_replace(array('.', ' '), array('', ''), $_POST['ep-presidenttel']));
	$tresorier = mysqlSecureText($_POST['ep-tresorier']);
	$tresoriermail = mysqlSecureText($_POST['ep-tresoriermail']);
	$tresoriertel = mysqlSecureText(str_replace(array('.', ' '), array('', ''), $_POST['ep-tresoriertel']));

    try {
        $asso = Asso::getById($nom);
        $asso->reactiver($mail, $president, $presidentmail, $presidenttel,
                         $tresorier, $tresoriermail, $tresoriertel,
                         $_POST['ep-password'], $_POST['ep-password2']);

		echo '<script>alert("Votre demande a été enregistrée avec succès !");document.location.href="'."$racine$module/$section".'";</script>';
	}
    catch(UnknowObject $e) {
        saveFormData($_POST);
        ajouterErreur("L'asso sélectionnée n'existe pas");
		header("Location: $racine$module/$section");
    }
	catch(ReactivationException $e) { // S'il y a des erreurs de saisie
		saveFormData($_POST);
        ajouterErreur($e->getMessage());
		header("Location: $racine$module/$section");
	}
} // Fin si le formulaire est entier
?>
