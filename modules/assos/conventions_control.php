<?php
if((isset($_POST['ep-asso'], $_POST['ep-signataire'], $_POST['ep-but'], $_POST['ep-polarsengage'], $_POST['ep-assosengage']))) {
	$asso=intval($_POST['ep-asso']);
	$but=mysqlSecureText($_POST['ep-but']);
	$type_signataire=intval($_POST['ep-signataire']);
	$polarsengage=mysqlSecureText($_POST['ep-polarsengage']);
	$assosengage=mysqlSecureText($_POST['ep-assosengage']);
	if($type_signataire==0){
		$req = query("SELECT Asso, President FROM polar_assos WHERE id=$asso");
		$signataire='Pr&eacute;sident';
	}
	else{
		$req = query("SELECT Asso, Tresorier FROM polar_assos WHERE id=$asso");
		$signataire='Tr&eacute;sorier';
	}

	$data = mysql_fetch_row($req);
	$nom_asso = mysql_real_escape_string($data[0]);
	$nom_signataire = mysql_real_escape_string($data[1]);

	query("INSERT INTO polar_assos_conventions (
		Asso,
		Date,
		But,
		PolarSengage,
		AssoSengage,
		TypeSignataire,
		NomSignataire
	) VALUES(
		'$nom_asso',
		NOW(),
		'$but',
		'$polarsengage',
		'$assosengage',
		'$signataire',
		'$nom_signataire'
	)");

	header("Location: $racine$module/$section");
}
else if(isset($_GET['pdf']) && isset($_GET['id'])){
	$convention = intval($_GET['id']);
	$sql = query("SELECT * FROM polar_assos_conventions WHERE id=$convention");
	$donnees = mysql_fetch_assoc($sql);

	$sql=query("SELECT Valeur FROM polar_parametres WHERE Nom='prez'");
	$prez = mysql_fetch_assoc($sql);

	require_once('inc/tcpdf.php');
	$pdf=new PolarPDF("Convention de partenariat n° ".$convention);
	$pdf->SetFontSize(12);
	$output = '<p align="right">Compiègne, le '.date("d/m/Y").'</p>';

	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);


	$html = '<p>Le Polar s&rsquo;engage :<br />'.nl2br($donnees['PolarSengage']).'</p>';
	$html .= '<p>'.$donnees['Asso'].' s&rsquo;engage :<br />'.nl2br($donnees['AssoSengage']).'</p>';
	$html .= '<p>Imprimé le '.date("d/m/Y").' en deux exemplaires originaux.</p>';

	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$pdf->Signature("prez", 'Pour '.$donnees['Asso'].',<br />'.$donnees['NomSignataire'].', '.$donnees['TypeSignataire']);
	$file = 'Documents/Convention_'.$donnees['Asso'].'_'.$donnees['ID'].'.pdf';
	$pdf->Output($file, 'F');
	header('Location: '.$racine.$file);
}
?>
