<?php

require('_fonctions.php');
if(isset($_GET['Valider'])) {
	$asso = Asso::getById(intval($_GET['Valider']));

    if ($asso != NULL)
        $asso->activer();

	header("Location: $racine$module/$section");
}
elseif(isset($_GET['refuser'])){
	$asso = Asso::getById(intval($_GET['Valider']));

	$message = nl2br(htmlentities($_POST['ri-message'], ENT_COMPAT, 'UTF-8'));

	if(get_magic_quotes_gpc() == 1)
	 	$message = stripslashes($message);

    if ($asso != NULL)
        $asso->refuser($mesage);

	header("Location: $racine$module/$section");
}
elseif(isset($_GET['facturer'])){
	$id_asso = intval($_POST['id-asso']);
	facturerCompteAsso($id_asso);
	header("Location: $racine$module/$section?id_asso=$id_asso");
}
elseif(isset($_GET['global'])){
	$assos = Asso::select('*');

	foreach($assos as $asso) {
		if(isset($_POST['chk_'.$asso->get_id()]))
			$asso->facturer();
		if(isset($_POST['chk_cloturer_'.$asso->get_id()]))
			$asso->cloturer();
	}
	echo "<script>alert(\"Tous les comptes sélectionnés ont été facturés et clôturés avec succès !\");
	document.location.href=\"$racine$module/$section?actifs\";</script>";
}
elseif(isset($_GET['defaut_paiement'])){
	if((isset($_POST['ep-id'], $_POST['ep-bloque'], $_POST['ep-raison']))) {
		$bloque = intval($_POST['ep-bloque']);
		$raison = htmlentities($_POST['ep-raison'], ENT_COMPAT, 'UTF-8');
		$asso = Asso::getById(intval($_POST['ep-id']));
		if($bloque==1){
			$msg = "<html><body><p>";
			$msg .= stripslashes(nl2br($raison));
			$msg .= "</p></body></html>";
            $asso->bloquer($msg);
		}
		else {
            $asso->debloquer();
		}
	}
	header("Location: $racine$module/$section?id_asso=$id");
}
elseif(isset($_GET['suspendre'])){
	if((isset($_POST['ep-id']))) {
        $asso = Asso::getById(intval($_POST['ep-id']));
        if ($asso != NULL)
            $asso->cloturer($_POST['ep-raison']);
	}
	header("Location: $racine$module/$section?actifs");
}
// gestion des sous comptes
else if (isset($_GET['desactiverCompte'])) {
    try {
        $compte = CompteAsso::getById(intval($_GET['desactiverCompte']));
    } catch(UnknownObject $e) {
        redirectWithErrors('actifs', 'Le sous-compte '.intval($_GET['desactiverCompte']).' n\'existe pas');
    }
    $compte->cloturer();
    redirectOk('id_asso='.$compte->get_object_id('Asso'));
}
else if (isset($_GET['setTarifAsso'], $_GET['val'])) {
    try {
        $compte = CompteAsso::getById(intval($_GET['setTarifAsso']));
    } catch(UnknownObject $e) {
        redirectWithErrors('actifs', 'Le sous-compte '.intval($_GET['setTarifAsso']).' n\'existe pas');
    }
    $compte->TarifAsso = $_GET['val'];
    $compte->save();

    redirectOk('id_asso='.$compte->get_object_id('Asso'));
}
else if (isset($_GET['creerCompte'], $_POST['asso'],
               $_POST['nom'], $_POST['quota'], $_POST['pass'])) {
    try {
        $asso = Asso::getById(intval($_POST['asso']));
    } catch (UnknownObject $e) {
        redirectWithErrors('actifs', 'Le compte '.intval($_POST['asso']).' n\'existe pas');
    }
    $compte = $asso->nouveauCompte($_POST['nom']);
    $quota = intval($_POST['quota']);
    $compte->Quota = $quota ? $quota : NULL;
    $compte->activer($_POST['pass']);

    redirectOk('id_asso='.intval($_POST['asso']));
}
else if (isset($_GET['reactiverCompte'], $_POST['compte'],
               $_POST['quota'], $_POST['pass'])) {
    try {
        $compte = CompteAsso::getById(intval($_POST['compte']));
    } catch (UnknownObject $e) {
        redirectWithErrors('actifs', 'Le sous-compte '.intval($_POST['compte']).' n\'existe pas');
    }
    $quota = intval($_POST['quota']);
    $compte->Quota = $quota ? $quota : NULL;
    $compte->activer($_POST['pass']);

    redirectOk('id_asso='.$compte->get_object_id('Asso'));
}
// édition ajax des infos de l'asso
// renvoie du json (donc pas de redirection si erreur)
else if (isset($_GET['liveEdit'], $_POST['id'], $_POST['value'])) {
    header('Content-Type: application/json');
    $champ = $_POST['id'];
    $val = $_POST['value'];
    try {
        $asso = Asso::getById(intval($_GET['liveEdit']));
    } catch (UnknownObject $e) {
        die('{error : "Identifiant d\'asso invalide"}');
    }

    $error = NULL;
    switch ($champ) {
    case 'MailAsso':
    case 'MailPresident':
    case 'MailTresorier':
        // vérification de l'email
        if (!verifMail($val))
            $error = 'Adresse mail invalide';
        break;
    case 'TelTresorier':
    case 'TelPresident':
        // vérification du téléphone
        if (!preg_match('/[0-9]{10}/', $val))
            $error = 'Numéro de téléphone mal formaté';
        break;
    default:
        $error = 'Champ invalide';
        break;
    }

    if (is_null($error)) {
        $asso->__set($champ, $val);
        $asso->save();
        echo '{ok: '.$asso->get_id().'}';
    } else {
        echo json_encode(array('error' => $error));
    }
}

?>
