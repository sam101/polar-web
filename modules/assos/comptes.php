<?php
$titrePage = "Comptes assos";
use_datatables();
// Afficher les détails d'une assos
if(isset($_GET['id_asso'])){
	require_once('inc/header.php');
    require('modules/assos/_details.php');
	require_once('inc/footer.php');
}
// Comptes assos actifs
else if(isset($_GET['actifs'])){
	require_once('inc/header.php');
?>
	<h1>Liste des comptes assos</h1>
         <?php afficherErreurs(); ?>
    <p>
	  <a href="<?php echo "$racine$module/$section"; ?>">Voir les demandes en attente</a>
	</p>
	  <?php
	    echo '
			<form method="post" action="'.$racine.$module.'/'.$section.'_control?global">
				<table class="datatables table table-bordered table-striped">
				<thead>
				<tr>
					<th>Asso</th>
					<th>Président</th>
					<th>Trésorier</th>
					<th>En-cours</th>
					<th>Facturer</th>
					<th>Cloturer</th>
				</tr>
				</thead>
				<tbody>';

    $compte = CompteAsso::select('CompteAsso.ID')->where('Asso.ID = CompteAsso.Asso');
    $ventes = OldVente::select('SUM(PrixFacture)')->where('Finalise = 0 AND Facture = 0 AND OldVente.Asso IN ('.$compte->get_sql().')')->addSubquery($compte);
    $assos = Asso::select('Asso.*')->select($ventes, 'EnCours')
             ->where('Asso.Etat != \'Supprime\' AND pa.Etat != \'AttenteActivation\'')
        ->order('Asso.Etat, Asso.Asso ASC')->execute(true);

	if(count($assos) > 0){
		foreach($assos as $asso) {
			if($asso->Etat != 'Actif')
				$encours = $asso->Etat;
			else
				$encours = round($asso->EnCours, 2).'&euro;';
        echo '
		    <tr>
				<td><a href=\''.$racine.$module.'/'.$section.'?id_asso='.$asso->get_id().'\'>',$asso->Asso,'</a></td>
				<td>',$asso->President,'</td>
				<td>',$asso->Tresorier,'</td>
				<td>'.$encours.'</td>
				<td><input type="checkbox" name="chk_'.$asso->get_id().'"/></td>
				<td><input type="checkbox" name="chk_cloturer_'.$asso->get_id().'"/></td>
		    </tr>';
      }

      echo '	</tbody>
			</table>
		<br /><br /><br />Effectuer les actions groupées (pas plus de 10 factures à la fois !) : <input type="submit" value="Valider" class="btn" />
		</form>';
      }
      else
        echo '<span style="font-style:italic;">Il n\'y a aucun compte actif.</span></p>';

	require('inc/footer.php');
}
// Comptes assos en attente
else {
	use_jquery_ui();
	addFooter('<script>
		function refuser(nom,id){
			$("#ri-name").val(nom);
			$("#ri-id").val(id);
			$("#dialog").modam("show");
		}

		</script>');

	require('inc/header.php');
?>
<h1>Liste des demandes d'ouverture de compte en attente</h1>
	<p>
		<a href="<?php echo "$racine$module/$section"; ?>?actifs">Voir les comptes actifs</a>
	</p>
<?php
	echo '<table class="table table-bordered table-striped table-condensed">
	<tr>
	<th>Assos</th>
	<th>Président</th>
	<th>Trésorier</th>
	<th>V</th>
	<th>R</th>
	</tr>';

        $facture = FactureAsso::select('COUNT(*)')->where('Encaisse = 0 AND Asso = Asso.ID');
        $assos = Asso::select('Asso.*')->select($facture, 'EnCours')
            ->where('Etat = ?', 'AttenteActivation')
            ->order('Asso ASC')->execute(true);

	if(count($assos) > 0){
		foreach ($assos as $asso) {
			echo '
				<tr>
				<td><a href=\''.$racine.$module.'/'.$section.'?id_asso='.$asso->get_id().'\'>',$asso->Asso,'</a></td>
				<td>',$asso->President,'</td>
				<td>',$asso->Tresorier,'</td>';
			echo '<td>';
//			if($asso->EnCours > 0)
//				echo '<a href="'.$racine.$module.'/'.$section.'_control?Valider='.$asso->get_id().'" onclick="return confirm(\'Cette association a une facture non payée. Continuer ?\');" title="Valider ce compte !"><img src="'.$racine.'styles/0/icones/ajouter.png" alt="-" /></a>';
//			else
			if($asso->EnCours == 0)
				echo '<a href="'.$racine.$module.'/'.$section.'_control?Valider='.$asso->get_id().'" title="Valider ce compte !"><i class="icon-check"></i></a>';

			echo '</td>';
			echo '<td><a href="javascript:refuser(\''.addslashes($asso->Asso).'\','.$asso->get_id().')" title="Refuser ce compte !"><i class="icon-ban-circle"></i></a></td>
				</tr>';
		}
	}
	else
		echo '<tr><td colspan="5">Il n\'y a aucune demande d\'ouverture de compte pour le moment.</td></tr>';

	echo '</table>';
?>
<div id="dialog" class="modal hide">
  <div class="modal-header">
    <h3>Refuser un compte assos</h3>
  </div>
    <div class="modal-body">
	<p>Ecrire le mail qui sera envoyé au demandeur pour expliciter le problème de création de compte. Laisser vide pour ne pas envoyer de message.</p>
	<p><strong>Asso concernée :</strong> <input type="text" readonly="readonly" id="ri-name" /></p>

	<form id="form-refuser" method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control?refuser">
		<input type="hidden" name="ri-id" id="ri-id" value="" />
		<table style="margin-top:5px;">
			<tr>
				<th style="vertical-align:top; padding-top:3px;">Message : </th>
				<td>
					<textarea name="ri-message" style="width:400px;height: 200px;" class="in-texte">Bonjour,
						La création de votre compte Assos pose problème. Merci de passer au local FE008 afin de rencontrer un membre du bureau.
						L'équipe du polar.</textarea>
				</td>
            </tr>
            <tr>
				<td></td>
				<td style="text-align:right;">
					<input class="btn" type="submit" value="Refuser le compte !" />
				</td>
			</tr>
		</table>
	</form>
  </div>
</div>
<?php
	require('inc/footer.php');
}
?>
