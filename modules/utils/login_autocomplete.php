<?php
if(isset($_GET['q'])) {
	$queryString = $_GET['q'];

	if(strlen($queryString) >0) {
    try {
      $users = $ginger->findPersonne($queryString);
      if(!isset($users->error)) {
        foreach ($users as $result) {
          echo $result->login.'|'.$result->nom.'|'.$result->prenom."\n";
        }
      } else {
        echo 'ERROR: There was a problem with the query.';
      }
    } catch (ApiException $e) {
			echo 'ERROR: There was a problem with the query : '.mysqlSecureText($e->getMessage());
    }
	}
}
else if (isset($_REQUEST['l'])) {
  $login = $_REQUEST['l'];
  
  try {
    $user = $ginger->getUser($login);
    echo json_encode($user);
  } catch (ApiException $e) {
    echo "{'erreur': '".mysqlSecureText($e->getMessage())."'}";
  }
}
?>
