<?php
header("Content-type: image/png");

// On crée une image de 100x20 pixels
$img = imagecreatefrompng('styles/0/icones/verif-base.png');

if($img && isset($_SESSION['captcha'])){
	// Couleur du texte
	$noir = imagecolorallocate($img, 0, 0, 0);

	// On écrit le texte / ImageString($im, $taille_texte, $coordonn&eacute;es_x, $coordonn&eacute;es_y, $texte, $couleur_texte);
	imagestring($img, 4, 15, 0, $_SESSION['captcha'], $noir);

	// On génère l'image
	imagepng($img);
}

else {
	exit();
}
?>
