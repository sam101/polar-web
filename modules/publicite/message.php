<?php
$titrePage = "Déposer un message sur le PolarScreen";
require("inc/header.php");
?>
<h1>Envoyer un texte sur l'écran publicitaire</h1>
<p>Utilisez ce formulaire pour envoyer un message sur le PolarScreen. Celui-ci défilera en bas de l'écran dans le Polar. Attention : maximum 140 caractères !</p>
<?php
afficherErreurs();
if(isset($_SESSION['ps-success'])){
	echo '<p style="color:green;">Votre message a été proposé avec succès ! Il sera visible après validation.</p>';
	unset($_SESSION['ps-success']);
}
$nom = getFormData('ep-nom');
if(empty($nom))
	$nom = $_SESSION['con-prenom'];
?>
<form name="formulaire" method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
  <table>
	<tr>
	<tr>
	  <td style="font-size:12px;">Pseudo :</td>
	  <td><input type="text" maxlength="25" class="in-text" name="ep-nom" value="<?php echo $nom; ?>" /></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Message :</td>
	  <td><input maxlength="140" type="text" class="in-text" size="80" name="ep-message" value="<?php echo getFormData('ep-message'); ?>"  /></td>
	</tr>
	<tr>
	  <td colspan="2" style="text-align:right;"><input type="submit" value="Envoyer la demande !" class="btn" /></td>
	</tr>
  </table>
</form>
<?php
require("inc/footer.php");
?>


