<?php
$titrePage = "Envoyer une image sur le PolarScreen";
require("inc/header.php");
?>
<h1>Envoyer une image sur le PolarScreen</h1>
<p>Cette page permet d'envoyer une image sur le PolarScreen. <b>Cette fonction est réservée aux associations détentrices d'un compte au Polar.</b></p>
<p>Votre fichier doit être au format JPEG et peser moins de 5Mo.</p>
<p>La taille optimale du fichier est de 1900x1200 pixels. Attention : le texte défile sur 100 pixels en bas ! Il faut donc éviter d'écrire dans cette zone, sinon on ne verra rien !</p>
<p></p>
<?php
afficherErreurs();

if(isset($_SESSION['ps-imagesuccess'])){
	echo '<p style="color:green;">Votre image sera affichée après validation par Le Polar !</p>';
	unset($_SESSION['ps-imagesuccess']);
}
?>
<form method="post" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section; ?>_control">
	<table>
		<tr>
			<td>Asso :</td>
			<td>
<?php
$req = query("SELECT ID, Asso FROM polar_assos WHERE Etat = 'Actif' ORDER BY Asso ASC");
echo '<select name="ep-asso">';
while($data = mysql_fetch_assoc($req)){
	$selected = ($data['ID'] == getFormData('ep-asso')) ? ' selected="selected"' : '';
	echo '<option value="'.$data['ID'].'"'.$selected.'>'.$data['Asso'].'</option>';
}
echo '</select>';
?>
			</td>
		</tr>
		<tr>
			<td>Mot de passe :</td>
			<td>
				<input type="password" class="in-text" name="ep-password" />
			</td>
		</tr>
		<tr>
			<td>Fichier à publier :</td>
			<td>
				<input type="file" name="ep-fichier" />
			</td>
		</tr>
		<tr>
			<td colspan="2"  style="text-align:right;">
				<input type="submit" value="Envoyer la demande !" class="btn" />
			</td>
		</tr>
	</table>
</form>
<?php
require("inc/footer.php");
?>
