<?php
if(isset($_POST['ep-nom'], $_POST['ep-message'])) {
	$nom = mysqlSecureText($_POST['ep-nom']);
	$mail = $_SESSION['con-email'];
	$message = mysqlSecureText($_POST['ep-message']);

	if(empty($pseudo))
		ajouterErreur('le pseudo doit être rempli');

	if(empty($message))
		ajouterErreur("le message n'a pas été précisé");
		
	if(strlen($message) > 140)
		ajouterErreur("le message est trop long");

	if(hasErreurs())
		saveFormData($_POST);
	else {
		query("INSERT INTO polar_screen (
			Date,
			Mail,
			Nom,
			Message,
			Actif
		)VALUES(
			NOW(),
			'$mail',
			'$nom',
			'$message',
			0
		)");
		
		sendMail('polar@assos.utc.fr', 'Le Polar', array('polar-communication@assos.utc.fr'), 'Nouveau message sur le PolarScreen', "Une nouveau message vient d'être proposé sur le PolarScreen. Merci de passer le modérer !");
		
		$_SESSION['ps-success'] = true;
	}
}
header("Location: $racine$module/$section");
?>
