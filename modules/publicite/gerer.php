<?php
$titrePage = "Validation des publications du PolarScreen";
require("inc/header.php");
?>
<h1>Les demandes de publication de messages</h1>
<?php

echo '<table class="datatables table table-bordered table-striped table-condensed">
<tr>
<th>Date</th>
<th>Nom</th>
<th>Message</th>
<th>V</th>
<th>S</th>
</tr>';

$req = query('SELECT * FROM polar_screen ORDER BY ID DESC');

while($donnees = mysql_fetch_assoc($req)){
	echo '
		<tr>
			<td>',$donnees['Date'],'</td>
			<td><a href="mailto:',$donnees['Mail'],'" title="">',$donnees['Nom'],'</a></td>
			<td>',$donnees['Message'],'</td>
		<td>';
	if($donnees['Actif'] == 0)
		echo '<a href="'.$racine.$module.'/'.$section.'_control?ValiderTexte='.$donnees['ID'].'"><img title="Validez ce message !" src="'.$racine.'styles/0/icones/ajouter.png" alt="-" /></a>';

	echo '</td>
			<td><a href="'.$racine.$module.'/'.$section.'_control?SupprimerTexte='.$donnees['ID'].'"><img title="Supprimez ce message !" src="'.$racine.'styles/0/icones/croix.png" alt="-" /></td>
	</tr>';
}

echo '</table>';
?>
<br />
<h1>Les demandes de publication de photos</h1>
<?php

echo '<table class="datatables table table-bordered table-striped table-condensed">
<tr>
<th>Date</th>
<th>Nom</th>
<th>Photo</th>
<th>V</th>
<th>R</th>
</tr>';

$req = query('SELECT * FROM polar_screen_photos ORDER BY ID DESC');

while($donnees = mysql_fetch_assoc($req)) {
  echo '
<tr>
<td>',$donnees['Date'],'</td>
<td>',$donnees['Nom'],'</td>
<td><img width="240" src="'.$racine.'/PolarScreen/Photos/'.$donnees['Adresse'].'" /></td>
<td>';
if($donnees['Actif'] == 0)
		echo '<a href="'.$racine.$module.'/'.$section.'_control?ValiderImage='.$donnees['ID'].'"><img title="Validez ce message !" src="'.$racine.'styles/0/icones/ajouter.png" alt="-" /></a>';

	echo '</td>
			<td><a href="'.$racine.$module.'/'.$section.'_control?SupprimerImage='.$donnees['ID'].'"><img title="Supprimez ce message !" src="'.$racine.'styles/0/icones/croix.png" alt="-" /></td>
	</tr>';
}

echo '</table>';
require("inc/footer.php");
?>


