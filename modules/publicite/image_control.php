<?php
if(isset($_POST['ep-asso'], $_POST['ep-password'], $_FILES['ep-fichier'])){
	$asso = intval($_POST['ep-asso']);
	$mdp = $_POST['ep-password'];

	if(empty($mdp))
		ajouterErreur("saisissez le mot de passe de l'association");
	else {
        $checkAssoMdp = checkMotDePasse($asso, $mdp, 'polar_assos');
		if(!$checkAssoMdp)
			ajouterErreur("mot de passe incorrect");
	}

	if(!hasErreurs()){
		$req = query("SELECT Asso FROM polar_assos WHERE ID=$asso");

		$assos = mysql_fetch_assoc($req);

		if(is_file($_FILES['ep-fichier']['tmp_name'])){
			$morceaux = explode('.', $_FILES['ep-fichier']['name']);
			$extension = strtolower($morceaux[sizeof($morceaux) - 1]);

			//On vérifie si l'extension est autorisée
			if($extension != 'jpg' && $extension != 'jpeg' && exif_imagetype($_FILES['ep-fichier']['tmp_name']) != IMAGETYPE_JPEG)
				  ajouterErreur("ce type de fichier n'est pas accepté");
			else {
				$nom_fichier = md5($assos['Asso']).'_'.date('dmY-Hmi').'.'.$extension;

				if(move_uploaded_file($_FILES['ep-fichier']['tmp_name'], 'PolarScreen/Photos/'.$nom_fichier)){
					query("INSERT INTO polar_screen_photos (
						Date,
						Nom,
						Adresse,
						Actif
					) VALUES(
						NOW(),
						'".mysqlSecureText($assos['Asso'])."',
						'$nom_fichier',
						0
					)");

					sendMail('polar@assos.utc.fr', 'Le Polar', array('polar-communication@assos.utc.fr'), 'Nouvelle image sur le PolarScreen', "Une nouvelle image vient d'être proposée sur le PolarScreen. Merci de passer la modérer !");

					$_SESSION['ps-imagesuccess'] = true;
				}
				else
					ajouterErreur('le chargement du fichier a échoué pour une raison inconnue');
			}
		}
	}
	if(!hasErreurs())
		saveFormData($_POST);

	header("Location: $racine$module/$section");
}
?>
