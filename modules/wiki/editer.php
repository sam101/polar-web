<?php
if(is_numeric($_GET['Page'])){
	$wpage = PageWiki::getPage($_GET['Page']);

	require("parseRaw.inc.php");
	require("inc/header.php");
?>
	<form method="post" action="<?php echo urlControl() ?>" class="form-inline">

	<label for="ep-titre">Titre : </label>
    <input id="ep-titre" type="text" name="titre" value="<?php echo $wpage->Titre ?>" class="input-large"/>
	<label for="ep-categorie">Catégorie : </label>
	<select name="categorie" id="ep-categorie">
<?php
	foreach (CategorieWiki::select() as $categorie){
		echo '<option value="'.$categorie->get_id().'"';
		if($categorie->get_id() == $wpage->get_object_id('Categorie'))
			echo ' selected="selected"';
		echo '>'.$categorie->Nom.'</option>';
	}
?>
	</select>
    <a href="<?php echo urlTo('wiki', 'upload') ?>" class="btn" target="_blank">Ajouter un fichier</a>

	<input type="hidden" name="Enregistrer" value="<?php echo $wpage->Page ?>" />
	<input type="submit" value="Enregistrer" class="btn btn-primary" />

	<p><textarea style="width:95%;height:40em;" name="texte"><?php echo $wpage->Contenu ?></textarea></p>


	</form>
<?php
	require("inc/footer.php");
}

// Liste des pages
else {
	header("Location: $racine$module");
}
?>
