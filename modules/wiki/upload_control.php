<?php

$modele_anchor = '<a href="%s" target="_blank">%s</a>';
$modele_image = '<img src="%s" width="%d" height="%d"/>';
$dossier_upload = "upload/wiki/";
$mimes_images = array('image/gif', 'image/jpeg', 'image/png', 'image/tiff');

$max_width = 700;

// on va chercher le mime-type du fichier
// si c'est un fichier image on va générer le code pour l'intégrer au wiki
// sinon on génère juste le code de la balise anchor pour accèder au fichier
// bien vérifier que l'execution de fichier php soit désactivé dans le dossier où on stocke les fichiers
// (php_flag engine off dans le .htaccess)

if (isset($_FILES['ep-fichier'])) {
    if(!empty($_FILES['ep-fichier']['tmp_name']) && is_file($_FILES['ep-fichier']['tmp_name'])) {

        $nom = $_FILES['ep-fichier']['name'];
        $parts = explode('.', $nom);
        $extension = $parts[count($parts) - 1];
        unset($parts[count($parts) - 1]);
        $cible = $dossier_upload . rand(0, 9999) . '_' . implode('_', $parts) . '.' . $extension;
        
        if(move_uploaded_file($_FILES['ep-fichier']['tmp_name'], $cible)) {
            $info = new finfo(FILEINFO_MIME_TYPE);
            $type = $info->file($cible);
            Log::info("Wiki : fichier $cible, mime-type $type");

            if (in_array($type, $mimes_images)) {
                // on va déterminer la taille de l'image
                list($width, $height) = getimagesize($cible);
                if ($width > $max_width) {
                    $width = $max_width;
                    $height = (int) ($max_width * $height / $width);
                }

                $url = urlTo($cible);
                $img = sprintf($modele_image, $url, $width, $height);
                $_SESSION['code_integration'] = sprintf($modele_anchor, $url, $img);
            } else {
                $_SESSION['code_integration'] = sprintf($modele_anchor,
                                                        urlTo($cible),
                                                        ' -*- Nom du lien -*- ');
            }
            redirectOk();
        }
    }
}
