<?php
if(is_numeric($_GET['Page'])){
	$wpage = PageWiki::getPage($_GET['Page']);

	require("parseRaw.inc.php");
	require("inc/header.php");

	echo '<h1>'.$wpage->Titre.'</h1>';
	echo '<p>'.simpleText("\n".$wpage->Contenu).'</p>';
    echo '<h1></h1><p><i>Version du '.$wpage->Date.' par '.$wpage->Auteur->Prenom.'.</i> <a href="'.urlTo(NULL, 'editer', 'Page='.$wpage->Page).'" title="Éditer '.$page->Titre.'">Éditer cette page</a></p>';
	require("inc/footer.php");
}

// Liste des pages
else {
	require("inc/header.php");

	echo "<h1>PolarWiki</h1>
	<p>Ce wiki permet l'enregistrement d'informations concernant le Polar.</p>";

	$pages = PageWiki::select('PageWiki.*, CategorieWiki.Nom as NomCategorie')
        ->join('CategorieWiki', 'PageWiki.Categorie = CategorieWiki.ID')
        ->groupBy('PageWiki.Page')
        ->order('PageWiki.Categorie, PageWiki.Titre, PageWiki.Date DESC');

	$lastCategorie = "";
	foreach ($pages as $wpage){
		if($lastCategorie != $wpage->NomCategorie){
			$lastCategorie = $wpage->NomCategorie;
			echo "<h2>$lastCategorie</h2>";
		}
		echo '<p style="margin-left: 15px;"><a href="'.urlSection('Page='.$wpage->Page).'" title="Voir '.$wpage->Titre.'">'.$wpage->Titre.'</a></p>';
	}

	require("inc/footer.php");
}
?>
