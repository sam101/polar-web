<?php

$titrePage = 'Ajouter un fichier au wiki';

require('inc/header.php');
?>
<h1>Ajouter un fichier au wiki</h1>

<?php
afficherErreurs();

if (isset($_SESSION['code_integration'])) {
?>
    <p class="alert alert-success">
        Le document a bien été envoyé sur le serveur, voici le code
        pour l'intégrer au wiki :</p>
    <p class="well">
        <?php echo htmlspecialchars($_SESSION['code_integration']); ?>
    </p>
    <p><a class="btn" href="<?php echo urlSection() ?>">Envoyer un
        nouveau fichier</a></p>
<?php
    unset($_SESSION['code_integration']);
} else {
?>
<?php afficherErreurs() ?>
<div class="well">
    <p>
    Utilisez cette page pour ajouter un fichier au wiki.<br/>
    Si c'est une image le site génèrera le code html à ajouter au wiki pour afficher l'image.<br/>
    Si c'est un fichier le site fournira le lien à insérer dans le wiki pour y accèder.
    </p>
</div>

<div>
  <form method="POST" enctype="multipart/form-data" action="<?php echo urlControl() ?>" class="form-inline">
    <label class="control-label" for="ep-fichier">Fichier à envoyer</label>
    <input type="file" id="ep-fichier" name="ep-fichier" />
    <input type="submit" value="Envoyer" class="btn btn-primary" />
  </form>
</div>

<?php
}
?>

<?php
require('inc/footer.php');
?>
