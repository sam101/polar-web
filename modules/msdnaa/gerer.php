<?php
	$titrePage = 'Gestion des permanences';
	require_once('inc/header.php');
?>
<h1>Modification du système MSDNAA</h1>
<?php
  if(isset($_SESSION['mm-erreur'])) {
	echo '<p>
  <img src="',$racine,'styles/',$design,'/icones/exclamation.png" alt="" /> &nbsp;<span class="erreur-script">',htmlentities($_SESSION['mm-erreur']),'</span></p>';
	unset($_SESSION['mm-erreur']);
  }

  $req = mysql_query('SELECT (SELECT Valeur FROM polar_parametres WHERE Nom LIKE \'max_msdnaa\') AS max_login_msdnaa, (SELECT Valeur FROM polar_parametres WHERE Nom LIKE \'mail_msdnaa\') AS mail_msdnaa');
  $donnees = mysql_fetch_assoc($req);
?>

<form id="formulaire-news" method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
  <table>
	<tr>
	  <td style="font-size:12px;">Max login à enregistrer :</td>
	  <td><input type="text" name="mm-nblogin" value="<?php echo $donnees['max_login_msdnaa']; ?>"/></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Destinataire UTC :</td>
	  <td><input type="text" name="mm-mail" value="<?php echo $donnees['mail_msdnaa']; ?>"/></td>
	</tr>
	<tr>
	  <td></td>
	  <td><input class="btn" name="ModifierMSDNAA" type="submit" value="Modifier !" /></td>
	</tr>
  </table>
</form>
<?php
	require_once('inc/footer.php');
?>

