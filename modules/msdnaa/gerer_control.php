<?php
if(isset($_POST['ModifierMSDNAA'])){
	$nb_login = intval($_POST['mm-nblogin']);
    $mail = htmlentities($_POST['mm-mail']);
    if(!verifMail($mail)) {
      $_SESSION['mm-erreur'] = 'L\'adresse mail du destinataire UTC n\'est pas valide.';
    }
    else if($nb_login < 10) {
      $_SESSION['mm-erreur'] = 'Le nombre maximum de login doit au moins être égal à 10 : faut pas spammer le personnel de l\'UTC !';
    }
    else {
      query('UPDATE polar_parametres SET Valeur='.$nb_login.' WHERE Nom LIKE \'max_msdnaa\'');
      query('UPDATE polar_parametres SET Valeur=\''.$mail.'\' WHERE Nom LIKE \'mail_msdnaa\'');
	  $_SESSION['mm-erreur'] = 'Les informations ont été modifiées avec succès.';
    }
    header('Location: '.$racine.$module.'/'.$section);
}
?>
