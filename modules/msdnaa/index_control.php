<?php
if(isset($_POST['al-login'])){
	function envoyerMSDNAA(){
		$req = query("SELECT COUNT(*) as nbr, (SELECT Valeur FROM polar_parametres WHERE Nom LIKE 'max_msdnaa') AS max, (SELECT Valeur FROM polar_parametres WHERE Nom LIKE 'mail_msdnaa') AS mail FROM polar_msdnaa WHERE envoye=0");
		$data = mysql_fetch_assoc($req);

		if($data['nbr'] >= $data['max']) {
			$reqmails = query("SELECT login FROM polar_msdnaa WHERE envoye=0");

			$message = "Bonjour,<br />
				Veuillez trouver ci-dessous la liste des &eacute;tudiants demandant un acc&egrave;s &agrave; MSDNAA.<br/>";
			while($etus = mysql_fetch_assoc($reqmails)) {
				$message .= $etus['login'].'@etu.utc.fr<br />';
				sendMail('polar@assos.utc.fr', 'Le Polar', array($etus['login'].'@etu.utc.fr'), '[Le Polar] Inscription MSDNAA', "Bonjour,<br/>Ta demande de compte MSDNAA vient d'être transmise à l'UTC.<br/>Tu recevras tes indentifiants sur ta boite UTC d'ici une semaine.<br/><br/>Pour toute demande de support merci de contacter 5000@utc.fr<br/>br/>Cordialement,<br/>L'équipe du Polar.");
			}

			$message .= "<br /><br />Cordialement,<br />L'&eacute;quipe du Polar</p>";

			sendMail('polar@assos.utc.fr', 'Le Polar', array($data['mail'], 'polar@assos.utc.fr'), '[Le Polar] Liste des demandes MSDNAA', $message);
			query("UPDATE polar_msdnaa SET envoye=1 WHERE envoye=0");
		}
	}

    $login = mysqlSecureText($_POST['al-login']);

	if(!verifLoginUTC($login)){
		ajouterErreur("Ce login n'est pas reconnu par le système.");
		header("Location: $racine$module/$section");
		die();
	}

	$req = query("SELECT 1 FROM `polar_msdnaa` WHERE login LIKE '$login'");
	$data = mysql_fetch_assoc($req);
	if(mysql_num_rows($req) == 0) {
		query("INSERT INTO polar_msdnaa (
				login,
				date
			) VALUES(
				'$login',
				NOW()
			)");
		envoyerMSDNAA();
		echo '<script>
			alert("Votre demande a été prise en compte et sera traitée sous 2 semaines.");
			document.location = \''.$racine.'\';
			</script>';
	}
	else{
		ajouterErreur("Vous avez déjà demandé un accès MSDNAA. Veuillez lire la FAQ pour savoir comment retrouver votre mot de passse");
		header("Location: $racine$module/$section");
		die();
	}
}
?>
