<?php
$titrePage = 'Demande d\'acc&egrave;s &agrave; MSDNAA';
require_once('inc/header.php');
?>
<h1>MSDNAA n'existe plus !</h1>
<p>
Depuis septembre les téléchargements gratuits Microsoft sont disponibles depuis l'ENT rubrique Partenariat -> Microsoft Campus.<br/>
Le Polar n'assure pas le support de cette fonction, merci de contacter le 5000 en cas de problème.
</p>

<?php
require_once('inc/footer.php');
?>

