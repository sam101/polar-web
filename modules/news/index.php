<?php
require_once('inc/smcode.php');

// Récupération des news
$liste_news = News::select('News.*')->select('Utilisateur.Prenom')
    ->join('Utilisateur', 'News.auteur = Utilisateur.ID')
    ->where('Etat LIKE \'online\'')
    ->order('News.ID DESC');

include("inc/header.php");
?>
<div class="row">

<div class="span3">
<h4>Accès rapide</h4>
<?php
    function big_button($module, $section, $texte, $moar='') {
    return '<a href="'.urlTo($module, $section).'" class="btn btn-large polar-side-btn '.$moar.'">'.$texte.'</a>';
    }
    echo big_button('commander', 'posters', 'Commander un poster');
    echo big_button('annales', 'commander', 'Commander des annales');
    echo big_button('billetterie', 'preinscription', 'S\'inscrire à un évènement');
    echo big_button('commander', 'badges', 'Commander des badges');
    echo big_button('videoprojecteur', '', 'Réserver un vidéoprojecteur');
    echo "<hr/>\n";
    echo big_button('assos', 'reactiver', 'Réactiver un compte asso');
    if ($user->is_logged() && $user->Staff && caisseAutorisee()) {
        echo "<hr/>\n";
        echo big_button('caisse', 'index', 'Accèder à la caisse', 'btn-primary');
    }
?>
</div>

<div class="span9">

<?php afficherErreurs(); ?>

<div class="well">
  <h1>Le Polar <small style="font-style: italic;">Imprime ta propre histoire</small></h1>
  <p><?php
    echo smCode(nl2br(Parametre::get('DebutPresentation')));
  ?><br/>
  <a href="<?php echo urlTo('static', 'presentation') ?>" title="Apprenez-en encore plus sur le polar et ses activités !">En savoir plus...</a></p>
</div>

<div class="separateur-titre"></div>

<h1>Les news du polar</h1>
  <?php 

	$i = 0;
	foreach($liste_news as $news){
      echo '<div class="well well-small">';
	  echo '<h2>'.unhtmlentities($news->Titre).'</h2>
      <p>'.unhtmlentities(smCode($news->News)).'</p>';
      echo '<p class="signature">Par '.unhtmlentities($news->Prenom).', le '.$news->Date.'</p>';

	  $i++;
      echo '</div>';
	}

	if($i == 0)
	  echo '<p style="font-style:italic;">Il n\'y a aucune news pour le moment.</p>';

?>
</div>
</div> <!-- row -->
<?php
	include("inc/footer.php");

?>


