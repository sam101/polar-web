<?php
if(isset($_GET['ModifierNews'])){
	$id = intval($_GET['ModifierNews']);
    if ($id)
        $news = News::getById($id);
    else
        $news = new News(array('Date'=>raw('NOW()'), 'Auteur'=>$user));

    $titre = $_POST['gan-titre'];
    $texte = $_POST['gan-news'];
	$status = $_POST['status'];

	if(empty($titre))
		ajouterErreur("le titre est vide");
	if(empty($texte))
		ajouterErreur("la news est vide (!)");
	if(empty($status))
		$status = 'online';

	if(!hasErreurs()){
        $news->Titre = $titre;
        $news->News = $texte;
        $news->Etat = $status;
        $news->save();
		redirectOk();
	}
	else {
		saveFormData($_POST);
        redirectWithErrors('ModifierNews='.$news);
	}
}
else if(isset($_GET['SupprimerNews'])){
	$id = intval($_GET['SupprimerNews']);
    query("DELETE FROM polar_news WHERE id=$id");
	redirectOk();
}
?>
