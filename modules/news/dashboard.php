<?php
$titrePage='Dashboard du bureau';
require("inc/header.php");

function doItem($quer, $singulier, $pluriel) {
  if ($pluriel == 1)
    $pluriel = $singulier;

  $req = query($quer);
  $data = mysql_fetch_array($req);
  echo "<li>\n";
  if ($data[0] == 0 OR $data[0] == 1)
    echo $data[0] . $singulier;
  else
    echo $data[0] . $pluriel;
  echo "</li>\n";
}
?>
<!--
	- factures assos en cours-->
<h1>Commandes</h1>
<ul>
<?php
doItem("SELECT COUNT(*) FROM `polar_commandes` pc
  INNER JOIN `polar_commandes_types` pct ON pc.Type = pct.ID
  WHERE pc.DatePrete IS NULL AND pc.Termine = 0 AND pct.Nom = 'Poster'",
       " poster en attente",
       " posters en attente");
doItem("SELECT COUNT(*) FROM `polar_commandes` pc
  INNER JOIN `polar_commandes_types` pct ON pc.Type = pct.ID
  WHERE pc.DatePrete IS NULL AND pc.Termine = 0 AND pct.Nom = 'Badge'",
       " badge en attente",
       " badges en attente");
doItem("SELECT COUNT(*) FROM `polar_commandes` pc
  INNER JOIN `polar_commandes_types` pct ON pc.Type = pct.ID
  WHERE pc.DatePrete IS NULL AND pc.Termine = 0 AND pct.Nom = 'Plastification'",
       " plastification en attente",
       " plastifications en attente");
doItem("SELECT COALESCE(SUM(pap.pages_nbrPages), 0) FROM polar_commandes pc
	INNER JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
	INNER JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
	INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
	WHERE pc.DatePrete IS NULL
    AND pc.DatePaiement IS NOT NULL
    AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types
    AND pct.Nom = 'Annale'",
       " page d'annale à imprimer",
       " pages d'annales à imprimer");
?>
</ul>

<h1>Fric</h1>
<ul>
<?php
// Récupération du dernier id de vente, avec lequel on va travailler
$req = query("SELECT MAX(IDVente) FROM polar_caisse_ventes");
$data = mysql_fetch_row($req);
$dernierID = $data[0];
if(is_null($dernierID))
  $dernierID = 0;

$montant = Caisse::getSolde();

doItem("SELECT ROUND((CASE WHEN SUM(PrixFacture) IS NULL THEN $montant ELSE (SUM(PrixFacture)+$montant) END), 2)
	FROM polar_caisse_ventes
	WHERE MoyenPaiement = 'especes' AND IDVente <= $dernierID",
       "€ dans la caisse", 1);
doItem("SELECT ROUND((CASE WHEN SUM(PrixFacture) IS NULL THEN 0 ELSE SUM(PrixFacture) END), 2)
	FROM polar_caisse_ventes
	WHERE MoyenPaiement = 'cb' AND IDVente <= $dernierID",
       "€ de CB non collecté",
       "€ de CB non collectés");
doItem("SELECT ROUND((CASE WHEN SUM(PrixFacture) IS NULL THEN 0 ELSE SUM(PrixFacture) END), 2)
	FROM polar_caisse_ventes
  WHERE MoyenPaiement = 'moneo' AND IDVente <= $dernierID",
       "€ de monéo non collecté",
       "€ de monéo non collectés");
doItem("SELECT ROUND(SUM(Billets) + SUM(Pieces), 2) FROM polar_caisse_coffre",
       "€ dans le coffre", 1);
doItem("SELECT ROUND(SUM(Montant), 2) FROM polar_caisse_cheques WHERE PEC=1 AND Ordre='Le Polar' AND DateEncaissement IS NULL",
       "€ de chèques à déposer", 1);
?>
<!--
	<li>
		500&euro; dans la caisse
	</li>
-->
</ul>

<h1>Assos</h1>
<ul>
<?php
doItem("SELECT ROUND(SUM(Montant), 2) FROM `polar_assos_factures` WHERE Encaisse = 0",
       "€ de facture en attente", 1);
doItem("SELECT COUNT(*) FROM `polar_assos` WHERE Etat = 'AttenteActivation'",
       " compte en attente",
       " comptes en attente");
?>
</ul>

<h1>Clients</h1>
<ul>
<?php
doItem("SELECT COUNT(*) FROM `polar_tickets` WHERE status = 'new'",
       " ticket non répondu",
       " tickets non répondus");
doItem("SELECT COUNT(*) FROM `polar_tickets` WHERE status != 'closed'",
       " ticket ouvert",
       " tickets ouverts");
?>
</ul>
<?php
require("inc/footer.php");
?>


