<?php
$titrePage = 'Gestion des news';

include("inc/header.php");

if(isset($_GET['ModifierNews'])){
	$titre = getFormData('gan-titre');
	$contenu = getFormData('gan-news');
	$etat = getFormData('status');

	$id = intval($_GET['ModifierNews']);
	// Si on fait une édition (pas création)
	if($id > 0){
        $news = News::getById($id);

        $titre = $news->Titre;
        $contenu = $news->News;
        $status = $news->Etat;
	}

	// État par défaut
	if(empty($status))
		$status = 'online';
	?>
	<h1>Édition d'une news</h1>
	<a href="<?php echo urlSection(); ?>" title="Gérer les news">Retour</a>
	<form id="formulaire-news" method="post" action="<?php echo urlControl('ModifierNews='.(isset($news) ? $news : '')); ?>">
	<?php afficherErreurs(); ?>
	<p>Titre : <input class="in-texte" type="text" name="gan-titre" value="<?php echo $titre; ?>" /></p>
	<p>Contenu :<br /><textarea class="in-texte" name="gan-news" style="width:500px;height:200px;"><?php echo $contenu; ?></textarea></p>
	<p>État : <label><input type="radio" name="status" value="online" <?php if($status == 'online') echo 'checked="checked "'; ?>/>En ligne</label>
		<label><input type="radio" name="status" value="offline" <?php if($status == 'offline') echo 'checked="checked "'; ?>/>Hors ligne</label>
	</p>
	<p><input class="btn" type="submit" value="Enregistrer" /></p>
    </form>
	<?
}
else {
?>
<h1>Liste des news</h1>
<a href="<?php echo urlSection('ModifierNews'); ?>" title="Créer une nouvelle news">Nouvelle news</a>
<?php
echo '<table class="datatables table table-bordered table-striped table-condensed">
  <thead><tr>
	<th>Titre</th>
	<th>Publié le</th>
	<th>Par</th>
	<th>État</th>
	<th>M</th>
	<th>S</th>
  </tr></thead><tbody>';

$req = News::select('News.*, Utilisateur.Prenom')
->join('Utilisateur', 'Utilisateur.ID = News.Auteur')
->order('Date DESC');

	foreach($req as $news) {
	  echo '
  <tr>
	<td>',$news->Titre,'</td>
	<td>',$news->Date,'</td>
	<td>',$news->Prenom,'</td>
	<td>'.$news->Etat.'</td>
	<td><img title="Modifiez cette news !" onclick="location.href= \'',$racine,$module,'/',$section,'?ModifierNews=',$news,'\';" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/crayon.png" alt="-" /></td>
	<td><img title="Supprimez cette news !" onclick="if(confirm(\'Voulez-vous vraiment supprimer définitivement cette news ?\')) location.href= \'',$racine,$module,'/',$section,'_control?SupprimerNews=',$news,'\';" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/croix.png" alt="-" /></td>
  </tr>';
	}

	echo '</tbody></table>';
}
include("inc/footer.php");
?>
