<?php

require_once('inc/file_uploader.php');
require_once('_consts.php');

function getWallpaperVotesTable($uploadVoteDirectory) {
    global $racine, $module, $section;

    $user = intval($_SESSION['con-id']);
    $req = query("SELECT t1.ID AS ID,
t1.ImageFileName AS ImageFileName,
t1.ThumbnailFileName AS ThumbnailFileName,
COALESCE(t2.NbVotes, 0) AS NbVotes,
t3.UserID IS NOT NULL AS UserVoted,
t4.Prenom AS Prenom, t4.Nom as Nom,
t1.Date AS Date
FROM polar_wallpaper t1
LEFT JOIN (
SELECT WallpaperID, COUNT(UserId) AS NbVotes FROM polar_wallpaper_votes
GROUP BY WallpaperID
) t2 ON t1.ID = t2.WallpaperID
LEFT JOIN (
SELECT UserID, WallpaperID FROM polar_wallpaper_votes WHERE UserID='$user'
) t3 ON t1.ID = t3.WallpaperID
INNER JOIN polar_utilisateurs t4 ON t1.UserCreatorID = t4.ID
ORDER BY t2.NbVotes DESC, t1.Date ASC");

    $str = '<table id="vote_image_table"><tbody>';
    $str .= '<tr class="vote_image_title"><th>Voter</th><th>Image</th><th>Proposé par</th></tr>';
    while($data = mysql_fetch_assoc($req)) {
        $str .= '<tr><td>';

        if ($data['UserVoted'] == 1) {
            $str .= "<a class='votedButton vote_image_link_button' href='".urlControl("RemoveVote&wId=${data['ID']}")."'>${data['NbVotes']}</a></td>";
        } else {
            $str .= "<a class='notVotedButton vote_image_link_button' href='".urlControl("AddVote&wId=${data['ID']}")."'>${data['NbVotes']}</a></td>";
        }

        $str .= '<td><a href="'.$uploadVoteDirectory.$data['ImageFileName'].'" class="highslide" onclick="return hs.expand(this)"><img src="'.$uploadVoteDirectory.$data['ThumbnailFileName'].'" /></a></td>';
        $str .= '<td>'.$data['Prenom'].' '.$data['Nom'].'</td>';
        $str .= '</tr>';
    }
    $str .= '</tbody></table>';
    return $str;
}


function getWallpaperStorageTable($uploadStorageDirectory) {

    $req = query("SELECT t1.ID AS ID, t1.Week AS Week,
t1.ImageFileName AS ImageFileName,
t1.ThumbnailFileName AS ThumbnailFileName,
t2.Prenom AS Prenom, t2.Nom as Nom
FROM polar_wallpaper_storage t1
INNER JOIN polar_utilisateurs t2 ON t1.UserCreatorID = t2.ID
ORDER BY t1.Date ASC");

    $str = '<div id="storage_image_table_div"><table class="storage_image_table"><tbody>';

    $header = '<tr class="storage_image_header">';
    $value = '<tr class="storage_image_value">';
    $i = 0;
    while($data = mysql_fetch_assoc($req)) {
        $header .= '<th>Semaine '.$data['Week'].'</th>';

        $value .= '<td>';
        $value .= '<a href="'.$uploadStorageDirectory.$data['ImageFileName'].'" class="highslide" onclick="return hs.expand(this)"><img src="'.$uploadStorageDirectory.$data['ThumbnailFileName'].'" /></a><br/>';

        $value .= 'Proposé par<br/>'.$data['Prenom'].' '.$data['Nom'];
        $value .= '</td>';

        $i++;
        if (($i % 3) === 0) {
            $header .= '</tr>';
            $value .= '</tr>';

            $str .= $header.$value.'</tbody></table>';
            $str .= '<table class="storage_image_table"><tbody>';

            $header = '<tr class="storage_image_header">';
            $value = '<tr class="storage_image_value">';
        }
    }
    $header .= '</tr>';
    $value .= '</tr>';

    $str .= $header;
    $str .= $value;
    $str .= '</tbody></table></div>';
    return $str;
}



$titrePage = 'Voter pour le prochain fond d\'&eacute;cran';


$controlFile = urlControl();

$file_uploader = new FileUploader($controlFile. '?Upload', $ALLOWED_EXTENSION);
$image_viewer = new ImageDocumentViewer($controlFile. '?NewWallpaper', $file_uploader,
    $SCREEN_IMAGE_SIZE, $PREVIEW_IMAGE_WIDTH);

$file_uploader->registerDocumentViewer('jpeg', $image_viewer);
$file_uploader->registerDocumentViewer('jpg', $image_viewer);
$file_uploader->registerDocumentViewer('png', $image_viewer);

//use_jquery_ui();
addHeaders('<link href="'.$racine.'lib/highslide/highslide.css" rel="stylesheet" type="text/css"/>');
addFooter('
    <script language="javascript" type="text/javascript" src="'.$racine.'lib/highslide/highslide.packed.js"></script>
    <script type="text/javascript">
        hs.graphicsDir = \''.$racine.'lib/highslide/graphics/\';
        hs.outlineType = \'rounded-white\';
        hs.showCredits = false;
        hs.align = \'center\';
    </script>');

addFooter($file_uploader->getJs());
addHeaders($file_uploader->getCss());
addFooter($image_viewer->getJs());
addHeaders($image_viewer->getCss());

addHeaders(<<<EOT

    <style type="text/css">
        .qq-upload-button {
            margin: 0 auto;
        }

        #image_viewer_resize_mode {
            float:left;
            margin-left:200px;
            margin-top:20px;
        }

        #image_viewer_color_div {
            float:right;
            margin-right:200px;
            margin-top:10px;
        }
        #image_viewer_color_span {
            float:left;
            margin-top:10px;
            margin-right:5px;
        }
        #image_viewer_color_selector {
            float:right;
        }

        #image_viewer_submit {
            clear: both;
            margin-top: 50px;
        }

        #vote_image_div, #storage_image_div {
            width:100%;
        }

        #vote_image_table, .storage_image_table {
            text-align:center;
            width:100%;
            border:1px solid black;
            border-collapse: collapse;
        }

        .storage_image_table {
            margin-top: -1px;
        }

        #vote_image_table tr, #vote_image_table td,  #vote_image_table th,
        .storage_image_table tr, .storage_image_table td, .storage_image_table th {
            border:1px solid black;
        }

        .vote_image_title, .storage_image_header {
            background-color:#ccc;
        }

        .vote_image_link_button:link {text-decoration: none !important;}
        .vote_image_link_button:visited {text-decoration: none !important;}
        .vote_image_link_button:active {text-decoration: none !important;}
        .vote_image_link_button:hover {text-decoration: none !important;}

        .vote_image_link_button {
            position:relative;
            width:30px;
            color:#000 !important;
            font-style:normal !important;
            display:block;
            text-decoration:none;
            margin:0 auto;
            border-radius:5px;
            background:#cb3b27;
            text-align:center;
            padding:5px 5px;
            -webkit-transition: all 0.1s;
            -moz-transition: all 0.1s;
            transition: all 0.1s;
        }

        .votedButton {
            color:#FFF !important;
            background:#009608;
            border:solid 1px #004f04;
            -webkit-box-shadow: 0px 4px 0px #004f04;
            -moz-box-shadow: 0px 4px 0px #004f04;
            box-shadow: 0px 4px 0px #004f04;
        }

        .notVotedButton {
            background:#d9d9d9;
            border:solid 1px #525252;
            -webkit-box-shadow: 0px 4px 0px #525252;
            -moz-box-shadow: 0px 4px 0px #525252;
            box-shadow: 0px 4px 0px #525252;
        }

        .vote_image_link_button:active {
            position:relative;
            top:2px;
        }

        .votedButton:active {
            -webkit-box-shadow: 0px 2px 0px #004f04;
            -moz-box-shadow: 0px 2px 0px #004f04;
            box-shadow: 0px 2px 0px #004f04;
        }

        .notVotedButton:active {
            -webkit-box-shadow: 0px 2px 0px #525252;
            -moz-box-shadow: 0px 2px 0px #525252;
            box-shadow: 0px 2px 0px #525252;
        }

        #vote_information {
            text-align:justify;
        }

        #vote_send_link_div {
            text-align:center;
            margin-top:10px;
            margin-bottom:10px;
        }

        #vote_cancel_link_div {
            width:100%;
            padding-top:5px;
            padding-bottom:5px;
            background-color:black;
            border:1px solid red;
            text-align:center;
            margin-top:10px;
            margin-bottom:10px;
        }

        #vote_cancel_link {
            color:white !important;
            font-style:normal !important;
            font-size:11pt;
        }


        #vote_send_div {
            display:none;
        }


        #vote_storage_hr {
            margin-top: 20px;
            margin-bottom: 20px;
        }

        #storage_image_div {
            text-align:center;
        }

        #storage_image_table_div {
            margin-top: 11px;
        }

    </style>

EOT
);
addFooter(<<<EOT

    <script type="text/javascript">
    var file_should_clean = true;

    function cleanTmpFile() {
        var controlFile = '${controlFile}?Clean';
        var tmpFileName = $('#image_viewer_file_name').val();
        if(!tmpFileName) {
            return {noFile:true};
        }

        var responseText = $.ajax({
            type: "POST",
            url: controlFile,
            cache: false,
            async: false,
            data: { fileName: tmpFileName },
            timeout: 5000,
        }).responseText;

        //console.log("cleanTmpFile responseText = " + responseText);
        try {
            response = eval("(" + responseText + ")");
        } catch(err){
            response = {};
        }
        return response;
    }

    $(document).ready(function(){
        $(window).bind('beforeunload', function(){
            if (file_should_clean) {
                cleanTmpFile();
            }
        });


        $('#image_viewer_form').submit(function() {
            file_should_clean = false;
            return true;
        });

        $('#vote_send_link').click(function() {
            $('#vote_main_div').hide();
            $('#vote_send_div').show();
        });

        $('#vote_cancel_link').click(function() {
            $('#vote_send_div').hide();
            {$file_uploader->showJsFunct()};
            {$image_viewer->hideJsFunct()};
            $('#vote_main_div').slideDown();
            cleanTmpFile();
        });
    });

    </script>

EOT
);


require('inc/header.php');
?>
<h1>Voter pour le prochain fond d'&eacute;cran</h1>

<?php
afficherErreurs();
?>

<div id="vote_information">
    <p>
    Vous pouvez sur cette page voter pour le fond d'écran de la semaine prochaine. <br/>
    Les fonds d'écran sont choisis par les permanenciers et vous disposez d'un seul vote par semaine. <br/>
    Si vous voulez voir votre fond d'écran favori sur les ordinateurs du Polar, soumettez-le au vote en utilisant le lien ci-dessous :
    </p>
</div>

<div id="vote_send_div">

    <div id="vote_cancel_link_div">
        <a href="javascript:void(0);" id="vote_cancel_link">Annuler l'envoi</a>
    </div>

    <?php
    echo $file_uploader->getDiv();
    echo $image_viewer->getDiv();
    ?>

</div>

<div id="vote_main_div">
    <div id="vote_send_link_div">
        <a href="javascript:void(0);" id="vote_send_link">Envoyer un nouveau fond d'écran</a>
    </div>
    <div id="vote_image_div">
        <?=getWallpaperVotesTable($UPLOAD_VOTE_DIRECTORY);?>
    </div>

    <hr id="vote_storage_hr"/>

    <div id="storage_image_div">
        <span id="storage_image_desc">Retrouvez ci-dessous les derniers fonds d'écran choisis :</span>

        <?=getWallpaperStorageTable($UPLOAD_STORAGE_DIRECTORY);?>
    </div>

</div>

<?php
require('inc/footer.php');
?>
