<?php

require_once('_consts.php');

function getWallpaperVotesTable($uploadVoteDirectory) {
    global $racine, $module, $section;

    $req = query("SELECT t1.ID AS ID,
t1.ImageFileName AS ImageFileName,
t1.ThumbnailFileName AS ThumbnailFileName,
COALESCE(t2.NbVotes, 0) AS NbVotes,
t3.Prenom AS Prenom, t3.Nom as Nom,
t1.Date AS Date
FROM polar_wallpaper t1
LEFT JOIN (
SELECT WallpaperID, COUNT(UserId) AS NbVotes FROM polar_wallpaper_votes
GROUP BY WallpaperID
) t2 ON t1.ID = t2.WallpaperID
INNER JOIN polar_utilisateurs t3 ON t1.UserCreatorID = t3.ID
ORDER BY t2.NbVotes DESC");

    $str = '<table id="delete_vote_id_table"><tbody>';
    $str .= '<tr class="delete_vote_id_title"><th>Supprimer</th><th>ID</th><th>Nb Votes</th><th>Image</th><th>Proposé par</th></tr>';
    while($data = mysql_fetch_assoc($req)) {
        $str .= '<tr><td>';

        $str .= "<a class='delete_button' href='$racine$module/{$section}_control?DeleteVote&id=${data['ID']}' onclick='return deleteVoteWithIdConfirm(".$data['ID'].");'>X</a></td>";

        $str .= '<td>'.$data['ID'].'</td>';
        $str .= '<td>'.$data['NbVotes'].'</td>';
        $str .= '<td><a href="'.$uploadVoteDirectory.$data['ImageFileName'].'" class="highslide" onclick="return hs.expand(this)"><img src="'.$uploadVoteDirectory.$data['ThumbnailFileName'].'" /></a></td>';
        $str .= '<td>'.$data['Prenom'].' '.$data['Nom'].'</td>';
        $str .= '</tr>';
    }
    $str .= '</tbody></table>';
    return $str;
}


function getWallpaperStorageTable($uploadStorageDirectory) {
    global $racine, $module, $section;

    $req = query("SELECT t1.ID AS ID, t1.Week AS Week,
t1.ImageFileName AS ImageFileName,
t1.ThumbnailFileName AS ThumbnailFileName,
t2.Prenom AS Prenom, t2.Nom as Nom
FROM polar_wallpaper_storage t1
INNER JOIN polar_utilisateurs t2 ON t1.UserCreatorID = t2.ID
ORDER BY t1.Date ASC");


    $str = '<table id="delete_storage_id_table"><tbody>';
    $str .= '<tr class="delete_storage_id_title"><th>Supprimer</th><th>ID</th><th>Semaine</th><th>Image</th><th>Proposé par</th></tr>';
    while($data = mysql_fetch_assoc($req)) {
        $str .= '<tr><td>';

        $str .= "<a class='delete_button' href='$racine$module/{$section}_control?DeleteStorage&id=${data['ID']}' onclick='return deleteStorageWithIdConfirm(".$data['ID'].");'>X</a></td>";

        $str .= '<td>'.$data['ID'].'</td>';
        $str .= '<td>'.$data['Week'].'</td>';
        $str .= '<td><a href="'.$uploadStorageDirectory.$data['ImageFileName'].'" class="highslide" onclick="return hs.expand(this)"><img src="'.$uploadStorageDirectory.$data['ThumbnailFileName'].'" /></a></td>';
        $str .= '<td>'.$data['Prenom'].' '.$data['Nom'].'</td>';
        $str .= '</tr>';
    }
    $str .= '</tbody></table>';

    return $str;
}

$titrePage = 'Gérer les fonds d\'&eacute;cran';

addHeaders('<link href="'.$racine.'lib/highslide/highslide.css" rel="stylesheet" type="text/css"/>');
addFooter('
    <script language="javascript" type="text/javascript" src="'.$racine.'lib/highslide/highslide.packed.js"></script>
    <script type="text/javascript">
        hs.graphicsDir = \''.$racine.'lib/highslide/graphics/\';
        hs.outlineType = \'rounded-white\';
        hs.showCredits = false;
        hs.align = \'center\';
    </script>');
addHeaders(<<<EOT

    <style type="text/css">

        #gerer_information {
            text-align:justify;
        }

        .div_hr {
            margin-top: 20px;
            margin-bottom: 20px;
        }

        #delete_vote_id_div p,
        #delete_storage_id_div p{
            font-weight:bold;
            text-align:center;
            margin-bottom:10px;
        }

        #delete_unused_div {
            text-align:center;
        }

        #delete_vote_id_table, #delete_storage_id_table {
            text-align:center;
            width:100%;
            border:1px solid black;
            border-collapse: collapse;
        }

        #delete_vote_id_table tr, #delete_vote_id_table td,  #delete_vote_id_table th,
        #delete_storage_id_table tr, #delete_storage_id_table td, #delete_storage_id_table th {
            border:1px solid black;
        }

        .delete_vote_id_title, .delete_storage_id_title {
            background-color:#ccc;
        }


        .delete_button:link {text-decoration: none !important;}
        .delete_button:visited {text-decoration: none !important;}
        .delete_button:active {text-decoration: none !important;}
        .delete_button:hover {text-decoration: none !important;}

        .delete_button {
            position:relative;
            width:30px;
            font-style:normal !important;
            display:block;
            text-decoration:none;
            margin:0 auto;
            border-radius:5px;
            text-align:center;
            padding:5px 5px;
            -webkit-transition: all 0.1s;
            -moz-transition: all 0.1s;
            transition: all 0.1s;
            background:#db0000;
            color:#FFF !important;
            border:solid 1px #820000;
            -webkit-box-shadow: 0px 4px 0px #820000;
            -moz-box-shadow: 0px 4px 0px #820000;
            box-shadow: 0px 4px 0px #820000;
        }


        .delete_button:active {
            -webkit-box-shadow: 0px 2px 0px #820000;
            -moz-box-shadow: 0px 2px 0px #820000;
            box-shadow: 0px 2px 0px #820000;
            position:relative;
            top:2px;
        }
    </style>

EOT
);
addFooter(<<<EOT

    <script type="text/javascript">

    $(document).ready(function(){
        $('#delete_unused_vote_link').click(function() {
            return confirm("Voulez-vous vraiment supprimer les fichiers non utilisés du dossier de vote ?");
        });

        $('#delete_unused_storage_link').click(function() {
            return confirm("Voulez-vous vraiment supprimer les fichiers non utilisés du dossier de stockage ?");
        });
    });

    function deleteVoteWithIdConfirm(voteId) {
        return confirm("Voulez-vous vraiment supprimer le fond d'écran en vote avec l'id "+ voteId + " ?");
    }

    function deleteStorageWithIdConfirm(storageId) {
        return confirm("Voulez-vous vraiment supprimer le fond d'écran en stockage avec l'id "+ storageId + " ?");
    }

    </script>

EOT
);


require('inc/header.php');

?>

<h1>Gérer les fonds d'écrans</h1>

<div id="gerer_information">
    <p>
    Vous pouvez sur cette page gérer pour les fonds d'écran.
    </p>
</div>

<hr class="div_hr"/>

<div id="delete_unused_div">
    <a href="<?php echo "$racine$module/{$section}_control?DeleteUnusedVote"?>" id="delete_unused_vote_link">Supprimer les fichiers non utilisés du dossier de vote</a>
    <br/>
    <a href="<?php echo "$racine$module/{$section}_control?DeleteUnusedStorage"?>"" id="delete_unused_storage_link">Supprimer les fichiers non utilisés du dossier de stockage</a>
</div>

<div id="delete_vote_id_div">


</div>

<hr class="div_hr"/>

<div id="delete_vote_id_div">
    <p>Vote</p>
    <?=getWallpaperVotesTable($UPLOAD_VOTE_DIRECTORY);?>
</div>

<hr class="div_hr"/>

<div id="delete_storage_id_div">
    <p>Stockage</p>
    <?=getWallpaperStorageTable($UPLOAD_STORAGE_DIRECTORY);?>
</div>

<?php
require('inc/footer.php');
?>
