<?php
/* Ce fichier n'est pas destiné à être appelé directement, mais par
 * le script cron du NAS du polar.
 */

require_once('inc/file_uploader.php');
require_once('_consts.php');

function testFailResult($result) {
    return !isset($result['success']) || $result['success'] != true;
}

class WeeklyScript {
    const MAGIC_NUMBER = 4210146;

    public $uploadVoteDirectory;
    public $uploadStorageDirectory;

    public $cleanOldStorageFile;
    public $maxStorageFile;

    public $filePrefixName;
    public $thumbnailSuffixName;



    public function processQuery() {
        $result = $this->storeAndCleanImages();

        if (testFailResult($result)) {
            $this->outputError(@$result['error']);
        } else {
            $this->outputImageFile($result['imagePath']);
        }
    }

    private function storeAndCleanImages() {

        list($week , $year) = explode('-', @date('W-Y'));

        $req = query("SELECT ImageFileName FROM polar_wallpaper_storage WHERE Week='$week' AND YEAR='$year'");
        if (mysql_num_rows($req) == 0) {
            $result = $this->storeTopVoteImageAndReturnImagePath($week, $year);
        } else {
            $result = $this->getImagePathFromRequest($req);
        }
        if (testFailResult($result)) {
         return $result;
        }
        $imagePath = $result['imagePath'];


        if ($this->cleanOldStorageFile) {
            $result = $this->deleteOldStorageImages();
            if (testFailResult($result)) {
                return $result;
            }

            $result = $this->cleanOldStorageTable();
            if (testFailResult($result)) {
                return $result;
            }
        }

        return array('success'=>true, 'imagePath'=>$imagePath);
    }

    private function storeTopVoteImageAndReturnImagePath($week, $year) {
        $result = $this->retrieveTopVoteImage();
        if (testFailResult($result)) {
            return $result;
        }
        $tab = $result['tab'];

        $voteImageID = $tab['ID'];
        $voteImageName = $tab['ImageFileName'];
        $voteThumbnailName = $tab['ThumbnailFileName'];
        $userCreatorID = $tab['UserCreatorID'];

        $result = $this->cleanVoteImageTable($voteImageID);
        if (testFailResult($result)) {
            return $result;
        }

        $result = $this->deleteOtherVoteImages($voteImageName, $voteThumbnailName);
        if (testFailResult($result)) {
            return $result;
        }

        $result = $this->chooseStoreFileNames();
        if (testFailResult($result)) {
            return $result;
        }
        $storeImageName = $result['storeImageName'];
        $storeThumbnailName = $result['storeThumbnailName'];


        $result = $this->copyTopImageAndReturnImagePath($voteImageName, $voteThumbnailName, $storeImageName, $storeThumbnailName);
        if (testFailResult($result)) {
            return $result;
        }
        $imagePath = $result['imagePath'];

        $result = $this->registerStorageImage($week, $year, $storeImageName, $storeThumbnailName, $userCreatorID);
        if (testFailResult($result)) {
            return $result;
        }

        return array('success'=>true, 'imagePath'=>$imagePath);
    }

    private function retrieveTopVoteImage() {
        $sql = "SELECT t1.ID AS ID,
t1.ImageFileName AS ImageFileName,
t1.ThumbnailFileName AS ThumbnailFileName,
COALESCE(t2.NbVotes, 0) AS NbVotes,
t1.UserCreatorID AS UserCreatorID
FROM polar_wallpaper t1
LEFT JOIN (
	SELECT WallpaperID, COUNT(UserId) AS NbVotes FROM polar_wallpaper_votes
	GROUP BY WallpaperID
) t2 ON t1.ID = t2.WallpaperID
ORDER BY t2.NbVotes DESC, t1.Date ASC
LIMIT 1;";
        $req = query($sql);
        if (mysql_num_rows($req) == 0) {
            return array('error'=>'No top vote image');
        }

        return array('success'=>true, 'tab'=> mysql_fetch_assoc($req));
    }


    private function cleanVoteImageTable($topImageID) {
        $sql1 = "DELETE FROM polar_wallpaper WHERE ID<>'$topImageID'";
        $sql2 = "DELETE FROM polar_wallpaper_votes";
        query($sql1);
        query($sql2);
        return array('success'=>true);
    }

    private function deleteOtherVoteImages($topImageName, $topThumbnailName) {
        $voteDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadVoteDirectory;
        if (!is_dir($voteDirectoryPath)) {
            return array('error'=>'Vote directory path is not accessible');
        }
        $listFiles = scandir($voteDirectoryPath);
        foreach ($listFiles as $file) {
            if (($file === '.') || ($file === '..') ||
                ($file === $topImageName) ||
                ($file === $topThumbnailName)) {
                continue;
            }
            $filePath = $voteDirectoryPath.$file;
            if (!@unlink($filePath)) {
                return array('error'=>"Unable to delete file '$file' from vote directory.");
            }
        }
        return array('success' => true);
    }


    private function deleteOldStorageImages() {
        $storageDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadStorageDirectory;
        $sql = "SELECT t1.ID AS ID, t1.ImageFileName AS ImageFileName, t1.ThumbnailFileName AS ThumbnailFileName
FROM polar_wallpaper_storage t1
LEFT JOIN (
	SELECT ID
	FROM polar_wallpaper_storage
	ORDER BY Date DESC
	LIMIT {$this->maxStorageFile}
) t2
ON t1.ID = t2.ID
WHERE t2.ID is NULL";
        $rep = query($sql);
        while($tab = mysql_fetch_assoc($rep)) {
            $imagePath = $storageDirectoryPath.$tab['ImageFileName'];
            $thumbnailPath = $storageDirectoryPath.$tab['ThumbnailFileName'];

            if (!@unlink($imagePath)) {
                return array('error'=>"Unbale to delete file '{$tab['ImageFileName']}' from storage directory.");
            }

            if (!@unlink($thumbnailPath)) {
                return array('error'=>"Unbale to delete file '{$tab['ThumbnailFileName']}' from storage directory.");
            }
        }
        return array('success'=>true);
    }

    private function cleanOldStorageTable() {
        $sql = "DELETE polar_wallpaper_storage FROM polar_wallpaper_storage
LEFT JOIN (
	SELECT ID
	FROM polar_wallpaper_storage
	ORDER BY Date DESC
	LIMIT {$this->maxStorageFile}
) AS not_to_delete
ON polar_wallpaper_storage.ID = not_to_delete.ID
WHERE not_to_delete.ID is NULL";
        query($sql);
        return array('success'=>true);
    }

    private function chooseStoreFileNames() {
        $storageDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadStorageDirectory;


        $imageFileNameChooser = new FileNameChooserWithPrefixTimeBased($this->filePrefixName);
        $imageFileInfo = $imageFileNameChooser->chooseFileName($storageDirectoryPath, NULL, 'jpg');
        $storeImageName = $imageFileInfo['basename'];

        $thumbnailNameChooser = new FileNameChooserWithoutReplace();
        $thumbnailPreFileName = $imageFileInfo['filename'].'_'.$this->thumbnailSuffixName;
        $thumbnailFileInfo = $thumbnailNameChooser->chooseFileName($storageDirectoryPath, $thumbnailPreFileName, 'jpg');
        $storeThumbnailName = $thumbnailFileInfo['basename'];

        return array('success'=>true, 'storeImageName'=>$storeImageName, 'storeThumbnailName'=>$storeThumbnailName);
    }

    private function copyTopImageAndReturnImagePath($voteImageName, $voteThumbnailName, $storeImageName, $storeThumbnailName) {
        $voteDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadVoteDirectory;
        $storageDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadStorageDirectory;

        $voteFilePath = $voteDirectoryPath.$voteImageName;
        $voteThumbnailPath = $voteDirectoryPath.$voteThumbnailName;


        $storageFilePath = $storageDirectoryPath.$storeImageName;
        $storageThumbnailPath = $storageDirectoryPath.$storeThumbnailName;

        if (file_exists($storageFilePath)) {
            return array('error'=>'Storage image file already exists.');
        }

        if (file_exists($storageThumbnailPath)) {
            return array('error'=>'Storage thumbnail file already exists.');
        }

        if (!mkrndir($storageDirectoryPath)) {
            return array('error'=>'Unable to create storage directory.');
        }

        if (!copy($voteFilePath, $storageFilePath)) {
            return array('error'=>'Unable to copy storage image.');
        }
        if (!copy($voteThumbnailPath, $storageThumbnailPath)) {
            return array('error'=>'Unable to copy storage thumbnail.');
        }

        return array('success'=>true, 'imagePath'=>$storageFilePath);
    }

    private function registerStorageImage($week, $year, $imageName, $thumbnailName, $userCreatorID) {
        $imageName = mysqlSecureText($imageName);
        $thumbnailName = mysqlSecureText($thumbnailName);
        $sql = "INSERT INTO polar_wallpaper_storage (Year, Week,  	ImageFileName, ThumbnailFileName, Date, UserCreatorID) VALUES ($year, $week, '$imageName', '$thumbnailName', NOW(), $userCreatorID)";
        query($sql);
        return array('success' => true);
    }

    private function getImagePathFromRequest($req) {
        $tab = mysql_fetch_assoc($req);
        $storageDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadStorageDirectory;
        return array('success'=>true, 'imagePath'=>$storageDirectoryPath.$tab['ImageFileName']);
    }

    private function outputError($error) {
        header("HTTP/1.0 500 Internal Server Error");
        echo 'Error: '. $error;
    }

    private function outputImageFile($imagePath) {
        $fp = fopen($imagePath, 'rb');

        header("Content-Type: image/jpeg");
        header("Content-Length: " . filesize($imagePath));

        fpassthru($fp);
        fclose($fp);
    }

}

$MAGIC_NUMBER = 4210146;

if (isset($_POST['magic_number']) &&
    (intval($_POST['magic_number']) === WeeklyScript::MAGIC_NUMBER)) {
    $weeklyScript = new WeeklyScript();

    $weeklyScript->uploadVoteDirectory = $UPLOAD_VOTE_DIRECTORY;
    $weeklyScript->uploadStorageDirectory = $UPLOAD_STORAGE_DIRECTORY;

    $weeklyScript->cleanOldStorageFile = $CLEAN_OLD_STORAGE_FILE;
    $weeklyScript->maxStorageFile = $MAX_STORAGE_FILE;

    $weeklyScript->filePrefixName = $FILE_PREFIX;
    $weeklyScript->thumbnailSuffixName = $THUMBNAIL_SUFFIX;

    $weeklyScript->processQuery();
} else {
    require_once('modules/static/404.php');
}

?>
