<?php

$UPLOAD_TMP_DIRECTORY = $racine.'upload/wallpaper/tmp/';
$UPLOAD_VOTE_DIRECTORY = $racine.'upload/wallpaper/vote/';
$UPLOAD_STORAGE_DIRECTORY = $racine.'upload/wallpaper/storage/';

$ALLOWED_EXTENSION = array('jpeg', 'jpg', 'png');

$SIZE_LIMIT = 10 * 1024 * 1024;
$FILE_PREFIX = 'wallpaper';
$THUMBNAIL_SUFFIX = 'min';
$OBSOLOTE_TMP_FILE_LIMIT = 2 * 3600;

$SCREEN_IMAGE_SIZE = array(1440, 900);

$PREVIEW_IMAGE_WIDTH = 600;
$THUMBNAIL_IMAGE_WIDTH = 200;

$CLEAN_OLD_STORAGE_FILE = true;
$MAX_STORAGE_FILE = 3;

?>
