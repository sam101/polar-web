<?php

ini_set('gd.jpeg_ignore_warning', 1);

require_once('inc/file_uploader.php');
require_once('_consts.php');

function testFailResult($result) {
    return !isset($result['success']) || $result['success'] != true;
}

/**
 * Classe contenant l'ensemble des actions du fichier control
 * @author Nicolas Pauss
 */
class WallpaperControl {
    public $uploadTmpDirectory;
    public $uploadVoteDirectory;
    public $uploadStorageDirectory;

    public $allowedExtensions;
    public $sizeLimit;
    public $filePrefixName;
    public $thumbnailSuffixName;

    public $obsoleteTmpFileLimit; //s

    public $screenImageSize;
    public $thumbnailImageWidth;
    //---------------------------------------------
    // Public functions
    //---------------------------------------------

    /**
     * Envoie les fichiers vers le dossier temporaire
     */
    public function uploadFileToTmpDirectory() {
        $uploader = new qqFileUploader($this->allowedExtensions, $this->sizeLimit,
            new FileNameChooserWithPrefixTimeBased($this->filePrefixName));
        return $uploader->handleUpload($this->uploadTmpDirectory);
    }


    /**
     * Supprime un fichier temporaire du dossier temporaire
     */
    public function deleteTmpFile($fileName) {
        if (empty($fileName) || !is_string($fileName)) {
            return array('error' => 'File Url is not valid.');
        }
        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->uploadTmpDirectory.$fileName;

        if (!file_exists($filePath)) {
            return array('error' => "File '$fileName' doesn't exist.");
        }

        if (!@unlink($filePath)) {
            return array('error' => "Unable to delete file '$fileName'. Check that you have the right to delete this file.");
        }

        return array('success' => true);
    }

    /**
     * Supprime les fichiers temporaires obsolètes.
     * @param array $avoidFiles Tableau de fichiers à ne pas supprimer
     */
    public function cleanObsoleteTmpWallpapers(array $avoidFiles = array()) {
        $uploadTmpDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadTmpDirectory;
        if (!is_dir($uploadTmpDirectoryPath)) return true;
        $listFiles = scandir($uploadTmpDirectoryPath);
        $obsoleteFileTime = time() - $this->obsoleteTmpFileLimit;
        foreach ($listFiles as $file) {
            if (in_array($file, $avoidFiles)) {
                continue;
            }
            if (startsWith($file, $this->filePrefixName)) {
                $filePath = $uploadTmpDirectoryPath.$file;
                if (filemtime($filePath) < $obsoleteFileTime) {
                    if(!@unlink($filePath)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    public function addNewWallpaper($fileName) {
        $result = $this->transformNewWallpaper($fileName);
        if (testFailResult($result))
            return $result;
        return $this->registerNewWallpaper($result['voteFileName'], $result['thumbnailFileName']);
    }


    public function addVote($wallpaperId) {
        $user = intval($_SESSION['con-id']);
        $sql = "REPLACE INTO polar_wallpaper_votes (UserID, WallpaperID) VALUES ($user, $wallpaperId)";
        query($sql);
    }

    public function removeVote($wallpaperId) {
        $user = intval($_SESSION['con-id']);
        $sql = "DELETE FROM polar_wallpaper_votes WHERE UserID=$user AND WallpaperID=$wallpaperId";
        query($sql);
    }

    //---------------------------------------------
    // Private functions
    //---------------------------------------------

    /**
     * Transforme le fichier temporaire en fichier final soumis au vote
     */
    private function transformNewWallpaper($fileName) {
        if (empty($fileName) || !is_string($fileName)) {
            return array('error' => 'l\'url du fichier n\'est pas valide');
        }
        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->uploadTmpDirectory.$fileName;
        if (!file_exists($filePath)) {
            return array('error' => 'le fichier n\'existe pas, il a peut-être été supprimé car trop ancien');
        }

        $resizeMode = @$_POST['resize_mode'];
        $bgColor = @json_decode(@$_POST['bg_color'], true);

        $error = '';

        if (empty($resizeMode) ||
            (($resizeMode != 'fit') && ($resizeMode != 'fill') &&
            ($resizeMode != 'stretch') && ($resizeMode != 'center'))) {
            if (!empty($error)) $error .= ', ';
            $error .= 'le type de redimensionnement n\'est pas valide';
        }

        if ($bgColor === false || !isset($bgColor['r']) ||
            !isset($bgColor['b']) || !isset($bgColor['g']) ) {
            if (!empty($error)) $error .= ', ';
            $error .= 'la couleur de l\'arrière plan n\'est pas valide';
        }

        if (!empty($error)) {
            return array('error' => $error);
        }

        $imgSize = getimagesize($filePath);

        $uploadVoteDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadVoteDirectory;
        if (!@mkrndir($uploadVoteDirectoryPath)) {
            return array('error' => 'le dossier accueillant les fonds d\'écran soumis aux votes n\'a pu être créé');
        }
        $path_info = pathinfo($filePath);
        $preFileName = $path_info['filename'];

        $voteFileNameChooser = new FileNameChooserWithPrefixTimeBased($this->filePrefixName);
        $voteFileInfo = $voteFileNameChooser->chooseFileName($uploadVoteDirectoryPath, $preFileName, 'jpg');
        $voteFileName = $voteFileInfo['basename'];

        $thumbnailNameChooser = new FileNameChooserWithoutReplace();
        $thumbnailPreFileName = $voteFileInfo['filename'].'_'.$this->thumbnailSuffixName;
        $thumbnailFileInfo = $thumbnailNameChooser->chooseFileName($uploadVoteDirectoryPath, $thumbnailPreFileName, 'jpg');
        $thumbnailFileName = $thumbnailFileInfo['basename'];

        $voteFilePath = $uploadVoteDirectoryPath.$voteFileName;
        $thumbnailFilePath = $uploadVoteDirectoryPath.$thumbnailFileName;


        if (($this->screenImageSize[0] === $imgSize[0]) &&
            ($this->screenImageSize[1] == $imgSize[1])) {
            if ($imgSize['mime'] == 'image/png') {
                $result = $this->convertPngTmpToJpgVote($filePath, $voteFilePath);
            } else {
                $result = $this->moveTmpToVote($filePath, $voteFilePath);
            }
            $this->createThumbnailImageFromFileImage($voteFilePath, $thumbnailFilePath,
                $this->thumbnailImageWidth, $this->screenImageSize);
        } else {
            $result = $this->createNewImageVote($filePath, $voteFilePath, $thumbnailFilePath,
                $this->screenImageSize, $imgSize, $this->thumbnailImageWidth,
                $bgColor, $resizeMode);
        }

        if(testFailResult($result))
            return $result;

        return array('success' => true, 'voteFileName' => $voteFileName,
            'thumbnailFileName'=>$thumbnailFileName);
    }


    private function moveTmpToVote($tmpFilePath, $voteFilePath) {
        if(!rename($tmpFilePath, $voteFilePath)) {
            return array('error' => 'le nouveau fond d\'écran n\'a pu être déplacé');
        }
        return array('success' => true);
    }

    private function convertPngTmpToJpgVote($tmpFilePath, $voteFilePath) {
        $newImg = @imagecreatefrompng($tmpFilePath);
        $result = imagejpeg($newImg, $voteFilePath, 100);
        imagedestroy($newImg);
        @unlink($tmpFilePath);
        if (!$result) {
            return array('error' => 'la convertion du fichier de png en jpeg n\'a pu être effectuée');
        }
        return array('success' => true);
    }

    private function createNewImageVote($tmpFilePath, $voteFilePath, $thumbnailFilePath,
        $finalImgSize, $origImgSize, $thumbnailWidth, $bgColor, $resizeMode) {
        $r = @intval($bgColor['r']);
        $g = @intval($bgColor['g']);
        $b = @intval($bgColor['b']);
        if ((($r < 0) || ($r > 255)) ||
            (($g < 0) || ($g > 255)) ||
            (($b < 0) || ($b > 255))) {
            return array('error' => 'la couleur de l\'arrière plan n\'est pas valide');
        }

        switch ($origImgSize['mime']) {
            case 'image/jpeg': $isJpeg = true; break;
            case 'image/png': $isJpeg = false; break;
            default: return array('error' => 'le type de l\'image originale n\'est pas valide'); break;
        }


        $newImage = imagecreatetruecolor($finalImgSize[0], $finalImgSize[1]);
        $bgColorImg = imagecolorallocate($newImage, $r, $g, $b);
        imagefill($newImage, 0, 0, $bgColorImg);

        if ($isJpeg) {
            $tmpImg = @imagecreatefromjpeg($tmpFilePath);
        } else {
            $tmpImg = @imagecreatefrompng($tmpFilePath);
        }

        switch ($resizeMode) {
            case 'fit': $this->fitImg($newImage, $tmpImg, $finalImgSize, $origImgSize); break;
            case 'fill': $this->fillImg($newImage, $tmpImg, $finalImgSize, $origImgSize); break;
            case 'stretch': $this->stretchImg($newImage, $tmpImg, $finalImgSize, $origImgSize); break;
            case 'center': $this->centerImg($newImage, $tmpImg, $finalImgSize, $origImgSize); break;
        }

        imagejpeg($newImage, $voteFilePath, 100);

        $this->createThumbnailImageFromGDImage($newImage, $thumbnailFilePath, $thumbnailWidth, $finalImgSize);

        imagedestroy($newImage);
        imagedestroy($tmpImg);

        @unlink($tmpFilePath);
        return array('success' => true);
    }

    private function fitImg($newImg, $tmpImg, $finalImgSize, $tmpImgSize) {
        $newImgRatio = $finalImgSize[0] / $finalImgSize[1];
        $tmpImgRatio = $tmpImgSize[0] / $tmpImgSize[1];
        if ($tmpImgRatio > $newImgRatio) {
            $w = $finalImgSize[0];
            $h = $w / $tmpImgRatio;
        } else {
            $h = $finalImgSize[1];
            $w = $h * $tmpImgRatio;
        }
        $x = ($finalImgSize[0] - $w) / 2.0;
        $y = ($finalImgSize[1] - $h) / 2.0;
        imagecopyresampled($newImg, $tmpImg, $x, $y, 0, 0, $w, $h, $tmpImgSize[0], $tmpImgSize[1]);
    }

    private function fillImg($newImg, $tmpImg, $finalImgSize, $tmpImgSize) {
        $newImgRatio = $finalImgSize[0] / $finalImgSize[1];
        $tmpImgRatio = $tmpImgSize[0] / $tmpImgSize[1];
        if ($tmpImgRatio > $newImgRatio) {
            $h = $tmpImgSize[1];
            $w = $h * $newImgRatio;
        } else {
            $w = $tmpImgSize[0];
            $h = $w / $newImgRatio;
        }
        $x = ($tmpImgSize[0] - $w) / 2.0;
        $y = ($tmpImgSize[1] - $h) / 2.0;
        imagecopyresampled($newImg, $tmpImg, 0, 0, $x, $y, $finalImgSize[0], $finalImgSize[1], $w, $h);
    }

    private function stretchImg($newImg, $tmpImg, $finalImgSize, $tmpImgSize) {
        imagecopyresampled($newImg, $tmpImg, 0, 0, 0, 0, $finalImgSize[0], $finalImgSize[1], $tmpImgSize[0], $tmpImgSize[1]);
    }

    private function centerImg($newImg, $tmpImg, $finalImgSize, $tmpImgSize) {
        if (($tmpImgSize[0] > $finalImgSize[0]) || ($tmpImgSize[1] > $finalImgSize[1])) {
            $this->fitImg($newImg, $tmpImg, $finalImgSize, $tmpImgSize);
            return;
        }
        $x = ($finalImgSize[0] - $tmpImgSize[0]) / 2;
        $y = ($finalImgSize[1] - $tmpImgSize[1]) / 2;
        imagecopyresampled($newImg, $tmpImg, $x, $y, 0, 0, $tmpImgSize[0], $tmpImgSize[1], $tmpImgSize[0], $tmpImgSize[1]);
    }

    private function createThumbnailImageFromFileImage($voteFilePath, $thumbnailFilePath, $thumbnailWidth, $finalImgSize) {
        $voteImage = @imagecreatefromjpeg($voteFilePath);
        $this->createThumbnailImageFromGDImage($voteImage, $thumbnailFilePath, $thumbnailWidth, $finalImgSize);
        imagedestroy($voteImage);
    }

    private function createThumbnailImageFromGDImage($newImage, $thumbnailFilePath, $thumbnailWidth, $finalImgSize) {
        $thumbnailHeight = $thumbnailWidth * $finalImgSize[1] / $finalImgSize[0];
        $thumbnailImage = imagecreatetruecolor($thumbnailWidth, $thumbnailHeight);
        imagecopyresampled($thumbnailImage, $newImage, 0, 0, 0, 0, $thumbnailWidth, $thumbnailHeight, $finalImgSize[0], $finalImgSize[1]);
        imagejpeg($thumbnailImage, $thumbnailFilePath, 100);
        imagedestroy($thumbnailImage);
    }

    private function registerNewWallpaper($voteFileName, $thumbnailFileName) {
        $user = intval($_SESSION['con-id']);
        $voteFileName = mysqlSecureText($voteFileName);
        $thumbnailFileName = mysqlSecureText($thumbnailFileName);
        query("INSERT INTO polar_wallpaper (ImageFileName, ThumbnailFileName, Date, UserCreatorID) VALUES ('$voteFileName', '$thumbnailFileName', NOW(), $user)");
        return array('success' => true);
    }

}


$wallpaperControl = new WallpaperControl();

$wallpaperControl->uploadTmpDirectory = $UPLOAD_TMP_DIRECTORY;
$wallpaperControl->uploadVoteDirectory = $UPLOAD_VOTE_DIRECTORY;
$wallpaperControl->uploadStorageDirectory = $UPLOAD_STORAGE_DIRECTORY;

$wallpaperControl->allowedExtensions = $ALLOWED_EXTENSION;
$wallpaperControl->sizeLimit = $SIZE_LIMIT;
$wallpaperControl->filePrefixName = $FILE_PREFIX;
$wallpaperControl->thumbnailSuffixName = $THUMBNAIL_SUFFIX;

$wallpaperControl->obsoleteTmpFileLimit = $OBSOLOTE_TMP_FILE_LIMIT; //s

$wallpaperControl->screenImageSize = $SCREEN_IMAGE_SIZE;
$wallpaperControl->thumbnailImageWidth = $THUMBNAIL_IMAGE_WIDTH;

if (isset($_GET['Upload'])) {

    $wallpaperControl->cleanObsoleteTmpWallpapers();
    $result = $wallpaperControl->uploadFileToTmpDirectory();
    echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);


} else if (isset($_GET['Clean'])) {

    $fileName = @$_POST['fileName'];
    $wallpaperControl->cleanObsoleteTmpWallpapers();
    $result = $wallpaperControl->deleteTmpFile($fileName);
    echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);


} else if (isset($_GET['NewWallpaper'])) {

    $fileName = @$_POST['fileName'];
    $result = $wallpaperControl->addNewWallpaper($fileName);

    if (testFailResult($result)) {
        ajouterErreur($result['error'].'. Veuillez renouveler l\'opération ou contacter le webmaster');
    }

    $wallpaperControl->cleanObsoleteTmpWallpapers();

    header("Location: $racine$module/$section");
} else if(isset($_GET['AjaxShowVotes'])) {
    echo htmlspecialchars(getWallpaperVotesTable(), ENT_NOQUOTES);
} else if(isset($_GET['AddVote'])) {
    $wallpaperId = intval(@$_GET['wId']);
    $wallpaperControl->addVote($wallpaperId);
    header("Location: $racine$module/$section");
} else if(isset($_GET['RemoveVote'])) {
    $wallpaperId = intval(@$_GET['wId']);
    $wallpaperControl->removeVote($wallpaperId);
    header("Location: $racine$module/$section");
} else {
    header("Location: $racine$module/$section");
}

?>
