<?php

require_once('_consts.php');

function testFailResult($result) {
    return !isset($result['success']) || $result['success'] != true;
}

class GererControl {
    public $uploadVoteDirectory;
    public $uploadStorageDirectory;


    public function deleteUnsusedVote() {
        $voteDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadVoteDirectory;
        return $this->deleteUnsusedFilesFromTable('polar_wallpaper', $voteDirectoryPath);
    }

    public function deleteUnusedStorage() {
        $storageDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadStorageDirectory;
        return $this->deleteUnsusedFilesFromTable('polar_wallpaper_storage', $storageDirectoryPath);
    }


    public function deleteVote($voteId) {
        $voteDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadVoteDirectory;
        return $this->deleteAndUnregisterFileWithId('polar_wallpaper', $voteDirectoryPath, $voteId);
    }

    public function deleteStorage($storageId) {
        $storageDirectoryPath = $_SERVER['DOCUMENT_ROOT'].$this->uploadStorageDirectory;
        return $this->deleteAndUnregisterFileWithId('polar_wallpaper_storage', $storageDirectoryPath, $storageId);
    }


    private function deleteUnsusedFilesFromTable($tableName, $directoryPath) {
        $sql = "SELECT ImageFileName, ThumbnailFileName FROM $tableName";
        $result = query($sql);
        $usedFiles = $this->convertSqlResultToFlattenArray($result);
        return $this->deleteUnsusedFilesFromDirectory($directoryPath, $usedFiles);
    }

    private function convertSqlResultToFlattenArray($sqlResult) {
        $tab = array();
        while ($sqlTab = mysql_fetch_row($sqlResult)) {
            foreach ($sqlTab as $val) {
                $tab[] = $val;
            }
        }
        return $tab;
    }

    private function deleteUnsusedFilesFromDirectory($directoryPath, $usedFiles) {
        if (!is_dir($directoryPath)) {
            return array('error'=>'Directory path is not accessible');
        }
        $listFiles = scandir($directoryPath);
        foreach ($listFiles as $file) {
            if (($file === '.') || ($file === '..') ||
                (in_array($file, $usedFiles))) {
                continue;
            }
            $filePath = $directoryPath.$file;
            if (!@unlink($filePath)) {
                return array('error'=>"Unable to delete file '$file'.");
            }
        }
        return array('success' => true);
    }

    private function deleteAndUnregisterFileWithId($tableName, $directoryPath, $id) {
        $result = $this->deleteFileWithId($tableName, $directoryPath, $id);
        if (testFailResult($result)) {
            return $result;
        }
        return $this->unregisterFileWithId($tableName, $id);
    }

    private function deleteFileWithId($tableName, $directoryPath, $id) {
        $sql = "SELECT ImageFileName, ThumbnailFileName FROM $tableName WHERE ID=$id";
        $resp = query($sql);
        $files = $this->convertSqlResultToFlattenArray($resp);
        return $this->deleteFilesFromDirectory($directoryPath, $files);
    }

    private function deleteFilesFromDirectory($directoryPath, $files) {
        if (!is_dir($directoryPath)) {
            return array('error'=>'Directory path is not accessible');
        }

        foreach($files as $file) {
            $filePath = $directoryPath.$file;
            if (!@unlink($filePath)) {
                return array('error'=>"Unable to delete file '$file'.");
            }
        }
        return array('success' => true);
    }

    private function unregisterFileWithId($tableName, $id) {
        $sql = "DELETE FROM $tableName WHERE ID=$id";
        query($sql);
        if (mysql_affected_rows() == -1) {
            return array('error'=>"Unable to unregister file with id $id from table $tableName.");
        }
        return array('success' => true);
    }

}

$gererControl = new GererControl();

$gererControl->uploadVoteDirectory = $UPLOAD_VOTE_DIRECTORY;
$gererControl->uploadStorageDirectory = $UPLOAD_STORAGE_DIRECTORY;


if (isset($_GET['DeleteUnusedVote'])) {
    $result = $gererControl->deleteUnsusedVote();
    if (testFailResult($result)) {
        ajouterErreur($result['error']);
    }
} else if (isset($_GET['DeleteUnusedStorage'])) {
    $result = $gererControl->deleteUnusedStorage();
    if (testFailResult($result)) {
        ajouterErreur($result['error']);
    }
} else if (isset($_GET['DeleteVote'])) {
    $voteId = intval(@$_GET['id']);
    $result = $gererControl->deleteVote($voteId);
    if (testFailResult($result)) {
        ajouterErreur($result['error']);
    }
} else if (isset($_GET['DeleteStorage'])) {
    $storageId = intval(@$_GET['id']);
    $result = $gererControl->deleteStorage($storageId);
    if (testFailResult($result)) {
        ajouterErreur($result['error']);
    }
}

header("Location: $racine$module/$section");

?>
