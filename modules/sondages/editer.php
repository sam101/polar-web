<?php
if(isset($_GET['id'])) {
	$titrePage = "&Eacute;diter un sondage";
	require("inc/header.php");

	$id = intval($_GET['id']);
	$req = query("SELECT ps.Nom, psq.Question, psq.ID AS qID, psr.Reponse, psr.ID AS rID, psq.Type
		FROM polar_sondages ps
		LEFT JOIN polar_sondages_questions psq ON psq.Sondage = ps.ID
		LEFT JOIN polar_sondages_reponses psr ON psr.Question = psq.ID
		WHERE ps.ID=$id");

	$noQuestion = 0;
	$questionActuelle = -1;
	while($ligne = mysql_fetch_assoc($req)){
		if($noQuestion == 0)
			echo "<h1>$titrePage : ".$ligne['Nom'].'</h1>';

		if($ligne['qID'] != NULL){
			if($ligne['qID'] != $questionActuelle){
				// Affichage de la question
				$questionActuelle = $ligne['qID'];
				$noQuestion++;
				$noReponse = 1;
				echo '</fieldset>';
				echo '<fieldset>';
				echo '<legend><strong>Question '.$noQuestion.' </strong>: '.$ligne['Question'];

				switch($ligne['Type']){
					case 'unique' :
					 	echo ' (Choix unique)';
					break;
					case 'multiple':
				 		echo ' (Choix multiples)';
					break;
					case 'libre':
						echo ' (Champ libre)';
				}

				echo ' <a href="'.$racine.$module.'/'.$section.'_control?SupprimerQuestion='.$ligne['qID'].'&id='.$id.'" title="Supprimer la question"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="X" /></a>';

				echo '</legend>';

				// Formulaire d'ajout d'une réponse
				if($ligne['Type'] == 'unique' || $ligne['Type'] == 'multiple')
					echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control?AjouterReponse='.$ligne['qID'].'&id='.$id.'">
							Ajouter une réponse :
							<input type="text" class="in-text" size="40" name="texte" />
							<input type="submit" value="Ajouter la réponse" class="btn" />
						</form>';
				else
					echo "<p>Les participants s'expriment librement à cette question !</p>";
			}

			if($ligne['Reponse'] != NULL){
				// Affichage des réponses
				echo 'Réponse '.$noReponse.' : '.$ligne['Reponse'];
				echo ' <a href="'.$racine.$module.'/'.$section.'_control?SupprimerReponse='.$ligne['rID'].'&id='.$id.'" title="Supprimer la réponse"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="X" /></a>';
				echo '<br />';
				$noReponse++;
			}
		}
	} // Fin de la boucle sur les lignes

	// Formulaire d'ajout d'une question
	echo '</fieldset>';

		echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control?AjouterQuestion='.$id.'">
				Ajouter une question :
				<input type="text" class="in-text" size="40" name="ep-nouvelle-question" />
				Type ? <select name="ep-type">
                    	<option value="unique">Choix unique</option>
                    	<option value="multiple">Choix multiple</option>
						<option value="libre">Champ libre</option>
                	</select>
				<input type="submit" value="Ajouter la question" class="btn" />
			</form>
			<hr />';
	echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control?ValiderSondage='.$id.'"><br />
			Le sondage est sauvegardé automatiquement à chaque modification.<br />
			Lancer le sondage (re)donne un jeton de vote à chaque utilisateur Staff du site et <b>SUPPRIME</b> toutes les réponses existantes.<br />
			<input type="submit" value="Lancer le sondage" class="btn" />
		</form>';

	echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control?FermerSondage='.$id.'"><br />
			Fermer le sondage supprime les jetons, et empêche donc tout nouveau vote.<br />
			<input type="submit" value="Fermer le sondage" class="btn" />
		</form>';

	require("inc/footer.php");
}
elseif(isset($_GET['Resultats'])){
	$id = intval($_GET['Resultats']);

	$titrePage = "Résultats du sondage";
	require("inc/header.php");

	$req = query("SELECT ps.Nom, psq.Question, psr.Reponse, psr.NbVotes, psq.ID AS qID, psq.Type
		FROM polar_sondages_questions psq
		LEFT JOIN polar_sondages_reponses psr ON psr.Question = psq.ID
		LEFT JOIN polar_sondages ps ON ps.ID = psq.Sondage
		WHERE ps.ID = $id");

	$noQuestion = 0;
	$questionActuelle = -1;
	while($ligne = mysql_fetch_assoc($req)){
		if($noQuestion == 0)
			echo "<h1>$titrePage : ".$ligne['Nom'].'</h1>';

		if($ligne['qID'] != NULL){
			if($ligne['qID'] != $questionActuelle){
				// Affichage de la question
				$questionActuelle = $ligne['qID'];
				$noQuestion++;
				$noReponse = 1;
				echo '</fieldset>';
				echo '<fieldset>';
				echo '<legend><strong>Question '.$noQuestion.' </strong>: '.$ligne['Question'].'</legend>';
			}

			if($ligne['Reponse'] != NULL){
				// Affichage des réponses
				echo 'Réponse '.$noReponse.' : '.$ligne['Reponse'] . ' - ';
				echo ' Nombre de voix : '.$ligne['NbVotes'];
				echo '<br />';
				$noReponse++;
			}
			elseif($ligne['Type'] == 'libre'){
				// Je sais c'est mal, mais j'ai la flemme de me replonger dans l'autre requête
				$req2 = query("SELECT Reponse FROM polar_sondages_reponses_libres
					WHERE Question = $questionActuelle");
				while($res2 = mysql_fetch_assoc($req2))
					echo '<li>'.$res2['Reponse'].'</li>';
			}
		}
	} // Fin de la boucle sur les lignes

	// Formulaire d'ajout d'une question
	echo '</fieldset>';

	require("inc/footer.php");
}
else {
	use_jquery_ui();
	addFooter('<script>
		$(document).ready(function() {
			$("#dialog").dialog({ width: 700, autoOpen: false });
			$("#open").click(function(){ $("#dialog").dialog("open"); });
		});
		</script>');
	$titrePage = "Sondages disponibles";
	require("inc/header.php");

	$req = query("SELECT ps.ID, ps.Nom, ps.Date, ps.Par, pu.Prenom as Auteur
		FROM polar_sondages ps
		INNER JOIN polar_utilisateurs pu ON ps.Par=pu.ID");

	echo '<p><input type="button" class="btn" id="open" value="Nouveau sondage" /></p>';

	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<tr><th>Nom</th><th>Proposé le</th><th>Proposé par</th><th>Résultats</th><th>Editer</th><th>Supprimer</th></tr>';
	while($data=mysql_fetch_assoc($req)){
		echo '<tr>';
		echo '<td>'.$data['Nom'].'</td>';
		echo '<td>'.$data['Date'].'</td>';
		echo '<td>'.$data['Auteur'].'</td>';
		echo '<td><a href="'.$racine.$module.'/'.$section.'?Resultats='.$data['ID'].'" title="Voir les résultats"><img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></a></td>';
		echo '<td><a href="'.$racine.$module.'/'.$section.'?id='.$data['ID'].'" title="Editer"><img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></a></td>';
		echo '<td><a href="'.$racine.$module.'/'.$section.'_control?SupprimerSondage='.$data['ID'].'" title="Supprimer"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="X" /></a></td>';
		echo '</tr>';
	}
	echo '</table>';

	echo '<div id="dialog" title="Nouveau sondage">';
	echo '<table>';
	echo '<form name="formulaire" method="post" action="'.$racine.$module.'/'.$section.'_control?NouveauSondage">';
	echo '<tr><td>Nom du sondage</td><td><input type="text" class="in-text" size="50" name="ep-nom" /></td></tr>';
	echo '<tr><td colspan="2"><input type="submit" value="Valider le nom" class="btn" /></td></tr>';
	echo '</form>';
	echo '</table>';
	echo '</div>';

	require("inc/footer.php");
}

?>
