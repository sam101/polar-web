<?php
// On va récupérer les infos
$user = intval($_SESSION['con-id']);
$sondage = intval($_GET['id']);

$req = query("SELECT ps.Nom, psq.Question, psq.ID AS qID, psr.Reponse, psr.ID AS rID, psq.Type
	FROM polar_sondages ps
	LEFT JOIN polar_sondages_questions psq ON psq.Sondage = ps.ID
	LEFT JOIN polar_sondages_reponses psr ON psr.Question = psq.ID
	INNER JOIN polar_sondages_jetons psj ON psj.Utilisateur = $user
	WHERE ps.ID=$sondage");


$noQuestion = 0;
while($ligne = mysql_fetch_assoc($req)){
	if($ligne['qID'] != NULL){
		// Affichage des réponses
		if($ligne['Reponse'] != NULL && (
			($ligne['Type'] == 'unique' && isset($_POST[$ligne['qID']]) && $_POST[$ligne['qID']] == $ligne['rID'])
			|| ($ligne['Type'] == 'multiple' && !empty($_POST[$ligne['qID'].'_'.$ligne['rID']]) && $_POST[$ligne['qID'].'_'.$ligne['rID']] == $ligne['rID'])))
			query("UPDATE polar_sondages_reponses SET NbVotes = NbVotes + 1 WHERE ID=".$ligne['rID']);
		else if($ligne['Type'] == 'libre' && !empty($_POST[$ligne['qID']])){
			$saisie = mysqlSecureText($_POST[$ligne['qID']]);
			$question = $ligne['qID'];
			query("INSERT INTO polar_sondages_reponses_libres (
				Question,
				Reponse
			) VALUES (
				$question,
				'$saisie'
			)");
		}
	}
} // Fin de la boucle sur les lignes


query("DELETE FROM polar_sondages_jetons WHERE Sondage=$sondage AND Utilisateur=$user");

echo '<script>alert("Merci de votre participation !"); document.location.href="'.$racine.$module.'";</script>';
?>
