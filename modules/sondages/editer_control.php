<?php
if(isset($_GET['NouveauSondage'])){
	$user = intval($_SESSION['con-id']);
	$nom = mysqlSecureText($_POST['ep-nom']);
	query("INSERT INTO polar_sondages (
			Nom,
			Date,
			Par
		) VALUES(
			'$nom',
		 	NOW(),
			$user
		)
	");
	$num = mysql_insert_id();
	header("Location: $racine$module/editer?id=$num");
}
elseif(isset($_GET['SupprimerSondage'])){
	$idSondage = intval($_GET['SupprimerSondage']);
	query("DELETE FROM polar_sondages WHERE ID=$idSondage");
	header("Location: $racine$module/$section");
}
elseif(isset($_GET['AjouterQuestion'])){
	$idSondage = intval($_GET['AjouterQuestion']);
	$type = mysqlSecureText($_POST['ep-type']);
	$texte = mysqlSecureText($_POST['ep-nouvelle-question']);
	query("INSERT INTO polar_sondages_questions (
			Sondage,
			Question,
			Type
		) VALUES (
			$idSondage,
			'$texte',
			'$type'
		)
	");
	header("Location: $racine$module/$section?id=$idSondage");
}
elseif(isset($_GET['AjouterReponse'])){
	$idQuestion = intval($_GET['AjouterReponse']);
	$idSondage = intval($_GET['id']);
	$text = mysqlSecureText($_POST['texte']);
	query("INSERT INTO polar_sondages_reponses (
			Question,
			Reponse
		) VALUES (
			$idQuestion,
			'$text'
		)
	");
	header("Location: $racine$module/$section?id=$idSondage");
}
elseif(isset($_GET['SupprimerReponse'])){
	$idSondage = intval($_GET['id']);
	$idReponse = intval($_GET['SupprimerReponse']);
	query("DELETE FROM polar_sondages_reponses WHERE ID=$idReponse");
	header("Location: $racine$module/$section?id=$idSondage");
}
elseif(isset($_GET['SupprimerQuestion'])){
	$idSondage = intval($_GET['id']);
	$idQuestion = intval($_GET['SupprimerQuestion']);
	query("DELETE FROM polar_sondages_reponses WHERE Question=$idQuestion");
	query("DELETE FROM polar_sondages_questions WHERE ID=$idQuestion");
}
elseif(isset($_GET['ValiderSondage'])){
	$idSondage = intval($_GET['ValiderSondage']);

	query("UPDATE polar_sondages_reponses
		INNER JOIN polar_sondages_questions ON polar_sondages_questions.ID = polar_sondages_reponses.Question
		SET NbVotes = 0
		WHERE Sondage = $idSondage");

	query("DELETE FROM polar_sondages_reponses_libres
		WHERE Question IN (SELECT ID FROM polar_sondages_questions WHERE Sondage = $idSondage)");

	$req = query("SELECT ID FROM polar_utilisateurs WHERE Staff = 1");
	while($data = mysql_fetch_assoc($req)){
		$user = intval($data['ID']);
		query("INSERT IGNORE INTO polar_sondages_jetons (
				Sondage,
				Utilisateur
			) VALUES(
				$idSondage,
				$user
			)");
	}

	header("Location: $racine$module/$section");
}
elseif(isset($_GET['FermerSondage'])){
	$sondage = intval($_GET['FermerSondage']);
	query("DELETE FROM polar_sondages_jetons WHERE Sondage=$sondage");

	header("Location: $racine$module/$section");
}
