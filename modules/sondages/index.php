<?php
$titrePage = "Sondages";
require("inc/header.php");

if(isset($_GET['Participer']) && intval($_GET['Participer']) > 0){
	$id = intval($_GET['Participer']);

	$titrePage = "Participer au sondage";

	$req = query("SELECT ps.Nom, psq.Question, psq.ID AS qID, psr.Reponse, psr.ID AS rID, psq.Type
		FROM polar_sondages ps
		LEFT JOIN polar_sondages_questions psq ON psq.Sondage = ps.ID
		LEFT JOIN polar_sondages_reponses psr ON psr.Question = psq.ID
		WHERE ps.ID=$id");

	$noQuestion = 0;
	$questionActuelle = -1;
	while($ligne = mysql_fetch_assoc($req)){
		if($noQuestion == 0){
			echo "<h1>$titrePage : ".$ligne['Nom'].'</h1>';

			afficherErreurs();

			echo '<form action="'.$racine.$module.'/'.$section.'_control?id='.$id.'" method="post" id="formulaire-news">';
		}

		if($ligne['qID'] != NULL){
			if($ligne['qID'] != $questionActuelle){
				// Affichage de la question
				$questionActuelle = $ligne['qID'];
				$noQuestion++;
				echo '</fieldset>';
				echo '<fieldset>';
				echo '<legend><strong>Question '.$noQuestion.' </strong>: '.$ligne['Question'].'</legend>';
			}

			echo '<label>';
			$saisie = getFormData($ligne['qID'].'_'.$ligne['rID']);
			if($saisie && $ligne['Type'] != 'libre')
				$checked = ' checked="checked"';
			else
				$checked = '';

			switch($ligne['Type']){
				case 'unique':
					echo '<input type="radio" name="'.$ligne['qID'].'" value="'.$ligne['rID'].'"'.$checked.' />';
				break;
				case 'multiple':
					echo '<input type="checkbox" name="'.$ligne['qID'].'_'.$ligne['rID'].'" value="'.$ligne['rID'].'"'.$checked.' />';
				break;
				case 'libre':
					echo '<textarea name="'.$ligne['qID'].'" class="in-texte">'.$saisie.'</textarea>';
				break;
			}

			echo $ligne['Reponse'];
			echo '</label>';
			echo '<br />';
		}
	} // Fin de la boucle sur les lignes

	// Fin du sondage
	echo '</fieldset>';

	echo '<input type="submit" value="Valider ma participation" class="btn" />
		</form>';
}
else {
	echo '<h1>Les sondages disponibles</h1>';

	$user = intval($_SESSION['con-id']);
	$req=query("SELECT ps.ID, ps.Nom, ps.Date, ps.Par, pu.Prenom as Auteur
		FROM polar_sondages ps
		INNER JOIN polar_sondages_jetons psj ON ps.ID=psj.Sondage
		INNER JOIN polar_utilisateurs pu ON ps.Par=pu.ID
		WHERE psj.Utilisateur=$user");

	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<tr><th>Nom</th><th>Proposé le</th><th>Proposé par</th><th>Participer</th></tr>';
	while($data=mysql_fetch_assoc($req)){
		echo '<tr>';
		echo '<td>'.$data['Nom'].'</td>';
		echo '<td>'.$data['Date'].'</td>';
		echo '<td>'.$data['Auteur'].'</td>';
		echo '<td><a href="'.$racine.$module.'/'.$section.'?Participer='.$data['ID'].'" title="Participer !"><img src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></a></td>';
		echo '</tr>';
	}
	echo '</table>';
}

require("inc/footer.php");
?>

