<?php
$titrePage = "Sondages";

addHeaders('<link href="'.$racine.'lib/highslide/highslide.css" rel="stylesheet" type="text/css"/>');
addFooter('
           <script language="javascript" type="text/javascript" src="'.$racine.'lib/highslide/highslide.packed.js"></script>
           <script type="text/javascript">
           hs.graphicsDir = \''.$racine.'lib/highslide/graphics/\';
           hs.outlineType = \'rounded-white\';
           hs.showCredits = false;
           hs.align = \'center\';
           </script>
'
           );

require("inc/header.php");

if (isset($_GET['sondage'])) {
    $id = intval($_GET['sondage']);
    if ($db->validObject('Sondage', $id)) {
        $sondage = Sondage::getById($id);

        echo '<h1>'.$sondage->Titre.'</h1>';
        echo '<form action="'.$racine.$module.'/'.$section.'_control" method="post">';
        echo '<input type="hidden" name="sondage" value="'.$sondage->get_id().'">';
        foreach ($sondage->questions as $question) {
            echo '<h3>Question : '.$question->Question.'</h3>';

            foreach ($question->reponses as $reponse) {
                echo '<input type="checkbox" name="r'.$question->get_id().'-'.$reponse->get_id().'"/>'.$reponse->Reponse.'<br/>';
                if ($reponse->Image != NULL) {
                    echo '<a href="'.$racine.'Documents/ImagesSondages/'.$reponse->Image.'" class="highslide" onclick="return hs.expand(this)"><img src="'.$racine.'Documents/ImagesSondages/'.$reponse->Image.'" style="max-width: 650px; max-height: 200px;"/></a><br/><br/>';
                }
            }
        }
        echo '<input type="submit" value="Voter"/></form>';
    } else {
        echo "<h1>Ce sondage n'existe pas</h1>";
    }
}
require("inc/footer.php");
?>