<?php
if (isset($_POST["sondage"])) {
    $id = intval($_POST['sondage']);
    if ($db->validObject('Sondage', $id)) {
        $sondage = Sondage::getById($id);

        foreach ($sondage->questions as $question) {
            foreach ($question->reponses as $reponse) {
                $name = 'r'.$question->get_id().'-'.$reponse->get_id();
                if (isset($_POST[$name])) {
                    $reponse->votes->add($conid);
                }
            }
        }
        $db->save($sondage);
    }
}

echo '<script>alert("Merci de votre participation !"); document.location.href="'.$racine.'";</script>';
?>