<?php

class ErreurCaisse extends PolarException { }

function getPost($name, $default=NULL) {
    return isset($_POST[$name]) ? $_POST[$name] : $default;
}

if (isset($_GET['payer'])) {
    $to_remove = array(); // stocke les commandes qui doivent être enlevées du panier
    try {
        if (!Request::$panier->actif())
            throw new ErreurCaisse("Impossible de payer, aucun panier en cours.");

        $db->beginTransaction();

        $raison = getPost('ep-raison', '');

        if (!isset($_POST['ep-compte']))
            throw new ErreurCaisse("Paiement par compte asso mais aucun compte précisé");

        $compte = CompteAsso::select('CompteAsso.*')
            ->select('COALESCE((SELECT SUM(`PrixFacture`) FROM `polar_caisse_ventes`
                               WHERE Asso = CompteAsso.ID AND `Finalise` = 0 ),0) +
                      COALESCE((SELECT SUM(`PrixFacture`) FROM `polar_caisse_ventes_global`
                               WHERE Asso = CompteAsso.ID AND `Finalise` =0 ), 0)', 'EnCours')
            ->where('CompteAsso.ID = ?', intval($_POST['ep-compte']))
            ->getOne();
        if ($compte === null)
            throw new ErreurCaisse("Compte invalide");
        else if (is_null($compte->DateActivation))
            throw new ErreurCaisse("Compte désactivé");

        if(!$compte->checkMotDePasse($_POST['ep-pass-asso']))
            throw new ErreurCaisse('Mauvais mot de passe');

        // on crée une vente qui va nous servir de modèle pour la suite
        // on ne la sauvegarde pas
        $vente1 = new Vente(false);
        $vente1->IDVente = $vente1->generate_idvente();
        $vente1->Asso = $compte;
        $vente1->MoyenPaiement = 'assos';
        $vente1->Tarif = $compte->TarifAsso ? 'asso' : 'normal';
        $vente1->Permanencier = $user->is_logged() ? $user : UtilisateurSite::getVendeurDefaut();
        $vente1->Client = $raison;

        // le total sert à savoir si on dépasse le quota du compte
        $total = 0;
        foreach(Request::$panier->getCommandes() as $commande) {
            // on vérifie d'abord si la commande est un billet
            // et si il reste de la place
            if ($commande->NomType == 'Billet' &&
                !Commande::validerBilletterie($commande->get_id())) {

                $to_remove[] = $commande;
            }

            $vente = $vente1->creerSimilaire($commande->CodeCaisse,
                                             $commande->Quantite);
            $total += $vente->PrixFacture;
            $vente->save();

            // on marque la commande comme payée
            $commande->DatePaiement = raw('NOW()');
            $commande->IDVente = $vente1->IDVente;
            $commande->save();
        }

        if (count($to_remove) > 0)
            throw new ErreurCaisse("Erreur de validation d'une billetterie");

        if ($compte->Quota != NULL && $total + $compte->EnCours > $compte->Quota)
            throw new ErreurCaisse("Quota dépassé !\nVous ne pouvez payer que ".formatPrix($compte->Quota - -$compte->EnCours)."€ avec le compte asso\n");

        // On sauvegarde le tout
        $db->commit();

        // Et on met à jour le stock
        MAJStock($vente1->IDVente);

        $panier = Request::$panier->getPanier();
        if ($panier instanceof PanierWeb) {
            $panier->Etat = 'paid';
            $panier->save();

            $mail = $panier->Mail;
        }

        if (!isset($mail)) {
            $mail = $compte->Asso->MailAsso;
        }

        CommandesMailer::confirmerPaiementAsso($mail, Request::$panier->getCommandes());

        Request::$panier->nettoyer();
        redirectOk('', "Vente validée !\nVous allez recevoir un mail récapitulatif", 'news', 'index');
    } catch (ErreurCaisse $e) {
        $db->rollBack();

        $message = $e->getMessage();
        // on enleve les billetteries pleines du panier maintenant
        // sinon c'est annulé en même temps que le reste de la transaction
        foreach($to_remove as $commande) {
            Request::$panier->removeCommande($commande);
            $message .= "Désolé, la billetterie ".$commande->NomBilletterie." est complète, la commande a été retirée de votre panier.\n";
        }

        Log::info("Erreur lors du paiement asso en ligne",
                  array("exception" => $e));
        redirectWithErrors('', $message);
    }
}
else if (isset($_GET['get_comptes'])) {
    $asso = Asso::select()->where("ID = ? AND Etat = 'Actif'", intval($_GET['get_comptes']))->getOne();
    if ($asso == NULL)
        die("{'erreur' : 'Asso Invalide'}");

    $result = array();

    foreach ($asso->getComptesActifs() as $compte) {
    $result[] = array('id'=>$compte->get_id(),
                                 'nom'=>$compte->Nom,
                                 'tarif_asso'=>$compte->TarifAsso);
    }
    echo json_encode($result);
}
?>