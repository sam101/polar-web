<?php
$nomCommande = 'Plastification';
$extensionsAutorisees = array('pdf');
$quantiteMax = 10;

$mailExpediteur = 'polar-plastification@assos.utc.fr';
$adresseNotification = 'polar-plastification@assos.utc.fr';
$mailClient = array("Demande de plastification enregistrée !",
	"Bonjour,<br />La demande de plastification que tu viens de faire sur le site du Polar a bien été enregistrée.<br/><br/>Ton numéro de commande est le : %id%.<br/><br/>Tu recevras un courriel quand le plastification sera prêt !<br /><br />Cordialement,<br />L'équipe du Polar.");
$mailNotification = array("Nouvelle demande de plastification !",
	"Une nouvelle plastification vient d'être déposée sur le site du Polar.");

require("_commander_control.php");
