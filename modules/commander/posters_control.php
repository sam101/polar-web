<?php
$nomCommande = 'Poster';
$extensionsAutorisees = array('pdf');
$quantiteMax = 5;

$mailExpediteur = 'polar-posters@assos.utc.fr';
$adresseNotification = 'polar-posters@assos.utc.fr';
$mailClient = array("Demande d'impression de poster enregistrée !",
	"Bonjour,<br />La demande d'impression de poster que tu viens de faire sur le site du Polar a bien été enregistrée.<br/><br/>Ton numéro de commande est le : %id%.<br/><br/>Tu recevras un courriel quand le poster sera prêt !<br /><br />Cordialement,<br />L'équipe du Polar.");
$mailNotification = array("Nouvelle demande d'impression de poster !",
	"Une nouvelle impression vient d'être déposée sur le site du Polar.");

require("_commander_control.php");
