<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>

  <body>
    <div id="header">
      <img src="<?php echo self::urlAbsolue(urlStyle('textures/logo_mail.png')) ?>" alt="Le Polar" height="50" width="63"/>
      <span style="font-size: 2.5em;font-weight: bold;">Le Polar - <?php echo $titre ?></span>
    </div>

    <div class="intro">
      <h3>Bonjour,</h3>
      <?php echo $intro ?>
    </div>

    <?php if ($listing !== null): ?>
    <div class="listing">
      <?php echo $listing ?>
    </div>
     <?php endif; ?>

    <div class="fin">
      <?php echo $fin ?>
      <p>
        En cas de problème vous pouvez nous contacter en répondant à ce mail, en passant au Polar (UTC bâtiment Benjamin Franklin, local E008) ou par téléphone au 03.44.23.43.73.<br/><br/>

      Cordialement,<br/>
      L'équipe du Polar
    </div>

    <div class="footer">
      <p>
      <a href="<?php echo $CONF['polar_url']; ?>">Le Polar</a> - Rue Roger Coutollenc 60200 Compiègne - polar@assos.utc.fr - 03.44.23.43.73 - SIRET : 42898163300012
    </p>
    </div>
  </body>
</html>
