<?php

$req = query("SELECT pca.ID, pct.Actif, pca.Nom, pca.PrixVente FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
	WHERE pct.Nom LIKE '$nomCommande' AND pct.Actif = 1 AND pca.Actif = 1 AND pca.EnVente = 1
	ORDER BY Nom");

use_jquery_ui();

addFooter('<script type="text/javascript" src="'.$racine.'js/jquery.timepicker.js"></script>');
addFooter('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.timepicker.css"/>');

addFooter('<script type="text/javascript">

var erreur_mail = [["@eut.utc.fr", "@etu.utc.fr"],
                   ["@utc.etu.fr", "@etu.utc.fr"],
                   ["@etu.fr", "@etu.utc.fr"],
                   ["@gmail.fr", "@gmail.com"],
                   ["@etu.utc.com", "@etu.utc.fr"]
                  ]

$(document).ready(function(){
	$("#ep-format,#ep-quantite").change(function(){
		$("#tprix").html("Total : " + $("#ep-format > option:selected").attr("prix") * $("#ep-quantite").val() + " EUR");
	});
    $(\'#ep-mail\').keyup(function() {
        var text = $(\'#ep-mail\').val();
        if (text.length > 10) {
            erreur_mail.forEach(function(elt, index, arr) {
                                    var idx = text.indexOf(elt[0]);
                                    if (idx > 0)
                                        $(\'#ep-mail\').val(text.replace(elt[0], elt[1]));
                                });
        }
    });
	$(\'#date-retrait\').datetimepicker({
		timeFormat: "HH:mm",
		dateFormat: "yy-mm-dd"
	});
});
</script>');

if ($user->is_logged()) {
    $nom = $user->Nom;
    $prenom = $user->Prenom;
    $email = $user->Email;
} else {
    $infos = Request::$panier->getUserInfos();
    if ($infos !== false) {
        $nom = $infos['nom'];
        $prenom = $infos['prenom'];
        $email = $infos['mail'];
    } else {
        $nom = getFormData('ep-nom');
        $prenom = getFormData('ep-prenom');
        $email = getFormData('ep-mail');
    }
}

require('inc/header.php');
?>
<div class="well">
<h1><?php echo $titrePage; ?></h1>
<?php
if(mysql_num_rows($req) > 0) {
	echo $descriptif.'</div>';
    if (!$user->is_logged()):
?>
    <div class="alert alert-info">
    <strong>Nouveau !</strong> <a href="<?php echo $CONF["cas_login"]; ?>">Connectez vous sur le site</a> avec votre compte UTC pour pouvoir suivre vos commandes.
    </div>
<?php
    endif;
	afficherErreurs();
?>
<div class="text-center">
  <div style="display:inline-block;text-align:left;">
    <form method="post" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section; ?>_control" class="form-horizontal">
      <div class="control-group">
        <label class="control-label" for="ep-nom">Nom</label>
			<div class="controls">
				<input type="text" class="in-texte" id="ep-nom" name="ep-nom" value="<?php echo $nom ?>" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="ep-prenom">Prénom</label>
			<div class="controls">
				<input type="text" class="in-texte" id="ep-prenom" name="ep-prenom" value="<?php echo $prenom ?>" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="ep-mail">Adresse mail UTC/ESCOM</label>
			<div class="controls">
				<input type="text" class="in-texte" name="ep-mail" value="<?php echo $email ?>" id="ep-mail" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="ep-quantite">Quantite</label>
			<div class="controls">
			<?php
			if(isset($quantiteMax)){
			?>
				<select class="in-text" id="ep-quantite" name="ep-quantite">
					<?php
					for($i=1;$i<=$quantiteMax;$i++)
						echo '<option value="'.$i.'">'.$i.'</option>';
					?>
				</select>
				<small>Nous contacter pour plus de <?php echo $quantiteMax; ?> exemplaires.</small>
			<?php
			}
			else {
			?>
				<input type="text" class="in-texte" name="ep-quantite" value="<?php echo getFormData('ep-quantite'); ?>" />
			<?php
			}
			?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="ep-format">Format</label>
			<div class="controls">
				<select class="in-text" id="ep-format" name="ep-format">
					<option value="0" prix="0">---</option>
					<?php
					while($data = mysql_fetch_assoc($req)){
						if(getFormData('ep-format') == $data['ID'])
							$checked = ' selected="selected"';
						else
							$checked = '';

						echo '<option value="'.$data['ID'].'" prix="'.$data['PrixVente'].'"'.$checked.'>'.$data['Nom'].'</option>';
					}
					?>
				</select>
                
                <div id="tprix" class="help-block"></div>
			</div>
		</div>
        <?php if (Parametre::get('modeSoutenance') == 'on' && $nomCommande == 'Poster'): ?>
 		<div class="control-group">
			<label class="control-label" for="ep-date-retrait">Branche</label>
 			<div class="controls">
				<select id="ep-date-retrait" name="ep-date-retrait">
                  <option value="2014-02-21 10:00">Pas un poster de soutenance</option>
                  <option value="2014-02-20 01:00">GM</option>
                  <option value="2014-02-20 02:00">GSM</option>
                  <option value="2014-02-20 03:00">GSU</option>
                  <option value="2014-02-20 04:00">MPI</option>
                  <option value="2014-02-20 05:00">GI</option>
                  <option value="2014-02-20 05:00">GB</option>
                  <option value="2014-02-20 07:00">GP</option>
                </select>
 			</div>
 		</div>
        <?php else: ?>
		<div class="control-group">
			<label class="control-label" for="date-retrait">Date de retrait souhaitée</label>
 			<div class="controls">
              <input type="text" class="in-texte hasDatePicker" id="date-retrait" name="ep-date-retrait" value="<?php echo getFormData('ep-date-retrait'); ?>" />
			</div>
		</div>
        <?php endif; ?>
		<div class="control-group">
			<label class="control-label" for="ep-fichier">Fichier à imprimer</label>
			<div class="controls">
				<input type="file" id="ep-fichier" name="ep-fichier" />
			</div>
		</div>
		<div class="control-group">
          <div class="controls">
            <input type="submit" value="Envoyer la demande !" class="btn btn-primary" />
          </div>
		</div>
    </form>
  </div>
</div>
<?php
} // Fin si actif
else
	echo '<p>Ce secteur est actuellement ferm&eacute;. Merci de revenir plus tard !</p>';

require('inc/footer.php');
?>
