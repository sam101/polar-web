<?php
$nomCommande = 'Poster';
$quantiteMax = 5;

$titrePage = "Faire une demande d'impression de poster";
if (Parametre::get('modeSoutenance') == 'on') {
    $descriptif = "<p>Pour les posters de soutenance quelques l'UTC merci de choisir le format papier A0 glacé avec oeillets et de préciser votre branche.</p>
<p><b>Les commandes non payées ne seront pas imprimées.</b> Vous pouvez payer en ligne par CB ou payutc.<br/>
Le retrait des posters se fera sur les lieux des soutenances.
</p>";
} else {
    $descriptif = "<p>Un poster à faire imprimer ? Rien de plus simple, utilisez le formulaire suivant en remplissant correctement tous les champs. Vous recevrez une confirmation dès que le poster sera imprimé.</p>
<p><b>Les commandes doivent être payées avant que nous les imprimions.</b> Vous pouvez payer en ligne par CB ou payutc ou payer au Polar.<br/>
<p><b>Nouveau !</b> Pour 1€ de plus, nous ajoutons des oeillets sur votre poster, permettant un accrochage simplifié !<br/>";
}

$descriptif .= "<p>Vous recevrez un mail de confirmation à réception de votre fichier puis un mail de notification lorsqu'il aura été imprimé. Les commandes sont en général réalisées sous 24h en semaine, sauf rupture de stock ou problème matériel.</p>
<p>Le fichier envoyé doit être format pdf et ne doit pas dépasser 45Mo.</p>";

require("_commander.php");
