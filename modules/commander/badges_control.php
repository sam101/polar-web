<?php
$nomCommande = 'Badges';
$extensionsAutorisees = array('jpg', 'jpeg', 'png', 'gif');
$quantiteMax = 20;

$mailExpediteur = 'polar-badges@assos.utc.fr';
$adresseNotification = 'polar-badges@assos.utc.fr';
$mailClient = array("Demande de fabrication de badges enregistrée !",
	"Bonjour,<br />La demande de fabrication de badges que tu viens de faire sur le site du Polar a bien été enregistrée.<br/><br/>Ton numéro de commande est le : %id%.<br/><br/>Tu recevras un courriel quand tes badges seront prêts !<br /><br />Cordialement,<br />L'équipe du Polar.");
$mailNotification = array("Nouvelle demande de fabrication de badges !",
	"Une nouvelle fabrication de badges vient d'être déposée sur le site du Polar.");

require("_commander_control.php");
?>
