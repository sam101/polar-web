<?php

foreach(Request::$panier->getCommandes() as $commande) {
    // on vérifie d'abord si la commande est un billet
    // et si il reste de la place
    if ($commande->NomType == 'Billet' &&
        !Commande::validerBilletterie($commande->get_id())) {

        Request::$panier->removeCommande($commande);
        ajouterErreur("Désolé, la billetterie ".$commande->NomBilletterie." est complète, la commande a été retirée de votre panier.");
    }
}
/* Ce que fait cette page :
Affiche un résumé de la commande avec le prix total
redirige vers la page donnée par payutc

paiement_control fait:
vérifie la réussite du paiement
créer la vente polar
envoie un mail de succès avec la facture en pièce jointe

*/
require_once 'inc/header.php';
?>
<div class="row-fluid">
  <div class="span8">
    <h1>Paiement par payutc</h1>
  </div>
  <div class="span4 text-right">
    <a href="<?php echo urlTo('commander', 'panier') ?>" class="btn">Retour</a>
  </div>
</div>
<?php afficherErreurs(); ?>
<?php if (Request::$panier->actif()): ?>
<div class="row-fluid">
  <div class="span6">
    <br/>
    <p>payutc est le moyen de paiement commun de toutes les associations de l'UTC.<br/>
    Tous les cotisants au BDE-UTC peuvent avoir un compte qu'il peuvent utiliser pour règler leurs achats au Polar et au Pic'asso notamment.<br/></p>
    <p>Si vous avez assez d'argent sur votre compte payutc le paiement sera automatique.<br/>
    Si vous n'avez pas assez d'argent vous pourrez recharger votre compte par carte bleue (minium 10€).
    </p>
    <p><strong>Même si vous n'avez pas de compte payutc il est possible de payer en carte bleue pour toutes les commandes supérieures à 10€</strong></p>
    <?php if ($CONF['use_payutc']): ?>
    <div class="text-center">
      <a href="<?php echo urlControl('goPayutc') ?>" class="btn btn-primary btn-large">Aller vers la page de paiement</a>
    </div>
    <?php else: ?>
    <p class="alert alert-error">Malheureusement payutc est indisponible pour le moment.</p>
    <?php endif; ?>
  </div>

  <div class="span6">
  <?php
    ob_start();
    $total = 0;
    $total_asso = 0;
  foreach(Request::$panier->getCommandes() as $commande): ?>
  <div class="well">
    <div class="row-fluid">
      <div class="span9">
        <?php
          if ($commande->NomType == 'Billet')
              $descType = $commande->NomBilletterie . ' : ' .$commande->NomTarif;
          else
        $descType = $commande->NomArticle; ?>
        <h4><?php echo $descType ?></h4>
        <p>
          Numéro de commande : <?php echo $commande->get_id() ?><br/>
          <?php if ($commande->NomType == 'Annale'):
          require_once('inc/annales.php'); ?>
          UVS :
          <ul>
            <?php foreach(explode(' ', $commande->Contenu) as $uv): ?>
            <li><?php echo Annales::parseCode($uv) ?></li>
            <?php endforeach; ?>
          </ul>
          Nombre de pages : <?php echo $commande->Quantite ?><br/>
          <?php endif; ?>

          <?php if ($commande->NomType == 'Poster'): ?>
          Quantité : <?php echo $commande->Quantite ?>
          <?php endif; ?>
        </p>
      </div>
      <div class="span3 text-right">
        <?php 
          $article = Article::getById($commande->Article);
          $prix = $article->calculerPrix($commande->Quantite, false);
          $total += $prix;
        ?>
        <h4><?php echo formatPrix($prix); ?>€</h4>
      </div>
    </div>
  </div>
  <?php endforeach ; ?>
  <?php $liste = ob_get_clean(); ?>
  <div class="well">
    <div class="row-fluid">
      <div class="span9">
        <h4>Total</h4>
      </div>
      <div class="span3 text-right">
        <h4><?php echo formatPrix($total); ?>€</h4>
      </div>
    </div>
  </div>
  <?php echo $liste; ?>
</div>
<?php else: ?>
<div class="alert alert-error">
<h4>Vous n'avez aucun panier en cours</h4>
Utilisez les boutons ci dessous pour préparer votre commande.
</div>

<?php
function big_button($url, $texte) {
    return '<a href="'.$url.'" class="btn btn-large polar-side-btn">'.$texte.'</a>';
}
?>
<div class="row-fluid">
<div class="span4"><?php echo big_button(urlTo('commander', 'posters'), 'Commander un poster'); ?></div>
<div class="span4"><?php echo big_button(urlTo('annales', 'commander'), 'Commander des annales'); ?></div>
<div class="span4"><?php echo big_button(urlTo('billetterie', 'preinscription'), "S'inscrire à un évènement"); ?></div>
</div>

<?php endif; ?>
<?php
require_once 'inc/footer.php';
?>