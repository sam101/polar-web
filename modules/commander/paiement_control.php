<?php

if (isset($_GET['goPayutc'])) {
    if (!Request::$panier->actif())
        redirectWithErrors('payer', "Vous n'avez aucun panier en cours");
    if (Request::$panier->getNbCommandes() == 0)
        redirectWithErrors('payer', 'Votre panier est vide');


    $vente1 = new Vente(false);
    $vente1->MoyenPaiement = 'payutc';
    $vente1->Tarif = 'normal';
    $vente1->Permanencier = $user->is_logged() ? $user : UtilisateurSite::getVendeurDefaut();
    $vente1->Client = '';

    $items = array();

    // on vérifie d'abord si la commande est un billet
    // et si il reste de la place
    // puis on l'ajoute à la liste des items à payer
    foreach(Request::$panier->getCommandes() as $commande) {
        if ($commande->NomType == 'Billet') {
            if (!Commande::validerBilletterie($commande->get_id())) {
                Request::$panier->removeCommande($commande);
                ajouterErreur("Désolé, la billetterie ".$commande->NomBilletterie." est complète, la commande a été retirée de votre panier.");
            } else {
                /* on pré-rempli la case DatePaiement pour pas que des gens puissent
                 * payer en attendant qu'on ai fini la transaction
                 */
                $commande->DatePaiement = Commande::$DatePreValidation;
                $commande->save();
                Log::info("Commande $commande marquée comme prépayée");
            }
        }

        // on ne fait pas re-payer les articles déjà payés
        // (permet de gérer les annales avec réduction)
        if ($commande->DatePaiement === null || $commande->DatePaiement == Commande::$DatePreValidation) {
            $vente = $vente1->creerSimilaire($commande->CodeCaisse,
                                             $commande->Quantite);
            $items[] = $vente->getPayutcArray();
        }
    }
    if (hasErreurs()) {
        redirectWithErrors('payer');
    }

    $panier = Request::$panier->getPanierForWebsale($user);
    if ($panier === false) {
        Log::info("Impossible de créer la panier pour la websale");
        redirectWithErrors('payer', "Malheureusement une erreur est survenue, essayez de refaire votre commande.\nSi le problème persiste vous pouvez contacter le Polar (polar@assos.utc.fr)");
    }
    
    $websale = Payutc::loadService('WEBSALE', 'web', false);
    try {
        $res = $websale->createTransaction(array('items' => json_encode($items),
                                                           'fun_id' => $CONF['payutc_fundation'],
                                                           'mail' => $panier->Mail,
                                                           'return_url' => urlTo('commander', 'paiement_control', 'retour='.$panier)
                                                           ));
    } catch(\JsonClient\JsonException $e) {
        Log::info("Erreur preparation transaction websale", array('exception' => $e));
        redirectWithErrors('payer', "Une erreur est survenue lors de la préparation de votre paiement, nous nous excusons de ce dérangement.\nVous pouvez contacter le Polar (polar@assos.utc.fr) en précisant votre identifiant de panier : ".$panier);
    }

    Log::info("Transaction payutc (".$res->tra_id.") crée pour le panier ".$panier);
    $panier->Etat = 'sent';
    $panier->TransactionID = $res->tra_id;
    $panier->save();

    // on nettoie le panier pour pas que la gars ajoute des commandes pendant la transaction
    Request::$panier->nettoyer();

header('Location: '.$res->url);
}
else if (isset($_GET['retour'])) {
    $id = $_GET['retour'];
    Log::info('Retour de la transaction payutc, panier='.$id);

    $db->beginTransaction();

    try {
        $panier = PanierWeb::getById($id);
    } catch (UnknowObject $e) {
        Log::info("Retour de websale payutc, impossible de trouver le panier ".$id);
        redirectWithErrors('', "Nous n'arrivons pas à retrouver votre panier, nous nous excusons de ce dérangement.\nVous pouvez contacter le Polar (polar@assos.utc.fr) en précisant votre identifiant de panier : ".$id, 'news', 'index');
    }

    if ($panier->Etat == 'paid') {
        Log::info("Tentative de revalidation du panier déjà payé ".$id);
        redirectWithErrors('', "Ce panier est déjà payé.\nVous pouvez contacter le Polar (polar@assos.utc.fr) en précisant votre identifiant de panier (".$id.") si vous pensez qu'il s'agit d'une erreur.", 'news', 'index');
    }

    $websale = Payutc::loadService('WEBSALE', 'web', false);
    try {
        $infos = $websale->getTransactionInfo(array('fun_id' => $CONF['payutc_fundation'],
                                                    'tra_id' => $panier->TransactionID));
    } catch (\JsonClient\JsonException $e) {
        Log::info("Erreur lors de la recupération de l'état de la transaction payutc ".$panier->TransactionID, array('exception' => $e));
        redirectWithErrors('payer', 'Il y a eu une erreur lors de la transaction, nous nous excusons de ce dérangement.\nVous pouvez contacter le Polar (polar@assos.utc.fr) en précisant votre identifiant de panier : '.$panier);
    }

    if ($infos->status == 'V') {
        // la transaction est validée par payutc
        // on va maintenant créer la vente pour de vrai
        Log::info("Transaction payutc n° ".$panier->TransactionID." validée");

        $vente1 = new Vente(false);
        $vente1->IDVente = $vente1->generate_idvente();
        $vente1->MoyenPaiement = 'payutc';
        $vente1->Tarif = 'normal';
        $vente1->Permanencier = $user->is_logged() ? $user : UtilisateurSite::getVendeurDefaut();
        $vente1->Client = '';

        $ventes = array();
        foreach($panier->getCommandes() as $commande) {
            // pas de vérification des billets, le mec a payé donc on
            // valide sa place quoi qu'il arrive
            Log::info("Enregistrement paiement commande ".$commande);
            if ($commande->DatePaiement === null || $commande->DatePaiement == Commande::$DatePreValidation) {
                $nb_ventes++;
                Log::info("done");
                $commande->IDVente = $vente1->IDVente;
                $commande->DatePaiement = raw('NOW()');
                $commande->save();

                $vente = $vente1->creerSimilaire($commande->CodeCaisse,
                                                 $commande->Quantite);
                $vente->save();
                $ventes[] = $vente;
            }
        }
        if (count($ventes) == 0) {
            $db->rollBack();
            Log::critical("Panier $panier pas de vente à enregistrer !");
            redirectWithErrors('', "Nous sommes désolé, il y a eu une erreur lors de la validation de votre commande. Nous vous contacterons dès que le problème sera résolu.", 'news', 'index');
        }

        // On sauvegarde le lien IdVente <-> TransactionId
        $vpayutc = new VentePayutc($panier->TransactionID, $vente1->IDVente);
        try {
            $vpayutc->save();
        } catch (PDOException $e) {
            // on s'est fait piquer l'IDVente donc il faut en changer
            $vente1->IDVente = $vente1->generate_idvente();
            $vpayutc = new VentePayutc($panier->TransactionID, $vente1->IDVente);
            $vpayutc->save();
            foreach($ventes as $vente) {
                $vente->IDVente = $vente1->IDVente;
                $vente->save();
            }
        }

        $panier->Etat = 'paid';
        $panier->IDVente = $vente1->IDVente;
        $panier->save();

        CommandesMailer::confirmerPaiementWeb($panier);
        Request::$panier->nettoyer();
        $db->commit();
        redirectOk('', "Paiement réussi !\n\nMerci de votre commande, vous allez recevoir un mail récapitulatif d'ici peu.", 'news', 'index');
    } else {
        // on enlève le pré-paiement
        foreach($panier->getCommandes() as $commande) {
            if ($commande->DatePaiement == Commande::$DatePreValidation) {
                $commande->DatePaiement = null;
                $commande->save();
            }
        }

        // on réactive le panier mais on marque l'échec de transaction
        $panier->Etat = 'failed'; // pour que le panier soit réactivable
        Request::$panier->activer($panier);
        $panier->Etat = 'failed';
        $panier->save();

        Log::info("Échec de la transaction payutc ".$panier->TransactionID." (panier ".$panier."), etat : ".$infos->status);
        $db->commit();
        ajouterErreur("La transaction a échoué, vous pouvez recommencer ou contacter le Polar (polar@assos.utc.fr) si le problème persiste.");
        Request::$panier->redirectTo();
    }
}