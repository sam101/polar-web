<?php
$dossierUpload = "upload/commandes/";
$req = query("SELECT COUNT(*) FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
	WHERE pct.Nom LIKE '$nomCommande' AND pct.Actif = 1 AND pca.Actif = 1 AND pca.EnVente = 1");

if(mysql_num_rows($req) > 0) {
	// Sécurisation des données entrantes
	$nom = mysqlSecureText($_POST['ep-nom']);
	$prenom = mysqlSecureText($_POST['ep-prenom']);
	$article = intval($_POST['ep-format']);
	$quantite = intval($_POST['ep-quantite']);
	$mail = mysqlSecureText($_POST['ep-mail']);
    $date = mysqlSecureText($_POST['ep-date-retrait']);

    if(!preg_match('/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d$/', $date))
        $date = NULL;

	if(empty($mail))
		ajouterErreur("l'adresse mail n'a pas été précisée");

	else if(!verifMailUTC($mail))
		ajouterErreur("l'adresse mail n'est pas une adresse UTC/ESCOM correcte");

	if(empty($nom))
		ajouterErreur("le nom n'a pas été précisé");

	if(empty($prenom))
		ajouterErreur("le prénom n'a pas été précisé");

	$reqArticle = query("SELECT ID FROM polar_commandes_types WHERE CodeProduit=(SELECT CodeCaisse FROM polar_caisse_articles WHERE ID=$article)");
	if(mysql_num_rows($reqArticle) == 0)
		ajouterErreur("l'article choisi est incorrect");
	else {
		$resArticle = mysql_fetch_assoc($reqArticle);
		$idType = $resArticle['ID'];
	}

	if($quantite == 0 || $quantite > $quantiteMax)
		ajouterErreur("quantite invalide");

	if(!hasErreurs()) {
		if(!empty($_FILES['ep-fichier']['tmp_name']) && is_file($_FILES['ep-fichier']['tmp_name'])) {
			$morceaux = explode('.', $_FILES['ep-fichier']['name']);
			$extension = $morceaux[sizeof($morceaux) - 1];

			// On vérifie si l'extension est autorisée
			if(!in_array($extension, $extensionsAutorisees)){
				ajouterErreur("ce type de fichier n'est pas accepté.");
				saveFormData($_POST);
				header("Location: $racine$module/$section");
				die();
			}

			// Ajout de la commande
			$ip = mysqlSecureText(get_ip());
			query("INSERT INTO polar_commandes (
					Type,
					Nom,
					Prenom,
					Mail,
					DateCommande,
					DateRetrait,
					IPCommande
				) VALUES(
					$idType,
					'$nom',
					'$prenom',
					'$mail',
					NOW(),
					'$date:00',
					'$ip'
				)
			");
			$newid = mysql_insert_id();

			// Détermination du nom du fichier
			list($login,) = explode("@", $mail);
			$nom_fichier = mysqlSecureText($newid.'-'.$login.'.'.$extension);

			if(move_uploaded_file($_FILES['ep-fichier']['tmp_name'], $dossierUpload.$nom_fichier)) {
				query("INSERT INTO polar_commandes_contenu (
						IDCommande,
						Detail,
						Quantite
					) VALUES(
						$newid,
						'$nom_fichier',
						$quantite
					)
				");

				sendmail($mailExpediteur, 'Le Polar', array($adresseNotification), $mailNotification[0], str_replace("%id%", $newid, $mailNotification[1]));

                $commande = Commande::getById($newid); // todo
                Request::$panier->addCommande($commande, $user);
                Request::$panier->redirectTo($commande);
			}
			else {
				ajouterErreur('le chargement du fichier a échoué pour une raison inconnue');
				saveFormData($_POST);
				header("Location: $racine$module/$section");
			}
		}
		else {
			ajouterErreur("vous n'avez pas envoyé de fichier ou celui-ci n'existe pas");
			saveFormData($_POST);
			header("Location: $racine$module/$section");
		}
	}
	else {
		saveFormData($_POST);
		header("Location: $racine$module/$section");
	}
}
else
	header("Location: $racine");
?>
