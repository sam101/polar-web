<?php
$titre = "Paiement par compte asso";
require_once 'inc/header.php';
addFooter('
<script type="text/javascript">
$(function() {
  $("#ep-asso").change(function() {
      var value = $("#ep-asso").val();
      if (value > 0) {
        $.ajax({url: "'.urlControl('get_comptes').'="+value,
               dataType:"json",
                success : function(data, code, xhr) {
                  if (data.erreur) {
                    $("#ep-asso").val(0);
                  } else {
                    var html = "";
                    for (var index in data) {
                      var compte = data[index];
                      html += "<option value=\""+compte.id+"\" tarif-asso=\""+compte.tarif_asso+"\">"+compte.nom+"</option>";
                    }
                    $("#ep-compte").html(html);
                    $("#ep-compte").change();
                  }
               }}
             );
    } else {
      $("#ep-compte").html("<option value=\"0\">- Choisissez d\'abord une asso -</option>");
    }});
  $("#ep-compte").change(function() {
      if( $("#ep-compte option[value="+$(this).attr("value")+"]").attr("tarif-asso") == "true") {
          $(".tarif_asso").show();
          $(".tarif_normal").hide();
      } else {
          $(".tarif_normal").show();
          $(".tarif_asso").hide();
      }
  });
  $(".tarif_asso").hide();
  $(".remarque_tarif").hide();
});
</script>
');
?>
<div class="row-fluid">
  <div class="span8">
    <h1>Paiement par compte asso</h1>
  </div>
  <div class="span4 text-right">
    <a href="<?php echo urlTo('commander', 'panier') ?>" class="btn">Retour</a>
  </div>
</div>
<?php afficherErreurs(); ?>
<?php if (Request::$panier->actif()): ?>
<div class="row-fluid">
  <div class="span6">

    <div class="text-center">
      <div style="display:inline-block;text-align:left;">
        <br/>
        <form method="post" enctype="multipart/form-data" action="<?php echo urlControl('payer') ?>" class="form-horizontal">
        <div class="control-group">
          <label class="control-label" for="ep-asso">Asso</label>
          <div class="controls">
            <select class="input-xlarge" id="ep-asso">
              <option value="0">- Choisissez une asso -</option>
              <?php foreach(Asso::selectActives() as $asso): ?>
              <option value="<?php echo $asso ?>"><?php echo $asso->Asso ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="ep-compte">Compte</label>
          <div class="controls">
            <select class="input-xlarge" id="ep-compte" name="ep-compte">
              <option value="0">- Choisissez d'abord une asso -</option>
            </select>
            <span class="help-block tarif_normal remarque_tarif">Vous ne bénéficiez du tarif asso.</span>
            <span class="help-block tarif_asso remarque_tarif">Vous bénéficiez du tarif asso.</span>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="pass-asso">Mot de passe</label>
          <div class="controls">
            <input type="password" class="input-xlarge" id="pass-asso" name="ep-pass-asso" />
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="ep-raison">Raison</label>
          <div class="controls">
            <input type="text" class="input-xlarge" name="ep-raison" value="<?php echo getFormData('ep-raison') ?>" id="ep-raison" />
            <span class="help-block">La raison de l'achat apparaitra sur la facture en fin de semestre.</span>
          </div>
        </div>
        <div class="control-group">
          <div class="controls">
            <input type="submit" value="Valider" class="btn btn-primary" />
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="span6">
  <?php
    ob_start();
    $total = 0;
    $total_asso = 0;
  foreach(Request::$panier->getCommandes() as $commande): ?>
  <div class="well">
    <div class="row-fluid">
      <div class="span9">
        <?php
          if ($commande->NomType == 'Billet')
              $descType = $commande->NomBilletterie . ' : ' .$commande->NomTarif;
          else
        $descType = $commande->NomArticle; ?>
        <h4><?php echo $descType ?></h4>
        <p>
          Numéro de commande : <?php echo $commande->get_id() ?><br/>
          <?php if ($commande->NomType == 'Annale'):
          require_once('inc/annales.php'); ?>
          UVS :
          <ul>
            <?php foreach(explode(' ', $commande->Contenu) as $uv): ?>
            <li><?php echo Annales::parseCode($uv) ?></li>
            <?php endforeach; ?>
          </ul>
          Nombre de pages : <?php echo $commande->Quantite ?><br/>
          <?php endif; ?>

          <?php if ($commande->NomType == 'Poster'): ?>
          Quantité : <?php echo $commande->Quantite ?>
          <?php endif; ?>
        </p>
      </div>
      <div class="span3 text-right">
        <?php 
          $article = Article::getById($commande->Article);
          $prix = $article->calculerPrix($commande->Quantite, false);
          $total += $prix;

          $prix_asso = $article->calculerPrix($commande->Quantite, true);
          $total_asso += $prix_asso;
        ?>
        <h4 class="tarif_normal"><?php echo formatPrix($prix); ?>€</h4>
        <h4 class="tarif_asso"><?php echo formatPrix($prix_asso); ?>€</h4>
      </div>
    </div>
  </div>
  <?php endforeach ; ?>
  <?php $liste = ob_get_clean(); ?>
  <div class="well">
    <div class="row-fluid">
      <div class="span9">
        <h4>Total</h4>
      </div>
      <div class="span3 text-right">
        <h4 class="tarif_normal"><?php echo formatPrix($total); ?>€</h4>
        <h4 class="tarif_asso"><?php echo formatPrix($total_asso); ?>€</h4>
      </div>
    </div>
  </div>
  <?php echo $liste; ?>
</div>
<?php else: ?>
<div class="alert alert-error">
<h4>Vous n'avez aucun panier en cours</h4>
Utilisez les boutons ci dessous pour préparer votre commande.
</div>

<?php
function big_button($url, $texte) {
    return '<a href="'.$url.'" class="btn btn-large polar-side-btn">'.$texte.'</a>';
}
?>
<div class="row-fluid">
<div class="span4"><?php echo big_button(urlTo('commander', 'posters'), 'Commander un poster'); ?></div>
<div class="span4"><?php echo big_button(urlTo('annales', 'commander'), 'Commander des annales'); ?></div>
<div class="span4"><?php echo big_button(urlTo('billetterie', 'preinscription'), "S'inscrire à un évènement"); ?></div>
</div>

<?php endif; ?>
<?php
require_once 'inc/footer.php';
?>