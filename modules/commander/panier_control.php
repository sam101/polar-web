<?php

if (isset($_GET['archiver'])) {
    Request::$panier->archiver();
    Request::$panier->redirectTo();
}
else if (isset($_GET['reactiver'])) {
    try {
        $panier = PanierWeb::getById(intval($_GET['reactiver']));
        if ($panier->get_object_id('User') == $user->get_id()) {
            Request::$panier->activer($panier);
            Request::$panier->redirectTo();
        } else {
            redirectWithErrors('', "Vous ne pouvez pas réactiver le panier de quelqu'un d'autre !");
        }
    } catch (UnknwonObject $e) {
        redirectWithErrors('', "Ce panier n'existe pas.");
    } catch (PanierWebNonReactivable $e) {
        redirectWithErrors('', "Le panier n'est pas réactivable.");
    }
}
else if (isset($_GET['pay_later'])) {
    try {
        $id = Request::$panier->payLater();
        Request::$panier->nettoyer();
        if ($id !== false)
            redirectOk('', "Panier enregistré - Identifiant ".$id." \nVous allez recevoir un mail récapitulatif.", 'news', 'index');
        else
            redirectWithErrors('', "Le panier n'a malhereusement pas pu être enregistré, vous pouvez contacter le polar (polar@assos.utc.fr) si il s'agit d'un bug.", 'news', 'index');
    } catch (UnknwonObject $e) {
        redirectWithErrors('', "Ce panier n'existe pas.");
    }
} else if (isset($_GET['supprimer'])) {
    try {
        $commande = Commande::getById(intval($_GET['supprimer']));
        Request::$panier->supprimerCommande($commande);
    } catch (UnknownObject $e) {
        Log::info("Panier : Suppression impossible, commande ".$_GET['supprimer']." introuvable");
    }
    Request::$panier->redirectTo();
} else if (isset($_GET['annulerSuppression'])) {
    Request::$panier->annulerSuppression();
    Request::$panier->redirectTo();
}