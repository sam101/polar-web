<?php

class ErreurCommande extends Exception { }

try {
    switch(isset($_POST['action']) ? $_POST['action'] : '') {
    case 'commander':
        if (!isset($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['billetterie'], $_POST['tarif']))
            throw new ErreurCommande("Arguments manquants", 2);

        try {
            $tarif = TarifBilletterie::getById(intval($_POST['tarif']));
        } catch (UnknownObject $e) {
            throw new ErreurCommande("Ce tarif n'existe pas", 3, $e);
        }

        if($tarif->get_object_id('Evenement') != intval($_POST['billetterie']))
           throw new ErreurCommande("Le tarif et la billetterie ne correspondent pas !", 4);

        $prises = Commande::select('COUNT(*)')
            ->join('TarifBilletterie', 'TarifBilletterie.TypeCommande = Commande.Type')
            ->where('Commande.DatePaiement IS NOT NULL AND TarifBilletterie.Evenement = ? AND Commande.Termine = 0', $tarif->get_object_id('Evenement'));
        $billetterie = Billetterie::select('Billetterie.*')
            ->select($prises, 'PlacesPrises')
            ->where('Billetterie.ID = ?', $tarif->get_object_id('Evenement'))
            ->getOne();

        if (is_null($billetterie))
            throw new ErreurCommande("Billetterie invalide", 8);

        if (date('Y-m-d') > substr($billetterie->Date, 0, 10))
            throw new ErreurCommande("La date d'inscription est dépassée", 6);

        if ($billetterie->PlacesPrises >= $billetterie->Places)
            throw new ErreurCommande("Il n'y a plus de places pour cette billetterie", 7);

        if (!$billetterie->Preinscription)
            throw new ErreurCommande("Il n'est pas possible de se préinscrire pour cette billetterie", 8);

        $commande = new Commande(array('Type' => $tarif->TypeCommande,
                                       'Nom' => $_POST['nom'],
                                       'Prenom' => $_POST['prenom'],
                                       'Mail' => $_POST['email'],
                                       'DateCommande' => raw('NOW()'),
                                       'Termine' => false,
                                       'IPCommande' => get_ip()
                                       ));
        $commande->save();

        echo json_encode(array('success' => $commande->get_id()));
        break;
    case 'lister':
        if (!isset($_POST['billetterie']))
            throw new ErreurCommande("Billetterie non précisée", 5);

        $commandes = Commande::select('COUNT(*)')->where('Commande.Type = TarifBilletterie.TypeCommande AND Commande.Termine = 0');
        $payes = Commande::select('COUNT(*)')->where('Commande.Type = TarifBilletterie.TypeCommande AND Commande.DatePaiement IS NOT NULL AND Commande.Termine = 0');
        $tarifs = TarifBilletterie::select('TarifBilletterie.*, Article.Nom as NomArticle, Article.PrixVente')
            ->select($commandes, 'Commandes')
            ->select($payes, 'Payes')
            ->join('Commande', 'TarifBilletterie.TypeCommande = Commande.type')
            ->join('CommandeType', 'CommandeType.ID = Commande.Type')
            ->join('Article', 'Article.CodeCaisse = CommandeType.CodeProduit')
            ->where('TarifBilletterie.Evenement = ?', intval($_POST['billetterie']))
            ->where('Article.EnVente = 1 AND Article.Actif = 1') // check that
            ->groupBy('TarifBilletterie.ID');

        $result = array();

        foreach($tarifs as $tarif) {
            $result[] = array('id' => $tarif->get_id(),
                              'tarif' => $tarif->Intitule,
                              'prix' => $tarif->PrixVente,
                              'article' => $tarif->NomArticle,
                              'commandes' => intval($tarif->Commandes),
                              'payes' => intval($tarif->Payes));
        }

        echo json_encode($result);
        break;
    default:
        throw new ErreurCommande("Action Invalide", 1);
        break;
    }
} catch (ErreurCommande $e) {
    Log::info("Erreur API Commande : ".$e->getMessage());
    echo json_encode(array('error' => $e->getCode(),
                           'message' => $e->getMessage()
                           ));
    
}