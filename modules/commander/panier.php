<?php
require_once 'inc/header.php';

function big_button($url, $texte, $moar='') {
    return '<a href="'.$url.'" class="btn btn-large polar-side-btn '.$moar.'">'.$texte.'</a>';
}
?>
<h1>Votre panier</h1>

<div class="row">
  <div class="span3">
    <?php if (Request::$panier->actif()): ?>
    <?php echo big_button(urlTo('commander', 'paiement', 'websale'), 'Payer maintenant'); ?>
    <?php echo big_button(urlControl('pay_later'), 'Payer au Polar'); ?>
    <?php echo big_button(urlTo('commander', 'paiement_asso'), 'Payer par compte asso'); ?>
    <hr/>
    <?php endif; ?>
    <?php echo big_button(urlTo('commander', 'posters'), 'Commander un poster'); ?>
    <?php echo big_button(urlTo('annales', 'commander'), 'Commander des annales'); ?>
    <?php echo big_button(urlTo('billetterie', 'preinscription'), "S'inscrire à un évènement"); ?>
    <?php if (Request::$panier->actif()): ?>
    <hr/>
    <?php if($user->is_logged()): ?>
    <?php echo big_button(urlControl('archiver'), 'Archiver ce panier') ?>
    <?php else: ?>
    <a href="<?php echo urlControl('archiver') ?>" class="btn btn-large polar-side-btn" onclick="return confirm('Si vous supprimez ce panier vous ne pourrez pas récupérer les commandes qu\'il contient.\nVoulez vous vraiment le supprimer ?');">Supprimer ce panier</a>
    <?php endif; ?>
    <?php endif; ?>
  </div>

  <div class="span9">
    <?php afficherErreurs(); ?>

    <?php if (isset($_GET['deleted'])): ?>
    <div class="alert alert-success">
    <p>La commande n°<?php echo htmlspecialchars($_GET['deleted']) ?> a bien été enlevée du panier.&nbsp;
    <a href="<?php echo urlControl('annulerSuppression') ?>" title="Remettre la commande dans le panier">Annuler</a>
    </p>
    </div>
    <?php endif; ?>

    <?php if (Request::$panier->actif()): ?>
    <?php if (isset($_GET['added'])): ?>
    <div class="alert alert-success">
    <h4>Merci, votre commande n°<?php echo htmlspecialchars($_GET['added']) ?> a bien été ajoutée au panier.</h4>
    <p><br/>
    Vous pouvez <b>ajouter des commandes à votre panier</b> ou <b>valider votre panier</b> grâce au menu sur la gauche.
    </p>
    </div>
    <?php endif; ?>

    <?php if (!$user->is_logged()): ?>
      <p class="alert alert-info">
        <a href="<?php echo $CONF["cas_login"]; ?>"><strong>Connectez vous</strong> sur le site</a> pour sauvegarder vos paniers automatiquement.
      </p>
    <?php endif; ?>

    <?php foreach(Request::$panier->getCommandes() as $commande): ?>
    <div class="well">
      <div class="row-fluid pull-container">
        <div class="span9">
          <?php
            if ($commande->NomType == 'Billet')
                $descType = $commande->NomBilletterie . ' : ' .$commande->NomTarif;
            else
                $descType = $commande->NomArticle; ?>
          <h4><?php echo $descType ?></h4>
          <p>
            Numéro de commande : <?php echo $commande->get_id() ?><br/>
            <?php if ($commande->NomType == 'Annale'):
            require_once('inc/annales.php'); ?>
            UVS :
            <ul>
            <?php foreach(explode(' ', $commande->Contenu) as $uv): ?>
            <li><?php echo Annales::parseCode($uv) ?></li>
            <?php endforeach; ?>
            </ul>
            Nombre de pages : <?php echo $commande->Quantite ?><br/>
            <?php endif; ?>

            <?php if ($commande->NomType == 'Poster'): ?>
            Quantité : <?php echo $commande->Quantite ?>
            <?php endif; ?>
          </p>
        </div>
        <div class="span3 text-right">
          <h4><?php echo formatPrix(Article::getById($commande->Article)->calculerPrix($commande->Quantite)) ?>€</h4>
          <a class="pull-bottom" href="<?php echo urlControl('supprimer='.$commande) ?>" title="Enlever cette commande du panier">Supprimer</a>
        </div>
      </div>
    </div>
    <?php endforeach ; ?>
    </div>

    <?php else: // si aucun panier en cours ?>
    <p class="well">
      Vous n'avez aucun panier en cours, utilisez les boutons sur le côté pour ajouter des commandes à votre panier.
    </p>
    <?php if ($user->is_logged()): ?>
    <table class="table table-striped table-condensed table-bordered table-hover">
      <theader>
        <tr>
          <th>Date de création</th>
          <th>État</th>
          <th>Reprendre</th>
        </tr>
      </thead>
    <tbody>
    <?php foreach(PanierWeb::getForUser($user)->order('PanierWeb.ID DESC') as $panier): ?>
    <tr>
      <td><?php echo $panier->DateCreation ?></td>
      <td><?php echo $panier->getEtatAffichable() ?>
      <td><?php if ($panier->reactivable()): ?>
      <a href="<?php echo urlControl('reactiver='.$panier) ?>" title="Ajouter des commandes au panier"><i class="icon-play"></i></a>
      <?php endif; ?>
      </td>
    </tr>
    <?php endforeach; ?>
    <?php else: ?>
      <p class="alert alert-info">
        <a href="<?php echo $CONF["cas_login"]; ?>"><strong>Connectez vous</strong> sur le site</a> pour voir vos paniers sauvegardés.
      </p>
    <?php endif; ?>
    <?php endif; ?>
  </tbody>
</table>
  </div>
</div>
<?php
require_once 'inc/footer.php';
?>