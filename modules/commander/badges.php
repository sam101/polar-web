<?php
$nomCommande = 'Badges';
$quantiteMax = 20;

$titrePage = "Faire une demande de fabrication de badges";
$descriptif = "<p>Un badge à fabriquer ? Rien de plus simple, utilisez le formulaire suivant en remplissant correctement tous les champs. Vous recevrez une confirmation dès que le(s) badge(s) sera(ront) imprimé(s).<br />
	<b>ATTENTION</b> :
	<ul>
		<li>Le badge est circulaire, de 45mm de diamètre. Envoyez une image carrée, elle sera mise en forme automatiquement.</li>
		<li>Le fichier doit impérativement être une image (bmp, png, jpg), de 5Mo maximum.</li>
		<li>Remises sur les grandes quantités (>20 pièces) : passez au Polar !</li>
	</ul>
</p>";

require("_commander.php");
