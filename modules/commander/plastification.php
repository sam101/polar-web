<?php
$nomCommande = 'Plastification';
$quantiteMax = 10;

$titrePage = "Faire une demande de plastification";
$descriptif = "<p>Le Polar dispose d'une plastifieuse, permettant une protection des documents imprimés. Utilisez le formulaire suivant en remplissant correctement tous les champs. Vous recevrez une confirmation dès que votre plastification sera prête !</p>
<p></p>
<p>Le prix de l'impression couleur est inclus dans le prix de la plastification.<br />
<u>Le fichier doit avoir une taille maximale de <b>15Mo</b> et être au format <b>PDF</b>.</u></p>";

require("_commander.php");
