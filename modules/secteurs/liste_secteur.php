<?php
require("inc/header.php");
?>

<h1><?php echo $titre; ?></h1>
<p><?php echo $descriptif; ?></p>

<?php
$articles = Article::getActifBySection($codeSecteur);
if ($articles) : ?>

<ul class="thumbnails">
<?php foreach($articles as $article) : ?>
	<li class="span3" style="height: 270px;">
    	<div class="thumbnail" style="height: 270px;">
            <img alt="Image <?php echo $article->Nom; ?>" src="../upload/articles/<?php echo ($article->Photo != '' && file_exists(__DIR__.'/../../upload/articles/'.$article->Photo)) ? $article->Photo : 'default.png'; ?>" style="max-height: 180px; max-width: 200px;" />
            <div class="caption">
                <h4><?php echo $article->Nom; ?></h4>
                <p>Prix unitaire : <?php echo $article->PrixVente; ?>€</p>
            </div>
        </div>
    </li>
<?php endforeach; ?>
</ul>
<?php else : ?>
    <div class="alert alert-info">
    Il n'y a pas d'article disponible pour cette section !
    </div>
<?php
endif;
require("inc/footer.php");
?>


