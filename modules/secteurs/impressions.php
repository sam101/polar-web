<?php
$titre = 'Impressions';
$descriptif = "Le secteur principal du Polar : les impressions ! &Agrave; partir d'un original papier ou d'un fichier (clé USB, Internet...), du A4 au A0+, noir et blanc ou couleur, tout est possible au Polar !";
$codeSecteur = 5;
require("liste_secteur.php");
