<?php
$titrePage = 'Offrir une réduction';
require("inc/annales..php");
addHeaders(Annales::$ANNALES_CSS);
addFooter('<script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
  <link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />
  <script type="text/javascript">
  $(function() {
    $("#login").autocomplete("'.$racine.'utils/login_autocomplete",{
        matchContains:1,
                mustMatch:false,
                delay: 100,
                formatItem: function(row, i, max) {
                return row[0]+" - "+row[1]+" "+row[2];
            },
                formatResult: function(row) {
                return row[0];
            }
        });
		
	$("#sujets, #corrections, #libre").change(function() {
		$("#total").text( ( parseInt($("#sujets").val()) * '.Annales::$PAGES_OFFERTES_PAR_SUJET.' + parseInt($("#corrections").val()) * '.Annales::$PAGES_OFFERTES_PAR_CORRECTION.' + parseInt($("#libre").val()) ) );
	});
  });
  </script>');
require("inc/header.php");

echo '<h1>Offrir une réduction</h1>';

?>
<form name="formulaire" method="post" action="<?php echo $racine.$module.'/'.$section.'_control';?>">

<?php

afficherErreurs();
if(isset($_SESSION['annales_ok'])){
	echo '<script>$(document).ready(function(){alert("Commande numéro '.$_SESSION['annales_ok'].' enregistrée !");});</script>';
	unset($_SESSION['annales_ok']);
}

?>

<p>
	Cette page permet d'offrir des pages d'annales à un étudiant ayant déposé un sujet ou un corrigé dans la boîte un médians.<br />
	Entre le login, le nombre de sujets et le nombre de corrigés validés.<br />
	Un mail sera automatiquement envoyé au gentil étudiant.
</p>
<br /><br />

<label for="login">Login : </label><input type="text" id="login" name="login" size="50" required autofocus /><br />
<label for="sujets">Nbr de sujets : </label><input type="number" name="sujets" id="sujets" required value="0" min="0" max="9" maxlength="1" size="3" /> * <?php echo Annales::$PAGES_OFFERTES_PAR_SUJET ?> pages <br />
<label for="corrections">Nbr de corrections : </label><input type="number" name="corrections" id="corrections" required value="0" min="0" max="9" maxlength="1" size="3" /> * <?php echo Annales::$PAGES_OFFERTES_PAR_CORRECTION ?> pages<br />
<label for="libre">Pages supplémentaires : </label><input type="number" name="libre" id="libre" required value="0" min="0" max="100" maxlength="3" size="3" /> * 1 page<br />
<b>Total : <span id="total">0</span> pages</b><br />
<input type="submit" value="Valider">

</form>

<?php
require("inc/footer.php");
