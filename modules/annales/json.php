<?php
//"[{'Nom':'AP30', 'Pages':21}, {'Nom':'AR03', 'Pages':6}]"
if (isset($_GET['liste-annales'])) {
  $req = query("SELECT uv_code, uv_nbrPages FROM polar_annales_uvs WHERE uv_enVente = 1 ORDER BY uv_code");
  $annales = array();
  while ($data = mysql_fetch_assoc($req)) {
	$annales[] = array('Nom' => $data['uv_code'], 'Pages' => $data['uv_nbrPages']);
  }
  echo json_encode($annales);
}
else if(isset($_GET['annales']) && isset($_GET['login'])) {

  // On commence -------------------------------

  $login = mysqlSecureText($_GET['login']);
  $mail = $login.'@etu.utc.fr';

  // On ne fait pas un verifLoginUTC pour avoir le nom/prénom !
  $req = query("SELECT nom, prenom FROM cotisants.cot_listeetu WHERE login='$login'");
  if(mysql_num_rows($req) != 1) die("Login inconnu");

  $res = mysql_fetch_assoc($req);
  $nom = mysqlSecureText($res['nom']);
  $prenom = mysqlSecureText($res['prenom']);

  $ip = get_ip();

  //OK, on ajoute la commande
  query("INSERT INTO polar_commandes (
	Type,
	Nom,
	Prenom,
	Mail,
	DateCommande,
	IPCommande
) VALUES (
	(SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale'),
	'$nom',
	'$prenom',
	'$mail',
	NOW(),
	'$ip'
)");

  $idCommande = mysql_insert_id();

  $listeUV = "";

  $req_contenu = "INSERT INTO polar_commandes_contenu (
	IDCommande,
	Detail,
	Quantite
	) VALUES ";

  $annales = explode(',', $_GET['annales']);
  foreach($annales as $annale) {
    $uv = mysqlSecureText(substr($annale, 0, 4));
    $req_contenu .= "($idCommande, '$uv', (SELECT uv_nbrPages FROM polar_annales_uvs WHERE uv_code = '$uv')), ";
    $listeUV .= $uv.", ";
  }

  // On vire la virgule finale
  $req_contenu = substr($req_contenu, 0, -2);
  $listeUV = substr($listeUV, 0, -2);
  query($req_contenu);

  // On envoie un mail de confirmation
  $message = "Bonjour,<br />
<br />
Tu viens de passer une commande d'annales. Il faut maintenant que tu te rendes au Polar pour la régler, si ce n'est pas déjà fait, après quoi nous lancerons l'impression !<br />
<br />
Pour rappel, voici les informations de ta commande :<br />
Numéro de commande : $idCommande<br />
UV commandées : $listeUV<br />
<br />
À très bientôt,<br />
L'équipe du Polar<br /><br />
Les annales sont disponibles grâce à TOI !
Après les examens, dépose ton sujet dans la boîte à médians située au Polar !";
  sendMail('polar-annales@assos.utc.fr', 'Le Polar', array($mail), "[Le Polar] Commande d'annales n°$idCommande", $message);

  echo $idCommande;

}

?>
