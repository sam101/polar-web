<?php
require_once('inc/annales.php');

addHeaders('
   <link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />
	<style type="text/css">
		div.message {
			border: 1px solid black;
			padding: 1em;
			margin: .5em;
		}
	</style>');
addFooter('<script type="text/javascript" src="'.$racine.'js/jquery.bgiframe.min.js"></script>
   <script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
	<script type="text/javascript">
    var commandes = []; ');

	$req = query("SELECT ID, Mail FROM polar_commandes
		WHERE DatePaiement IS NOT NULL
		AND DatePrete IS NOT NULL
		AND DateRetrait IS NULL
		AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')");
    while($result = mysql_fetch_assoc($req)){
		addFooter('commandes.push(["'.$result['ID'].'", "'.$result['Mail'].'", "'.$result['ID'].'"]);');
    }
    addFooter('
    </script>
     <script type="text/javascript">
    $().ready(function() {
		$("#suggest1").focus().autocomplete(commandes,{
			matchContains:1,
			mustMatch:false,
			formatItem: function(row, i, max) {
				return row[0]+" - "+row[1];
			},
			formatResult: function(row) {
				return row[2];
			}
		});
    });
    </script>
');
$titrePage = "Retrait d'une annale";

require("inc/header.php");
?>
<h1>Effectuer un retrait</h1>
<?php
if(!isset($_POST['ID'])){
	echo '<p>
			<center>
					<form method="post" action="'.$racine.$module.'/'.$section.'">
					Chercher par numéro de commande ou login,<br />
					ou bien scanner directement le code-barre de la page de garde.<br /><br />
					<input type="text" id="suggest1" size="50" name="ID" /><br /><br />
					N\'apparaissent ici que les commandes payées, imprimées et non retirées.<br />
					<input type="submit" class="btn" value="Voir la commande" />
				</form>
			</center>
		</p>';
}
else{
	$id = mysqlSecureText($_POST['ID']);
	$noshow = false;

	if(strlen($id) == 10){
		$id = substr($id, 3);
		$noshow = true;
	}
    $id = intval($id);

	$req = query("SELECT * FROM polar_commandes WHERE DateRetrait IS NULL AND DatePaiement IS NOT NULL AND DatePrete IS NOT NULL AND ID = $id");
	$row_comm = mysql_fetch_assoc($req);
	if(@mysql_num_rows($req) != 1){
		echo "<script>alert(\"Cette commande n'existe pas, est déjà retirée, n'est pas payée ou n'est pas imprimée !\");document.location.href=\"$racine$module/$section\";</script>";
		die();
	}
	if($noshow){
		echo "<script>document.location.href='$racine$module/{$section}_control?idcommandevalider=$id';</script>";
		die();
	}
	else {
		echo '<div class="message"><center>
			<table>
			<tr><td>N&deg; de commande : </td><td>'.$row_comm['ID'].'</td></tr>
			<tr><td>Adresse de contact : </td><td>'.$row_comm['Mail'].'</td></tr>
			<tr><td>Annales command&eacute;es : </td><td><b>';

		$somme_annale = 0;

		$rqt_annale = query("SELECT pap.pages_code AS Code, pap.pages_types AS Types, pap.pages_nbrPages AS NbPages
			FROM polar_commandes_contenu pcc
			INNER JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
			WHERE pcc.IDCommande = ".$row_comm['ID']."
            AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types
			ORDER BY Code ASC");

        $notFirst = false;
		while($row_annale = mysql_fetch_assoc($rqt_annale)){
            if ($notFirst) {
                echo ' ; ';
            }
			echo $row_annale['Code'], ' - ', Annales::getTypesDescription($row_annale['Types'], Annales::$ANNALES_PAGES_TYPES_DESCRIPTIONS);

			$somme_annale += $row_annale['NbPages'];
            $notFirst = true;
		}

		echo "</b></td></tr>
			<tr><td>Total : </td><td>$somme_annale pages</td></tr>
			<tr><td>Date commande : </td><td>".$row_comm['DateCommande']."</td></tr>";

		echo "<tr><td>Date paiement : </td><td>";
		if($row_comm['DatePaiement'] != NULL)
			echo '<big style="color:green";>OUI</big> (le '.$row_comm['DatePaiement'].')</td></tr>';
		else
			echo '<big style="color:red;">NON</big></td></tr>';

		echo "<tr><td>Date impression : </td><td>";
		if($row_comm['DatePrete'] != NULL)
			echo '<big style="color:green";>OUI</big> (le '.$row_comm['DatePrete'].')</td></tr>';
		else
			echo '<big style="color:red;">NON</big></td></tr>';

		echo "<tr><td>Date retrait : </td><td>";
		if($row_comm['DateRetrait'] != NULL)
			echo '<big style="color:green";>OUI</big> (le '.$row_comm['DateRetrait'].')</td></tr>';
		else
			echo '<big style="color:red;">NON</big></td></tr>';

		echo '</table></form>
			</div>';

		echo '<center>
				<form method="get" action="'.$racine.$module.'/'.$section.'_control">
					<input type="hidden" name="idcommandevalider" value="'.$id.'" />
					<input type="submit" class="autofocus" id="buttonClicked" value="Marquer comme retiré" />
				</form>
				<form action="'.$racine.$module.'/'.$section.'" method="get">
					<input type="submit" value="Retour">
				</form>
			</center>';
	}
}

require("inc/footer.php");
?>


