<?php
require_once("_fonctions.php");
require_once("_constantes.php");

$requete = query("SELECT uv_code FROM polar_annales_uvs WHERE uv_enVente = 1 ORDER BY uv_code ASC");
$allannales = "";
while($u = mysql_fetch_assoc($requete))
  $allannales .= $u['uv_code'].",";
$allannales = substr($allannales, 0, -1);

addFooter("<script>
	var toutes = '".$allannales."';
	var i = 0;
	var finished = true;
	var annalestodo = [];

	function next(){
		if(i < annalestodo.length){
			$('#encours').append('<li>'+annalestodo[i]+'... <img src=\"$racine/styles/0/icones/loading.gif\" /></li>');
			$.ajax({
				url: '$racine$module/${section}_control?UV='+annalestodo[i],
				success: function(data){
					if(data.substring(0,2) == 'ok')
						$('#encours li:last').html('<li>'+annalestodo[i]+'... OK ('+data.substring(2)+' pages générées) !</li>');
					else
						$('#encours li:last').html('<li>'+annalestodo[i]+'... '+data+	'</li>');
					i++;
					next();
				}
			});
		}
		else{
		  $('#encours').append('<li>Terminé !</li>');
			finished = true;
		}
	}

	$(document).ready(function() {
		$('#addall').click(function(){
		  $('#annales').val(toutes);
		});

		$('#doannales').click(function(){
		  annalestodo = $('#annales').val().split(',');
		  if(annalestodo[0].length > 0){
  		  finished = false;
  		  $('#setup').hide();
  		  i = 0;
        next();
		  }
		  else
		    alert('Aucune annale saisie !');
		});

		$(window).bind('beforeunload', function(){
			if(!finished)
				return 'Traitement en cours, attendez avant de quitter.';
		});
  	});
</script>
");

$titrePage = 'Générer les annales';
require("inc/header.php");

?>

<h1>Générer les annales</h1>
<div id="setup">
  <p>
    <b>ATTENTION : générer une annale remplace le fichier qui est imprimé pour les commandes de cette annale. Donc toute commande
    antérieure non imprimée (qu'elle soit payée ou non) sera impactée. À ne jamais utiliser en cours de ventes donc, sauf extrême nécessité.</b>
  </p>
  <p>
    Annales à générer, séparées par des virgules (<a href="#" id="addall">ajouter toutes les UVs en vente</a>) :<br />
    <textarea id="annales" cols="80" rows="20"></textarea><br />
    <input type="button" value="Générer" id="doannales" class="btn" />
  </p>
</div>
<ul id="encours">

</ul>
<?php
require("inc/footer.php");
?>
