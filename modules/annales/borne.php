<?php

require_once('inc/annales.php');

if (isset($_GET['liste-annales'])) {
    $req = query("SELECT pau.uv_code AS Code, pap.pages_types AS Types, pap.pages_nbrPages AS NbrPages
FROM polar_annales_uvs pau
INNER JOIN polar_annales_pages pap ON pap.pages_code = pau.uv_code
WHERE pau.uv_enVente = 1
ORDER BY pau.uv_code ASC");


    $jsonArray = array();
    while ($data = mysql_fetch_assoc($req)) {
        $dataArr = array(Annales::getTypesDescription($data['Types'], Annales::$ANNALES_PAGES_TYPES_DESCRIPTIONS), intval($data['NbrPages']));
        if (isset($jsonArray[$data['Code']])) {
            $jsonArray[$data['Code']][$data['Types']] = $dataArr;
        } else {
            $jsonArray[$data['Code']] = array($data['Types'] => $dataArr);
        }
    }
    echo htmlspecialchars(json_encode($jsonArray), ENT_NOQUOTES);
}
else if (isset($_GET['details-annale'])) {

    $id = mysqlSecureText($_GET['details-annale']);

    $req = query("SELECT * FROM `polar_annales_sujets`
                 WHERE `sujet_uv`='".$id."'
                 AND sujet_ecarte=0
                 ORDER BY `sujet_type`, `sujet_annee`, `sujet_semestre` DESC");

    $jsonArray = array();
    while ($data = mysql_fetch_assoc($req)) {
        $pageType = Annales::$ANNALES_SUBJECT_PAGES_TYPES[$data['sujet_type']];
        if (!isset($jsonArray[$pageType])) {
            $jsonArray[$pageType] = array(Annales::$ANNALES_PAGES_TYPES_DESCRIPTIONS[$pageType], array());
        }
        if (!isset($jsonArray[$pageType][1][$data['sujet_type']])) {
            $jsonArray[$pageType][1][$data['sujet_type']] = array(Annales::$ANNALES_SUBJECT_TYPES[$data['sujet_type']], array());
        }
        $jsonArray[$pageType][1][$data['sujet_type']][1][] =
            array(intval($data['sujet_annee']), $data['sujet_semestre'],
                $data['sujet_corrige'] === '1');
    }
    echo htmlspecialchars(json_encode($jsonArray), ENT_NOQUOTES);

}
else if (isset($_GET['verif-login'])) {
    $login = mysqlSecureText($_GET['verif-login']);
    $jsonArray = array();
    if (strlen($login) > 0) {
        try {
            $users = $ginger->findPersonne($login);
            if(!isset($users->error)) {
              foreach ($users as $result) {
                  $jsonArray[] = $result->login;
              }
            }
            if (!in_array($login, $jsonArray)) {
                $user = $ginger->getUser($login);
                if (!isset($user->error)) {
                    $jsonArray[] = $login;
                }
            }
        } catch (ApiException $e) {}
    }
    echo htmlspecialchars(json_encode($jsonArray), ENT_NOQUOTES);

    $users = $ginger->findPersonne($login);
}
else if (isset($_GET['get-logins'])) {
  $users = $ginger->findPersonne('*');
  $logins = array();
  foreach ($users as $user) {
    $logins[] = $user->login;
  }

  echo htmlspecialchars(json_encode($logins), ENT_NOQUOTES);
}
else if (isset($_POST['uv0'])) {
  $login = mysqlSecureText($_POST['login']);
  try {
      $idCommande = Annales::commander_annales($login);
      $commande = Commande::getById($idCommande);
      CommandesMailer::validerCommande($commande);
  } catch (AnnalesException $e) {
    $idCommande = "";
  }

  echo $idCommande;
} else if (isset($_GET['get-preferences'])) {
    $req = query("SELECT PrixVente FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pct.CodeProduit = pca.CodeCaisse
	WHERE pct.Nom LIKE 'Annale' AND pct.Actif = 1 AND pca.Actif = 1 LIMIT 1");
    $data = mysql_fetch_assoc($req);
    $prixParPage = $data['PrixVente'];

    $req = query("SELECT Valeur
    FROM polar_parametres
    WHERE Nom = 'annales_default_types'");
    if (mysql_num_rows($req) > 0) {
        $data = mysql_fetch_array($req);
        $defaultTypes =  $data[0];
    } else {
        $defaultTypes = '';
    }

    $jsonArray = array('prixParPage' => $prixParPage,
        'defaultTypes' => $defaultTypes);

    echo htmlspecialchars(json_encode($jsonArray), ENT_NOQUOTES);

}

?>
