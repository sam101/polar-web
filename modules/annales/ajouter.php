<?php
$titrePage = "Ajout d'un sujet d'annales";
use_jquery_ui();
addFooter('
<script type="text/javascript">
$(document).ready(function() {
  $("#dialog").dialog({ width: 950, autoOpen: false, modal: true,
        close: function(event, ui) {
                 $("#action").val("delete");
				 informer("<span style=\"color: red; font-weight: bold;\">L\'importation a été interrompue. Réinitialisation en cours...</span>");
                 $("#save_form").submit();
      }});
  $("#save_form").submit(function() {
		compter_erreurs();
		if($("#action").val() == "delete")
			return true;
		else if(erreurs_presentes)
			return confirm("Les sujets contenant des erreurs ne seront PAS importés. Continuer quand même ?");
		else
			return true;
		
	});
  $("#erreur_dialog").dialog({autoOpen: false });
  $("#progress").hide();

  
  if ((typeof FileReader == "undefined") || !("draggable" in document.createElement("span")) || !window.FormData) {
    $("#holder").hide();
  }
  else {
    $("#old_browser").hide();
    $("#holder").bind({
      dragover: function () {
        $(this).addClass("hover");
        return false;
      },
      dragend: function () {
        $(this).removeClass("hover");
        return false;
      },
      drop: function (e) {
        $(this).removeClass("hover");
        e = e.originalEvent || e;
        e.preventDefault();
        readfiles(e.dataTransfer.files);
      }});
  }
});

var erreurs = [];
var erreurs_presentes = true;
var verif_en_cours = 0;

function check(value) {
  return (value) ? "checked=\'checked\'" : "";
}

function build_valeurs(id) {
  return {"UV" : $("input[name=UV"+id+"]").val(),
          "type" : $("select[name=type"+id+"]").val(),
          "annee" : $("input[name=annee"+id+"]").val(),
          "semestre" : $("select[name=semestre"+id+"]").val()};
}

function verifier_sujet(event) {
  var id = $(event.target).parent().parent().attr("id").replace("fichier", "");
  $("#erreur"+id).html("<img src=\''.$racine.'styles/0/icones/loading.gif\' />");
  action_supplementaire(id, "", "");
  faire_patienter("go");
  $.post("'.$racine.'annales/ajouter_control?Verifier", build_valeurs(id),
         function(data) {
           switch (data.erreur) {
             case "ok":
               generer_ok(id);
               break;
             case "uv-invalide":
               generer_erreur(id, "Le nom d\'UV n\'est pas valide.");
               break;
			 case "uv-inconnue":
			   action_supplementaire(id, "Créer une nouvelle UV", \'creer-uv\');
               generer_ok(id);
			   break;
             case "existe":
               generer_sujet_existe(id, data);
               break;
             default:
               generer_erreur(id, "Erreur inconnue, tu peux déposer un ticket et frapper le webmaster");
           }
		     faire_patienter("stop");
         }, "json");
}

function faire_patienter(action) {
	if(action == "go")
		verif_en_cours++;
	else if(action == "stop")
		verif_en_cours--;
		
	
	if(verif_en_cours > 0)
		  $("#savebutton").val("[ Patiente ... ]").attr("disabled", true);
	else
		$("#savebutton").val("Enregistrer").attr("disabled", false);
}

function nom_uv(data) {
	var annee = data.sujet_annee;
	if(annee >= 0 && annee < 10)
		annee = "0" + annee;
  return data.sujet_uv + "_" + data.sujet_type.toUpperCase() + data.sujet_semestre + annee;
}

function generer_sujet_existe(id, data) {

  var html = format("Ce sujet existe déjà : {nom_sujet} <br/>\
             {pages} page(s)<br/>\
             {corrige}<br/><br/>\
             Remplacez l\'ancien sujet uniquement si le nouveau est de meilleure qualité ou contient un corrigé<br/>\
             <a target=\'_blank\' href=\''.$racine.'annales/ajouter_control?VoirSujet&Tmp={tmp_file}\'>Nouveau sujet (pdf)</a><br/>\
             <a target=\'_blank\' href=\''.$racine.'annales/ajouter_control?VoirSujet&Old={old_file}\'>Ancien sujet (pdf)</a>",
             {nom_sujet: nom_uv(data), pages: data.sujet_pages, corrige: ((data.sujet_corrige) == 1 ? "Avec corrigé" : "Non corrigé"),
              tmp_file: $("input[name=fichier"+id+"]").val(), old_file: nom_uv(data) + ".pdf"});

  buttons = {"Utiliser le nouveau sujet" : function() {
                       //$.post("'.$racine.'annales/ajouter_control?SupprimerAncienSujet", data);
					   action_supplementaire(id, "Remplacer le sujet existant", \'remplacer\');
                       generer_ok(id);
                       $(this).dialog("close");
             }, "Garder l\'ancien sujet" : function() {
					   informer("Le sujet "+nom_uv(data)+" ne sera pas importé.");
                       $.post("'.$racine.'annales/ajouter_control", {"save" : "delete", "nb_sujets": 1, "fichier1" : $("input[name=fichier"+id+"]").val()});
                       $("#fichier"+id).remove();
                       $(this).dialog("close");
             }}
  generer_erreur(id, html, buttons);
}

function generer_erreur(id, message, buttons) {
  if(!buttons) {
   buttons = {}
  }

  erreurs[id] = {buttons: buttons,
              message: message,
              id: id};

  $("#erreur"+id).html("<img id=\'erreur_image"+id+"\' src=\''.$racine.'styles/'.$design.'/icones/exclamation.png\'"+
                       " title=\'Cliquez pour résoudre les erreurs\' alt=\'Erreur\'/>");
  $("#erreur_image"+id).click(function() {
    $("#erreur_dialog").dialog("option", "buttons", erreurs[id].buttons);
    $("#erreur_dialog").html(erreurs[id].message).dialog("open");
  });
  $("#est_erreur_"+id).val(1);
  compter_erreurs();
}

function generer_ok(id) {
  $("#est_erreur_"+id).val(0);
  $("#erreur"+id).html("<img src=\''.$racine.'styles/'.$design.'/icones/ok.png'.'\' alt=\'OK\' />");
  compter_erreurs();
}

function compter_erreurs() {
	var nb_sujets = parseInt($("#nb_sujets").val());
	var i;
	var total_erreurs = 0;
	for(i = 1; i <= nb_sujets; i++) {
		if(parseInt($("#est_erreur_"+i).val()) == 1)
			total_erreurs++;
	}
	
	erreurs_presentes = (total_erreurs > 0);
	
	/*if(erreurs_presentes)
		$("#savebutton").val("[ Corrige les erreurs pour valider ]").attr("disabled", true);
	else
		$("#savebutton").val("Enregistrer").attr("disabled", false);*/
}

function action_supplementaire(id, affichage, valeur) {
	$("#disp_action_supplementaire_" + id).text(affichage);
	$("#action_supplementaire_" + id).val(valeur);
}

function show_dialog(files) {
  for(var i=0; i<files.length; i++) {
    create_line(files[i], i+1, files.length);
  }

  $("#nb_sujets").val(files.length);
  informer(files.length + " sujets détectés. En attente des détails...");

  $("#dialog").dialog("open");

  // on bind l événement change de tous les input et select pour vérifier si le nom est valable ou si l uv existe déjà
  $("#dialog input, select").change(verifier_sujet);

  // on va maintenant générer un faux événement change sur tous les noms de sujet au cas où certaines lignes seraients pré-remplies
  $("[name^=UV]").each(function(index) {
    $(this).change();
  });
}

function options(possibilites, selected) {
  result = "";
  for (var t in possibilites) {
    result += "<option value=\'"+t+"\' ";
    if (t==selected) {
      result += "selected=\'selected\'";
    }
    result += ">"+possibilites[t]+"</option>";
  }
  return result;
}

types_possibles = {"T" : "T - Test 1", "U" : "U - Test 2", "V" : "V - Test 3",
                   "P" : "P - Partiel 1", "Q" : "Q - Partiel 2",
                   "M" : "M - Médian", "F" : "F - Final"};
semestres_possibles = {"A" : "Automne", "P" : "Printemps"};

function create_line(file, i, n) {
  if (file.NomOrig == undefined) {
    $("#liste_fichiers").append("<tr><td style=\"font-weight: bold; color: red;\">Erreur</td></tr>");
    return;
  }

  file = $.extend({Nom: "Erreur", UV: "", Semestre:"A", Corrige:false, Type:"M", Annee:12, NbPages: 0}, file);

  var template = "\
    <tr id=\'fichier{i}\'>\
      <td>{nomOriginal}</td>\
      <input type=\'hidden\' value=\'{nom}\' name=\'fichier{i}\' />\
      <td><input type=\'text\' maxlength=\'4\' size=\'4\' value=\'{uv}\' name=\'UV{i}\'/></td>\
      <td><select name=\'type{i}\'>\
      {optionsType}\
      </select></td>\
      <td><select name=\'semestre{i}\'>\
      {optionsSemestre}\
      </select></td>\
      <td><input type=\'text\' maxlength=\'2\' size=\'2\' value=\'{annee}\' name=\'annee{i}\'/></td>\
      <td><input type=\'checkbox\' {corrige} name=\'corrige{i}\'/></td>\
      <td><input type=\'text\' maxlength=\'2\' size=\'2\' value=\'{nbPages}\' name=\'nbpages{i}\'/></td>\
      <td><input type=\'text\' name=\'info{i}\'/></td>\
      <td><input type=\'text\'/ name=\'commentaire{i}\'></td>\
      <td><input type=\'checkbox\' checked=\'checked\' name=\'vendre{i}\'></td>\
	  <td>\
		<span id=\'disp_action_supplementaire_{i}\'></span>\
		<input type=\'hidden\' value=\'1\' id=\'est_erreur_{i}\' name=\'est_erreur_{i}\' />\
		<input type=\'hidden\' id=\'action_supplementaire_{i}\' name=\'action_supplementaire_{i}\' />\
	  </td>\
      <td id=\'erreur{i}\'></td>\
    </tr>";

  var result = format(template, {i: i, nomOriginal: file.NomOrig, nom: file.Nom, uv: file.UV,
                           annee: file.Annee, corrige: check(file.Corrige), nbPages: file.NbPages,
                           optionsType: options(types_possibles, file.Type),
                           optionsSemestre: options(semestres_possibles, file.Semestre)});

  $("#liste_fichiers").append(result);
}

function readfiles(files) {
  var formData = new FormData();
  for (var i = 0; i < files.length; i++) {
    if (files[i].type == "application/pdf" || /.pdf$/.test(files[i].name)) {
      formData.append("file"+i, files[i]);
    }
  }
  $("#holder").hide();
  $("#progress").show();
  
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "'.$racine.$module.'/'.$section.'_control?EnvoyerFichiers");
  xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
		//informer(xhr.responseText); //Debug
		  var result = eval(xhr.responseText); // Données textuelles récupérées
		  show_dialog(result);
	  }
  }
  
  xhr.upload.onprogress = function(e) {
        progress.value = e.loaded;
        progress.max = e.total;
		document.getElementById("progress_detail").innerHTML = e.loaded + "/" + e.total;
    };
  xhr.send(formData);
  informer("Envoi des fichiers en cours...");
}

function format(str, col) {
    col = typeof col === "object" ? col : Array.prototype.slice.call(arguments, 1);

    return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
        if (m == "{{") { return "{"; }
        if (m == "}}") { return "}"; }
        return col[n];
    });
};

function informer(message) {
	$("#infos").append("<br />" + message);
}

</script>
');
addHeaders('
<style>
#holder { border: 10px dashed #ccc; width: 300px; min-height: 300px; margin: 20px auto;}
#holder.hover { border: 10px dashed #0c0; }
progress { width: 70%; }
progress:after { content: "%"; }
.fail { background: #c00; padding: 2px; color: #fff; }
#liste_fichiers tr:hover { background-color: white; }
</style>
');

require("inc/header.php");
?>
<h1>Ajouter des sujets d'annales</h1>
<?php
	if(isset($_SESSION['notification'])) {
		echo $_SESSION['notification'];
		unset($_SESSION['notification']);
	}
?>
<p>Tu peux glisser-déposer les fichiers pdf de sujets d'annales ci-dessous.<br />
Il est déconseillé d'uploader trop de fichiers d'un coup (15 maximum...)</p>

<h2 id="old_browser">Votre navigateur ne supporte pas l'envoi de fichier par glisser-déposer, merci d'utiliser un navigateur plus récent</h2>
<div id="holder">
</div>
<br /><br />
<div id="progress_detail"></div>
<progress id="progress"></progress>
<br />
<p id="infos"></p>
<div id="dialog" title="Enregistrement des sujets envoyés">
  <form method="post" action="<?php echo $racine.$module.'/'.$section.'_control?SauvegarderOuSupprimer'; ?>" id="save_form">
    <input type="hidden" name="save" value="save" id="action"/>
    <table id="liste_fichiers">
      <th>Nom du fichier</th>
      <th>UV</th>
      <th>Type</th>
      <th>Semestre</th>
      <th>Année</th>
      <th>Avec corrigé</th>
      <th>Nbr pages</th>
      <th>Info (imprimé sur le sommaire)</th>
      <th>Commentaire (Non imprimé)</th>
      <th>En vente</th>
	  <th></th>
      <th>  </th>
      </table>
    <input type="hidden" id="nb_sujets" name="nb_sujets" value="0"/>
    <input id="savebutton" type="submit" class="btn" value="Enregistrer"/>
  </form>
</div>

<div id="erreur_dialog" title="Erreur sur les infos de l'UV">
</div>

<?php require("inc/footer.php"); ?>
