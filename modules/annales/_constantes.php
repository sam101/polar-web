<?php

define('PAGES_OFFERTES_PAR_SUJET', 15);
define('PAGES_OFFERTES_PAR_CORRECTION', 30);

define('ANNALES_CSS', '<style type="text/css">
td button {
	font-size: 0.75em;
	margin: 0px;
	padding-bottom: 0px;
	padding-top: 0px;
	padding-bottom: 0px;
	margin-top: 0px;
}
.info {
	border-left: 2px solid green;
	margin-left: 10px;
	padding-left: 5px;
	color: green;
}
.alerte {
	border-left: 2px solid orange;
	margin-left: 10px;
	padding-left: 5px;
	color: orange;
}
/************************* Tableaux ************************/

table.annalestab {
	border-collapse: collapse;
}

table.annalestab th {
	background-color: brown;
	font-family: Calibri;
	color: white;
	border: 1px solid white;
	padding: 3px;
}
table.annalestab td {
	border: brown 1px solid;
	padding: 2px;
}

td.code {
	font-weight: bold;
}
td.premiere_annale {
	font-size: 1.3em;
	color: brown;
}

td.lettre {
	background-color: orange;
	color: white;
	padding-left: 7px;
	font-weight: bold;
}


tr.mev_info {
	border: 2px solid green;
	background-color: lightgreen;
}
tr.mev_alerte {
	border: 2px solid #ff9900;
	background-color:  #ffff99;
}

/****************************** Formulaire de filtrage ******************/

#form_filtrer .multi-col {
	display: inline-block;
	vertical-align: top;
}
#form_filtrer h4 {
	font-size: 1.2em;
	text-align: center;
	margin-top: 3px;
	margin-bottom: 5px;
}

legend{
	font-weight: bold;
}

#form_filtrer fieldset {
	border-radius: 3px;
}
#form_filtrer fieldset div{
	margin-left: 50px;
}
#form_filtrer fieldset:hover {
	background-color: #eeeeee;
}
#form_filtrer legend input~label {
	color: grey;
}
#form_filtrer legend input:checked~label {
	color: black;
}

/****************************** Sommaires ********************************/

.uv {
	margin-top: 10px;
	border-bottom: 2px solid black;
	font-size: 18px;
	font-family: Impact;
	text-align: center;
}

.titre  {
	font-weight: bold;
	font-size: 48px;
}

.type {
	text-decoration: underline;
	font-size: 26px;
}

.colonnes {
	max-width: 30%;
	margin: 0px 15px;
	display: inline-block;
	vertical-align: top;
}

.message_bas {
	margin-top: 20px;
	font-size: 18px;
	font-family: Impact;
	text-align: center;
}
</style>');

define('IMPRESSION_CSS', '<style type="text/css">
.uv {
	font-family: impact;
	text-align: center;
}

.titre  {
	margin-top: 80px;
	font-weight: bold;
	font-size: 90px;
}

.type {
	text-decoration: underline;
	font-size: 50px;
}

.message_bas {
	font-size: 24px;
	font-family: impact;
	text-align: center;
}
.intercalaire {
  width: 99%;
  text-align: center;
  font-family: impact;
  font-size: 80pt;
}
.uvpdg {
  width: 99%;
  text-align: center;
  font-family: impact;
  font-size: 100pt;
}
.typespdg {
  width: 99%;
  text-align: center;
  font-family: impact;
  font-size: 40pt;
}

</style>');


$ANNALES_SUBJECT_TYPES = array('t' => 'Tests', 'u' => 'Tests 2', 'v' => 'Tests 3', 'p' => 'Partiels', 'q' => 'Partiels 2', 'm' => 'Médians', 'f' => 'Finaux');
$ANNALES_SUBJECT_TYPES_CODES = array_keys($ANNALES_SUBJECT_TYPES);


$ANNALES_PAGES_TYPES_DESCRIPTIONS = array('p' => 'Partiels', 'm' => 'Médians', 'f' => 'Finaux');
$ANNALES_PAGES_SUBJECT_TYPES = array('p' => array('t', 'u', 'v', 'p', 'q'), 'm' => array('m'), 'f' => array('f'));
$ANNALES_SUBJECT_PAGES_TYPES = array('t' => 'p', 'u' => 'p', 'v' => 'p', 'p' => 'p', 'q' => 'p', 'm' => 'm', 'f' => 'f');
