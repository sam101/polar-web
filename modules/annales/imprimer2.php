<?php

require_once('inc/annales.php');

// Shootage du timeout
$_SESSION['con-lastPrivateActive'] = time() + 6*3600;
$_SESSION['con-derniereactivite'] = time() + 6*3600;

addFooter("
<script>
    var IS_FOXIT_READER = true;
    var PDF_HTML_VALUE = '<head></head><body></body>';
    var PDF_LOAD_MAX_TIME = 20000;
    var PDF_WAIT_TRAITEMENT = 4000;

	var donnees = null;
	var pagesOk = 0, annalesOk = 0, lastId = 0, arret = 0;

    var pdfLoadTimer = null;
    var prevPdfUrl = null;

    var iframeIsPdf = function() {
        if (IS_FOXIT_READER) {
            try {
                var pdfContent = $('#pdf3').contents();
                var pdfHtmlVal = pdfContent.children().html();
                return pdfHtmlVal.search('application/pdf') !== -1;
            } catch(e) {}
            return false;
        } else {
            return true;
        }
    }

    var iframeLoaded = function() {
        if (pdfLoadTimer === null) {
            return;
        }
        clearTimeout(pdfLoadTimer);
        pdfLoadTimer = null;

        if (iframeIsPdf()) {
            endPdfPrint();
        } else {
            loadPdfTimeOut();
        }
    }

    var loadPdfTimeOut = function() {
        startPrintPdf(prevPdfUrl);
    }

    var startPrintPdf = function(pdfUrl) {
        prevPdfUrl = pdfUrl;
        $('#pdf3').attr('src', pdfUrl);
        pdfLoadTimer = window.setTimeout('loadPdfTimeOut();', PDF_LOAD_MAX_TIME);
    }

    var endPdfPrint = function() {
        if(donnees == null) {
            return;
        }
        var currentCommande = donnees.commandes[0];
        if(lastId != currentCommande.id){
            lastId = currentCommande.id;
        } else {
            pagesOk += currentCommande.pages;
            annalesOk++;
            // Supprimer l'annale donnees.commandes[0].contenu[0]
            currentCommande.contenu.splice(0, 1);
        }
        window.setTimeout('traitement();', PDF_WAIT_TRAITEMENT);
    }


	var traitement = function(){
		if(donnees.commandes.length == 0)
			$('#printingannales').append('<li><b>Impression terminée !</b></li>');
		else {
            var currentCommande = donnees.commandes[0];
			// Fin d'une commande (et marquage comme prête)
			if(currentCommande.contenu.length == 0){
				$('#printingannales').append('</li>');

				// Marquer la commande prête
				$.ajax({
					url: '$racine$module/${section}_control?MarquerPrete='+currentCommande.id,
					success: function(data){
						if(data == 'ok' && !arret)
							traitement();
						else if(arret){
							alert('Arrêt sur demande.');
							$('#printingannales').append('<li><b>Impression arrêtée.</b></li>');
						}
						else
							alert('Impossible de marquer la dernière commande comme prête. Arrêt.');
					},
					error: function(){alert('Impossible de marquer la dernière commande comme prête. Arrêt.');}
				});

				// Supprimer la commande donnees.commandes[0]
				donnees.commandes.splice(0, 1);

			} else { // Traitement des impressions

				// Si c'est la première UV de la commande
				if(lastId != currentCommande.id){
					$('#printingannales').append('<li>Commande '+ currentCommande.id +' : <span class=\"listeuv\"></span></li>');
					$('#printingannales .listeuv:last').append(' Page de garde');
					startPrintPdf('$racine$module/${section}_control?GetPdfCommande='+ currentCommande.id);
				} else {
					$('#printingannales .listeuv:last').append(' '+ currentCommande.contenu[0].uv);
					startPrintPdf('$racine$module/${section}_control?GetFile=' + currentCommande.contenu[0].uv);
				}

			}
		}
	}

	$(document).ready(function() {
		$('.print').click(function(e){
			e.preventDefault();
			$.getJSON('$racine$module/${section}_control?Commandes='+$(this).attr('rel')+'&id='+$('#unique').val(), null, function(data){
					if(data.error)
						alert(data.error);
					else {
						donnees = data;
						$('#welcome').hide();
						$('#stop').show();
						traitement();
					}
				}
			);
		});
		$('.printspecial').click(function(e){
			startPrintPdf('$racine$module/${section}_control?GetFile='+$('#special').val());
			$('#special').val('');
		});
        $('.printpdg').click(function(e){
			startPrintPdf('$racine$module/${section}_control?GetPdfCommande='+$('#page_de_garde').val());
		});
		$('#stop').click(function(e){
			arret = 1;
			$(this).attr('disabled', 'disabled').val('Arrêt à la fin de cette commande...');
		});


        $('#pdf3').load(function() {
            iframeLoaded();
        });


  	});

</script>");
require("inc/header.php");

$req = query("SELECT COUNT(pc.ID) AS Impaires, SUM(pap.pages_nbrPages) AS ImpairesPg FROM polar_commandes pc
	INNER JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
	INNER JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
	INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
	WHERE pc.DatePrete IS NULL
    AND pc.DatePaiement IS NOT NULL
    AND pct.Nom = 'Annale'
    AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types
    AND (pc.ID % 2) = 1");
$res = mysql_fetch_assoc($req);

$req = query("SELECT COUNT(pc.ID) AS Paires, SUM(pap.pages_nbrPages) AS PairesPg FROM polar_commandes pc
	INNER JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
	INNER JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
	INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
	WHERE pc.DatePrete IS NULL
    AND pc.DatePaiement IS NOT NULL
    AND pct.Nom = 'Annale'
    AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types
    AND (pc.ID % 2) = 0");
$res = array_merge($res, mysql_fetch_assoc($req));

$req = query("SELECT COUNT(pc.ID) AS Toutes, SUM(pap.pages_nbrPages) AS ToutesPg FROM polar_commandes pc
	INNER JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
	INNER JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
	INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
	WHERE pc.DatePrete IS NULL
    AND pc.DatePaiement IS NOT NULL
    AND pct.Nom = 'Annale'
    AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types");
$res = array_merge($res, mysql_fetch_assoc($req));


$req = query("SELECT pau.uv_code AS Code, pap.pages_types AS Types, pap.pages_nbrPages AS NbPages
FROM polar_annales_pages pap
INNER JOIN polar_annales_uvs pau ON pap.pages_code = pau.uv_code
WHERE pau.uv_enVente = 1 AND pap.pages_nbrPages > 0
ORDER BY Code, Types DESC");

$uvTypesPages = array();
while ($data = mysql_fetch_assoc($req)) {
    $uvTypesPages[] = $data;
}


?>
<h1>Impression des annales</h1>
<div id="welcome">
	<p>
		Le script qui va suivre va lancer automatiquement l'impression sur l'imprimante par défaut de cet ordinateur,
		avec les paramètres par défaut. Il est conseillé d'utiliser une session Annales mise en place avec le responsable
		du matériel informatique.
	</p>
	<p>
		Afin de réaliser l'impression, ce script charge des fichiers PDF contenant du code JavaScript. Il faut donc avoir
		installé dans ce navigateur un plugin lecteur de PDF autorisant l'exécution du JavaScript. Les tests ont été réalisés
		avec Foxit Reader, avec le mode <i>Trust</i> désactivé et JavaScript activé.
	<p>
		Choisissez les commandes à imprimer :
	</p>
	<ul>
		<li><a class="print" rel="paires" href="#">Imprimer les commandes paires</a> (<?php echo $res['Paires']; ?> annales/<?php echo $res['PairesPg']?> pages)</li>
		<li><a class="print" rel="impaires" href="#">Imprimer les commandes impaires</a> (<?php echo $res['Impaires']; ?> annales/<?php echo $res['ImpairesPg']?> pages)</li>
		<li><a class="print" rel="toutes" href="#">Imprimer toutes les commandes</a> (<?php echo $res['Toutes']; ?> annales/<?php echo $res['ToutesPg']?> pages)</li>
		<li>Commande unique : <input type="text" id="unique" class="in-texte" /> <a class="print" rel="unique" href="#">Lancer</a></li>

		<li>Annale unique :
            <select type="text" id="special" >
                <?php
                foreach ($uvTypesPages as $data) {
                    echo '<option value="',$data['Code'],'_',$data['Types'],'">',
                        $data['Code'], ' - ', Annales::getTypesDescription($data['Types'], Annales::$ANNALES_PAGES_TYPES_DESCRIPTIONS), ' ',
                        ' (',$data['NbPages'],' pages)</option>';
                }
                ?>
            </select>
            <a class="printspecial" rel="GetFile" href="#">Lancer</a></li>

        <li>Page de garde unique : <input type="text" id="page_de_garde" class="in-texte" /> <a class="printpdg" href="#">Lancer</a></li>
	</ul>
</div>
<div>
	<ul id="printingannales"></ul>
	<input type="button" value="Arrêter" id="stop" style="display:none;" /><br />
	<!--
	    Attention : il est strictement interdit de récupérer les fichiers des annales, pour quelque raison que ce soit.
	    Si le fichier sort du site, il est certain qu'il circulera très vite.
	    Tout abus des droits qui te sont attribués sur ce site fera l'objet de sanctions.
  -->
	<iframe id="pdf3" src="<?php echo "$racine$module/${section}_control?GetFile=blank"; ?>" style="border:0;width:0px;height:0px;"></iframe>
</div>
<?php
require("inc/footer.php");
?>
