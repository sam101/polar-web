<?php

// On définit quelques fonctions utiles
// Le reste du fichier est un gros if / else if / else if ...

function count_pages($pdfname) {
  $pdftext = file_get_contents($pdfname);
  $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
  return $num;
}

function get_text($champ, $i) {
  return mysqlSecureText($_POST[$champ.$i]);
}

function get_int($champ, $i) {
  return intval($_POST[$champ.$i]);
}

function get_check($champ, $i) {
  if(isset($_POST[$champ.$i]) AND get_text($champ, $i) == "on") {
    return 1;
  } else {
    return 0;
  }
}

function format_annee($annee) {
  $annee = $annee % 100;
  if ($annee < 10) {
    return "0".$annee;
  } else {
    return $annee;
  }
}

/* Fonctionnement de l'upload de sujets d'annales
   1 - __ EnvoyerFichiers __
       On dépose les annales, elles sont envoyées sur le serveur
       On regarde ensuite si le nom du fichier est exploitable
       On déplace les sujets dans le dossier upload/annales/sujets/tmp avec un nom unique type uuid
       On renvoie la liste des fichiers uploadés ainsi que les informations extraites
	   
   2 - __ SauvegarderOuSupprimer __ ($_POST["save"] == "save")
       La page ajouter.php renvoie les informations choisies par le resp annales
       On déplace les sujets au bon endroit et on les ajoute à la base
  
   3 - __ SauvegarderOuSupprimer __ ($_POST["save"] == "delete")
       Permet de supprimer certains fichiers temporaires par exemple
        si ou l'utilisateur ferme le dialog d'enregistrement des sujets ou
        si il uploade un fichier qui existait déjà et décide de garder l'ancien
   
   4 - __ Verifier __
       Appellé à chaque fois qu'un champs est modifié dans le dialog d'enregistrement
       On vérifie si le sujet existe pas déjà dans la base et si c'est le cas on renvoie les
        informations pour que le resp annales puisse choisir quel sujet garder
   
   5 - __ VoirSujet __
       Permet de voir le pdf des sujets en attente et des sujets existants
   

	
 */

 
if(isset($_GET['EnvoyerFichiers'])) {
	if(!empty($_FILES)) {
		//met tous les fichiers valides dans upload/annales/sujets/tmp
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		foreach ($_FILES as $x => $file) {
			if ($file["error"] == 0 and finfo_file($finfo, $file["tmp_name"]) == "application/pdf") {
				$cur_fichier = Array("NbPages" => count_pages($file["tmp_name"]),
									"NomOrig" => $file["name"],
									"Nom" => uniqid().".pdf");
				if (preg_match_all("/^(?<UV>[A-Z]{2}\d{2})_(?<Type>[TUVPQMFtuvpqmf]{1})(?<Semestre>[APap]{1})(?<Annee>\d{2})(?<Corrige>_c)?\.pdf$/", $file["name"], $matches)) {
					$cur_fichier["UV"] = $matches["UV"][0];
					$cur_fichier["Type"] = strtoupper($matches["Type"][0]);
					$cur_fichier["Semestre"] = strtoupper($matches["Semestre"][0]);
					$cur_fichier["Annee"] = $matches["Annee"][0];
					$cur_fichier["Corrige"] = $matches["Corrige"][0] == "_c";
				}
				$resultat[] = $cur_fichier;

				$dest_dir = $_SERVER['DOCUMENT_ROOT'].$racine."upload/annales/sujets/tmp/";
				if(!is_dir($dest_dir))
					mkdir($dest_dir);
				$destination = $dest_dir.$cur_fichier["Nom"];
				
				move_uploaded_file($file["tmp_name"], $destination);
			} else {
				$resultat[] = Array("Erreur" => "Erreur de transmission ou fichier non valide",
									"Nom" => $file["name"]);
			}
		}
		finfo_close($finfo);
		echo json_encode($resultat);
	}
	else {
		echo 'Aucun fichier envoye';
	}
}
elseif(isset($_GET['SauvegarderOuSupprimer'])) {
	if($_POST["save"] == "save") {
		//déplace dans le bon dossier et ajoute à la base

		$ajout = "INSERT INTO polar_annales_sujets(sujet_id, sujet_uv,
											   sujet_type, sujet_semestre,
											   sujet_annee, sujet_corrige,
											   sujet_pages, sujet_info,
											   sujet_commentaire, sujet_ecarte)
					VALUES "; // on va faire ça à la Rémi
		$maj = "UPDATE polar_annales_uvs SET `uv_modif` = NOW() WHERE ";
		$fichiers_potentiels = 0;
		$fichiers_valides = 0;
		$_SESSION['notification'] = '';
		$n = intval($_POST["nb_sujets"]);
		
		for ($i = 1; $i <= $n; $i++) {
			if (!isset($_POST["fichier".$i])) {
				continue;
			}
			$fichiers_potentiels++;

			if (get_int('est_erreur_', $i) == 1) continue;

			$fichier = str_replace("/", "", get_text("fichier", $i));

			$uv = strtoupper(get_text("UV", $i));
			if (!preg_match("/^[A-Z]{2}\d{2}$/", $uv)) continue;

			$type = strtoupper(get_text("type", $i));
			if (!in_array($type, Array("T", "U", "V", "P", "Q", "M", "F"))) continue;

			$semestre = strtoupper(get_text("semestre", $i));
			if (!in_array($semestre, Array("A", "P"))) continue;

			$annee = format_annee(get_int("annee", $i));
			$corrige = get_check("corrige", $i);
			$nbpages = get_int("nbpages", $i);
			$info = get_text("info", $i);
			$commentaire = get_text("commentaire", $i);
			$vendre = !get_check("vendre", $i);

			//déplacement du fichier
			$nom_final = $uv."_".$type.$semestre.$annee.".pdf";
			$new_path_dir = $_SERVER['DOCUMENT_ROOT'].$racine."upload/annales/sujets/".$uv[0]."/";
			mkdir($new_path_dir);
			$new_path = $new_path_dir.$nom_final;
			$old_path = $_SERVER['DOCUMENT_ROOT'].$racine."upload/annales/sujets/tmp/".$fichier;
			//echo $old_path."<br/>".$new_path."<br/>"; // DEBUG
			rename($old_path, $new_path);

			//enregistrement dans la base
			if ($fichiers_valides >= 1) {
				$ajout .= ", ";
				$maj .= " OR ";
			}
			
			switch($_POST['action_supplementaire_'.$i]) {
				case 'remplacer':
					$suppr[] = "(`sujet_uv` = '$uv' AND `sujet_type` = '$type' AND `sujet_semestre` = '$semestre' AND `sujet_annee` = $annee)";
					break;
				case 'creer-uv':
					$new_uv[] =  "('$uv', 'Inconnu', '', 1, NOW())";
						// On marque l'UV comme enseignée en inconnu par défaut. Avec la génération numérique des annales, ça n'a plus trop d'importance
					$_SESSION['notification'] .= '[Nouvelle UV] ';
					break;
			}

			$type = strtolower($type);
			$ajout .= " ('', '$uv', '$type', '$semestre', '$annee', $corrige, $nbpages, '$info', '$commentaire', '$vendre')";
			$maj .= " `uv_code`='$uv' ";
			$_SESSION['notification'] .= $uv.'_'.strtoupper($type).$semestre.$annee.'<br />';
			$fichiers_valides++;
		}
		if ($fichiers_valides > 0) {
			if(isset($suppr))
				query("DELETE FROM `polar_annales_sujets` WHERE ".implode($suppr, " OR "));
			if(isset($new_uv))
				query("INSERT INTO polar_annales_uvs(uv_code, uv_enseignee, uv_info, uv_enVente, uv_modif) VALUES".implode($new_uv, ","));
			query($ajout);
			query($maj);
				
			$_SESSION['notification'] = '<div style="color: green;"><b>'.$fichiers_valides.' fichiers ajoutés avec succès</b> :<br />'.$_SESSION['notification'].'</div>';
			if($fichiers_potentiels - $fichiers_valides > 0)
				$_SESSION['notification'] .= '<div style="color: red;"><b>'.($fichiers_potentiels - $fichiers_valides).' fichiers n\'ont pas pu être importés</div>';
		}
		else {
			$_SESSION['notification'] = '<div style="color: red;"><b>'.$fichiers_potentiels.' fichiers envoyés.<br />Aucun fichier n\'a été importé</b></div>';
		}

		// on redirige
		header("Location: $racine$module/$section");
	}
	elseif($_POST["save"] == "delete") {
		// on supprime les fichiers temporaires
		for ($i = 1; $i <= intval($_POST["nb_sujets"]); $i++) {
			if (!isset($_POST["fichier".$i])) {
				continue;
			}
			
			$path = $_SERVER['DOCUMENT_ROOT'].$racine."upload/annales/sujets/tmp/".str_replace("/", "", get_text("fichier", $i));
			unlink($path);

		}

		// on redirige
		header("Location: $racine$module/$section");
	}
}
elseif(isset($_GET["Verifier"])) {
	// Vérification de l'UV (nom valide + existence)
	$uv = get_text("UV", "");
	if (!preg_match("/^[A-Za-z]{2}\d{2}$/", $uv)) {
		echo '{"erreur": "uv-invalide"}';
		exit();
	}

	$req = query("SELECT COUNT(*) AS nbr_uv FROM polar_annales_uvs WHERE uv_code = '$uv'");
	$data = mysql_fetch_assoc($req);
	$nbr_uvs = intval($data['nbr_uv']);
		
	if($nbr_uvs < 1) {
		echo '{"erreur": "uv-inconnue"}';
		exit();
	}
	
	
	// Vérification du sujet (pas en double)
	$type = strtolower(get_text("type", ""));
	$semestre = get_text("semestre", "");
	$uv = strtoupper($uv);
	$annee = format_annee(get_int("annee", ""));

	$result = query("SELECT * FROM `polar_annales_sujets` WHERE `sujet_uv` = '$uv' AND `sujet_type` = '$type' AND `sujet_semestre` = '$semestre' AND `sujet_annee` = $annee");
	if (mysql_num_rows($result) == 0) {
		echo '{"erreur": "ok"}';
		exit();
	}
	else {
		$sujet = mysql_fetch_assoc($result);
		$sujet["erreur"] = "existe";
		echo json_encode($sujet);
		exit();
	}

}
elseif(isset($_GET["VoirSujet"])) {
  // Selon le type de sujet à envoyer on récupère le nom et l'emplacement
  if (isset($_GET["Tmp"])) {
    $nom = str_replace("/", "", mysqlSecureText($_GET["Tmp"]));
    $fichier = "upload/annales/sujets/tmp/".$nom;
  } else if (isset($_GET["Old"])) {
    $nom = str_replace("/", "", mysqlSecureText($_GET["Old"]));
    $fichier = "upload/annales/sujets/".$nom[0]."/".$nom;
  }

  // On envoie envoie le fichier en précisant son type et son nom
  if (is_file($fichier)) {
		header('Content-type: application/pdf');
    header('Content-Disposition: attachment; filename="'.$nom.'"');

		readfile($fichier);
  }
}



//il faut un moyen pour virer le javascript qu'il peut y avoir dans le fichier:
//si quelqu'un envoie un fichier, si un resp annales le vérifie alors qu'il est dans la session annales
//donc avec le trust mode activé, bah c'est la merde
?>
