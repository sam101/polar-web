<?php
require_once("inc/annales.php");
addHeaders(Annales::$ANNALES_CSS);
addFooter('
<script type="text/javascript">
/*****************************************************************
*****************************************************************/
/** Liste des UV **/
var objetModif = false;
var objetModifUv = false;
function modifierUv(objet, code, enseignee, info, enVente) {
	$(\'#ajouterUv\').attr(\'disabled\', true);
	$(\'#activerEnVente\').attr(\'disabled\', true);

	//On rétablit les couleurs d\'origines de l\'uv précédemmente modifié
	if(objetModif != false)
		objetModif.parentNode.parentNode.style.backgroundColor = \'\';
	objetModif = objet;

	// Remplissage du formulaire de modification
	var form = document.getElementById(\'form_modif\');
	form.modif_code.value = code;
	form.modif_code2.value = code;
	form.modif_enseignee.value = enseignee;
	form.modif_info.value = info.replace(/\\\'/g, \'\\\'\').replace(/\"/g, \'"\'); //Equivalent simplifié de striplaslashes() en PHP
	form.modif_enVente.checked = (enVente == 1 ? true : false);
	form.submitModif.disabled = \'\';

	//Coloration de la ligne en cours de modif
	objet.parentNode.parentNode.style.backgroundColor = \'lightblue\';

	form.style.display = \'block\';
	form.modif_nbrPages.focus();
}
function annulerModifUv() {
	objetModif.parentNode.parentNode.style.backgroundColor = \'\';
	objetModif = false;

	$(\'#ajouterUv\').attr(\'disabled\', false);
	$(\'#activerEnVente\').attr(\'disabled\', false);

	document.getElementById(\'submitModif\').disabled = true;
	document.getElementById(\'form_modif\').style.display = \'none\';
}

function ajouterUv() {
	$(\'#form_ajouter\').css(\'display\', \'block\');
	$(\'#ajouterUv\').attr(\'disabled\', true);
	$(\'#activerEnVente\').attr(\'disabled\', true);
	$(\'button.boutonModif\').attr(\'disabled\', true);
	$(\'#ajouter_code\').focus();
}
function annulerAjouterUv() {
	$(\'#form_ajouter\').css(\'display\', \'none\');
	$(\'#ajouterUv\').attr(\'disabled\', false);
	$(\'#activerEnVente\').attr(\'disabled\', false);
	$(\'button.boutonModif\').attr(\'disabled\', false);
}

function activerEnVente() {
	$(\'#ajouterUv\').attr(\'disabled\', true);
	$(\'td.tdEnVente input:checkbox\').attr(\'disabled\', false);
	$(\'td.tdEnVente\').css(\'background-color\', \'lightblue\');
	$(\'button.boutonModif\').attr(\'disabled\', true);
	$(\'#outilsModifEnVente\').css(\'display\', \'block\');
	$(\'#activerEnVente\').css(\'display\', \'none\');
}
function annulerEnVente() {
	if(confirm(\'Attention, toutes les modifications non validees seront perdues. Veux-tu continuer ?\')) {
		$(\'#ajouterUv\').attr(\'disabled\', false);
		$(\'td.tdEnVente input:checkbox\').attr(\'disabled\', true);
		$(\'td.tdEnVente\').css(\'background-color\', \'\');
		$(\'button.boutonModif\').attr(\'disabled\', false);
		$(\'#outilsModifEnVente\').css(\'display\', \'none\');
		$(\'#activerEnVente\').css(\'display\', \'inline\');
		return true;
	}
	else
		return false;
}
function cocherAppliquer() {
	var form = document.getElementById(\'form_modif_enVente\');
	var action = form.cocherAction.value;
	var filtre = form.cocherEnseignee.value;
	var cocher = (action == \'cocher\');

	if(filtre != \'Tout\')
		$(\'td:contains(" \'+filtre+\' ")+td input:checkbox\').attr(\'checked\', cocher);
	else
		$(\'td+td input:checkbox\').attr(\'checked\', cocher);
}
</script>
');
$titrePage = 'Liste des UVs';
require("inc/header.php");

echo '<h1>Liste des UV</h1>';

echo "<h2>N'oubliez pas de regénérer les Annales après modifications !</h2>";

afficherErreurs();
if(isset($_SESSION['notification'])) {
	echo $_SESSION['notification'];
	$mevCode = $_SESSION['mevCode']; //mev = mise en valeur
	unset($_SESSION['notification'], $_SESSION['mevCode']);
}
else {
	$mevCode = '';
}


if(isset($_SESSION['whereUv'])) {
	$whereUv = $_SESSION['whereUv'];
	$descWhereUv = $_SESSION['descWhereUv'];
}
else {
	$whereUv = "";
	$descWhereUv = "";
}



$requete = query("SELECT COUNT(*) AS nbrUvs FROM polar_annales_uvs WHERE 0 = 0 $whereUv");
$donnees = mysql_fetch_assoc($requete);
echo '<p>'.$descWhereUv.'<br /><strong>'.$donnees['nbrUvs'].'</strong> UVs trouvées.'; ?>
	<br />
	<button type="button" onclick="javascript: $('#form_filtrer').css('display', 'block');">Filtrer</button>
	<?php	if(isset($_SESSION['whereUv'])): ?>
  	<form method="post" id="form_filtrer" action="<?php echo "$racine$module"; ?>/lister_control">
  	  <input type="hidden" name="action_formulaire" value="raz_filtre" />
  		<input type="hidden" name="type" value="uvs" />
	    <input type="submit" value="Supprimer le filtre" />
	  </form>
	<?php endif; ?>
</p>

<p id="liste_lettres">
	<?php
	$lettres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	for($i = 0; $i <= 25; $i++) {
		echo '<a href="#'.$lettres[$i].'">'.$lettres[$i].'</a> ';
	}
	echo '<a href="#leBas">Bas</a>';
?>
</p>
<button type="button" id="ajouterUv" onclick="javascript: ajouterUv();">Ajouter une UV</button>
<button type="button" id="activerEnVente" onclick="javascript: activerEnVente();">Modifier les mises en vente</button>

<form method="post" id="form_modif_enVente" action="<?php echo "$racine$module/$section"; ?>_control">
	<fieldset id="outilsModifEnVente" style="display: none; position: fixed; top: 40%; left: 20%; background-color: lightblue;">
		<legend>Modifier les mises en vente</legend>
		<select name="cocherAction"><option value="cocher">Mettre en vente</option><option value="decocher">Enlever de la vente</option></select>
		toutes les UV enseignées en <select name="cocherEnseignee">
										<option value="A">A - Automne</option>
										<option value="P">P - Printemps</option>
										<option value="AP">AP - Automne et Printemps</option>
										<option value="Inconnu">? - Inconnu</option>
										<option value="Rempl">Rempl - UV remplacée</option>
										<option value="Fin">Fin - N'est plus enseignée</option>
										<option value="Tout">TOUT</option>
									</select>
		<button type="button" onclick="javascript: cocherAppliquer();">Appliquer</button><br />
		<input type="reset" onclick="javascript: return annulerEnVente();" value="Annuler" />
		<input type="submit" value="Valider les modifications de mise en vente" />
	</fieldset>
	<table class="annalestab">
	<tr>
		<th>Code</th>
		<th>Enseignée</th>
		<th>En Vente</th>
		<th>Sujets</th>
		<th>Pages après génération</th>
		<th>Info</th>
		<th>Dernière modif</th>
		<th>Action</th>
	</tr>

	<?php



	$requete = query("SELECT COUNT(pas.sujet_id) AS nbrSujetsTotal, SUM(pas.sujet_ecarte) AS nbrSujetsEcartes,
                    pau.uv_code, pau.uv_enseignee, pau.uv_info, pau.uv_enVente, pau.uv_modif
					FROM polar_annales_sujets pas
					RIGHT JOIN polar_annales_uvs pau ON pas.sujet_uv = pau.uv_code
					WHERE 0 = 0
                    $whereUv
					GROUP BY pau.uv_code
					ORDER BY pau.uv_code
					");
	$premiere_lettre = '';
	$liste_uvs = '';
	
    $typesPages = Annales::retrieveTypesPages(false, true);

	while ($donnees = mysql_fetch_assoc($requete)) {
        $uvCode = $donnees['uv_code'];

        $uvTypes = array();
        if(isset($typesPages[$uvCode])) {
            $uvTypes = $typesPages[$uvCode];
        }

		if($premiere_lettre != ucfirst(substr($uvCode, 0, 1))) {
			if($liste_uvs != '') {
				$liste_uvs .= '</optgroup>';
			}
			$premiere_lettre = ucfirst(substr($uvCode, 0, 1));
			$liste_uvs .= '<optgroup label="'.$premiere_lettre.'">';
			echo '<tr id="'.$premiere_lettre.'" style="border-top: 3px solid brown;'.($mevCode == $uvCode ? ' border: 2px solid green; background-color: lightgreen;' : '').'">';
			echo '<td style="font-weight: bold;"><a href="#liste_lettres" style="font-size: 1.5em;">'.$premiere_lettre.'</a>'.substr($uvCode, 1).'</td>';
		}
		else {
			echo '<tr'.($mevCode == $uvCode ? ' style="border: 2px solid green; background-color: lightgreen;"' : '').'>';
			echo '<td style="font-weight: bold;">'.$donnees['uv_code'].'</td>';
		}
		$liste_uvs .= '<option value="'.$uvCode.'">'.$uvCode.'</option>';

		echo '<td> '.$donnees['uv_enseignee'].' </td>'; //Les espaces sont importants !
		$nbrSujetsDisponibles = $donnees['nbrSujetsTotal'] - $donnees['nbrSujetsEcartes'];

		echo '<td class="tdEnVente"><input type="checkbox" style="width: 60px;" value="1" name="enVente_',$uvCode,'" ',($donnees['uv_enVente'] == 1 ? 'checked="checked"' : ''),' disabled="disabled" /></td>
			<td>',$nbrSujetsDisponibles,'</td>';

        echo '<td style="font-size: 0.9em; text-align: right;">';
        if (empty($uvTypes)) {
            echo '-';
        } else {
            foreach($uvTypes as $typesDescrPages) {
                echo $typesDescrPages[1].'<span style="display: inline-block; width: 50px; text-align: right;">'.$typesDescrPages[0].'</span><br />';
            }
        }
        echo '</td>';


		echo '<td>',$donnees['uv_info'],'</td>
			<td>',$donnees['uv_modif'],'</td>
            <td><button type="button" class="boutonModif" onclick="javascript: modifierUv(this, \'',$uvCode.'\', \'',($donnees['uv_enseignee'] == '?' ? 'Inconnu' : $donnees['uv_enseignee']),'\', \'',addslashes($donnees['uv_info']).'\', ',$donnees['uv_enVente'].')">Modif</button></td>
		</tr>';
        /*
			<td><button type="button" onclick="javascript: window.open(\'sommaires.php?uv='.$donnees['uv_code'].'\', \'sommaire-'.$donnees['uv_code'].'\', \'menubar=yes, width=600, height=400, scrollbars=yes\');">Voir</button>
             */
	}
echo '</table>';
echo '</form>';

/*************************************
Les formulaires (Modifier, Ajouter, Filtrer)
**************************************/
?>
<a id="leBas" href="#liste_lettres">Haut</a>
<!-- Modifier une UV -->
<form method="post" action="<?php echo "$racine$module/$section"; ?>_control" id="form_modif" style="display: none; position: fixed; top: 40%; left: 20%; background-color: lightblue;">
	<fieldset>
		<legend>Modifier</legend>
		<input type="hidden" name="modif_code" value="" />
		<input type="text" name="modif_code2" disabled="disabled" value="" size="4" />
		<select name="modif_enseignee" id="modif_type">
											<optgroup label="UV enseignée en">
												<option value="A">A - Automne</option>
												<option value="P">P - Printemps</option>
												<option value="AP">AP - Automne et Printemps</option>
												<option value="Inconnu" selected="selected">? - Inconnu</option>
												<option value="Rempl">Rempl - UV remplacée</option>
												<option value="Fin">Fin - N'est plus enseignée</option>
											</optgroup>
										</select><br />
		<input type="checkbox" name="modif_enVente" id="modif_enVente" /><label for="modif_enVente">En Vente</label><br />
		<label for="modif_info">Info </label><input type="text" id="modif_info" name="modif_info" value="" size="50" /><br />
		<input type="reset" value="Annuler" onclick="annulerModifUv()" /><input type="submit" name="submitModif" id="submitModif" value="Modifier" /><br />
	</fieldset>
</form>

<!-- Ajouter une UV -->
<form method="post" action="<?php echo "$racine$module/$section"; ?>_control" id="form_ajouter" style="display: none; position: fixed; top: 40%; left: 20%; background-color: lightgreen;">
	<fieldset>
		<legend>Ajouter</legend>
		<label for="ajouter_code">Code </label><input type="text" name="ajouter_code" id="ajouter_code" size="4" maxlength="4" /><br />
		<label for="ajouter_enseignee">UV enseignée en </label><select name="ajouter_enseignee" id="ajouter_enseignee">
												<option value="A">A - Automne</option>
												<option value="P">P - Printemps</option>
												<option value="AP">AP - Automne et Printemps</option>
												<option value="Inconnu" selected="selected">? - Inconnu</option>
												<option value="Rempl">Rempl - UV remplacée</option>
												<option value="Fin">Fin - N'est plus enseignée</option>
										</select><br />
		<input type="checkbox" name="ajouter_enVente" id="ajouter_enVente" /><label for="ajouter_enVente">En Vente</label><br />
		<label for="ajouter_info">Info </label><input type="text" id="ajouter_info" name="ajouter_info" value="" size="50" /><br />
		<input type="reset" value="Annuler" onclick="annulerAjouterUv()" /><input type="submit" name="submitAjout" id="submitAjout" value="Ajouter" /><br />
	</fieldset>
</form>

<!-- Filtrer les UVs -->
	<form method="post" id="form_filtrer" action="<?php echo "$racine$module"; ?>/lister_control" style="display: none; position: fixed; left: 20%; top: 20%; background-color: lightgray; border: 2px outset white;">
		<input type="hidden" name="action_formulaire" value="filtrer" />
		<input type="hidden" name="type" value="uvs" />
		<fieldset>
			<legend><input type="checkbox" name="condition_code" id="condition_code" /><label for="condition_code">Condition sur le code</label></legend>
			Ne garder que les UV dont le code est compris entre <input type="text" maxlength="4" size="5" name="code_debut" value="AA00" /> et <input type="text" maxlength="4" size="5" value="ZZ99" name="code_fin" />
		</fieldset>
		<fieldset>
			<legend><input type="checkbox" name="condition_enseignee" id="condition_enseignee" /><label for="condition_enseignee">Condition sur le semestre d'enseignement</label></legend>
			<input type="checkbox" name="enseignee_a" id="enseignee_a" /><label for="enseignee_a">A</label>
			<input type="checkbox" name="enseignee_p" id="enseignee_p" /><label for="enseignee_p">P</label>
			<input type="checkbox" name="enseignee_ap" id="enseignee_ap" /><label for="enseignee_ap">AP</label>
			<input type="checkbox" name="enseignee_inconnu" id="enseignee_inconnu" /><label for="enseignee_inconnu">?</label>
			<input type="checkbox" name="enseignee_rempl" id="enseignee_rempl" /><label for="enseignee_rempl">UV Remplacée</label>
			<input type="checkbox" name="enseignee_fin" id="enseignee_fin" /><label for="enseignee_fin">UV Finie</label>
		</fieldset>
		<fieldset>
			<legend><input type="checkbox" name="condition_modif" id="condition_modif" /><label for="condition_modif">Condition sur la dernière modification</label></legend>
				UV modifiée entre <input type="text" maxlength="17" size="17" value="0000-00-00 00:00" name="date_debut" /> et <input type="text" maxlength="17" size="17" name="date_fin" value="<?php echo date('Y-m-d H:i'); ?>" /><br />
				<em>Inscrire les dates au format aaaa-mm-jj hh:mm</em>
		</fieldset>
		<fieldset>
			<legend><input type="checkbox" name="condition_enVente" id="condition_enVente" /><label for="condition_enVente">Condition sur la mise en vente</label></legend>
				Afficher uniquement les UVs
				<input type="radio" name="enVente" id="enVente_oui" value="enVente_oui" checked="checked" /><label for="enVente_oui">en vente</label>
				<input type="radio" name="enVente" id="enVente_non" value="enVente_non" /><label for="enVente_non">pas en vente</label>
		</fieldset>
		<input type="reset" value="Annuler" onclick="javascript: $('#form_filtrer').css('display','none');" />
		<input type="submit" value="Filtrer" />
	</form>
<?php
require("inc/footer.php");
?>
