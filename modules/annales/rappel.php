<?php
$titrePage = "Mail de rappel";
require("inc/header.php");
?>

<h1>Envoyer un mail de rappel</h1>
<?php

$idLimit = 0;
if (!empty($_GET['idLimit'])) {
    $idLimit = intval($_GET['idLimit']);
}

$rqt_mail = query("SELECT ID, Mail
				FROM polar_commandes
				WHERE DatePaiement IS NOT NULL AND DatePrete IS NOT NULL AND DateRetrait IS NULL AND Termine = 0
				AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')
                AND ID >= $idLimit
				GROUP BY Mail
				ORDER BY ID");
echo '<form method="get" action="',$racine,$module,'/',$section,'">';
echo 'Filtrer en utilisant un ID minimal : <input type="text" name="idLimit" value="',$idLimit, '"/>';
echo ' <input type="submit" value="Filtrer" />';
echo '</form><br/>';



echo "<p>Ce formulaire va adresser un mail &agrave; chaque &eacute;tudiant dont les annales ont &eacute;t&eacute; imprim&eacute;es mais qu'il n'est pas venu retirer. Vous pouvez &eacute;diter
	ce courriel de rappel :</p>
	<p>%id% sera remplacé par le numéro de la commande !</p>
	<p>&nbsp;</p>";

echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control">
	<input type="hidden" name="done" value="ok" />
	<input type="hidden" name="idLimit" value="',$idLimit,'" />
	Sujet : <input type="text" size="50" maxlength="50" name="sujet_mail" value="[Le Polar] Tes annales sont prêtes !"><br />
	<textarea rows="8" cols="80" name="text_mail">Bonsoir,

Tes annales ont été imprimées ! Tu peux venir les retirer directement au Polar, en prenant soin de bien noter ton numéro de commande : %id%

À bientôt au Polar !


Ce message n\'a pas été généré automatiquement par Demeter.
Pour tout renseignement complémentaire, contacte polar-annales@assos.utc.fr
</textarea>
	<p><input type="submit" value="Envoyer le courriel" /></p>
	</form>
	<p>&nbsp;</p>';

echo "Destinataires (&agrave; titre d'information): <br><i><ul type=square>";

if(mysql_num_rows($rqt_mail) != 0){
	while($row_mail = mysql_fetch_array($rqt_mail))
		echo "<li>",$row_mail['ID'], ' - ', $row_mail['Mail'],"</li>";
}
else {
	echo "<big><font color=red><b>Aucun destinataire !</big></font></b>";
}
echo "</i></ul>";

require("inc/footer.php");
?>


