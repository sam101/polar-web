<?php
require_once('inc/annales.php');

$id = intval($_GET['idcommandevalider']);
$perm = intval($_SESSION['con-id']);
query("UPDATE polar_commandes SET DateRetrait = NOW(), IDRetrait = $perm, Termine = 1
	WHERE ID=$id AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')
	LIMIT 1");

$req = query("SELECT Mail FROM polar_commandes WHERE ID=$id");
$data = mysql_fetch_assoc($req);

$message .= "Bonjour,<br /><br />
Merci d'avoir commandé des annales au Polar !<br /><br />
Aide-nous à améliorer la qualité des annales en nous faisant part des problèmes que tu recontres (sujets illisibles, à l'envers, mal ordonnés, incomplets, ...). Tu aideras par la même occasion de nombreux étudiants.<br />
<br />
Après tes médians, tu peux également déposer ton sujet dans la boîte à Médians située au Polar, en indiquant ton login dans un coin ou au dos du sujet.<br />
Si tu es le premier ou la première à nous déposer ce sujet ou sa correction, tu bénéficeras de<br />
- ".Annales::$PAGES_OFFERTES_PAR_SUJET." pages offertes par nouveau sujet déposé,<br />
- ".Annales::$PAGES_OFFERTES_PAR_CORRECTION." pages offertes par nouveau corrigé déposé.<br />
Ces réductions sont cumulables et utilisables une fois sans limite de temps. Elles seront décomptées automatiquement lors de ta prochaine commande d'annales !<br />
<br />
<br />
Nous te souhaitons bon courage dans tes révisions !<br /><br />
L'équipe du Polar";
  sendMail('polar-annales@assos.utc.fr', 'Le Polar', array($data['Mail']), "[Le Polar] Commande d'annales terminée", $message);

echo '<script>alert("Retrait enregistré avec succès !");document.location.href="'.$racine.$module.'/'.$section.'";</script>';
?>
