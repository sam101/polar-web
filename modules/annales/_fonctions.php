<?php

require_once('_constantes.php');

function fabriquerAnnale($code, array $pagesTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES){
    require_once('lib/tcpdf/tcpdf.php');
    require_once('lib/fpdi/fpdi.php');

    $pagesTypesKey = array_keys($pagesTypes);
    $pagesTypesStr = implode('', $pagesTypesKey);
    $pagesTypesDescr = getTypesDescription($pagesTypesStr, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES);
    $sujetTypes = getSujetTypesFromPagesTypes($pagesTypesKey, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES);
    error_log($sujetTypes);
    $sujetTypesStr = implode('', $sujetTypes);


    $requete = query("SELECT sujet_uv, sujet_type, sujet_semestre, sujet_annee, sujet_corrige, sujet_commentaire
                        FROM polar_annales_sujets
                        WHERE sujet_uv LIKE '$code' AND sujet_ecarte = 0
                        AND sujet_type RLIKE '[$sujetTypesStr]'
                        ORDER BY sujet_uv, sujet_type, sujet_annee, sujet_semestre DESC, sujet_corrige");
    $type = '';

    $files = array();
    $sommaire = IMPRESSION_CSS;

    // Ajout de la phrase de pub pour la boîte à médians
    $sommaire .= '
    <div class="uv">
        <span class="titre">Annales '.$code.'</span><br />Version '.getSemestre().'<br />';

    while($donnees = mysql_fetch_assoc($requete)) {
    if($donnees['sujet_type'] != $type) {
        if(in_array($type, $ANNALES_SUBJECT_TYPES_CODES))
            $sommaire .= '</p>';

        $type = $donnees['sujet_type'];
        if(in_array($type, $ANNALES_SUBJECT_TYPES_CODES))
            $sommaire .= '<p class="colonnes">';
        else
            $sommaire .= '<p>';

        $sommaire .= '<span class="type">'.$ANNALES_SUBJECT_TYPES[$donnees['sujet_type']].'</span><br />';
    }
    $sommaire .= $donnees['sujet_semestre'].($donnees['sujet_annee'] < 10 ? '0'.$donnees['sujet_annee'] : $donnees['sujet_annee']);
    if($donnees['sujet_corrige'] == 1)
        $sommaire .= ' + Corrigé';

    if(!empty($donnees['sujet_info']))
        $sommaire .= '<span style="margin-left: 30px;">'.$donnees['sujet_info'].'</span>';

    $sommaire .= '<br />';

    // On ajoute le fichier dans la liste des fichiers à inclure
    $files[] = array($type, strtoupper(substr($code, 0, 1).'/'.$code.'_'.$donnees['sujet_type'].$donnees['sujet_semestre'].sprintf("%02d", $donnees['sujet_annee'])));
    }

    $sommaire .= '</div>';
    $sommaire .= '<div class="message_bas">Les annales sont disponibles grâce à TOI !<br />
    Après les examens, dépose ton sujet dans la boîte à médians située au Polar !</div>';

    $pdf = new FPDI();
    $pagetotal = 0;

    $pdf->SetFontSubsetting(true);

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // Ajout de la page de garde
    $pagecount = $pdf->setSourceFile("upload/annales/pdg.pdf");
    $tplidx = $pdf->ImportPage(1);
    $s = $pdf->getTemplatesize($tplidx);
    $pdf->AddPage('P', array($s['w'], $s['h']));
    $pdf->useTemplate($tplidx);
    $pdf->writeHTML(IMPRESSION_CSS."<br /><div class=\"uvpdg\"><br /><br /><br />$code</div><div class=\"typespdg\">$pagesTypesDescr</div>", $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
    $pagetotal++;

    // Ajout du sommaire
    $pdf->AddPage('P');
    $pdf->writeHTML($sommaire, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
    $pagetotal++;

    // Import de l'annale
    $typeactuel = '';
    foreach($files as $file){
      if($file[0] != $typeactuel){
        // Ajout de l'intercalaire
      $pdf->AddPage('P');
      $intercalaire = IMPRESSION_CSS.'<div class="intercalaire"><br /><br /><br />'.$ANNALES_SUBJECT_TYPES[$file[0]].'</div>';
      $pdf->writeHTML($intercalaire, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
      $pagetotal++;
      $typeactuel = $file[0];
      }
    $pagecount = $pdf->setSourceFile("upload/annales/sujets/{$file[1]}.pdf");
    for ($i = 1; $i <= $pagecount; $i++) {
        $tplidx = $pdf->ImportPage($i);
        $s = $pdf->getTemplatesize($tplidx);
        $pdf->AddPage('P', array($s['w'], $s['h']));
        $pdf->useTemplate($tplidx);
    }
    $pagetotal += $pagecount;
    }

    // Ajout du print automatique
    $pdf->IncludeJS("print(false, true);");

    // On sauvegarde le fichier
    $pdf->Output("upload/annales/{$code}_{$pagesTypesStr}.pdf", 'F');

    unset($pdf);

    $update = query("INSERT INTO polar_annales_pages (pages_code, pages_types, pages_nbrPages)
        VALUES ('$code', '$pagesTypesStr', $pagetotal)
        ON DUPLICATE KEY UPDATE pages_nbrPages = $pagetotal");
    return $pagetotal;
}

function getSujetTypesFromPagesTypes($pagesTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES) {
    $retArray = array();
    foreach($pagesTypes as $p) {
        $retArray = array_merge($retArray, $ANNALES_PAGES_SUBJECT_TYPES[$p]);
    }
    return $retArray;
}

function getPagesTypesFromSujetTypes($sujetTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES) {

    $retArray = array();
    foreach ($sujetTypes as $s) {
        $pageType = $ANNALES_SUBJECT_PAGES_TYPES[$s];
        if (isset($retArray[$pageType])) {
            $retArray[$pageType][] = $s;
        } else {
            $retArray[$pageType] = array($s);
        }
    }
    return $retArray;
}

function getSujetTypesForAnnales($code, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES) {
    $req = query("SELECT sujet_type
FROM polar_annales_sujets
WHERE sujet_uv = '$code'
AND sujet_ecarte = 0
GROUP BY sujet_type");
    $retArr = array();
    while ($data = mysql_fetch_array($req)) {
        $retArr[] = $data[0];
    }
    return $retArr;
}


function array_power_set($array) {
    // initialize by adding the empty set
    $results = array(array());

    foreach ($array as $element) {
        foreach ($results as $combination) {
            $results[] = array_merge($combination, array($element));
        }
    }
    array_shift($results);
    return $results;
}

function getPagesTypesCombinations(array $pagesTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES) {
    $retArray = array();
    while (count($pagesTypes) !== 0) {
        $retArray[] = $pagesTypes;
        array_shift($pagesTypes);
    }
    return $retArray;
}

function getSujetPagesTypesCombinations(array $sujetTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES) {
    $pagesTypes = getPagesTypesFromSujetTypes($sujetTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES);
    return getPagesTypesCombinations($pagesTypes, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES, $ANNALES_SUBJECT_PAGES_TYPES);
}

function getTypesDescription($types, $typesDescriptions) {
    if(!is_array($types)) {
        $typesExploded = str_split($types);
    } else {
        $typesExploded = $types;
    }

    $retStr = '';
    $notFirst = false;
    foreach ($typesExploded as $type) {
        if ($notFirst) {
            $retStr .= ' + ';
        }
        $retStr .= $typesDescriptions[$type];
        $notFirst = true;
    }

    return $retStr;
}

function retrieveTypesPages($onlyActives = true, $reduit = false, $ANNALES_SUBJECT_TYPES, $ANNALES_SUBJECT_TYPES_CODES, $ANNALES_PAGES_TYPES_DESCRIPTIONS, $ANNALES_PAGES_SUBJECT_TYPES) {
    $query = 'SELECT pau.uv_code AS Code, pap.pages_types AS Types, pap.pages_nbrPages AS NbPages
FROM polar_annales_pages pap
INNER JOIN polar_annales_uvs pau ON pap.pages_code = pau.uv_code';
    if ($onlyActives) {
        $query .= '
WHERE pau.uv_enVente = 1 AND pap.pages_nbrPages > 0';
    }
    $query .= '
ORDER BY Code, NbPages DESC, Types DESC';
    $req = query($query);

    $retArr = array();

    while($data = mysql_fetch_assoc($req)) {
        $code = $data['Code'];
        $types = $data['Types'];
        $nbPages = intval($data['NbPages']);

        if (isset($retArr[$code])) {
			if($reduit)
				$retArr[$code][$types] = array(strtoupper($types), $nbPages);
			else
				$retArr[$code][$types] = array(getTypesDescription($types, $ANNALES_PAGES_TYPES_DESCRIPTIONS), $nbPages);
        } else {
			if($reduit)
				$retArr[$code] = array($types => array(strtoupper($types), $nbPages));
			else
				$retArr[$code] = array($types => array(getTypesDescription($types, $ANNALES_PAGES_TYPES_DESCRIPTIONS), $nbPages));
        }
    }

    return $retArr;
}

?>
