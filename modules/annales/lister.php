<?php
$titrePage = 'Liste des sujets';
require("inc/annales.php");
addHeaders(Annales::$IMPRESSION_CSS);
addFooter('
<script type="text/javascript">
  var objetModif = false;
  var objetSuppr = false;
  function inverserAffichage() {
  	var lettres = \'ABCDEFGHIJKLMNOPQRSTUVWXYZ\';
  	for(var i = 0; i < 26; i++) {
  		if(document.getElementById(lettres[i]) != null)
  			afficherListeSujets(lettres[i]);
  	}
  }
  function afficherListeSujets(lettre) {
  	var Bouton = document.getElementById(\'bouton\'+lettre).firstChild;
  	if(Bouton.nodeValue == \'+\') {
  		Bouton.nodeValue = \'-\';
  		var nouvelAffichage = \'table-row\';
  	}
  	else {
  		Bouton.nodeValue = \'+\';
  		var nouvelAffichage = \'none\';
  	}

  	$(\'tr.\'+lettre+\'XXX\').css(\'display\', nouvelAffichage);
  }


  function modifierSujet(objet, id, uv, type, semestre, corrige, pages, info, commentaire, ecarte) {
  	//On annule la suppression avant d\'afficher le formulaire de modification
  	if(document.getElementById(\'form_suppr\').style.display == \'block\')
  		annulerSuppr();

  	//On rétablit les couleurs d\'origines du sujet précédemment modifié (s\'il existe)
  	if(objetModif != false)
  		objetModif.parentNode.parentNode.style.backgroundColor = \'\';
  	objetModif = objet;

  	// Remplissage du formulaire de modification
  	var form = document.getElementById(\'form_modif\');
  	form.modif_id.value = id;
  	form.modif_id2.value = id;
  	form.modif_uv.value = uv;
  	form.modif_type.value = type;
  	form.modif_semestre.value = semestre;
  	form.modif_corrige.checked = (corrige == 1 ? true : false);
  	form.modif_pages.value = pages;
  	form.modif_ecarter.checked = (ecarte == 1 ? true : false);
  	form.modif_info.value = info.replace(/\\\'/g, \'\\\'\').replace(/\"/g, \'"\');
  	form.modif_commentaire.value = commentaire.replace(/\\\'/g, \'\\\'\').replace(/\"/g, \'"\');
  	form.submitModif.disabled = false;

  	//Coloration de la ligne en cours de modif
  	objet.parentNode.parentNode.style.backgroundColor = \'lightblue\';

  	form.style.display = \'block\';
  	form.modif_pages.focus();
  }

  function annulerModif() {
  	objetModif.parentNode.parentNode.style.backgroundColor = \'\';
  	objetModif.focus();
  	objetModif = false;

  	document.getElementById(\'submitModif\').disabled = true;
  	document.getElementById(\'form_modif\').style.display = \'none\';
  }

  function supprimerSujet(objet, id) {
  	//Si une modification est en cours, on annule la modification avant d\'afficher la confirmation de suppression
  	if(document.getElementById(\'form_modif\').style.display == \'block\')
  		annulerModif();

  	//On rétablit les couleurs d\'origines du sujet précédemment modifié (s\'il existe)
  	if(objetSuppr != false)
  		objetSuppr.parentNode.parentNode.style.backgroundColor = \'\';
  	objetSuppr = objet;

  	// Remplissage du formulaire de modification
  	var form = document.getElementById(\'form_suppr\');
  	form.suppr_id.value = id;
  	form.suppr_id2.value = id;

  	objet.parentNode.parentNode.style.backgroundColor = \'pink\';

  	form.submitSuppr.disabled = false;
  	form.style.display = \'block\';
  }

  function annulerSuppr() {
  	objetSuppr.parentNode.parentNode.style.backgroundColor = \'\';
  	objetSuppr = false;

  	document.getElementById(\'submitSuppr\').disabled = true;
  	document.getElementById(\'form_suppr\').style.display = \'none\';
  }

  function filtragePersonnalise(nom) {
  	var form = document.getElementById(\'form_filtrer\');

  	if(nom == \'NonScannesEnVente\') {
  		/*form.condition_enseignee.checked = true;
  			form.enseignee_p.checked = true;
  			form.enseignee_ap.checked = true;
  			form.enseignee_inconnu.checked = true;
  			form.enseignee_rempl.checked = true;
  			form.enseignee_fin.checked = true;*/
  		form.condition_pages.checked = true;
  			form.pages_mini.value = \'0\';
  			form.pages_maxi.value = \'0\';
  		form.condition_enVente.checked = true;
  			form.enVente_oui.checked = \'checked\';
  		form.condition_ecartes.checked = true;
  			form.ecartes_non.checked = \'checked\';
  	}

  }
</script>
');
require("inc/header.php");

echo '<h1>Liste des Sujets</h1>';


afficherErreurs();

if(isset($_SESSION['notification'])) {
	echo $_SESSION['notification'];
	$mevId = $_SESSION['mevId']; //mev = mise en valeur
	$afficherLettre = $_SESSION['afficherLettre']; // Pour afficher automatiquement une lettre

	unset($_SESSION['notification'], $_SESSION['mevId'], $_SESSION['afficherLettre']);
}
else {
	$mevId = '';
	$afficherLettre = '';
}

// Récupération des critères de filtrage
if(isset($_SESSION['whereSujet'])) {
	$whereSujet = $_SESSION['whereSujet'];
	$whereUv = $_SESSION['whereUv'];
	$descWhereSujet = $_SESSION['descWhereSujet'];
	$descWhereUv = $_SESSION['descWhereUv'];
	$jointures = $_SESSION['jointures'];
	$champsSupJointures = $_SESSION['champsSupJointures'];
}
else {
	$whereSujet = "";
	$descWhereSujet = "";
	$whereUv = "";
	$descWhereUv = "";
	$jointures = "";
	$champsSupJointures = "";
}

	?>
	<p>
		<?php
		echo $descWhereSujet.$descWhereUv.'<br />';
		// Comptons le nombre de résultats
		$requete = query("SELECT COUNT(*) AS nbrSujets, SUM(sujet_ecarte) AS nbrSujetsEcartes, COUNT(DISTINCT sujet_uv) AS nbrUvs
								FROM polar_annales_sujets pas
								LEFT JOIN polar_annales_uvs pau ON pas.sujet_uv = pau.uv_code
								WHERE 0 = 0
									$whereSujet
									$whereUv");
		$donnees = mysql_fetch_assoc($requete);
		?>
		<strong><?php echo $donnees['nbrSujets']; ?></strong> sujets trouvés (<strong><?php echo $donnees['nbrUvs']; ?></strong> UVs)  dont <?php echo $donnees['nbrSujets'] - $donnees['nbrSujetsEcartes']; ?> sujets en vente.<br />
		<button type="button" onclick="javascript: $('#form_filtrer').css('display', 'block'); $('#condition_type').focus();">Filtrer</button>
  	<?php	if(isset($_SESSION['whereUv'])): ?>
    	<form method="post" id="form_filtrer" action="<?php echo "$racine$module"; ?>/lister_control">
    	  <input type="hidden" name="action_formulaire" value="raz_filtre" />
    		<input type="hidden" name="type" value="sujets" />
  	    <input type="submit" value="Supprimer le filtre" />
  	  </form>
  	<?php endif; ?>
	</p>


	<?php
	echo '<p id="liste_lettres">';
		$lettres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		for($i = 0; $i <= 25; $i++) {
			echo '<a href="#'.$lettres[$i].'">'.$lettres[$i].'</a> ';
		}
		echo '<a href="#leBas">Bas</a>';
	echo '</p>';


	/**********************
	Formulaire d'ajout
	*********************/
	//Préparation de la liste des UVs
	$liste_uvs = '';
	$requete = query("SELECT uv_code FROM polar_annales_uvs ORDER BY uv_code");
	while ($donnees = mysql_fetch_assoc($requete)) {
		$liste_uvs .= '<option value="'.$donnees['uv_code'].'">'.$donnees['uv_code'].'</option>';
	}
	?>
	<!-- Ajouter des sujets -->
	<!--<button type="button" onclick="javascript: $('#form_ajouter').css('display', 'block'); $(this).css('display', 'none'); $('select[name=\'uv_1\']').focus();">Ajouter des sujets</button>
	<form method="post" action="<?php echo "$racine$module/$section"; ?>_control" id="form_ajouter" style="display: none;">
		<fieldset>
			<legend>Ajouter des sujets</legend>
			<input type="hidden" name="action_formulaire" value="ajout" />
			<span style="font-size: 0.8em; font-style: italic;">L'<strong>info</strong> apparaît sur le sommaire ; le <strong>commentaire</strong> n'est présent que sur la BDD.</span><br />
			<?Php
			for($i = 1; $i <= 5; $i++) {
				echo '<strong>'.$i.'. </strong>';
				echo '<select name="uv_'.$i.'"><option value="-">-UV-</option>'.$liste_uvs.'</select>';
				echo '<select name="type_'.$i.'">
						<option value="-">-Type-</option>
						<option value="t">T - Test (1)</option>
						<option value="u">U - Test 2</option>
						<option value="v">V - Test 3</option>
						<option value="p">P - Partiel (1)</option>
						<option value="q">Q - Partiel 2</option>
						<option value="m">M - Médian</option>
						<option value="f">F - Final</option>
					</select>';

				echo ' Semestre <input name="semestre_'.$i.'" type="text" maxlength="3" size="4" />';
				echo ' <input type="checkbox" name="corrige_'.$i.'" id="corrige_'.$i.'" /><label for="corrige_'.$i.'">Corrigé</label>';
				echo "\t".' <label for="pages_'.$i.'">Pages</label><input type="text" name="pages_'.$i.'" id="pages_'.$i.'" maxlength="3" size="4" />';
				echo "\t".' Info <input name="info_'.$i.'" type="text" maxlength="50" />';
				echo "\t".' Commentaire <input name="commentaire_'.$i.'" type="text" maxlength="50" /><br />';
			}
			?>
			<input type="submit" value="Ajouter" />
		</fieldset>
	</form><br /><br />
-->

	<?php
	/***************************************************************************
	Le Tableau
	***************************************************************************/
	echo '<button type="button" onclick="javascript: inverserAffichage();">Inverser l\'affichage</button>';
	$requete = query("SELECT sujet_id, sujet_uv, sujet_type, sujet_semestre, sujet_annee, sujet_corrige, sujet_pages, sujet_info, sujet_commentaire, sujet_ecarte
								$champsSupJointures
							FROM polar_annales_sujets
							$jointures
							WHERE 0 = 0
								$whereSujet
								$whereUv
							ORDER BY sujet_uv, sujet_type, sujet_annee, sujet_semestre DESC");

	echo '<table class="annalestab">
			<tr>
				<th>UV</th>
				<th>Type</th>
				<th>Semestre</th>
				<th>Corrigé</th>
				<th>Pages</th>
				<th>Info</th>
				<th>Commentaire</th>
				<th>Modif</th>
				<th>Suppr</th>
			</tr>';


			$uv = '';
			$premiere_lettre = '';
			while($donnees = mysql_fetch_assoc($requete)) {
				if($donnees['sujet_uv'] != $uv) {
					$uv = $donnees['sujet_uv'];

					if(substr($donnees['sujet_uv'], 0, 1) != $premiere_lettre) {
						$premiere_lettre = substr($uv, 0, 1);
						echo '<tr id="'.$premiere_lettre.'"><td colspan="9" class="lettre"><span style="float: right;"><a href="#liste_lettres">Haut</a> <button type="button" id="bouton'.$premiere_lettre.'" onclick="javascript: afficherListeSujets(\''.$premiere_lettre.'\');">+</button></span>'.$premiere_lettre.'</td></tr>';
					}

					//Après une modification / suppression, on affiche automatiquement les UV commençant par la même lettre (à l'aide de $afficher) et on met en valeur le sujet qui vient d'être modifié (avec $mevId)
					$afficher = ($premiere_lettre == $afficherLettre ? 'table-row' : 'none');
					echo '<tr class="'.$premiere_lettre.'XXX" id="'.$donnees['sujet_uv'].'"
							style="display: '.$afficher.';'
										.($donnees['sujet_ecarte'] == 1 ? ' color: grey;' : '')
										.($donnees['sujet_id'] == $mevId ? ' border: 2px solid green; background-color: lightgreen;' : '').'">
							<td class="code premiere_annale">'.$uv.'</td>';
				}
				else {
					echo '<tr class="'.$premiere_lettre.'XXX" id="'.$donnees['sujet_uv'].'"
							style="display: '.$afficher.';'
										.($donnees['sujet_ecarte'] == 1 ? ' color: grey;' : '')
										.($donnees['sujet_id'] == $mevId ? ' border: 2px solid green; background-color: lightgreen;' : '').'">
							<td class="code">'.$donnees['sujet_uv'].'</td>';
				}
					$semestre = $donnees['sujet_semestre'].($donnees['sujet_annee'] < 10 ? '0'.$donnees['sujet_annee'] : $donnees['sujet_annee']);

					echo '<td>'.$donnees['sujet_type'].'</td>';
					echo '<td>'.$semestre.'</td>';
					echo '<td>'.($donnees['sujet_corrige'] == 1 ? 'Oui' : '-').'</td>';
					echo '<td'.(($donnees['sujet_pages'] == 0 AND $donnees['sujet_ecarte'] != 1) ? ' style="background-color: red; color: white; font-weight: bold;"' : '').' >'.$donnees['sujet_pages'].'</td>';
					echo '<td>'.(empty($donnees['sujet_info']) ? '-' : $donnees['sujet_info']).'</td>';
					echo (empty($donnees['sujet_commentaire']) ? '<td>-</td>' : '<td style="background-color: yellow;">'.$donnees['sujet_commentaire'].'</td>');
					echo '<td><button type="button" onclick="javascript: modifierSujet(this, '.intval($donnees['sujet_id']).', \''.$donnees['sujet_uv'].'\', \''.$donnees['sujet_type'].'\', \''.$semestre.'\', '.$donnees['sujet_corrige'].', '.$donnees['sujet_pages'].', \''.addslashes($donnees['sujet_info']).'\', \''.addslashes($donnees['sujet_commentaire']).'\', '.$donnees['sujet_ecarte'].');">Modif</button></td>';
					echo '<td><button type="button" onclick="javascript: supprimerSujet(this, '.intval($donnees['sujet_id']).');">Suppr</button></td>';
				echo '</tr>';
			}

	echo '</table>';


	/***************************************************************************
	Les Formulaires (Modification, Suppression, Filtrage)
	***************************************************************************/
	?>

	<div id="leBas"></div>
	<!-- Modifier un sujet -->
	<form method="post" action="<?php echo "$racine$module/$section"; ?>_control" id="form_modif" style="display: none; position: fixed; left: 20%; top: 40%; background-color: lightblue;">
		<fieldset>
			<legend>Modifier</legend>
			<input type="hidden" name="action_formulaire" value="modif" />
			<input type="hidden" name="modif_id" id="modif_id" value="" />
			<input type="text" name="modif_id2" id="modif_id2" disabled="disabled" size="4" value="" /> <!-- On a 2 champs car l'input disabled n'est pas renvoyé pendant l'envoi du formulaire -->
			<select name="modif_uv" id="modif_uv"><option value="-">-UV-</option><?php echo $liste_uvs; ?></select>

			<?php
			$types = array('t' => 'Test (1)', 'u' => 'test 2', 'v' => 'test 3',
							'p' => 'Partiel (1)', 'q' => 'Partiel 2',
							'm' => 'Médian', 'f' => 'Final');
			echo '<select name="modif_type" id="modif_type">
				<option value="-">-Type-</option>';
				foreach($types as $une_lettre => $un_type) {
					echo '<option value="'.$une_lettre.'">'.strtoupper($une_lettre).' - '.$un_type.'</option>';
				}
			echo '</select>';

			echo '<label for="modif_semestre">Semestre </label><input name="modif_semestre" id="modif_semestre" type="text" maxlength="3" size="4" value="" />';

			echo '<input type="checkbox" name="modif_corrige" id="modif_corrige" /><label for="modif_corrige">Corrigé</label>';
			echo "\t".'Pages <input name="modif_pages" id="modif_pages" type="text" size="4" maxlength="3" value="" /><br />';
			echo "\t".'Info <input name="modif_info" id="modif_info" type="text" maxlength="50" value="" />';
			echo "\t".'Commentaire <input name="modif_commentaire" id="modif_commentaire" type="text" maxlength="50" value="" />';
			?>
			<br />
			<span style="font-size: 0.8em; font-style: italic;">L'<strong>info</strong> apparaît sur le sommaire ; le <strong>commentaire</strong> n'est présent que sur la BDD.</span><br />
			<input type="checkbox" name="modif_ecarter" id="modif_ecarter" /><label for="modif_ecarter">Mettre le sujet de côté</label><br />
			<input type="reset" onclick="javascript: annulerModif();" value="Annuler" /><input type="submit" value="Modifier" id="submitModif" disabled="disabled" />
		</fieldset>
	</form>

	<!-- Supprimer un sujet -->
	<form method="post" action="<?php echo "$racine$module/$section"; ?>_control" id="form_suppr" style="display: none; position: fixed; left: 20%; top: 40%; background-color: pink;">
		<fieldset>
			<legend>Supprimer</legend>
			<input type="hidden" name="action_formulaire" value="suppr" />
			<input type="hidden" name="suppr_id" id="suppr_id" value="" />
			Es-tu vraiment certain(e) de vouloir supprimer ce sujet ?
			<input type="text" name="suppr_id2" id="suppr_id2" disabled="disabled" size="4" value="" /> <!-- On a 2 champs car l'input disabled n'est pas renvoyé pendant l'envoi du formulaire -->
			<input type="reset" onclick="javascript: annulerSuppr();" value="Non" />
			<input type="submit" value="Oui" id="submitSuppr" disabled="disabled" />
		</fieldset>
	</form>

	<!-- Filtrer les sujets -->
	<form method="post" id="form_filtrer" action="<?php echo "$racine$module/$section"; ?>_control" style="display: none; position: fixed; left: 10%; top: 15%; background-color: #dddddd; border: 2px outset white;">
		<input type="hidden" name="action_formulaire" value="filtrer" />
		<input type="hidden" name="type" value="sujets" />
		<div class="multi-col">
			<h4>Conditions appliquées aux sujets</h4>
			<fieldset>
			<legend><input type="checkbox" name="condition_type" id="condition_type" /><label for="condition_type">Condition sur le type</label></legend>
				<div>
					<input type="checkbox" name="type_t" id="type_t" /><label for="type_t">Tests (T, U, V)</label>
					<input type="checkbox" name="type_p" id="type_p" /><label for="type_p">Partiels (P, Q)</label>
					<input type="checkbox" name="type_m" id="type_m" /><label for="type_m">Médians (M)</label>
					<input type="checkbox" name="type_f" id="type_f" /><label for="type_f">Finaux (F)</label>
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_semestre" id="condition_semestre" /><label for="condition_semestre">Condition sur le semestre</label></legend>
				<div>
					<input type="radio" name="semestre" id="semestre_a" value="semestre_a" checked="checked" /><label for="semestre_a">Automne</label>
					<input type="radio" name="semestre" id="semestre_p" value="semestre_p" /><label for="semestre_p">Printemps</label>
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_annee" id="condition_annee" /><label for="condition_annee">Condition sur l'année</label></legend>
				<div>Entre 20<input type="text" maxlength="2" size="2" name="annee_debut" /> et 20<input type="text" maxlength="2" size="2" name="annee_fin" /></div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_pages" id="condition_pages" /><label for="condition_pages">Condition sur le nombe de pages</label></legend>
				<div>
					Nombre de pages compris entre <input type="text" maxlength="2" size="2" name="pages_mini" value="0" /> et <input type="text" maxlength="2" size="2" name="pages_maxi" value="99" /> (inclus)
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_ecartes" id="condition_ecartes" /><label for="condition_ecartes">Condition sur les sujets écartés</label></legend>
				<div>
					Afficher uniquement les sujets
					<input type="radio" name="ecartes" id="ecartes_oui" value="ecartes_oui" /><label for="ecartes_oui">Mis de côtés</label>
					<input type="radio" name="ecartes" id="ecartes_non" value="ecartes_non" checked="checked" /><label for="ecartes_non">En vente dans les annales</label>
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_commentaires" id="condition_commentaires" /><label for="condition_commentaires">Condition sur les infos &amp; commentaires</label></legend>
				<div>
					Afficher uniquement les sujets
					<input type="checkbox" name="commentaire_info" id="commentaire_info" /><label for="commentaire_info">Avec une info</label>
					<input type="checkbox" name="commentaire_commentaire" id="commentaire_commentaire" /><label for="commentaire_commentaire">Avec un commentaire</label>
				</div>
			</fieldset>
		</div>
		<div class="multi-col">
			<h4>Conditions appliquées aux UV</h4>
			<fieldset>
				<legend><input type="checkbox" name="condition_code" id="condition_code" /><label for="condition_code">Condition sur le code</label></legend>
				<div>
					Ne garder que les UV dont le code est compris entre <input type="text" maxlength="4" size="5" name="code_debut" value="AA00" /> et <input type="text" maxlength="4" size="5" value="ZZ99" name="code_fin" />
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_enseignee" id="condition_enseignee" /><label for="condition_enseignee">Condition sur le semestre d'enseignement</label></legend>
				<div>
					<input type="checkbox" name="enseignee_a" id="enseignee_a" /><label for="enseignee_a">A</label>
					<input type="checkbox" name="enseignee_p" id="enseignee_p" /><label for="enseignee_p">P</label>
					<input type="checkbox" name="enseignee_ap" id="enseignee_ap" /><label for="enseignee_ap">AP</label>
					<input type="checkbox" name="enseignee_inconnu" id="enseignee_inconnu" /><label for="enseignee_inconnu">?</label>
					<input type="checkbox" name="enseignee_rempl" id="enseignee_rempl" /><label for="enseignee_rempl">UV Remplacée</label>
					<input type="checkbox" name="enseignee_fin" id="enseignee_fin" /><label for="enseignee_fin">UV Finie</label>
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_modif" id="condition_modif" /><label for="condition_modif">Condition sur la dernière modification</label></legend>
				<div>
					UV modifiée entre <input type="text" maxlength="17" size="17" value="0000-00-00 00:00" name="date_debut" /> et <input type="text" maxlength="17" size="17" name="date_fin" value="<?php echo date('Y-m-d H:i'); ?>" /><br />
					<em>Inscrire les dates au format aaaa-mm-jj hh:mm</em>
				</div>
			</fieldset>
			<fieldset>
				<legend><input type="checkbox" name="condition_enVente" id="condition_enVente" /><label for="condition_enVente">Condition sur la mise en vente</label></legend>
				<div>
					Afficher uniquement les UVs
					<input type="radio" name="enVente" id="enVente_oui" value="enVente_oui" checked="checked" /><label for="enVente_oui">en vente</label>
					<input type="radio" name="enVente" id="enVente_non" value="enVente_non" /><label for="enVente_non">pas en vente</label>
				</div>
			</fieldset>
		</div>
		<hr />
		<div style="text-align: center;">
			<div>
				Filtres personnalisés <button onclick="filtragePersonnalise('NonScannesEnVente')" type="button">[1] En vente non scannés</button>
				<input type="reset" value="RàZ" />
			</div>
			<input type="reset" value="Annuler" onclick="javascript: document.getElementById('form_filtrer').style.display = 'none';" />
			<input type="submit" value="Filtrer" />
		</div>
	</form>
<?php
require("inc/footer.php");
?>
