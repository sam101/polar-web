<?php
require('inc/annales.php');

if(!isset($_POST['login'], $_POST['sujets'], $_POST['corrections'])){
	ajouterErreur('Une erreur est survenue, merci de réessayer.');
	header("Location: $racine$module/$section");
	exit();
}


$login = mysqlSecureText(trim($_POST['login']));
$sujets = intval($_POST['sujets']);
$corrections = intval($_POST['corrections']);
$supplement = intval($_POST['libre']);

$id_user = intval($_SESSION['con-id']);
$login_utilisateur = mysqlSecureText($_SESSION['con-pseudo']);

$pages_offertes = $sujets * Annales::$PAGES_OFFERTES_PAR_SUJET + $corrections * Annales::$PAGES_OFFERTES_PAR_CORRECTION + $supplement;

if(empty($login)) {
	ajouterErreur('Merci d\'indiquer un login');
}
if($pages_offertes == 0) {
	ajouterErreur('Aucune page à offrir');
}
if($login == $login_utilisateur) {
	ajouterErreur('Tu ne peux pas t\'offrir toi-même des pages..');
}


if(hasErreurs()){
	header("Location: $racine$module/$section");
	exit();
}
else {

	$req = query("INSERT INTO polar_annales_reductions(Login, User, Date, Pages) VALUES('$login', '$id_user', NOW(), $pages_offertes)");

	// Mail de confirmation

	/* Si possibilité d'ajouter le prénom, c'est encore mieux */
	$message = "Bonjour,<br /><br />
	Pour te remercier de ta participation à l'amélioration du contenu des annales, le Polar t'offre $pages_offertes pages d'annales. Celles-ci seront automatiquement décomptées sur ta prochaine commande d'annales !<br />
	<br />
	<br />
	A bientôt !
	<br />
	La boîte à annales";

	sendMail('polar-annales@assos.utc.fr', 'Le Polar', array($login.'@etu.utc.fr'), "[Le Polar] Pages offertes sur ta prochaine commande d'annales !", $message);

	echo '<script>alert("'.$pages_offertes.' pages ont été offertes à '.$login.' avec succès !");document.location.href="'.$racine.$module."/".$section.'";</script>';
}



?>
