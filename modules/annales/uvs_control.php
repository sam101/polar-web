<?php
/* Dans l'ordre : Ajout, Modification d'une UV, Modification de la mise en vente */


if(isset($_GET['GetSujetsDisponibles'])) {
	
	if(!isset($_POST['uv'])) {
		/***/
	}
	
}
/*****************************************************
Ajout d'une UV
******************************************************/
elseif(isset($_GET['ajouter_code'])) {
	if(empty($_POST['ajouter_code']) OR strlen($_POST['ajouter_code']) != 4)
		ajouterErreur('Le code de l\'UV doit faire 4 caractères');

	if(!hasErreurs()) {
		$code = mysqlSecureText(strtoupper($_POST['ajouter_code']));
		$enseignee = $_POST['ajouter_enseignee'];
		$enVente = (isset($_POST['ajouter_enVente']) ? 1 : 0);
		$info = mysqlSecureText($_POST['ajouter_info']);

		query("INSERT INTO polar_annales_uvs(uv_code, uv_enseignee, uv_info, uv_enVente, uv_modif)
									VALUES('$code', '$enseignee', '$info', $enVente, NOW())");

		$_SESSION['notification'] = '<p style="color: green;">UV '.$code.' ajoutée</p>';
		$_SESSION['mevCode'] = $code;

		header("Location: $racine$module/$section#".ucfirst(substr($code, 0, 1)));
		exit();
	}
	else {
	  header("Location: $racine$module/$section");
		exit();
	}
}

/*****************************************************
Modification d'un sujet
******************************************************/
elseif(isset($_POST['modif_code'])) {
	if(empty($_POST['modif_code']))
		ajouterErreur('Une erreur interne est survenue. Veuillez réessayer.');

	if(!hasErreurs()) {
		$code = mysqlSecureText($_POST['modif_code']);
		$enseignee = $_POST['modif_enseignee'];
		$enVente = (isset($_POST['modif_enVente']) ? 1 : 0);
		$info = mysqlSecureText($_POST['modif_info']);

		query("UPDATE polar_annales_uvs SET uv_enseignee = '$enseignee', uv_info = '$info', uv_enVente = $enVente, uv_modif = NOW() WHERE uv_code = '$code'");

		$_SESSION['notification'] = '<p style="color: green;">UV '.$code.' modifiée avec succès.</p>';
		$_SESSION['mevCode'] = $code;
	}

	header("Location: $racine$module/$section#".ucfirst(substr($code, 0, 1)));
	exit();
}

/*****************************************************
Modification des mises en vente
******************************************************/
elseif(isset($_POST['cocherAction'])) {

	//Récupération des UVs à modifier
	//Il est possible qu'un filtre ait été utilisé pendant la modification des mises en vente. Il faut en tenir compte pour ne pas retirer de la vente toutes les UVs non affichées
	if(isset($_SESSION['whereUv']))
		$whereUv = $_SESSION['whereUv'];
	else
		$whereUv = "";

	$requete = query("SELECT uv_code, uv_enVente FROM polar_annales_uvs WHERE 0 = 0 $whereUv ORDER BY uv_code ");
	$enleverVente = '';
	$mettreEnVente = '';

	while($donnees = mysql_fetch_array($requete)){
		if($donnees['uv_enVente'] == 1 AND !isset($_POST['enVente_'.$donnees['uv_code']])) {
			//On retire de la vente
			$enleverVente .= "'".$donnees['uv_code']."', ";
		}
		elseif($donnees['uv_enVente'] == 0 AND isset($_POST['enVente_'.$donnees['uv_code']])) {
			//On met en vente
			$mettreEnVente .= "'".$donnees['uv_code']."', ";
		}
	}

	if(!empty($enleverVente)) {
		$enleverVente = "(".substr($enleverVente, 0, -2).")";
		query("UPDATE polar_annales_uvs SET uv_enVente = 0 WHERE uv_code IN $enleverVente");
	}

	if(!empty($mettreEnVente)) {
		$mettreEnVente = "(".substr($mettreEnVente, 0, -2).")";
		query("UPDATE polar_annales_uvs SET uv_enVente = 1 WHERE uv_code IN $mettreEnVente");
	}

	$_SESSION['notification'] = '<span style="color: green;">Les modifications ont bien été enregistrées</span>';
	$_SESSION['mevCode'] = '';
  header("Location: $racine$module/$section");
	exit();
}

else {
	ajouterErreur('Impossible d\'effectuer l\'action demandée');
  header("Location: $racine$module/$section");
	exit();
}
