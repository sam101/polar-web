<?php
$titrePage = "Commandes d'annales";
use_jquery_ui();
use_datatables();
addFooter(<<<EOT
	<script>
    var controlFileUrl = "$racine$module/${section}_control";

	function infos(id, etu, commande, paye, pret, retrait, uvs){
        var payeHtml = paye;
        if (payeHtml) {
            payeHtml += " - <a href=\""+controlFileUrl+"?AnnulerPaiement="+id+"\""+
            "onClick=\"return confirm(\'Voulez-vous vraiment annuler le paiement ?\');\">"+
                "Annuler Paiement</a>";
        }

        var pretHtml = pret;
        if (pretHtml) {
            pretHtml += " - <a href=\""+controlFileUrl+"?AnnulerPret="+id+"\"" +
                "onClick=\"return confirm('Voulez-vous vraiment annuler l\\\'impression ?');\">"+
                "Annuler Impression</a>";
        }

        var retraitHtml = retrait;
        if (retraitHtml) {
            retraitHtml += " - <a href=\""+controlFileUrl+"?AnnulerRetrait="+id+"\"" +
                "onClick=\"return confirm(\'Voulez-vous vraiment annuler le retrait ?\');\">"+
                "Annuler Retrait</a>";
        }

		$("#com-id").html(id);
		$("#com-uv").html(uvs);
		$("#com-etudiant").html(etu);
		$("#com-commande").html(commande);
		$("#com-paiement").html(payeHtml);
		$("#com-pret").html(pretHtml);
		$("#com-retrait").html(retraitHtml);
		$("#dialog").dialog("open");
	}
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
	});
	</script>
EOT
);
require("inc/header.php");
?>
<h1>Les commandes d'annales</h1>

<?php
if(isset($_GET['toutes']))
	$suppl = "";
else {
	$suppl = " AND Termine=0";
	echo '<p><a href="'.$racine.$module.'/'.$section.'?toutes">Afficher également les commandes terminées</a></p>';
}
?>

<table class="datatables table table-bordered table-striped">
	<thead>
		<tr><th>ID</th><th>Nom</th><th>Prénom</th><th>Mail</th><th>Commande</th><th>Paiement</th><th>Impression</th><th>Retrait</th><th>Infos</th></tr>
	</thead>
	<tbody>
<?php
$req = query("SELECT pc.ID,pc.Nom,Prenom,Mail,DATE(DateCommande),DateCommande,DATE(DatePaiement),DatePaiement,DATE(DatePrete),DatePrete,DATE(DateRetrait),DateRetrait,Detail FROM polar_commandes pc
	INNER JOIN polar_commandes_types pct ON pc.type=pct.ID
	LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
	WHERE pct.Nom LIKE 'Annale'$suppl ORDER BY pc.ID DESC, pcc.Detail ASC");
	$lastID = 0;
	$ligne = "";
	if(mysql_num_rows($req) > 0){
		while($data=mysql_fetch_assoc($req)){
			if($data['ID'] != $lastID){
				if($lastID != 0){
					// On vire la dernière virgule
					$ligne = substr($ligne, 0, -2);

					// Fin de la ligne
					$ligne .= '\');" style="cursor:pointer;" src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></td>';
					$ligne .= '</tr>';
				}

				// Affichage de la ligne précédente
				echo $ligne;

				// Nouvelle ligne
				$lastID = $data['ID'];
				$ligne = '<tr>';
				$ligne .= '<td>'.$data['ID'].'</td>';
				$ligne .= '<td>'.$data['Nom'].'</td>';
				$ligne .= '<td>'.$data['Prenom'].'</td>';
				$ligne .= '<td>'.$data['Mail'].'</td>';
				$ligne .= '<td>'.$data['DATE(DateCommande)'].'</td>';
				$ligne .= '<td>'.$data['DATE(DatePaiement)'].'</td>';
				$ligne .= '<td>'.$data['DATE(DatePrete)'].'</td>';
				$ligne .= '<td>'.$data['DATE(DateRetrait)'].'</td>';
				$ligne .= '<td><img title="Infos" onclick="infos(';
				$ligne .= "'".$data['ID']."','".$data['Prenom']." ".$data['Nom']." (".$data['Mail'].")','".$data['DateCommande']."','".$data['DatePaiement']."','".$data['DatePrete']."','".$data['DateRetrait']."','";
			}
			// Ajout du detail
			$ligne .= $data['Detail'].", ";
		}

		// Fin et affichage de la dernière ligne (il manque un tour de boucle)
		$ligne = substr($ligne, 0, -2);
		$ligne .= '\');" style="cursor:pointer;" src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></td>';
		$ligne .= '</tr>';
		echo $ligne;
	}
?>
	</tbody>
</table>

<div id="dialog" title="Détail de la commande">
	<table>
		<tr>
			<th>ID</th>
			<td id="com-id"></td>
		</tr>
		<tr>
			<th>Contenu</th>
			<td id="com-uv"></td>
		</tr>
		<tr>
			<th>Etudiant</th>
			<td id="com-etudiant"></td>
		</tr>
		<tr>
			<th>Date de commande</th>
			<td id="com-commande"></td>
		<tr>
			<th>Date de paiement</th>
			<td id="com-paiement"></td>
		</tr>
		<tr>
			<th>Date d'impression</th>
			<td id="com-pret"></td>
		</tr>
		<tr>
			<th>Date de retrait</th>
			<td id="com-retrait"></td>
		</tr>
	</table>
</div>
<?php
require('inc/footer.php');
?>

