<?php
require("inc/header.php");

// Shootage du timeout
$_SESSION['con-lastPrivateActive'] = time() + 6*3600;
$_SESSION['con-derniereactivite'] = time() + 6*3600;
?>
<h1>Nouvelle séance d'impression</h1>
<form method="post" action="<?php echo "$racine$module/$section"; ?>_control">
<p style="color:red;">Attention ! Les coches n'ont aucun effet ! Elles sont juste l&agrave; pour vous aider lors de la s&eacute;ance d'impression !</font><p>
<?
$req = query("SELECT MAX(pc.ID) AS ID, pcc.Detail AS Code, COUNT(pcc.Detail) AS Nombre, pap.pages_nbrPages AS NbPages, SUM(pap.pages_nbrPages) AS NbPagesUV
	FROM polar_commandes pc
	LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
	LEFT JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
	INNER JOIN polar_commandes_types pct ON pc.Type = pct.ID
	WHERE pc.DatePrete IS NULL
    AND pc.DatePaiement IS NOT NULL
    AND pct.Nom = 'Annale'
    AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types
	GROUP BY pcc.Detail
	ORDER BY pcc.Detail ASC
");

if(mysql_num_rows($req) == 0)
	echo "<p>Aucune commande ne doit &ecirc;tre imprim&eacute;e !</p>";
else {
	echo '<ul>';
	$totalPages = 0;
	$totalAnnales = 0;
	$commandes = array();

	while($row = mysql_fetch_array($req)){
		echo '<li>'.$row['Code'].' ('.$row['NbPages'].' pages)..........<i>'.$row['Nombre'].' exemplaires</i>
			<input type="checkbox" disabled="disabled"></li>';

		$totalPages += $row['NbPagesUV'];
		$totalAnnales += $row['Nombre'];
		if(!in_array($row['ID'], $commandes))
			$commandes[] = $row['ID'];
	}

echo "</ul>";

// Détermination de l'heure qu'il est
$req = query("SELECT NOW()");
$res = mysql_fetch_row($req);
$heureDebut = $res[0];
?>
<p>
	Heure de d&eacute;but : <input type="text" size="20" name="heureDebut" value="<?php echo $heureDebut; ?>" /><br />
	Nombre d'annales à imprimer : <input type="text" name="nbAnnales" readonly="readonly" value="<?php echo $totalAnnales; ?>" size="6" /><br />
	Nombre de pages &agrave; imprimer : <input type="text" name="nbPages" readonly="readonly" value="<?php echo $totalPages; ?>" size="6" /><br />
	Nombre de pertes imprim&eacute;es: <input type="text" name="nbPertes" autocomplete="off" value="" size="6" /><br />

	<input type="hidden" name="lastId" value="<?php echo max($commandes); ?>" />
	<input type="submit" value="Confirmer l'impression de TOUTES les commandes" />
</p>
</form>

<?php
}
require("inc/footer.php");
?>
