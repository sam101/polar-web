<?php
require_once('inc/annales.php');

function printColumn(&$currentTable, $prix) {
    $currentUvIndex = &$currentTable[0];
    $uvDataTable = &$currentTable[1];
    if ($currentUvIndex >= count($uvDataTable)) {
        echo '<td/><td/><td/>';
        return;
    }
    $currentUvData = &$uvDataTable[$currentUvIndex];
    $currentUvCode = $currentUvData[0];
    $currentTypesIndex = &$currentUvData[1];
    $currentTypesData = $currentUvData[2];
    if ($currentTypesIndex >= count($currentTypesData)) {
        $currentUvIndex++;
        printColumn($currentTable, $prix);
        return;
    }

    $currentTypesDescrPages = $currentTypesData[$currentTypesIndex];
    $maxCurrentTypesIndex = count($currentTypesData);

    $additionnalClass = '';
    if ($currentTypesIndex == 0) {
        echo '<td class="top_cell left_cell bottom_cell" rowspan="'.$maxCurrentTypesIndex.'">'.$currentUvCode.'</td>';
        $additionnalClass .= 'top_cell ';
    }
    if (($currentTypesIndex + 1) == $maxCurrentTypesIndex) {
        $additionnalClass .= 'bottom_cell ';
    }

    echo '<td class="',$additionnalClass,'">',$currentTypesDescrPages[0],'</td>';
    echo '<td class="right_cell ', $additionnalClass,'">',number_format($prix * $currentTypesDescrPages[1], 2, ',', ' '),' €</td>';


    $currentTypesIndex++;
}

addHeaders(<<<EOT

    <style type="text/css">
        .empty_cell {
            background:#fafafa !important;
            border:0px solid #d0d6e7 !important;
            padding:5px !important;
            text-align:center !important;
        }

        .left_cell {
            border-left:1px solid black !important;
        }

        .right_cell {
            border-right:1px solid black !important;
        }

        .top_cell {
            border-top:1px solid black !important;
        }

        .bottom_cell {
            border-bottom:1px solid black !important;
        }


    </style>


EOT
);




$titrePage = "Les annales en vente";
require("inc/header.php");

$req = query("SELECT pct.Actif, PrixVente FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
	WHERE pca.Actif = 1 AND pct.Nom = 'Annale'");
$data = mysql_fetch_assoc($req);

if($data['Actif'] != 1)
	echo "<p>Ce secteur est actuellement fermé. Rendez-vous au semestre prochain !</p>";
else {
	echo "<h1>Les annales vendues au polar</h1>";
	$prix = $data['PrixVente'];

    $annales = Annales::retrieveTypesPages();

	$nbAnnales = count($annales);

	echo '
	<p>La procédure est la suivante :
	<ol>
	<li>Tu commandes tes annales
		<ul>';

		if($conid > 0)
			echo '<li>directement sur le site du Polar, en <a href="'.$racine.'annales/commander" title="Commander des annales">cliquant ici</a>';
		else
			echo '<li>directement sur le site du Polar, après t&rsquo;<a href="'.$CONF["cas_login"].'">être connecté</a>.</li>';

		echo '	<li>en passant directement au Polar</li>
		</ul>
	</li>
	<li>Tu te rends au Polar afin de régler ta commande. <b>Il faudra alors préciser ton numéro de commande.</b></li>
	<li>Lorsque tes annales ont été imprimées, tu reçois un mail t&rsquo;indiquant où et quand les récupérer. Tu auras également besoin de ton numéro de commande.</li>
	</ol>
	</p>
	<p>&nbsp;</p>
	<p>Il y a '.$nbAnnales.' annales en vente ce semestre !</p>';

    if ($nbAnnales == 0) {
		echo '<p><em>Il n\'y a aucune annale en vente pour le moment.</em></p>';
    } else {

        echo '<table class="datatables table table-bordered table-striped table-condensed">
				<tr>
				<th>Nom de l\'UV</th>
				<th>Type</th>
				<th>Prix</th>
				<th class="empty_cell">&nbsp;</th>
				<th>Nom de l\'UV</th>
				<th>Type</th>
				<th>Prix</th>
				<th class="empty_cell">&nbsp;</th>
				<th>Nom de l\'UV</th>
				<th>Type</th>
				<th>Prix</th>
				</tr>';

        // Sépration des annales dans 3 tables séparées

        if($nbAnnales % 3 == 0) {
            $nbMaxColumn = $nbAnnales / 3;
        } else {
            $nbMaxColumn = ceil($nbAnnales / 3);
        }

        $i = 0;
        $j = 0;
        $tableColumnLength = array(0, 0, 0);
        $tableColumnData = array(array(0, array()), array(0, array()), array(0, array()));
        foreach($annales as $uvCode => $typesPages) {
            $tableColumnData[$i][1][] = array($uvCode, 0, array_values($typesPages));

            $tableColumnLength[$i] += count($typesPages);

            $j++;
            if ($j >= $nbMaxColumn) {
                $j = 0;
                $i++;
            }
        }
        $maxColumLength = max($tableColumnLength);

        for ($i = 0 ; $i < $maxColumLength ; $i++) {
            echo '<tr>';

            printColumn($tableColumnData[0], $prix);
            echo '<td class="empty_cell">&nbsp;</td>';
            printColumn($tableColumnData[1], $prix);
            echo '<td class="empty_cell">&nbsp;</td>';
            printColumn($tableColumnData[2], $prix);

            echo "</tr>\n";
        }

        echo '</table>';

    }

} // Fin de Annale actif
require("inc/footer.php");
?>


