<?php

require_once('inc/annales.php');

if(isset($_GET['GetReduction'])) {
	
	$login = mysqlSecureText($_POST['login']);
	$req = query("SELECT SUM(Pages) AS PagesOffertes FROM polar_annales_reductions WHERE Login = '$login' AND DateUtilisation IS NULL");
	
	$data = mysql_fetch_assoc($req);
	
	echo intval($data['PagesOffertes']);
}
elseif(isset($_GET['Commande'])) {

    if(!$user->Staff){
        $login = $user->Login;
    }
    else{
        $login = mysqlSecureText($_POST['login']);
    }

    try {
        $idCommande = Annales::commander_annales($login);

        $commande = Commande::getById($idCommande); // todo
        Request::$panier->addCommande($commande, $user);
        Request::$panier->redirectTo($commande);
    } catch (AnnalesException $e) {
        redirectWithErrors('', $e->getMessage());
    }
    header("Location: $racine$module/$section");
} else {
    header("Location: $racine$module/$section");
}
?>
