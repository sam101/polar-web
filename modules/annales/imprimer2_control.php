<?php
require_once('inc/annales.php');

// Shootage du timeout
$_SESSION['con-lastPrivateActive'] = time() + 6*3600;
$_SESSION['con-derniereactivite'] = time() + 6*3600;

if(isset($_GET['Commandes'])){
	switch($_GET['Commandes']){
		case 'paires':
			$cond = "AND (pc.ID % 2) = 0";
			break;
		case 'impaires':
			$cond = "AND (pc.ID % 2) = 1";
			break;
		case 'unique':
			$cond = "AND pc.ID = ".intval($_GET['id']);
			break;
		default:
			$cond = "";
			break;
	}
	$req = query("SELECT pc.ID, pcc.Detail, pap.pages_nbrPages AS NbPages
		FROM polar_commandes pc
		INNER JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
		INNER JOIN polar_annales_pages pap ON pap.pages_code = SUBSTRING_INDEX(pcc.Detail, '_', 1)
		INNER JOIN polar_commandes_types pct ON pc.Type = pct.ID
		WHERE pc.DatePrete IS NULL
        AND pc.DatePaiement IS NOT NULL
        AND pct.Nom = 'Annale'
        AND pcc.Quantite > 0
        AND SUBSTRING_INDEX(pcc.Detail, '_', -1) = pap.pages_types
        $cond
		ORDER BY pc.ID ASC, pcc.Detail ASC
	");

	$totalAnnales = mysql_num_rows($req);
	if($totalAnnales == 0)
		echo json_encode(array('error' => "Aucune commande à imprimer."));
	else {
		$totalPages = 0;
		$lastId = 0;
		$commandes = array();
		$commande = array();

		while($row = mysql_fetch_array($req)){
			if($lastId != $row['ID'] && $lastId != 0){
				$commandes[] = array(
					'id' => $lastId,
					'contenu' => $commande
				);
				$commande = array();
			}

			$lastId = $row['ID'];
			$commande[] = array(
				'pages' => $row['NbPages'],
				'uv' => $row['Detail'],
			);

			$totalPages += $row['NbPages'];
		}

		$commandes[] = array(
			'id' => $lastId,
			'contenu' => $commande
		);

		echo json_encode(array(
			'totalAnnales' => $totalAnnales,
			'totalPages' => $totalPages,
			'commandes' => $commandes
		));
	}
}
else if(isset($_GET['MarquerPrete'])){
	$id = intval($_GET['MarquerPrete']);

    $req = query("SELECT pc.Mail AS Mail
        FROM polar_commandes pc
        INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
        WHERE pct.Nom = 'Annale'
        AND pc.DatePaiement IS NOT NULL
        AND pc.DatePrete IS NULL
        AND pc.ID = $id");

    $numRows = mysql_num_rows($req);

    if ($numRows <= 0) {
        echo "error";
        exit();
    }

    $data = mysql_fetch_assoc($req);
    $mail = $data['Mail'];


	query("UPDATE polar_commandes SET DatePrete = NOW(), IDPreparateur = $conid
		WHERE `ID` = $id AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')
		AND DatePrete IS NULL AND DatePaiement IS NOT NULL");


    $message = "Bonsoir,<br />
<br />
Tes annales ont été imprimées ! Tu peux venir les retirer directement au Polar, en prenant soin de bien noter ton numéro de commande : $id<br />
<br />
À bientôt au Polar !<br />
<br />
<br />
Ce message n'a pas été généré automatiquement par Demeter.<br />
Pour tout renseignement complémentaire, contacte polar-annales@assos.utc.fr";
    sendMail('polar-annales@assos.utc.fr', 'Le Polar', array($mail), "[Le Polar] Commande d'annales n°$id", $message);
	echo "ok";
}
else if(isset($_GET['GetFile'])){
	$uv = mysqlSecureText($_GET['GetFile']);
	if(is_file("upload/annales/$uv.pdf")){
		header('Content-type: application/pdf');
		readfile("upload/annales/$uv.pdf");
	}
	else {
		die("Fichier introuvable.");
	}
}
else if(isset($_GET['GetPdfCommande'])){

	$commande = intval($_GET['GetPdfCommande']);

	$req = query("SELECT pc.ID,pc.Nom,Prenom,Mail,
        DATE(DateCommande),DateCommande,DATE(DatePaiement),
        DatePaiement,DATE(DatePrete),DatePrete,DATE(DateRetrait),DateRetrait,
        SUBSTRING_INDEX(pcc.Detail, '_', 1) AS Code,
        SUBSTRING_INDEX(pcc.Detail, '_', -1) AS Types
		FROM polar_commandes pc
		INNER JOIN polar_commandes_types pct ON pc.type = pct.ID
		INNER JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
		WHERE pct.Nom LIKE 'Annale' AND pc.ID = $commande AND pcc.Quantite > 0
		ORDER BY pcc.Detail ASC");

	$countUv = mysql_num_rows($req);
	if($countUv == 0)
		die("Commande inconue.");

	$donnees = mysql_fetch_assoc($req);
	$listeUv = $donnees['Code'] . ' ' . Annales::getTypesDescription($donnees['Types'], Annales::$ANNALES_PAGES_TYPES_DESCRIPTIONS);
	while($donnees2 = mysql_fetch_assoc($req)){
		$listeUv .= ', '.$donnees2['Code'] . ' ' . Annales::getTypesDescription($donnees2['Types'], Annales::$ANNALES_PAGES_TYPES_DESCRIPTIONS);
	}

	require_once('inc/tcpdf.php');
	$pdf = new PolarPDF("Commande d'annales");
	$pdf->SetFontSize(12);
	$pdf->SetPrintFooter(false);

	// Ajout du print automatique
	$pdf->IncludeJS("print(false, true);");

	$pdf->Ln(50);

	$html = '<p align="center">'.$donnees['Prenom'].' '.$donnees['Nom'].'<br />
	'.$donnees['Mail'].'<br />
	Contenu de la commande : '.$listeUv.' ('.$countUv.' annales)<br />
	Imprimé le '.date("Y-m-d").'</p>';
	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$html = '<p align="center"><font size="120pt">'.$donnees['ID'].'</font></p>';
	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$style = array(
		'text' => true,
		'stretchtext' => 4,
		'font' => 'helvetica',
		'fontsize' => 8
	);
	$pdf->write1DBarcode(sprintf("%010s", 120000000+$donnees['ID']), "C128C", 86.95, 75, 180, 12, .4, $style);


	// Gauche
	$pdf->setXY(15,150);
	$pdf->Rotate(-90);
	$pdf->write(2, $donnees['ID']);
	$pdf->Rotate(90);

	$pdf->Output('commande.pdf', 'I');
}
