<?php
require_once('inc/annales.php');

function getDefaultTypes() {
    $req = query("SELECT Valeur
FROM polar_parametres
WHERE Nom = 'annales_default_types'");
    if (mysql_num_rows($req) > 0) {
        $data = mysql_fetch_array($req);
        return $data[0];
    }
    return '';
}


$titrePage = "Commande d'annales";

$req = query("SELECT pca.EnVente,PrixVente FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pct.CodeProduit = pca.CodeCaisse
	WHERE pct.Nom LIKE 'Annale' AND pct.Actif = 1 AND pca.Actif = 1 LIMIT 1");

$maxAnnales = 8;

if (mysql_num_rows($req) === 0) {
    $annales_actif = false;
    $prixParPage = 0;
} else {
    $data = mysql_fetch_assoc($req);
    $annales_actif = $data['EnVente'];
    $prixParPage = $data['PrixVente'];
}

$uvsPages = Annales::retrieveTypesPages($onlyActives = true, $reduit = false);
$defaultTypes = getDefaultTypes();

addHeaders('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />');
addFooter('
    <script type="text/javascript" src="'.$racine.'js/jquery.bgiframe.min.js"></script>
    <script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		var uvsPages = '.json_encode($uvsPages).';
		var nbrPagesOffertes = 0;

		var computePrice = function() {
			var somme = 0;
			var restant = 0;

			for(i=0;i<'.$maxAnnales.';i++){
				var uvStr = $("#uv"+i).val();
				if(uvStr != ""){
					var typesStr = $("#types"+i).val();
					var val = uvsPages[uvStr][typesStr][1];
					somme += val;
				}
			}
			
			var total = somme - nbrPagesOffertes;
			if(total < 0) {
				if(somme > 0)	// Evite d\'afficher le message avant la sélection de la première UV
					$("#PagesOffertesInutilisees").text("Les " + (-total) + " pages offertes restantes seront décomptées lors de ta prochaine commande.").fadeIn(200);
				else
					$("#PagesOffertesInutilisees").hide(200);

				total = 0;
			}
			else {
				$("#PagesOffertesInutilisees").hide(200);
			}
			
			$("#TotalPages").html(somme + " pages");
			$("#TotalCommande").html(total + " pages");
			$("#PrixCommande").html((total*'.$prixParPage.').toFixed(2));
			$("#ValiderCommande").attr("disabled", (somme == 0));
		};
		
		var GetReduction = function(login) {
			$("#PagesOffertes").html(\'<img src="'.$racine.'styles/0/icones/loading.gif" /><span style="font-weight: normal;"> Calcul...</span>\');
			$.ajax({
				url: "'.$racine.$module.'/'.$section.'_control?GetReduction",
				type: "POST",
				data: { \'login\': login },
				success: function(retour) {
						nbrPagesOffertes = retour;
						$("#PagesOffertes").html(retour + " pages");
						computePrice();
					},
				dataType: "text"
			});
		};
	
        
		$("#login").autocomplete("'.$racine.'utils/login_autocomplete",{
            matchContains:1,
            mustMatch:false,
            delay: 100,
            formatItem: function(row, i, max) {
                return row[0]+" - "+row[1]+" "+row[2];
            },
            formatResult: function(row) {
                return row[0];
            }
        });
		
		
		$("#login").blur(function() {
			GetReduction($(this).val());
		});

        $("#login").focus();

        $(".uv").change(function(){
            var types = $(this).next();
            types.empty();

            var uvStr = $(this).val();
            if (uvStr != "") {
                $.each(uvsPages[uvStr], function(typesVal, descrPages) {
                    var option = $("<option></option>").attr("value", typesVal)
                        .text(descrPages[0]);
                    if (typesVal == "'.$defaultTypes.'") {
                        option.attr("selected", "selected");
                    }
                    types.append(option);
                });
            }
            computePrice();
        });

        $(".types").change(function(){
            computePrice();
        });
		
		
		'.(!$user->Staff ? 'GetReduction("'.$user->Login.'");' : '').'
		
    });
</script>
');
require("inc/header.php");
?>
<h1>Effectuer une commande d'annales</h1>
<?php
if($annales_actif==1){
	echo '<center><form name="formulaire" method="post" action="'.$racine.$module.'/'.$section.'_control?Commande">';

	afficherErreurs();
	if(isset($_SESSION['annales_ok'])){
		/*echo '<p style="color:green;text-align:center;">Commande n&circ;<big>'.$_SESSION['annales_ok'].'</big> enregistrée !</p>
		<p>Tu dois maintenant aller la payer à la caisse du Polar, <b>en précisant ce numéro</b>.</p>
		<p style="margin-bottom: 100px;"></p>';*/
		echo '<script>$(document).ready(function(){alert("Commande numéro '.$_SESSION['annales_ok'].' enregistrée !");});</script>';
		unset($_SESSION['annales_ok']);
	}

	?>
	<!--echo '<p>Cette page permet de créer une commande d&rsquo;annales.<br />
	Une fois la commande passée, il te faudra passer au Polar pour la régler.<br />
	L&rsquo;impression sera alors réalisée dans les meilleurs délais.<br /><br /></p>';-->
	<table style="width: 75%;">
		<tr><td style="background-color: brown; font-weight: bold; color: white;">Etape 1</td><td><b>Effectue ta commande sur cette page</b></td></tr>
		<tr><td style="color: brown; font-weight: bold;">Etape 2</td><td>Paye ta commande au Polar</td></tr>
		<tr><td colspan="2" style="color: grey; font-size: 0.9em;">Les commandes sont imprimées le soir par nos soins.<br />
								<b>Un mail de confirmation est automatiquement envoyé lorsque ta commande est prête.</b></td></tr>
		<tr><td style="color: brown; font-weight: bold;">Etape 3</td><td>Tu peux récupérer ta commande au Polar dès que tu<br />reçois le mail de confirmation d'impression.</td></tr>
	</table>
	<br />
	<br />

	<?php
	
	if($isStaff)
		echo 'Login : <input type="text" id="login" name="login" size="50" /><br /><br />';
	else {
		echo '<b>Login : </b>'.$user->Login.'<br /><br />';
	}


	$ListeUV = '';
    foreach ($uvsPages as $uvCode => $typesPages) {
        $ListeUV .= "<option value='$uvCode'>$uvCode</option>";
    }

	echo '<p style="display: inline-block; text-align: left;">';
	for($i=0; $i<$maxAnnales; $i++){
		echo "Choix n&deg;".($i + 1).': <select name="uv'.$i.'" id="uv'.$i.'" size="1" class="uv"><option selected="selected" value="">-</option>';
		echo $ListeUV;
		echo "</select>";
        echo "<select name='types$i' id='types$i' class='types'></select>\n";
		echo "<br />\n";
	}
	echo '</p>';
	?>
	<br /><br />
	<div style="display: inline-block; text-align: right;">
		<b>Nbr de pages : </b>	<span id="TotalPages">0 pages</span><br />
		<b>Pages offertes <span style="cursor: help;" title="Sois le premier à apporter un nouveau sujet ou une nouvelle correction pour avoir des pages offertes sur ta prochaine commande !">[?]</span> :</b>	 <span id="PagesOffertes">0 pages</span><br />
		<b>Total :	<span id="TotalCommande">0 pages</span><br />
		(<span id="PrixCommande">0.00</span> €)</b>
	</div>
	
	<br /><br />
	<span style="font-size: 0.9em; display:none;" id="PagesOffertesInutilisees">*Les X pages offertes restantes seront reportées sur ta prochaine commande</span>

	<p><input type="submit" id="ValiderCommande" disabled="disabled" value="Valider la commande"></form>
	</center>
	<?php
}
else{
	echo '<center><b>Les commandes sont fermées pour l&rsquo;instant. Merci de réessayer plus tard.</b></center>';
}
require("inc/footer.php");
