<?php
if (isset($_GET['AnnulerPaiement'])) {
    $id = intval($_GET['AnnulerPaiement']);

    query("UPDATE polar_commandes SET
        DatePaiement = NULL, IDVente = NULL,
        DatePrete = NULL, IDPreparateur = NULL,
        DateRetrait = NULL, IDRetrait = NULL,
        Termine = 0
		WHERE `ID` = $id AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')");

    header("Location: $racine$module/$section");
} elseif (isset($_GET['AnnulerPret'])) {
    $id = intval($_GET['AnnulerPret']);

    query("UPDATE polar_commandes SET
        DatePrete = NULL, IDPreparateur = NULL,
        DateRetrait = NULL, IDRetrait = NULL,
        Termine = 0
		WHERE `ID` = $id AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')");

    header("Location: $racine$module/$section");
} elseif (isset($_GET['AnnulerRetrait'])) {
    $id = intval($_GET['AnnulerRetrait']);

    query("UPDATE polar_commandes SET
        DateRetrait = NULL, IDRetrait = NULL,
        Termine = 0
		WHERE `ID` = $id AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')");

    header("Location: $racine$module/$section");
} else {
    header("Location: $racine$module/$section");
}


?>
