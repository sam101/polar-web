<?php
require_once("inc/annales.php");

if(!empty($_GET['UV'])){
  $code = mysqlSecureText(trim(strtoupper($_GET['UV'])));
  $req1 = query("SELECT uv_code FROM polar_annales_uvs WHERE uv_code LIKE '$code'");
  if(mysql_num_rows($req1) != 1)
    die("UV introuvable.");


  $req2 = query("SELECT sujet_uv FROM polar_annales_sujets WHERE sujet_uv LIKE '$code' AND sujet_ecarte = 0");
  if(mysql_num_rows($req2) == 0)
    die("Aucun sujet à mettre dans l'UV.");

  $req3 = query("DELETE FROM polar_annales_pages WHERE pages_code = '$code'");

  $sujetTypes = Annales::getSujetTypesForAnnales($code);
  $pagesTypesCombin = Annales::getSujetPagesTypesCombinations($sujetTypes);

  $result = " ";

  foreach ($pagesTypesCombin as $pagesTypes) {
      $nbpages = Annales::fabriquerAnnale($code, $pagesTypes);
      $result .= $pageTypes . ' : ' . $nbpages . ', ';
  }

  $nbpages = "<b>$result</b>";

  echo "ok $nbpages";
}
?>
