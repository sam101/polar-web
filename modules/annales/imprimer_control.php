<?php
// On sauve les statistiques
$heureDebut = mysqlSecureText($_POST['heureDebut']);
$nbAnnales = intval($_POST['nbAnnales']);
$nbPages = intval($_POST['nbPages']);
$nbPagesReel = $nbPages + intval($_POST['nbPertes']);

$lastId = intval($_POST['lastId']);
$idPerm = intval($_SESSION['con-id']);

query("
INSERT INTO polar_annales_statistiques (
	`Debut`,
	`Fin`,
	`NbAnnales`,
	`NbPagesTheoriques`,
	`NbPagesReel`
) VALUES (
	'$heureDebut',
	NOW(),
	$nbAnnales,
	$nbPages,
	$nbPagesReel
)");
query("UPDATE polar_commandes SET DatePrete = NOW(), IDPreparateur = $idPerm
	WHERE `ID` <= $lastId AND Type = (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale')
	AND DatePrete IS NULL AND DatePaiement IS NOT NULL");

header("Location: $racine$module/rappel");
?>


