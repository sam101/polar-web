<?php
if(!isset($_POST['action_formulaire'])) {
	ajouterErreur('Erreur interne - Formulaire incorrect');
	header("Location: $racine$module/$section");
	exit();
}
/* Dans l'ordre : Ajout, Modification, Suppression de sujets */

/*****************************************************
Ajout de sujets
******************************************************/
if($_POST['action_formulaire'] == 'ajout') {
	$_SESSION['notification'] = 'L\'ajout de sujets passe désormais par la page Upload';
	
	header("Location: $racine$module/$section");
	exit();
}

/*****************************************************
Modification d'un sujet
******************************************************/
elseif($_POST['action_formulaire'] == 'modif') {
	$id = intval($_POST['modif_id']);
	
	if($_POST['modif_uv'] == '-' OR $_POST['modif_type'] == '-' OR empty($_POST['modif_semestre'])) {
		ajouterErreur('Impossible d\'effectuer la modification : tous les champs n\'ont pas été remplis.');
	}
	
	//Vérification du semestre
	$ap = strtoupper(substr($_POST['modif_semestre'], 0, 1));
	$annee = substr($_POST['modif_semestre'], 1, 2);
	if(($ap != 'A' AND $ap != 'P') OR intval($annee) == 0) {
		ajouterErreur('Impossible d\'effectuer la modification : Le format du semestre est incorrect');
	}
	
	if(!hasErreurs()) {
		$corrige = (isset($_POST['modif_corrige']) ? 1 : 0);
		$pages = intval($_POST['modif_pages']);
		$ecarter = (isset($_POST['modif_ecarter']) ? 1 : 0);
		
		$uv = mysqlSecureText($_POST['modif_uv']);
		$type = mysqlSecureText($_POST['modif_type']);
		$info = mysqlSecureText($_POST['modif_info']);
		$commentaire = mysqlSecureText($_POST['modif_commentaire']);
		
		
		query("UPDATE polar_annales_sujets SET sujet_uv = '$uv', sujet_type = '$type', sujet_semestre = '$ap', sujet_annee = $annee,
					sujet_corrige = $corrige, sujet_pages = $pages, sujet_info = '$info', sujet_commentaire = '$commentaire', sujet_ecarte = $ecarter
					WHERE sujet_id = $id");
		query("UPDATE polar_annales_uvs SET uv_modif = NOW() WHERE uv_code = '$uv'");
		
		$_SESSION['notification'] = '<p style="color: green;">Sujet modifié avec succès</p>';
		$_SESSION['afficherLettre'] = ucfirst(substr($uv, 0, 1));
		$_SESSION['mevId'] = $id;
	}
	
	header("Location: $racine$module/$section#$uv");
	exit();
}

/*****************************************************
Suppression d'un sujet
******************************************************/
elseif($_POST['action_formulaire'] == 'suppr') {
	$id = intval($_POST['suppr_id']);
	
	$requete = query("SELECT sujet_uv, sujet_type, sujet_semestre, sujet_annee, sujet_corrige, sujet_pages, sujet_info, sujet_commentaire FROM polar_annales_sujets WHERE sujet_id = $id");
	if(mysql_num_rows($requete) != 1) {
		ajouterErreur('Ce sujet a déjà été supprimé');
		header("Location: $racine$module/$section");
		exit();
	}
	
	$donnees = mysql_fetch_assoc($requete);
	$uv = $donnees['sujet_uv'];
	$type = strtoupper($donnees['sujet_type']);
	$semestre = $donnees['sujet_semestre'];
	$annee = $donnees['sujet_annee'];
	if ($annee < 10)
		$annee = '0'.$annee;
	query("DELETE FROM polar_annales_sujets WHERE sujet_id = $id");
	query("UPDATE polar_annales_uvs SET uv_modif = NOW() WHERE uv_code = '$uv'");
	
	
	$_SESSION['notification'] = '<p style="color: green;">Sujet supprimé avec succès<br />
									'.$uv.' - '.$type.' - '.$semestre.$annee.' - '.($donnees['sujet_corrige'] == 0 ? 'Non Corrigé' : 'Corrigé').' - '.$donnees['sujet_pages'].' pages - '.$donnees['sujet_info'].' - '.$donnees['sujet_commentaire'].'</p>';
	$_SESSION['afficherLettre'] = ucfirst(substr($uv, 0, 1));
	$_SESSION['mevId'] = '';
	
	
	$path = $_SERVER['DOCUMENT_ROOT'].$racine.'upload/annales/sujets/'.$uv[0].'/'.$uv.'_'.$type.$semestre.$annee.'.pdf';
	if(is_file($path)) {
		unlink($path);
	}
	else {
		$_SESSION['notification'] .= '<p style="color: red;">Le fichier n\'a pas pu être supprimé du serveur.</p>';
	}
	
	header("Location: $racine$module/$section");
	exit();
}
elseif($_POST['action_formulaire'] == 'filtrer'){ // Appelé à la fois par Lister et Uvs
  /**** Le système de filtrage crée les variables de session 
  $_SESSION['whereSujet'], $_SESSION['whereUv'], $_SESSION['descWhereSujet'], $_SESSION['descWhereUv'], $_SESSION['jointures'], $_SESSION['champsSupJointure']
  qui viendront s'ajouter à la requête de récupération des sujets en fonction des besoins de filtrage.

  On pourra ainsi faire des requêtes de la forme
  	SELECT sujet_1, sujet_2, sujet_3
  			, uv_1, uv_2 							<---- Ajoutés par $champsSupJointure
  	FROM sujets
  	LEFT JOIN uvs ON uvs.uv_1 = sujets.sujet_1		<---- Ajouté par $jointures
  	WHERE 0 = 0
  		AND sujet_1 = 'p'							<---- Ajouté par $whereSujet
  		AND uv_2 = 'truc'							<---- Ajouté par $whereUv


  $descWhereSujet et $descWhereUv permettent quant à elles d'afficher
  "Sujets dont sujet_1 = p
  UVs avec uv_1 = truc"

  */


  $whereSujet = "";
  $descWhereSujet = "";
  $whereUv = "";
  $descWhereUv = "";
  $champsSupJointures = "";
  $jointures = "";
  $jointureUvsNecessaire = false; //La jointure sera nécessaire en cas de condition sur le semestre d'enseignement et une condition sur la date de modif.
  								//Pour éviter de faire deux fois la jointure sur la table polar_annales_uvs, on la fera à la fin, si nécessaire


  // ====================== Conditions appliquées aux UV ====================== //

  // Condition sur le code
  if(isset($_POST['condition_code'])) {
  	$jointureUvsNecessaire = true;
  	$champs_sup_jointures .= ", polar_annales_uvs.uv_code";

  	$code_debut = mysqlSecureText(substr($_POST['code_debut'], 0, 4));
  	$code_fin = mysqlSecureText(substr($_POST['code_fin'], 0, 4));
  	if(empty($code_fin))
  		$code_fin = 'zzzz';

  	$whereUv .= " AND (uv_code >= '$code_debut' AND uv_code <= '$code_fin')";
  	$descWhereUv .= "<br />UVs entre <strong>$code_debut</strong> et <strong>$code_fin</strong>";
  }


  // Condition sur le semestre d'enseignement
  if(isset($_POST['condition_enseignee'])) {
  	$jointureUvsNecessaire = true;
  	$champs_sup_jointures .= ", polar_annales_uvs.uv_enseignee";
  	$whereUv .= " AND (uv_enseignee = 'z'"; // Impossible mais permet de simplifier l'utilisation des OR
  	$descWhereUv .= "<br />UVs enseignées en<strong>";
  	if(isset($_POST['enseignee_a'])) {
  		$whereUv .= " OR uv_enseignee = 'A'";
  		$descWhereUv .= " A,";
  	}
  	if(isset($_POST['enseignee_p'])) {
  		$whereUv .= " OR uv_enseignee = 'P'";
  		$descWhereUv .= " P,";
  	}
  	if(isset($_POST['enseignee_ap'])) {
  		$whereUv .= " OR uv_enseignee = 'AP'";
  		$descWhereUv .= " AP,";
  	}
  	if(isset($_POST['enseignee_inconnu'])) {
  		$whereUv .= " OR uv_enseignee = 'Inconnu'";
  		$descWhereUv .= " Inconnu";
  	}
  	if(isset($_POST['enseignee_rempl'])) {
  		$whereUv .= " OR uv_enseignee = 'Rempl'";
  		$descWhereUv .= " Rempl";
  	}
  	if(isset($_POST['enseignee_fin'])) {
  		$whereUv .= " OR uv_enseignee = 'Fin'";
  		$descWhereUv .= " Fin";
  	}
  	$whereUv .= ")";
  	$descWhereUv .= "</strong>";
  }

  // Condition sur la date de dernière modification
  if(isset($_POST['condition_modif'])) {
  	$date_debut = trim($_POST['date_debut']);
  	$date_fin = trim($_POST['date_fin']);

  	// La regex permet d'écrire 2011, 2011-10, 2011-10-15, 2011-10-15 23, 2011-10-15 23:52
  	//if(!preg_match('#^[0-9]{4}(-[0-9]{2}){2} [0-9]{2}:[0-9]{2}$#', $date_debut))
  	if(!preg_match('#^[0-9]{4}(-[0-9]{2}){0,2}( [0-9]{2}(:[0-9]{2})?)?$#', $date_debut))
  		$date_debut = '0000-00-00 00:00';
  	//if(!preg_match('#^[0-9]{4}(-[0-9]{2}){2} [0-9]{2}:[0-9]{2}$#', $date_fin))
  	if(!preg_match('#^[0-9]{4}(-[0-9]{2}){0,2}( [0-9]{2}(:[0-9]{2})?)?$#', $date_fin))
  		$date_fin = date('Y-m-d H:i');

  	$jointureUvsNecessaire = true;
  	$champs_sup_jointures .= ", polar_annales_uvs.uv_modif";
  	$whereUv .= " AND (uv_modif >= '$date_debut' AND uv_modif <= '$date_fin')";
  	$descWhereUv .= "<br />UVs modifiées entre <strong>$date_debut</strong> et <strong>$date_fin</strong>";
  }

  // Condition sur la mise en vente des UVs
  if(isset($_POST['condition_enVente'])) {
  	if($_POST['enVente'] == 'enVente_oui') {
  		$whereUv .= " AND uv_enVente = 1";
  		$descWhereUv .= '<br />Uvs <strong>en vente</strong>';
  	}
  	elseif($_POST['enVente'] == 'enVente_non') {
  		$whereUv .= " AND uv_enVente = 0";
  		$descWhereUv .= '<br />UVs <strong>pas en vente</strong>';
  	}

  	$jointureUvsNecessaire = true;
  	$champs_sup_jointures .= ", polar_annales_uvs.uv_enVente";
  }

  // ============================= Conditions appliquées aux sujets ========================== //

  // Condition sur le type de sujet
  if(isset($_POST['condition_type'])) {
  	$whereSujet .= " AND (sujet_type = 'z'"; // Idem, impossible
  	$descWhereSujet .= "<br />Sujets de";
  	if(isset($_POST['type_t'])) {
  		$whereSujet .= " OR sujet_type = 't' OR sujet_type = 'u' OR sujet_type = 'v'";
  		$descWhereSujet .= " Tests,";
  	}
  	if(isset($_POST['type_p'])) {
  		$whereSujet .= " OR sujet_type = 'p' OR sujet_type = 'q'";
  		$descWhereSujet .= " Partiels,";
  	}
  	if(isset($_POST['type_m'])) {
  		$whereSujet .= " OR sujet_type = 'm'";
  		$descWhereSujet .= " Médians,";
  	}
  	if(isset($_POST['type_f'])) {
  		$whereSujet .= " OR sujet_type = 'f'";
  		$descWhereSujet .= " Finaux.";
  	}
  	$whereSujet .= ")";
  }

  // Condition sur le semestre de l'annale
  if(isset($_POST['condition_semestre'])) {
  	$whereSujet .= ' AND sujet_semestre = "'.substr($_POST['semestre'], -1).'"';
  	$descWhereSujet .= '<br />Sujets de <strong>'.ucfirst(substr($_POST['semestre'], -1)).'XX</strong>';
  }

  // Condition sur l'année
  if(isset($_POST['condition_annee'])) {
  	$annee_debut = intval($_POST['annee_debut']);
  	$annee_fin = intval($_POST['annee_fin']);
  	$whereSujet .= " AND (sujet_annee >= $annee_debut AND sujet_annee <= $annee_fin)";

  	$annee_debut += 2000;
  	$annee_fin += 2000;
  	$descWhereSujet .= "<br />Sujets entre <strong>$annee_debut</strong> et <strong>$annee_fin</strong>";
  }

  // Conditions sur le nombre de pages
  if(isset($_POST['condition_pages'])) {
  	$pages_mini = intval($_POST['pages_mini']);
  	$pages_maxi = intval($_POST['pages_maxi']);
  	$whereSujet .= " AND (sujet_pages >= $pages_mini AND sujet_pages <= $pages_maxi)";
  	$descWhereSujet .= "<br />Sujets avec <strong>$pages_mini</strong> à <strong>$pages_maxi</strong> pages";
  }

  // Condition sur les sujets mis à l'écart
  if(isset($_POST['condition_ecartes'])) {
  	if($_POST['ecartes'] == 'ecartes_oui') {
  		$whereSujet .= " AND sujet_ecarte = 1";
  		$descWhereSujet .= '<br />Sujets <strong>mis de côté</strong>';
  	}
  	elseif($_POST['ecartes'] == 'ecartes_non') {
  		$whereSujet .= " AND sujet_ecarte = 0";
  		$descWhereSujet .= '<br />Sujets <strong>en vente</strong>';
  	}
  }

  if(isset($_POST['condition_commentaires'])) {
  	$whereSujet .= " AND (0=1";
  	$descWhereSujet .= "<br />Sujets";
  	if(isset($_POST['commentaire_info'])) {
  		$whereSujet .= " OR sujet_info != ''";
  		$descWhereSujet .= " <strong>avec une info</strong>,";
  	}
  	if(isset($_POST['commentaire_commentaire'])) {
  		$whereSujet .= " OR sujet_commentaire != ''";
  		$descWhereSujet .= " <strong>avec un commentaire</strong>";
  	}
  	$whereSujet .= ")";
  }




  // ===================================================================== //


  if($jointureUvsNecessaire)
  	$jointures .= " LEFT JOIN polar_annales_uvs ON polar_annales_sujets.sujet_uv = polar_annales_uvs.uv_code";

  // On peut rediriger
  $_SESSION['whereSujet'] = $whereSujet;
  $_SESSION['whereUv'] = $whereUv;
  $_SESSION['descWhereSujet'] = $descWhereSujet;
  $_SESSION['descWhereUv'] = $descWhereUv;
  $_SESSION['jointures'] = $jointures;
  $_SESSION['champsSupJointures'] = $champsSupJointures;  

  if($_POST['type'] == 'sujets')
    header("Location: $racine$module/lister");
  elseif($_POST['type'] == 'uvs')
    header("Location: $racine$module/uvs");
  else
    header("Location: $racine$module/lister"); 
}
elseif($_POST['action_formulaire'] == 'raz_filtre'){
  //On commence par Réinitialiser le filtre si demandé
 	unset($_SESSION['whereSujet'], $_SESSION['whereUv'], $_SESSION['descWhereSujet'], $_SESSION['descWhereUv'], $_SESSION['jointures'], $_SESSION['champsSupJointure']);
 	if($_POST['type'] == 'sujets')
     header("Location: $racine$module/lister");
   elseif($_POST['type'] == 'uvs')
     header("Location: $racine$module/uvs");
   else
     header("Location: $racine$module/lister");
   die();
}
