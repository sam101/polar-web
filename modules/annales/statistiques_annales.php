<?php
  // Mise en cache des données entrantes
  ob_start('ob_gzhandler');

  // On sécurise les variables SESSIONS
  ini_set('session.use_trans_sid', 0);
  session_start();
  session_regenerate_id();

  // On empêche l'affichage des erreurs
  error_reporting(0);

  require_once('inc/bdd_racine.php'); // Connexion à la bdd
  require_once('inc/func.php'); // Fonctions usuelles
  require_once('inc/infos_membre.php'); // Variables sur le visiteur
  include 'inc/library.php';

  $tps_ini = getTime();
  $nb_requetes = 0;

  if(isset($_SESSION['con-connecte'], $_SESSION['con-id']) && $_SESSION['con-connecte'] == 'oui' && $_SESSION['con-id'] > 0) {
    $id = intval($_SESSION['con-id']);
    $sql = ('SELECT nom, prenom, presentation, email FROM polar_staff WHERE id='.$id);
    $req = query($sql);

    $i = 0;
    while($donnees = mysql_fetch_row($req)) {
      $nom = $donnees[0];
      $prenom = $donnees[1];
      $i++;
    }


    if($i == 1) {
?>
<!-- Site réalisé par BkM[123] <bkm123m@hotmail.com> pour l'Association "Le polar" -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
  <head>
    <title>Le polar - Le pôle Annales</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" media="screen" type="text/css" title="Feuille de style principale" href="<?php echo $racine; ?>styles/<?php echo $design; ?>/style.css" />

    <?php echo '<script type="text/javascript" src="',$racine,'js/infobulles.js"></script>
    <script type="text/javascript">var visible = false;</script>'; ?>

  </head>

  <body>
    <div id="infobulle"></div>

    <!-- Début Conteneur -->
    <div id="conteneur">
      <!-- Début Contenu -->
      <div id="contenu">

        <?php require_once('inc/header.php'); ?>


        <!-- Début Zone Centrale -->
        <div id="zone-centrale">
          <?php require_once('inc/menu.php'); ?>


          <!-- Début Zone Gauche -->
          <div id="zone-gauche">
            <?php require_once('inc/espace_staff.php'); ?>


            <?php require_once('inc/sous_menu.php'); ?>


            <div class="complement">&nbsp;</div>
          </div>
          <!-- Fin Zone Gauche -->

          <!-- Début Zone Droite -->
          <div id="zone-droite">
            <h1>Statistiques générales</h1>
			<?php
			echo "<h2>Les annales</h2><br />";

			echo "Nombre d'annales en vente ce semestre : <i>";

			$sql_nb_annale = "SELECT COUNT(code) FROM t_annales WHERE envente = 1;";
			$rqt_nb_annale = query($sql_nb_annale);
			$row_nb_annale = mysql_fetch_array($rqt_nb_annale);

			echo $row_nb_annale[0]."</i>.<br>";

			echo "Nombre total d'annales : <i>";

			$sql_total = "SELECT COUNT(code) FROM t_annales;";
			$rqt_total = query($sql_total);
			$row_total = mysql_fetch_array($rqt_total);

			echo $row_total[0]."</i>.<br>";

			echo "Soit <b>".number_format($row_nb_annale[0]/$row_total[0]*100, 2, ',', ' ')."</b>% des annales disponibles.<p>";

			echo "Nombre total de pages disponibles : <i>";

			$sql_nbpages = "SELECT SUM(nbpages) FROM t_annales WHERE envente = 1";
			$rqt_nbpages = query($sql_nbpages);
			$row_nbpages = mysql_fetch_array($rqt_nbpages);

			echo $row_nbpages[0]."</i>.<br>";

			echo "Nombre total de pages des annales : <i>";

			$sql_total_page = "SELECT SUM(nbpages) FROM t_annales;";
			$rqt_total_page = query($sql_total_page);
			$row_total_page = mysql_fetch_array($rqt_total_page);

			echo $row_total_page[0]."</i>.<br>";

			echo "Soit <b>".number_format($row_nbpages[0]/$row_total_page[0]*100, 2, ',', ' ')."</b>% de pages d'annales disponibles.<p>";

			echo "Prix par page: <b>".prixparpage()."&euro;</b><p>";

			if($row_nb_annale[0] !=0)
				$page_annale_vente = $row_nbpages[0]/$row_nb_annale[0];
			else
				$page_annale_vente = 0;
			$page_annale_total = $row_total_page[0]/$row_total[0];

			echo "Nombre moyen de pages par annale en vente : <i>".number_format($page_annale_vente, 2, ',', ' ')." (".number_format($page_annale_vente*prixparpage(), 2, '.', ' ')."&euro;)</i>.<br>
			Nombre moyen de pages pour toutes les annales: <i>".number_format($page_annale_total, 2, ',', ' ')." (".number_format($page_annale_total*prixparpage(), 2, '.', ' ')."&euro;)</i>.<p><p>";

			echo "<h2>Les commandes</h2><br>";

			echo "Nombre total de commandes : <i>";

			$sql_total_comm = "SELECT COUNT(numcommande) FROM t_n_commandes;";
			$rqt_total_comm = query($sql_total_comm);
			$row_total_comm = mysql_fetch_array($rqt_total_comm);

			echo $row_total_comm[0]."</i>.<br>
			Dont imprim&eacute;es: <i>";

			$sql_imp_comm = "SELECT COUNT(numcommande) FROM t_n_commandes WHERE numsceance IS NOT NULL;";
			$rqt_imp_comm = query($sql_imp_comm);
			$row_imp_comm = mysql_fetch_array($rqt_imp_comm);

			if($row_total_comm[0] != 0)
				echo $row_imp_comm[0]."</i> (soit <b>".number_format($row_imp_comm[0]/$row_total_comm[0]*100, 2, ',', ' ')."%</b>).<br>
			Dont distribu&eacute;es: <i>";
			else
				echo $row_imp_comm[0]."</i> (soit <b>".number_format(0, 2, ',', ' ')."%</b>).<br>
			Dont distribu&eacute;es: <i>";

			$sql_dist_comm = "SELECT COUNT(numcommande) FROM t_n_commandes WHERE dateretire IS NOT NULL;";
			$rqt_dist_comm = query($sql_dist_comm);
			$row_dist_comm = mysql_fetch_array($rqt_dist_comm);

			if($row_total_comm[0] != 0)
				echo $row_dist_comm[0]."</i> (soit <b>".number_format($row_dist_comm[0]/$row_total_comm[0]*100, 2, ',', ' ')."%</b>).<p>";
			else
				echo $row_dist_comm[0]."</i> (soit <b>".number_format(0, 2, ',', ' ')."%</b>).<p>";

			echo "Nombre total d'annales vendues : <i>";

			$sql_ann = "SELECT COUNT(numcommande) FROM t_c_commandes;";
			$rqt_ann = query($sql_ann);
			$row_ann = mysql_fetch_array($rqt_ann);

			echo $row_ann[0]."</i>.<br>";

			echo "Nombre total de pages vendues : <i>";
			$sql_page = "SELECT sum( nbpages )
			FROM t_c_commandes
			INNER JOIN t_annales ON t_annales.code = t_c_commandes.code;";
			$rqt_page = query($sql_page);
			$row_page = mysql_fetch_array($rqt_page);

			echo $row_page[0]." (".number_format($row_page[0]*prixparpage(), 2, '.', ' ')."&euro;)</i>.<p>";

			echo "Nombre moyen d'annales par commande : <i>";

		// 	On en peut pas faire de sous-requête sur la version actuellement disponible à l'UTC de MySQL
		// 	Pensez à simplifier ce bout de code si l'on passe à MySQL 4.1
		// 	SELECT AVG(SousTab.nb)
		// 	FROM(
		// 	SELECT numcommande, COUNT(code) AS nb
		// 	FROM t_c_commandes
		// 	GROUP BY numcommande
		// 	ORDER BY numcommande) AS SousTab;

			$sql_compt = "SELECT numcommande, COUNT(code) as nb
			FROM t_c_commandes
			GROUP BY numcommande
			ORDER BY numcommande;";
			$rqt_compt = query($sql_compt);
			$somme_annale = 0;
			while($row_compt = mysql_fetch_array($rqt_compt))
				$somme_annale += $row_compt['nb'];

			if($row_total_comm[0] != 0)
				echo number_format($somme_annale/$row_total_comm[0], 2, ',', ' ')."</i>.<br>";
			else
				echo number_format(0, 2, ',', ' ')."</i>.<br>";

			echo "Nombre moyen de pages par commande : <i>";

		// 	Pareil que plus haut, il faudra penser à optimiser cette commande dès que possible (i.e. à l'installation
		// 	de MySQL 4.1 ou supérieur à l'UTC)
		// 	SELECT AVG(SousTab.page) AS moy_pages
		// 	FROM(
		// 	SELECT t_c_commandes.numcommande, SUM(t_annales.nbpages) AS page
		// 	FROM t_c_commandes
		// 	INNER JOIN t_annales
		// 	ON t_c_commandes.code = t_annales.code
		// 	GROUP BY numcommande
		// 	ORDER BY numcommande) AS SousTab;

			$sql_compt = "SELECT numcommande, SUM(t_annales.nbpages) AS nb
			FROM t_c_commandes
			INNER JOIN t_annales ON t_c_commandes.code = t_annales.code
			GROUP BY numcommande
			ORDER BY numcommande;";
			$rqt_compt = query($sql_compt);
			$somme_page = 0;
			while($row_compt = mysql_fetch_array($rqt_compt))
				$somme_page += $row_compt['nb'];

			if($row_total_comm[0] != 0)
				echo number_format($somme_page/$row_total_comm[0], 2, ',', ' ')." (".number_format($somme_page/$row_total_comm[0]*prixparpage(), 2, '.', ' ')."&euro;)</i>.<p>";
			else
				echo number_format(0, 2, ',', ' ')." (".number_format(0*prixparpage(), 2, '.', ' ')."&euro;)</i>.<p>";

			echo "Nombre moyen de commandes faites par jour ouvr&eacute; : <i>";

			$sql_jour_vente = "SELECT COUNT(DISTINCT t_n_commandes.numcommande) commande, SUM(t_annales.nbpages) pages, COUNT(DISTINCT jourcommande) jour
			FROM (
			t_n_commandes
			NATURAL JOIN t_c_commandes
			)
			NATURAL JOIN t_annales;";
			$rqt_jour_vente = query($sql_jour_vente);
			$row_jour_vente = mysql_fetch_array($rqt_jour_vente);

			if($row_total_comm[0] != 0)
				echo number_format($row_jour_vente['commande']/$row_jour_vente['jour'], 2, ',', ' ')."</i>.<br>";
			else
				echo number_format(0, 2, ',', ' ')."</i>.<br>";

			echo "Nombre moyen de pages vendues par jour ouvr&eacute; : <i>";

			if($row_total_comm[0] != 0)
				echo number_format($row_jour_vente['pages']/$row_jour_vente['jour'], 2, ',', ' ')." (".number_format($row_jour_vente['pages']/$row_jour_vente['jour']*prixparpage(), 2, '.', ' ')."&euro;)</i>.<p>";
			else
				echo number_format(0, 2, ',', ' ')." (".number_format(0*prixparpage(), 2, '.', ' ')."&euro;)</i>.<p>";

			if($row_ann[0] != 0)
			{
				echo "Top 5 des meilleures ventes :<br><ul type=square>";

				$sql_top = "SELECT code, COUNT(code) nbre
				FROM t_c_commandes
				GROUP BY code
				ORDER BY nbre DESC
				LIMIT 0, 5;";
				$rqt_top = query($sql_top);

				while($row_top = mysql_fetch_array($rqt_top))
				{
					echo "<li>".$row_top['code'].".....".$row_top['nbre']." commandes (soit <b>".number_format($row_top['nbre']/$row_ann[0]*100, 2, ',', ' ')."</b>% des annales disponibles).";
				}

				echo "</ul><p>";
			}

			echo "<h2>Les s&eacute;ances d'impression</h2><br>";

			$sql_sc_imp = "SELECT COUNT(numsceance)
			FROM t_sceances;";
			$rqt_sc_imp = query($sql_sc_imp);
			$row_sc_imp = mysql_fetch_array($rqt_sc_imp);

			echo "Nombre de s&eacute;ances d'impression : <i>".$row_sc_imp[0]."</i>.<br>";

			$sql_ann = "SELECT SUM(nbannales)
			FROM t_sceances;";
			$rqt_ann = query($sql_ann);
			$row_ann = mysql_fetch_array($rqt_ann);

			echo "Nombre total d'annales imprim&eacute;es : <i>".$row_ann[0]."</i>.<br>";

			$sql_imp = "SELECT SUM(nbpagesreel)
			FROM t_sceances;";
			$rqt_imp = query($sql_imp);
			$row_imp = mysql_fetch_array($rqt_imp);

			$sql_perte = "SELECT (SUM(nbpagesreel) - SUM(nbpagesth)) perte
			FROM t_sceances;";
			$rqt_perte = query($sql_perte);
			$row_perte = mysql_fetch_array($rqt_perte);

			if($row_imp[0] != 0)
				echo "Nombre de pages imprim&eacute;es : <i>".$row_imp[0]."</i>.<br>
			Dont pertes: <i>".$row_perte[0]."</i> (soit <b>".number_format($row_perte[0]/$row_imp[0]*100, 2, ',', ' ')."%</b>).<p>";
			else
				echo "Nombre de pages imprim&eacute;es : <i>".$row_imp[0]."</i>.<br>
			Dont pertes: <i>".$row_perte[0]."</i> (soit <b>".number_format(0*100, 2, ',', ' ')."%</b>).<p>";

			if($row_sc_imp[0] != 0)
				echo "Nombre moyen d'annales imprim&eacute;es par s&eacute;ances : <i>".number_format($row_ann[0]/$row_sc_imp[0], 2, ',', ' ')."</i>.<br>";
			else
				echo "Nombre moyen d'annales imprim&eacute;es par s&eacute;ances : <i>".number_format(0, 2, ',', ' ')."</i>.<br>";

			if($row_sc_imp[0] != 0)
				echo "Nombre moyen de pages imprim&eacute;es par s&eacute;ances : <i>".number_format($row_imp[0]/$row_sc_imp[0], 2, ',', ' ')."</i>.<br>";
			else
				echo "Nombre moyen de pages imprim&eacute;es par s&eacute;ances : <i>".number_format(0, 2, ',', ' ')."</i>.<br>";

			if($row_sc_imp[0] != 0)
				echo "Nombre moyen de pertes par s&eacute;ance : <i>".number_format($row_perte[0]/$row_sc_imp[0], 2, ',', ' ')."</i>.<br>";
			else
				echo "Nombre moyen de pertes par s&eacute;ance : <i>".number_format(0, 2, ',', ' ')."</i>.<br>";

			if($row_ann[0] != 0)
				echo "Nombre moyen de pages par annale imprim&eacute;e : <i>".number_format($row_imp[0]/$row_ann[0], 2, ',', ' ')."</i>.<br>";
			else
				echo "Nombre moyen de pages par annale imprim&eacute;e : <i>".number_format(0, 2, ',', ' ')."</i>.<br>";

			$sql_heure = "SELECT (
			TIME_TO_SEC(hfin) - TIME_TO_SEC(hdebut)
			)temps_a, (
			TIME_TO_SEC('23:59:59') - TIME_TO_SEC(hdebut) + TIME_TO_SEC(hfin) +1
			)temps_r
			FROM t_sceances";
			$rqt_heure = query($sql_heure);
			$total = 0;
			while($row_heure = mysql_fetch_array($rqt_heure))
			{
				if($row_heure['temps_a'] > 0)
					$total += $row_heure['temps_a'];
				else
					$total += $row_heure['temps_r'];
			}

			if($row_sc_imp[0] != 0)
				echo "Temps total de production : <i>".date("z \\j\o\u\\r\(\s\)  H:i:s", $total-3600)."</i>.<br>
			Temps moyen de production par s&eacute;ance: <i>".date("H:i:s", number_format((($total/$row_sc_imp[0])-3600), 0, '', '')) ."</i>.<p>";
			else
				echo "Temps total de production : <i>".date("H:i:s", $total-3600)."</i>.<br>
			Temps moyen de production par s&eacute;ance: <i>".date("H:i:s", number_format((0), 0)-3600)."</i>.<p>";

			echo "<h2>Les retraits</h2><br>";

			$sql_ret = "SELECT COUNT(numcommande)
			FROM t_n_commandes
			WHERE dateretire IS NOT NULL;";
			$rqt_ret = query($sql_ret);
			$row_ret = mysql_fetch_array($rqt_ret);

			echo "Nombre de commandes distribu&eacute;es : <i>".$row_ret[0]."</i>.<br>";

			$sql_retire = "SELECT COUNT(t_c_commandes.numcommande)
			FROM t_c_commandes
			NATURAL JOIN t_n_commandes
			WHERE t_n_commandes.dateretire IS NOT NULL;";
			$rqt_retire = query($sql_retire);
			$row_retire = mysql_fetch_array($rqt_retire);

			echo "Nombre d'annales distribu&eacute;es : <i>".$row_retire[0]."</i>.<p>";

			$sql_jour = "SELECT COUNT(DISTINCT dateretire)
			FROM t_n_commandes;";
			$rqt_jour = query($sql_jour);
			$row_jour = mysql_fetch_array($rqt_jour);

			if($row_jour[0] != 0)
				echo "Nombre de jour de distribution : <i>".$row_jour[0]."</i>.<br>
			Nombre moyen de commandes distribu&eacute;es par jour de distribution: <i>".number_format($row_ret[0]/$row_jour[0], 2, ',', ' ')."</i>.<br>
			Nombre moyen d'annales distribu&eacute;es par jour de distribution : <i>".number_format($row_retire[0]/$row_jour[0], 2, ',', ' ')."</i>.<p>";
			else
				echo "Nombre de jour de distribution : <i>".$row_jour[0]."</i>.<br>
			Nombre moyen de commandes distribu&eacute;es par jour de distribution: <i>".number_format(0, 2, ',', ' ')."</i>.<br>
			Nombre moyen d'annales distribu&eacute;es par jour de distribution : <i>".number_format(0, 2, ',', ' ')."</i>.<p>";

			$sql_nonret = "SELECT COUNT(numcommande)
			FROM t_n_commandes
			WHERE dateretire IS NULL AND numsceance IS NOT NULL;";
			$rqt_nonret = query($sql_nonret);
			$row_nonret = mysql_fetch_array($rqt_nonret);

			echo "Nombre de commandes non-distribu&eacute;es : <i>".$row_nonret[0]."</i>.<br>";

			$sql_anonret = "SELECT COUNT(t_n_commandes.numcommande)
			FROM t_n_commandes
			NATURAL JOIN t_c_commandes
			WHERE dateretire IS NULL AND numsceance IS NOT NULL;";
			$rqt_anonret = query($sql_anonret);
			$row_anonret = mysql_fetch_array($rqt_anonret);

			echo "Nombre d'annales non-distribu&eacute;es : <i>".$row_anonret[0]."</i>.<br>";

			?>

          </div>

          <!-- Fin Zone Droite -->

          <div id="clearer"></div>
          <?php require_once('inc/footer.php'); ?>

          <div class="complement">&nbsp;</div>
        </div>
        <!-- Fin Zone Centrale -->
      </div>
      <!-- Fin contenu -->
    </div>
    <!-- Fin Conteneur -->
  </body>
</html>
<?php
    }
    else
        header('Location: '.$racine);
  }
  else
    header('Location: '.$racine);

  // Fermeture de la connexion à la bdd
  mysql_close();

  // Fin de la mise en cache
  ob_end_flush();
?>

