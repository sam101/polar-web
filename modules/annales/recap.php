<?php
if(isset($_GET['catalogue'])){
?>
<!DOCTYPE html>
<html>
<head>
	<title>Le super catalogue</title>
	<meta name="language" content="fr" />
	<meta charset="utf-8" />
	<style>
		/**************************** Catalogue *******************************/
		#catalogue {
			-webkit-column-count: 5; /* Ok sur @screen, mais bug sur @print */
			-moz-column-count: 5;
			column-count: 5;
			-webkit-column-rule: 1px solid black;
			-moz-column-rule: 1px solid black;
			column-rule: 1px solid black;
		}

		#info-catalogue {
			width: 100%;
			font-family: calibri;
			text-align: center;
			height: 115px;
			display: inline-block;
			vertical-align: middle;
			padding: 0px 10px;
		}

		#catalogue .uv {
			border-top: 1px solid black;
			font-family: calibri;
			font-size: 10px;
			margin: 0px;
			text-align: center;
			margin-top: 5px;
			padding-top: 5px;
			-webkit-column-break-inside: avoid;	/* Ok sur Chrome */
			-moz-column-break-inside: avoid; /* ...mais pas sur FF */
			column-break-inside: avoid;
			display: table;
			width: 100%;
		}

		#catalogue .titre{
			font-size: 20px;
			font-weight: bold;
		}

		#catalogue .type{
			font-size: 12px;
			text-decoration: underline;
		}

		#catalogue p {
			padding: 0px;
			margin: 0px;
		}
	</style>
</head>
<body>
<?php
$requete = query("SELECT sujet_uv, sujet_type, sujet_semestre, sujet_annee, sujet_corrige, sujet_info, uv_enVente, uv_info
						FROM polar_annales_sujets pas
						LEFT JOIN polar_annales_uvs pau ON pas.sujet_uv = pau.uv_code
						WHERE uv_enVente = 1 AND sujet_ecarte = 0
						ORDER BY sujet_uv, sujet_type, sujet_annee, sujet_semestre DESC, sujet_corrige");
$uv = '';
$type = '';
$types = array('t' => 'Tests', 'u' => 'Tests 2', 'v' => 'Tests 3', 'p' => 'Partiels', 'q' => 'Partiels 2', 'm' => 'Médians', 'f' => 'Finaux');

$semestre = getSemestre();
?>
<div id="info-catalogue">
	<img src="<?php echo $racine; ?>/upload/logo.png" alt="logo" style="height: 80px; float: left;" />
	<span style="font-size: 44px; font-weight: bold;">Annales en vente <?php echo $semestre; ?></span><br />
	<span style="font-size: 20px; font-style: italic;">Liste donnée à titre indicatif</span>
</div>

<div id="catalogue">
<div>
<?php
while($donnees = mysql_fetch_assoc($requete)) {
	if($donnees['sujet_uv'] != $uv) {
		$uv = $donnees['sujet_uv'];
		$type = '';

		echo '</div>';

		echo '<div class="uv">
			<p><span class="titre">'.$uv.'</span>';
	}
	if($donnees['sujet_type'] != $type) {
		if(in_array($type, array('t', 'u', 'v', 'p', 'q')))
			echo '</p>';

		$type = $donnees['sujet_type'];
		if(in_array($type, array('t', 'u', 'v', 'p', 'q')))
			echo '<p class="colonnes" style="margin-top: 3px;">';
		else
			echo '<p style="margin-top: 3px;">';

		echo '<span class="type">'.$types[$donnees['sujet_type']].'</span><br />';
	}
	echo $donnees['sujet_semestre'].($donnees['sujet_annee'] < 10 ? '0'.$donnees['sujet_annee'] : $donnees['sujet_annee']);
	if($donnees['sujet_corrige'] == 1)
		echo ' + Corrigé';

	if(!empty($donnees['sujet_info']))
		echo ' - '.$donnees['sujet_info'].'</span>';

	echo '<br />';
}
?>
	</div>
</div>
</body>
</html>
<?php
}
else {
addHeaders('
<style type="text/css">
/*------- Recap ----------*/
table.annalestab {
	border-collapse: collapse;
}

table.annalestab th {
	background-color: brown;
	font-family: Calibri;
	color: white;
	border: 1px solid white;
	padding: 3px;
}
#tab_recap tr {
  background: #fff;
}
#tab_recap tr:hover {
	background: yellow;
}
table.annalestab td {
	border: brown 1px solid;
	padding: 2px;
}
#tab_recap tr:hover {
	background: yellow;
}
#tab_recap .separation {
	background-color: brown;
}
#tab_recap td.enonce {
	background-color: lightgreen;
}
#tab_recap td.corrige {
	background-color: green;
}
</style>
');
$titrePage = 'Récapitulatif des sujets';
require("inc/header.php");

afficherErreurs();

?>
<h1><?php echo $titrePage; ?></h1>
<p><a href="<?php echo "$racine$module/$section"; ?>?catalogue">Afficher le catalogue (version imprimable des annales en vente)</a></p>
<?php

if(isset($_GET['annee_debut'], $_GET['annee_fin'])) {
	$annee_debut = $_GET['annee_debut'];
	$annee_fin = $_GET['annee_fin'];
}
else {
	$annee_fin = date('y');
	$annee_debut = date('y') - 3;
}

echo '<form method="get" action="'.$racine.$module.'/'.$section.'">
		Afficher les semestres P<input type="text" name="annee_debut" size="2" maxlength="2" value="'.$annee_debut.'" /> à A<input type="text" name="annee_fin" size="2" maxlength="2" value="'.$annee_fin.'" />
		<input type="submit" value="Afficher" />
	</form>';

$requete = query("SELECT sujet_uv, sujet_type, sujet_semestre, sujet_annee, sujet_corrige, sujet_commentaire
				FROM polar_annales_sujets
				WHERE sujet_annee >= $annee_debut AND sujet_annee <= $annee_fin
				ORDER BY sujet_uv, sujet_annee, sujet_semestre DESC, sujet_type");

echo '<p><strong>T</strong> Test - <strong>P</strong> Partiel - <strong>M</strong> Médian - <strong>F</strong> Final<br />
<span style="color: lightgreen; font-weight: bold;">E</span> Enoncé récupéré - <span style="color: green; font-weight: bold;">C</span> Enoncé ET Corrigé récupérés</p>';
echo '<table id="tab_recap" class="annalestab">';
	$types = 'tuvpqmf';
	$nbr_types = strlen($types);
	$uv = '';
	$annee = $annee_debut;
	$numero_type = 0;
	$semestre = 'P';
	$n = 20; // Toutes les X UVs, on réaffiche les en-tête. On met X dès le début pour pouvopir afficher une en-tête sur la 1ère ligne.

	while ($donnees = mysql_fetch_assoc($requete)) {
		if($uv != $donnees['sujet_uv']) {
			if($uv != '') { // On évite ainsi la toute 1ère ligne
				//On finit la ligne de l'UV précédente
				for($i = $numero_type; $i < $nbr_types; $i++) {
					echo '<td class="non">.</td>';
				}
				echo '<td class="separation">.</td>';

				//On remplit avec des cases vides (c'est pas au point mais bon...)
				if($semestre == 'P') {
					echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
					echo '<td class="separation">.</td>';
				}
				$annee++;
				for($a = $annee; $a <= $annee_fin; $a++) {
					echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
					echo '<td class="separation">.</td>';
					echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
					echo '<td class="separation">.</td>';
				}

				echo '</tr>';
			}


			// On réaffiche régulièrement les en-têtes
			if($n == 20) {
				$n = 0;
				echo '<tr>
					<th rowspan="2">UV</th>';
					echo '<th class="separation">.</th>';
					for($i = $annee_debut; $i <= $annee_fin; $i++) {
						echo '<th colspan="'.$nbr_types.'">P'.$i.'</th>
								<th class="separation">.</th>
								<th colspan="'.$nbr_types.'">A'.$i.'</th>';
						echo '<th class="separation">.</th>';
					}
				echo '</tr>
				<tr>';
					for($i = $annee_debut; $i <= $annee_fin; $i++) {
						for($j = 0; $j <= 1; $j++) {
							echo '<th class="separation">.</th>';
							for($k = 0; $k < $nbr_types; $k++) {
								echo '<th>'.strtoupper(substr($types, $k, 1)).'</th>';
							}
						}
					}
					echo '<th class="separation">.</th>';
			}

			//Et on commence la ligne de la nouvelle UV
			$n++;
			$uv = $donnees['sujet_uv'];
			$annee = $annee_debut;
			$semestre = 'P';
			$numero_type = 0;
			echo '<tr>
				<td>'.$donnees['sujet_uv'].'</td>';
			echo '<td class="separation">.</td>';
		}


		// On change d'année.
		if($annee != $donnees['sujet_annee']) {
			// On commence par compléter le semestre en cours, puis on se place sur l'année demandée.
			for($i = $numero_type; $i < $nbr_types; $i++) {
				echo '<td class="non">.</td>';
			}
			echo '<td class="separation">.</td>';

			if($semestre == 'P') {
				echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
				echo '<td class="separation">.</td>';
			}

			$annee++;
			for($a = $annee; $a < $donnees['sujet_annee']; $a++) {
				echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
				echo '<td class="separation">.</td>';
				echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
				echo '<td class="separation">.</td>';
			}

			if($donnees['sujet_semestre'] == 'A') {
				echo '<td class="non" colspan="'.$nbr_types.'">.</td>';
				echo '<td class="separation">.</td>';
			}

			$annee = $donnees['sujet_annee'];
			$numero_type = 0;
		}
		// L'année reste la même, mais le semestre change
		elseif($semestre != $donnees['sujet_semestre']) {
			//On finit de remplir le semestre précédent pour se positionner sur le semestre en cours.
			for($i = $numero_type; $i < $nbr_types; $i++) {
				echo '<td class="non">.</td>';
			}
			echo '<td class="separation">.</td>';
			$numero_type = 0;
		}

		$semestre = $donnees['sujet_semestre'];

		$numero_type_annale = strpos($types, $donnees['sujet_type']);
		for($i = $numero_type; $i < $numero_type_annale; $i++) {
			echo '<td class="non">.</td>';
		}
		$numero_type = strpos($types, $donnees['sujet_type']) + 1; //+1 parce que c'est comme ça

		if($donnees['sujet_corrige'] == 1)
			echo '<td class="corrige">C</td>';
		else
			echo '<td class="enonce">E</td>';
	}

	//On finit le tableau
	//(A faire ?)

echo '</table>';

require("inc/footer.php");
}
?>
