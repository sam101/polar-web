<div class="text-center">
  <div style="display:inline-block;text-align:left;">
    <h1>Authentification extérieurs</h1>
    <form class="form-horizontal" method="POST" action"<?php echo $racine; ?>">
    <?php if(isset($_GET['erreur_login'])) { ?>
    <div class="alert alert-error">
      <strong>Échec d'authentification !</strong> Adresse mail ou mot de passe incorrect.
    </div>
    <?php } ?>
    <div class="control-group">
      <label class="control-label" for="inputEmail">Email</label>
      <div class="controls">
        <input type="text" id="inputEmail" name="ext-mail">
        </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="inputPassword">Mot de passe</label>
      <div class="controls">
        <input type="password" id="inputPassword" name="ext-mdp">
        </div>
    </div>
    <div class="control-group">
      <div class="controls">
        <button type="submit" class="btn btn-primary">Valider</button>
      </div>
    </div>
  </form>
</div>
</div>