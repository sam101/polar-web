<?php
header("HTTP/1.0 403 Forbidden");
include("inc/header.php");
?>
<div class="text-center">
  <h1>Vous devez être connecté pour accéder à cette page</h1>
  <p>Vous essayez d'accéder à une page privée, pour y accéder merci de vous authentifier<br/><br>
  <a href="<?php echo $CONF['cas_login'] ?>" class="btn btn-primary btn-large">Utilisateurs CAS (UTC)</a><br/><br>
  <a href="<?php echo $racine ?>/membres/connexion" class="btn btn-primary btn-large">Utilisateurs extérieurs</a></p>
</div>
<?php
include("inc/footer.php");
?>
