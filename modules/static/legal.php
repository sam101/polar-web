<?php
$titrePage = "Mentions légales";
require_once('inc/header.php');

$sql = query("SELECT Valeur FROM polar_parametres WHERE Nom LIKE 'prez'");
$prez = mysql_fetch_assoc($sql);
?>
<h1>Mentions légales</h1>
<p>Ce site est édité par l'association :<br />
<b>Promotion des Oeuvres Livresques Au Rabais</b><br />
Université de Technologie de Compiègne<br />
Rue Roger Couttolenc<br />
60200 Compiègne<br />
Tél. : 03 44 23 43 73<br />
E-mail : polar@assos.utc.fr<br />
SIRET : 42898163300012</p>
<?php
//Numéro de déclaration à la préfecture de xxx :
?>

<p>L'éditeur de ce site est le président de l'association : <?php echo $prez['Valeur']; ?>.</p>

<p>Ce site est hébergé par le BDE-UTC.</p>

<h2>Données personnelles</h2>

<p>Les informations recueillies sur ce site sont nécessaires afin de vous identifier en tant qu'étudiant et de permettre le traitement de votre commande.
Elles font lobjet dun traitement informatique et sont destinées à l'association.
Conformément à la loi « informatique et libertés » du 6 janvier 1978
modifiée en 2004, vous bénéficiez dun droit daccès et de
rectification aux informations qui vous concernent. Si vous souhaitez exercer ce droit et obtenir communication des informations
vous concernant, veuillez vous adresser à l'association, à l'adresse indiquée ci-dessus.</p>

<h2>Cookie</h2>
<p>Ce site web implante un cookie dans votre ordinateur.
Ce cookie enregistre des informations relatives à votre navigation sur
notre site et stocke un identifiant permettant de vous garder connecter.
Ce cookie est supprimé lors de la fermeture de votre navigateur.</p>
<p>Nous vous informons que vous pouvez vous opposer à l'enregistrement de
"cookies" en configurant votre navigateur de la manière suivante :</p>
<p>
Pour Mozilla firefox :
<ol>
<li>Choisissez le menu "Outils " puis "Options"</li>
<li>Cliquez sur l'icône "Vie privée"</li>
<li>Repérez le menu "Cookie" et sélectionnez les options qui vous conviennent</li>
</ol>
</p>
<p>
Pour Microsoft Internet Explorer :
<ol>
<li>Choisissez le menu "Outils" puis "Options Internet"</li>
<li>Cliquez sur l'onglet "Confidentialité"</li>
<li>Sélectionnez le niveau souhaité à l'aide du curseur</li>
</ol>
</p>
<p>
Pour Opéra 6.0 et au-delà :
<ol>
<li>Choisissez le menu "Fichier">"Préférences"</li>
<li>Vie Privée</li>
</ol>
</p>
<?php
require_once('inc/footer.php');
?>
