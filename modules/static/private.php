<?php
require_once('inc/header.php');
?>
<div class="text-center">
  <h1>Ré-authentification nécessaire</h1>
  <p>Cette page nécessite la confirmation de votre mot de passe.<br/>
  <?php if($user->is_logged_with_cas()): ?>
  <b><a class="btn btn-large btn-primary" href="<?php echo $CONF['cas_login'] ?>">Accèder au CAS</a></b>
  <?php elseif ($user->is_logged_with_password()):
    include("modules/static/_password_form.php");
  endif; ?>
</p>
<?php
if(!empty($_POST) || !empty($_GET))
	echo '<p>Les données que vous avez saisi sur la page précédente seront transmises. <b>Ne refaites pas l\'action !</b></p>';

?>
</div>
<?php require_once('inc/footer.php'); ?>
