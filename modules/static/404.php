<?php
header("HTTP/1.0 404 Not Found");
include("inc/header.php");
?>
<div class="text-center">
  <h1>4 x 100 + 4</h1>
  <p>La page que vous avez tenté de visiter n'existe pas !<br/><br>
  <a href="<?php echo $racine ?>" class="btn btn-primary btn-large">Retour à l'accueil</a></p>
</div>
<?php
include("inc/footer.php");
?>
