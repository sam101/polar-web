<?php require_once('inc/header.php'); ?>
<h1>Ils participent au d&eacute;veloppement</h1>
<ul>
  <li>
    <b>Florent Thévenet,</b> sur la maintenance du site et le design actuel
  </li>
  <li>
  	<b>Antoine GAVEL,</b> contribueur occasionel
  </li>
</ul>
<h1>Ils ont particip&eacute;</h1>
<ul>
	<li>
		<b>Anis SAFINE,</b> sur la structure originale du site et son design
	</li>
	<li>
		<b>Cl&eacute;ment VERET,</b> sur le module de commande d'annales
	</li>
	<li>
		<b>Geoffrey THIESSET,</b> sur les modules actuels et la partie bases de données
	</li>
	<li>
		<b>Thomas LUL&Eacute;,</b> sur l'interface d'enregistrement des ventes
	</li>
	<li>
		<b>Arthur PUYOU,</b> sur l'architecture actuelle du site et la création de nouveaux modules
	</li>
    <li>
      <b>Nicolas PAUSS,</b> sur le développement de nouveaux modules
    </li>
    <li>
      <b>Rémi SEGUIN,</b> sur la gestion des annales
    </li>
</ul>
<?php require_once('inc/footer.php'); ?>
