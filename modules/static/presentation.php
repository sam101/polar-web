<?php
include("inc/header.php");
?>

            <h1>C'est quoi le Polar ?</h1>
            <p>Le Polar est une association loi 1901, cr&eacute;&eacute;e en 1978 par trois visionnaires: <br />
Fabrice GIRARD, Philippe PIERRARD et Xavier DUPEYRON.<br/>
<br />
De son vrai nom Promotion des Œuvres Livresques Au Rabais (quel subtil jeu de mot <img style="vertical-align:middle;" src="/polar/styles/0/smileys/4.gif" alt="Smiley 4" /> ), le Polar est enti&egrave;rement g&eacute;r&eacute; par des &eacute;tudiants. Le Bureau actuel contient six personnes, auxquelles s'ajoutent les responsables des diff&eacute;rents secteurs. Le d&eacute;tail de la Dream Team du Polar est disponible dans la rubrique &quot;L'&eacute;quipe&quot; dans le menu en bas &agrave; gauche.<br/><br/>Son but est de centraliser les achats des &eacute;tudiants pour obtenir les prix les plus bas possibles pour du mat&eacute;riel de bonne qualit&eacute;, dans des domaines aussi vari&eacute;s que les impressions, les fournitures, les accessoires informatique, les produits d&eacute;riv&eacute;s UTC...<br />
<br />
Le Polar ne propose pas les prix les plus bas du march&eacute;, tu trouveras toujours moins cher sur Internet. Il s'efforce de proposer le prix le plus bas sur la gamme propos&eacute;e, c'est &agrave; dire des produits de bonne qualité. Le Polar, c'est aussi le service &agrave; l'&eacute;tudiant, avec une prise en charge en garantie imm&eacute;diate et un remboursement rapide.<br />
<br />
Au Polar, tu peux &eacute;galement <span class="gras">commander tes annales d'examens</span>, <span class="gras">louer un vid&eacute;oprojecteur</span>, <span class="gras">acheter tes places pour les concerts</span>, <span class="gras">ESTU</span>, et diff&eacute;rents &eacute;v&eacute;nements organis&eacute;s par les assos de l'UTC, <span class="gras">louer les livres de langues</span>.... et plein d'autres choses ! <img style="vertical-align:middle;" src="/polar/styles/0/smileys/1.gif" alt="Smiley 1" /><br />
<br />
Sache d&egrave;s &agrave; pr&eacute;sent que si tu int&egrave;gres le Polar, tu auras droit &agrave; des avantages non n&eacute;gligeables : acc&egrave;s au local entre 8h et 20h, priorit&eacute; des impressions personnelles, 50% d'abattement sur les impressions personnelles, gratuit&eacute; d'utilisation du mat&eacute;riel de l'association. Pour int&eacute;grer le Polar, envoie un mail &agrave; polar@assos.utc.fr !            </p>
<?php
include("inc/footer.php");
?>


