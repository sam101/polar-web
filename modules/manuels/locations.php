<?php
$titrePage = "Locations de manuels";
use_jquery_ui();
use_datatables();
addFooter('
	<script>
	function infos(id, uv, etu, manuel, paye, retrait, retour, termine){
		$("#com-id").html(id);
		$("#com-uv").html(uv);
		$("#com-etudiant").html(etu);
		$("#com-manuel").html(manuel);
		$("#com-paiement").html(paye);
		$("#com-retrait").html(retrait);
		$("#com-retour").html(retour);
		if(termine)
			$("#rboutons").hide();
		else
			$("#rboutons").show();
		$("#dialog").dialog("open");
	}
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });

		$("#restitution").click(function(){
			var id = $("#com-id").html();
			document.location = "'.$racine.'manuels/retour_control?id="+id;
		});
		$("#encaisser").click(function(){
			var id = $("#com-id").html();
			var banque=prompt("Le manuel sera marqué comme perdu et le chèque encaissé.\n\nQuelle est la banque du chèque ?");
			if(banque != null)
				document.location = "'.$racine.'manuels/locations_control?Encaisser="+id+"&banque="+banque;
		});
		$("#attestation").click(function(){
			var id = $("#com-id").html();
			var num=prompt("Numéro du chèque ?");
			if(num > 0)
				document.location = "'.$racine.'manuels/locations_control?attestation&id="+id+"&cheque="+num;
		});
	});
	</script>');
require("inc/header.php");
?>
<h1>Les locations</h1>
<p><a href="<?php echo $racine.$module.'/'.$section; ?>?toutes">Afficher toutes les locations</a></p>
<table class="datatables table table-bordered table-striped">
	<thead>
		<tr><th>ID</th><th>UV</th><th>Nom</th><th>Prénom</th><th>Mail</th><th>Infos</th></tr>
	</thead>
	<tbody>
<?php
if(isset($_GET['toutes']))
	$suppl = "";
else
	$suppl = " AND Termine=0";

$req = query("SELECT pc.ID,pc.Nom,Prenom,Mail,uv,DatePaiement,DateRetrait,DateRetour,Detail,Termine FROM polar_commandes pc
	INNER JOIN polar_commandes_types pct ON pc.type=pct.ID
	LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
	LEFT JOIN polar_manuels pm ON pm.id=SUBSTRING(pcc.Detail FROM 2 FOR 3)
	WHERE pct.Nom LIKE 'Manuel'$suppl AND pc.DateRetrait IS NOT NULL ORDER BY pc.ID DESC");
	while($data=mysql_fetch_assoc($req)){
		echo '<tr>';
		echo '<td>'.$data['ID'].'</td>';
		echo '<td>'.$data['uv'].'</td>';
		echo '<td>'.$data['Nom'].'</td>';
		echo '<td>'.$data['Prenom'].'</td>';
		echo '<td>'.$data['Mail'].'</td>';
		$parametres = "'".$data['ID']."','".$data['uv']."','".$data['Prenom']." ".$data['Nom']." (".$data['Mail'].")','".$data['Detail']."','".$data['DatePaiement']."','".$data['DateRetrait']."','".$data['DateRetour']."',".$data['Termine'];
		echo '<td><img title="Infos" onclick="infos('.$parametres.');" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></td>';
		echo '</tr>';
	}
?>
	</tbody>
</table>
<div id="dialog" title="Détail de la location">
	<table>
		<tr>
			<th>ID</th>
			<td id="com-id"></td>
		</tr>
		<tr>
			<th>UV</th>
			<td id="com-uv"></td>
		</tr>
		<tr>
			<th>Etudiant</th>
			<td id="com-etudiant"></td>
		</tr>
		<tr>
			<th>Manuel</th>
			<td id="com-manuel"></td>
		<tr>
			<th>Date de paiement</th>
			<td id="com-paiement"></td>
		</tr>
		<tr>
			<th>Date de retrait</th>
			<td id="com-retrait"></td>
		</tr>
		<tr>
			<th>Date de retour</th>
			<td id="com-retour"></td>
		</tr>
		<tr>
			<td colspan="2" id="rboutons"><input type="button" id="restitution" value="Restitution" class="btn" /><!-- <input type="button" id="attestation" value="Attestation chèque" class="btn" />--> <input type="button" id="encaisser" value="Encaisser caution" class="btn" /></form></td>
		</tr>
	</table>
</div>
<?php
require('inc/footer.php');
?>
