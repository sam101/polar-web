<?php
if(isset($_GET['getlogin']) && isset($_GET['param'])){
	$req = query("SELECT `Mail`,`Detail`,`DateRetrait`,`DatePaiement` FROM `polar_commandes`
		LEFT JOIN `polar_commandes_contenu` ON  `polar_commandes_contenu`.`IDCommande`=`polar_commandes`.`ID`
	 	INNER JOIN `polar_commandes_types` ON `polar_commandes`.`Type`=`polar_commandes_types`.`ID`
		WHERE `polar_commandes`.`ID`=".intval($_GET['param'])." AND `polar_commandes_types`.`Nom` LIKE 'Manuel'");
	if(mysql_num_rows($req) == 1){
		$data = mysql_fetch_assoc($req);
		if(empty($data['DatePaiement']))
			$result['erreur'] = "Commande non payee !";
		else if(!empty($data['DateRetrait']))
			$result['erreur'] = "Commande deja retiree !";
		else{
			$result['mail'] = $data['Mail'];
			$result['uv'] = $data['Detail'];
		}
	}
	else
		$result['erreur'] = "Commande inconnue.";

	echo json_encode($result);
} elseif(isset($_GET['checkbon']) && isset($_GET['bon']) && isset($_GET['login'])) {

    $req = query("SELECT Detail,DateRetrait,DatePaiement FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
	 	INNER JOIN polar_commandes_types pct ON pc.Type=pct.ID
		WHERE pc.ID=".intval($_GET['bon'])." AND pct.Nom LIKE 'Manuel'");
    $req2 = query("SELECT Staff FROM polar_utilisateurs WHERE Login='".mysqlSecureText($_GET['login'])."'");

    $isStaff = false;
    if (mysql_num_rows($req2) === 1) {
        $data = mysql_fetch_assoc($req2);
        $isStaff = ($data['Staff'] == 1);
    }

	if(mysql_num_rows($req) == 1){
		$data = mysql_fetch_assoc($req);
		if(empty($data['DatePaiement'])) {
            if ($isStaff) {
                $result['staff'] = 'Permanencier, rien à payer !';
            } else {
                $result['erreur'] = "Commande non payee !";
            }
        } else if(!empty($data['DateRetrait'])) {
			$result['erreur'] = "Commande deja retiree !";
        } else {
			$result['uv'] = $data['Detail'];
		}
	} else {
		$result['erreur'] = "Commande inconnue.";
    }

	echo json_encode($result);

} elseif(isset($_GET['checkmanuel']) && isset($_GET['manuel'])) {
	echo 'true';

} elseif(isset($_GET['checkstaff']) && isset($_GET['login'])) {

    $req = query("SELECT Staff FROM polar_utilisateurs WHERE Login='".mysqlSecureText($_GET['login'])."'");

    $isStaff = false;
    if (mysql_num_rows($req) === 1) {
        $data = mysql_fetch_assoc($req);
        $isStaff = ($data['Staff'] == 1);
    }
    if ($isStaff) {
        $result['staff'] = 'Permanencier, rien à payer !';
    } else {
        $result['staff'] = '';
    }

	echo json_encode($result);

} elseif(isset($_GET['location'])) {
	$param = intval($_POST['ep-param']);
	// On vérifie que la commande est valide
	$req = query("SELECT `polar_commandes`.`DatePaiement` IS NOT NULL FROM `polar_commandes` INNER JOIN `polar_commandes_types`
		WHERE `polar_commandes`.`ID`=$param AND `polar_commandes`.`DateRetrait` IS NULL
	 	AND `polar_commandes`.`Type`=`polar_commandes_types`.`ID` AND `polar_commandes_types`.`Nom` LIKE 'Manuel'");
    $req2 = query("SELECT Staff FROM polar_utilisateurs WHERE Login='".mysqlSecureText($_POST['epp-login'])."'");

    $isStaff = false;
    if (mysql_num_rows($req2) === 1) {
        $data = mysql_fetch_assoc($req2);
        $isStaff = ($data['Staff'] == 1);
    }

	if (mysql_num_rows($req) == 0) {
		ajouterErreur("Commande inconnue ou d&eacute;j&agrave; retir&eacute;e.");
    } else {
        $data = mysql_fetch_row($req);
        if (!($isStaff || ($data[0] == 1))) {
            ajouterErreur("Commande non pay&eacute;e.");
        } else {
            // Lecture du code-barre
            $manuel = $_POST['ep-manuel']; // trop gros pour intval
            $uvManuel = intval(substr($manuel, 1, 3));
            $serieManuel = intval(substr($manuel, 4, 3));
            $idManuel = intval(substr($manuel, 7, 3));

            // On vrifie que le bouquin est bien dispo
            $req = query("SELECT 1 FROM `polar_manuels_flotte` WHERE Manuel=$uvManuel AND Serie=$serieManuel AND Numero=$idManuel AND Etat='stock'");
            if(mysql_num_rows($req) == 0) {
                ajouterErreur("Manuel non disponible. Merci de le mettre de cote et d'en choisir un autre.");
            } else {
                // On marque le bouquin comme loué
                query("UPDATE `polar_manuels_flotte` SET Etat='location' WHERE Manuel=$uvManuel AND Serie=$serieManuel AND Numero=$idManuel AND Etat='stock' LIMIT 1");

                // On ajoute le bouquin  la commande
                query("UPDATE polar_commandes_contenu SET Detail=$manuel WHERE IDCommande=$param");

                $query = "UPDATE `polar_commandes` SET
                    DateRetrait=NOW(),
                    IDRetrait=".intval($_SESSION['con-id']).",
                    Mail='".mysqlSecureText($_POST['ep-mail'])."',
                    Nom='".mysqlSecureText($_POST['ep-nom'])."',
                    Prenom='".mysqlSecureText($_POST['ep-prenom'])."'";

                if ($isStaff) {
                    $query .= ",\nDatePaiement=NOW()";
                }

                $query .= "\nWHERE ID=$param";

                // On marque la commande comme retire en sauvant les coordonnes
                query($query);

                // Mail de confirmation
                $prenom = htmlentities($_POST['ep-prenom'], ENT_COMPAT, 'UTF-8');
                $req = query("SELECT uv FROM `polar_manuels` WHERE id=$uvManuel LIMIT 1");
                $result = mysql_fetch_assoc($req);
                $uv = $result['uv'];
                $message ="Bonjour $prenom,<br /><br />
    Merci d'avoir loué un manuel au Polar !<br />
    <br />
    Pour information, la référence de location de ton manuel de $uv est $param.<br />
    <br />
    <br/>
    Nous te rappelons qu'il est interdit d'écrire dans ton manuel.<br />
    Il devra être rendu à la fin du semestre (généralement la semaine avant les finaux). Reste attentif aux affichages et au weekmail pour ne pas manquer cette date.<br />
    Si nous n'avons pas récupéré ton manuel à la fin de la semaine des finaux, ton chèque de caution de 25 Euros sera encaissé.<br />
    <br />
    A bientôt,<br />
    <br />
    L'équipe du Polar";

                sendMail('polar-manuels@assos.utc.fr', 'Le Polar', array(htmlentities($_POST['ep-mail'])), "[Le Polar] A propos de la location de ton manuel de $uv", $message);

                echo '<script>alert("Location enregistrée !");document.location.href="'.$racine.$module."/".$section.'";</script>';
            }
        }
	}

	if(hasErreurs()){
		saveFormData($_POST);
		header("Location: $racine$module/$section");
	}

}
?>
