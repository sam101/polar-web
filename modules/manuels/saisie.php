<?php
$titrePage = 'Gestion de la location des Manuels de langue';

$req = query("SELECT pct.CodeProduit FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
	WHERE pct.Nom LIKE 'Manuel' AND pct.Actif = 1 AND pca.Actif = 1 AND pca.EnVente = 1");
if(mysql_num_rows($req) > 0){
	$data = mysql_fetch_row($req);
	$codeArticle = $data[0];

    addHeaders('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />');
	addFooter('<script type="text/javascript" src="'.$racine.'js/jquery.bgiframe.min.js"></script>
	   <script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
	   <script type="text/javascript">

	   $(document).ready(function() {
            var logins = Array();

            var completeNomPrenom = function() {
                var login = $("#login").val();
                if (!(login in logins)) {
                    $("#mail").val("");
                    $("#nom").val("");
                    $("#prenom").val("");
                    $("#infos_staff").html("");
                    return false;
                }

                $.ajax({
                    url: "'.$racine.'utils/login_autocomplete",
                    data: "l="+login,
                    async: false,
                    dataType: "json",
                    success: function(data) {
                        if (data.erreur) {
                            alert(data.erreur);
                        } else {
                            $("#mail").val(data.mail);
                            $("#nom").val(data.nom);
                            $("#prenom").val(data.prenom);
                        }
                    }
                });

                $.ajax({
                    url: "'.$racine.'manuels/saisie_control?checkstaff",
                    data: "login="+login,
                    async: false,
                    dataType: "json",
                    success: function(data) {
                        if (data.erreur) {
                            alert(data.erreur);
                            $("#login").val("").focus();
                        } else {
                            $("#infos_staff").html(data.staff);
                        }
                    }
                });

                return true;
            };

			$("#login").autocomplete("'.$racine.'utils/login_autocomplete",{
				matchContains:1,
				mustMatch:false,
				delay: 100,
				formatItem: function(row, i, max) {
                    console.log("req " + i + "/" + max + " - " + row[0]+" - "+row[1]+" "+row[2]);
					return row[0]+" - "+row[1]+" "+row[2];
				},
				formatResult: function(row) {
          logins[row[0]] = "";
					return row[0];
				}
			});

            $("#login").keyup(function() {
                if(completeNomPrenom()) {
                    $("#bon").focus();
                }
            });

            var manuelCode = "";

			$("#formulaire").submit(function(event){
                // si le manuel est saisi on passe au login

                if(($("#manuel").val().length > 0) && (manuelCode !== $("#manuel").val())) {
                    if ($("#manuel").val().length !== 10) {
                        alert("Ce code ne correspond pas à un manuel.");
                        $("#manuel").val("").focus();
                        manuelCode = "";
                    } else {
                        $.ajax({
                            url: "'.$racine.'manuels/saisie_control?checkmanuel",
                            data: "manuel="+$("#manuel").val(),
                            async: false,
                            dataType: "json",
                            success: function(data) {
                                if (data.erreur) {
                                    alert(data.erreur);
                                    $("#manuel").val("").focus();
                                    manuelCode = "";
                                } else {
                                    manuelCode = $("#manuel").val();
                                    $("#login").focus();
                                }
                            }
                        });
                    }
                }

				// On remplace un code barre par un numéro de commande
				if($("#bon").val().length == 10){
					if(parseInt($("#bon").val().substr(0,4), 10) == '.$codeArticle.') // 4 premiers chiffres : code
						$("#bon").val(parseInt($("#bon").val().substr(4,6), 10)); // 6 derniers chiffres : param
					else{
						$("#bon").val("");
						alert("Ce code ne correspond pas à un bon de location.");
					}
				}

				// Si login saisi mais pas de mail, on tente avec @etu et nom et prénom
				if($("#login").val().length > 0 && $("#mail").val().length == 0){
					completeNomPrenom();
				}

                if ($("#manuel").val().length > 0 && $("#mail").val().length == 0) {
                    $("#login").focus();
                }

                // Si mail saisi, on demande le bon
				if($("#mail").val().length > 0){
                    $("#bon").focus();
				}

				// Si tout est là, on valide
				if($("#bon").val().length > 0 &&
                    $("#mail").val().length > 0 &&
                    $("#manuel").val().length > 0 &&
                    $("#nom").val().length > 0 && $("#prenom").val().length > 0) {
					return true;
                }

				return false;
			});

			$("#manuel").focus();
	   });
	   </script>');
	require('inc/header.php');
	?>

	            <h1>Ajouter une location</h1>
				<p style="color:red;"><b>Utiliser la touche Entrée pour valider chaque ligne !</b></p>
	<?php
	afficherErreurs();
	?>
				<form id="formulaire" name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module."/".$section; ?>_control?location">
				<ol>
					<li>Scanner le manuel : <input type="text" id="manuel" name="ep-manuel" autocomplete="off" value="<?=getFormData('ep-manuel')?>" /></li>
          <li>Vérifier l'état du manuel et faire vérifier par l'étudiant.</li>
					<li>Saisir le login de l'étudiant <input type="text" id="login" size="30" name="epp-login" value="<? echo getFormData('epp-login'); ?>" /><span id="infos_staff" style="color: green; font-style:italic ; margin-left: 5px"></span></li>
					<li>Si ce n'est pas automatique, saisir son nom <input type="text" id="nom" name="ep-nom" autocomplete="off" value="<? echo getFormData('ep-nom'); ?>" />, son prénom <input type="text" id="prenom" name="ep-prenom" autocomplete="off" value="<? echo getFormData('ep-prenom'); ?>" /><br />et son mail <input type="text" id="mail" name="ep-mail" autocomplete="off" value="<? echo getFormData('ep-mail'); ?>" /></li>
                    <li>Scanner le bon de commande : <input type="text" id="bon" name="ep-param" autocomplete="off" /></li>
					<li><input type="submit" value="Ajouter la location !" class="btn" /></li>
				</ol>
	            </form>
<?php
	require('inc/footer.php');
} // Fin commande active
else {
	require('inc/header.php');
	echo '<p>Ce secteur est actuellement fermé. Merci de revenir plus tard !</p>';
	require('inc/footer.php');
}
?>


