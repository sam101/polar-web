﻿<?php
$titrePage="Restituer une location";
addFooter('<script>$(document).ready(function(){
	$("#formulaire").submit(function(){
		if($("#idloc").val().length == 10)
			$("#idloc").val(parseInt($("#idloc").val().substr(4,6), 10));
	})
});</script>');
require('inc/header.php');
?>
<h1>Restituer une location</h1>
<?php
if(!empty($_SESSION['retourok'])){
	echo '<p style="color:green;">Restitution de la location '.$_SESSION['retourok'].' enregistr&eacute;e !</p>';
	unset($_SESSION['retourok']);
}
echo afficherErreurs();

?>
<p>Cette page permet d'enregistrer le retour d'un manuel dans les conditions idéales (manuel en bon état et caution en chèque). Pour tous les autres cas, utiliser les boutons du module Locations.</p>
<form id="formulaire" method="get" action="<?php echo $racine.$module.'/'.$section; ?>_control">
	<table>
		<tr>
			<td>Num&eacute;ro de location :</td>
			<td>
				<input type="text" id="idloc" class="autofocus" name="id"/> <small>Il est possible de scanner un bon.</small>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="Restituer" class="btn" />
			</td>
		</tr>
	</table>
</form>
<?php
require('inc/footer.php');
?>


