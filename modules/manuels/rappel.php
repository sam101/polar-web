<?php
$titrePage = "Mail de rappel manuels";
require("inc/header.php");
?>
<h1>Relancer par mail</h1>
<p>Cette page permet d'envoyer le mail de relance aux personnes ayant loué un manuel.</p>
<form method="post" action="<?php echo "$racine$module/${section}_control"; ?>">
Sujet : <input type="text" size="50" maxlength="50" name="sujet_mail" value="[Le Polar] Restitution de ton manuel de langue"><br />
<textarea rows="8" cols="80" name="text_mail">Bonjour,

La fin du semestre approche et il est temps pour toi de restituer ton manuel de langue.

Pour cela, passe au Polar courant de semaine prochaine (entre le 4 et le 8 janvier 2010). Tu pourras nous rendre ton manuel et ainsi récupérer ta caution.

Attention, si tu ne viens pas rendre ton manuel la semaine prochaine, nous encaisserons ta caution !

Ton identifiant de location est le %id%.

Tu devras nous l'indiquer à la restitution !

Cordialement,
L'équipe du Polar
</textarea><br />
<p><input type="submit" value="Envoyer le courriel" /></p>
</form>
<?php
require("inc/footer.php");
?>
