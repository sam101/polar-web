<?php
$titrePage="Rechercher une location";
require('inc/header.php');
?>
<h1>Rechercher une location</h1>
<?php
if(!empty($_SESSION['nblocation'])){
	echo '<p>La location correspondant à ce manuel est la location n°'.$_SESSION['nblocation'].' !
	<br /><b>&Eacute;tat du manuel :</b> '.$_SESSION['manuel_etat'].'.</p>';
	unset($_SESSION['nblocation']);
	unset($_SESSION['manuel_etat']);

	echo '<p><input type="button" id="idloc" class="btn" onclick="location.href=\''.$racine.$module.'/'.$section.'\';" value="Nouvelle recherche" /></p>';
}
else {
?>
<p>Cette section permet de retrouver le numéro d'une location à partir du code-barres d'un manuel.</p>
<?php echo afficherErreurs(); ?>
<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
	<table>
		<tr>
			<td>Num&eacute;ro du manuel :</td>
			<td>
				<input type="text" id="idloc" class="autofocus" name="manuel" /> <small>Scanner le code-barre du manuel.</small>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="Rechercher" class="btn" />
			</td>
		</tr>
	</table>
</form>
<?php
}
require('inc/footer.php');
?>
