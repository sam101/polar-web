<?php
require('inc/header.php');

$prixreq = query("SELECT PrixVente FROM polar_caisse_articles pca
	INNER JOIN polar_commandes_types pct ON pct.CodeProduit = pca.CodeCaisse
	WHERE pct.Nom = 'Manuel' AND pca.Actif = 1 AND pca.EnVente = 1
	LIMIT 1");

if(mysql_num_rows($prixreq) > 0){
	$prix = mysql_fetch_assoc($prixreq);
	$somme = "Contre la somme de <b>".$prix['PrixVente']."€ pour le semestre</b>, nous";
}
else
	$somme = "Nous";
?>
<h1>Présentation du service</h1>
<p>Dans sa noble mission qu'est d'aider l'étudiant, le Polar te propose un service pour toi financièrement intéressant.</p>
<p>En effet, tu en as marre, à chaque fois que tu fais une UV de langue, d'avoir à acheter au prix fort un livre de langue qui ne va servir qu'un semestre pour ensuite trainer négligemment sous une armoire dans ton 9m²...</p>
<p>Le Polar a pensé à toi. <?php echo $somme; ?> te prêtons un exemplaire du manuel de ton UV. Attention, tu devras déposer un chèque de caution de 25€ qui sera encaissé si tu ne ramènes pas ton manuel en fin de semestre ou si tu le ramènes dans un état dégradé...</p>
<p></p>
<p>La location d'un manuel au Polar implique l'acceptation des <a href="<?php echo $racine; ?>upload/manuels/CGL_Manuels.pdf" title="CGL Manuels">Conditions Générales de Location des manuels de langue</a>.</p>
<p></p>
<h1>Les manuels disponibles</h1>
<?php
if(mysql_num_rows($prixreq) > 0){
	$req=query("SELECT polar_manuels.ID IDManuel, uv, photo, (SELECT COUNT(*) FROM polar_manuels_flotte WHERE Manuel=IDManuel AND etat LIKE 'stock') AS EnStock
	FROM polar_manuels_flotte
	RIGHT JOIN polar_manuels ON polar_manuels_flotte.Manuel=polar_manuels.ID
	GROUP BY polar_manuels.ID
	ORDER BY uv ASC");

	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<tr><th>UV</th><th>Manuel</th><th>Exemplaires en stock</th></tr>';
	while($data=mysql_fetch_assoc($req)){
		echo '<tr><td>'.$data['uv'].'</td><td>';
		if(is_file('upload/manuels/manuels_'.$data['photo']))
			echo '<a href="',$racine,'upload/manuels/manuels_'.$data['photo'],'" title="Voir l\'image agrandie."><img style="width:100px; height:128px;" src="',$racine,'upload/manuels/manuels_'.$data['photo'],'" alt="" /></a>';
		else
			echo '<img src="',$racine,'styles/0/icones/inconnu.jpg" style="width:100px; height:128px;" alt="" />';
		echo '</td><td>'.$data['EnStock'].'</td></tr>';
	}
	echo '</table>';
}
else
	echo "<p>Désolé, mais ce secteur est actuellement fermé.</p>";

require('inc/footer.php');
?>
