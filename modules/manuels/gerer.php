<?php
$titrePage = 'Gestion de la location des Manuels de langue';

use_jquery_ui();
addFooter('
<script>
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
		$("#dialog-bons").dialog({ width: 700, autoOpen: false });
		$("#open").click(function(){ $("#dialog").dialog("open"); });
		$("#open-bons").click(function(){ $("#dialog-bons").dialog("open"); });
	});
</script>');
require('inc/header.php');
?>
<h1>Les manuels disponibles</h1>
<input type="hidden" value="Ajouter un manuel !" class="btn" id="open" />
<input type="button" value="Imprimer des bons !" class="btn" id="open-bons" />
<p><b>Rappel :</b> Imprimer des étiquettes génère un PDF contenant 975 étiquettes au format Avery J8651. Imprimez seulement le nombre de pages dont vous avez besoin. Le numéro de série augmente après inventaire : la série 4 sera générée après qu'au moins un manuel de la série 3 ait été entré dans l'inventaire.</p>
<?php
$req = query("SELECT polar_manuels.ID IDManuel, uv, photo,
	(CASE WHEN CD = 1 THEN 'Oui' ELSE 'Non' END) AS CD,
	(CASE WHEN Actif = 1 THEN 'Oui' ELSE 'Non' END) AS Actif,
 	(SELECT COUNT(*) FROM polar_manuels_flotte WHERE Manuel=IDManuel AND etat LIKE 'stock') as EnStock,
	(SELECT COUNT(*) FROM polar_manuels_flotte WHERE Manuel=IDManuel AND etat LIKE 'rendu') as Rendu,
	(SELECT COUNT(*) FROM polar_manuels_flotte WHERE Manuel=IDManuel AND etat LIKE 'perdu') as Perdu,
	(SELECT COUNT(*) FROM polar_manuels_flotte WHERE Manuel=IDManuel AND etat LIKE 'location') as Location
	FROM polar_manuels_flotte RIGHT JOIN polar_manuels
	ON polar_manuels_flotte.Manuel = polar_manuels.ID
	GROUP BY polar_manuels.ID
	ORDER BY uv ASC");
echo '<table class="datatables table table-bordered table-striped table-condensed">';
echo '<tr><th>UV</th><th>ID</th><th>Inventaire</th><th>Actif</th><th>CD</th><th>Photo</th><th>Supprimer</th></tr>';
	while($data=mysql_fetch_assoc($req)){
	echo '<tr>';
	echo '<td>'.$data['uv'].'</td>';
	echo '<td>'.$data['IDManuel'].'</td>';
	echo '<td>
	<form name="formulaire" method="post" action="'.$racine.$module.'/'.$section.'_control?imprimer">
	<input type="hidden" name="id" value="'.$data['IDManuel'].'"/>
	<input type="submit" onclick="alert(\'Attention : choisir Mise à l\\\'échelle : Aucune lors de l\\\'impression\')" value="Imprimer étiquettes" class="btn" />
	</form>
	<p>'.$data['Location'].' exemplaires loués<br />
	'.$data['Rendu'].' exemplaires rendus*<br />
	'.$data['EnStock'].' exemplaires en stock<br />
	'.$data['Perdu'].' exemplaires perdus</p>
	</td>';
	echo '<td>'.$data['Actif'].'</td>';
	echo '<td>'.$data['CD'].'</td>';
	echo '<td>';
	if(is_file('upload/manuels/manuels_'.$data['photo']))
		echo '<a href="',$racine,'upload/manuels/manuels_'.$data['photo'],'" title="Voir l\'image agrandie."><img style="width:100px; height:128px;" src="',$racine,'upload/manuels/manuels_'.$data['photo'],'" alt="" /></a>';
	else
		echo '<img style="width:100px; height:128px;" src="',$racine,'styles/0/icones/inconnu.jpg" alt="" />';
	echo '<td><img title="Supprimez ce manuel !" onclick="if(confirm(\'Voulez-vous vraiment SUPPRIMER définitivement ce manuel, ainsi que tous les stocks qui lui sont attachés ?\')) location.href= \'',$racine.$module.'/'.$section.'_control?supprimer&id=',$data['IDManuel'],'\';" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/croix.png" alt="-" /></td>';
	echo '</tr>';
	}
echo '</table>';
echo '<p>* : locations clôturées, mais manuels pas encore inventoriés.';
?>
<div id="dialog" title="Ajouter un manuel">
<form name="formulaire" method="post" style="margin-top:10px;" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control?ajouter'; ?>">
     <table>
       <tr>
         <td style="font-size:12px;">UV :</td>
         <td><input type="text" class="in-text" name="ep-uv" /></td>
       </tr>
       <tr>
         <td style="font-size:12px;">Photo :</td>
         <td><input type="file" name="ep-fichier" /></td>
       </tr>
       <tr>
         <td colspan="2" style="text-align:right;"><input type="submit" value="Ajouter le manuel !" class="btn" /></td>
       </tr>
     </table>
   </form>
</div>
<div id="dialog-bons" title="Imprimer des bons de location">
<form name="formulaire" method="post" style="margin-top:10px;" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control?bons'; ?>">
     <table>
       <tr>
         <td style="font-size:12px;">Nombre de bons :</td>
         <td><input type="text" class="in-text" name="pages" value="100" /></td>
       </tr>
       <tr>
         <td style="font-size:12px;">Semestre :</td>
         <td><input type="text" class="in-text" name="semestre" value="A10" /> <small>3 caractères !</small></td>
       </tr>
       <tr>
         <td colspan="2" style="text-align:right;"><input type="submit" value="Générer" class="btn" /> <small>ATTENTION : les bons ne peuvent être générés qu'une seule fois ! Ne pas recharger la page !</small></td>
       </tr>
     </table>
   </form>
</div>
<?php
require('inc/footer.php');
?>
