<?php
if(isset($_GET['attestation'])){
	die("<b>Une erreur de sécurité s'est produite.</b>");
	$location=intval($_GET['id']);
	$cheque=intval($_GET['cheque']);
	$req='SELECT count(*) AS nbr FROM polar_caisse_cheques_manuels WHERE Location=\''.$location.'\'';
	$sql=query($req);
	$donnees=mysql_fetch_array($sql);
	$sql='SELECT * FROM polar_commandes WHERE id='.$location;
	$req_etu=query($sql);
	$data_etu=mysql_fetch_array($req_etu);
	if($donnees['nbr']==0)
	{
		$sql='INSERT INTO polar_caisse_cheques_manuels (Client,Date,Location,Montant,Detail,Cheque) VALUES(
			\''.$data_etu['Prenom'].' '.$data_etu['Nom'].'\',
			NOW(),'.$location.',25,\'Remboursement de la caution versée en espèces ou en CB.\','.$cheque.')';
		$req=query($sql);
		$sql='INSERT INTO polar_caisse_cheques (NumVente, Date, Numero, Banque, Montant, Emetteur, Ordre, PEC) VALUES(
			-1,NOW(),'.$cheque.',\'Societe Generale\',25,\'Le Polar\',\''.$data_etu['Prenom'].' '.$data_etu['Nom'].'\',1)';
		$req=query($sql);
	}
	$sql='SELECT * FROM polar_caisse_cheques_manuels WHERE Location='.$location;
	$req=query($sql);
	$donnees=mysql_fetch_array($req);

	$pdf = new PolarPDF("ATTESTATION");

	$pdf->SetFontSize(12);
	$output = '<p align="right">Compiègne, le '.date("d/m/Y").'</p>';

	$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	if(strlen($donnees['Client'])>1)
		$html = '<p>'.$donnees['Client'];
	else
		$html = '<p>___________________________________';

	$html .= ' déclare avoir reçu le chèque suivant :</p>
	<ul>
		<li>Banque : Société Générale</li>
		<li>Numéro : '.$donnees['Cheque'].'</li>
		<li>Montant : '.formatPrix($donnees['Montant']).'€</li>
	</ul>
	correspondant à la location n°'.$donnees['Location'].'</p>';

	$html .= '<p>Détails :<br />'.unhtmlentities($donnees['Motif']).'</p>';
	$html .= 'Imprimé le '.date("d/m/Y").' en deux exemplaires originaux.';

	$pdf->writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
	$pdf->Ln(10);

	$pdf->Signature("prez", 'Le bénéficaire,<br />'.$donnees['Client']);
	$file = 'Documents/Remboursement_caution_'.$donnees['Client'].'_'.$donnees['id'].'.pdf';
	$pdf->Output($file, 'F');
	header('Location: '.$racine.$file);
}
if(isset($_GET['Encaisser'])){
	$location = intval($_GET['Encaisser']);
	$manuelrendu = intval($_GET['manuelrendu']);
	$banque = mysqlSecureText($_GET['banque']);
	$perm = intval($_SESSION['con-id']);

	// Infos sur la location
	$req = query("SELECT Mail, Prenom, Nom FROM polar_commandes
		WHERE polar_commandes.ID=$location");
	$data = mysql_fetch_assoc($req);
	$mail = $data['Mail'];
	$prenom = $data['Prenom'];
	$nom = $data['Nom'];

	// On ajoute le chèque
	query("INSERT INTO polar_caisse_cheques (Date, Banque, Montant, Emetteur, Ordre, PEC) VALUES (
		NOW(),
		'$banque',
		25,
		'$prenom $nom',
		'Le Polar',
		1)
	");

	// Clôture de la location
	query("UPDATE polar_commandes SET Termine=1 WHERE ID=$location LIMIT 1");

	// Livre perdu
	query("UPDATE polar_manuels_flotte SET etat='perdu'
		WHERE Manuel=SUBSTRING((SELECT Detail FROM polar_commandes_contenu WHERE IDCommande=$location) FROM 2 FOR 3)
		AND Serie=SUBSTRING((SELECT Detail FROM polar_commandes_contenu WHERE IDCommande=$location) FROM 5 FOR 3)
		AND Numero=SUBSTRING((SELECT Detail FROM polar_commandes_contenu WHERE IDCommande=$location) FROM 8 FOR 3)
		LIMIT 1");

  // Ajout dans le journal des ventes
  // on cherche l'article 'Encaissement caution manuel', si il n'existe pas on le crée
  $req1 = query("SELECT CodeCaisse FROM polar_caisse_articles WHERE Nom LIKE 'Encaissement caution manuel'");
  if (mysql_num_rows($req1) ==  0) {
    $codeArticle = getPremierDisponible();
    $codeArticle = $codeArticle[1];
    query("INSERT INTO polar_caisse_articles
          (CodeCaisse, EnVente, Actif, Nom, Secteur, PrixAchat, PrixVente, PrixVenteAsso,
           TVA, Palier1, Remise1, Palier2, Remise2, CodeJS, Photo, Date)
           VALUES ($code, 1, 1, 'Encaissement caution manuel', 1, 25,
                   25, 0, 0, 0, 0, 0, 0, '', '', NOW())");
  } else {
    $data1 = mysql_fetch_assoc($req1);
    $codeArticle = $data1['CodeCaisse'];
  }

  $reqArticle = query("SELECT ID, TVA, PrixVente FROM polar_caisse_articles WHERE CodeCaisse = $codeArticle");
  $article = mysql_fetch_assoc($reqArticle);
  $idArticle = $article['ID'];
  $prix = (float) $article['PrixVente'];
  $tva = ($prix * (float) $article['TVA']) / (1 + (float) $article['TVA']);

	$permanencier = intval($_SESSION['con-id']);
  $iDVente = genererIDVente();

  query("INSERT INTO polar_caisse_ventes_global
         (IDVente, Article, Quantite, Date, Finalise, Asso, Client, Facture, PrixFacture,
         MontantTVA, Tarif, MoyenPaiement, Permanencier)
         VALUES ($iDVente, $idArticle, 1, NOW(), 1, NULL, '', 0, $prix, $tva, 'normal', 'cheque', $permanencier)");

	// Mail pour prévenir
	$message = 'Bonjour,<br /><br />
Ton manuel n\'ayant pas été restitué, nous t\'informons de l\'encaissement de ta caution de 25€.<br /><br />
Cordialement,<br />L\'équipe du Polar';

	sendMail('polar-manuels@assos.utc.fr', 'Le Polar - Manuels', array($mail), '[Le Polar] A propos de la location de ton manuel de langue', $message);

	header("Location: $racine$module/$section");
}
?>
