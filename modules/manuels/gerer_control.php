<?php

if(isset($_GET['ajouter'])){
	die("Not implemented.");
	// Sécurisation des données entrantes
	$uv = mysqlSecureText($_POST['ep-uv']);
	$stock = intval($_POST['ep-stock']);

	if(get_magic_quotes_gpc() == 1)
        $uv = stripslashes($uv);

	if(!empty($_FILES['ep-fichier']['tmp_name']) && is_file($_FILES['ep-fichier']['tmp_name'])){
        $morceaux = explode('.', $_FILES['ep-fichier']['name']);
        $extension = $morceaux[sizeof($morceaux) - 1];
        $nom_fichier = '';

        //On vérifie si l'extension est autorisée
        if(!($extension=='gif' || $extension=='jpg' || $extension=='jpeg' || $extension=='png')){
         	$_SESSION['ep-erreur'] = 'Ce type de fichier n\'est pas accepté (gif, jpg ou png).';
        	header("Location: $racine$module/$section");
        	die();
        }

        if(exif_imagetype($_FILES['ep-fichier']['tmp_name']) != IMAGETYPE_GIF && exif_imagetype($_FILES['ep-fichier']['tmp_name']) != IMAGETYPE_JPEG && exif_imagetype($_FILES['ep-fichier']['tmp_name']) != IMAGETYPE_PNG && exif_imagetype($_FILES['ep-fichier']['tmp_name']) != IMAGETYPE_JPG) {
        	$_SESSION['ep-erreur'] = 'Ce type de fichier n\'est pas accepté.';
        	header("Location: $racine$module/$section");
        	die();
        }

        $i = '';
        while(is_file('upload/manuels/manuels_'.$uv.'.'.$extension)) {
        	if($i == '')
        		$i = 0;
        	else
        		$i++;
        }

        $nom_fichier = $uv.'.'.$extension;

        if(move_uploaded_file($_FILES['ep-fichier']['tmp_name'], 'upload/manuels/manuels_'.$nom_fichier)){
        	$sql = "INSERT INTO `polar_manuels` (`UV`, `photo`) VALUES('$uv','$nom_fichier')";

	        if(mysql_query($sql))
	        	echo "<script>alert('Manuel ajouté avec succès !');document.location.href='$racine$module/$section';</script>";
	        else {
				unlink('upload/manuels/manuels_'.$nom_fichier);
				$_SESSION['ep-erreur'] = 'La requête a échoué pour une raison inconnue. Merci d\'en informer le webmaster.';
	        	header("Location: $racine$module/$section");
			}
		}
		else {
			$_SESSION['ep-erreur'] = 'Le chargement du fichier a échoué pour une raison inconnue.';
        	header("Location: $racine$module/$section");
		}
	}
	else {
		$_SESSION['ep-fichier-erreur'] = 'Vous n\'avez pas envoyé de fichier ou celui-ci n\'existe pas.';
       	header("Location: $racine$module/$section");
	}
} // Fin ajout de manuel
else if(isset($_GET['supprimer'])){
	$manuel=intval($_GET['id']);

	// Suppression de la photo
	$req = query("SELECT `UV`,`photo` FROM `polar_manuels` WHERE `id`=$manuel");
	$data = mysql_fetch_assoc($req);
	if(mysql_num_rows($req) == 1){
		unlink('upload/manuels_'.$data['photo']);

		// Suppression de l'inventaire
		query("DELETE FROM `polar_manuels_flotte` WHERE `Manuel`=$manuel");

		// Suppression du manuel
		query("DELETE FROM `polar_manuels` WHERE `id`=$manuel");
	}
	header("Location: $racine$module/$section");
} // Fin d'ajout d'un manuel
else if(isset($_GET['bons'])){
	$nbpage = intval($_POST['pages']);
	$semestre = mysqlSecureText($_POST['semestre']);

	// Réucpération de la liste des manuels
	$req = query("SELECT uv, CD FROM polar_manuels WHERE Actif = 1 ORDER BY uv ASC");
	$manuels = array();
	while($res = mysql_fetch_assoc($req)){
        $manuels[] = array('uv' => $res['uv'],
                           'cd' => $res['CD'] == 1);
	}

	require('lib/tcpdf/tcpdf.php');
	require('lib/fpdi/fpdi.php'); // Pour l'import d'un fichier pdf existant

	// Create a PDF object and set up the properties
	$pdf = new FPDI("p", "mm", "a4");
	$pdf->SetAuthor("Le Polar");
	$pdf->SetTitle("Bon de location");

	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// Set line drawing defaults
	$pdf->SetDrawColor(224);
	$pdf->SetLineWidth(1);

	// On charge le bon de location à partir du pdf
	$pdf->setSourceFile("upload/manuels/bon_location.pdf");
	$bon = $pdf->ImportPage(1);

	for($i=0;$i<$nbpage;$i++){
		// Add new page & use the base PDF as template
		$pdf->AddPage();
		$pdf->useTemplate($bon);

		// Ajout du semestre
        $pdf->SetFont('Helvetica', 'B', 26);

		$pdf->Text(163, 25, $semestre);

		// Ajout des manuels en location
        $pdf->SetFont('Helvetica', '', 16);
        $row = 0;
        $column = 0;

        $startColumn = 83;
        $startRow = 135.6;
        $spaceXCellText = 9.5;
        $spaceYCellText = 1;
        $columnSpace = 32.2;
        $rowSpace = 15.55;

		for($j=0;$j<count($manuels);){

            $x = $startColumn + ($column * $columnSpace);
            $y = $startRow + ($row * $rowSpace);
			$pdf->SetXY($x, $y);
            $pdf->Cell(9, 9, '', array('LTRB' => array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0))));
            $x += $spaceXCellText;
            $y += $spaceYCellText;
            if ($manuels[$j]['cd'])
                $pdf->SetFont('', 'B');
            $pdf->Text($x, $y, $manuels[$j]['uv']);
            $pdf->SetFont('');

            if ((++$j % 4) == 0) {
                $row++;
                $column = 0;
            } else {
                $column++;
            }
		}

		// On ajoute la commande dans la base de données
		query("INSERT INTO polar_commandes (Type,IPCommande,DateCommande) VALUES ((SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Manuel'),'127.0.0.1',NOW())");
		$id = mysql_insert_id();
		query("INSERT INTO polar_commandes_contenu (IDCommande) VALUES ($id)");

		// Code-barre
		$style = array(
			'text' => true,
			'stretchtext' => 4,
			'font' => 'helvetica',
			'fontsize' => 8
		);
		$pdf->write1DBarcode(sprintf("%010s", 130000000+$id), "C128C", 155, 7, 180, 12, .4, $style);
	}
	$pdf->Output();
	$pdf->closeParsers();
} // Fin d'impression des bons de location
else if(isset($_GET['imprimer'])){
	if(!isset($_POST['id'])){
		$titrePage = 'Manuels';
		require('inc/header.php');
		echo '<h1>Imprimer des étiquettes de manuels</h1><p>ID du manuel manquant.</p>';
		require('inc/footer.php');
	}
	else {
		$id = intval($_POST['id']);

		// On cherche la plus grande serie
		$sql="SELECT MAX(Serie) FROM polar_manuels_flotte WHERE Manuel=$id;";
		$req=query($sql);

		if(mysql_num_rows($req) == 0)
			$serie = 1;
		else {
			$data = mysql_fetch_array($req);
			$serie = $data[0]+1;
		}

		// On espère que ça arrivera pas trop souvent
		if($serie > 999)
			die('Max 999 séries par manuel. Créez un nouveau manuel.');

		$sql='SELECT uv FROM polar_manuels WHERE id='.$id;
		$req=query($sql);
		$data=mysql_fetch_array($req);
		$intitule_uv=$data[0];

		require('lib/tcpdf/tcpdf.php'); // Must include this

		// Create a PDF object and set up the properties
		$pdf = new TCPDF("p", "mm", "a4");
		$pdf->SetAuthor("Le Polar");
		$pdf->SetFontSize(10);
		$pdf->SetTitle("Planche de code-barres");
		$pdf->SetMargins(0, 0, 0, 0);
		$pdf->SetAutoPageBreak(false);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// 90AABBBCCC où AA=uv, BBB=série et CCC=id
		for($page=0;$page<15;$page++){ // On fait 15 pages
			$pdf->AddPage();

			// Étiquettes
			for($i=0;$i<13;$i++){
				for($j=0;$j<5;$j++){
					// Case
					$w = 36;
					$h = 21.1;
					$x = $j*($w+5)+5; // 5mm de marge entre chaque étiquette, 5mm de marge à gauche
					$y = $i*$h+12.2; // 12mm de marge en haut

					$pdf->SetXY($x, $y);
					$pdf->Cell($w, 4, "Ce manuel est la", 0, 0, "C");
					$pdf->SetXY($x, $y+4);
					$pdf->Cell($w, 4, "propriété du Polar.", 0, 0, "C");

					// Code barre
					$xcode = $x + 1;
					$ycode = $y + 9;
					$wcode = 33;
					$hcode = 11;

					$style = array(
						'text' => true,
						'stretchtext' => 4,
						'font' => 'helvetica',
						'fontsize' => 8
					);

					$code = 9e9 + $id*1e6 + $serie*1e3 + $page*65+$i*5+$j+1;
					$pdf->write1DBarcode(sprintf("%010s", $code), "C128C", $xcode, $ycode, $wcode, $hcode, .4, $style);
				}
			}

			// UV et numéro de série
			$pdf->SetXY(40, 21*13+15);
			$pdf->Cell(20, 4, $intitule_uv, 0, 0, "C");
			$pdf->SetXY(130, 21*13+15);
			$pdf->Cell(20, 4, "Série ".$serie, 0, 0, "C");
		}

		$pdf->Output();
		$pdf->closeParsers();

	}
} // Fin de l'impression d'étiquettes
?>
