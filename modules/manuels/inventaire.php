<?php
$titrePage = 'Inventaire des Manuels de langue';
require('inc/header.php');
?>
<h1>Inventaire des manuels</h1>
<p>Saisir un manuel ici le notera comme <i>en stock</i>, ce qui confirme sa présence au Polar. Les manuels jamais vus sont automatiquement ajoutés dans la base. Il n'y a pas de risque à scanner un manuel déjà dans le stock.</p>
<?php
// Si le formulaire est rempli
if(!empty($_POST['ep-manuel']) && is_numeric($_POST['ep-manuel'])){
	// Lecture du code-barre
	$manuel = $_POST['ep-manuel']; // trop gros pour intval
	$uvManuel = intval(substr($manuel, 1, 3));
	$serieManuel = intval(substr($manuel, 4, 3));
	$idManuel = intval(substr($manuel, 7, 3));

	// Le code barre est bon ?
	if(intval(substr($manuel, 0, 1)) != 9 || $uvManuel == 0 || $serieManuel == 0 || $idManuel == 0)
		echo '<p style="color:red;">Erreur : code incorrect.</p>';
	else {
		// Récup du nom en clair de l'UV
		$sql='SELECT uv FROM polar_manuels WHERE id='.$uvManuel;
		$req=query($sql);
		$data=mysql_fetch_array($req);
		$intitule_uv=$data[0];

		if(empty($intitule_uv))
			echo '<p style="color:red;">Erreur : ce code-barre ne correspond à aucune UV enregistrée.</p>';
		else {
			// Recherche du manuel
			$sql="SELECT Etat FROM polar_manuels_flotte WHERE Manuel=$uvManuel AND Serie=$serieManuel AND Numero=$idManuel;";
			$req=query($sql);

			// Si le manuel existe déjà
			if(mysql_num_rows($req)){
				$data=mysql_fetch_array($req);
				$etat=$data[0];

				if($etat == 'stock') {// Si le manuel est déjà en stock
                    Log::info("Manuel deja en stock : ".$manuel);
					echo "<p style=\"color:red;\">Erreur : le manuel est déjà dans le stock (UV=$intitule_uv, Serie=$serieManuel, Ref=$idManuel).</p>";
                }
				else { // Sinon : on le met
                    $commande = mysql_fetch_assoc(query("SELECT pc.ID, pc.DateRetour FROM polar_commandes pc JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID WHERE pcc.Detail LIKE '".mysqlSecureText($manuel)."'"));
                    if (is_null($commande['DateRetour'])) {
                        Log::info("Manuel mis en stock et commande finie : ".$manuel);
                        $sql= 'UPDATE polar_commandes SET DateRetour = NOW(), IDRetour = '.$_SESSION['con-id'].', Termine = 1 WHERE ID = '.$commande['ID'].';';
                        echo "<p style=\"color:green;\">Manuel remis en stock (UV=$intitule_uv, Serie=$serieManuel, Ref=$idManuel) et commande terminée.</p>";
                    } else {
                        Log::info("Manuel mis en stock : ".$manuel);
                        $sql='';
                        echo "<p style=\"color:green;\">Manuel remis en stock (UV=$intitule_uv, Serie=$serieManuel, Ref=$idManuel).</p>";
                    }

					$sql.="UPDATE polar_manuels_flotte SET Etat='stock' WHERE Manuel=$uvManuel AND Serie=$serieManuel AND Numero=$idManuel;";
					$req=query($sql);

					echo '<embed src="'.$racine.'/upload/pop.mp3" autostart="true" hidden="true" loop="false">';
				}
			}
			else { // Si le manuel n'existe pas encore, on l'ajoute
				$sql="INSERT INTO polar_manuels_flotte (Manuel,Serie,Numero,Etat) VALUES ($uvManuel,$serieManuel,$idManuel,'stock');";
				$req=query($sql) or die("Erreur sql insert.");
				echo "<p style=\"color:green;\">Manuel ajouté (UV=$intitule_uv, Serie=$serieManuel, Ref=$idManuel).</p>";
				echo '<embed src="'.$racine.'/upload/pop.mp3" autostart="true" hidden="true" loop="false">';
			}
		}
	}
}
?>
			<form id="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module."/".$section; ?>">
              <table>
                <tr>
                  <td style="font-size:12px;">Manuel :</td>
				<td><input type="text" id="manuel" class="autofocus" name="ep-manuel"/></td>
                </tr>
				<tr>
                  <td colspan="2" style="text-align:right;"><input type="submit" autocomplete="off" value="Compter !" class="btn" /></td>
                </tr>
              </table>
            </form>
<?php
require('inc/footer.php');
?>


