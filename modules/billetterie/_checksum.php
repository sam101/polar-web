<?php
function checksum($in){
	$out = "";
	for($i=0;$i<strlen($in);$i++)
		$out .= chr(((ord($in[$i]) + $i*7 + $in) % 26) + 65);

	return strtoupper(substr($out, 0, 3));
}

function parseCoords($in){
	$coords = explode("\n", $in);
	$coordsOut = array();
	foreach($coords as $coord)
		$coordsOut[] = explode(",", trim($coord));
	return $coordsOut;
}
?>
