<?php
$titrePage = "Listing";
use_datatables();
require("inc/header.php");

if(isset($_GET['Event'])){
	$event = intval($_GET['Event']);
?>
<h1>Listing</h1>
<p><a href="<?php echo urlControl('export='.$event); ?>">Export Comet</a></p>
<p><a href="<?php echo urlControl('export2='.$event); ?>">Export entrées</a></p>
<p><a href="<?php echo urlControl('exportJson='.$event); ?>">Export Json</a></p>
<table class="datatables table table-bordered table-striped">
	<thead>
		<tr><th>Code barre</th><th>Nom</th><th>Prénom</th><th>Mail</th><th>Tarif</th><th>Commande</th><th>Paiement</th><th>Entrée</th><th>Supprimer</th></tr>
	</thead>
	<tbody>
<?php
$req = query("SELECT pc.ID,DateCommande,DatePaiement,DateRetrait,pct.CodeProduit, pc.Nom, pc.Prenom, pc.Mail, pca.PrixVente AS PrixTTC, pbt.Intitule AS Tarif
	FROM polar_commandes pc
	INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
	INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
	INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
	INNER JOIN (SELECT CodeCaisse, MAX(ID) AS Max FROM polar_caisse_articles GROUP BY CodeCaisse) pcam ON pcam.CodeCaisse = pct.CodeProduit
	INNER JOIN polar_caisse_articles pca ON pca.ID = pcam.Max
	INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
 	WHERE pbo.User = $conid AND pb.ID = $event AND Termine = 0
	GROUP BY pc.ID
	ORDER BY pc.ID DESC");
	while($data = mysql_fetch_assoc($req)){
		echo '<tr>';
		echo '<td>'.sprintf("%010s", $data['CodeProduit']*1e6+$data['ID']).'</td>';
		echo '<td>'.$data['Nom'].'</td>';
		echo '<td>'.$data['Prenom'].'</td>';
		echo '<td>'.$data['Mail'].'</td>';
		echo '<td>'.$data['Tarif'].' ('.$data['PrixTTC'].'€)</td>';
		echo '<td>'.$data['DateCommande'].'</td>';
		echo '<td>';
		if(empty($data['DatePaiement']))
			echo '<a href="'.$racine.$module.'/'.$section.'_control?PayNow='.$data['ID'].'" onclick="return confirm(\'Ceci marquera la place '.$data['ID'].' comme payée. Cette action ne peut être annulée. Continuer ?\');"><img title="Marquer comme payée" src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></a>';
		else
			echo $data['DatePaiement'];
		echo '</td>';
		echo '<td>'.$data['DateRetrait'].'</td>';
		echo '<td><a href="'.$racine.$module.'/'.$section.'_control?Supprimer='.$data['ID'].'" onclick="return confirm(\'Ceci supprimera définitivement la place '.$data['ID'].'. Continuer ?\');"><img title="Supprimer" src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="-" /></a></td>';
		echo '</tr>';
	}
?>
	</tbody>
</table>
<?php
}
else {
	$req = query("SELECT ID, Titre, Date
		FROM polar_billetterie pb
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		WHERE pbo.User = $conid");
	echo '<h1>Billetterie</h1>';
	echo '<ul>';
	while($event = mysql_fetch_assoc($req)){
		echo '<li><a href="'.$racine.$module.'/'.$section.'?Event='.$event['ID'].'" title="">'.$event['Titre'].'</a> du '.$event['Date'].'</li>';
	}
	echo '</ul>';
}
require('inc/footer.php');
?>
