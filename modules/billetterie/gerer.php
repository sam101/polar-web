<?php
$titrePage = 'Gestion de la billetterie';
require("_checksum.php");

if(isset($_GET['add'])) {
	$id = intval($_GET['add']);

	if(BilletterieOrganisateur::userIsOrganisateur($id, $user)) {
		require("inc/header.php");
        $tarifs = TarifBilletterie::getForBilletterie($id);
?>
<h1>Ajouter une inscription (Aighttt !)</h1>
<p>Cette fonctionnalité permet d'inscrire quelqu'un à un évènement en outrepassant toutes les règles en vigueur dans ce monde.</p>
<?php afficherErreurs(); ?>
<form method="post" action="<?php echo urlControl('add='.$id) ?>" id="frm">
	<table>
		<tr>
			<td>Tarif :</td>
			<td>
				<select name="tarif" id="tarifs">
                  <?php foreach($tarifs as $tarif): ?>
                  <option value="<?php echo $tarif->get_id() ?>"><?php echo $tarif->Intitule ?></option>
                  <?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td>
				<input type="text" class="in-texte" name="nom" />
			</td>
		</tr>
		<tr>
			<td>Prénom :</td>
			<td>
				<input type="text" class="in-texte" name="prenom" />
			</td>
		</tr>
		<tr>
			<td>Adresse mail :</td>
			<td>
				<input type="text" id="ep-mail" class="in-texte" name="mail" />
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input type="submit" name="Preinscription" value="Ajouter !" class="btn" />
			</td>
		</tr>
      </table>
    </form>
<?php require("inc/footer.php");
	} else {
        throw new PolarUserError("Vous n'êtes pas autorisé à accéder à cette billetterie.");
    }
}
else if(isset($_GET['Email'])){
	$id = intval($_GET['Email']);

	$req = query("SELECT pb.ID, pb.Contact FROM polar_billetterie pb
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		WHERE pb.ID = $id AND pbo.User = $conid");
	if(mysql_num_rows($req) == 1){
		addFooter('<script type="text/javascript">
		$(document).ready(function(){
			$("#frm").submit(function(event){
				$("#submitter").val("Envoi en cours...").attr("disabled", "disabled");
			});
		});
		</script>');
		$result = mysql_fetch_assoc($req);
		require("inc/header.php");
		?>
		<h1>Envoi de mail aux inscrits</h1>
		<p>Cette fonctionnalité permet l'envoi d'un mail aux personnes inscrites à un événement.</p>
		<form method="post" action="<?php echo $racine.$module."/".$section; ?>_control?Email=<?php echo $result['ID']; ?>" id="frm">
			<table>
				<tr>
					<td>De</td>
					<td><?php echo $result['Contact']; ?></td>
				</tr>
				<tr>
					<td>À</td>
					<td>
						<select name="ep-groupe">
							<option value="nonpaye">Personnes n'ayant pas encore payé</option>
							<option value="paye">Personnes ayant payé</option>
							<option value="payenonentre">Personnes ayant payé mais n'étant pas entrées</option>
							<option value="tous">Tous</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Sujet</td>
					<td>
						<input name="ep-sujet" class="in-texte" size="50">
					</td>
				</tr>
				<tr>
					<td>Message</td>
					<td>
						<textarea name="ep-message" rows="20" cols="75"></textarea><br />
						<small>Utiliser %id% pour afficher le numéro de commande de la personne.</small>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input class="btn" id="submitter" type="submit" value="Envoyer !" />
					</td>
				</tr>
			</table>
		</form>
		<?php
		require("inc/footer.php");
	}
}
else {
	use_jquery_ui();

	$req2 = query("SELECT pb.ID AS Event, pbt.ID AS Tarif, Intitule, pb.CodesBarres FROM polar_billetterie_tarifs pbt
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		WHERE pbo.User = $conid");
	$tarifs = array();
	while($res2 = mysql_fetch_assoc($req2)){
		$res2['NbParPages'] = count(parseCoords($res2['CodesBarres']));
		$tarifs[$res2['Event']][] = $res2;
	}
	$tarifs = json_encode($tarifs);

	addFooter('
	<script>
	var tarifs = '.$tarifs.';
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
		$("#dialog-places").dialog({ width: 700, autoOpen: false });
		//$("#newevent").click(function(){
		//	$("#dialog").dialog("open");
		//	$("#nbPages").focus();
		//});
		$(".butplaces").each(function(){
			$(this).click(function(){
				var evenement = $(this).attr("event");
				$("#idEvent").val(evenement);
				$("#tarif").html("");
				for(i in tarifs[evenement])
					$("#tarif").append(\'<option value="\'+tarifs[evenement][i].Tarif+\'">\'+tarifs[evenement][i].Intitule+\'</option>\');
				$("#nbPlaces").val(tarifs[$("#idEvent").val()][0]["NbParPages"]);
				$("#dialog-places").dialog("open");
			});
		});
		$("#nbPages").keyup(function(){
			$("#nbPlaces").val($(this).val() * tarifs[$("#idEvent").val()][0]["NbParPages"]);
		});
	});
	</script>
	');
	require('inc/header.php');
	?>
	<h1>Gestion de la billetterie</h1>
	<!--<input type="button" value="Ajouter un événement !" class="btn" id="newevent" />-->
	<?php
	$req = query("SELECT ID, Titre, DATE(Date) AS Date, CodesBarres, pb.Contact,
	 	(SELECT COUNT(*) FROM polar_commandes pc INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type WHERE DateCommande IS NOT NULL AND pbt.Evenement = pb.ID AND pc.Termine = 0) AS NbGenerees,
	 	(SELECT COUNT(*) FROM polar_commandes pc INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type WHERE DateCommande IS NOT NULL AND DatePaiement IS NOT NULL AND pbt.Evenement = pb.ID AND pc.Termine = 0) AS NbPaye,
	 	(SELECT COUNT(*) FROM polar_commandes pc INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type WHERE DateCommande IS NOT NULL AND DatePaiement IS NOT NULL AND DateRetrait IS NOT NULL AND pbt.Evenement = pb.ID AND pc.Termine = 0) AS NbEntrees
		FROM polar_billetterie pb
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		WHERE pbo.User = $conid
		ORDER BY Date ASC");
	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<tr>
			<th>ID</th><th>Nom</th><th>Date</th><th>Places</th><th>Envoyer un mail</th><th>+ Résa</th></tr>';
	while($data = mysql_fetch_assoc($req)){
		echo '<tr>';
		echo '<td>'.$data['ID'].'</td>';
		echo '<td>'.$data['Titre'].'</td>';
		echo '<td>'.$data['Date'].'</td>';
		echo '<td>
		<p>'.$data['NbGenerees'].' places générées/préinscriptions<br />
		'.$data['NbPaye'].' places payées<br />
		'.$data['NbEntrees'].' entrées</p>
		<p>';
		if(!empty($data['CodesBarres']))
			echo '<input type="button" value="Impimer des places !" class="btn butplaces" event="'.$data['ID'].'" />';
		echo '</p>
		</td>';
		echo '<td>';
		if(!empty($data['Contact']))
			echo '<a href="'.$racine.$module.'/'.$section.'?Email='.$data['ID'].'"><img title="Envoyer un mail aux inscrits" src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></a>';
		echo '</td>';
		echo '<td>';
        echo '<a href="'.$racine.$module.'/'.$section.'?add='.$data['ID'].'"><img title="Ajouter une réservation" src="'.$racine.'styles/'.$design.'/icones/ajouter.png" alt="-" /></a>';
		echo '</td>';
		echo '</tr>';
	}
	echo '</table>';
	?>
	<div id="dialog" title="Ajouter un événement">
	<form name="formulaire" method="post" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control?ajouter'; ?>">
	     <table>
	       <tr>
	         <td>UV :</td>
	         <td><input type="text" class="in-text" name="ep-uv" /></td>
	       </tr>
	       <tr>
	         <td>Photo :</td>
	         <td><input type="file" name="ep-fichier" /></td>
	       </tr>
	       <tr>
	         <td colspan="2" style="text-align:right;"><input type="submit" value="Ajouter le manuel !" class="btn" /></td>
	       </tr>
	     </table>
	   </form>
	</div>
	<div id="dialog-places" title="Imprimer des places">
		<form method="post" action="<?php echo $racine.$module.'/'.$section.'_control?places'; ?>">
			<table>
				<tr>
					<td>Tarif :</td>
					<td>
						<select name="tarif" id="tarif"></select>
					</td>
				</tr>
				<tr>
					<td>Nombre de pages :</td>
					<td>
						<input type="text" class="in-text" id="nbPages" name="pages" value="1" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<td>Nombre de places :</td>
					<td>
						<input type="text" class="in-text" id="nbPlaces" value="6" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Marquer ces places comme payées :</td>
					<td>
						<input type="checkbox" name="paye" value="1" />
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:right;">
						<input type="submit" value="Générer" class="btn" /> <small>ATTENTION : les places ne peuvent être générés qu'une seule fois ! Ne pas recharger la page !</small>
					</td>
				</tr>
			</table>
			<input type="hidden" name="event" id="idEvent" value="" />
		</form>
	</div>
	<?php
	require('inc/footer.php');
}
?>
