<?php
if(isset($_GET['export'])){
	require_once("_checksum.php");

	$id = intval($_GET['export']);

	$req = query("SELECT pc.ID,pc.DatePaiement,pc.DateRetrait, pct.CodeProduit
		FROM polar_commandes pc
		INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
	 	WHERE pbo.User = $conid AND pb.ID = $id AND pc.Termine = 0
		ORDER BY pc.ID DESC");

	header("Content-Disposition: attachment; filename=event-$id.csv");

	while($data = mysql_fetch_assoc($req)){
		$status = "unpaid";
		if(!empty($data['DatePaiement']))
			$status = "paid";
		if(!empty($data['DateRetrait']))
			$status = "in";

		$barcode = sprintf("%010s", $data['CodeProduit']*1e6+$data['ID']);
		$checksum = checksum($barcode);
		echo $barcode.$checksum.";".$status."\n";
	}
}
elseif(isset($_GET['export2'])){
	require_once("_checksum.php");

	$id = intval($_GET['export2']);

	$req = query("SELECT pc.ID,DateCommande,DatePaiement,DateRetrait,pct.CodeProduit, pc.Nom, pc.Prenom, pc.Mail, pca.PrixVente AS PrixTTC
		FROM polar_commandes pc
		INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
		INNER JOIN (SELECT CodeCaisse, MAX(ID) AS Max FROM polar_caisse_articles GROUP BY CodeCaisse) pcam ON pcam.CodeCaisse = pct.CodeProduit
		INNER JOIN polar_caisse_articles pca ON pca.ID = pcam.Max
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
	 	WHERE pbo.User = $conid AND pb.ID = $id AND Termine = 0
		GROUP BY pc.ID
		ORDER BY pc.ID DESC");

	header("Content-Disposition: attachment; filename=event2-$id.csv");

	while($data = mysql_fetch_assoc($req)){
		$id = $data['ID'];
		$barcode = sprintf("%010s", $data['CodeProduit']*1e6+$data['ID']);
		$barcode .= checksum($barcode);
		$nom = $data['Nom'];
		$prenom = $data['Prenom'];
		$commande = $data['DateCommande'];
		$paiement = $data['DatePaiement'];
		$entree = $data['DateRetrait'];
		$tarif = $data['PrixTTC'];
		$email = $data['Mail'];

		echo "$id;$barcode;$nom;$prenom;$commande;$paiement;$entree;$tarif;$email\n";
	}
}
elseif (isset($_GET['exportJson'])) {
    $id = intval($_GET['exportJson']);
	header("Content-Disposition: attachment; filename=polar-event-$id.json");
    header("Content-Type: application/json");

    // on regarde si l'utilisateur actuel à le droit d'accèder à cette billetterie
    $right = BilletterieOrganisateur::select('COUNT(*)')
        ->where('Billetterie = ? AND User = ?', $id, $user->get_id())
        ->rawExecute();

    if ($right->fetchColumn() == 0)
        die(json_encode(array("erreur" => "vous n'avez pas le droit d'accèder à cette billetterie")));
    $right->closeCursor();

    $result = array('billetterie' => $id, 'tarifs' => array(),
                    'commandes' => array());

    $tarifs = TarifBilletterie::select('TarifBilletterie.*, Article.Nom as NomArticle, CommandeType.CodeProduit')
        ->join('CommandeType', 'CommandeType.ID = TarifBilletterie.TypeCommande')
        ->join('Article', 'Article.CodeCaisse = CommandeType.CodeProduit')
        ->where('Article.Actif = 1')
        ->where('TarifBilletterie.Evenement = ?', $id);

    foreach ($tarifs as $tarif)
        $result['tarifs'][] = array('id' => $tarif->get_id(),
                                    'nom' => $tarif->Intitule,
                                    'code_article' => $tarif->CodeProduit,
                                    'nom_article' => $tarif->NomArticle);

    $commandes = Commande::select('Commande.*, TarifBilletterie.ID as Tarif, CommandeType.CodeProduit as CodeCaisse')
        ->join('TarifBilletterie', 'Commande.Type = TarifBilletterie.TypeCommande')
        ->join('CommandeType', 'CommandeType.ID = Commande.Type')
        ->where('TarifBilletterie.Evenement = ?', $id)
        ->where('Commande.Termine = 0');

    foreach ($commandes as $commande)
        $result['commandes'][] = array('id' => $commande->get_id(),
                                       'barcode' => $commande->get_barcode(),
                                       'tarif' => $commande->Tarif,
                                       'nom' => $commande->Nom,
                                       'prenom' => $commande->Prenom,
                                       'mail' => $commande->Mail,
                                       'date_commande' => $commande->DateCommande,
                                       'date_paiement' => $commande->DatePaiement,
                                       'date_retrait' => $commande->DateRetrait);

    echo json_encode($result);
}
elseif(isset($_GET['PayNow'])){
	$id = intval($_GET['PayNow']);

	$req = query("SELECT pb.ID FROM polar_commandes pc
		INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		WHERE pc.ID = $id AND pbo.User = $conid AND pc.Termine = 0");
	if(mysql_num_rows($req) == 1){
		$res = mysql_fetch_assoc($req);
		query("UPDATE polar_commandes SET DatePaiement = NOW(), IDVente = NULL WHERE ID = $id");
		$event = $res['ID'];
		header("Location: $racine$module/$section?Event=$event");
	}
}
elseif(isset($_GET['Supprimer'])){
	$id = intval($_GET['Supprimer']);

	$req = query("SELECT pb.ID FROM polar_commandes pc
		INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		WHERE pc.ID = $id AND pbo.User = $conid AND pc.Termine = 0");
	if(mysql_num_rows($req) == 1){
		$res = mysql_fetch_assoc($req);
		query("UPDATE polar_commandes SET Termine = 1 WHERE ID = $id");
		$event = $res['ID'];
		header("Location: $racine$module/$section?Event=$event");
	}
}
?>
