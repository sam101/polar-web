<?php
if(isset($_POST['Preinscription'])){
	// Récup des infos
	$prenom = mysqlSecureText($_POST['prenom']);
	$nom = mysqlSecureText($_POST['nom']);
	$tarif = intval($_POST['tarif']);
	$ip = mysqlSecureText(get_ip());
	$mail = mysqlSecureText($_POST['mail']);

	// Vérif des informations
	if(empty($prenom))
		ajouterErreur("saisissez votre prénom");
	if(empty($nom))
		ajouterErreur("saisissez votre nom");
	if(empty($mail) || !verifMail($mail))
		ajouterErreur("adresse e-mail non valide");
	if(empty($tarif))
		ajouterErreur("choisissez l'événement auquel vous souhaitez participer");

	$req = query("SELECT  pb.Titre, DATE(pb.Date) AS Date, pb.Places, pa.Asso AS NomAsso, pb.Contact,
	 	pbt.Intitule, pca.PrixVente AS Tarif, pb.InfosPreinscription, pbt.RequireBDE, pbt.Adulte, pbt.TypeCommande as CommandeType,
		(SELECT COUNT(*) FROM polar_commandes pc
		INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
		WHERE DatePaiement IS NOT NULL AND pbt.Evenement = pb.ID AND Termine = 0) AS PlacesPrises
		FROM polar_billetterie_tarifs pbt
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		LEFT JOIN polar_assos pa ON pa.ID = pb.Asso
		INNER JOIN polar_commandes_types pct ON pct.ID = pbt.TypeCommande
		INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
		WHERE Preinscription = 1 AND NOW() < pb.Date AND pca.Actif = 1 AND pca.EnVente = 1 AND pbt.ID = $tarif AND (pbt.Prive IS NULL OR pbt.Prive = 0)");
	if(mysql_num_rows($req) != 1) {
        Log::notice("Tentative d'inscription au tarif ".$tarif);
		ajouterErreur("événement non disponible");
    }

	if(hasErreurs()){
		saveFormData($_POST);
		header("Location: $racine$module/$section");
		die();
	}
	else {
		$data = mysql_fetch_assoc($req);
        $type = $data['CommandeType'];
        if ($data['Places'] <= $data['PlacesPrises'])
            ajouterErreur('Il n\'y a plus de places pour cet évènement !');

        if ($user->is_logged()) {
            $guser = $ginger->getUser($user->Login);
            if (!isset($guser->error)) {
                $cot = $guser->is_cotisant;
                $adulte = $guser->is_adulte;
            } else {
                $cot = false;
                $adulte = false;
            }
        }

        if ($data['Adulte'] == 1 && !$adulte)
            ajouterErreur("Désolé, il faut être adulte pour réserver une place à ce tarif.");

        if ($data['RequireBDE'] == 1) { //cotisants only
            if ($cot) {
                $req = query("SELECT pu.Email, COUNT(pc.ID) as Resas FROM polar_utilisateurs pu JOIN polar_commandes pc ON pc.Mail = pu.Email WHERE pc.Termine = 0 AND pu.ID = ".$_SESSION['con-id']." AND pc.Type = ".$type);
                $usr = mysql_fetch_assoc($req);

                if ($usr['Resas'] >= 1) {
                    Log::notice('Tentative de réinscription au tarif $tarif pour '.$user->Email);
                    ajouterErreur('Vous avez déjà réservé un tarif cotisant pour cet évènement');
                } else {
                    // ok !
                    // on remplace le mail fourni par l'utilisateur par le mail de la session pour être sûr de bien pouvoir vérifier si il essaye de prendre une deuxième place cotisant
                    $mail = $usr['Email'];
                }
            } else {
                Log::notice("Tentative d'inscription à un tarif (".$tarif.") cotisant pour ".$mail);
                ajouterErreur('Ce tarif est réservé aux cotisants BDE-UTC');
            }
        }

        if(hasErreurs()){
            saveFormData($_POST);
            header("Location: $racine$module/$section");
            die();
        }

		// Sauvegarde de la commande
		query("INSERT INTO polar_commandes (
				Type,
				Nom,
				Prenom,
				Mail,
				IPCommande,
				DateCommande
			) VALUES (
				$type,
				'$nom',
				'$prenom',
				'$mail',
				'$ip',
				NOW()
			)
		");
		$id = mysql_insert_id();

        $commande = Commande::getById($id); // todo
        Request::$panier->addCommande($commande, $user);
	}
    Request::$panier->redirectTo($commande);
}
else if (isset($_GET['get-tarifs'])) {
  $billetterie = intval($_GET['get-tarifs']);
  $tarifs = array();
  if ($billetterie > 0) {

      if ($user->is_logged() && isCotisant($user->Login)) {
          $cond = '';
      } else {
          $cond = 'AND pbt.RequireUTC = 0';
      }

    $req = query("SELECT pbt.ID, pbt.Intitule, pca.PrixVente As Tarif
                  FROM polar_billetterie_tarifs pbt
                  INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
                  INNER JOIN polar_commandes_types pct on pct.ID = pbt.TypeCommande
                  INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
                  WHERE pb.ID = $billetterie AND pca.Actif = 1 AND pca.EnVente = 1 $cond AND (pbt.Prive IS NULL OR pbt.Prive = 0)");

    while ($data = mysql_fetch_assoc($req)) {
      $tarifs[] = $data;
    }
  } else {
    Log::notice("Tentative d'accès à la billetterie ".$billetterie);
    $tarifs["erreur"] = "Mauvaise billetterie";
  }

  echo json_encode($tarifs);
}
?>
