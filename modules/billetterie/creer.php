<?php

use_jquery_ui();

if (isset($_GET['billetterie'])) {
    $gesarticle = Payutc::loadService('GESARTICLE', 'manager');

    $billetterie = intval($_GET['billetterie']);
    $infos = mysql_fetch_assoc(query("SELECT * FROM polar_billetterie WHERE ID = $billetterie"));
    $tarifs = query("SELECT pbt.Intitule, pbt.RequireUTC, pbt.RequireBDE, pca.PrixVente, pca.Nom
                    FROM polar_billetterie_tarifs pbt
                    JOIN polar_commandes_types pct ON pct.ID = pbt.TypeCommande
                    JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
                    WHERE pbt.Evenement = $billetterie");
    $organisateurs = query("SELECT pu.Nom, pu.Prenom
                            FROM polar_billetterie_organisateurs pbo
                            JOIN polar_utilisateurs pu ON pbo.User = pu.ID
                            WHERE pbo.Billetterie = $billetterie");
    $utilisateurs = query("SELECT ID, Nom, Prenom FROM polar_utilisateurs ORDER BY Nom");
    $infos_code = mysql_fetch_array(query("SELECT COALESCE(MAX(CodeCaisse), 999) FROM polar_caisse_articles WHERE CodeCaisse >= 1000 AND CodeCaisse < 1400"));
    $code = $infos_code[0]+1;

addFooter('
	<script type="text/javascript" src="'.$racine.'js/jquery.jeditable.min.js"></script>
	<script>
    submit = function(value, settings) { 
               $.post("'.$racine.$module.'/'.$section.'_control?liveEdit='.$billetterie.'", {id: $(this).attr("id"), value: value});

               return(value);
    };
    $(document).ready(function() {
        $(".edit").editable(submit, {submit : "OK",
                                     tooltip: "Cliquer pour modifier",
                                     style: "inherit",
                                     width: "none"});
        $(".edit_area").editable(submit, {submit : "OK",
                                          tooltip: "Cliquer pour modifier",
                                          type: "textarea",
                                          rows: 10,
                                          cols: 30});
})
    </script>
');

    require('inc/header.php');
    afficherErreurs();
?>
<h1>Billetterie <?php echo $billetterie.': '.$infos['Titre'] ?></h1>
<p>
Date limite de réservation : <span class="edit" style="display: inline" id="date"><?php echo $infos['Date']; ?></span><br/>
Nombre de places : <span class="edit" style="display: inline" id="places"><?php echo $infos['Places']; ?></span><br/>
Adresse de contact : <span class="edit" style="display: inline" id="contact"><?php echo $infos['Contact']; ?></span><br/>
Message de préinscription : <br/>
<div class="edit_area" id="message"><?php echo nl2br($infos['InfosPreinscription']); ?></div>

<br/>
<h3>Tarifs</h3>
<?php while($data = mysql_fetch_assoc($tarifs)) {
  echo $data['Nom'].' ('.$data['Intitule'].') : '.formatPrix($data['PrixVente']).'€<br/>';
}
?>

<br/>
<h3>Organisateurs</h3>
<?php while($data = mysql_fetch_assoc($organisateurs)) {
  echo $data['Nom'].' '.$data['Prenom'].'<br/>';
}
?>

<br/>

<h1>Ajouter un tarif</h1>
<br/>
<form action="<?php echo $racine.$module.'/'.$section.'_control' ?>" method="POST">
    <input type="hidden" name="billetterie" value="<?php echo $billetterie ?>" />
    <label for="prix">Prix : </label><input type="text" name="prix" id="prix" /><br/>
    <label for="nom">Nom : </label><input type="text" name="nom" id="nom" /><br/>
    <label for="code-article">Code Article : </label><input type="text" name="code-article" id="code-article" value="<?php echo $code; ?>"/><br/>
    <label for="nom-article">Nom Article : </label><input type="text" name="nom-article" id="nom-article" size="40" />(40 caractères maximum)<br/>
    <input type="checkbox" name="require-utc" id="require-utc" /><label for="require-utc">UTCéen nécessaire</label><br/>
    <input type="checkbox" name="require-bde" id="require-bde" /><label for="require-bde">Cotisant BDE nécessaire</label><br/>
    <input type="checkbox" name="is-prive" id="is-prive" /><label for="is-prive">Tarif privé (réservation uniquement via l'API)</label><br/>
    <input type="checkbox" name="is-adulte" id="is-adulte" /><label for="is-adulte">Tarif réservé aux adultes</label><br/>
    <br/>
    <input type="submit" value="Ajouter" />
</form>

<br/>

<h1>Ajouter un organisateur</h1>
<br/>
<form action="<?php echo $racine.$module.'/'.$section.'_control' ?>" method="POST">
    <input type="hidden" name="billetterie" value="<?php echo $billetterie ?>" />
    <label for="users-list">Utilisateur : </label><select type="text" name="organisateur" id="users-list">
    <?php while($data = mysql_fetch_assoc($utilisateurs)) {
        echo '<option value="'.$data['ID'].'">'.$data['Nom'].' '.$data['Prenom'].'</option>';
    } ?>
    </select>
    <input type="submit" value="Ajouter" />
</form>
</p>
<?php
} else {
    $assos = Asso::select()->where("Etat = 'Actif'")->order('Asso');
    $billetteries = Billetterie::select('Billetterie.*, Asso.Asso as NomAsso')->where('CodesBarres is NULL')->join('Asso', 'Asso.ID= Billetterie.Asso')->order('Billetterie.ID DESC');

    addFooter('<script type="text/javascript" src="'.$racine.'js/jquery.timepicker.js"></script>');
    addFooter('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.timepicker.css"/>');
    addFooter('
    <script type="text/javascript">
	$(document).ready(function() {
		$("#datepicker").datetimepicker({ dateFormat: "yy-mm-dd",
		                              timeFormat: "HH:mm" });
	});
	</script>');

    require('inc/header.php');
?>
<h1>Créer une billetterie</h1>
<br/>
<p><b>Attention</b> : cette page ne permet de créer que des billetteries sans billets physique (type Cac'carotte)</p>
<br/>
<form action="<?php echo $racine.$module.'/'.$section.'_control' ?>" method="POST">
    <label for="titre-billetterie">Titre : </label><input type="text" name="titre" id="titre-billetterie" /><br/>
    <label for="date-billetterie">Date limite de réservation : </label><input size="15" type="text" class="in-text hasDatePicker" name="date" id="datepicker" /><br/>
    <label for="nb-places">Nombre de places : </label><input type="text" name="nb-places" id="nb-places" value="0"/> (0 pour pas de limite)<br/>
    <label for="asso-list">Asso : </label><select type="text" name="asso" id="asso-list">
    <option value="0">- Pas d'asso liée -</option>
    <?php foreach($assos as $asso) {
          echo '<option value="'.$asso->get_id().'">'.$asso->Asso.'</option>';
    } ?>
    </select><br/><br/>
    <label for="infos">Message préinscription : </label><br/>
    <textarea name="infos" id="infos"></textarea>
    <br/>
    <input type="submit" value="Créer" />
</form>
<h1>Modifier une billetterie</h1>
<table class="table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>Nom de la billetterie</th>
      <th>Asso</th>
      <th>Date de fin</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($billetteries as $billetterie) { ?>
    <tr>
      <td><a href="<?php echo urlSection('billetterie='.$billetterie->get_id()); ?>"><?php echo $billetterie->Titre; ?></a></td>
      <td><?php echo $billetterie->NomAsso; ?></td>
      <td><?php echo $billetterie->Date; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php
}
require('inc/footer.php');
?>
