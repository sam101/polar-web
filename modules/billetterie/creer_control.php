<?php
if (isset($_POST['titre'], $_POST['date'], $_POST['nb-places'], $_POST['asso'], $_POST['infos'])) {
    $titre = mysqlSecureText($_POST['titre']);
    $date = mysqlSecureText($_POST['date']);
    $asso = intval($_POST['asso']);
    $asso = ($asso > 0) ? $asso : 'NULL';
    $places = intval($_POST['nb-places']);
    $places = ($places > 0) ? $places : 'NULL';
    $infos = mysqlSecureText($_POST['infos']);

    query("INSERT INTO polar_billetterie
           (Titre, Date, Asso, Places, Preinscription, Contact, InfosPreinscription)
           VALUES ('$titre', '$date', $asso, $places, 1, 
           (SELECT MailAsso FROM polar_assos WHERE ID = $asso), '$infos')");
    $id = mysql_insert_id();
    header("Location: $racine$module/$section?billetterie=$id");
}
else if (isset($_POST['billetterie'], $_POST['prix'], $_POST['nom'], $_POST['code-article'], $_POST['nom-article'])) {
    $billetterie = Billetterie::getById(intval($_POST['billetterie']));
    $code = intval($_POST['code-article']);

    if (!($billetterie instanceof Billetterie))
        redirectWithErrors('', "Cette billetterie n'existe pas");
    elseif (!preg_match('/^\d+[.,]?\d*$/', $_POST['prix']))
        redirectWithErrors('billetterie='.$billetterie, "Prix mal formaté");
    elseif ($code >= 1400 or $code < 1000)
        redirectWithErrors('billetterie='.$billetterie, "Code invalide $code");
    elseif ($_POST['nom-article'] == '')
        redirectWithErrors('billetterie='.$billetterie, "Merci de remplir le champs 'Nom Article'");
    elseif ($_POST['nom'] == '')
        redirectWithErrors('billetterie='.$billetterie, "Merci de remplir le champs 'Nom'");

    else {
        $db->beginTransaction();

        $prix = strtr($_POST['prix'], ',', '.');

        $article = new Article(array('CodeCaisse' => $code,
                                     'Nom' => $_POST['nom-article'],
                                     'Secteur' => 6,
                                     'PrixAchat' => $prix,
                                     'PrixVente' => $prix,
                                     'TVA' => 0,
                                     'Date' => raw('NOW()'),
                                     'EnVente' => True,
                                     'Actif' => True
                                     ));
        $article->save();
        PayutcTools::addArticle($article);

        $type = new TypeCommande(array('Nom' => 'Billet',
                                       'CodeProduit' => $code,
                                       'Actif' => 1,
                                       'RequireCommande' => 1));
        $type->save();

        $tarif = new TarifBilletterie(array('Evenement' => $billetterie,
                                            'TypeCommande' => $type,
                                            'Intitule' => $_POST['nom'],
                                            'RequireUTC' => isset($_POST['require-utc']),
                                            'RequireBDE' => isset($_POST['require-bde']),
                                            'Prive' => isset($_POST['is-prive']),
                                            'Adulte' => isset($_POST['is-adulte'])));
        $tarif->save();
        $db->commit();
    }
    redirectOk('billetterie='.$billetterie);
}
else if (isset($_POST['billetterie'], $_POST['organisateur'])) {
    $billetterie = intval($_POST['billetterie']);
    $user = intval($_POST['organisateur']);
    if ($billetterie > 0 and $user > 0) {
        query("INSERT INTO polar_billetterie_organisateurs
               (Billetterie, User) VALUES ($billetterie, $user)");
    }
    redirectOk('billetterie='.$billetterie);
} else if (isset($_GET['liveEdit'], $_POST['id'], $_POST['value'])) {
    $billetterie = Billetterie::getById(intval($_GET['liveEdit']));
    if ($billetterie instanceof Billetterie) {
        $val = $_POST['value'];
        switch($_POST['id']) {
        case 'date':
            $billetterie->Date = $val;
            break;
        case 'message':
            $billetterie->InfosPreinscription = $val;
            break;
        case 'contact':
            $billetterie->Contact = $val;
            break;
        case 'places':
            $places = intval($val);
            $billetterie->Places = ($places > 0) ? $places : NULL;
            break;
        default:
            die();
        }
        $billetterie->save();
    }
} else {
    throw new PolarUserError('Action invalide');
}
?>
