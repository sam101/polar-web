<?php
$titrePage = 'Préinscription à un événement';

if ($user->is_logged()) {
    $cotisant = isCotisant($user->Login);
    $connecte = true;
    $nom = $user->Nom;
    $prenom = $user->Prenom;
    $email = $user->Email;
} else {
    $connecte = false;
    $cotisant = false;

    $infos = Request::$panier->getUserInfos();
    if ($infos !== false) {
        $nom = $infos['nom'];
        $prenom = $infos['prenom'];
        $email = $infos['mail'];
    } else {
        $nom = getFormData('ep-nom');
        $prenom = getFormData('ep-prenom');
        $email = getFormData('ep-mail');
    }
}

addFooter('
<script type="text/javascript">
$(function() {
var erreur_mail = [["@eut.utc.fr", "@etu.utc.fr"],
                   ["@utc.etu.fr", "@etu.utc.fr"],
                   ["@etu.fr", "@etu.utc.fr"],
                   ["@gmail.fr", "@gmail.com"],
                   ["@etu-utc.fr", "@etu.utc.fr"]
                  ]
  $(\'#ep-mail\').keyup(function() {
    var text = $(\'#ep-mail\').val();
    if (text.length > 10) {
      erreur_mail.forEach(function(elt, index, arr) {
                              var idx = text.indexOf(elt[0]);
                              if (idx > 0)
                                  $(\'#ep-mail\').val(text.replace(elt[0], elt[1]));
                          });
    }
  });
  $("#billetteries").change(function() {
      var value = $("#billetteries").val();
      if (value > 0) {
        $.ajax({url: "'.$racine.$module.'/'.$section.'_control?get-tarifs="+value,
               dataType:"json",
                success : function(data, code, xhr) {
                  if (data.erreur) {
                    $("#billetteries").val(0);
                  } else {
                    var html = "";
                    for (var index in data) {
                      var tarif = data[index];
                      html += "<option value=\""+tarif["ID"]+"\">"+tarif["Intitule"]+" ("+tarif["Tarif"]+"€)</option>";
                    }
                    $("#tarifs").html(html);
                  }
               }}
             );
    } else {
      $("#tarifs").html("<option value=\"0\">Choisissez une billetterie...</option>");
    }});
});
</script>
');

require('inc/header.php');
?>
<h1>Préinscription à un événement</h1>
<div class="well">
<p>Cette page vous permet de vous préinscrire à un événement proposé par une association. Vous pourrez ensuite régler cette place au Polar ou bien directement auprès de l'association.</p>
<p>Après inscription, vous recevrez un mail de confirmation avec un numéro de commande. <b>Attention : votre préinscription ne sera confirmée qu'après paiement !</b></p>
</div>
<?php if($connecte) {
if ($cotisant) { ?>
<p>Vous avez accès au tarif cotisant !<br/>
</p>
<?php } else { ?>
<p>
Désolé, vous n'êtes pas cotisant BDE, vous n'avez pas accès aux tarifs spécials cotisants.<br/>
</p>
<?php } } else { ?>
<p class="alert alert-info">
<b><a href="<?php echo $CONF['cas_login'] ?>">Connectez-vous</a></b> pour avoir accès aux tarifs cotisant et aux tarifs réservés aux adultes.<br/>
</p>
<?php } ?>
<?php afficherErreurs(); ?>
<div class="text-center">
  <div style="display:inline-block;text-align:left;">
    <form method="post" action="<?php echo urlControl(); ?>" class="form-horizontal">
    <div class="control-group">
      <label class="control-label" for="billetteries">Évènement</label>
      <div class="controls">
        <select class="in-text" id="billetteries" name="billetterie">
          <option value="">---</option>
		<?php

  $req = query("SELECT pb.ID, pb.Titre, DATE(pb.Date) AS Date, pa.Asso, pb.Places - (SELECT COUNT(*) FROM polar_commandes pc
                                                                                    INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
                                                                                    WHERE DatePaiement IS NOT NULL AND pbt.Evenement = pb.ID AND pc.Termine = 0) AS Places
			FROM polar_billetterie pb
			LEFT JOIN polar_assos pa ON pa.ID = pb.Asso
			WHERE Preinscription = 1 AND NOW() < pb.Date
			ORDER BY Date ASC");

  		while($data = mysql_fetch_assoc($req)){
        echo '<option value="' . $data["ID"] . '" ';
        if(getFormData('billeterie') == $data['ID'])
          echo 'selected="selected"';
        if ($data['Places'] == 0)
            echo '>' . $data['Titre'] . ' le ' . $data['Date'] . ' (COMPLET)</option>\n';
        else if (!is_null($data['Asso']))
            echo '>' . $data['Titre'] . ' par ' . $data['Asso'] . ' le ' . $data['Date'] . ' ('.$data['Places'].' places restantes)</option>\n';
        else
            echo '>' . $data['Titre'] . ' le ' . $data['Date'] . ' ('.$data['Places'].' places restantes)</option>\n';
    }
		?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="tarifs">Tarif</label>
			<div class="controls">
				<select name="tarif" id="tarifs">
					<option value="0">Choisissez une billetterie...</option>
				</select>
			</div>
		</div>
		<div class="control-group">
          <label class="control-label" for="ep-nom">Nom</label>
          <div class="controls">
            <input type="text" class="in-texte" id="ep-nom" name="nom" value="<?php echo $nom ?>" />
          </div>
        </div>
		<div class="control-group">
          <label class="control-label" for="ep-prenom">Prénom</label>
          <div class="controls">
            <input type="text" class="in-texte" id="ep-prenom" name="prenom" value="<?php echo $prenom; ?>" />
          </div>
		</div>
		<div class="control-group">
          <label class="control-label" for="ep-mail">Adresse mail</label>
          <div class="controls">
            <input type="text" id="ep-mail" class="in-texte" name="mail" value="<?php echo $email; ?>" />
			</div>
		</div>
		<div class="control-group">
          <div class="controls">
            <input type="submit" name="Preinscription" value="Envoyer la demande !" class="btn btn-primary" />
          </div>
		</div>
      </form>
    </div>
  </div>
<p><small>Les informations que vous saisissez dans ce formulaire sont collectées par Le Polar et stockées de la même manière que pour toute commande sur ce site. Dans le cadre du partenariat entre Le Polar et l'association organisatrice de l'événement, ces informations lui seront transmises afin de permettre l'organisation de l'événement.</small></p>
<?php
require('inc/footer.php');
?>
