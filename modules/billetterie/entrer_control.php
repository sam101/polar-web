<?php
$id = mysqlSecureText($_POST['id']);
$perm = intval($_SESSION['con-id']);

require_once("_checksum.php");

if(strlen($id) == 13 && substr($id, 10, 3) != checksum(substr($id, 0, 10)))
	ajouterErreur("code barre incorrect");
elseif(strlen($id) == 10)
	$commande = substr($id, 4, 6);
else
	$commande = $id;

if(!hasErreurs()){
	$req = query("SELECT DatePaiement,DateRetrait FROM polar_commandes pc
		INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
		WHERE pc.ID = $commande AND DateCommande IS NOT NULL AND pct.Nom LIKE 'Billet' AND Termine = 0");

	if(mysql_num_rows($req) != 1)
		ajouterErreur("place incorrecte");

	if(!hasErreurs()){
		$res = mysql_fetch_assoc($req);
		if(empty($res["DatePaiement"]))
			ajouterErreur("place non payée");

		if(!empty($res["DateRetrait"]))
			ajouterErreur("place déjà utilisée");

		if(!hasErreurs()){
			query("UPDATE polar_commandes SET DateRetrait=Now(),IDRetrait = $perm WHERE ID = $commande LIMIT 1");
			$_SESSION["EstuSuccess"] = true;
		}
	}
}

header("Location: ".$_SERVER['HTTP_REFERER']);
?>
