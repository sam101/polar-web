<?php

if (isset($_GET['add'])) {
    if(!BilletterieOrganisateur::userIsOrganisateur(intval($_GET['add']),
                                                    $user))
        redirectWithErrors('add='.$_GET['add'], "Vous n'avez pas le droit d'accèder à cette billetterie");

	// Vérif des informations
	if(!isset($_POST['prenom']) || empty($_POST['prenom']))
		ajouterErreur("saisissez le prénom");
	if(!isset($_POST['nom']) || empty($_POST['nom']))
		ajouterErreur("saisissez le nom");
    if(!isset($_POST['mail']) || !verifMail($_POST['mail']))
		ajouterErreur("adresse e-mail non valide");
	if(!isset($_POST['tarif']))
		ajouterErreur("choisissez le tarif");

    $tarif = TarifBilletterie::select()
        ->where('Evenement = ? AND ID = ?', intval($_GET['add']),
                intval($_POST['tarif']))
        ->getOne();
    if ($tarif === NULL)
        ajouterErreur("Nananan, ce tarif n'existe pas");

    if (hasErreurs())
        redirectWithErrors('add='.$_GET['add']);

    $commande = new Commande(array('Type' => $tarif->TypeCommande,
                                   'Nom' => shtml($_POST['nom']),
                                   'Prenom' => shtml($_POST['prenom']),
                                   'Mail' => shtml($_POST['mail']),
                                   'DateCommande' => raw('NOW()'),
                                   'Termine' => false,
                                   'IPCommande' => get_ip()
                                   ));
    $commande->save();
    redirectOk('add='.$_GET['add'], "Inscription numéro $commande ajoutée !");
} else if(isset($_GET['places'])){
	require_once("_checksum.php");
	$nbpage = intval($_POST['pages']);
	$event = intval($_POST['event']);
	$tarif = intval($_POST['tarif']);

	// TODO sélection du tarif dans la génération
	$req = query("SELECT pct.CodeProduit, pbt.TypeCommande, pb.CodesBarres, pb.CBOrientation, pb.CBWidth, pb.CBHeight, pb.CBType
		FROM polar_billetterie_tarifs pbt
		INNER JOIN polar_commandes_types pct ON pct.ID = pbt.TypeCommande
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		WHERE pbt.ID = $tarif AND pbt.Evenement = $event AND pct.Nom LIKE 'Billet'
		LIMIT 1");
	if(mysql_num_rows($req) != 1)
		die("L'événement n'existe pas.");

	$evenement = mysql_fetch_assoc($req);
	$typeCommande = $evenement['TypeCommande'];
	$codeProduit = $evenement['CodeProduit'];

	require('lib/pdfb/pdfb.php'); // Must include this

	// Create a PDF object and set up the properties
	$pdf = new PDFB($evenement['CBOrientation'], "mm", "a4");
	$pdf->SetMargins(0, 0, 0, 0);

	$pdf->SetAuthor("Le Polar");
	$pdf->SetTitle("Billetterie");

	$pdf->SetFont("Arial", "", 10);

	// Set line drawing defaults
	$pdf->SetDrawColor(224);
	$pdf->SetLineWidth(1);

	// Load the base PDF into template
	$pdf->setSourceFile("upload/billetterie/$event-$tarif.pdf");
	$tplidx = $pdf->ImportPage(1);

	// Emplacement des code barres
	$coords = parseCoords($evenement['CodesBarres']);

	for($pg=0;$pg<$nbpage;$pg++){
		// Add new page & use the base PDF as template
		$pdf->AddPage();
		$pdf->useTemplate($tplidx);

		// Paramètres des code barres
		$xres = 2;
		$scale = .5;

		foreach($coords as $coord){
			// On ajoute la commande dans la base de données
			if(isset($_POST['paye']))
				query("INSERT INTO polar_commandes (
						Type,
						IPCommande,
						DateCommande,
						DatePaiement
					) VALUES (
						$typeCommande,
						'127.0.0.1',
						NOW(),
						NOW()
					)
				");
			else
				query("INSERT INTO polar_commandes (
						Type,
						IPCommande,
						DateCommande
					) VALUES (
						$typeCommande,
						'127.0.0.1',
						NOW()
					)
				");

			$id = mysql_insert_id();

			switch($evenement['CBType']){
				case "label-checksum": 	// Code-barre avec checksum en texte au dessus (pour étiquettes type manuels)
					$type = "C128C";
					$barcode = sprintf("%010s", $codeProduit*1e6+$id);
					$checksum = strtolower(checksum($barcode));

					$pdf->SetXY($coord[0], $coord[1]-5);
					$pdf->Cell($evenement['CBWidth'], 4, "sdf/$id/$checksum", 0, 0, "C");
				break;
				case "full-checksum": // Code-barre complet avec checksum intégré
					$type = "C128B";
					$barcode = sprintf("%010s", $codeProduit*1e6+$id);
					$barcode = $barcode.checksum($barcode);
				break;
				case "festupic12":
					$type = "C128C";
					$barcode = sprintf("%010s", $codeProduit*1e6+$id);

					$pdf->SetFont('courier', '', 16);
					$pdf->SetXY($coord[0]-158, $coord[1]-7);
					$pdf->Cell(40, 4, $id, 0, 0, "L");
					$pdf->SetXY($coord[0]-113, $coord[1]-7);
					$pdf->Cell(40, 4, $id, 0, 0, "L");
				break;
			  	case "sdf12":
					$type = "C128C";
					$barcode = sprintf("%010s", $codeProduit*1e6+$id);

					$pdf->SetFont('courier', '', 16);
					$pdf->setTextColor(255, 255, 255);
					$pdf->SetXY($coord[0]-172, $coord[1]+3);
					$pdf->Cell(40, 4, $id, 0, 0, "L");
				break;
			}


			// See pdfb/pdfb.php for parameters on BarCode()
			$pdf->BarCode($barcode, $type, $coord[0], $coord[1], $evenement['CBWidth']*$xres, $evenement['CBHeight']*$xres, 1/$xres, 1/$xres, $xres, 5, "", "PNG");
		}
	}
	$pdf->Output();
	$pdf->closeParsers();
} // Fin d'impression des bons de location
else if(isset($_GET['Email'])){
	// On vérifie qu'il a les droits sur cet événement
	$id = intval($_GET['Email']);

	// Sélection des gens à qui on écrit
	$and = "";
	switch($_POST['ep-groupe']){
		case 'nonpaye':
			$and = " AND pc.DatePaiement IS NULL ";
		break;
		case 'paye':
			$and = " AND pc.DatePaiement IS NOT NULL ";
		break;
		case 'payenonentre':
			$and = " AND pc.DatePaiement IS NOT NULL AND pc.DateRetrait IS NULL";
		break;
	}

	$req = query("SELECT pc.ID, pc.Mail, pb.Contact, pa.Asso FROM polar_commandes pc
		INNER JOIN polar_billetterie_tarifs pbt ON pbt.TypeCommande = pc.Type
		INNER JOIN polar_billetterie pb ON pb.ID = pbt.Evenement
		INNER JOIN polar_billetterie_organisateurs pbo ON pbo.Billetterie = pb.ID
		INNER JOIN polar_assos pa ON pb.Asso = pa.ID
		WHERE pb.ID = $id AND pbo.User = $conid AND pc.Termine = 0 $and");

	if(mysql_num_rows($req) > 0){
		$message = nl2br(stripslashes($_POST['ep-message']));
		$sujet = stripslashes($_POST['ep-sujet']);

		while($data = mysql_fetch_assoc($req))
			sendMail($data['Contact'], $data['Asso'], array($data['Mail']), $sujet, str_replace("%id%", $data['ID'], $message));
	}
	echo '<script>alert("Messages envoyés avec succès !");document.location.href="'.$racine.$module.'/'.$section.'";</script>';
}
?>
