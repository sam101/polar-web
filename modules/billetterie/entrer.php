<?php
$titrePage="Entrée à un événement";
addFooter('<script>$(document).ready(function(){
	$("#idloc").focus();
});</script>');
require('inc/header.php');
?>
            <h1>Entrer</h1>
<?php
if(hasErreurs())
	echo afficherErreurs();

if(isset($_SESSION["EstuSuccess"])){
	echo '<p style="color:green;font-size:20px;">Bienvenue !</p>';
	unset($_SESSION['EstuSuccess']);
}

?>
			<form id="formulaire" name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section; ?>_control">
              <table>
                <tr>
                  <td style="font-size:12px;">Num&eacute;ro de place :</td>
				<td><input type="text" id="idloc" name="id" autocomplete="off" /> <small>Scanner la place.</small></td>
                </tr>
                  <td colspan="2"><input type="submit" value="Entrer !" class="btn" /></td>
                </tr>
              </table>
            </form>
<?php
require('inc/footer.php');
?>


