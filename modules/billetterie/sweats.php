<?php

addHeaders('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />');
addFooter('<script type="text/javascript" src="'.$racine.'js/jquery.bgiframe.min.js"></script>
   <script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
	<script type="text/javascript">
    var commandes = []; ');

	$req = query("SELECT ID, Mail FROM polar_commandes
		WHERE DatePaiement IS NOT NULL
		AND DateRetrait IS NULL
		AND Type >= 128 AND Type <= 162");
    while($result = mysql_fetch_assoc($req)){
		addFooter('commandes.push(["'.$result['ID'].'", "'.$result['Mail'].'", "'.$result['ID'].'"]);');
    }
    addFooter('
    </script>
     <script type="text/javascript">
    $().ready(function() {
		$("#suggest1").focus().autocomplete(commandes,{
			matchContains:1,
			mustMatch:false,
			formatItem: function(row, i, max) {
				return row[0]+" - "+row[1];
			},
			formatResult: function(row) {
				return row[2];
			}
		});
    });
    </script>
	<style type="text/css">
		div.message {
			border: 1px solid black;
			padding: 1em;
			margin: .5em;
		}
	</style>
');
$titrePage = "Retrait d'une annale";

require("inc/header.php");
?>
<h1>Effectuer un retrait</h1>
<?php
if(!isset($_POST['ID'])){
	echo '<p>
			<center>
					<form method="post" action="'.$racine.$module.'/'.$section.'">
					Chercher par numéro de commande ou login,<br />
					ou bien scanner directement le code-barre du sweat.<br /><br />
					<input type="text" id="suggest1" size="50" name="ID" /><br /><br />
					<input type="submit" class="btn" value="Voir la commande" />
				</form>
			</center>
		</p>';
}
else{
	$id = mysqlSecureText($_POST['ID']);
	$noshow = false;

	if(strlen($id) == 10){
		$id = substr($id, 5);
		$noshow = true;
	}

	$req = query("SELECT * FROM polar_commandes WHERE DateRetrait IS NULL AND DatePaiement IS NOT NULL AND ID = $id");
	if(@mysql_num_rows($req) != 1){
		echo "<script>alert(\"Cette commande n'existe pas, est déjà retirée, n'est pas payée !\");document.location.href=\"$racine$module/$section\";</script>";
		die();
	}
	else{
		echo "<script>document.location.href='$racine$module/{$section}_control?idcommandeprete=$id';</script>";
		die();
	}
}

require("inc/footer.php");
?>


