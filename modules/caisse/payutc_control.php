<?php

if (isset($_POST['montant'], $_POST['commentaire'])) {
    $montant = floatVal(str_replace(',', '.', $_POST['montant']));

    $virement = new Virement();
    $virement->Date = raw('NOW()');
    $virement->Emetteur = "Payutc";
    $virement->Destinataire = "Le Polar";
    $virement->Montant = $montant;
    $virement->Effectue = 0;
    $virement->save();

    $transfert = new TransfertPayutc();
    $transfert->Date = raw('NOW()');
    $transfert->Montant = $montant;
    $transfert->Commentaire = $_POST['commentaire'];
    $transfert->Virement = $virement;
    $transfert->save();

    redirectOk();
}