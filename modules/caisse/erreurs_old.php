<?php
$titrePage = 'Suivi des erreurs de caisse';
use_datatables();
use_jquery_ui();
addFooter('
	<script type="text/javascript" src="'.$racine.'js/jquery.jeditable.min.js"></script>
	<script>
	$(document).ready(function() {
		$("#tableau_erreurs tbody td.editaaable").editable( \''.$racine.$module.'/'.$section.'_control\', {
			"callback": function( sValue, y ) {
				var aPos = theDataTable.fnGetPosition( this );
				theDataTable.fnUpdate( sValue, aPos[0], aPos[1] );
			},
			"submitdata": function ( value, settings ) {
				return { "row_id": this.parentNode.getAttribute(\'id\') };
			},
			"height": "14px"
		});
		$("#datepickerfin").datepicker({ dateFormat: \'yy-mm-dd\' });
		$("#datepicker").datepicker({ dateFormat: \'yy-mm-dd\' });
	});
	</script>');
	require_once('inc/header.php');
?>
<h1>Cher membre de l'équipe Comptable, Joyeuses Pâques !</h1>
<p>Cet outil révolutionnaire te permet de suivre et de commenter les erreurs de caisse. Tu peux les exporter au format CSV ou PDF pour l'envoyer à Brubru ou l'archiver.</p>
<p>
  <form action="<?php echo $racine.$module.'/'.$section.'_control?export' ?>" method="POST">
    Extraire du <input size="15" type="text" class="in-text" name="ep-datedebut" id="datepicker">
			au <input size="15" type="text" class="in-text" name="ep-datefin" id="datepickerfin"> &nbsp;
			<input type="submit" name="ExtraireJournal" value="Exporter" class="btn" />
  </form>
</p>
<?php
$req=query("SELECT polar_caisse_erreurs.*, polar_utilisateurs.prenom FROM polar_caisse_erreurs
	INNER JOIN polar_utilisateurs ON polar_utilisateurs.id=polar_caisse_erreurs.user
	ORDER BY id DESC");

echo '<table id="tableau_erreurs" class="datatables table table-bordered table-striped">';
echo '<thead><tr><th>Date</th><th>Erreur Espèces</th><th>Erreur CB</th><th>Erreur Monéo</th><th>Commentaire</th></tr></thead><tbody>';
while($data=mysql_fetch_array($req))
	echo '<tr id="'.$data['id'].'"><td>'.$data['date'].'</td><td>'.$data['erreur'].'</td><td>'.$data['diff_cb'].'</td><td>'.$data['diff_moneo'].'</td><td class="editaaable">'.$data['commentaire'].'</td></tr>';

echo '</tbody></table>';
require_once('inc/footer.php');
