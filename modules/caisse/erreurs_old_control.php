<?php
if(isset($_POST['row_id'])){
	$row = intval($_POST['row_id']);
	$message = mysqlSecureText($_POST['value']);
	query("UPDATE polar_caisse_erreurs SET
		commentaire = '$message'
		WHERE ID = $row");
	echo stripslashes($_POST['value']);
}
else if (isset($_GET['export'], $_POST['ep-datedebut'], $_POST['ep-datefin'])) {
    $datedebut = mysqlSecureText($_POST['ep-datedebut']);
	$datefin = mysqlSecureText($_POST['ep-datefin']);
	$nom = 'Documents/ErreursCaisse-'.date('dmY').'-'.date('His').'.csv';

	$req = query("SELECT date, erreur, diff_cb, diff_moneo, commentaire
		FROM polar_caisse_erreurs
        WHERE date >= '$datedebut' AND
		      date <= '$datefin'
		ORDER BY id ASC");

	$liste = array();
	$liste[] = array(
                     'Date',
                     'Erreur Especes',
                     'Erreur CB',
                     'Erreur Moneo',
                     'Commentaire'
                     );

	while($data=mysql_fetch_assoc($req)){
		$liste[] = array(
                         $data['date'],
                         str_replace(".", ",", $data['erreur']),
                         str_replace(".", ",", $data['diff_cb']),
                         str_replace(".", ",", $data["diff_moneo"]),
                         unhtmlentities($data['commentaire'])
                         );
	}

	$fichier = fopen($nom, "w");
	foreach($liste as $ligne){
		foreach($ligne as $item){
			fwrite($fichier, '"'.str_replace('"', '""', $item).'";');
		}
		fwrite($fichier, "\n");
	}
	fclose($fichier);
	header("Location: $racine$nom");
}
?>
