<?php

if (isset($_POST['especes'])) {
    $moyens = array('moneo', 'cb', 'cheque', 'especes', 'payutc');
    foreach ($moyens as $moyen) {
        Parametre::set('moyen_'.$moyen, $_POST[$moyen]);
    }
    redirectOk();
}
else
    redirectOk('', 'Action invalide');