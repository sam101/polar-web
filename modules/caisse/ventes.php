<?php
$titrePage = 'Gestion des ventes';

require_once('inc/header.php');
$ventes = Vente::select('Vente.*, SUM(Vente.PrixFacture) as MontantFacture, Utilisateur.Nom, Utilisateur.Prenom')
    ->join('Utilisateur', 'Utilisateur.ID = Vente.Permanencier')
    ->groupBy('Vente.IDVente')
    ->order('IDVente DESC');

?>
<h1>Les ventes du jour</h1>
<table class="table table-bordered table-striped table-condensed">
<tr><th>Date</th><th>Montant</th><th>Effectuée par</th><th>Moyen paiement</th><th>Supprimer vente</th></tr>
<?php foreach($ventes as $vente)
{
    echo '<tr>
		<td>'.$vente->Date.'</td>
		<td>'.round($vente->MontantFacture,2).'€</td>
		<td>'.$vente->Prenom.' '.$vente->Nom.'</td>
		<td>'.$vente->MoyenPaiement.'</td>
		<td><a href="'.urlControl('SupprimerVente='.$vente->IDVente).'" title="Cliquez pour supprimer cette vente !"><img src="'.$racine.'styles/'.$design.'/icones/croix.png" alt="-" /></a></td>
		</tr>';
}
?>
</table>
<?php
require_once('inc/footer.php');
?>
