<?php
$titrePage = 'Editer le journal des ventes';
use_jquery_ui();
addFooter('
	<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker({ dateFormat: \'yy-mm-dd\' });
	});
	</script>
	<script type="text/javascript">
	$(function() {
		$("#datepickerfin").datepicker({ dateFormat: \'yy-mm-dd\' });
	});
	</script>');
require_once('inc/header.php');
?>
			<h1>Editer le journal des ventes</h1>
			<p><form name="formulaire" method="post" style="margin-top:10px;" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control' ?>">
			Extraire à partir du <input size="15" type="text" class="in-text" name="ep-datedebut" id="datepicker"> (format JJ/MM/YYYY) <br />
			Jusqu'au <input size="15" type="text" class="in-text" name="ep-datefin" id="datepickerfin"> (format JJ/MM/YYYY) <br />
			<input type="checkbox" name="today" /> Inclure les ventes du jour (ventes où la caisse n'a pas encore été faite)<br />
			<input type="submit" name="ExtraireJournal" value="Extraire !" class="btn" />
			</form></p>
<?php
require_once('inc/footer.php');
?>
