<?php
if(isset($_POST['row_id'])){
    $res = Caisse::update()
        ->set_value('Commentaire', htmlspecialchars($_POST['value']))
        ->where('ID = ?', intval($_POST['row_id']))
        ->execute();
    if ($res->rowCount() == 1)
        echo stripslashes($_POST['value']);
}
else if (isset($_GET['export'], $_POST['ep-datedebut'], $_POST['ep-datefin'])) {
	$nom = 'Documents/ErreursCaisse-'.date('dmY').'-'.date('His').'.csv';

    $caisses = Caisse::select('Caisse.*, MouvementCoffre.Date')
        ->join('MouvementCoffre', 'Caisse.Mouvement = MouvementCoffre.ID')
        ->where('MouvementCoffre.Date >= ? AND MouvementCoffre.Date <= ?',
                $_POST['ep-datedebut'], $_POST['ep-datefin']);

	$liste = array(array('Date',
                         'Erreur Especes',
                         'Erreur CB',
                         'Erreur Moneo',
                         'Erreur Payutc',
                         'Commentaire'
                         ));

    foreach($caisses as $caisse) {
		$liste[] = array($caisse->Date,
                         str_replace(".", ",", $caisse->getErreurEspeces()),
                         str_replace(".", ",", $caisse->getErreurCB()),
                         str_replace(".", ",", $caisse->getErreurMoneo()),
                         str_replace(".", ",", $caisse->getErreurPayutc()),
                         unhtmlentities($caisse->Commentaire)
                         );
	}

	$fichier = fopen($nom, "w");
	foreach($liste as $ligne){
		foreach($ligne as $item){
			fwrite($fichier, '"'.str_replace('"', '""', $item).'";');
		}
		fwrite($fichier, "\n");
	}
	fclose($fichier);
	header("Location: ".urlTo($nom));
}
?>
