<?php
$titrePage = 'Suivi des erreurs de de caisse';
use_datatables();
use_jquery_ui();

$caisses = Caisse::select('Caisse.*, MouvementCoffre.Date')
    ->join('MouvementCoffre', 'Caisse.Mouvement = MouvementCoffre.ID')
    ->order('Caisse.ID DESC');

require_once('inc/header.php');
?>
<h1>Suivi des erreurs de caisse</h1>
<p>Cet outil comptabilise les erreurs depuis le 1er novembre 2013. Pour les caisses faites avant cette date vous pouvez utiliser <a href="<?php echo urlTo('caisse', 'erreurs_old') ?>">l'ancien outil de gestion</a>.</p>
<div>
  <form action="<?php echo urlControl('export') ?>" method="POST">
  Extraire du <input size="15" type="text" class="in-text" name="ep-datedebut" id="datepicker">
  au <input size="15" type="text" class="in-text" name="ep-datefin" id="datepickerfin"> &nbsp;
  <input type="submit" name="ExtraireJournal" value="Exporter" class="btn" />
</form>
</div>

<table id="tableau_erreurs" class="datatables table table-bordered table-striped">
  <thead>
    <tr>
      <th>Date</th>
      <th>Erreur Espèces</th>
      <th>Erreur CB</th>
      <th>Erreur Monéo</th>
      <th>Erreur Payutc</th>
      <th>Commentaire</th>
    </tr>
  </thead>
  <tbody>
<?php foreach($caisses as $caisse): ?>
	<tr id="<?php echo $caisse->get_id() ?>">
      <td><?php echo $caisse->Date ?></td>
      <td><?php echo formatPrix($caisse->getErreurEspeces()) ?></td>
      <td><?php echo formatPrix($caisse->getErreurCB()) ?></td>
      <td><?php echo formatPrix($caisse->getErreurMoneo()) ?></td>
      <td><?php echo formatPrix($caisse->getErreurPayutc()) ?></td>
      <td class="editable"><?php echo $caisse->Commentaire ?></td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>

<?php
addFooter('
	<script type="text/javascript" src="'.$racine.'js/jquery.jeditable.min.js"></script>
	<script>
	$(document).ready(function() {
            $("#tableau_erreurs tbody td.editable").editable( \''.$racine.$module.'/'.$section.'_control\', {
                    "callback": function( sValue, y ) {
                        var aPos = theDataTable.fnGetPosition( this );
                        theDataTable.fnUpdate( sValue, aPos[0], aPos[1] );
                    },
                        "submitdata": function ( value, settings ) {
                            return { "row_id": this.parentNode.getAttribute(\'id\') };
                        },
                            "height": "14px"
                                });
            $("#datepickerfin").datepicker({ dateFormat: \'yy-mm-dd\' });
            $("#datepicker").datepicker({ dateFormat: \'yy-mm-dd\' });
        });
</script>');
require_once('inc/footer.php');
