<?php
if(isset($_POST['AjouterMouvement'])){
    if (!isset($_POST['pieces'], $_POST['billets'], $_POST['commentaire']))
        redirectWithErrors('', 'Champ(s) manquant(s)');

    $mouvement = new MouvementCoffre();
    $mouvement->User = $user;
    $mouvement->Date = raw('NOW()');
	$mouvement->Pieces = (float)($_POST['pieces']);
	$mouvement->Billets = (float)($_POST['billets']);
	$mouvement->Commentaire = htmlspecialchars($_POST['commentaire']);
    $mouvement->Echange = (isset($_POST['transfertcaisse']) && $_POST['transfertcaisse'] == "oui") ? 'o' : 'n';

    $mouvement->save();
    redirectOk();
}
else
	header('Location: '.urlTo());
?>
