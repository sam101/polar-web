<?php
$titrePage = 'Gestion des IP autorisées';
use_jquery_ui();
addFooter('<script>
	  function ouvrirPopup(){
		$("#dialog").dialog("open");
		$("#IP").focus();
	  }
	  $(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
	  });
	  </script>');
require_once('inc/header.php');
?>
<h1>Gérer les adresses IP autorisées à encaisser des ventes</h1>
<input type=submit value="Ajouter une IP !" class="btn" onClick="ouvrirPopup();" />
<?php
	$sql='SELECT * FROM polar_caisse_ip';
	$req=query($sql);
	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<tr><th>Description</th><th>Adresse IP</th><th>Supprimer</th></tr>';
	while($data=mysql_fetch_array($req))
		echo '<tr><td>'.$data['Type'].'</td>
		<td>'.$data['ip'].'</td>
		<td><img title="Supprimez cette fabrication !" onclick="if(confirm(\'Voulez-vous vraiment SUPPRIMER définitivement cette adresse IP ?\')) location.href= \'',$racine,$module,'/',$section.'_control?SupprimerIP=',$data['ip'],'\';" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/croix.png" alt="-" /></td></tr>';
	echo '</table>';
?>
<br />
<div id="dialog" title="Ajouter une adresse IP">
<form name="formulaire" method="post" style="margin-top:10px;" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
  <table>
	<tr>
	  <td style="font-size:12px;">Description :</td>
	  <td><input type="text" id="IP" class="in-text" name="ep-description" /></td>
	</tr>
	<tr>
	  <td style="font-size:12px;">Adresse :</td>
	  <td><input type="text" class="in-text" name="ep-ip"/></td>
	</tr>
	<tr>
	  <td colspan="2" style="text-align:right;"><input type="submit" name="AjouterIP" value="Ajouter l'adresse !" class="btn" /></td>
	</tr>
  </table>
</form>
</div>
<?php
require_once('inc/footer.php');
?>
