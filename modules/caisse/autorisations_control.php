<?php
if (isset($_GET['delete']) && is_numeric($_GET['delete'])) {
    $id = intval($_GET['delete']);
    if (AutorisationCaisse::desactiver($id)) {
        Log::info('Autorisation caisse '.$id.' désactivée');
        $_SESSION['success'] = '<b>Succès</b> Autorisation Supprimée !';
    }
} elseif (isset($_GET['add'])) {
    if (isset($_GET['nom'])) {
        $token = uniqid('', true);
        $aut = new AutorisationCaisse(array('Token' => $token,
                                            'Nom' => htmlspecialchars($_GET['nom']),
                                            'User' => $user,
                                            'DateCreation' => raw('NOW()')));
        $aut->save();

        // Le cookie est valide jusqu'en 2038 !
        $forever = 2000000000;

        // On veut que le cookie soit aussi accessible depuis la page d'accueil
        $parts = explode('/', $CONF['polar_url']);
        $path = '/' . implode('/', array_slice($parts, 3)); // on dégage le http(s)://domain/

        setcookie('private_browser_token', $token, $forever, $path);

        Log::info('Autorisation caisse '.$aut.' crée');
        $_SESSION['success'] = '<b>Succès</b> Autorisation ajoutée pour le navigateur actuel !';
    }
}
redirectOk();