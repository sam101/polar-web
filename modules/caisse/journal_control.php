<?php
if(isset($_POST['ExtraireJournal'])){
	$datedebut = mysqlSecureText($_POST['ep-datedebut']);
	$datefin = mysqlSecureText($_POST['ep-datefin']);
	$nom = 'Documents/JournalDesVentes-'.date('dmY').'-'.date('His').'.csv';

	$req = query("SELECT pcvg.ID, pcvg.IDVente, pcvg.Date, pcs.Secteur, pca.Nom,
		pcvg.Quantite, pcvg.Tarif, ROUND(pca.PrixVenteAsso, 2) AS PrixVenteAsso, ROUND(pca.PrixVente, 2) AS PrixVente, pcvg.PrixFacture, pca.TVA, pcvg.MontantTVA, pcvg.MoyenPaiement
		FROM polar_caisse_ventes_global pcvg
		INNER JOIN polar_caisse_articles pca ON pca.ID = pcvg.Article
		INNER JOIN polar_caisse_secteurs pcs ON pcs.Code = pca.Secteur
		AND DATE(pcvg.Date) >= '$datedebut'
		AND DATE(pcvg.Date) <= '$datefin'
		ORDER BY pcvg.ID ASC");

	$liste = array();
	$liste[] = array(
		'ID',
		'IDVente',
		'Date',
		'Secteur',
		'Designation',
		'Quantite',
		'PrixArticle',
		'PrixFacture',
		'TauxTVA',
		'MontantTVA',
		'MoyenPaiement'
	);

	while($data=mysql_fetch_assoc($req)){
		if($data['Tarif'] == 'asso' && !empty($data['PrixVenteAsso']) && $data['PrixVenteAsso'] != "0.00")
			$prixVente = $data['PrixVenteAsso'];
		else
			$prixVente = $data['PrixVente'];

		$liste[] = array(
			$data['ID'],
			$data['IDVente'],
			$data['Date'],
			$data['Secteur'],
			stripAccents(html_entity_decode($data['Nom'])),
			$data['Quantite'],
			str_replace(".", ",", $prixVente),
			str_replace(".", ",", $data['PrixFacture']),
			str_replace(".", ",", $data['TVA']),
			str_replace(".", ",", $data["MontantTVA"]),
			$data['MoyenPaiement']
		);
	}

	if(isset($_POST['today'])){
	  $req = query("SELECT pcvg.ID, pcvg.IDVente, pcvg.Date, pcs.Secteur, pca.Nom,
  		pcvg.Quantite, pcvg.Tarif, ROUND(pca.PrixVenteAsso, 2) AS PrixVenteAsso, ROUND(pca.PrixVente, 2) AS PrixVente, pcvg.PrixFacture, pca.TVA, pcvg.MontantTVA, pcvg.MoyenPaiement
  		FROM polar_caisse_ventes pcvg
  		INNER JOIN polar_caisse_articles pca ON pca.ID = pcvg.Article
  		INNER JOIN polar_caisse_secteurs pcs ON pcs.Code = pca.Secteur
  		AND DATE(pcvg.Date) >= '$datedebut'
  		AND DATE(pcvg.Date) <= '$datefin'
  		ORDER BY pcvg.ID ASC");

  	while($data=mysql_fetch_assoc($req)){
  		if($data['Tarif'] == 'asso' && !empty($data['PrixVenteAsso']) && $data['PrixVenteAsso'] != "0.00")
  			$prixVente = $data['PrixVenteAsso'];
  		else
  			$prixVente = $data['PrixVente'];

  		$liste[] = array(
  			$data['ID'],
  			$data['IDVente'],
  			$data['Date'],
  			$data['Secteur'],
  			stripAccents(html_entity_decode($data['Nom'])),
  			$data['Quantite'],
  			str_replace(".", ",", $prixVente),
  			str_replace(".", ",", $data['PrixFacture']),
  			str_replace(".", ",", $data['TVA']),
  			str_replace(".", ",", $data["MontantTVA"]),
  			$data['MoyenPaiement']
  		);
  	}
	}

	$fichier = fopen($nom, "w");
	foreach($liste as $ligne){
		foreach($ligne as $item){
			fwrite($fichier, '"'.str_replace('"', '""', $item).'";');
		}
		fwrite($fichier, "\n");
	}
	fclose($fichier);
	header("Location: $racine$nom");
}
?>
