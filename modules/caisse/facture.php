<?php require('inc/header.php'); ?>
<h1>Éditer une facture à une vente du jour</h1>
<table class="datatables table table-bordered table-striped table-condensed">
<tr><th>Date/heure</th><th>Montant</th><th>Effectuée par</th><th>Moyen Paiement</th><th>Voir facture</th></tr>
<?php
	$sql='SELECT polar_caisse_ventes.IDVente, SUM(PrixFacture), Date, Nom, Prenom, IDVente, MoyenPaiement FROM polar_caisse_ventes INNER JOIN polar_utilisateurs ON polar_caisse_ventes.Permanencier=polar_utilisateurs.ID GROUP BY polar_caisse_ventes.IDVente ORDER BY IDVente DESC';
	$req=query($sql);
	while($data=mysql_fetch_assoc($req))
	{
		echo '<tr>
		<td>'.$data['Date'].'</td>
		<td>'.round($data['SUM(PrixFacture)'],2).'€</td>
		<td>'.$data['Prenom'].' '.$data['Nom'].'</td>
		<td>'.$data['MoyenPaiement'].'</td>
		<td>';

		if(!in_array($data['MoyenPaiement'], array('assos', 'interne')))
			echo '<img title="Cliquez pour voir la facture ! " onclick="var test=prompt(\'Entrez le nom à afficher sur la facture ?\');window.open(\'',$racine,'caisse/facture_control?id=',$data['IDVente'],'&nom=\'+test);" style="cursor:pointer;" src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" />';

		echo '</td>
		</tr>';
	}
?>
</table>
<?php require('inc/footer.php'); ?>

