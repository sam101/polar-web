<?php
// EnVente : l'utilisateur peut choisir ou non si l'article est disponible
// Actif : sert à distinguer les articles supprimés/mis à jour de ceux en vente

$gesarticle = Payutc::loadService('GESARTICLE', 'manager');

function deleteIfExists($gesarticle, $liste, $id) {
    if ($id == NULL)
        return;

    if (isset($liste[$id]))
        deleteArticle($id, $gesarticle);
}

if(isset($_GET['DesactiverArticle'])){
    Article::update()->set_value('EnVente', 0)
        ->where('ID = ?', intval($_GET['DesactiverArticle']))
        ->execute();
}
else if(isset($_GET['ReactiverArticle'])){
    Article::update()->set_value('EnVente', 1)
        ->where('ID = ?', intval($_GET['ReactiverArticle']))
        ->execute();
}
else if(isset($_GET['SupprimerArticle'])){
    Article::update()->set_value('Actif', 0)
        ->where('ID = ?', intval($_GET['SupprimerArticle']))
        ->execute();

    // on supprime l'ancien article de payutc
    $p = ArticlePayutc::select()
        ->where('Article = ?', intval($_GET['SupprimerArticle']))
        ->getOne();
    if ($p instanceof ArticlePayutc) {
        PayutcTools::deleteArticle($p, $gesarticle);
    }

    // Suppression de l'image
    $Article = Article::getById(intval($_GET['SupprimerArticle']));
    if ($Article->Photo != '' && $Article->Photo != 'default.png' && file_exists(__DIR__.'/../../upload/articles/'.$Article->Photo))
        unlink(__DIR__.'/../../upload/articles/'.$Article->Photo);

}
else if(isset($_GET['EditerArticle'])){
    $article = new Article(array('Actif' => True,
                                 'Date' => raw('NOW()'),
                                 'Auteur' => $user,
                                 'CodeJS' => '',
                                 'Photo' => ''));

	$article->CodeCaisse = intval($_POST['code']);
	$article->EnVente = ($_POST['actif'] == 'on');
	$article->Nom = $_POST['nom'];
	$article->Secteur = intval($_POST['secteur']);
	$article->PrixAchat = (float)($_POST['achat']);
	$article->PrixVente = (float)($_POST['vente']);
	$article->PrixVenteAsso = isset($_POST['asso']) ? (float)($_POST['asso']) : 0;
	$article->TVA = (float)($_POST['tva']);
	$article->Stock = !empty($_POST['stock']) ? intval($_POST['stock']) : NULL;
    $article->StockInitial = $article->Stock;
	$article->SeuilAlerte = !empty($_POST['seuil']) ? intval($_POST['seuil']) : NULL;
	$article->EAN13 = !empty($_POST['EAN13']) ? $_POST['EAN13'] : NULL;
	$article->Remise1 = (float)(intval($_POST['remise1'])/100);
	$article->Palier1 = intval($_POST['palier1']);
	$article->Remise2 = (float)(intval($_POST['remise2'])/100);
	$article->Palier2 = intval($_POST['palier2']);
    $article->Fournisseur = $_POST['fournisseur'];
    $article->Photo = $_POST['photo'];

	// On désactive l'ancien article si besoin
	if(intval($_GET['EditerArticle']) > 0){
        Article::update()->set_value('Actif', 0)
            ->where('ID = ?', intval($_GET['EditerArticle']))
            ->execute();

        if ($CONF['use_payutc']) {
            // on supprime l'ancien article de payutc
            $p = ArticlePayutc::select()
                ->where('Article = ?', intval($_GET['EditerArticle']))
                ->getOne();
            if ($p instanceof ArticlePayutc) {
                PayutcTools::deleteArticle($p);
            }
        }
	}

    $article->save();
    PayutcTools::addArticle($article, $gesarticle);
}


// Modification de l'image d'un article
else if(isset($_GET['EditerImage'])){
    $maxsize = 2097152 ;
    if ($_FILES['image']['error'] > 0)
        ajouterErreur('Erreur lors du transfert !') ;
    if ($_FILES['image']['size'] > $maxsize)
        ajouterErreur('Le fichier est trop volumineux ! ') ;
    $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
    $extension_upload = strtolower(substr(strrchr($_FILES['image']['name'], '.') ,1));
    if (!in_array($extension_upload,$extensions_valides))
        ajouterErreur('Extension invalide ! ') ;
        
    if (hasErreurs()) {
        redirectWithErrors('EditerImage='.intval($_GET['EditerImage']));
    }
    else
    {
        $nom = strtolower(preg_replace('#[^0-9a-z]+#i', '-', $_POST['nom']));

        while(strpos($nom, '--') !== false)
        {
            $nom = str_replace('--', '-', $nom);
        }   
        $nom = trim($nom, '-');

        $dir = __DIR__.'/../../upload/articles/';

        if ($_POST['nom_actuel'] != '' && preg_match('#^[a-z0-9_-]+\.[a-z]+$#', $_POST['nom_actuel']) && file_exists($dir.$_POST['nom_actuel']) && $_POST['nom_actuel'] != 'default.png')
            unlink($dir.$_POST['nom_actuel']);

        move_uploaded_file($_FILES['image']['tmp_name'], $dir.$nom.'.'.$extension_upload);
        
        // Vérification du mine-type
        $mimes_images = array('image/gif', 'image/jpeg', 'image/png');
        $info = new finfo(FILEINFO_MIME_TYPE);
        $type = $info->file($dir.$nom.'.'.$extension_upload);

        // Si on ne trouve pas le 
        if (!in_array($type, $mimes_images)) {
             unlink($dir.$nom.'.'.$extension_upload && $nom != 'default');
             ajouterErreur('Le type de fichier est incorrect, veuillez réessayer !)');
        }

        // On récupère la taille de l'image
        $width = 0;
        $height = 0;

        list($width, $height) = getimagesize($dir.$nom.'.'.$extension_upload);

        // Si l'image est trop grande on la redimensionne
        if ($width > 200 || $height > 180)
        {
            $rImage = new Image($dir.$nom.'.'.$extension_upload);
            $rImage->resize(180, 200, true);
            
            // Suppression de l'image si on change l'extension pour éviter les doublons
            if (file_exists($dir.$nom.'.'.$extension_upload) && $nom != 'default')
                unlink($dir.$nom.'.'.$extension_upload);

            $rImage->saveToPng($dir, $nom);
        }

        // Enregistrement en db
        Article::update()->set_value('Photo', $nom.'.png')
            ->where('ID = ?', intval($_GET['EditerImage']))
            ->execute();
    }
}
else if (isset($_GET['payutcSyncCategories'])) {
    if (!$CONF['use_payutc'])
        die();

    Secteur::load();
    foreach(Secteur::getSecteurs() as $secteur) {
        if ($secteur->Payutc == NULL) {
            try {
                $r = $gesarticle->setCategory(array(
                                         'name' => $secteur->Secteur,
                                         'parent_id' => NULL,
                                         'fun_id' => $CONF['payutc_fundation'])
                                              );
                $secteur->Payutc = $r->success; //id de la catégorie
                $secteur->save();
            } catch(\JsonClient\JsonException $e) {
                
            }
        }
    }
}
else if (isset($_GET['payutcSync'])) {
    if (!$CONF['use_payutc'])
        die();

    // On synchronise tous les articles que de la base vers payutc
    $articles = Article::select('Article.*, ArticlePayutc.IDPayutc AS Payutc')
        ->leftJoin('ArticlePayutc', 'ArticlePayutc.Article = Article.ID')
        ->where('Article.Actif = 1 AND Article.EnVente = 1');

    $particles = $gesarticle->getProducts(array('fun_ids' =>
                                                '['.$CONF['payutc_fundation'].']'
                                                ));
    $pbyid = array();
    foreach($particles as $p) {
        $pbyid[$p->id] = $p;
    }

    foreach($articles as $article) {
        // si l'article est non supprimé et non présent dans payutc on l'ajout
        // si il est désactivé et présent dans payutc on le désactive
        // si il est supprimé on ne fait rien

        if ($article->Actif && $article->EnVente) {
            if (!$article->Payutc) { // il n'est pas dans payutc !
                PayutcTools::addArticle($article, $gesarticle);
            }
        } else {
            // on supprime les articles correspondant à chaque tarif supprimé
            deleteIfExists($gesarticle, $pbyid, $article->Payutc);
        }
    }
}
redirectOk();
?>
