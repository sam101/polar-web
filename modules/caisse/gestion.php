<?php
require('inc/header.php');
$moyens = array(array('id'=>'moneo', 'nom'=>'Monéo', 'message'=>Parametre::get('moyen_moneo')),
                array('id'=>'cb', 'nom'=>'Carte Bancaire', 'message'=>Parametre::get('moyen_cb')),
                array('id'=>'cheque', 'nom'=>'Chèque', 'message'=>Parametre::get('moyen_cheque')),
                array('id'=>'especes', 'nom'=>'Espèces', 'message'=>Parametre::get('moyen_especes')),
                array('id'=>'payutc', 'nom'=>'PayUTC', 'message'=>Parametre::get('moyen_payutc')));

function formatMoyen($moyen) {
    return '<div class="control-group">
<label class="control-label" for="'.$moyen['id'].'">'.$moyen['nom'].'</label>
<div class="controls">
<textarea name="'.$moyen['id'].'" id="'.$moyen['id'].'">'.$moyen['message'].'</textarea>
</div>
</div>';
};
?>
<h1>Paramètres de la caisse</h1>
<p class="well">Si un moyen de paiement est indisponible il suffit de mettre un message dans la case correspondante pour le bloquer.</p>
<form method="POST" action="<?php echo urlControl() ?>" class="form-horizontal">
<?php foreach($moyens as $moyen) echo formatMoyen($moyen) ?>
<div class="control-group">
  <div class="controls">
    <input type="submit" class="btn btn-primary" value="Enregistrer"/>
  </div>
</div>
</form>

<?php
require('inc/footer.php');
?>