<?php
$fournisseurs = Fournisseur::select()->where('DateSupprime IS NULL');
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $fournisseur = Fournisseur::select()->where('ID = ? AND DateSupprime IS NULL', intval($_GET['id']))->getOne();
    $contacts = $fournisseur->getContacts()->execute(true);
}
else
    $fournisseur = null;
require_once 'inc/header.php';
?>
<div class="row-fluid">
  <div class="span2">
    <ul class="nav nav-list">
      <li class="nav-header">Fournisseurs</li>
      <?php foreach($fournisseurs as $fournisseur_): ?>
      <li><a href="<?php echo urlSection('id='.$fournisseur_ ->get_id())?>"><?php echo $fournisseur_->Nom ?></a></li>
      <?php endforeach; ?>
      <li class="nav-header">Gestion</li>
      <li><a href="#" id="btn-add-fournisseur"><i class="icon-plus"></i> Nouveau fournisseur</a></li>
    </ul>
  </div>
  <div class="span10">
    <?php afficherErreurs() ?>
    <div id="details-fournisseur">
      <?php if(is_null($fournisseur)): ?>
      <p class="text-center">
        Aucun fournisseur sélectionné !<br/>
        Vous pouvez choisir un fournisseur dans la liste de gauche.
      </p>
      <?php else: ?>

      <a href="<?php echo urlControl('action=delete&id='.$fournisseur) ?>" class="pull-right btn btn-danger">Supprimer</a>
      <h2><?php echo $fournisseur->Nom ?></h2>

      <?php // --------- Liste des contacts ---- ?>
      <div class="row-fluid">
        <div class="span2">
          <p><b>Contacts</b>&nbsp;<a href="#" class="btn" id="btn-add-contact">Nouveau</a></p>
        </div>
        <div class="span10">
          <form action="<?php echo urlControl('action=add-contact') ?>" method="POST">
            <table class="table table-bordered table-striped table-hover table-condensed">
              <?php foreach($contacts as $contact): ?>
              <tr>
                <td><?php echo $contact->Nom ?></td>
                <td><?php echo $contact->Info ?></td>
                <td><a href="<?php echo urlControl('action=delete-contact&id='.$contact) ?>" class="btn btn-danger"><i class="icon-trash"></i></a></td>
              </tr>
              <?php endforeach; ?>
              <?php // ----- formulaire d'ajout de contact ?>
              <tr id="tr-add-contact" class="hidden">
                <input type="hidden" name="fournisseur" value="<?php echo $fournisseur ?>"/>
                <td><input type="text" placeholder="Nom et/ou Type du Contact" name="nom" /></td>
                <td><input type="text" placeholder="Numéro / mail / ..." name="info" /></td>
                <td><button type="submit" class="btn btn-primary"><i class="icon-save"></i></button></td>
              <tr>
            </table>
          </form>
        </div>
      </div>

      <a href="#" id="btn-add-ticket" class="pull-right btn btn-success">Nouveau ticket</a>
      <h3>Tickets</h3>
      <?php // --------- Nouveau ticket ---- ?>
      <div id="add-ticket" class="hidden well">
        <form action="<?php echo urlControl('action=add-ticket') ?>" method="POST" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" name="fournisseur" value="<?php echo $fournisseur ?>"/>
          <div class="control-group">
            <label class="control-label" for="input-titre-ticket">Titre</label>
            <div class="controls">
              <input type="text" id="input-titre-ticket" name="titre">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input-contenu-ticket">Contenu</label>
            <div class="controls">
              <textarea id="input-contenu-ticket" name="contenu" class="input-xxlarge"></textarea>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input-file-ticket">Fichier lié</label>
            <div class="controls">
              <input type="file" id="input-file-ticket" name="fichier">
            </div>
          </div>
          <div class="control-group hidden" id="control-nom-fichier">
            <label class="control-label" for="input-nom-fichier-ticket">Nom pour le fichier</label>
            <div class="controls">
              <input type="text" id="input-nom-fichier-ticket" name="nom-fichier">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input-contact-ticket">Contact utilisé</label>
            <div class="controls">
              <select id="input-contact-ticket" name="contact">
                <option value="">--</option>
                <?php foreach($contacts as $contact): ?>
                <option value="<?php echo $contact ?>"><?php echo $contact->Nom ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <input type="submit" class="btn btn-success" value="Enregistrer"/>
              <a href="#" id="btn-cancel-ticket" class="btn">Annuler</a>
            </div>
          </div>
        </form>
      </div>

      <?php // --------- Liste des tickets ---- ?>
      <?php foreach($fournisseur->getTickets() as $ticket): ?>
      <div class="well well-small">
        <div class="pull-right">
          <?php if (!is_null($ticket->NomFichier)): ?>
          <a target="_blank" title="Afficher le fichier" class="label label-success" href="<?php echo urlTo('upload/fournisseurs/'.$ticket->Fichier) ?>">Fichier : <?php echo $ticket->NomFichier ?></a>
          <?php endif; ?>
          <?php if (!is_null($ticket->Contact)): ?>
          <span class="label label-info">Contact : <?php echo $ticket->Contact->Nom ?></span>
          <?php endif; ?>
        </div>
        <h4><?php echo $ticket->Nom ?> <small>Le <?php echo $ticket->Date ?> par <?php echo $ticket->User->nomComplet() ?></small></h4>
        <?php echo nl2br($ticket->Contenu) ?>
      </div>
      <?php endforeach; ?>
      
      <?php endif; ?>
    </div>
    <div id="add-fournisseur" class="hidden">
      <h2>Ajouter un fournisseur</h2>
      <form action="<?php echo urlControl('action=add-fournisseur') ?>" method="POST" class="form-horizontal">
      <div class="control-group">
        <label class="control-label" for="input-nom-fournisseur">Nom</label>
        <div class="controls">
          <input type="text" id="input-nom-fournisseur" name="nom">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="input-description-fournisseur">Description</label>
        <div class="controls">
          <textarea id="input-description-fournisseur" name="description" class="input-xxlarge" rows="5"></textarea>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" class="btn btn-success" value="Enregistrer" />
          <a href="#" id="btn-cancel-ticket" class="btn">Annuler</a>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
<?php
addFooter('<script src="'.urlTo('js/modules/fournisseurs.js').'"></script>');
require_once 'inc/footer.php';