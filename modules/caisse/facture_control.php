<?php
$vente = intval($_GET['id']);
$nom = $_GET['nom'];

if(isset($_GET['global']))
	$table = "polar_caisse_ventes_global";
else
	$table = "polar_caisse_ventes";

$req = query("SELECT UNIX_TIMESTAMP(Date) AS Date, MoyenPaiement FROM `$table`
	WHERE IDVente=$vente LIMIT 1");
$donnees = mysql_fetch_assoc($req);

$numfact = 'C'.$vente.'.'.date('dmY', $donnees['Date']);
$moyenPaiement = $donnees['MoyenPaiement'];

if (in_array($moyenPaiement, array('assos', 'interne')))
    header("Location: $racine$module/$section");

require_once('inc/tcpdf.php');

$pdf = new PolarPDF("FACTURE");

$pdf->SetFontSize(12);

$output = '<p align="right">'."Facture N°$numfact";
$output .= ' du '.date("d/m/Y", $donnees['Date']).'<br>';
$output .= "Client : $nom</p>";

$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
$pdf->Ln(10);

$pdf->SetFontSize(10);
$output = '<table border="1" cellpadding="4">
<tr>
	<th width="355"><b>Désignation</b></th>
	<th width="70"><b>PU TTC</b></th>
	<th width="45"><b>Qté</b></th>
	<th width="45"><b>% Re<br/>mise</b></th>
	<th width="55"><b>TVA</b></th>
	<th width="75"><b>Total TTC</b></th>
</tr>';

$req = query("SELECT pca.Nom, (CASE Tarif WHEN 'asso' THEN PrixVenteAsso ELSE PrixVente END) AS PrixVente, PrixFacture, pca.TVA, pcv.Quantite, Client, GROUP_CONCAT(Detail SEPARATOR ', ') AS Details, pc.ID AS IDCommande, pca.Palier1, pca.Remise1, pca.Palier2, pca.Remise2, pcv.Tarif
	FROM `$table` pcv
	INNER JOIN polar_caisse_articles pca ON pca.ID=pcv.Article
	LEFT JOIN polar_commandes_types pct ON pct.CodeProduit = pca.CodeCaisse
	LEFT JOIN polar_commandes pc ON (pc.Type = pct.ID AND pcv.IDVente = pc.IDVente)
	LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
	WHERE pcv.IDVente=$vente
	GROUP BY pcc.IDCommande, pca.Nom, PrixFacture, pca.TVA, pcv.Quantite, Client");

// Données
while($data = mysql_fetch_assoc($req)){
	$output .= '<tr>';
	$output .= '<td width="355">'.$data['Nom'];

	if(!empty($data['IDCommande'])) // on a un numéro de commande
	  $output .= ' n°'.$data['IDCommande'];

	if(!empty($data['Client'])) // on a une info sur le client
		$output .= '<br/>&nbsp;&nbsp;'.stripslashes($data['Client']);

	if (!empty($data['Details'])) // on a le détail d'une commande
	  $output .= '<br/>&nbsp;&nbsp;'.stripslashes($data['Details']);

	$output .= '</td>';

	$output .= '<td width="70" align="right">'.number_format($data['PrixVente'], 2, ',', ' ').' &euro;</td>';
	$output .= '<td width="45" align="right">'.$data['Quantite'].'</td>';
	$output .= '<td width="45" align="right">';

  // Réduction sur les quantités
  $rem = 0;
  if ($data['Quantite'] > $data['Palier1'] && $data['Tarif'] != 'asso') {
    if ($data['Quantite'] > $data['Palier2']) {
      $rem = $data['Remise2'];
    } else {
      $rem = $data['Remise1'];
    }
  }
  if ($rem == 0) {
    $output .= '/';
  } else {
    $output .= $rem*100 . '%';
  }

  $output .= '</td>';
	$output .= '<td width="55" align="right">'.number_format($data['TVA']*100, 1, ',', ' ').'%</td>';
	$output .= '<td width="75" align="right">'.number_format($data['PrixFacture'], 2, ',', ' ').' &euro;</td>';
	$output .= '</tr>';
}

$output .= '</table>';

$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
$pdf->Ln(10);

$req = query("SELECT SUM(Prixfacture) AS MontantTTC, SUM(Prixfacture/(1 + TVA)) AS MontantHT
	FROM $table pcv
	INNER JOIN polar_caisse_articles pca ON pca.ID=pcv.Article
	WHERE IDVente = $vente");
$data = mysql_fetch_assoc($req);

$pdf->Image('upload/signature_prez.jpg', 45, '', 60, '', 'JPG', '', '', true, 150);

$output = '<p align="right"><b>Montant HT : '.number_format($data['MontantHT'], 2, ',', ' ').' €<br>';
$output .= 'Montant TVA : '.number_format($data['MontantTTC']-$data['MontantHT'], 2, ',', ' ').' €<br><br>';
$output .= 'Montant TTC : '.number_format($data['MontantTTC'], 2, ',', ' ').' €<br><br>';
$output .= "Facture réglée en : $moyenPaiement</b></p>";

$pdf->SetFontSize(12);
$pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');

$file = 'Documents/facture_'.$numfact.'_'.date('HmidmY').'.pdf';
$pdf->Output($file, 'F');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<body style="margin: 0; padding:0;">
<input type="button" onClick="window.close('idfact')" value="Fermer" style="width: 1436px; height: 40px; background: #FF9393;border: 1px solid #FF0000;font-size: 1.4em;color: #000000;margin: 2px;"><br />
<iframe style="width:98%; height:98%;" src="<?php echo $racine.$file; ?>">
</iframe>
</body>
</html>
