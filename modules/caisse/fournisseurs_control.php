<?php
switch($_GET['action']) {
case 'add-fournisseur':
    if (isset($_POST['nom'], $_POST['description'])) {
        $fournisseur = new Fournisseur();
        $fournisseur->Nom = htmlspecialchars($_POST['nom']);
        $fournisseur->Description = htmlspecialchars($_POST['description']);
        $fournisseur->save();

        redirectOk('id='.$fournisseur);
    } else {
        redirectWithErrors('', 'Champ(s) manquant(s).');
    }
    break;
case 'delete':
    if (!(isset($_GET['id']) && is_numeric($_GET['id'])))
        redirectWithErrors('', 'Identifiant de fournisseur invalide ou non précisé');
    $res = Fournisseur::update()
        ->set_value('DateSupprime', raw('NOW()'))
        ->where('DateSupprime IS NULL AND ID = ?', intval($_GET['id']))
        ->execute();
    if ($res->rowCount() == 1)
        redirectOk('', 'Fournisseur supprimé !');
    else
        redirectWithErrors('id='.intval($_GET['id']), 'La fournisseur est déjà supprimé.');
    break;
case 'add-ticket':
    if (isset($_POST['fournisseur'], $_POST['titre'], $_POST['contenu'],
              $_POST['nom-fichier'], $_POST['contact'])) {
        try {
            $fournisseur = Fournisseur::getById(intval($_POST['fournisseur']));
        } catch (UnknownObject $e) {

        }
        if (!is_null($fournisseur->DateSupprime))
            redirectWithErrors('', 'Le fournisseur '.$fournisseur->Nom.' est supprimé');
        
        $ticket = $fournisseur->nouveauTicket();
        $ticket->Nom = htmlspecialchars($_POST['titre']);
        $ticket->Contenu = htmlspecialchars($_POST['contenu']);
        $ticket->Date = raw('NOW()');
        $ticket->User = $user;

        // ajout du contact utilisé
        if (is_numeric($_POST['contact'])) {
            try {
                $ticket->Contact = intval($_POST['contact']);
            } catch (InvalidValue $e) {
                redirectWithErrors('id='.$fournisseur, 'Contact invalide');
            }
        }

        // ajout du fichier
        if ($_POST['nom-fichier'] != '' && isset($_FILES['fichier'])) {
            $destination = rand(0, 999) . '-' . $_FILES['fichier']['name'];
            if(move_uploaded_file($_FILES['fichier']['tmp_name'], 'upload/fournisseurs/' . $destination)) {
                $ticket->NomFichier = htmlspecialchars($_POST['nom-fichier']);
                $ticket->Fichier = $destination;
            }
        }
        $ticket->save();
        redirectOk('id='.$fournisseur);
    } else {
        redirectWithErrors('', 'Champ(s) manquant(s).');
    }
    break;
case 'add-contact':
    if (isset($_POST['fournisseur'], $_POST['nom'], $_POST['info'])) {
        try {
            $fournisseur = Fournisseur::getById(intval($_POST['fournisseur']));
        } catch (UnknownObject $e) {
            redirectWithErrors('', 'Le fournisseur '.intval($_POST['fournisseur']).' n\'existe pas');
        }
        if (!is_null($fournisseur->DateSupprime))
            redirectWithErrors('', 'Le fournisseur '.$fournisseur->Nom.' est supprimé');

        $contact = $fournisseur->nouveauContact();
        $contact->Nom = htmlspecialchars($_POST['nom']);
        $contact->Info = htmlspecialchars($_POST['info']);
        $contact->save();

        redirectOk('id='.$fournisseur);
    } else {
        redirectWithErrors('', 'Champ(s) manquant(s).');
    }
    break;
case 'delete-contact':
    if (!(isset($_GET['id']) && is_numeric($_GET['id'])))
        redirectWithErrors('', 'Identifiant de contact invalide ou non précisé');
    try {
        $contact = FournisseurContact::getById(intval($_GET['id']));
    } catch (UnknownObject $e) {
        redirectWithErrors('', 'Le contact '.intval($_GET['id']).' n\'existe pas');
    }
    if (is_null($contact->DateSupprime)) {
        $contact->DateSupprime = raw('NOW()');
        $contact->save();
        redirectOk('id='.$contact->get_object_id('Fournisseur'));
    } else
        redirectWithErrors('id='.$contact->get_object_id('Fournisseur'), 'Le contact est déjà supprimé.');
    break;
default:
    die('Action inconnu');
}