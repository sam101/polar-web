<?php

addFooter('<script type="text/javascript">
	  $(function() {
        $("#btn-add-transfert").click(function() {
            $("#dialog-add-transfert").modal("show");
            $("#Montant").focus();
	    });
      });
</script>');

$transferts = TransfertPayutc::select()->execute(true);
if (count($transferts)
    && !is_null($last = $transferts[count($transferts) -1 ])) {
    $date = $last->Date;
} else {
    $date = null;
}

if ($CONF['use_payutc']) {
    $stats = Payutc::loadService('STATS', 'manager');
    $payutc_revenue = $stats->getRevenue(array('fun_id' => $CONF['payutc_fundation'],
                                               'start' => $date,
                                               'app_id' => $CONF['payutc_caisse_app_id'])) / 100;
}

require_once 'inc/header.php';
?>
<a href="#" class="pull-right btn btn-primary" id="btn-add-transfert">Ajouter un transfert</a>
<h1>Gestion payutc</h1>
<?php if($CONF['use_payutc']): ?>
<p class="well">Solde en attente de reversement : <?php echo formatPrix($payutc_revenue) ?> €</p>
<?php else: ?>
<p class="alert alert-danger"><b>Dommage !</b> Payutc est désactivé.</p>
<?php endif; ?>
<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>Date</th>
      <th>Montant</th>
      <th>Commentaire</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($transferts as $transfert): ?>
    <tr>
      <td><?php echo htmlspecialchars($transfert->Date) ?></td>
      <td><?php echo formatPrix($transfert->Montant) ?> €</td>
      <td><?php echo htmlspecialchars($transfert->Commentaire) ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<div class="modal hide" id="dialog-add-transfert">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Ajouter un transfert</h3>
  </div>
  <div class="modal-body">
	<form name="formulaire" method="post" style="margin-top:10px;" enctype="multipart/form-data" action="<?php echo urlControl() ?>">
	<table>
      <tr><td>Montant</td><td><input id="Montant" type="number" name="montant" value="<?php echo $payutc_revenue ?>" />€</td></tr>
	<tr><td>Commentaire</td><td><input type="text" name="commentaire"/></td></tr>
	</table>
	<input type="submit" value="Ajouter" class="btn" />
	</form>
  </div>
</div>
<?php
require_once 'inc/footer.php';
?>