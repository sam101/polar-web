<?php
  if(caisseAutorisee()) {
      if ($user->Login == '') {
          $payutcEnabled = false;
      } else {
          $poss = Payutc::loadService('POSS3', 'caisse');
          $payutcEnabled = true;
      }

      addHeaders('
    <link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />
    ');

      $moyens_paiement = json_encode(array(array('id'=>'moneo', 'nom'=>'Monéo', 'message'=>Parametre::get('moyen_moneo')),
                                          array('id'=>'cb', 'nom'=>'Carte Bancaire', 'message'=>Parametre::get('moyen_cb')),
                                          array('id'=>'cheque', 'nom'=>'Chèque', 'message'=>Parametre::get('moyen_cheque')),
                                           array('id'=>'especes', 'nom'=>'Espèces', 'message'=>Parametre::get('moyen_especes')),
                                           array('id'=>'payutc', 'nom'=>'PayUTC', 'message'=>($payutcEnabled ? Parametre::get('moyen_payutc') : "Vous ne pouvez pas vendre par payutc"))));

      $script = <<<EOS
    <script type="text/javascript">
      var racine = "$racine";
    </script>
    <script type="text/javascript" src="${racine}js/jquery.bgiframe.min.js"></script>
    <script type="text/javascript" src="${racine}js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="${racine}js/caisse_tableau.js"></script>
    <script type="text/javascript" src="${racine}js/caisse_validation.js"></script>
    <script type="text/javascript" src="${racine}js/caisse_raccourcis.js"></script>
    <script type="text/javascript" src="${racine}js/caisse_recupdata.js"></script>
    <script type="text/javascript" src="${racine}js/caisse_autocommande.js"></script>
    <script type="text/javascript" src="${racine}js/caisse_real_world.js"></script>
    <script>
      $(function() {
      /*$("#info-vendeur").popover({placement:"right",
                      html: true,
                      trigger:"hover",
                      content:"Passe ta carte étudiant pour te connecter.",
                      title:"Infos vendeur"});*/
      var moyens = $moyens_paiement;
      for(var i in moyens) {
if (moyens[i].message != '') {
$('#'+moyens[i].id).addClass('disabled').popover({placement: 'right',
html: true,
                      trigger:"hover",
title: 'Impossible de payer en '+moyens[i].nom,
content: moyens[i].message});
}
}
      });
    </script>
EOS;
    addFooter($script);

	require('inc/header.php');
?>
<div class="row">
  <div class="span2">
    <a href="#" class="btn btn-danger btn-large bouton-paiement invisible" id="erreur">Erreur</a>
    <a href="#" class="btn btn-info btn-large bouton-paiement" id="especes">Espèces <span class="raccourcis">F8</span></a>
    <a href="#" class="btn btn-info btn-large bouton-paiement" id="cheque">Chèque <span class="raccourcis">F9</span></a>
    <a href="#" class="btn btn-info btn-large bouton-paiement" id="cb">CB <span class="raccourcis">F10</span></a>
    <a href="#" class="btn btn-info btn-large bouton-paiement" id="moneo">Monéo <span class="raccourcis">F7</span></a>
    <a href="#" class="btn btn-info btn-large bouton-paiement" id="payutc">PayUTC <span class="raccourcis">Badger</span></a>
    <label class="checkbox">
      <input type="checkbox" id="btn-facture"> Faire une facture <span class="raccourcis">F3</span>
    </label>
    <hr/>
    <a href="#" class="btn btn-info btn-large bouton-paiement" id="assos">Compte asso <span class="raccourcis">F12</span></a>
  </div>
  <div class="span10">
    <div class="row entete-caisse">
      <div class="span5">
        À la caisse en ce moment<br/>
        <a id="info-vendeur" href="#"><span class="name"><?php echo $user->Prenom . ' ' . $user->Nom ?> </span> <i class="icon icon-info-sign"></i></a>
      </div>
      <div class="span5 total-caisse">
        <span id="total">0.00</span> €
      </div> 
    </div>
    
    <table class="table table-bordered table-striped" id="tableau_caisse">
      <thead>
        <tr>
          <th class="code">Code</th>
          <th class="designation">D&eacute;signation</th>
          <th class="qte">Quantit&eacute;</th>
          <th class="prix">Prix unitaire (&euro;)</th>
          <th class="prix_asso invisible">Prix unitaire (&euro;)</th>
          <th class="remise">Remise</th>
          <th class="sous-total">Sous-total (&euro;)</th>
          <th class="sous-total_asso invisible">Sous-total (&euro;)</th>
          <th class="param invisible">Param&egrave;tre</th>
          <th class="colonne_suppr"></th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

    <p class="text-center"><a class="btn btn-info" href="#modal-commandes" role="button" data-toggle="modal" id="btn-search-commande"><i class="icon-white icon-search"></i> Rechercher une commande <span class="raccourcis">F6</span></a></p>
  </div>
</div>

<!-- Modal Commandes -->
<div id="modal-commandes" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="commandes-titre" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="commandes-titre">Rechercher une commande</h3>
  </div>
  <div class="modal-body">
    <div class="search">
    <p>Entre le nom de l'étudiant, le numéro de commande ou scanne le code-barre pour retrouver la commande</p>
    <input type="text" autocomplete="off" class="input-xxlarge" id="input-search-commande" />
    <table class="table table-striped table-bordered table-hover select-table">
      <tbody>
      </tbody>
    </table>
  </div>
  <div class="infos"></div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Fermer</button>
    <button class="btn invisible" id="btn-retour-commandes">Retour <span class="raccourcis"><i class="icon icon-arrow-left"></i></span></button>
    <button class="btn btn-primary invisible" id="btn-display-infos-commande">Infos <span class="raccourcis">Entrée</span></button>
    <button class="btn btn-primary invisible" id="btn-valider-infos-commande">Payer <span class="raccourcis">Entrée</span></button>
  </div>
</div>

<!-- Modal Encaissement -->
<div id="modal-validation" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="validation-titre" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="validation-titre">Enregistrer la vente</h3>
  </div>
  <div class="modal-body">
    <div class="modal-page cb moneo invisible">
      <p>Tu peux maintenant saisir le montant sur le terminal de paiement, puis valider lorsque le ticket est imprimé.<br/><br/>
      La touche # sert à faire un double 0.</p>
    </div>
    <div class="modal-page cheque invisible">
      <form class="form-horizontal">
        <div class="control-group">
          <label class="control-label" for="emetteur">Émetteur</label>
          <div class="controls">
            <input type="text" id="cheque-emetteur"/>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="banque">Banque</label>
          <div class="controls">
            <select name="banque" id="cheque-banque">
              <?php
                $liste_banques = array("Banque Populaire","BNP Paribas","Caisse d'Epargne","CIC","Crédit Agricole","Crédit du Nord","Crédit Mutuel","LCL","HSBC","La Banque Postale","Société Générale");
                sort($liste_banques);
                $liste_banques[] = "Autre";
                foreach($liste_banques as $banque)
                    echo '<option value="'.$banque.'">'.$banque.'</option>';
			  ?>
            </select>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-page especes invisible">
      <form class="form-horizontal">
        <div class="control-group">
          <label class="control-label" for="especes-donne">Montant donné</label>
          <div class="controls">
            <input type="text" id="especes-donne" class="donne"/>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">À rendre</label>
          <div class="controls">
            <span class="rendu">- </span> €
          </div>
        </div>
      </form>
    </div>
    <div class="modal-page payutc invisible">
      <p>Merci de passer la carte étudiant sur la badgeuse.</p>
    </div>
    <div class="modal-page assos invisible">
      <form class="form-horizontal">
        <div class="control-group">
          <label class="control-label">Asso</label>
          <div class="controls">
            <input type="text" autocomplete="off" id="nom-asso" />
          </div>
        </div>
        <div class="control-group invisible">
          <label class="control-label">Compte</label>
          <div class="controls">
            <select id="compte-asso">
            </select>
          </div>
        </div>
        <div class="control-group invisible">
          <div class="controls">
            <label class="checkbox">
              <input type="checkbox" name="tarifasso"/> Appliquer le tarif asso
            </label>
          </div>
        </div>
        <div class="control-group invisible">
          <label class="control-label">Quota</label>
          <div class="controls">
            <span id="quota-compte-asso"></span>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">Raison</label>
          <div class="controls">
            <input type="text" autocomplete="off" id="client-asso" />
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">Mot de passe</label>
          <div class="controls">
            <input type="password" class="mot-de-passe" />
          </div>
        </div>
      </form>
    </div>
    <div class="modal-page working invisible">
      <p class="text-center">Enregistrement de la vente en cours...<br/>Merci de patienter<br/><br/>
      <img src="<?php echo $racine ?>styles/0/icones/loading.gif" alt="processing..." /></p>
    </div>
    <div class="modal-page ok invisible">
      <p class="text-center message"></p>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn revenir" data-dismiss="modal" aria-hidden="true">Retour</button>
    <button class="btn btn-success valider">Valider</button>
  </div>
</div>

<?php
	require('inc/footer.php');
  }
  else {
      Log::info("Accès à la caisse depuis un navigateur non autorisé");
	echo "<script>alert(\"La caisse n'est accessible qu'à partir du PC Caisse du Polar !\");document.location.href=\"".$racine."\";</script>";
	}
?>
