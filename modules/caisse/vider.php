<?php
if(isset($_GET['Backup'])){
	$titrePage = 'Sauvegarde';
	addFooter('
    <script type="text/javascript">var url = "'.urlControl().'";
                                   var icone = "'.urlStyle('icones/loading.gif').'";</script>
    <script type="text/javascript" src="'.urlTo('js/modules/vider_control.js').'"></script>');
	require("inc/header.php");
?>
<h1>Sauvegarde de la base de données</h1>
<p>La sauvegarde de la base de données est en cours. Cette opération peut durer entre une et trois minutes. <b>Merci de ne pas quitter cette page.</b></p>
<ul id="encours">
</ul>
<p id="finished"></p>
<?php
	require("inc/footer.php");
}
else {
	// On repousse ça pour pas se faire jeter par le timeout
	$_SESSION['con-lastPrivateActive'] = time() + 2*3600;

	$titrePage = 'Vider la caisse';
	addHeaders('
		<style type="text/css">
		#monnaie input {
			width: 40px;
			margin-right: 20px;
		}
        </style>');
    addFooter('
       <script type="text/javascript">var racine = "'.urlTo().'";</script>
        <script type="text/javascript" src="'.urlTo('js/caisse_real_world.js').'"></script>
		<script type="text/javascript" src="'.urlTo('js/modules/vider.js').'"></script>');

    Log::info('Ouverture de l\'interface vider la caisse');

    $db->beginTransaction();
    $dernierIDVente = Vente::select('MAX(IDVente)', 'IDVente')
        ->rawExecute()->fetchColumn();
    if (is_null($dernierIDVente))
        Log::info('Vider la caisse : aucune vente !');
    else
        Log::info('Vider la caisse : dernierIDVente = '.$dernierIDVente);

    // Récupération du solde de la caisse à l'issue de la dernière séance
    try {
        $solde_precedent = Caisse::getSolde();
    } catch (CaisseSoldeErreur $e) {
        // on récupère le solde dans la table de l'ancien système
        $res = $db->query('SELECT TotalApresTransfert FROM polar_caisse_detail ORDER BY ID DESC LIMIT 1');
        $solde_precedent = $res->fetchColumn();
    }

	// Récupération du montant des ventes pour chaque moyen de paiement
    $q = Vente::select('COALESCE(SUM(PrixFacture), 0) as Montant, MoyenPaiement')
        ->groupBy('MoyenPaiement');

    $montant_theorique = array('cb' => 0, 'moneo' => 0,
                               'especes' => $solde_precedent,
                               'cheques' => 0, 'payutc' => 0);
    foreach ($q->rawExecute() as $ligne) {
        if ($ligne['MoyenPaiement'] == 'especes') // on ajoute au montant résiduel
            $montant_theorique[$ligne['MoyenPaiement']] += $ligne['Montant'];
        else
            $montant_theorique[$ligne['MoyenPaiement']] = $ligne['Montant'];
    }

    // Récupération du montant des ventes Payutc (si actif)    
    if ($CONF['use_payutc']) {
        $stats = Payutc::loadService('STATS', 'manager');
        $dateCaisseHier = Caisse::getDateDernierCaisse();
        $payutc_revenue = $stats->getRevenue(array('fun_id' => $CONF['payutc_fundation'],
                                                   'start' => $dateCaisseHier,
                                                   'app_id' => $CONF['payutc_caisse_app_id'])) / 100;
    }

	// Récupération des chèques en cours
    $cheques = Cheque::select()->where('PEC = 0 AND NumVente > 0')
        ->order('Montant DESC, Banque');
    $db->commit();

    // Récupération des tickets actifs
    $tickets = Ticket::select('Ticket.*, TicketMessage.message')
        ->leftJoin('TicketMessage', 'TicketMessage.ticket = Ticket.id')
        ->where("status = 'active' OR status = 'new'")
        ->order('TicketMessage.id DESC')
        ->groupBy('Ticket.id');

	require_once('inc/header.php');
	?>

<div class="row">
<div class="span8">
<h1>Vider la caisse</h1>
<p>Cette page a été générée le <?php echo date("d/m/Y à H:i:s"); ?>. Les encaissements des ventes réalisées après cette date ne doivent <b>pas</b> être comptabilisés.</p>
<p>Utilisez le point comme séparateur décimal.</p>
<p>Pour tout problème rencontré en faisant la caisse, faire le plus rapidement possible un ticket dans la catégorie Site Web en précisant l'intégralité des données (nombre de pièces/billets, montants des télécollectes...). Ne jamais refaire la caisse pour tenter une correction, cela complique la réparation !</p>

<?php if (is_null($dernierIDVente)): ?>
<div class="alert alert-error">
  <h4>Attention !</h4>
  Aucune vente n'a été enregistrée depuis la dernière fois, es-tu sûr de vouloir faire la caisse ?
</div>
<?php endif; ?>

<?php afficherErreurs() ?>

<form name="formulaire" id="formCaisse" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
<table>
	<tr>
		<td>
			<h2>Les chèques</h2>
			Supprimez les chèques qui ne sont <b>pas</b> dans la caisse. Cela ne supprime pas la vente : à vous de récupérer l'argent par la suite !<br />
			Si un chèque est manquant, faites une nouvelle vente.<br />
			Si des données sont erronées, ce n'est pas grave : elles seront corrigées par le trésorier à la remise des chèques.
		</td>
	</tr>
	<?php
	foreach($cheques as $cheque) {
		echo '
		<tr>
			<td>'.$cheque->Banque.' - '.formatPrix($cheque->Montant).'€ - '.$cheque->Emetteur.'
			<img title="Supprimez ce chèque !" src="'.urlStyle('/icones/croix.png').'" alt="X" /></a></td>
		</tr>';
	}
	?>
	<tr>
		<td colspan="2">
			<h2>Les pièces</h2>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" value="Ouvrir la caisse" id="otc" class="btn" />
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>Montant monnaie théorique : </td>
		<td>
          <?php echo round($montant_theorique['especes'], 2); ?> €
			<input type="hidden" name="theoriqueEspeces" value="<?php echo round($montant_theorique['especes'], 2); ?>" />
		</td>
	</tr>
	<tr>
		<td>
			Montant monnaie pratique : </td>
		<td id="montantpratique">0 €</td>
	</tr>
</table>
<table id="monnaie">
	<tr>
		<td>100 €</td>
		<td>
			<input name="100e" value="0" id="100e" class="focus" />
		</td>
		<td>50 €</td>
		<td>
			<input name="50e" value="0" id="50e" />
		</td>
		<td>20 €</td>
		<td>
			<input name="20e" value="0" id="20e" />
		</td>
		<td>10 €</td>
		<td>
			<input name="10e" value="0" id="10e" />
		</td>
		<td>5 €</td>
		<td>
			<input name="5e" value="0" id="5e" />
		</td>
	</tr>
	<tr>
		<td>2 €</td>
		<td>
			<input name="2e" value="0" id="2e" />
		</td>
		<td>1 €</td>
		<td>
			<input name="1e" value="0" id="1e" />
		</td>
		<td>50c</td>
		<td>
			<input name="50c" value="0" id="50c" />
		</td>
		<td>20c</td>
		<td>
			<input name="20c" value="0" id="20c" />
		</td>
		<td>10c</td>
		<td>
			<input name="10c" value="0" id="10c" />
		</td>
	</tr>
	<tr>
		<td>5c</td>
		<td>
			<input name="5c" value="0" id="5c" />
		</td>
		<td>2c</td>
		<td>
			<input name="2c" value="0" id="2c" />
		</td>
		<td>1c</td>
		<td>
			<input name="1c" value="0" id="1c" />
		</td>
		<td></td>
	</tr>
</table>
<table>
	<tr>
		<td colspan="2">
			<h2>Les paiements numériques</h2>
		</td>
	</tr>
	<tr>
		<td>Ventes par CB :</td>
		<td>
			<input name="telCB" size="5"/> (Montant théorique : <?php echo round($montant_theorique['cb'],2); ?> €)
			<input type="hidden" name="theoriqueCB" value="<?php echo $montant_theorique['cb']; ?>"/>
		</td>
	</tr>
	<tr>
		<td>Ventes par Monéo :</td>
		<td>
			<input name="telMoneo" size="5"/> (Montant théorique : <?php echo round($montant_theorique['moneo'],2); ?> €)
			<input type="hidden" name="theoriqueMoneo" value="<?php echo $montant_theorique['moneo']; ?>"/>
		</td>
	</tr>
	<tr>
		<td>Ventes par payutc :</td>
		<td>
          <input name="revenuePayutc" value="<?php echo isset($payutc_revenue) ? $payutc_revenue : 0 ; ?>"/> (Montant théorique : <?php echo round($montant_theorique['payutc'],2); ?> €)
			<input type="hidden" name="theoriquePayutc" value="<?php echo $montant_theorique['payutc']; ?>"/>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<h2>Les transferts</h2>
		</td>
	</tr>
	<tr>
		<td>Transfert vers coffre (billets) :</td>
		<td>
			<input name="verscoffreBillets" size="5" value="0"/>
		</td>
	</tr>
	<tr>
		<td>Transfert vers coffre (pièces) :</td>
		<td>
			<input name="verscoffrePieces" size="5" value="0"/>
		</td>
	</tr>
	<tr>
		<td>Transfert vers caisse (billets) :</td>
		<td>
			<input name="verscaisseBillets" size="5" value="0"/>
		</td>
	</tr>
	<tr>
		<td>Transfert vers caisse (pièces) :</td>
		<td>
			<input name="verscaissePieces" size="5" value="0"/>
		</td>
	</tr>
	<tr>
		<td>
          <input type="hidden" name="dernierId" value="<?php echo $dernierIDVente; ?>" />
			<input type="submit" name="ConfirmerVider" value="Envoyer les données !" class="btn" />
		</td>
		<td>
			<input type="button" value="Annuler !" class="btn" id="boutonAnnuler" />
		</td>
	</tr>
</table>
</form>
</div>

<div class="span4">
  <h1>Tickets actifs</h1>
  <?php foreach($tickets as $ticket): ?>
  <div class="well well-small">
    <h4><a href="<?php echo urlTo('hotline', 'liste', 'Ticket='.$ticket) ?>"><?php echo $ticket->subject ?></a> <small>par  <?php echo $ticket->author ?></small></h4>
    <p><?php echo nl2br($ticket->message) ?></p>
  </div>
  <?php endforeach; ?>
</div>

</div>
	<?php
	require_once('inc/footer.php');
}
?>
