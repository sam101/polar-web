<?php
$titrePage = 'Suivi des espèces du coffre';
use_datatables();
use_jquery_ui();
addFooter('
	<script>
	$(document).ready(function() {
		$("#dialog").dialog({ width: 700, autoOpen: false });
		$("#ajouterbout").click(function() {
			$("#dialog").dialog("open");
			$("#first").focus();
		});
	});
	</script>');
require_once('inc/header.php');
?>
<h1>Suivi des espèces du coffre</h1>
<input type="button" id="ajouterbout" class="btn" value="Ajouter un mouvement" />
<?php
	$req = query("SELECT SUM(Billets) AS Billets, SUM(Pieces) AS Pieces FROM polar_caisse_coffre");
	$data = mysql_fetch_assoc($req);
	echo '<h2>Montant en billets : '.round($data['Billets'],2).'€</h2>';
	echo '<h2>Montant en pièces : '.round($data['Pieces'],2).'€</h2>';

	$req=query("SELECT polar_caisse_coffre.*, polar_utilisateurs.Prenom FROM polar_caisse_coffre
		INNER JOIN polar_utilisateurs ON polar_utilisateurs.ID=polar_caisse_coffre.User
		ORDER BY Date DESC");

	echo '<table class="datatables table table-bordered table-striped">';
	echo '<thead><tr><th>Date</th><th>Utilisateur</th><th>Billets</th><th>Pièces</th><th>Commentaire</th></tr></thead><tbody>';
	while($data=mysql_fetch_array($req))
	{
		echo '<tr><td>'.$data['Date'].'</td><td>'.$data['Prenom'].'</td><td>'.$data['Billets'].'€</td><td>'.$data['Pieces'].'€</td><td>'.$data['Commentaire'].'</td></tr>';
	}
	echo '</tbody></table>';
?>
<div id="dialog" title="Ajouter un mouvement de fonds">
	<p>Écrire 300€ pour un ajout de 300€ du coffre, et -300€ pour un retrait de 300€ du coffre.</p>
	<form name="formulaire" method="post" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
	<table>
    <tr><td><label for="first">Mouvement billets</label></td><td><input type="text" id="first" name="billets" />€</td></tr>
	<tr><td><label for="pieces">Mouvement pièces</label></td><td><input type="text" id="pieces" name="pieces" />€</td></tr>
	<tr><td><label for="transfertcaisse">Échange caisse &lt;-&gt; coffre</label></td><td><input type="checkbox" id="transfertcaisse" name="transfertcaisse" value="oui" checked="checked" /> <small>(ceci retirera l'argent de la caisse lors du mouvement)</small></td></tr>
	<tr><td><label for="commentaire">Commentaire (obligatoire)</label></td><td><input type="text" name="commentaire" /></td></tr>
	</table>
	<input type="submit" name="AjouterMouvement" value="Ajouter !" class="btn" />
	</form>
</div>
<?php
require_once('inc/footer.php');
?>

