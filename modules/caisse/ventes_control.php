<?php
if(isset($_GET['SupprimerVente'])){
	$vente = intval($_GET['SupprimerVente']);

	// Mettre à jour le stock
	MAJStockSupprimer($vente);

	// Supprimer la vente dans la table
	query("DELETE FROM polar_caisse_ventes WHERE IDVente=$vente");

	//Supprimer le chèque associé (éventuellement)
	query("DELETE FROM polar_caisse_cheques WHERE NumVente=$vente");
	header("Location: $racine$module/$section");
}
?>
