<?php
$gesarticle = Payutc::loadService('GESARTICLE', 'manager'); // on précharge

$titrePage = 'Gestion des articles';
addFooter('
  	<script>
	var TableauPremier = '.json_encode(getPremierDisponible()).';
	$(document).ready(function() {
		$("#secteur").change(function(){
			$("#code").val(TableauPremier[$(this).val()]);
		});

		$(".checkdis").click(function(){
			if($(this).attr("checked"))
				$(this).next("input").removeAttr("disabled").focus();
			else
				$(this).next("input").val("").attr("disabled", "disabled");
		});

$(".supprime-article").click(function() {
var id = $(this).attr("data-id");
var ligne = $(this).parents("tr")[0];
$.get("'.urlControl().'", {"SupprimerArticle": id}, function() {
theDataTable.fnDeleteRow(ligne); //cf inc/headers.php
});
});

$(".desactiver-article").click(function() {
var id = $(this).attr("data-id");
var btn1 = $(this);
var btn2 = $(".reactiver-article[data-id="+id+"]");
$.get("'.urlControl().'", {"DesactiverArticle": id}, function() {
btn1.addClass("hidden");
btn2.removeClass("hidden");
});
});

$(".reactiver-article").click(function() {
var id = $(this).attr("data-id");
var btn1 = $(this);
var btn2 = $(".desactiver-article[data-id="+id+"]");
$.get("'.urlControl().'", {"ReactiverArticle": id}, function() {
btn1.addClass("hidden");
btn2.removeClass("hidden");
});
});
	});
	</script>');
use_datatables();
require_once('inc/header.php');
?>
<h1>Les articles en vente</h1>
<?php
$user = intval($_SESSION['con-id']);
if(isset($_GET['EditerArticle'])){
	echo '<p><a href="'.$racine.$module.'/'.$section.'" title="Articles">Retour à la liste des articles (sans sauvegarder)</a></p>';
	$article = intval($_GET['EditerArticle']);
	$req=query("SELECT pa.* FROM polar_caisse_articles pa
		INNER JOIN polar_utilisateurs pu ON pu.Responsable = pa.Secteur OR pu.Bureau = 1
		WHERE Actif = 1 AND pu.ID = $user AND pa.ID = $article
		ORDER BY pa.Nom ASC");
	$data = mysql_fetch_assoc($req);
	echo '<table class="datatables table table-bordered table-striped table-condensed">';
	echo '<form method="post" action="'.$racine.$module.'/'.$section.'_control?EditerArticle='.$data['ID'].'">';
	echo '<tr>
			<td>Nom : </td>
			<td><input name="nom" size="50" value="'.$data['Nom'].'" class="autofocus" /></td>
		</tr>
		<tr>
			<td>Photo : </td>
			<td><input name="photo" size="50" value="'.$data['Photo'].'" /></td>
		</tr>';
	echo '<tr>
		<td>Secteur : </td>
		<td><select id="secteur" name="secteur">
			<option value="">---</option>';
			$req = query("SELECT Code,Secteur FROM polar_caisse_secteurs pcs
				INNER JOIN polar_utilisateurs pu ON pu.Responsable = pcs.Code OR pu.Bureau = 1
				WHERE pu.ID = $user
				ORDER BY Secteur ASC");
			while($data2 = mysql_fetch_assoc($req)) {
				echo '<option value="'.$data2['Code'].'"';
				if($data2['Code'] == $data['Secteur'])
					echo ' selected="selected"';
				echo '>'.$data2['Secteur'].'</option>';
			}
		echo '	</select>
				</td>
			</tr>';

	echo '<tr>
			<td>Code caisse : </td>
			<td><input name="code" id="code" size="4" value="'.$data['CodeCaisse'].'"/></td>
		</tr>';
	echo '<tr>
			<td>Actif : </td>
			<td><input type="checkbox" name="actif"';
	if($data['EnVente'] == 1 || empty($data['CodeCaisse']))
		echo 'checked="checked"';
	echo '/></td>
		</tr>';
	echo '<tr>
			<td>Prix achat TTC : </td>
			<td><input name="achat" size="4" value="'.$data['PrixAchat'].'"/> €</td>
		</tr>';
	echo '<tr>
			<td>Prix vente TTC : </td>
			<td><input name="vente" size="4" value="'.$data['PrixVente'].'"/> €</td>
		</tr>';
	echo '<tr>
			<td>Prix vente asso TTC : </td>
			<td><input type="checkbox" class="checkdis"';
			if(!empty($data['PrixVenteAsso'])) echo ' checked="checked"';
			echo ' /><input name="asso" size="4"" value="'.$data['PrixVenteAsso'].'"';
			if(empty($data['PrixVenteAsso'])) echo ' disabled="disabled"';
			echo ' /> €
			</td>
		</tr>';
	echo '<tr>
			<td>TVA : </td>
			<td><select name="tva">
				<option value="0"'; if($data['TVA']==0) echo ' selected="selected"';
    echo '>0</option>
			<option value="0.05"'; if($data['TVA']==0.05) echo ' selected="selected"';
    echo '>5</option>
			<option value="0.055"'; if($data['TVA']==0.055) echo ' selected="selected"';
			echo '>5.5</option>
			<option value="0.07"'; if($data['TVA']==0.07) echo ' selected="selected"';
			echo '>7</option>
			<option value="0.10"'; if($data['TVA']==0.1) echo ' selected="selected"';
			echo '>10</option>
			<option value="0.196"'; if($data['TVA']==0.196) echo ' selected="selected"';
			echo '>19.6</option>
			<option value="0.2"'; if($data['TVA']==0.2) echo ' selected="selected"';
			echo '>20</option>
		</select> %</td></tr>';
	echo '<tr>
			<td>Stock : </td>
			<td><input type="checkbox" class="checkdis"';
			if($data['Stock'] !== NULL) echo ' checked="checked"';
			echo ' /><input name="stock" size="4"" value="'.$data['Stock'].'"';
			if($data['Stock'] === NULL) echo ' disabled="disabled"';
			echo ' />
			</td>
		</tr>';
	echo '<tr>
			<td>Seuil d\'alerte : </td>
			<td><input type="checkbox" class="checkdis"';
			if($data['SeuilAlerte'] !== NULL) echo ' checked="checked"';
			echo ' /><input name="seuil" size="4"" value="'.$data['SeuilAlerte'].'"';
			if($data['SeuilAlerte'] === NULL) echo ' disabled="disabled"';
			echo ' />
			</td>
		</tr>';
	echo '<tr>
			<td>EAN 13: </td>
			<td><input type="checkbox" class="checkdis"';
			if($data['EAN13'] !== NULL) echo ' checked="checked"';
			echo ' /><input name="EAN13" size="15" value="'.$data['EAN13'].'"';
			if($data['EAN13'] === NULL) echo ' disabled="disabled"';
			echo ' />
			</td>
		</tr>';
	if (isset($data['Remise1']))
		$data['Remise1'] *= 100;
	if (isset($data['Remise2']))
		$data['Remise2'] *= 100;
	echo '<tr>
			<td>Palier 1 : </td>
			<td><input size="4" name="palier1" value="'.$data['Palier1'].'"/></td>
		</tr>';
	echo '<tr>
			<td>Remise 1 : </td>
			<td><input size="4" name="remise1" value="'.$data['Remise1'].'"/>%</td>
		</tr>';
	echo '<tr>
			<td>Palier 2 : </td>
			<td><input size="4" name="palier2" value="'.$data['Palier2'].'"/></td>
		</tr>';
	echo '<tr>
			<td>Remise 2 : </td>
			<td><input size="4" name="remise2" value="'.$data['Remise2'].'"/>%</td>
		</tr>';
	echo '<tr>
			<td>Fournisseur / Réf : </td>
			<td><input type="text" name="fournisseur" value="'.$data['Fournisseur'].'"/></td>
		</tr>';
	echo '<tr>
			<td></td>
			<td><input type="submit" class="btn"></td>
		</tr>';
	echo '</form>';
	echo '</table>';

	// Affichage de l'histoirque de l'article
	echo '<h1>Historique pour le code '.$data['CodeCaisse'].'</h1>
	<table class="datatables table table-bordered table-striped table-condensed">
	<tr><th>Nom</th><th>Stock initial</th><th>Stock final</th><th>Prix achat</th><th>Prix vente</th><th>Prix vente asso</th><th>TVA</th><th>Date</th></tr>';
	$code = empty($data['CodeCaisse']) ? 0 : $data['CodeCaisse'];

	$req2 = query("SELECT ID, Nom, ROUND(PrixAchat,2) AS PrixAchat, ROUND(PrixVente, 2) AS PrixVente, ROUND(PrixVenteAsso, 2) AS PrixVenteAsso, ROUND(TVA*100,1) AS TVA, StockInitial, Stock, DATE(Date) AS Date FROM polar_caisse_articles pca
		WHERE CodeCaisse = $code");
	while($ancien = mysql_fetch_assoc($req2)){
	 	echo '<tr><td>'.$ancien['Nom'].'</td><td>'.$ancien['StockInitial'].'</td><td>';
	 	if($ancien['ID'] == $article)
	  		echo '<i>Article en vente</i>';
		else
	 		echo $ancien['Stock'];
		echo '</td><td>'.$ancien['PrixAchat'].'€</td><td>'.$ancien['PrixVente'].'€</td><td>'.$ancien['PrixVenteAsso'].'€</td><td>'.$ancien['TVA'].'%</td><td>'.$ancien['Date'].'</td></tr>';
	}
	echo '</table>';
}

/*
=======================
 EDITION DE L'IMAGE !
=======================
*/

else if (isset($_GET['EditerImage']))
{
	afficherErreurs();
	$article = intval($_GET['EditerImage']);
	$req=query("SELECT pa.* FROM polar_caisse_articles pa
		INNER JOIN polar_utilisateurs pu ON pu.Responsable = pa.Secteur OR pu.Bureau = 1
		WHERE Actif = 1 AND pu.ID = $user AND pa.ID = $article
		ORDER BY pa.Nom ASC");
	$data = mysql_fetch_assoc($req);

	// Remplacement des caractères spéciaux
	$nom = html_entity_decode($data['Nom']);
	$nom = strtolower(preg_replace('#[^0-9a-z]+#i', '-', $nom));

	while(strpos($nom, '--') !== false)
	{
		$nom = str_replace('--', '-', $nom);
	}

	$nom = trim($nom, '-');
	?>
	<p>Uploader une nouvelle image pour l'article : <?php echo $data['Nom']; ?>.</p>
	<div class="float">
		<img alt="Image <?php echo $data['Photo']; ?>" style="max-width: 180px;" class="media-grid" src="../upload/articles/<?php echo ($data['Photo'] != '' && file_exists(__DIR__.'/../../upload/articles/'.$data['Photo'])) ? $data['Photo'] : 'default.png'; ?>" />
		<p style="text-align: center;"><?php echo ($data['Photo'] != '' && file_exists(__DIR__.'/../../upload/articles/'.$data['Photo'])) ? 'Nom de l\'image actuelle : '.$data['Photo'] : 'Image par défault'; ?></p>
	</div>

	<div class="float">
		<p><b>Format autorisé : jpg, png, gif, 2Mo max</b></p>
		<form method="post" action="<?php echo $racine.$module.'/'.$section.'_control?EditerImage='.$data['ID']; ?>" enctype="multipart/form-data">
			<label for="nom" style="font-weight: bold;">Le nom est généré automatiquement, mais vous pouvez le modifier :</label>
			<input type="text" value="<?php echo $nom; ?>" name="nom" id="nom" /><br />
			<label for="image" style="font-weight: bold;">Votre image :</label>
			<input type="file" name="image" id="image" />
			<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
			<input type="hidden" name="nom_actuel" value="<?php echo $data['Photo']; ?>" />
			<br />
			<input type="submit" name="img" value="valider" class="btn btn-default" />
		</form>
	</div>
	<?php
}
else{
    afficherErreurs();
	$req = query("SELECT pa.* FROM polar_caisse_articles pa
		INNER JOIN polar_utilisateurs pu ON pu.Responsable = pa.Secteur OR pu.Bureau = 1
		WHERE Actif = 1 AND pu.ID = $user
		ORDER BY pa.Nom ASC");
	echo '<a href="'.$racine.$module.'/'.$section.'?EditerArticle" title="Nouvel article">Créer un nouvel article</a>';
	echo '<table class="datatables table table-bordered table-striped">';
	echo '<thead>
			<tr>
				<th>Code</th><th>Nom</th><th>Prix</th><th>Stock</th><th>Modifier</th><th>Photo</th><th>Supprimer</th><th>Désactiver</th><th>Réactiver</th>
			</tr>
		</thead>
		<tbody>';

	while($data = mysql_fetch_assoc($req)){
		echo '<tr>
			<td>'.$data['CodeCaisse'].'</td>
			<td>'.$data['Nom'].'</td>
			<td>'.$data['PrixVente'].'€</td>
			<td>';
		if(is_null($data['Stock']))
			echo 'NC';
		else
			echo $data['Stock'];
		echo '</td>
		<td>
			<a href="',$racine,$module,'/',$section.'?EditerArticle=',$data['ID'],'"><img title="Editez cet article !" src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></a>
		</td>
		<td>
			<a href="',$racine,$module,'/',$section.'?EditerImage=',$data['ID'],'"><img title="Editez cette image !" src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></a>
		</td>
		<td>
			<img href="#" data-id="'.$data['ID'].'" class="supprime-article" title="Supprimez cet article !" src="',$racine,'styles/',$design,'/icones/croix.png" alt="-" />
		</td>';
		if($data['EnVente'] == 1) {
            $class1 = '';
            $class2 = 'hidden';
        } else {
            $class1 = 'hidden';
            $class2 = '';
        }
        echo '<td><img data-id="'.$data['ID'].'" class="desactiver-article '.$class1.'" title="Désactivez cet article !" src="',$racine,'styles/',$design,'/icones/croix.png" alt="-" /></td>';
        echo '<td><img data-id="'.$data['ID'].'" class="reactiver-article '.$class2.'" title="Réactivez cet article !" src="',$racine,'styles/',$design,'/icones/ajouter.png" alt="-" /></td>';
	}
	echo '</tbody>
	</table>';
}

require_once('inc/footer.php');
