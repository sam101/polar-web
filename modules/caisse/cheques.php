<?php
$titrePage = 'Encaisser les ch&egrave;ques clients';
use_datatables();
require_once('inc/header.php');
?>
<h1>Encaisser les chèques clients</h1>
<h2>Montant total à encaisser : <?php
	$req=query('SELECT SUM(Montant) AS nbr FROM polar_caisse_cheques WHERE PEC=1 AND Ordre=\'Le Polar\' AND DateEncaissement IS NULL');
	$data=mysql_fetch_assoc($req);
	echo round($data['nbr'],2);
?>€</h2>
<form action="<?php echo "$racine$module/$section"; ?>_control?MarquerEncaisses" method="post"><input type="submit" value="Marquer encaissés" class="btn" /></form>
<table class="datatables table table-bordered table-striped">
<thead><tr><th>ID</th><th>Date</th><th>Banque</th><th>Montant</th><th>Emetteur</th><th>Marquer encaissé</th></tr></thead><tbody>
<?php
	$sql='SELECT * FROM `polar_caisse_cheques` WHERE PEC=1 AND Ordre=\'Le Polar\' AND DateEncaissement IS NULL';
	$req=query($sql);
	while($data=mysql_fetch_assoc($req)){
		echo '<tr><td>'.$data['ID'].'</td>
		<td>'.$data['Date'].'</td>
		<td>'.stripslashes($data['Banque']).'</td>
		<td>'.$data['Montant'].'</td>
		<td>'.stripslashes($data['Emetteur']).'</td>
		<td><a href="'.$racine.$module.'/'.$section.'_control?EncaisserCheque='.$data['ID'].'" title="Marquer ce chèque comme encaissé"><i class="icon-check"></i></a></td>';
	}
?></tbody></table>
<?php
require_once('inc/footer.php');
?>
