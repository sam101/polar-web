<?php
$titrePage = 'Statistiques de la caisse';
$id = intval($_SESSION['con-id']);

if(!empty($_POST['ep-datedebut']))
	$datedebut = mysqlSecureText($_POST['ep-datedebut']);
else
	$datedebut = '1970-01-01';

if(!empty($_POST['ep-datefin']))
	$datefin = mysqlSecureText($_POST['ep-datefin']);
else
	$datefin = date('Y-m-d');

use_jquery_ui();
addFooter('
	<script>
	$(document).ready(function() {
		$("#datepicker").datepicker({ dateFormat: \'yy-mm-dd\' });
		$("#datepickerfin").datepicker({ dateFormat: \'yy-mm-dd\' });
	});
	</script>');

require('inc/header.php');
?>
<h1>Sélectionner une période</h1>
<form method="post" action="<?php echo $racine.$module.'/'.$section ?>">
	Du <input size="15" type="text" class="in-text" name="ep-datedebut" id="datepicker" value="<?php if(isset($_POST['ep-datedebut'])) echo $_POST['ep-datedebut']; ?>"> (format YYYY-MM-JJ) <br />
	Au <input size="15" type="text" class="in-text" name="ep-datefin" id="datepickerfin" value="<?php if(isset($_POST['ep-datefin'])) echo $_POST['ep-datefin']; ?>"> (format YYYY-MM-JJ) <br />
	<input type="submit" value="Go !" class="btn" />
</form>
<h1>Les meilleurs vendeurs</h1>
<?php
$req = query("SELECT SUM(PrixFacture) AS Somme, Prenom, Nom FROM `polar_caisse_ventes_global` pcvg
	INNER JOIN polar_utilisateurs pu ON pcvg.Permanencier = pu.id
	WHERE DATE(Date) >= '$datedebut'
	AND DATE(Date) <= '$datefin'
	GROUP BY Permanencier
	ORDER BY SUM(PrixFacture) DESC
	LIMIT 0,10");
echo '<table class="datatables table table-bordered table-striped table-condensed">';
echo '<tr><th>Vendeur</th><th>Chiffre d&rsquo;affaire</th></tr>';
while($data = mysql_fetch_assoc($req))
 	echo '<tr>
		<td>'.$data['Prenom'].' '.$data['Nom'].'</td>
		<td>'.round($data['Somme'],2).'€</td>
		</tr>';
echo '</table>';
?>
<h1>Les meilleurs articles</h1>
<?php
$req = query("SELECT SUM(PrixFacture) AS Somme, Nom FROM `polar_caisse_ventes_global` pcvg
	INNER JOIN polar_caisse_articles pca ON pcvg.Article = pca.ID
	WHERE DATE(pcvg.Date) >= '$datedebut' AND DATE(pcvg.Date) <= '$datefin'
	GROUP BY pca.CodeCaisse
	ORDER BY SUM(PrixFacture) DESC
	LIMIT 0,10");
echo '<table class="datatables table table-bordered table-striped table-condensed">';
echo '<tr><th>Article</th><th>Chiffre d&rsquo;affaire</th></tr>';
while($data = mysql_fetch_assoc($req))
	echo '<tr>
		<td>'.$data['Nom'].'</td>
		<td>'.round($data['Somme'],2).'€</td>
		</tr>';
echo '</table>';
?>
<h1>Les meilleures journées</h1>
<?php
$req = query("SELECT SUM(PrixFacture) AS Somme, DATE(pcvg.Date) AS Journee FROM `polar_caisse_ventes_global` pcvg
	WHERE DATE(pcvg.Date) >= '$datedebut' AND DATE(pcvg.Date) <= '$datefin'
	GROUP BY DATE(pcvg.Date)
	ORDER BY SUM(PrixFacture) DESC
	LIMIT 0,10");
echo '<table class="datatables table table-bordered table-striped table-condensed">';
echo '<tr><th>Journée</th><th>Chiffre d&rsquo;affaire</th></tr>';
while($data = mysql_fetch_assoc($req))
 	echo '<tr>
		<td>'.$data['Journee'].'</td>
		<td>'.round($data['Somme'],2).'€</td>
		</tr>';
echo '</table>';

require_once('inc/footer.php');
