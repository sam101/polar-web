<?php
$titrePage = 'Gestion des factures';
use_jquery_ui();
addFooter('<script>
	  function ouvrirPopupCheque(){
		$("#dialogCheque").modal("show");
		$("#dialogVirement").modal("hide");
		$("#NumCheque").focus();
	  }
	  function ouvrirPopupVirement(){
		$("#dialogVirement").modal("show");
		$("#dialogCheque").modal("hide");
		$("#Montant").focus();
	  }
	  </script>');
require_once('inc/header.php');
?>
<h1>Payer les factures</h1>
<input type=submit value="Payer par chèque !" class="btn" onClick="ouvrirPopupCheque();" />
<input type=submit value="Payer par virement !" class="btn" onClick="ouvrirPopupVirement();" />
<br />
<h1>Les chèques en cours</h1>
<div class="modal hide" id="dialogCheque">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Payer une facture par chèque</h3>
  </div>
  <div class="modal-body">
	<p>Remplir l'ensemble des champs pour ajouter une facture payée par chèque.</p>
	<form name="formulaire" method="post" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
	<table>
	<tr><td>Chèque n°</td><td><input id="NumCheque" type=text name="numero" /></td></tr>
	<tr><td>Banque</td><td><input type="text" name="banque" value="Societe Generale"/></td></tr>
	<tr><td>Montant</td><td><input type="text" name="montant" />€</td></tr>
	<tr><td>Emetteur</td><td><input type="text" name="emetteur" value="Le Polar"/></td></tr>
	<tr><td>Ordre</td><td><input type="text" name="ordre" /></td></tr>
	<tr><td>Motif</td><td><input type="text" name="motif" /></td></tr>
	</table>
	<input type="submit" name="AjouterCheque" value="Ajouter !" class="btn" />
	</form>
  </div>
</div>
<div class="modal hide" id="dialogVirement">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Payer une facture par virement</h3>
  </div>
  <div class="modal-body">
	<p>Remplir l'ensemble des champs pour ajouter une facture payée par virement.</p>
	<form name="formulaire" method="post" style="margin-top:10px;" enctype="multipart/form-data" action="<?php echo $racine.$module.'/'.$section.'_control'; ?>">
	<table>
	<tr><td>Montant</td><td><input id="Montant" type=text name="montant" />€</td></tr>
	<tr><td>Emetteur</td><td><input type="text" name="emetteur" value="Le Polar"/></td></tr>
	<tr><td>Ordre</td><td><input type="text" name="ordre" /></td></tr>
	</table>
	<input type="submit" name="AjouterVirement" value="Ajouter !" class="btn" />
	</form>
  </div>
</div>
<p>Voici la liste des chèques émis par Le Polar et non encore encaissés.</p>
<table class="table table-bordered table-striped table-hover">
<tr><th>ID</th><th>Numero</th><th>Date</th><th>Montant</th><th>Ordre</th><th>Motif</th><th>Marquer encaissé</th></tr>
<?php
	$sql="SELECT * FROM polar_caisse_cheques WHERE Emetteur='Le Polar' AND DateEncaissement IS NULL AND TO_DAYS(NOW())-TO_DAYS(Date) <= 396";
	$req=query($sql);
	while($data=mysql_fetch_assoc($req))
	{
		echo '<tr>
		<td>'.$data['ID'].'</td>
		<td>'.$data['Numero'].'</td>
		<td>'.$data['Date'].'</td>
		<td>'.$data['Montant'].'</td>
		<td>'.$data['Ordre'].'</td>
		<td>'.$data['Motif'].'</td>
		<td><a title="Marquer ce chèque comme encaissé." href="'.$racine.$module.'/'.$section.'_control?MarquerCheque='.$data['ID'].'" style="cursor:pointer;" /><i class="icon-check"></i></a></td>
		</tr>';
	}
?>
</table>
<?php
	$sql="SELECT SUM(Montant) AS Somme FROM polar_caisse_cheques WHERE Emetteur='Le Polar' AND DateEncaissement IS NULL AND TO_DAYS(NOW())-TO_DAYS(Date) <= 396";
	$req=query($sql);
	$data=mysql_fetch_array($req);
	echo '<p><b>Montant des chèques émis non encaissés : '.round((0+$data['Somme']),2).'€.</b></p>';
?>
<br />
<h1>Les virements en cours</h1>
<p>Voici la liste des virements en cours pour Le Polar.</p>
<table class="table table-bordered table-striped table-hover">
<tr><th>ID</th><th>Date</th><th>Montant</th><th>Emetteur</th><th>Ordre</th><th>Marquer effectué</th></tr>
<?php
	$sql='SELECT * FROM polar_caisse_virement WHERE Effectue=0';
	$req=query($sql);
	while($data=mysql_fetch_array($req))
	{
		echo '<tr>
		<td>'.$data['ID'].'</td>
		<td>'.$data['Date'].'</td>
		<td>'.$data['Montant'].'</td>
		<td>'.$data['Emetteur'].'</td>
		<td>'.$data['Destinataire'].'</td>
		<td><a title="Marquer ce virement comme effectué." href="'.$racine,$module.'/'.$section.'_control?MarquerVirement='.$data['ID'].'" style="cursor:pointer;" ><i class="icon-check"></i></a></td>
		</tr>';
	}
?>
</table>
<?php
	$sql='SELECT sum(Montant) AS Somme FROM polar_caisse_virement WHERE Emetteur=\'Le Polar\' AND Effectue=0';
	$req=query($sql);
	$data=mysql_fetch_array($req);
	echo '<p><b>Montant des virements non effectués : '.round((0+$data['Somme']),2).'€.</b></p>';
	require_once('inc/footer.php');
?>
