<?php
$autorisations = AutorisationCaisse::getActives();

require_once('inc/header.php');

if (isset($_SESSION['success'])): ?>
<p class="alert alert-success">
  <?php echo $_SESSION['success'] ?>
</p>
<?php
     unset($_SESSION['success']);
endif;
?>

<h2>Autoriser le navigateur</h2>
<form action="<?php echo urlControl() ?>" class="form-inline">
<label for="input-nom">Nom : </label> <input id="input-nom" name="nom" type="text" /> <input type="submit" value="Ajouter" class="btn btn-success" />
<input type="hidden" name="add">
</form>

<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Nom</th>
      <th>Supprimer</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($autorisations as $autorisation): ?>
    <tr>
      <td><?php echo $autorisation->Nom ?></td>
      <td><a href="<?php echo urlControl('delete='.$autorisation) ?>" class="btn btn-danger"><i class="icon icon-trash"></i></a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php
require_once('inc/footer.php');
?>