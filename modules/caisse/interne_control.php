<?php

if (isset($_POST['article'], $_POST['quantite'], $_POST['raison'])) {
    $idvente = genererIDVente();
    $article = intval($_POST['article']);
    $qte = intval($_POST['quantite']);
    $raison = mysqlSecureText($_POST['raison']);
    $user = intval($_SESSION['con-id']);
    
    $req = query("SELECT ID, PrixVente FROM polar_caisse_articles
                                       WHERE EnVente = 1 AND Actif = 1 AND
                                             CodeCaisse = $article");
    if (mysql_num_rows($req) >= 1) {
        $data = mysql_fetch_assoc($req);
        $total = $data['PrixVente'] * $qte;
        $id_article = $data['ID'];

        query("INSERT INTO polar_caisse_ventes
                          (IDVente, Article, Quantite,
                           Date, Finalise, Asso, Client,
                           Facture, PrixFacture, MontantTVA,
                           Tarif, MoyenPaiement, Permanencier)
                          VALUES
                          ($idvente, $id_article, $qte,
                           NOW(), 1, NULL, '$raison',
                           0, $total, 0,
                           'normal', 'interne', $user)");
        query("UPDATE polar_caisse_articles SET Stock = Stock - $qte  WHERE ID = $id_article");
    } else {
        ajouterErreur('Article introuvable');
    }
} else {
    ajouterErreur('Merci de choisir un article, une quantité et une raison');
}
header('Location: '.$racine.$module.'/'.$section);
?>