<?php

$depenses = query('(SELECT pcv.IDVente, pcv.Date, pcv.Quantite, pca.Nom AS Article, pu.Nom, pu.Prenom, pcv.Client
                    FROM polar_caisse_ventes pcv
                    JOIN polar_caisse_articles pca ON pcv.Article = pca.ID
                    JOIN polar_utilisateurs pu ON pcv.Permanencier = pu.ID
                    WHERE pcv.MoyenPaiement LIKE \'interne\' AND
                          pca.EnVente = 1 AND pca.Actif = 1)
                   UNION
                   (SELECT pcv.IDVente, pcv.Date, pcv.Quantite, pca.Nom AS Article, pu.Nom, pu.Prenom, pcv.Client
                    FROM polar_caisse_ventes_global pcv
                    JOIN polar_caisse_articles pca ON pcv.Article = pca.ID
                    JOIN polar_utilisateurs pu ON pcv.Permanencier = pu.ID
                    WHERE pcv.MoyenPaiement LIKE \'interne\' AND
                          pca.EnVente = 1 AND pca.Actif = 1
                    ORDER BY pcv.IDVente DESC
                    LIMIT 15)
                   ORDER BY IDVente DESC');

addFooter('
  <script type="text/javascript">var racine = "'.$racine.'";
          ajouterLigne = function() {};
          selectCase = function() {};
          majTotal = function() {
              $("#nom-article").autocomplete(prod_designation_filled, {
                      matchContains: true,
                      minChars: 0
              }).result(function(event, data, formatted) {
                      $("#code-article").val(prod_designation.indexOf(formatted));
$("#quantite").select();
              }).focus();
         };
$(function() {
   $("#code-article").change(function() {
var val = $(this).val();
if (val == "")
return;
var nom = prod_designation[val];
if (nom == undefined) {
alert("Code caisse invalide");
$(this).val();
} else {
$("#nom-article").val(nom);
$("#quantite").select();
}
});
});
</script>
  <script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
  <script type="text/javascript" src="'.$racine.'js/caisse_recupdata.js"></script>
  <link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />');

require('inc/header.php');
?>

<h1>Noter une dépense</h1>

<?php afficherErreurs(); ?>

<form action="<?php echo urlControl() ?>" method="POST" class="form-horizontal">
<div class="control-group">
<label for="code-article" class="control-label">Code Caisse</label>
<div class="controls">
  <input type="text" id="code-article" name="article" class="input-mini"/>
</div>
</div>
<div class="control-group">
<label for="nom-article" class="control-label">Désignation</label>
<div class="controls">
  <input type="text" id="nom-article" class="input-xlarge" />
</div>
</div>
<div class="control-group">
<label for="quantite" class="control-label">Quantité</label>
<div class="controls">
  <input type="text" value="1" name="quantite" id="quantite" />
</div>
</div>
<div class="control-group">
<label for="raison" class="control-label">Raison de l'utilisation</label>
<div class="controls">
  <input type="text" id="raison" name="raison" />
</div>
</div>
<div class="control-group">
<div class="controls">
  <input type="submit" value="Enregistrer" class="btn btn-primary" />
</div>
</div>
</form>

<h1>Dernières dépenses</h1>

<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>Permanencier</th>
      <th>Date</th>
      <th>Article</th>
      <th>Quantite</th>
      <th>Raison</th>
    </tr>
  </thead>
  <tbody>
    <?php while($data = mysql_fetch_assoc($depenses)): ?>
    <tr>
      <td><?php echo $data['Prenom'] . ' ' . $data['Nom'] ?></td>
      <td><?php echo $data['Date'] ?></td>
      <td><?php echo $data['Article'] ?></td>
      <td><?php echo $data['Quantite'] ?></td>
      <td><?php echo $data['Client'] ?></td>
    </tr>
    <?php endwhile; ?>
  </tbody>
</table>

<?php require('inc/footer.php'); ?>
