<?php
/**
 * Une partie de ce fichier s'occupe de la sauvegarde.
 * Le script effectue un dump de la base SQL, le crypte et
 * l'envoie par FTP sur un serveur externe.
 *
 * Pour récupérer une sauvegarde :
 * 1. Décrypter : openssl aes-256-cbc -d -a -in Sauvegarde_YYYYMMJJ.sql.gz.aes -out Sauvegarde_YYYYMMJJ.sql.gz
 * 2. Saisir/copier la clé qui est ci-dessous
 * 3. Décompresser : gunzip Sauvegarde_xxxxxx.sql.gz
 * 4. Reste à importer le fichier Sauvegarde_xxxxxx.sql
 *
 * @author Arthur Puyou
 * @author Florent Thévenet
 */

if(isset($_POST['ConfirmerVider'])){
    if (!isset($_POST['100e'], $_POST['50e'], $_POST['20e'], $_POST['10e'],
               $_POST['5e'], $_POST['2e'], $_POST['1e'], $_POST['50c'],
               $_POST['20c'], $_POST['10c'], $_POST['5c'], $_POST['2c'],
               $_POST['1c'], $_POST['theoriqueEspeces'], $_POST['dernierId'],
               $_POST['theoriqueCB'], $_POST['theoriqueMoneo'],
               $_POST['theoriquePayutc'], $_POST['telCB'],
               $_POST['telMoneo'], $_POST['revenuePayutc'],
               $_POST['verscoffreBillets'], $_POST['verscoffrePieces'],
               $_POST['verscaisseBillets'], $_POST['verscaissePieces']))
        redirectWithErrors('', 'Champ(s) manquant(s)');

    $db->beginTransaction();
    try {
        // Initialisation des objets pour tout stocker
        $caisse = new Caisse();
        $details = $caisse->getDetails();
        $caisse->Commentaire = '';
        $mouvement = $caisse->getMouvement();
        $mouvement->Date = raw('NOW()');
        $mouvement->Commentaire = 'Caisse du '.date("d/m/Y");
        $mouvement->User = $user;
        $mouvement->Echange = 'o';

        $caisse->DernierIDVente = intval($_POST['dernierId']);

        // Détail des pièces
        $details->e100 = intval($_POST['100e']);
        $details->e50 = intval($_POST['50e']);
        $details->e20 = intval($_POST['20e']);
        $details->e10 = intval($_POST['10e']);
        $details->e5 = intval($_POST['5e']);
        $details->e2 = intval($_POST['2e']);
        $details->e1 = intval($_POST['1e']);
        $details->c50 = intval($_POST['50c']);
        $details->c20 = intval($_POST['20c']);
        $details->c10 = intval($_POST['10c']);
        $details->c5 = intval($_POST['5c']);
        $details->c2 = intval($_POST['2c']);
        $details->c1 = intval($_POST['1c']);

        // Récupération des montants théoriques
        $caisse->TheoriqueEspeces = (float)($_POST['theoriqueEspeces']);
        $caisse->TheoriqueCB = (float)($_POST['theoriqueCB']);
        $caisse->TheoriqueMoneo = (float)($_POST['theoriqueMoneo']);
        $caisse->TheoriquePayutc = (float)($_POST['theoriquePayutc']);

        // Récupération des montants réels
        $caisse->ReelCB = (float)($_POST['telCB']);
        $caisse->ReelMoneo = (float)($_POST['telMoneo']);
        $caisse->ReelPayutc = (float)($_POST['revenuePayutc']);
        $caisse->ReelEspeces = $details->getTotal();

        // Récupération des transferts
        $verscoffreBillets = (float)($_POST['verscoffreBillets']);
        $verscoffrePieces = (float)($_POST['verscoffrePieces']);
        $verscoffre = $verscoffrePieces + $verscoffreBillets;
        $verscaisseBillets = (float)($_POST['verscaisseBillets']);
        $verscaissePieces = (float)($_POST['verscaissePieces']);
        $verscaisse = $verscaissePieces + $verscaisseBillets;

        $mouvement->Billets = $verscoffreBillets - $verscaisseBillets;
        $mouvement->Pieces = $verscoffrePieces - $verscaissePieces;

        // Espèces dans la caisse après le transfert
        $caisse->EspecesApres = $caisse->ReelEspeces + $verscaisse - $verscoffre;

        // On marque les chèques comme OK
        Cheque::update()->set_value('PEC', 1)
            ->where('PEC = 0 AND NumVente <= ? AND NumVente != -1',
                    $caisse->DernierIDVente)
            ->execute();

        // On transfère les ventes dans la table globale
        $db->query("INSERT INTO polar_caisse_ventes_global (IDVente, Article, Quantite, Date, Finalise, Asso, Client, Facture, PrixFacture, MontantTVA, Tarif, MoyenPaiement, Permanencier) (SELECT IDVente, Article, Quantite, Date, Finalise, Asso, Client, Facture, PrixFacture, MontantTVA, Tarif, MoyenPaiement, Permanencier FROM `polar_caisse_ventes` WHERE IDVente <= ".$caisse->DernierIDVente.")");
        $db->q_delete('Vente')->where('IDVente <= ?', $caisse->DernierIDVente)
            ->execute();

        // On sauvegarde le tout
        $mouvement->save();
        $details->save();
        $caisse->save();

        $db->commit();
        Log::info('Caisse vidée', array('donnees' => $_POST,
                                        'mouvement' => $mouvement,
                                        'details' => $details,
                                        'caisse' => $caisse
                                        ));

        redirectOk('Backup', 'Caisse vidée avec succès ! Maintenant, direction la sauvegarde !');
    } catch(PDOException $e) {
        $db->rollback();
        var_dump($e);
        redirectWithErrors('', 'Erreur lors de la sauvegarde : '.
                           get_class($e)."\n".$e->getMessage());
    }
}
elseif(isset($_GET['SupprimerCheque'])){
    Log::info('Vider la caisse : Supression du chèque '.$_GET['SupprimerCheque']);
query('DELETE FROM polar_caisse_cheques WHERE ID='.intval($_GET['SupprimerCheque']));
	header("Location: $racine$module/$section");
}
elseif(isset($_GET['BackupDB'])){
	require_once("inc/backup.php");
    $start = getTime();
    Log::info('Vider la caisse : début de la sauvegarde');

    $fichierDump = gzopen($dossier."Sauvegarde_".date("Ymd").".sql.gz", "wb9");
	$query = "-- ----------------------\n";
	$query .= "-- dump de la base du Polar au ".date("d-M-Y")."\n";
	$query .= "-- ----------------------\n\n\n";
	$query .= "SET FOREIGN_KEY_CHECKS = 0;\n\n\n";
	gzwrite($fichierDump, $query);

	$tables = query('SHOW TABLES');
	while($row = mysql_fetch_row($tables))
		$table_list[] = $row[0];

	for($i = 0; $i < count($table_list); $i++){
        // On sauvegarde seulement une partie des plus grosses tables (ex caisse = 25Mo)
        if ($table_list[$i] == 'polar_caisse_ventes_global')
            $results = query('SELECT * FROM '.$table_list[$i].' WHERE ID >= 127081'); // 1er janvier 2013
        else if ($table_list[$i] == 'polar_commandes')
            $results = query('SELECT * FROM '.$table_list[$i].' WHERE ID >= 35350'); // 1er janvier 2013
        else if ($table_list[$i] == 'polar_commandes_contenu')
            $results = query('SELECT * FROM '.$table_list[$i].' WHERE ID >= 23495'); // contenu de la commande 35350
        else
            $results = query('SELECT * FROM '.$table_list[$i]);
		$query = "-- -----------------------------\n";
		$query .= "-- insertions dans la table ".$table_list[$i]."\n";
		$query .= "-- -----------------------------\n";
		gzwrite($fichierDump, $query);

		while($row = mysql_fetch_assoc($results)){
			$query = 'INSERT INTO `' . $table_list[$i] .'` (';
			$data = Array();
			while(list($key, $value) = each($row)){
				$data['keys'][] = $key;
				if($value === NULL)
					$data['values'][] = "NULL";
				else
					$data['values'][] = "'".mysql_real_escape_string($value)."'";
		 	}
			$query .= join($data['keys'], ', ') . ') VALUES (' . join($data['values'], ', ') . ');' . "\n";
			gzwrite($fichierDump, $query);
		}
		gzwrite($fichierDump, "\n\n");
	}

	$query = "SET FOREIGN_KEY_CHECKS = 1;\n";
	gzwrite($fichierDump, $query);

	gzclose($fichierDump);

    $duration = getTime() - $start;
    Log::info('Vider la caisse : fin de la sauvegarde ('.$duration.'ms)');
    echo 'ok';
}
elseif(isset($_GET['EncryptDB'])){
	require_once("inc/backup.php");
    $start = getTime();
    Log::info('Vider la caisse : début du chiffrage');

	// Lecture de l'archivage
	if(!$fp = fopen($dossier."Sauvegarde_".date("Ymd").".sql.gz", "rb"))
		die("Erreur d'ouverture de l'archive.");
	$archive = '';
	while (!feof($fp))
	  $archive .= fread($fp, 8192);
	fclose($fp);

	if(strlen($archive) == 0) {
        Log::warning('Vider la caisse : chiffrage impossible, archive vide');
		die("Erreur de lecture de l'archive.");
    }

	// Cryptage
	require_once('lib/Crypt/AES.php');
	$aes = new Crypt_AES(CRYPT_AES_MODE_CBC);
    $aes->setKey($clecryptage);
	$aes->setKeyLength(256);
	$output = $aes->encrypt($archive);

	// Écriture des données cryptées
	if(!$fp = fopen($dossier."Sauvegarde_".date("Ymd").".sql.gz.aes", "wb")) {
        Log::warning('Vider la caisse : chiffrage impossible, impossible d\'ouvrir le fichier chiffré');
		die("Erreur d'ouverture du fichier crypté.");
    }
	if(fwrite($fp, $output) === FALSE) {
        Log::warning('Vider la caisse : chiffrage impossible, erreur d\'écriture du fichier chiffré');
		die("Erreur d'écriture.");
    }
	fclose($fp);

    $duration = getTime() - $start;
    Log::info('Vider la caisse : fin du chiffrage ('.$duration.'ms)');
	echo 'ok';
}
elseif(isset($_GET['UploadDB'])){
	require_once("inc/backup.php");
    $start = getTime();
    Log::info('Vider la caisse : début de l\'envoi');
    // Initialisation de cURL
	$ch = curl_init();

	$file = fopen($dossier."Sauvegarde_".date("Ymd").".sql.gz.aes", "rb");

	// Paramètres
	curl_setopt($ch, CURLOPT_URL, "ftp://$ftpuser:$ftppass@$ftphost/Sauvegarde_".date("Ymd").".sql.gz.aes");
	curl_setopt($ch, CURLOPT_PROXY, $ftpproxy);
	curl_setopt($ch, CURLOPT_UPLOAD, true);
	curl_setopt($ch, CURLOPT_INFILE, $file);
	curl_setopt($ch, CURLOPT_INFILESIZE, filesize($dossier."Sauvegarde_".date("Ymd").".sql.gz.aes"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_exec($ch);
	if(curl_errno($ch) == 0)
		echo 'ok';
	else {
        $erreur = curl_error($ch);
		echo 'Erreur: '.$erreur;
        Log::warning('Vider la caisse : erreur lors de l\'envoi', array('erreur' => $erreur));
    }
	curl_close($ch);

    $duration = getTime() - $start;
    Log::info('Vider la caisse : fin de l\'envoi ('.$duration.'ms)');
}
if(isset($_GET['CleanDB'])){
	require_once("inc/backup.php");
	// Nettoyage (on garde la sauvegarde compressée non-cryptée)
	unlink($dossier."Sauvegarde_".date("Ymd").".sql.gz.aes");
	echo 'ok';
}
?>
