<?php

class ErreurCaisse extends PolarException { }

function getPost($name, $default=NULL) {
    return isset($_POST[$name]) ? $_POST[$name] : $default;
}

function doCommande($ligne, $vente) {
    $param = $_POST["param_$ligne"];
    Log::info('Caisse : Vente '.$vente->IDVente.', param : '.$param);

    $param = intval($param);
    // Si on a un paramètre, on valide la commande
    if($param > 0){
        Commande::update()
            ->set_value('DatePaiement', raw('NOW()'))
            ->set_value('IDVente', $vente->IDVente)
            ->where('ID = ?', $param)
            ->execute();
    }
}

// Requête sur le contenu d'une commande
if(isset($_GET['commande'])){
	$param = intval($_GET['param']);
    if (isset($_GET['code'])) {
        $codeProduit = intval($_GET['code']);
    } else {
        $codeProduit = -1;
    }

	$req = query("SELECT pc.Nom AS NomClient, pc.Prenom, pc.Mail, SUM(Quantite) AS Qte, SUM(PrixTTC) AS PrixTTC,CodeProduit, DatePaiement, DatePrete, DateRetrait, pca.Nom FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
		INNER JOIN polar_commandes_types pct ON pc.Type = pct.ID
		INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
		WHERE pc.ID = $param AND pca.Actif = 1 AND pca.EnVente = 1 AND pc.Termine = 0
		GROUP BY CodeProduit
	");
	if(mysql_num_rows($req) == 0)
		$result['message'] = "Cette commande n'existe pas.";
	else if(mysql_num_rows($req) == 1){
		$data = mysql_fetch_array($req);
        // La commande est déjà payée
		if(!empty($data['DatePaiement'])) {
            if(empty($data['DatePrete'])) {
                $result['message'] = "Cette commande est déjà payée et n'est pas encore prête.";
            }
            else {
                // On marque la comande comme retirée si elle est prête
                if(empty($data['DateRetrait'])) {
                    query("UPDATE polar_commandes SET DateRetrait=NOW(), IDRetrait=".$_SESSION['con-id']." WHERE ID=$param");
                    $result['message'] = "Commande n°$param marquée comme retirée !\n\n(".unhtmlentities($data['Prenom'])." ".unhtmlentities($data['NomClient']).")";
                } else {
                    $result['message'] = "Cette commande a déjà été retirée";
                }
            }
        }
		else if($data['CodeProduit'] == $codeProduit){ // On vérifie que l'on paie la bonne chose
			$result['quantite'] = !empty($data['Qte']) ? $data['Qte'] : 1;
			$result['prix'] = round($data['PrixTTC'], 2);
			$result['param'] = $param;
			if(!empty($data['NomClient']) && !empty($data['Prenom']) && !empty($data['Mail']))
               			$result['messageok'] = "Vérifie le propriétaire de la commande n°$param :\n\n".unhtmlentities($data['Prenom'])." ".unhtmlentities($data['NomClient'])." (".unhtmlentities($data['Mail']).")";
		}
        else if($codeProduit == -1) { // Auto-commande
			$result['quantite'] = !empty($data['Qte']) ? $data['Qte'] : 1;
			$result['prix'] = round($data['PrixTTC'], 2);
			$result['param'] = $param;
            $result['code'] = $data['CodeProduit'];
			if(!empty($data['NomClient']) && !empty($data['Prenom']) && !empty($data['Mail']))
                $result['messageok'] = "Vérifie le propriétaire de la commande n°$param :\n\n".unhtmlentities($data['Prenom'])." ".unhtmlentities($data['NomClient'])." (".unhtmlentities($data['Mail']).")\n\n".unhtmlentities($data['Nom']);
        }
		else
			$result['message'] = "Le numéro de commande ne correspond pas à l'article saisi mais à : ".unhtmlentities($data['Nom']);
	}
	else {
		$result['message'] = 'La commande existe mais ne peut pas être réglée à la caisse. Contactez un membre du bureau.';
	}

	echo json_encode($result);
}
// Liste des articles
else if(isset($_GET['articles'])){
	$data = query("SELECT pca.*,
		 			(CASE WHEN pct.CodeProduit IS NULL THEN 0 ELSE 1 END) AS NeedParametre,
					(CASE WHEN pct.RequireCommande IS NULL THEN 0 ELSE pct.RequireCommande END) AS RequireCommande
					FROM polar_caisse_articles pca
					LEFT JOIN polar_commandes_types pct
					ON pct.CodeProduit=pca.CodeCaisse
					WHERE pca.Actif=1 AND EnVente=1");
	echo "<listearticles>\n";
	while($article = mysql_fetch_array($data)){
		$prixAsso = !empty($article['PrixVenteAsso']) ? $article['PrixVenteAsso'] : $article['PrixVente'];

		echo "\t<article>\n";
		echo "\t\t<code>".$article['CodeCaisse']."</code>\n";
		echo "\t\t<designation>".$article['Nom']."</designation>\n";
		echo "\t\t<prix>".$article['PrixVente']."</prix>\n";
		echo "\t\t<prixasso>".$prixAsso."</prixasso>\n";
		echo "\t\t<requireparam>".$article['NeedParametre']."</requireparam>\n";
		echo "\t\t<requireparamobligatoire>".$article['RequireCommande']."</requireparamobligatoire>\n";
		echo "\t\t<remise1>".$article['Remise1']."</remise1>\n";
		echo "\t\t<remise2>".$article['Remise2']."</remise2>\n";
		echo "\t\t<palier1>".$article['Palier1']."</palier1>\n";
		echo "\t\t<palier2>".$article['Palier2']."</palier2>\n";
		echo "\t\t<codejs>".$article['CodeJS']."</codejs>\n";
		echo "\t\t<ean13>".$article['EAN13']."</ean13>\n";
		echo "\t</article>\n";
	}
	echo "</listearticles>\n";
}
// Validation d'une vente
else if(isset($_POST['modepaiement'])){
    try {
    if (!caisseAutorisee())
        throw new ErreurCaisse('Navigateur non autorisé !');

    $db->beginTransaction();

	$raison = getPost('representant', '');
	$modePaiement = getPost('modepaiement');
    if ($modePaiement == NULL)
        throw new ErreurCaisse("Aucun mode de paiement précisé", 6);
	$tarif = getPost('tarif', 'normal');

	// On vérifie le mot de passe si c'est une assos
	if($modePaiement == 'assos'){
        if (!isset($_POST['asso']))
            throw new ErreurCaisse("Paiement par compte asso mais aucun compte précisé", 4);

        $compte = CompteAsso::select('CompteAsso.*')
            ->select('COALESCE((SELECT SUM(`PrixFacture`) FROM `polar_caisse_ventes`
                               WHERE Asso = CompteAsso.ID AND `Finalise` = 0 ),0) +
                      COALESCE((SELECT SUM(`PrixFacture`) FROM `polar_caisse_ventes_global`
                               WHERE Asso = CompteAsso.ID AND `Finalise` =0 ), 0)', 'EnCours')
            ->where('CompteAsso.ID = ?', intval($_POST['asso']))
            ->getOne();
        if (is_null($compte))
            throw new ErreurCaisse("Compte invalide", 5);
        else if (is_null($compte->DateActivation))
            throw new ErreurCaisse("Compte désactivé", 10);

        $checkAssoMdp = $compte->checkMotDePasse($_POST['motdepasse']);
        $checkBureauMdp = checkMotDePasseBureau($_POST['motdepasse']);

		if(!$checkAssoMdp && !$checkBureauMdp)
			throw new ErreurCaisse('Mauvais mot de passe', 1);
	} else {
        $compte = NULL;
    }

    if($modePaiement == 'payutc') {
        if(!isset($_POST['idetu']))
            throw new ErreurCaisse("Pas d'identifiant de carte étudiant transmis.", 2);
        $payutc = true;
    } else {
        $payutc = false;
    }

	// On traite chacun des articles
    if (!isset($_POST["article_1"]))
        throw new ErreurCaisse("Aucun article", 3);

    // on crée le premier article qui va nous servir de modèle pour la suite
    $vente1 = new Vente(intval($_POST["article_1"]), intval($_POST["quantite_1"]),
                         $modePaiement, $user,
                         $tarif, $compte, $raison);
    $total = $vente1->PrixFacture;
    $vente1->save();
    doCommande(1, $vente1);

    // On a une liste d'objets à envoyer à payutc
    if ($payutc) {
        $payutc_objects = array($vente1->getPayutcArray());
    }

	for($ligne = 2; isset($_POST["article_$ligne"]); $ligne++) {
        $vente = $vente1->creerSimilaire(intval($_POST["article_$ligne"]),
                                             intval($_POST["quantite_$ligne"]));
        $total += $vente->PrixFacture;
        $vente->save();
        doCommande($ligne, $vente1);

        if ($payutc)
            $payutc_objects[] = $vente->getPayutcArray();
	}

    if ($compte != NULL) {
        if ($compte->Quota != NULL && $total + $compte->EnCours > $compte->Quota)
            throw new ErreurCaisse("Quota dépassé !\nVous pouvez payer ".formatPrix($compte->Quota)."€ avec le compte asso\net le reste par un autre moyen de paiement (sans tarif asso)", 12);
    }

    // Envoie de la transaction à payutc, les erreurs renvoyées sont
    // compréhensibles par tous donc on les transmet directement à la caisse
    if ($payutc) {
        error_log(json_encode($payutc_objects));
        $poss = Payutc::loadService('POSS3', 'caisse');
        try {
            $r = $poss->transaction(array(
                             'fun_id' => $CONF['payutc_fundation'],
                             'badge_id' => $_POST['idetu'],
                             'obj_ids' => json_encode($payutc_objects)
                                               ));

            // On sauvegarde le lien IdVente <-> TransactionId
            $vpayutc = new VentePayutc($r->transaction_id, $vente1->IDVente);
            $vpayutc->save();

            $message = 'Merci '.$r->firstname.' '.$r->lastname."<br/>" .
                'Il te reste '.formatPrix($r->solde/100).' €';
        } catch (\JsonClient\JsonException $e) {
            $message = $e->getMessage();
            if ($e->getType() == "Payutc\Exception\NotEnoughMoney")
                $message = "Pas assez d'argent sur le compte payutc.";
            throw new ErreurCaisse($message, 8, $e);
        }
    }

	// Insertion du chèque dans la table des chèques
	if($modePaiement == 'cheque') {
        if (!isset($_POST['banque'], $_POST['emetteur']))
            throw new ErreurCaisse("Paiement par chèque mais la banque et l'emetteur ne sont pas précisés !", 7);

        $cheque = new Cheque(array('Banque' => $_POST['banque'],
                                   'Montant' => $total,
                                   'Date' => raw('NOW()'),
                                   'Emetteur' => $_POST['emetteur'],
                                   'Ordre' => raw("'Le Polar'"),
                                   'PEC' => raw("0"),
                                   'NumVente' => $vente1->IDVente
                                   ));
        $cheque->save();
	}

    // On sauvegarde le tout
    $db->commit();

	// Et on met à jour le stock
	MAJStock($vente1->IDVente);

    echo json_encode(array('idvente'=> $vente1->IDVente,
                           'message' => (isset($message) ? $message : '')
                           ));

    } catch (ErreurCaisse $e) {
        $db->rollBack();
        Log::info("Erreur lors de l'encaissement",
                     array("exception" => $e));
        echo json_encode(array('error' => $e->getCode(),
                               'message' => $e->getMessage()));
    }
}
?>
