<?php
$dossierUpload = "upload/commandes/";

// Nettoyage des vieux fichiers (et retrait de leur nom de polar_commandes_contenu)
function cleanupUploads($type){
	global $dossierUpload;
	$req = query("SELECT pc.ID, pcc.ID AS IDDetail, pcc.Detail FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
		INNER JOIN polar_commandes_types pct ON pct.ID = pc.Type
		WHERE Termine = 1 AND pct.Nom = '$type' AND (DATEDIFF(NOW(), DatePaiement) > 14 OR DATEDIFF(NOW(), DateCommande) > 90) AND pcc.Detail IS NOT NULL");
	$query = "UPDATE polar_commandes_contenu SET Detail = NULL WHERE ID IN(";
	while($res = mysql_fetch_assoc($req)){
		if(is_file($dossierUpload.$res['Detail']))
			unlink($dossierUpload.$res['Detail']);
		$query .= $res['IDDetail'].",";
	}
	$query .= "0)";
	query($query);
}

if(isset($_GET['valider']) && isset($_GET['id_impression'])){
  	// Sécurisation des données entrantes
  	$id = intval($_GET['id_impression']);
	$req = query("SELECT pc.Mail, pcc.Detail, pct.Nom AS Type FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
		INNER JOIN polar_commandes_types pct ON pct.ID=pc.Type
		WHERE pc.ID=$id AND pct.Nom='$nomCommande'
		ORDER BY DateCommande");

	if(mysql_num_rows($req) != 1) die("Mauvais ID.");

   	$donnees = mysql_fetch_assoc($req);

	$iduser = $_SESSION['con-id'];
	query("UPDATE polar_commandes SET DatePrete=NOW(), IDPreparateur=$iduser WHERE ID=$id");

	sendmail($mailExpediteur, 'Le Polar', array($donnees['Mail']), $mailOK[0], str_replace("%id%", $id, $mailOK[1]));

	// Nettoyage des anciennes commandes
	cleanupUploads($donnees['Type']);
	header("Location: $racine$module/$section");
}
elseif(isset($_GET['refuser']) && isset($_POST['id']) && isset($_POST['message'])){
    try {
        $commande = Commande::getById(intval($_POST['id']));
    } catch (UnknownObject $e) {
        redirectWithErrors('', "Cette commande n'existe pas.");
    }

	sendmail($mailExpediteur, 'Le Polar',
             array($commande->Mail, $mailExpediteur),
             $mailProbleme[0],
             str_replace(array("%id%", "%message%"),
                         array($commande->get_id(), nl2br($_POST['message'])),
                         $mailProbleme[1]));

    redirectOk('', 'Message envoyé !');
}
elseif(isset($_GET['supprimer']) && isset($_GET['id_impression'])){
	// Sécurisation des données entrantes
  	$id = intval($_GET['id_impression']);
	$req = query("SELECT pcc.Detail, pct.Nom AS Type FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
		INNER JOIN polar_commandes_types pct ON pct.ID=pc.Type
		WHERE pc.ID=$id AND pct.Nom='$nomCommande'
		ORDER BY DateCommande");

	if(mysql_num_rows($req) != 1) die("Mauvais ID.");

   	$donnees = mysql_fetch_assoc($req);

	query("UPDATE polar_commandes SET Termine=1 WHERE ID=$id");

	// Nettoyage des anciennes commandes
	cleanupUploads($donnees['Type']);
	header("Location: $racine$module/$section");
}
?>
