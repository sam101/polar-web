<?php
$nomCommande = 'Plastification';
$mailExpediteur = "polar-plastification@assos.utc.fr";

$mailOK = array('Plastification terminée',
'Bonjour !<br />Ta commande de plastification vient d\'être réalisée. Passe vite la récupérer muni de ton numéro de commande : %id% !<br />L\'équipe du Polar.');

$mailProbleme = array('Plastification',
                      'Bonjour,<br />La réalisation de ta plastification pose un problème.<br/>
Description du problème : %message% <br/>
Passe au local FE008 pour le régler avec nous.<br />L\'équipe du Polar');

require("_liste_control.php");
?>
