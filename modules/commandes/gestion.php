﻿<?php
$titrePage="Gérer les commandes";

if (isset($_GET['Commande'])) {
    try {
        $commande = Commande::getById(intval($_GET['Commande']));
        $type = $commande->Type;
        $panier = $commande->Panier;
    } catch (UnknownObject $e) {
        $commande = null;
    }
}

addHeaders('<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/jquery.autocomplete.css" />');
addFooter('
<script type="text/javascript" src="'.$racine.'js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="'.$racine.'js/jquery.bgiframe.min.js"></script>
<script type="text/javascript">
var url = "'.urlControl().'";
var autocomplete_url = "'.urlTo('utils', 'login_autocomplete').'";
</script>
<script type="text/javascript" src="'.urlTo('js/modules/gestion_commande.js').'"></script>
');

require('inc/header.php');
afficherErreurs();
?>
<?php
if(!isset($commande)):
    ?>
    <h1>Gestion des commandes</h1>
<p>Cette page permet de modifier l'état d'une commande. Elle fonctionne pour tout type de commande, mais sans aucun contrôle. À utiliser avec précaution !</p>
<form method="get" action="<?php echo $racine.$module.'/'.$section; ?>">
	<table>
		<tr>
			<td>Numéro de la commande :</td>
			<td>
				<input type="text" class="autofocus" name="Commande"/></small>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="Afficher" class="btn" />
			</td>
		</tr>
	</table>
</form>
<?php else: ?>
<p><a class="btn" href="<?php echo urlSection() ?>">Retour</a></p>
<?php
     if ($commande !== null):
?>
<h2>Commande n°<?php echo $commande; ?></h2>
<p>Détails : <?php echo $commande->Nom; ?> <?php echo $commande->Prenom;; ?> (<?php echo $commande->Mail; ?>)</p>
<p>Type : <?php echo $type->Nom; ?></p>
<form method="post" action="<?php echo urlControl('Commande='.$commande); ?>">
<p>Date de commande : <?php echo $commande->DateCommande; ?></p>
<p>Date de paiement : <?php echo $commande->DatePaiement; ?><br /><input type="submit" name="PayNow" value="Payée maintenant" class="btn" /> <input type="submit" name="NoPay" value="Non payée" class="btn" /></p>
<p>Date de préparation : <?php echo $commande->DatePrete; ?><br /><input type="submit" name="ReadyNow" value="Prête maintenant" class="btn" /> <input type="submit" name="NoReady" value="Non prête" class="btn" /></p>
<p>
    <?php if ($type->dateRetraitSouhaitee()): ?>
    Date de retrait souhaitée : <?php echo $commande->DateRetrait; ?>
    <?php 
    $branche = $commande->getBranche();
    if ($branche) {
        echo '(' . $branche . ')';
    } else {}

    else: ?>
    Date de retrait :
    <?php echo $commande->DateRetrait; ?><br /><input type="submit" name="RetraitNow" value="Retirée maintenant" class="btn" /> <input type="submit" name="NoRetrait" value="Non retirée" class="btn" /></p>
    <?php endif; ?>
<p>Date de retour : <?php echo $commande->DateRetour; ?><br /><input type="submit" name="RetourNow" value="Retour maintenant" class="btn" /> <input type="submit" name="NoRetour" value="Non retournée" class="btn" /></p>
</form>
    <?php if ($panier !== null): ?>
    <h2>Panier n° <?php echo $panier ?></h2>
    <p>
    Date de création : <?php echo $panier->DateCreation ?><br/>

    État : <?php echo $panier->getEtatAffichable() ?>
    <?php if($panier->reactivable()) echo '(Réactivable)' ?>
    <br/>

    Utilisateur :
    <?php if ($panier->User === null): ?>
      Aucun
    <?php else: ?>
      <a href="<?php echo urlTo('membres', 'gerer', 'ModifierMembre='.$panier->User) ?>"><?php echo $panier->User->nomComplet(); ?></a>
    <?php endif; ?>
    <br/>

    Mail : <?php echo $panier->Mail; ?><br/>

    ID de transaction Payutc : <?php echo $panier->TransactionID; ?>
    <?php if($panier->Etat === 'sent' || $panier->Etat === 'failed'): ?>
    <a class="btn" href="<?php echo urlTo('commander', 'paiement_control', 'retour='.$panier) ?>">Réessayer de valider la commande</a>
    <?php endif; ?>
    <br/>

    IDVente : <?php echo $panier->IDVente ?><br/>

    Autres commandes :
    <?php foreach($panier->getCommandes() as $c2):
    if ($c2->get_id() != $commande->get_id()): ?>
        <a href="<?php echo urlSection('Commande='.$c2); ?>"><?php echo $c2; ?></a>
    <?php endif; ?>
    <?php endforeach; ?>
    </p>
    <?php else: // pas de panier ?>
    <h2>Panier</h2>
    <p>
      <a class="btn" href="#" id="panier-btn">Créer un panier</a><br/>
      Ce bouton permet de créer un panier contenant cette commande<br/>
      Il faut que l'étudiant ait un compte sur le site du Polar pour pouvoir lui créer un panier, sinon il ne pourra pas le réactiver.<br/>
      L'étudiant recevra un mail récapitulatif avec un lien lui permettant de payer en ligne ou de modifier son panier.<br/>
      Le bouton suit le processus suivant :<br/>
      <ul>
        <li>Recherche d'un utilisateur avec cette adresse mail</li>
        <li>Recherche d'un utilisateur avec ce nom</li>
        <li>Afficher tous les utilisateurs</li>
      </ul>
      Une liste des étudiants possibles sera ensuite affiché et il faudra choisir le bon<br/>
    </p>
    <form class="hide" action="<?php echo urlControl() ?>" method="POST" id="form-panier">
      <input type="hidden" name="createPanier" value=""/>
      <input type="hidden" name="Commande" value="<?php echo $commande ?>"/>
      <select id="select-users" name="User" style="width:100%"></select><br/>
      ou créer à partir du login <input type="text" name="login" id="login"/><br/>
      <input class="btn btn-primary" type="submit" value="Créer"/>
    </form>
    <pre class="hide" id="users-loading">Recherche d'un utilisateur correspondant, merci de patienter...</pre>
    <?php endif; // panier ?>
<?php else: ?>
<p>Commande introuvable.</p>
<?php
    endif;
endif;
require('inc/footer.php');
?>
