<?php
if (isset($_GET['findUser'], $_GET['Commande'])) {
    try {
        $commande = Commande::getById(intval($_GET['Commande']));
    } catch (UnknownObject $e) {
        die();
    }

    // on cherche d'abord l'utilisateur par son adresse mail
    $q = Utilisateur::select()->where('Email = ?', $commande->Mail);
    $res = $q->execute(true);
    if (count($res) == 0) {
        // puis par son nom
        $q = Utilisateur::select()->where('Nom LIKE ?', '%'.$commande->Nom.'%');
        $res = $q->execute(true);
        if (count($res) == 0) {
            $res = Utilisateur::select();
        }
    }

    $users = array();
    foreach($res as $utilisateur) {
        $users[] = array('id' => $utilisateur->get_id(),
                         'nom' => $utilisateur->nomComplet(),
                         'mail' => $utilisateur->Email);
    }

    header('Content-Type: application/json');
    echo json_encode($users);
}
else if (isset($_POST['createPanier'], $_POST['Commande'], $_POST['User'])) {
    try {
        $commande = Commande::getById(intval($_POST['Commande']));
    } catch (UnknownObject $e) {
        redirectWithErrors('', "Cette commande n'existe pas");
    }

    // si on a un login on récupère l'utilisateur à partir du login
    if (!empty($_POST['login'])) {
        try {
            $utilisateur = Utilisateur::create_from_ginger($ginger,
                                                           $_POST['login']);
            if ($utilisateur->get_id() === null) {
                $utilisateur->save();
            }
        } catch (ApiException $e) {
            redirectWithErrors("Commande=".$commande, "Erreur ginger : ".$e->getMessage());
        }
    }
    else {
        try {
            $utilisateur = Utilisateur::getById(intval($_POST['User']));
        } catch (UnknownObject $e) {
            redirectWithErrors("Commande=".$commande, "Cet utilisateur n'existe pas");
        }
    }

    if ($commande->DatePaiement !== null)
        redirectWithErrors("Commande=".$commande, "Cette commande est déjà payée, impossible de créer un panier");

    if ($commande->Panier !== null)
        redirectWithErrors("Commande=".$commande, "Cette commande est déjà dans un panier !!");

    $panier = new PanierWeb();
    $panier->Etat = 'saved';
    $panier->DateCreation = raw('NOW()');
    $panier->User = $utilisateur;
    $panier->Mail = $commande->Mail;
    $panier->save();

    $commande->Panier = $panier;
    $commande->save();

    Log::info('Panier '.$panier.' créé manuellement pour la commande '.$commande);

    CommandesMailer::archiverPanier($panier);
    redirectOk('Commande='.$commande, 'Panier créé');
}
else {
    $id = intval($_GET['Commande']);

    $q = Commande::update()->where('ID = ?', intval($_GET['Commande']));

    if(isset($_POST['PayNow'])) {
        $q->set_value('DatePaiement', raw('NOW()'));
        $q->set_value('IDVente', null);
    }
    elseif(isset($_POST['NoPay'])) {
        $q->set_value('DatePaiement', null);
        $q->set_value('IDVente', null);
    }
    elseif(isset($_POST['ReadyNow'])) {
        $q->set_value('DatePrete', raw('NOW()'));
        $q->set_value('IDPreparateur', $user);
    }
    elseif(isset($_POST['NoReady'])) {
        $q->set_value('DatePrete', null);
        $q->set_value('IDPreparateur', null);
    }
    elseif(isset($_POST['RetraitNow'])) {
        $q->set_value('DateRetrait', raw('NOW()'));
        $q->set_value('IDRetrait', $user);
    }
    elseif(isset($_POST['NoRetrait'])) {
        $q->set_value('DateRetrait', null);
        $q->set_value('IDRetrait', null);
    }
    elseif(isset($_POST['RetourNow'])) {
        $q->set_value('DateRetour', raw('NOW()'));
        $q->set_value('IDRetour', $user);
    }
    elseif(isset($_POST['NoRetour'])) {
        $q->set_value('DateRetour', null);
        $q->set_value('IDRetour', null);
    } else {
        redirectWithErrors("Commande=$id", "Action invalide");
    }

    $q->execute();
    redirectOk("Commande=$id");
}
