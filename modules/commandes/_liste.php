<?php
$dossierUpload = "upload/commandes/";

use_datatables();

if(isset($_GET['tout'])){
	require('inc/header.php');
?>
	<h1>Liste des demandes en attente</h1>
	<p><a href="<?php echo urlSection() ?>" title="Demandes en attente">Afficher les demandes en attente</a></p>
	<table class="datatables table table-bordered table-striped" id="posters">
	<thead>
		<tr>
			<th>ID</th>
			<th>Demandeur</th>
			<th>Fichier</th>
			<th>Qté</th>
			<th>Format</th>
			<th>Envoyé</th>
			<th>Pour</th>
			<th>Imprimé</th>
			<th>Payé</th>
		</tr>
	</thead>
<?php
	$req = query("SELECT pc.ID, pc.Nom, Prenom, DateCommande, DatePrete, DatePaiement, DateRetrait, pca.Nom AS Format,
		pcc.Detail, pcc.Quantite, pc.Termine FROM polar_commandes pc
		LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande=pc.ID
		INNER JOIN polar_commandes_types pct ON pct.ID=pc.Type
		INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse=pct.CodeProduit
		WHERE pca.Actif=1 AND pct.Nom='$nomCommande'
		ORDER BY DateCommande");
	if(mysql_num_rows($req) > 0){
		echo '<tbody>';
		while($donnees = mysql_fetch_assoc($req)) {
			echo '
			<tr>
				<td>'.$donnees['ID'].'</td>
				<td>'.$donnees['Nom'].' '.$donnees['Prenom'].'</td>
				<td><a href="'.urlTo($dossierUpload.$donnees['Detail']).'" title="Télécharger ce fichier.">'.$donnees['Detail'].'</a></td>
				<td>'.$donnees['Quantite'].'</td>
				<td>'.$donnees['Format'].'</td>
				<td>'.$donnees['DateCommande'].'</td>
				<td>'.$donnees['DateRetrait'].'</td>
				<td>'.$donnees['DatePrete'].'</td>
				<td>'.$donnees['DatePaiement'].'</td>';

			echo '</tr>';
		}
		echo '</tbody>';
	}
	else
		echo '
		<tr>
			<td colspan="9">
				<span style="font-style:italic;">Il n\'y a aucune demande en attente pour le moment.</span>
			</td>
		</tr>';
?>
	</table>
<?php
	require('inc/footer.php');
}
else {
	require('inc/header.php');
    addFooter('<script type="text/javascript" src="'.urlTo('js/modules/liste_commandes.js').'"></script>');
    addFooter('<script type="text/javascript">
var server_addr = "https://radium.polar.utc/";
function print_soutenance(id, nom, code, branche) {
    $.post(server_addr+"print_soutenance", {"id" : id, "nom" : nom,
                                        "code" : code, "branche": branche});
}

function print_commande(id, nom, code, type) {
    $.post(server_addr+"print_commande", {"id" : id, "nom" : nom,
                                        "code" : code, "type": type});
}
</script>');
    afficherErreurs();
    $soutenance = Parametre::get('modeSoutenance') == 'on' && $nomCommande == 'Poster';
?>
	<h1>Liste des demandes en attente</h1>
	<p><a href="<?php echo urlSection('tout') ?>" title="Toutes les demandes">Afficher toutes les demandes</a></p>
	<table class="datatables table table-bordered table-striped" id="posters">
	<thead>
		<tr>
			<th>ID</th>
			<th>Demandeur</th>
			<th>Fichier</th>
			<th>Qté</th>
			<th>Article</th>
			<?php if ($soutenance): ?>
            <th>Branche</th>
            <?php else: ?>
            <th>Date Retrait</th>
            <?php endif; ?>
            <th>Pay</th>
            <th>P</th>
			<th>V</th>
			<th>R</th>
			<th>S</th>
		</tr>
	</thead>
	<tbody>
	<?php
    function parseBranche($commande) {
        $branche = $commande->getBranche();
        if ($branche)
            return $branche;
        else
            return "Autre";
    }

    $reqCommandes = Commande::select('Commande.*, Article.Nom AS Format, CodeCaisse, CommandeContenu.Detail, CommandeContenu.Quantite')
        ->leftJoin('CommandeContenu', 'CommandeContenu.IDCommande = Commande.ID')
        ->join('CommandeType', 'CommandeType.ID = Commande.Type')
        ->join('Article', 'Article.CodeCaisse = CommandeType.CodeProduit')
        ->where('Article.Actif = 1 AND CommandeType.Nom = ? AND Commande.Termine = 0 AND Commande.DatePrete IS NULL', $nomCommande)
        ->order('DateCommande');
    $commandes = $reqCommandes->execute(true);

    if (count($commandes) > 0):
        foreach($commandes as $commande):
            $nom = $commande->Nom . ' ' . $commande->Prenom;
    ?>
			<tr>
				<td><?php echo $commande ?></td>
				<td><?php echo $nom ?></td>
				<td><a href="<?php echo urlTo($dossierUpload.$commande->Detail) ?>" title="Télécharger ce fichier."><?php echo $commande->Detail ?></a></td>
				<td><?php echo $commande->Quantite ?></td>
				<td><?php echo $commande->Format ?></td>

                <?php if ($soutenance): ?>
				<td><?php echo parseBranche($commande) ?></td>
                <?php else: ?>
                <td><?php echo $commande->DateRetrait ?></td>
                <?php endif; ?>

				<td><?php echo ( is_null($commande->DatePaiement) ? '<span style="color:red;font-weight:100;">Non</span>' : '<span style="color:green;font-weight:900;">OK</span>' ) ?></td>

                <?php if ($soutenance): ?>
                <td><i onclick="print_soutenance('<?php echo $commande ?>', '<?php echo addslashes($nom) ?>', '<?php echo $commande->get_barcode() ?>', '<?php echo parseBranche($commande) ?>')" class="icon icon-print" style="cursor:pointer;"></i></a></td>
                <?php else: ?>
                <td><i onclick="print_commande('<?php echo $commande ?>', '<?php echo addslashes($nom) ?>', '<?php echo $commande->get_barcode() ?>', '<?php echo $commande->Format ?>')" class="icon icon-print" style="cursor:pointer;"></i></a></td>
                <?php endif; ?>

				<td><a href="<?php echo urlControl('valider&id_impression='.$commande) ?>"><img title="Validez cette impression !" src="<?php echo urlStyle('/icones/ajouter.png') ?>" alt="-" /></a></td>
				<td><img title="Refusez cette impression !" onclick="refuser_commande(<?php echo $commande ?>)" style="cursor:pointer;" src="<?php echo urlStyle('/icones/croix.png') ?>" alt="-" /></td>
				<td><img title="Supprimez cette impression !" onclick="if(confirm('Voulez-vous vraiment SUPPRIMER définitivement cette commande ?')) location.href= '<?php echo urlControl('supprimer&id_impression='.$commande) ?>';" style="cursor:pointer;" src="<?php echo urlStyle('/icones/croix.png') ?>" alt="-" /></td>
			</tr>
		<?php
	      endforeach;
	else: ?>

		<tr>
			<td colspan="10">
				<span style="font-style:italic;">Il n'y a aucune demande en attente pour le moment.</span>
			</td>
		</tr>

    <?php
    endif;
    ?>

	</tbody>
	</table>
	<p><b><?php echo count($commandes); ?> commande(s) en attente.</b></p>

    <div class="modal hide" id="dialogRefuser">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Refuser une commande</h3>
      </div>
      <div class="modal-body">
        <p>Merci d'entrer une description du problème ci-dessous.<br/>
Un mail sera envoyé avec cette description, mais la commande sera toujours affichée sur cette page.</p>
        <form name="formulaire" method="post" enctype="multipart/form-data" action="<?php echo urlControl('refuser') ?>">
        <input type="hidden" name="id" />
        <table>
          <tr><td>Description</td><td><textarea type="text" name="message" class="input-xlarge" rows="6"></textarea></td></tr>

        </table>
        <input type="submit"  value="Envoyer" class="btn" />
      </form>
    </div>
  </div>
<?php
	require('inc/footer.php');
}
?>
