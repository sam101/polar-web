<?php
$titrePage = 'Gestion des systèmes de commande';
require('inc/header.php');
?>
<h1>Modification des systèmes de commande</h1>
<p>Pour qu'un article soit disponible à la commande, le système de commande doit être actif ici ET l'article doit être actif
dans la caisse. Désactiver un article sur cette page permet donc d'empêcher les nouvelles commandes sans bloquer le paiement
de celles déjà en cours.</p>
<p>Vous pouvez également activer le mode "soutenances", dans ce mode les étudiants doivent choisir leur branche au lieu d'une date de retrait dans le formulaire de commande de poster et les tickets des posters mentionnent la branche.</p>
<form method="post" action="<?php echo $racine.$module.'/'.$section; ?>_control">
<?php
$req = query("SELECT pct.ID, pct.Nom, pct.Actif, pca.Nom AS Article
	FROM polar_commandes_types pct
	INNER JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
	WHERE pca.Actif = 1 AND pct.Nom NOT LIKE 'Billet'
	ORDER BY pct.Nom ASC");
$lastSysteme = "";
while($res = mysql_fetch_assoc($req)){
	if($lastSysteme != $res['Nom']){
		if(!empty($lastSysteme))
			echo '</fieldset>';

		echo '<fieldset>
			<legend>'.$res['Nom'].'</legend>';
		$lastSysteme = $res['Nom'];
	}
	?>
	<p>
		<?php echo $res['Article']; ?> :
		<select name="<?php echo $res['ID'] ?>">
			<option value="0"<?php if($res['Actif'] == 0) echo ' selected="selected"'; ?>>Inactif</option>
			<option value="1"<?php if($res['Actif'] == 1) echo ' selected="selected"'; ?>>Actif</option>
		</select>
	</p>
<?php
}
?>
</fieldset>
<fieldset>
  <legend>Mode Soutenance</legend>
  <select name="modeSoutenance">
    <option value="on" <?php if(Parametre::get('modeSoutenance') == 'on') echo ' selected="selected"'; ?>>Actif</option>
    <option value="off"<?php if(Parametre::get('modeSoutenance') != 'on') echo ' selected="selected"'; ?>>Inactif</option>
</select>
</fieldset>
<input type="submit" class="btn" />
</form>
<?php
require('inc/footer.php');
?>
