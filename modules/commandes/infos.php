<?php
use_datatables();
require('inc/header.php');

$commandes = query("SELECT pc.ID, pc.Nom, pc.Prenom, pca.Nom AS TypeCommande, pc.DateCommande, pc.DatePrete, pc.IDPreparateur, pc.DatePaiement, pc.DateRetrait, GROUP_CONCAT(pcc.Detail SEPARATOR '; ') AS Details
FROM polar_commandes_types pct
JOIN polar_commandes pc ON pc.Type = pct.ID
JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
WHERE pca.Actif = 1 AND pca.EnVente = 1 AND pct.nom IN ('Poster', 'Annale', 'Plastification', 'Badge')
             AND ((TO_DAYS(NOW()) - TO_DAYS(pc.DateCommande) <= 15))
             GROUP BY pc.ID");
?>
<h1>Commandes des 15 derniers jours</h1>
<table class="datatables table table-bordered table-striped" id="posters">
    <thead>
        <tr>
            <th>ID</th>
            <th>Demandeur</th>
            <th>Type</th>
            <th>Details</th>
            <th>Envoyé le</th>
            <th>Fait le</th>
            <th>Pour le</th>
    </thead>
    <tbody>
<?php while($data = mysql_fetch_assoc($commandes)) {
    echo "<tr>
            <td>".$data['ID']."</td>
            <td>".$data['Nom']." ".$data['Prenom']."</td>
            <td>".$data['TypeCommande']."</td>
            <td>".$data['Details']."</td>
            <td>".$data['DateCommande']."</td>
            <td>".($data['IDPreparateur'] ? $data['DatePrete'] : "En attente") ."</td>
            <td>".$data['DateRetrait']."</td>
          </tr>";
} ?>
    </tbody>
</table>
<?php
require('inc/footer.php');
?>
