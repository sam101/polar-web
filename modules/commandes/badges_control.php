<?php
$nomCommande = 'Badges';
$mailExpediteur = "polar-badges@assos.utc.fr";

$mailOK = array('Fabrication de badges terminée',
'Bonjour !<br />Ta commande de badges vient d\'être réalisée. Passe vite la récupérer muni de ton numéro de commande : %id% !<br />L\'équipe du Polar.');

$mailProbleme = array('Fabrication de badges',
                      'Bonjour,<br />La réalisation de ta commande de badges pose un problème.<br/>
Description du problème : %message% <br/>
Passe au local FE008 pour le régler avec nous.<br />L\'équipe du Polar');

require("_liste_control.php");
?>
