<?php
$nomCommande = 'Poster';
$mailExpediteur = "polar-posters@assos.utc.fr";

if (Parametre::get('modeSoutenance') == 'on') {
    $mailOK = array("Impression de poster terminée",
                    "Bonjour !<br />Le poster que tu as commandé vient d'être imprimé. Ton poster te sera donné directement par l'UTC sur le lieu des soutenances vendredi 21 février.<br/>Voici ton numéro de commande : %id% !<br /><br />Cordialement,<br />L'équipe du Polar.");
} else {
    $mailOK = array("Impression de poster terminée",
                    "Bonjour !<br />Le poster que tu as commandé vient d'être imprimé. Tu peux venir le récupérer pendant les horaires d'ouverture du Polar muni de ton numéro de commande : %id% !<br /><br />Cordialement,<br />L'équipe du Polar.");
}

$mailProbleme = array('Impression de poster',
	"Bonjour,<br />L'impression de ton poster numéro %id% pose un problème.<br/>
Description du problème : %message% <br/>
Passe au local FE008 pour le régler avec nous.<br />L'équipe du Polar");

require("_liste_control.php");
?>
