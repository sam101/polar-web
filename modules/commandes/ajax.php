<?php

$result = array();

switch($_GET['action']) {
case 'search':
    if (isset($_GET['search'])) {
        $search = $_GET['search'];
        if (preg_match('/^\d+$/', $search)) {
            $query = Commande::select()->where("Commande.ID LIKE ?", $search.'%');
        } else if (isset($_GET['panier'])) {
            $panier = CommandeContenu::select('Detail')
                ->where('IDCommande = ?', intval($_GET['panier']))
                ->limit(1);
            $query = Commande::select()->where("Commande.Panier = (".$panier->get_sql().")")
                ->addSubquery($panier);
        } else {
            $query = Commande::select()->where("(UPPER(CONCAT_WS( '', Commande.Prenom, Commande.Nom)) LIKE UPPER(?) OR UPPER(Commande.Mail) LIKE UPPER(?))", "%$search%", "%$search%");
        }
        if (isset($_GET['code']) && intval($_GET['code']))
            $query->where('Article.CodeCaisse = ?', intval($_GET['code']));
        $query->select('Commande.*, Article.Nom as NomArticle, Article.CodeCaisse, TarifBilletterie.Intitule as NomTarif, Billetterie.Titre as NomBilletterie, CommandeType.Nom as NomType, COALESCE(SUM(CommandeContenu.Quantite), 1) as Quantite')
            ->where('Article.EnVente = 1 AND Article.Actif = 1')
            ->where('Commande.Termine = 0') // 1 = commande annulée
            ->leftJoin('CommandeType', 'CommandeType.ID = Commande.Type')
            ->leftJoin('CommandeContenu', 'CommandeContenu.IDCommande = Commande.ID')
            ->leftJoin('Article', 'Article.CodeCaisse = CommandeType.CodeProduit')
            ->leftJoin('TarifBilletterie', 'TarifBilletterie.TypeCommande = Commande.Type')
            ->leftJoin('Billetterie', 'Billetterie.ID = TarifBilletterie.Evenement')
            ->groupBy('CommandeContenu.IDCommande, Commande.ID')
            ->order('Commande.DatePaiement, Commande.DatePrete, Commande.DateCommande');

        // Limite de lignes à récupérer, pour essayer de ne pas trop
        // détruire les performances
        $limit = isset($_GET['limit']) ? intval($_GET['limit']): 0;
        if ($limit > 0)
            $query->limit($limit);

        foreach($query as $commande) {
            if ($commande->NomType == 'Billet')
                $descType = $commande->NomBilletterie . ' : ' .$commande->NomTarif;
            else
                $descType = $commande->NomArticle;
            $result[] = array('nom'=>unhtmlentities($commande->Nom),
                              'type'=>unhtmlentities($descType),
                              'commande_type'=>$commande->NomType,
                              'article'=>$commande->CodeCaisse,
                              'quantite'=>$commande->Quantite,
                              'codebarre'=>$commande->get_barcode(),
                              'prenom'=>unhtmlentities($commande->Prenom),
                              'commande'=>$commande->DateCommande,
                              'paiement'=>$commande->DatePaiement,
                              'retrait'=>$commande->DateRetrait,
                              'pret'=>$commande->DatePrete,
                              'retour'=>$commande->DateRetour,
                              'id'=>$commande->get_id());
        }
    } else {
        $result[] = 'Terme de recherche manquant';
    }
    break;
case 'retirer':
    if (isset($_GET['id'])) {
        if (intval($_GET['id'])) {
            $req = Commande::update()
                ->set_value('DateRetrait', raw('NOW()'))
                ->set_value('IDRetrait', $user)
                ->where('ID = ? AND DateRetrait IS NULL', intval($_GET['id']));
            if ($req->execute()->rowCount() == 1)
                $result[] = "OK";
            else
                $result[] = "Commande déjà retirée";
        } else {
            $result[] = "ID invalide";
        }
    } else {
        $result[] = "ID de la commande non donné";
    }
    break;
case 'valider_billetterie':
    if (isset($_GET['id'])) {
        if (intval($_GET['id'])) {
            $b = Commande::select('TarifBilletterie.Evenement')
                ->join('TarifBilletterie', 'TarifBilletterie.TypeCommande = Commande.Type')
                ->where('Commande.ID = ?', intval($_GET['id']))
                ->groupBy('TarifBilletterie.Evenement');

            $r = Billetterie::select('Places - COUNT(*)')
                ->join('TarifBilletterie', 'TarifBilletterie.Evenement = Billetterie.ID')
                ->join('Commande', 'Commande.Type = TarifBilletterie.TypeCommande')
                ->addSubquery($b)
                ->where('Billetterie.ID = ('.$b->get_sql().')')
                ->where('Commande.Termine = 0 AND Commande.DatePaiement IS NOT NULL')
                ->rawExecute();
            if ($r->fetchColumn(0) > 0)
                $result[] = "OK";
            else
                $result[] = "nope";
        } else {
            $result[] = "ID invalide";
        }
    } else {
        $result[] = "ID de la commande non donné";
    }
    break;
default:
    $result[] = "Action invalide";
}

header('Content-type: text/json');
echo json_encode($result);

?>