<?php

$nomCommande = 'Poster';
$reqCommandes = Commande::select('Commande.*, Article.Nom AS Format, CodeCaisse, CommandeContenu.Detail, CommandeContenu.Quantite')
    ->leftJoin('CommandeContenu', 'CommandeContenu.IDCommande = Commande.ID')
    ->join('CommandeType', 'CommandeType.ID = Commande.Type')
    ->join('Article', 'Article.CodeCaisse = CommandeType.CodeProduit')
    ->where('Article.Actif = 1 AND CommandeType.Nom = ? AND Commande.Termine = 0 AND Commande.DatePrete IS NULL AND Commande.DatePaiement IS NULL', $nomCommande);

echo '<pre>';
foreach ($reqCommandes as $commande) {
    echo $commande->Mail;
    try {
        CommandesMailer::rappelPaiementCommande($commande);
    }
    catch (MailerException $e) {
        echo " (" . $e->getMessage() . ")";
    }
    echo "\n";
}
echo '</pre>';

