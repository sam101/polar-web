<?php

$nom = mysqlSecureText($user->Nom);
$prenom = mysqlSecureText($user->Prenom);
$email = mysqlSecureText($user->Email);

$req = "SELECT pc.ID, pc.Nom, pc.Prenom, pca.Nom AS TypeCommande, pc.DateCommande, pct.nom AS NomTypeCommande, pc.DatePrete, pc.IDPreparateur, pc.DatePaiement, pc.DateRetrait, GROUP_CONCAT(pcc.Detail SEPARATOR '; ') AS Details, GROUP_CONCAT(pcc.Quantite) AS Quantites, pm.UV, pc.DateRetour
FROM polar_commandes_types pct
JOIN polar_commandes pc ON pc.Type = pct.ID
JOIN polar_caisse_articles pca ON pca.CodeCaisse = pct.CodeProduit
LEFT JOIN polar_commandes_contenu pcc ON pcc.IDCommande = pc.ID
LEFT JOIN polar_manuels pm ON pm.ID = SUBSTR(pcc.Detail, 2, 3)+0
WHERE pca.Actif = 1 AND pca.EnVente = 1 AND pct.nom IN ('Poster', 'Annale', 'Plastification', 'Badge', 'Manuel', 'Billet')
             AND ((pc.Nom LIKE '$nom' AND pc.Prenom LIKE '$prenom') OR pc.Mail LIKE '$email')
             GROUP BY pc.ID
             ORDER BY pc.ID DESC";
$commandes = query($req);

$posters = '';
$plastifications = '';
$badges = '';
$annales = '';
$manuels = '';
$billetterie = '';

function format_commande($data) {
    return "<tr>
            <td>".$data['ID']."</td>
            <td>".$data['TypeCommande']."</td>
            <td>".$data['Quantites']."</td>
            <td>".$data['DateCommande']."</td>
            <td>".$data['DateRetrait']."</td>
            <td>".($data['IDPreparateur'] ? $data['DatePrete'] : "En attente") ."</td>
          </tr>";
}

function format_annales($data) {
    return "<tr>
            <td>".$data['ID']."</td>
            <td>".$data['Details']."</td>
            <td>".$data['DateCommande']."</td>
            <td>".$data['DateRetrait']."</td>
            <td>".($data['IDPreparateur'] ? $data['DatePrete'] : "En attente") ."</td>
          </tr>";
}

function format_manuels($data) {
    return "<tr>
            <td>".$data['ID']."</td>
            <td>".$data['UV']."</td>
            <td>".$data['Details']."</td>
            <td>".$data['DatePaiement']."</td>
            <td>".$data['DateRetrait']."</td>
            <td>".$data['DateRetour']."</td>
          </tr>";
}


function format_billets($data) {
    return "<tr>
            <td>".$data['ID']."</td>
            <td>".$data['TypeCommande']."</td>
            <td>".$data['DateCommande']."</td>
            <td>".$data['DatePaiement']."</td>
          </tr>";
}

while($data = mysql_fetch_assoc($commandes)) {
    switch($data['NomTypeCommande']) {
    case 'Poster':
        $posters .= format_commande($data);
        break;
    case 'Badge':
        $badges .= format_commande($data);
        break;
    case 'Plastification':
        $plastifications .= format_commande($data);
        break;
    case 'Annale':
        $annales .= format_annales($data);
        break;
    case 'Manuel':
        $manuels .= format_manuels($data);
        break;
    case 'Billet':
        $billetterie .= format_billets($data);
    default:
        break;
    }
}

require('inc/header.php');
?>
<h1>Suivi des commandes</h1>
<h2>Posters</h2>
<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>ID</th>
      <th>Type</th>
      <th>Quantité</th>
      <th>Commandé le</th>
      <th>Pour le</th>
      <th>Fait le</th>
    <tr>
  </thead>
  <tbody>
    <?php echo ($posters) ? $posters : '<tr><td></td><td>Aucune demande de poster enregistrée</td><td></td><td></td><td></td><td></td></tr>' ?>
  </tbody>
</table>

<h2>Plastifications</h2>
<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>ID</th>
      <th>Type</th>
      <th>Quantité</th>
      <th>Commandé le</th>
      <th>Pour le</th>
      <th>Fait le</th>
    <tr>
  </thead>
  <tbody>
    <?php echo ($plastifications) ? $plastifications : '<tr><td></td><td>Aucune demande de plastification enregistrée</td><td></td><td></td><td></td><td></td></tr>' ?>
  </tbody>
</table>

<h2>Badges</h2>
<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>ID</th>
      <th>Type</th>
      <th>Quantité</th>
      <th>Commandé le</th>
      <th>Pour le</th>
      <th>Fait le</th>
    <tr>
  </thead>
  <tbody>
    <?php echo ($badges) ? $badges : '<tr><td></td><td>Aucune demande de plastification enregistrée</td><td></td><td></td><td></td><td></td></tr>' ?>
  </tbody>
</table>

<h2>Annales</h2>
<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>ID</th>
      <th>UVs</th>
      <th>Commandé le</th>
      <th>Payé le</th>
      <th>Fait le</th>
    <tr>
  </thead>
  <tbody>
    <?php echo ($annales) ? $annales : '<tr><td></td><td>Aucune commande d\'annales enregistrée</td><td></td><td></td><td></td></tr>' ?>
  </tbody>
</table>

<h2>Manuels</h2>
<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>ID</th>
      <th>UV</th>
      <th>ID Manuel</th>
      <th>Payé le</th>
      <th>Retiré le</th>
      <th>Rendu le</th>
    <tr>
  </thead>
  <tbody>
    <?php echo ($manuels) ? $manuels : '<tr><td></td><td>Aucune location de manuel enregistrée</td><td></td><td></td><td></td><td></td></tr>' ?>
  </tbody>
</table>


<h2>Billetterie</h2>
<table class="datatables table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>ID</th>
      <th>Type</th>
      <th>Commandé le</th>
      <th>Payé le</th>
    <tr>
  </thead>
  <tbody>
    <?php echo ($billetterie) ? $billetterie : '<tr><td></td><td>Aucune réservation enregistrée</td><td></td><td></td><td></tr>' ?>
  </tbody>
</table>

<?php
require('inc/footer.php');
?>
