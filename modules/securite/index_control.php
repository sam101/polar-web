<?php

if(isset($_GET['droits'])){
	// S�curisation des donn�es entrantes
	$id = intval($_POST['ep-id']);

	//Pour toutes les pages, on met � jour les droits
	$req = query('SELECT * FROM polar_securite_pages');
	while($data = mysql_fetch_row($req)){
		//On met � jour le changement des droits
		if(isset($_POST['chk_'.$data[0]]) && $_POST['chk_'.$data[0]] == 'on')
			query('INSERT IGNORE INTO polar_securite_droits (ID, User) VALUES ('.$data[0].','.$id.')'); // La cl� primaire emp�che des ajouts redondants
		else
			query('DELETE FROM polar_securite_droits WHERE ID='.$data[0].' AND User='.$id);
	}
	header('Location: '.$racine.$module.'?droits&id='.$id);
}
else if(isset($_GET['pages'])){
	// S�curisation des donn�es entrantes
	$page = intval($_POST['ep-page']);

	//Pour toutes les pages, on met � jour les droits
	$req = query('SELECT ID FROM polar_utilisateurs');
	while($user = mysql_fetch_assoc($req)){
		//On met � jour le changement des droits
		if($_POST['chk_'.$user['ID']] == 'on')
			query('INSERT IGNORE INTO polar_securite_droits (ID, User) VALUES ('.$page.','.$user['ID'].')'); // La cl� primaire emp�che des ajouts redondants
		else
			query('DELETE FROM polar_securite_droits WHERE ID='.$page.' AND User='.$user['ID']);
	}
	header('Location: '.$racine.$module.'?pages&id='.$page);
}

?>
