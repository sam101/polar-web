<?php
require_once('inc/header.php');
?>
<h1>Liste des droits du site</h1>

<h2>Droits attribués à des utilisateurs non-staff (à surveiller)</h2>
<table class="datatables table table-bordered table-striped table-condensed">
<?php
$req = query("SELECT psp.ID, pu.ID as uID, Titre, Module, psp.ID AS Page, pu.Nom, pu.Prenom FROM polar_securite_pages psp
	LEFT JOIN polar_securite_droits psd ON psp.ID = psd.ID
	LEFT JOIN polar_utilisateurs pu ON psd.User = pu.ID
	WHERE Acces LIKE 'private' AND Staff = 0
	ORDER BY Module, Titre ASC");

$moduleActuel = "";
$titreActuel = "";
while($page = mysql_fetch_assoc($req)){
	if($page['Module'] != $moduleActuel){
		$moduleActuel = $page['Module'];
		echo '<tr><th>'.ucfirst($moduleActuel).'</th></tr>';
	}

	if($page['Titre'] != $titreActuel){
		$titreActuel = $page['Titre'];
		echo '<tr><td><i>'.$page['Titre'].'</i></td>';
	}

	echo "<tr><td>".$page['Nom']." ".$page['Prenom'];
	echo '<a href="'.$racine.$module.'/'.$section.'_control?Supprimer='.$page['ID'].'&user='.$page['uID'].'"><img title="Supprimer" src="'.$racine.'styles/0/icones/croix.png" alt="-" /></a>';
	echo "</td></tr>";
}
?>
</table>

<h2>Tous les droits (modifiables dans le module Droits d'accès)</h2>
<table class="datatables table table-bordered table-striped table-condensed">
<?php
$req = query("SELECT Titre, Module, psp.ID AS Page, pu.Nom, pu.Prenom FROM polar_securite_pages psp
	LEFT JOIN polar_securite_droits psd ON psp.ID = psd.ID
	LEFT JOIN polar_utilisateurs pu ON psd.User = pu.ID
	WHERE Acces LIKE 'private'
	ORDER BY Module, Titre ASC");

$moduleActuel = "";
$titreActuel = "";
while($page = mysql_fetch_assoc($req)){
	if($page['Module'] != $moduleActuel){
		$moduleActuel = $page['Module'];
		echo '<tr><th>'.ucfirst($moduleActuel).'</th></tr>';
	}

	if($page['Titre'] != $titreActuel){
		$titreActuel = $page['Titre'];
		echo '<tr><td><i>'.$page['Titre'].'</i></td>';
	}

	echo "<tr><td>".$page['Nom']." ".$page['Prenom']."</td></tr>";
}
?>
</table>
<?php
require_once('inc/footer.php');
?>
