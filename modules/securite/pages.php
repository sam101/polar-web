<?php
require_once('inc/header.php');
?>
<?php
if(!isset($_GET['id'])){
?>
<h1>Liste des pages du site</h1>
<table class="datatables table table-bordered table-striped table-condensed">
<tr><th>ID</th><th>Titre de la page</th><th>Titre menu</th></tr>
<?php
$req = query('SELECT * FROM polar_securite_pages WHERE Acces LIKE "private" ORDER BY Titre ASC');
while($data = mysql_fetch_assoc($req)){
	echo '<tr>
	<td>'.$data['ID'].'</td><td>';
	echo '<a href="'.$racine.$module.'/'.$section.'?id='.$data['ID'].'" title="G&eacute;rer les droits">'.$data['Titre'].'</a>';
	echo '</td><td>'.$data['Menu'].'</td>
	</tr>';
}

echo '</table>';

}
else {
	$idPage = intval($_GET['id']);
	$req = query("SELECT Titre FROM polar_securite_pages WHERE ID = $idPage");
	$res = mysql_fetch_assoc($req);
	$titre = $res['Titre'];
?>
	<h1>Droits pour la page : <?php echo $titre; ?></h1>
	<p><a href="<?php echo $racine.$module.'/'.$section; ?>" title="">Retour liste des pages</a></p>
	<form name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section.'_control?id='.$idPage; ?>">
	<table class="datatables table table-bordered table-striped table-condensed">
<?php
	$req = query('SELECT * FROM polar_utilisateurs WHERE Staff = 1 ORDER BY Nom ASC');
	echo '<input type="hidden" name="ep-page" value="'.$idPage.'" />';
	while($user = mysql_fetch_assoc($req)){
		echo '<tr><td><label for="chk_'.$user["ID"].'">'.$user['Nom'].' '.$user['Prenom'].'</label></td><td><input type="checkbox" name="chk_'.$user["ID"].'" id="chk_'.$user["ID"].'"';
		$req2 = query('SELECT COUNT(*) FROM polar_securite_droits WHERE ID='.$idPage.' AND User='.$user['ID']);
		$droit = mysql_fetch_row($req2);
		if($droit[0] == 1) echo ' checked=checked';
		echo '/></td></tr>';
	}
?>
	<tr><td colspan="2"><input type="submit" value="Mettre &agrave; jour !" class="btn" /></td></tr>
	</table>
	</form>
<?php
}
require_once('inc/footer.php');
?>
