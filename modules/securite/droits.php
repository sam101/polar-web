<?php
$admin = Payutc::loadService('ADMINRIGHT', 'manager');

require_once('inc/header.php');
?>
<?php afficherErreurs(); ?>
<h1>Liste des droits du site</h1>
<p>Utilisateur :
<?php
$req = query('SELECT ID, Nom, Prenom FROM polar_utilisateurs WHERE Staff = 1 ORDER BY Nom ASC');
echo '<select name="ep-permanencier" onchange="location.href=\''.$racine.$module.'/'.$section.'?id=\'+this.options[this.selectedIndex].value+\'\';">';
echo '<option value="">--</option>';
while($data = mysql_fetch_assoc($req))
 	echo '<option value="'.$data['ID'].'">'.$data['Nom'].' '.$data['Prenom'].'</option>';
echo '</select></p>';

if(isset($_GET['id'])){
	$idUser = intval($_GET['id']);
	$req = query('SELECT Prenom, Nom FROM polar_utilisateurs WHERE ID='.$_GET['id']);
	$data = mysql_fetch_assoc($req);
?>
	<form name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section.'_control?id='.$_GET['id']; ?>">
	<input type="hidden" name="ep-id" value="<?php echo $_GET['id']; ?>" />
	<table class="datatables table table-bordered table-striped table-condensed">
	<tr><th colspan="2">Utilisateur : <?php echo $data['Prenom'].' '.$data['Nom']; ?></th></tr>

<?php
	$req = query("SELECT Titre, Module, psp.ID AS Page, (CASE psd.User WHEN $idUser THEN 1 ELSE 0 END) AS HasDroit FROM polar_securite_pages psp
		LEFT JOIN polar_securite_droits psd ON psp.ID = psd.ID AND psd.User = $idUser
		WHERE Acces LIKE 'private'
		ORDER BY Module, Titre ASC");

	$moduleActuel = "";
	while($page = mysql_fetch_assoc($req)){
		if($page['Module'] != $moduleActuel){
			$moduleActuel = $page['Module'];
			echo '<tr><th>'.ucfirst($moduleActuel).'</th><th></th></tr>';
		}
		echo '<tr><td><label for="chk_'.$page['Page'].'">'.$page['Titre'].'</label></td><td><input type="checkbox" id="chk_'.$page['Page'].'" name="chk_'.$page['Page'].'"';
		if($page['HasDroit'] == 1) echo ' checked=checked';
		echo '/></td></tr>';
	}
?>
	<tr><td colspan="2"><input type="submit" value="Mettre &agrave; jour !" class="btn" /></td></tr>
	</table>
	</form>
<?php
}
require_once('inc/footer.php');
?>
