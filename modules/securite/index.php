<?php
require_once('inc/header.php');

// Affichage des pages
if(isset($_GET['pages'])){
?>
    <h1>Liste des pages du site</h1>
	<p><a href="<?php echo $racine.$module; ?>">Retour</a></p>
	<table class="datatables table table-bordered table-striped table-condensed">
	<tr><th>ID</th><th>Titre de la page</th><th>Titre menu</th></tr>
	<?php
	$req = query('SELECT * FROM polar_securite_pages WHERE Acces LIKE "private" ORDER BY Titre ASC');
	while($data = mysql_fetch_assoc($req)){
		echo '<tr>
		<td>'.$data['ID'].'</td><td>';
		if($data['ID'] == $_GET['id']) echo '<b>';
		echo '<a href="'.$racine.$module.'?pages&id='.$data['ID'].'" title="G&eacute;rer les droits">'.$data['Titre'].'</a>';
		if($data['ID'] == $_GET['id']) echo '</b>';
		echo '</td><td>'.$data['Menu'].'</td>
		</tr>';
	}

	echo '</table>';

	if(isset($_GET['id'])){
	?>
		<form name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section.'_control?pages&id='.$_GET['id']; ?>">
		<table class="datatables table table-bordered table-striped table-condensed">
	<?php
		$req = query('SELECT * FROM polar_utilisateurs WHERE Staff = 1 ORDER BY Nom ASC');
		echo '<input type="hidden" name="ep-page" value="'.$_GET['id'].'" />';
		while($user = mysql_fetch_assoc($req)){
			echo '<tr><td>'.$user['Nom'].' '.$user['Prenom'].'</td><td><input type="checkbox" name="chk_'.$user["ID"].'"';
			$req2 = query('SELECT COUNT(*) FROM polar_securite_droits WHERE ID='.$_GET['id'].' AND User='.$user['ID']);
			$droit = mysql_fetch_row($req2);
			if($droit[0] == 1) echo ' checked=checked';
			echo '/></td></tr>';
		}
	?>
		<tr><td colspan="2"><input type="submit" value="Mettre &agrave; jour !" class="btn" /></td></tr>
		</table>
		</form>
	<?php
	}
}
// Attribution de droits aux utilisateurs
else if(isset($_GET['droits'])){
?>
	<h1>Liste des droits du site</h1>
	<p><a href="<?php echo $racine.$module; ?>">Retour</a></p>
	<p>Utilisateur :
<?php
	$req = query('SELECT ID, Nom, Prenom FROM polar_utilisateurs ORDER BY nom asc');
	echo '<select name="ep-permanencier" onchange="location.href=\'',$racine,$module,'?droits&id=\'+this.options[this.selectedIndex].value+\'\';">';
	echo '<option value="">--</option>';
	while($data=mysql_fetch_assoc($req))
	 	echo '<option value="'.$data['ID'].'">'.$data['Nom'].' '.$data['Prenom'].'</option>';
	echo '</select></p>';

	if(isset($_GET['id'])){
		$req = query('SELECT Prenom, Nom FROM polar_utilisateurs WHERE ID='.$_GET['id']);
		$data = mysql_fetch_assoc($req);
?>
		<form name="formulaire" method="post" style="margin-top:10px;" action="<?php echo $racine.$module.'/'.$section.'_control?droits&id='.$_GET['id']; ?>">
		<table class="datatables table table-bordered table-striped table-condensed">
		<tr><th colspan="2">Utilisateur : <?php echo $data['Prenom'].' '.$data['Nom']; ?></th></tr>
<?php
		$req = query('SELECT * FROM polar_securite_pages WHERE Acces LIKE "private" ORDER BY Titre ASC');
		echo '<input type="hidden" name="ep-id" value="'.$_GET['id'].'" />';
		while($pages = mysql_fetch_assoc($req)){
			echo '<tr><td>'.$pages['Titre'].'</td><td><input type="checkbox" name="chk_'.$pages["ID"].'"';
			$req2 = query('SELECT count(*) FROM polar_securite_droits WHERE ID='.$pages['ID'].' AND User='.$_GET['id']);
			$droit = mysql_fetch_row($req2);
			if($droit[0] == 1) echo ' checked=checked';
			echo '/></td></tr>';
		}
?>
		<tr><td colspan="2"><input type="submit" value="Mettre &agrave; jour !" class="btn" /></td></tr>
		</table>
		</form>
<?php
	}
}
else {
?>
<h1>Activer les droits pour chaque utilisateur du site</h1>
<p>Il y a deux zones d'administration pour les droits des utilisateurs :</p>
<ul>
<li><a href="<?php echo $racine; ?>securite?pages">Liste des pages du site</a></li>
<li><a href="<?php echo $racine; ?>securite?droits">Liste des droits par utilisateur</a></li>
</ul>
<?php
}
require_once('inc/footer.php');
?>
