<?php

$appli = $_GET['appli'];

// est-ce que le nom de l'appli est valide ? (mélange lettres/chiffres/_)
if (preg_match('/^\\w+$/', $appli)) {
    if (file_exists('applis_store/'.$appli.'.tar.gz'))
        echo '{"hash" : "'.md5_file('applis_store/'.$appli.'.tar.gz').'"}';
    else
        echo '{"erreur" : "Ce fichier n\'existe pas"}';
}