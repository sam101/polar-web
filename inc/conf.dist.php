<?php

// Teh fichier de config

$CONF = array(
              // où est-ce que le site est déployé ?
              // sert comme url de service pour le cas
              "polar_url" => "http://localhost".$racine,
              // où est la cas ?
              "cas_url" => "https://cas.utc.fr/cas/",
              // où est ginger ? (service de gestion des cotisants du simde)
              // il nous permet de vérifier si quelqu'un est cotisant et
              // d'obtenir son nom/prénom à partir du login
              "ginger_url" => "https://assos.utc.fr/ginger/v1/",
              // la clé ginger pour le polar
              "ginger_key" => "",
              // À l'intérieur de l'utc on a besoin du proxy
              "proxy" => NULL,
              // Est-ce qu'on active payutc ?
              "use_payutc" => true,
              // où est le serveur payutc ?
              "payutc_url" => "http://localhost/server/web/",
              // clé d'application payutc pour la caisse
              "payutc_caisse_key" => "",
              // clé d'application payutc pour les pages de gestion
              // (articles, membres, droits)
              "payutc_manager_key" => "",
              // L'id de fondation du polar dans payutc
              "payutc_fundation" => 3
 	);

/* URLs pour le CAS
 * On peut les utiliser tel quel sauf pour validate où il faut concatener
 * le ticket à l'url
 */
$CONF["cas_login"] = $CONF["cas_url"] . 'login?renew=true&service=' . $CONF["polar_url"];
$CONF["cas_logout"] = $CONF["cas_url"] . 'logout?url=' . $CONF["polar_url"];
$CONF["cas_validate"] = $CONF["cas_url"] . 'serviceValidate?service=' . $CONF["polar_url"] . "&ticket=";

/* URLS cas pour payutc,
 * On fait pointer à un endroit spécifique du site pour pouvoir
 * le distinguer d'un ticket d'authentification,
 * en effet faut envoyer un ticket qui n'est pas encore validé à payutc
 */
$CONF["polar_url_for_payutc"] = $CONF["polar_url"] . 'payutc/payutc';
$CONF["payutc_cas_login"] = $CONF["cas_url"] . 'login?service=' . $CONF["polar_url_for_payutc"];

?>