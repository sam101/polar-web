      </div> <!-- container -->
    </div> <!-- wrap -->

<?php if(DEBUG == True): ?>
<pre>
  <?php var_dump($db->queries); ?>
</pre>
<?php endif; ?>

    <div id="footer">
      <div class="container muted">
        <hr/>
        <p class="credit text-center">
          Le Polar - Webmaster : <a href="<?php echo urlTo('static', 'contributeurs') ?>" title="Contributeurs">Florent THÉVENET</a> - <a href="<?php echo urlTo('static', 'legal') ?>" title="Mentions légales">Mentions légales</a></p>
      </div>
    </div>

    <script src="<?php echo urlTo('js/jquery-1.8.3.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo urlTo('js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo urlTo('js/polar.js') ?>" type="text/javascript"></script>
    <?php
	if(!empty($extraFooter))
		echo $extraFooter;
	?>
  </body>
</html>
