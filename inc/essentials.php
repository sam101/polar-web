<?php

/** Quelques fonction essentielles, qui doivent être chargées
 * avant le reste du bazar dans func.php
 */

/**
 * Renvoie l'IP du client (y compris via proxy)
 *
 * @return string L'adresse IP
 * @author Arthur Puyou
 */
function get_ip(){
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
		return $ip[0];
	}
	elseif(isset($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
	else
		return $_SERVER['REMOTE_ADDR'];
}

/**
 * Fonction qui renvoie un timestamp précis [microsecondes]
 *
 * @return float Temps en ms
 */
function getTime() {
	$temps = explode(' ', microtime());
	return $temps[1] + $temps[0];
}