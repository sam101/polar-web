<?php

require('lib/payutc.php');

class PayutcTools {
    /**
     * Ajoute un droit payutc à un membre du polar
     * @param $user Membre du Polar (instance d'Utilisateur)
     * @param $right Nom du service payutc, en majuscule
     * @return true si l'insertion à réussi, false sinon
     * Les erreurs sont ajoutées aux erreurs de la session
     */
    public static function addRight($user, $right, $admin=null) {
        global $CONF;
        if (!$CONF['use_payutc'])
            return;
        // pas de payutc pour les extérieurs
        if ($user->Login == '')
            return false;

        if (is_null($admin))
            $admin = Payutc::loadService('ADMINRIGHT', 'manager');

        try {
            $id = $admin->getUserId(array('login' => $user->Login));
            $params = array('usr_id' => $id,
                            'service' => $right,
                            'fun_id' => $CONF['payutc_fundation']
                            );
            $admin->setUserRight($params);
        } catch (\JsonClient\JsonException $e) {
            if ($e->getType() == 'Payutc\Exception\RightAlreadyExistsException')
                return true;
            ajouterErreur('Erreur payutc ('.$e->getType().') : '.$e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Enlève un droit payutc à un membre du polar
     * @param $user Membre du Polar (instance d'Utilisateur)
     * @param $right Nom du service payutc, en majuscule
     * @return true si la suppression à réussi, false sinon
     * Les erreurs sont ajoutées aux erreurs de la session
     */
    public static function removeRight($user, $right, $admin) {
        global $CONF;
        if (!$CONF['use_payutc'])
            return;
        // pas de payutc pour les extérieurs
        if ($user->Login == '')
            return false;

        if (is_null($admin))
            $admin = Payutc::loadService('ADMINRIGHT', 'manager');

        try {
            $id = $admin->getUserId(array('login' => $user->Login));
            $admin->removeUserRight(array('usr_id' => $id,
                                          'service' => $right,
                                          'fun_id' => $CONF['payutc_fundation']
                                          ));
        } catch (\JsonClient\JsonException $e) {
            if ($e->getType() == 'Payutc\Exception\SetRightException')
              return true;
            ajouterErreur('Erreur payutc ('.$e->getType().') : '.$e->getMessage());
            return false;
        }
        return true;
    }

    public static function deleteArticle($article, $gesarticle=NULL) {
        global $CONF;
        if (!$CONF['use_payutc'])
            return;

        if ($gesarticle == NULL)
            $gesarticle = Payutc::loadService('GESARTICLE', 'manager');

        if($article instanceof ArticlePayutc) {
            self::deleteArticle($article->IDPayutc, $gesarticle);
        } elseif(is_numeric($article)) {
            try {
                $gesarticle->deleteProduct(array('obj_id' => $article,
                                                 'fun_id' => $CONF['payutc_fundation'])
                                           );
            } catch (\JsonClient\JsonException $e) {
                ajouterErreur('Erreur payutc ('.$e->getType().') : '.$e->getMessage());
                return false;
            }
        }
    }

    public static function addArticle($article, $gesarticle=NULL) {
        global $CONF;
        if (!$CONF['use_payutc'])
            return;

        if ($gesarticle == NULL)
            $gesarticle = Payutc::loadService('GESARTICLE', 'manager');

        $particle = new ArticlePayutc(array('Article' => $article,
                                            'DateSync' => raw('NOW()')));

        $particle->IDPayutc = self::createArticle($gesarticle,
                                                $article->Nom,
                                                $article->PrixVente,
                                                $article->Secteur);

        $particle->save();
    }

    public static function createArticle($gesarticle, $nom, $prix, $categorie) {
        global $CONF;
        if (!$CONF['use_payutc'])
            return;

        try {
            $r = $gesarticle->setProduct(array('name' => $nom,
                                               'parent' => Secteur::getPayutcId($categorie),
                                               'prix' => $prix*100,
                                               'stock' => NULL,
                                               'alcool' => False,
                                               'image' => NULL,
                                               'fun_id' => $CONF['payutc_fundation'])
                                         );
            return $r->success;
        } catch(\JsonClient\JsonException $e) {
            ajouterErreur('Erreur payutc ('.$e->getType().') : '.$e->getMessage());
            return NULL;
        }
    }
}