<?php
  /*----------------------------------------------------------
     #  Fichier crée le 11/10/2007
     #  Par BkM[123]
     #  Dernière modification le11/10/2007 à 18h19
     #
     #  Fonctions smCode (équivalent de bbCode)
     #----------------------------------------------------------*/

  // Fonction qui transforme des balises en html
  function smCode($texte) {
    $decodeur = Array('');
    $decodeur[1] = ':)';
    $decodeur[2] = '=)';
    $decodeur[3] = ':D';
    $decodeur[4] = ';)';
    $decodeur[5] = '^^';
    $decodeur[6] = ':P';
    $decodeur[7] = '|D';
    $decodeur[8] = ':|';
    $decodeur[9] = ':(';
    $decodeur[10] = ':s';
    $decodeur[11] = ':/ ';
    $decodeur[12] = ':\\';
    $decodeur[13] = ':$';
    $decodeur[14] = '::zz::';
    $decodeur[15] = '::yeah::';
    $decodeur[16] = '(l)';
    $decodeur[17] = '(B)';
    $decodeur[18] = ':@';
    $decodeur[19] = ':O';
    $decodeur[20] = ':\'(';
    $decodeur[21] = '(L)';
    $decodeur[22] = '::666::';
    $decodeur[23] = '::i::';
    $decodeur[24] = '--&gt;';
    $decodeur[25] = '&lt;lien adresse="(.+)"&gt;§<a href="$1">§';
    $decodeur[26] = '&lt;/lien&gt;§</a>';
    $decodeur[27] = '&lt;image adresse="(.+)" \/&gt;§<img src="$1" alt="Image utilisateur" />§';

    $i = 1;

    $texte = preg_replace('#&lt;italique&gt;(.+)&lt;/italique&gt;#isU', '<span class="italique">$1</span>', $texte);
    $texte = preg_replace('#&lt;gras&gt;(.+)&lt;/gras&gt;#isU', '<span class="gras">$1</span>', $texte);
    $texte = preg_replace('#&lt;souligne&gt;(.+)&lt;/souligne&gt;#isU', '<span class="souligne">$1</span>', $texte);
    $texte = preg_replace('#&lt;barre&gt;(.+)&lt;/barre&gt;#isU', '<span class="barre">$1</span>', $texte);
    $texte = preg_replace('#&lt;image adresse=&quot;(.+)&quot; /&gt;#isU', '<img src="$1" alt="Image introuvable" />', $texte);
    $texte = preg_replace('#&lt;lien adresse=&quot;(.+)&quot;&gt;(.+)&lt;/lien&gt;#isU', '<a href="$1">$2</a>', $texte);

    while(isset($decodeur[$i])) {
      $texte = str_replace($decodeur[$i], '<img style="vertical-align:middle;" src="styles/0/smileys/'.$i.'.gif" alt="Smiley '.$i.'" />', $texte);
      $i++;
    }

    return $texte;
  }

?>
