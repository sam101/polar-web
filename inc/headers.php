<?php
/**
 * Ce fichier définit les headers à ajouter en début de page
 * les plus utilisés sur le site.
 *
 * @author Arthur Puyou
 */


// Jquery UI : js et css
define('H_JQUERY_UI', '
	<link href="'.$racine.'lib/jquery-ui/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css"/>
 	<script language="javascript" type="text/javascript" src="'.$racine.'lib/jquery-ui/jquery-ui-1.8.2.custom.min.js"></script>');

function use_jquery_ui() {
    global $racine;
    addHeaders('<link href="'.$racine.'lib/jquery-ui/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css"/>');
    addFooter('<script language="javascript" type="text/javascript" src="'.$racine.'lib/jquery-ui/jquery-ui-1.8.2.custom.min.js"></script>');
}

/**
 * Datables : js et css
 * Ajoute un datatable au tableau ayant la class datatables (max 1)
 */
define('H_DATATABLES', '
	<script type="text/javascript" src="'.$racine.'js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="'.$racine.'js/jquery.dataTables.plugins.js"></script>
	<script type="text/javascript" src="'.$racine.'js/TableTools.min.js"></script>
	<script type="text/javascript" src="'.$racine.'js/ZeroClipboard.js"></script>
	<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/datatable.css" />
	<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/TableTools.css" />
	<script>
	$(document).ready(function() {
		TableToolsInit.oFeatures.bCopy = false;
		TableToolsInit.sSwfPath = "'.$racine.'js/ZeroClipboard.swf";
		theDataTable = $(".datatables").dataTable({
			"sPaginationType": "full_numbers",
			"iDisplayLength": 50,
			"bLengthChange": false,
			"bAutoWidth": false,
			"sDom": \'T<>lfrtip\',
			aaSorting: []
		});
	});
	</script>');

function use_datatables() {
    global $racine;
    addHeaders('
	<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/datatable.css" />
	<link rel="stylesheet" type="text/css" href="'.$racine.'styles/0/TableTools.css" />');
    addFooter('
	<script type="text/javascript" src="'.$racine.'js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="'.$racine.'js/jquery.dataTables.plugins.js"></script>
	<script type="text/javascript" src="'.$racine.'js/TableTools.min.js"></script>
	<script type="text/javascript" src="'.$racine.'js/ZeroClipboard.js"></script>
	<script>
	$(document).ready(function() {
		TableToolsInit.oFeatures.bCopy = false;
		TableToolsInit.sSwfPath = "'.$racine.'js/ZeroClipboard.swf";
		theDataTable = $(".datatables").dataTable({
			"sPaginationType": "full_numbers",
			"iDisplayLength": 50,
			"bLengthChange": false,
			"bAutoWidth": false,
			"sDom": \'T<>lfrtip\',
			aaSorting: []
		});
	});
	</script>');
}