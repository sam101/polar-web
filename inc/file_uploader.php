<?php

require_once('func.php');


interface iFileNameChooser {
    /* Choisi un nom et retourne les données sous la même forme que la fonction @pathinfo() */
    function chooseFileName($dirname, $fileName, $ext);
}

/*
 * Permet de choisir un nom de fichier différent de ceux déjà présents
 * @author Nicolas Pauss
 */
class FileNameChooserWithoutReplace implements iFileNameChooser {

    public function chooseFileName($dirname, $fileName, $ext) {
        $basename = $fileName . '.' . $ext;
        $filePath = $dirname . $basename;

        $i = 0;
        while (file_exists($filePath)) {
            $fileName = $origFileName . '-' . $i;
            $basename =  $fileName . '.' . $ext;
            $filePath = $dirname . $basename;
            $i++;
        }

        return array
        (
          'dirname' => $dirname,
          'basename' => $basename,
          'extension' => $ext,
          'filename' => $fileName
        );
    }
}


/**
 * Permet de choisir un nom de fichier en fonction d'un préfix et du temps à l'exécution
 * @author Nicolas Pauss
 */
class FileNameChooserTimeBased extends FileNameChooserWithoutReplace {

    public function chooseFileName($dirname, $fileName, $ext) {
        $time = (int)(getTime() * 1000);
        $fileName = $fileName . '-'.$time;
        return parent::chooseFileName($dirname, $fileName, $ext);
    }
}

/**
 * Permet de choisir un fichier selon un préfix défini.
 * Le nom du fichier est alors ignoré lors de l'appel à la fonction
 * chooseFileName.
 * @author Nicolas Pauss
 */
class FileNameChooserWithPrefixTimeBased extends FileNameChooserTimeBased {
    private $prefix;

    function __construct($prefix = 'tmp_file') {
        $this->prefix = $prefix;
    }

    public function chooseFileName($dirname, $fileName, $ext) {
        return parent::chooseFileName($dirname, $this->prefix, $ext);
    }
}



/**
 * Permet de choisir un nom de fichier en fonction d'un nom d'utilisateur.
 */
class FileNameChooserUserId implements iFileNameChooser {

    private $user;
    private $replaceOldFile;

    function __construct($user, $replaceOldFile = FALSE) {
        $this->user = $user;
        $this->replaceOldFile = $replaceOldFile;
    }

    public function chooseFileName($dirname, $fileName, $ext) {
        $id = md5(uniqid());
        $fileName = $this->user."_".$id;
        if(!$this->replaceOldFile){
            /// don't overwrite previous files that were uploaded
	        while (file_exists($dirname . $fileName . '.' . $ext)) {
                $fileName .= rand(10, 99);
            }
        }

       return array
        (
          'dirname' => $dirname,
          'basename' => $fileName . '.' . $ext,
          'extension' => $ext,
          'filename' => $fileName
        );
    }
}

/**
 * Interface pour le visionnage et la modification des fichiers.
 * @author Nicolas Pauss
 */
interface iDocumentViewer {
    /**
     * Retourne le code javascript correspondant à l'affichage du visualisateur.
     */
    function showJsFunct();

    /**
     * Retourne le code javascript correspondant à la disparition du visualisateur.
     */
    function hideJsFunct();

    /**
     * Retourne le code javascript correspondant
     */
    function setFileJsFunct($jsOrigFileNameVar, $jsUrlVar, $jsCurrentFileNameVar, $jsExtVar, $jsFileIdVar);

}



/**
 * Classe contenant l'ensemble du code Html/Js/Css permettant d'utiliser
 * la lib js valums-file-uploader permettant un envoi de fichier efficace.
 * @author Nicolas Pauss
 */
class FileUploader {

    const TRANSLATED_TEXT = <<<'EOT'

        template: '<div class="qq-uploader">' +
                '<div class="qq-upload-drop-area"><span>Déposez ici les fichiers à envoyer</span></div>' +
                '<div class="qq-upload-button">Envoyer un fichier</div>' +
                '<ul class="qq-upload-list"></ul>' +
             '</div>',

        // template for one item in file list
        fileTemplate: '<li>' +
                '<span class="qq-upload-file"></span>' +
                '<span class="qq-upload-spinner"></span>' +
                '<span class="qq-upload-size"></span>' +
                '<a class="qq-upload-cancel" href="#">Annuler</a>' +
                '<span class="qq-upload-failed-text">Erreur</span>' +
            '</li>',

        // messages
        messages: {
            typeError: "{file} n'a pas une extension valide. Seul les fichiers {extensions} sont autorisés.",
            sizeError: "{file} est trop gros, la taille maximum des fichiers est {sizeLimit}.",
            minSizeError: "{file} est trop petit, la taille minimale des fichiers est {minSizeLimit}.",
            emptyError: "{file} est vide. Veuillez sélectionner d'autres fichiers en évitant celui-ci.",
            onLeave: "Les fichiers sont en train d'être envoyés. Si vous fermez la page maintenant, l'envoi sera annulé."
        },

EOT;


    private $fileUploaderLibDir;
    private $uploadPhpActionFile;
    private $authorizedDocumentTypes;
    private $divId;
    private $textTranslated;
    private $documentViewers = array();
    private $emptyViewer;
    private $sizeLimit;

        //-------------------------------------------------
    // Constructor
    //-------------------------------------------------

    function __construct($uploadPhpActionFile, $authorizedDocumentTypes = array('jpeg', 'jpg', 'png'), $divId = 'file_uploader', $textTranslated = true, $sizeLimit = 10485760) {
        global $racine;

        $this->fileUploaderLibDir = $racine. 'lib/valums-file-uploader';
        $this->uploadPhpActionFile = $uploadPhpActionFile;
        $this->authorizedDocumentTypes = array_map("strtolower", $authorizedDocumentTypes);
        $this->divId = $divId;
        $this->textTranslated = $textTranslated;
        $this->emptyViewer = new EmptyDocumentViewer($this);
        $this->sizeLimit = $sizeLimit;
    }

    //-------------------------------------------------
    // Public functions
    //-------------------------------------------------

    public function getJs() {
        $strRet = <<<EOT

<script src="{$this->fileUploaderLibDir}/fileuploader.js" type="text/javascript"></script>
<script type="text/javascript">
EOT;

        $this->createFileUploadJs($strRet);

        $strRet .= "</script>\n";
        return $strRet;
    }

    public function getCss() {
        $strRet = <<<EOT

    <link href="{$this->fileUploaderLibDir}/fileuploader.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #{$this->divId} {
            text-align:center;
            margin: 0 auto;
        }
    </style>
EOT;
        return $strRet;
    }

    public function getDiv() {
        $strRet = <<<EOT

<div id="{$this->divId}">
		<noscript>
			<p>Please enable JavaScript to use file uploader.</p>
			<!-- or put a simple form for upload here -->
		</noscript>
</div>

EOT;
        return $strRet;
    }

    public function hideJsFunct() {
        return '$("#' . $this->divId . '").hide()';
    }

    public function showJsFunct() {
        return '$("#' . $this->divId . '").show()';
    }

    public function slideDownJsFunct() {
        return '$("#' . $this->divId . '").slideDown()';
    }

    /**
     * Enregistre un visualisateur de fichier pour un type d'extension de fichier
     * donné. Cette fonction doit être appelée avant getJs().
     */
    public function registerDocumentViewer($documentExtension, $documentViewer) {
        if (!($documentViewer instanceof iDocumentViewer)) {
            return false;
        }
        $this->documentViewers[strtolower($documentExtension)] = $documentViewer;
        return true;
    }

    //-------------------------------------------------
    // Private functions
    //-------------------------------------------------

    private function createFileUploadJs(&$strRet) {
        // Head
        $strRet .= <<<EOT

$(document).ready(function() {
    var uploader = new qq.FileUploader({
        debug: false,
        multiple: false,
        sizeLimit: {$this->sizeLimit},
        element: document.getElementById('{$this->divId}'),
        action: '{$this->uploadPhpActionFile}',

EOT;

        // Allowed Extensions
        $strRet .= "\t\tallowedExtensions: [";
        $firstVal = true;
        foreach ($this->authorizedDocumentTypes as $val) {
            if (!$firstVal) {
                $strRet .= ', ';
            }
            $strRet .= "'$val'";
            $firstVal = false;
        }
        $strRet .= "],\n";


        // Transalation
        if ($this->textTranslated) {
            $strRet .= self::TRANSLATED_TEXT;
        }


        $this->createJsFunctions($strRet);


        // Foot
        $strRet .= <<<EOT
    });
});

EOT;
    }

    private function createJsFunctions(&$strRet) {
        $strRet .= <<<EOT
        onComplete: function(id, fileName, responseJSON){
            $(".qq-upload-list").empty();
            if(responseJSON.success){
                var ext = responseJSON.ext.toLowerCase();

EOT;
        $strRet .= $this->hideJsFunct() . ';
                ';


        if (empty($this->documentViewers)) {
            $this->createDocumentViewerJsCall($strRet, $this->emptyViewer);
        } else {
            $first_val = true;
            foreach ($this->documentViewers as $key => $val) {
                if ($first_val) {
                    $strRet .= '
                if (';
                } else {
                    $strRet .= ' else if (';
                }
                $strRet .= "ext == '$key') {
                    ";
                $this->createDocumentViewerJsCall($strRet, $val);
                $strRet .= '
                }';
                $first_val = false;
            }
            $strRet .= ' else {
                    ';
            $this->createDocumentViewerJsCall($strRet, $this->emptyViewer);
            $strRet .= '
                }';

        }

        $strRet .= <<<EOT

            }
        },

EOT;
    }

    private function createDocumentViewerJsCall(&$strRet, $documentViewer) {
        $strRet .= $documentViewer->setFileJsFunct('fileName', 'responseJSON.url', 'responseJSON.fileName', 'responseJSON.ext', 'responseJSON.fileId') . ";
                    " . $documentViewer->showJsFunct() . ";\n";
    }

}


/**
* Handle file uploads via XMLHttpRequest
*/
class qqUploadedFileXhr {
    /**
* Save the file to the specified path
* @return boolean TRUE on success
*/
    function save($path) {
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        if ($realSize != $this->getSize()){
            return false;
        }

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);

        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }
}

/**
* Handle file uploads via regular form post (uses the $_FILES array)
*/
class qqUploadedFileForm {
    /**
* Save the file to the specified path
* @return boolean TRUE on success
*/
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}


class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;
    private $fileNameChooser;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760, $fileNameChooser = null){
        $allowedExtensions = array_map("strtolower", $allowedExtensions);

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        $this->checkServerSettings();

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false;
        }

        if ($fileNameChooser === null) {
            $this->fileNameChooser = new FileNameChooserTimeBased();
        } else {
            $this->fileNameChooser = $fileNameChooser;
        }
    }

    private function checkServerSettings(){
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
        }
    }

    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

    /**
* Returns array('success'=>true, 'url'=>$url, 'ext'=>$ext, 'fileId'=>$fileId) or array('error'=>'error message')
*/
    function handleUpload($uploadDirectory){

        $realUploadDirectory = $_SERVER['DOCUMENT_ROOT'].$uploadDirectory;

        if(!@mkrndir($realUploadDirectory)) {
            return array('error'=> 'Could not save uploaded file.' .
                "The upload directory '$uploadDirectory' could not be created");
        }


        if (!is_writable($realUploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }

        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'File is empty');
        }

        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }

        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }

        $fileInfo = $this->fileNameChooser->chooseFileName($realUploadDirectory, $filename, $ext);
        $fileName = $fileInfo['basename'];
        $ext = $fileInfo['extension'];

        $realUploadFileName = $realUploadDirectory . $fileName;
        $uploadFileName = $uploadDirectory.$fileName;

        if ($this->file->save($realUploadFileName)){
            return array('success' => true, 'url' => $uploadFileName, 'fileName' => $fileName, 'ext' => $ext);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }

    }
}


class EmptyDocumentViewer implements iDocumentViewer {
    private $fileUploader;

    function __construct($fileUploader) {
        $this->fileUploader = $fileUploader;
    }


    public function setFileJsFunct($jsOrigFileNameVar, $jsUrlVar, $jsCurrentFileNameVar, $jsExtVar, $jsFileIdVar) {
        return "alert($jsOrigFileNameVar + ' - ' + $jsUrlVar)";
    }

    public function showJsFunct() {
        return $this->fileUploader->showJsFunct();
    }

    public function hideJsFunct() {
        return '';
    }
}

/**
 * Permet la visualisation des images et leur transformation.
 */
class ImageDocumentViewer implements iDocumentViewer {

    const DEFAULT_COLOR_HEX = '000000';
    const DEFAULT_COLOR_RGB = '{"r":0, "g":0, "b":0}';

    private $uploadPhpActionFile;
    private $colorpickerLibDir;
    private $fileUploader;
    private $imageSize;
    private $previewSize;
    private $divId;
    private $jsFunctPrefix;
    private $additionalDiv;

    function __construct($uploadPhpActionFile, $fileUploader, array $imageSize, $previewWidth, $divId = 'image_viewer', $jsFunctPrefix = 'image_viewer', $additionalDiv = '') {
        global $racine;

        $this->uploadPhpActionFile = $uploadPhpActionFile;
        $this->colorpickerLibDir = $racine. 'lib/colorpicker';
        $this->fileUploader = $fileUploader;
        $this->imageSize = $imageSize;
        $this->divId = $divId;
        $this->jsFunctPrefix = $jsFunctPrefix;
        $this->additionalDiv = $additionalDiv;

        $ratio = $imageSize[1] / $imageSize[0];
        $this->previewSize = array($previewWidth, $previewWidth * $ratio);
    }


    public function getJs() {
        $defaultColor = self::DEFAULT_COLOR_HEX;
        $strRet = <<<EOT

    <script type="text/javascript" src="{$this->colorpickerLibDir}/colorpicker.js"></script>
    <script type="text/javascript">
    var {$this->jsFunctPrefix} = {$this->jsFunctPrefix} || {};


    {$this->jsFunctPrefix}.set_file = function(url, fileName) {
        var newImg = new Image();

        newImg.onload = function() {

            {$this->jsFunctPrefix}.img = $(newImg);

            {$this->jsFunctPrefix}.imgWidth = {$this->jsFunctPrefix}.img.attr("width");
            {$this->jsFunctPrefix}.imgHeight = {$this->jsFunctPrefix}.img.attr("height");

            $('#{$this->divId}_file_name').val(fileName);
            $("#{$this->divId}_resize_mode").val("fit");
            {$this->jsFunctPrefix}.fitFill(true);
            $('#{$this->divId}_img').replaceWith({$this->jsFunctPrefix}.img);
            {$this->jsFunctPrefix}.img.attr("id", '{$this->divId}_img');
        }

        newImg.src = url; // this must be done AFTER setting onload
    }

    {$this->jsFunctPrefix}.fitFill = function(fit) {
        var maxWidth = {$this->previewSize[0]};
        var maxHeight = {$this->previewSize[1]};
        var imgWidth = {$this->jsFunctPrefix}.imgWidth;
        var imgHeight = {$this->jsFunctPrefix}.imgHeight;
        var previewRatio = maxWidth / maxHeight;
        var imgRatio = imgWidth / imgHeight;
        var w;
        var h;
        if (((imgRatio > previewRatio) && fit) ||
            ((imgRatio < previewRatio) && !fit)) {
            w = maxWidth;
            h = w / imgRatio;
        } else {
            h = maxHeight;
            w = h * imgRatio;
        }
        var x = (maxWidth - w) / 2;
        var y = (maxHeight - h) / 2;

        {$this->jsFunctPrefix}.img.width(w);
        {$this->jsFunctPrefix}.img.height(h);

        {$this->jsFunctPrefix}.img.css({position:'absolute', left:x, top:y});
    }


    {$this->jsFunctPrefix}.stretch = function() {
        var maxWidth = {$this->previewSize[0]};
        var maxHeight = {$this->previewSize[1]};

        {$this->jsFunctPrefix}.img.width(maxWidth);
        {$this->jsFunctPrefix}.img.height(maxHeight);

        {$this->jsFunctPrefix}.img.css({position:'absolute', left:0, top:0});
    }

    {$this->jsFunctPrefix}.center = function() {
        var endImgWidth = {$this->imageSize[0]};
        var endImgHeight = {$this->imageSize[1]};
        var maxWidth = {$this->previewSize[0]};
        var maxHeight = {$this->previewSize[1]};
        var imgWidth = {$this->jsFunctPrefix}.imgWidth;
        var imgHeight = {$this->jsFunctPrefix}.imgHeight;

        if ((imgWidth > endImgWidth) || (imgHeight > endImgHeight)) {
            {$this->jsFunctPrefix}.fitFill(true);
        } else {
            var endPreviewRatio = endImgWidth / maxWidth;
            var w = imgWidth / endPreviewRatio;
            var h = imgHeight / endPreviewRatio;
            var x = (maxWidth - w) / 2;
            var y = (maxHeight - h) / 2;

            {$this->jsFunctPrefix}.img.width(w);
            {$this->jsFunctPrefix}.img.height(h);

            {$this->jsFunctPrefix}.img.css({position:'absolute', left:x, top:y});
        }
    }

    {$this->jsFunctPrefix}.change_resize_mode = function() {
        var resize_mode = $("#{$this->divId}_resize_mode").val();
        if (resize_mode == "fit") {
            {$this->jsFunctPrefix}.fitFill(true);
        } else if (resize_mode == "fill") {
            {$this->jsFunctPrefix}.fitFill(false);
        } else if (resize_mode == "stretch") {
            {$this->jsFunctPrefix}.stretch();
        } else {
            {$this->jsFunctPrefix}.center();
        }
    }

    {$this->jsFunctPrefix}.change_background_color = function(hex, rgb) {
        $('#{$this->divId}_preview').css('backgroundColor', '#' + hex);
        var rgbStr = '{"r":'+ rgb.r + ',"g":' + rgb.g + ',"b":' + rgb.b + '}';
        $('#{$this->divId}_background_color').val(rgbStr);
    }

    $(document).ready(function(){
        $("#{$this->divId}_resize_mode").change(function () {
            {$this->jsFunctPrefix}.change_resize_mode();
        });

        $('#{$this->divId}_color_selector').ColorPicker({
			color: '#ff0000',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$('#{$this->divId}_color_selector div').css('backgroundColor', '#' + hex);
                {$this->jsFunctPrefix}.change_background_color(hex, rgb);
			}
		}).ColorPickerSetColor('#{$defaultColor}');
    });


    </script>

EOT;
        return $strRet;
    }

    public function getCss() {
        $defaultColor = self::DEFAULT_COLOR_HEX;
        $strRet = <<<EOT

    <link rel="stylesheet" media="screen" type="text/css" href="{$this->colorpickerLibDir}/colorpicker.css" />
    <style type="text/css">
        #{$this->divId}_main {
            text-align:center;
            display:none;
        }

        #{$this->divId}_preview {
            border: 5px solid #ccc;
            background-color: #{$defaultColor};
            width: {$this->previewSize[0]}px;
            height: {$this->previewSize[1]}px;
            overflow: hidden;

            margin:0 auto;
            position: relative;
        }
    </style>

EOT;
        return $strRet;
    }

    public function getDiv() {
        $defaultColorHex = self::DEFAULT_COLOR_HEX;
        $defaultColorRgb = self::DEFAULT_COLOR_RGB;
        $strRet = <<<EOT

<div id="{$this->divId}_main">
    <form method="post" id="{$this->divId}_form" enctype="multipart/form-data" action="{$this->uploadPhpActionFile}">
        {$this->additionalDiv}

        <div id="{$this->divId}_preview">
            <img id="{$this->divId}_img" />
        </div>

        <select name="resize_mode" id="{$this->divId}_resize_mode">
            <option value="fit" selected="selected">Ajuster</option>
            <option value="fill">Remplissage</option>
            <option value="stretch">Étirer</option>
            <option value="center">Centrer</option>
        </select>

        <div id="{$this->divId}_color_div">
            <span id="{$this->divId}_color_span">Couleur:</span>
            <div id="{$this->divId}_color_selector" class="colorselector"><div style="background-color: #{$defaultColorHex}"></div></div>
        </div>



        <input type="hidden" name="fileName" id="{$this->divId}_file_name" />
        <input type="hidden" name="bg_color" id="{$this->divId}_background_color" value='{$defaultColorRgb}' />

        <input type="submit" id="{$this->divId}_submit" value="Envoyer" />
    </form>
</div>

EOT;
        return $strRet;
    }


    public function setFileJsFunct($jsOrigFileNameVar, $jsUrlVar, $jsCurrentFileNameVar, $jsExtVar, $jsFileIdVar) {
        return "{$this->jsFunctPrefix}.set_file($jsUrlVar, $jsCurrentFileNameVar)";
    }

    public function showJsFunct() {
        return "$('#{$this->divId}_main').slideDown()";
    }

    public function hideJsFunct() {
        return "$('#{$this->divId}_main').hide()";
    }
}


/**
 * Permet la visualisation des images et leur transformation.
 */
class PdfDocumentViewer implements iDocumentViewer {

    private $uploadPhpActionFile;
    private $jsLibDir;
    private $fileUploader;
    private $previewSize;
    private $multipage;
    private $divId;
    private $jsFunctPrefix;
    private $additionalDiv;

    function __construct($uploadPhpActionFile, $fileUploader, array $previewSize, $multipage = true, $divId = 'pdf_viewer', $jsFunctPrefix = 'pdf_viewer', $additionalDiv = '') {
        global $racine;

        $this->uploadPhpActionFile = $uploadPhpActionFile;
        $this->jsLibDir = $racine. 'js';
        $this->fileUploader = $fileUploader;
        $this->previewSize = $previewSize;
        $this->multipage = $multipage;
        $this->divId = $divId;
        $this->jsFunctPrefix = $jsFunctPrefix;
        $this->additionalDiv = $additionalDiv;
    }


    public function getJs() {
        $multipageStr = ($this->multipage) ? 'true' : 'false';
        $strRet = <<<EOT

    <script type="text/javascript" src="{$this->jsLibDir}/pdf.min.js"></script>
    <script type="text/javascript" src="{$this->jsLibDir}/pdfjsviewer.js"></script>
    <script type="text/javascript">
    var {$this->jsFunctPrefix} = {$this->jsFunctPrefix} || {};

    {$this->jsFunctPrefix}.pdfJsViewer = null;

    {$this->jsFunctPrefix}.set_file = function(url, fileName) {
        console.log("set_file url = " + url);
        {$this->jsFunctPrefix}.pdfJsViewer.loadPdf(url);
        $('#{$this->divId}_file_name').val(fileName);
    }

    $(document).ready(function(){
        var div = $('#{$this->divId}_preview').get(0);
        {$this->jsFunctPrefix}.pdfJsViewer = new PdfJsViewer(div, {width:{$this->previewSize[0]}, height:{$this->previewSize[1]}, multipage: {$multipageStr}});
    });


    </script>

EOT;
        return $strRet;
    }

    public function getCss() {
        return <<<EOT
        <style type="text/css">
            #{$this->divId}_main {
                display:none;
            }

            #{$this->divId}_preview {
                margin:0 auto;
                text-align:center;
            }
            #{$this->divId}_submit {
                margin-top:5px;
            }
        </style>

EOT;
    }

    public function getDiv() {
        $strRet = <<<EOT

<div id="{$this->divId}_main">
    <form method="post" id="{$this->divId}_form" enctype="multipart/form-data" action="{$this->uploadPhpActionFile}">
        {$this->additionalDiv}

        <div id="{$this->divId}_preview">
        </div>

        <input type="hidden" name="fileName" id="{$this->divId}_file_name" />

        <input type="submit" id="{$this->divId}_submit" value="Envoyer" />
    </form>
</div>

EOT;
        return $strRet;
    }


    public function setFileJsFunct($jsOrigFileNameVar, $jsUrlVar, $jsCurrentFileNameVar, $jsExtVar, $jsFileIdVar) {
        return "{$this->jsFunctPrefix}.set_file($jsUrlVar, $jsCurrentFileNameVar)";
    }

    public function showJsFunct() {
        return "$('#{$this->divId}_main').slideDown()";
    }

    public function hideJsFunct() {
        return "$('#{$this->divId}_main').hide()";
    }
}


?>
