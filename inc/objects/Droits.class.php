<?php

require_once 'PolarObject.class.php';

class Droits extends PolarObject {
    public static $table = 'polar_securite_droits';
    protected static $attrs = array(
        'ID' => 'Page',
        'User' => 'Utilisateur');

}