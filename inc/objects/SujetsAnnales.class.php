<?php

require_once 'PolarObject.class.php';

class SujetsAnnales extends PolarObject {
    public static $table = 'polar_annales_sujets';
    protected static $attrs = array(
                                    'sujet_uv' => T_STR,
                                    'sujet_type' => array('t', 'u', 'v', 'p', 'q', 'm', 'f'),
                                    );
    protected static $nulls = array(
                                    );
    protected static $primary_key = 'sujet_id';
}