<?php

require_once 'PolarObject.class.php';

class Article extends PolarObject {
    public static $table = 'polar_caisse_articles';
    protected static $attrs = array(
        "CodeCaisse" => T_INT,
        "EnVente" => T_BOOL,
        "Actif" => T_BOOL,
        "Nom" => T_STR,
        "Secteur" => T_INT,
        "PrixAchat" => T_FLOAT,
        "PrixVente" => T_FLOAT,
        "PrixVenteAsso" => T_FLOAT,
        "TVA" => T_FLOAT,
        "Palier1" => T_INT,
        "Remise1" => T_FLOAT,
        "Palier2" => T_INT,
        "Remise2" => T_FLOAT,
        "StockInitial" => T_INT,
        "Stock" => T_INT,
        "SeuilAlerte" => T_INT,
        "CodeJS" => T_STR,
        "Photo" =>  T_STR,
        "EAN13" => T_STR,
        "Auteur" => 'Utilisateur',
        "Fournisseur" => T_STR,
        "Date" => T_STR);
    protected static $nulls = array(
        'StockInitial', 'Stock', 'SeuilAlerte',
        'EAN13', 'Auteur', 'Fournisseur');

    public static function getActifByCode($code) {
        return self::select('Article.*')
            ->where('CodeCaisse = ? AND Actif = 1 AND EnVente = 1', intval($code))
            ->getOne();
    }

    public static function getActifBySection($secteur) 
    {
        return self::select('Article.*')
            ->where('Secteur = ? AND Actif = 1 AND EnVente = 1', intval($secteur))
             ->order('Nom ASC')
            ->execute();
    }

    public function calculerPrix($quantite, $asso=false) {
        if ($asso) {
            if ($this->PrixVenteAsso > 0)
                return $quantite * $this->PrixVenteAsso;
            else
                return $quantite * $this->PrixVente;
        }
        else {
            $prix = $quantite * $this->PrixVente;
            if ($quantite >= $this->Palier1) {
                if ($this->Palier2 != NULL and $this->Palier2 > 0 and $quantite >= $this->Palier2)
                    return $prix * (1-$this->Remise2);
                else
                    return $prix * (1-$this->Remise1);
            }
        }
    }
}
