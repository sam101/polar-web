<?php

class TransfertPayutc extends PolarObject {
    public static $table = 'polar_payutc_transferts';
    protected static $attrs = array(
        'Date' => T_STR,
        'Montant' => T_FLOAT,
        'Commentaire' => T_STR,
        'Virement' => 'Virement');
    protected static $nulls = array();
}