<?php

require_once 'PolarObject.class.php';

class ArticlePayutc extends PolarObject {
    public static $table = 'polar_caisse_articles_payutc';
    protected static $attrs = array(
        "Article" => 'Article',
        "IDPayutc" => T_INT,
        "DateSync" => T_STR);
    protected static $nulls = array();

    public static function getByArticle($article) {
        return self::select()->where('Article = ?', $article)
            ->getOne();
    }

}