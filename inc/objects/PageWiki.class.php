<?php

require_once 'PolarObject.class.php';

class PageWiki extends PolarObject {
    public static $table = 'polar_wiki';
    protected static $attrs = array(
        'Page' => T_INT,
        'Categorie' => 'CategorieWiki',
        'Titre' => T_STR,
        'Contenu' => T_STR,
        'Auteur' => 'Utilisateur',
        'Date' => T_STR);
    protected static $nulls = array();

    public static function getPage($id) {
        return self::select('PageWiki.*')
            ->where('Page = ?', intval($id))
            ->order('Date DESC')
            ->limit(1)
            ->getOne();
    }

}
