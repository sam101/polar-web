<?php

require_once 'PolarObject.class.php';

class TarifBilletterie extends PolarObject {
    public static $table = 'polar_billetterie_tarifs';
    protected static $attrs = array(
        'Evenement' => 'Billetterie',
        'Intitule' => T_STR,
        'TypeCommande' => 'TypeCommande',
        'RequireUTC' => T_BOOL,
        'RequireBDE' => T_BOOL,
        'Prive' => T_BOOL,
        'Adulte' => T_BOOL);
    protected static $nulls = array('Prive', 'Adulte');

    public static function getForBilletterie($id) {
        return TarifBilletterie::select()->where('Evenement = ?', $id);
    }

}
?>
