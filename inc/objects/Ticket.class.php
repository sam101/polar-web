<?php

class Ticket extends PolarObject {
    public static $table = 'polar_tickets';
    public static $primary_key = 'id';
    protected static $attrs = array(
        'subject' => T_STR,
        'author' => T_STR,
        'category' => T_INT,
        'opened' => T_STR,
        'status' => array('new', 'active', 'closed'),
        'Salt' => T_STR);
    protected static $nulls = array();

}