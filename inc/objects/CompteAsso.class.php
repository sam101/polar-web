<?php

require_once 'PolarObject.class.php';

class CompteAsso extends PasswordProtected {
    public static $table = 'polar_assos_comptes';
    protected static $attrs = array(
        'Asso' => 'Asso',
        'Nom' => T_STR,
        'Quota' => T_INT,
        'TarifAsso' => T_BOOL,
        'MotDePasse' => T_STR,
        'MotDePasseSecurise' => T_INT,
        'DateCreation' => T_STR,
        'DateActivation' => T_STR);
    protected static $nulls = array(
        'DateActivation', 'Quota');

    public function activer($mdp) {
        $this->MotDePasse = createHash($mdp);
        $this->MotDePasseSecurise = 1;
        $this->DateActivation = raw('NOW()');
        $this->save();
    }

    public function cloturer() {
        $this->DateActivation = NULL;
        $this->save();
    }
}
?>
