<?php

/**
 * Pour les objets avec un champ mot de passe standard
 * (MotDePasse + MotDePasseSecurise)
 */
class PasswordProtected extends PolarObject {
    public function checkMotDePasse($mdp) {
        if ($this->MotDePasseSecurise) {
            return validatePassword($mdp, $this->MotDePasse);
        }

        if (md5($mdp) === $this->MotDePasse) {
            createMotDePasseSecurise($this->get_id(), $mdp, $this::$table);
            return true;
        }
    }

    public function changeMotDePasse($new) {
        $this->MotDePasseSecurise = 1;
        $this->MotDePasse = createHash($new);
    }
}