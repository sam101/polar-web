<?php

require_once 'PolarObject.class.php';

class CommandeContenu extends PolarObject {
    public static $table = 'polar_commandes_contenu';
    protected static $attrs = array(
        'IDCommande' => 'Commande',
        'Detail' => T_STR,
        'Quantite' => T_INT,
        'Article' => 'Article',
        'PrixTTC' => T_FLOAT);
    protected static $nulls = array();

}
?>
