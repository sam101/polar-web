<?php

require_once 'PolarObject.class.php';

class CaisseSoldeErreur extends PolarException { }

class Caisse extends PolarObject {
  public static $table = 'polar_caisses';
  protected static $attrs = array(
      'Mouvement' => 'MouvementCoffre',
      'Details' => 'DetailsCaisse',
      'TheoriqueCB' => T_FLOAT,
      'ReelCB' => T_FLOAT,
      'TheoriquePayutc' => T_FLOAT,
      'ReelPayutc' => T_FLOAT,
      'TheoriqueMoneo' => T_FLOAT,
      'ReelMoneo' => T_FLOAT,
      'TheoriqueEspeces' => T_FLOAT,
      'ReelEspeces' => T_FLOAT, // ex TotalAvantTransfert
      'EspecesApres' => T_FLOAT, // ex TotalApresTransfert
      'DernierIDVente' => T_INT,
      'Commentaire' => T_STR);
  protected static $nulls = array();

  public function getDetails() {
      if (is_null($this->Details))
          $this->Details = new DetailsCaisse();
      return $this->Details;
  }

  public function getMouvement() {
      if (is_null($this->Mouvement))
          $this->Mouvement = new MouvementCoffre();
      return $this->Mouvement;
  }

  public function getErreurEspeces() {
      return round($this->ReelEspeces - $this->TheoriqueEspeces, 2);
  }

  public function getErreurCB() {
      return round($this->ReelCB - $this->TheoriqueCB, 2);
  }

  public function getErreurMoneo() {
      return round($this->ReelMoneo - $this->TheoriqueMoneo, 2);
  }

  public function getErreurPayutc() {
      return round($this->ReelPayutc - $this->TheoriquePayutc, 2);
  }

  public static function getSolde() {
      $derniereCaisse = Caisse::select()->order("ID DESC")->getOne();
      if (is_null($derniereCaisse))
          throw new CaisseSoldeErreur('La table de caisse est vide');
      
      // on fait la différence entre le restant la dernière fois qu'on a fait la caisse et les transfert avec échange qui ont été faits depuis
      $transferts = MouvementCoffre::select('COALESCE(SUM(Billets) + SUM(Pieces), 0)')
          ->where('ID > ?', $derniereCaisse->get_object_id('Mouvement'))
          ->where("Echange = 'o'")
          ->rawExecute();
      return $derniereCaisse->EspecesApres - $transferts->fetchColumn();
  }

  public static function getDateDernierCaisse() {
      return self::select('Date')
          ->join('MouvementCoffre', 'Caisse.Mouvement = MouvementCoffre.ID')
          ->order('Caisse.ID DESC')
          ->limit(1)
          ->rawExecute()->fetchColumn();
  }
}
?>
