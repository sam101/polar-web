<?php

class TicketMessage extends PolarObject {
    public static $table = 'polar_tickets_messages';
    public static $primary_key = 'id';
    protected static $attrs = array(
        'ticket' => 'Ticket',
        'sender' => T_STR,
        'sent' => T_INT,
        'message' => T_STR);
    protected static $nulls = array();

}