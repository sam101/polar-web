<?php

require_once 'PolarObject.class.php';

class MouvementCoffre extends PolarObject {
    public static $table = 'polar_caisse_coffre';
    protected static $attrs = array(
        'User' => 'Utilisateur',
        'Date' => T_STR,
        'Billets' => T_FLOAT,
        'Pieces' => T_FLOAT,
        'Echange' => array('u', 'o', 'n'),
        /* Echange indique si le mouvement est un échange caisse<->coffre
         * u = unknow (pour les mouvements avant le nouveau système)
         * o = oui
        */
        'Commentaire' => T_STR);
    protected static $nulls = array();

}
?>
