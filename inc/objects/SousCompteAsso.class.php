<?php

require_once 'PolarObject.class.php';

class ReactivationException extends PolarException {}

class Asso extends PolarObject {
    public static $table = 'polar_assos';
    protected static $attrs = array(
                                    'MailAsso' => T_STR,
                                    'Nom' => T_STR,
                                    'President' => T_STR,
                                    'MailPresident' => T_STR,
                                    'TelPresident' => T_STR,
                                    'Tresorier' => T_STR,
                                    'MailTresorier' => T_STR,
                                    'TelTresorier' => T_STR,
                                    'MotDePasse' => T_STR,
                                    'DateCreation' => T_STR
    protected static $nulls = array(
                                    'MailPresident', 'TelPresident',
                                    'MailTresorier', 'TelTresorier');
