<?php

require_once 'PolarObject.class.php';

class ReactivationException extends PolarException {}

class Asso extends PasswordProtected {
    public static $table = 'polar_assos';
    protected static $attrs = array(
        'MailAsso' => T_STR,
        'Asso' => T_STR,
        'President' => T_STR,
        'MailPresident' => T_STR,
        'TelPresident' => T_STR,
        'Tresorier' => T_STR,
        'MailTresorier' => T_STR,
        'TelTresorier' => T_STR,
        'MotDePasse' => T_STR,
        'MotDePasseSecurise' => T_BOOL,
        'DateCreation' => T_STR,
        'Etat' => array('Actif', 'DefautPaiement',
                        'AttenteActivation', 'Clos',
                        'Supprime'));
    protected static $nulls = array(
        'MailPresident', 'TelPresident',
        'MailTresorier', 'TelTresorier');

    public function sendMail($from, $from_nom, $sujet, $html, $pj=array()) {
        $recipients = array();
        if ($this->MailAsso !== NULL)
            $recipients[] = $this->MailAsso;
        if ($this->MailTresorier !== NULL)
            $recipients[] = $this->MailTresorier;
        if ($this->MailPresident !== NULL)
            $recipients[] = $this->MailPresident;
        
        sendMail($from, $from_nom, $recipients, $sujet, $html, $pj);
    }

    public function facturer() {
        $facture = new FactureAsso(array('Asso'=>$this,
                                         'Date'=>raw('NOW()')));
        $facture->save();

        $comptes = $this->getComptes()->select('CompteAsso.ID');
        OldVente::update()->set_value('Finalise', 1)
            ->set_value('Facture', $facture)
            ->where('Asso IN('.$comptes->get_sql().') AND Finalise = 0')
            ->addSubquery($comptes)
            ->execute();

        $montant = OldVente::select('COALESCE(SUM(PrixFacture), 0)')
            ->where('Facture=?', $facture)->rawExecute()->fetchColumn();

        $facture->Montant = $montant;
        $facture->save();

        $chemin = $facture->genererPDF();

        // Message du mail
        $msg = "Bonjour,<br /><br />
Vous trouverez ci-joint la facture du Polar concernant votre association.
Vous pouvez la régler :<br />
- en déposant un chèque au Polar pendant les horaires d'ouverture<br />
- en faisant parvenir un chèque au Polar par courrier interne<br />
- par virement bancaire (demandez-nous notre RIB)<br /><br />
Cordialement,<br />L'équipe du Polar";

        $pj = array($chemin);

        $this->sendMail('polar@assos.utc.fr', 'Le Polar', "[Le Polar] Facture du semestre pour votre association", $msg, $pj);
    }

    public function cloturer() {
        $this->Etat = 'Clos';
        $this->sendMail('polar@assos.utc.fr', 'Le Polar',
                        '[Le Polar] Rappel de renouvellement de compte',
                        $this->format_from_attributes('Bonjour,<br /><br />
Le semestre se termine, nous venons donc de clôturer le compte de votre association {{Asso}}.<br />
Dès que la nouvelle équipe est formée, celle-ci devra réouvrir le compte au Polar à cette adresse : http://assos.utc.fr/polar/assos/reactiver<br /><br />
Merci de leur transmettre cette information.<br /><br />
A bientôt,<br />
L\'équipe du Polar'));
        $this->save();
    }

    public function activer() {
        $this->Etat = 'Actif';
        $this->DateCreation = raw('NOW()');
        $this->sendMail('polar@assos.utc.fr', 'Le Polar',
                        '[Le Polar] Validation de votre compte assos',
                        $this->format_from_attributes('Bonjour,<br /><br />Le compte de l\'association {{Asso}} a été validé !<br/><br/>Cordialement,<br />L\'équipe du Polar.'));
        $this->save();
    }

    public function refuser($message) {
        $this->Etat = 'Clos';
        $this->sendMail('polar@assos.utc.fr', 'Le Polar',
                        '[Le Polar] Refus de création de compte assos',
                        $message);
        $this->save();
    }

    public function bloquer($raison) {
        $this->Etat = 'DefautPaiement';
        $this->sendMail('polar@assos.utc.fr', 'Le Polar',
                        '[Le Polar] Votre compte Assos a été bloqué pour Défaut de Paiement',
                        $raison);
        $this->save();
    }

    public function debloquer() {
        $this->Etat = 'Actif';
        $this->sendMail('polar@assos.utc.fr', 'Le Polar',
                        '[Le Polar] Votre compte assos a été débloqué',
                        '"Bonjour, <br /> Nous vous confirmons que votre compte Asso a été débloqué, il peut dès à présent être utilisé.<br /><br />Cordialement,<br />L\'équipe du Polar');
        $this->save();
    }

    /* reactiver
     * throws ReactivationException
     */
    public function reactiver($mail, $president, $presidentmail,
                              $presidenttel, $tresorier, $tresoriermail,
                              $tresoriertel, $password, $password2) {
        $erreurs = '';
        $this->Etat = 'AttenteActivation';

        if(empty($mail))
            $erreurs .= "l'adresse mail de l'asso n'a pas été précisée\n";

        if(empty($president))
            $erreurs .= "le nom du président n'a pas été précisé\n";

        if(empty($presidentmail) || !verifMailUTC($presidentmail))
            $erreurs .= "l'adresse mail du président n'est pas correcte\n";

        if(empty($presidenttel) || !preg_match('/[0-9]{10}/', $presidenttel))
            $erreurs .= "le téléphone du président est incorrect\n";

        if(empty($tresorier))
            $erreurs .= "le nom du trésorier n'a pas été précisé\n";

        if(empty($tresoriermail) || !verifMailUTC($presidentmail))
            $erreurs .= "l'adresse mail du trésorier n'est pas correcte\n";

        if(empty($tresoriertel) || !preg_match('/[0-9]{10}/', $tresoriertel))
            $erreurs .= "le téléphone du trésorier est incorrect\n";

        if(empty($_POST['ep-password']))
            $erreurs .= "le mot de passe n'a pas été précisé\n";

        if($password != $password2)
            $erreurs .= "les deux mots de passe ne sont pas identiques\n";

        if ($erreurs != '') {
            throw new ReactivationException($erreurs);
        } else {
            $this->MailAsso = $mail;
            $this->President = $president;
            $this->MailPresident = $presidentmail;
            $this->TelPresident = $presidenttel;
            $this->Tresorier = $tresorier;
            $this->MailTresorier = $tresoriermail;
            $this->TelTresorier = $tresoriertel;
            $this->MotDePasse = createHash($password);
            $this->MotDePasseSecurise = 1;

            $compte = $this->getComptePrincipal();
            $compte->activer($password);

            $this->sendMail('polar@assos.utc.fr', 'Le Polar',
                            "Demande d'ouverture de compte enregistrée !",
                            "Bonjour,<br /><br />Votre demande d'ouverture de compte a bien été enregistrée. Vous recevrez un mail dès qu'elle sera traitée.<br /><br />Cordialement,<br />L'équipe du Polar.");
            $this->save();
        }
    }

    /**
     * Renvoie la requête permettant pour
     * obtenir les pour cette asso
     * @return PolarQuery ou NULL si l'asso n'est pas encore sauvegardée
     */
    public function getFactures() {
        if ($this->get_id() == NULL)
            return NULL;
        else
            return FactureAsso::select()->where('Asso=?', $this->get_id());
    }

    /**
     * Crée un nouveau sous-compte
     * Il faut l'activer immédiatement
     * @param $nom Nom du compte
     * @return 
     */
    public function nouveauCompte($nom) {
        return new CompteAsso(array('Asso' => $this,
                                    'Nom' => $nom,
                                    'TarifAsso' => True,
                                    'DateCreation' => raw('NOW()'),
                                    'DateActivation' => NULL));
    }

    public function getComptePrincipal() {
        $compte = CompteAsso::select()->where('Asso=?', $this)
            ->where('Nom=?', $this->Asso)->getOne();
        if (is_null($compte)) {
            $compte = $this->nouveauCompte($this->Asso);
            $compte->save();
        }
        return $compte;
    }

    public function getComptes() {
        return CompteAsso::select()->where('CompteAsso.Asso = ?', $this);
    }

    public function getComptesActifs() {
        return $this->getComptes()->where('CompteAsso.DateActivation IS NOT NULL');
    }

    public static function selectActives() {
        return Asso::select()->where('Etat = ?', 'Actif')->order('Asso ASC');
    }

    public function changeMotDePasse($new) {
        parent::changeMotDePasse($new);

        $compte = $this->getComptePrincipal();
        if (!is_null($compte)) {
            $compte->changeMotDePasse($new);
            $compte->save();
        }
    }
}
?>
