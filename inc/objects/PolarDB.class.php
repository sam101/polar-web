<?php

/**
 * PolarDB Implementation
 *
 * @author Florent Thévenet
 */

require_once 'PolarObject.class.php';
require_once 'PolarObjectsArray.class.php';
require_once 'PolarQuery.class.php';

/**
 * Surclasse de PDO pour Le Polar.
 *
 * Défini les attributs pour la connexion PDO.
 *
 * Compte et sauvegarde les requêtes.
 *
 * Il est possible d'avoir plusieur classes PolarDB instanciée en même temps
 */
class PolarDB extends PDO {
    private $select_req;
    private $objects_store;
    private $nb_query = 0;
    public $query = array();
    private $id_val = 0;

    /**
     * Construit le PolarDB et lance la connexion à la base
     * @param $server Serveur sur lequel PDO doit se connecter
     * @param $base Nom de la base à sélectionner
     * @param $user Nom d'utilisateur pour la connection
     * @param $pass Mot de passe pour la connection
     */
    public function __construct($server, $base, $user, $pass) {
        /* Connexion à une base ODBC avec l'invocation de pilote */
        global $CONF;
        $dsn = "mysql:dbname=$base;host=$server";

        parent::__construct($dsn, $user, $pass,
                            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Crée une PolarQuery
     * @param $type Type de la requête (QUERY_INSERT, QUERY_SELECT, QUERY_UPDATE ou QUERY_SELECT)
     * @param $class Nom de l'objet sur laquelle la requête porte
     * @return une PolarQuery correctement configurée
     */
    public function create_query($type, $class) {
        return new PolarQuery($this, $type, $class);
    }

    /**
     * Crée une PolarQuery en mode INSERT
     * @see create_query()
     */
    public function q_insert($class) {
        return $this->create_query(QUERY_INSERT, $class);
    }

    /**
     * Crée un PolarQuery en mode SELECT
     * @see create_query()
     */
    public function q_select($class) {
        return $this->create_query(QUERY_SELECT, $class);
    }

    /**
     * Crée une PolarQuery en mode DELETE
     * @see create_query()
     */
    public function q_delete($class) {
        return $this->create_query(QUERY_DELETE, $class);
    }

    /**
     * Crée une PolarQuery en mode UPDATE
     * @see create_query()
     */
    public function q_update($class) {
        return $this->create_query(QUERY_UPDATE, $class);
    }

    /**
     * Vérifie si l'objet de la classe $class avec la clé primaire $id existe.
     * @param $class Nom d'une classe dérivée de PolarObject
     * @param $id Clé Primaire de l'objet
     * @return True si l'objet existe, False sinon
     */
    public function validObject($class, $id) {
        $r = $this->q_select($class)->select('COUNT(*)')->where($class::$primary_key.'=?', (int) $id)->rawExecute();
        
        $b = $r->fetchColumn() == 1;
        $r->closeCursor();
        return $b;
    }

    /**
     * Supprime des objets de la base
     * @param ... PolarObject s à supprimer
     */
    public function delete() {
        foreach (func_get_args() as $obj) {
          if ($obj instanceof PolarObject and !is_null($obj->get_id())) {
              $obj->delete();
            }
        }
    }

    /**
     * Nombre de requêtes exécutées jusqu'à maintenant.
     * @return nombre de requête exécutées
     */
    public function get_nb_queries() {
        return $this->nb_query;
    }

    public function makeId($alias) {
        return $alias . $this->id_val++;
    }

    public function query($query) {
        $this->nb_query++;
        $this->queries[] = $query;
        return parent::query($query);
    }

    /**
     * prepare la requête passer en premier argument
     * puis l'execute avec les arguments passés après
     * On utilise le marqueur ? pour indiquer l'emplacement où
     * substituer les arguments
     */
    public function preparedQuery($query) {
        $q = $this->prepare($query);

        $args = func_get_args();
        array_shift($args); // On enlève $query
        $q->execute($args);

        return $q;
    }

    public function prepare($query) {
        $this->nb_query++;
        $this->queries[] = $query;
        return parent::prepare($query);
    }

    // on ignore (mais on log) les tentatives d'imbriquer plusieurs transactions
    protected $transLevel = 0;

    public function beginTransaction() {
        $this->transLevel++;
        if($this->transLevel == 1) {
            return parent::beginTransaction();
        } else {
            Log::info("Transaction PDO imbriquées");
        }
    }

    public function commit() {
        $this->transLevel--;

        if($this->transLevel == 0) {
            return parent::commit();
        } else {
            Log::info("Commit d'une transaction PDO imbriquée");
        }
    }

    public function rollBack() {
        $this->transLevel--;

        if($this->transLevel == 0) {
            return parent::rollBack();
        } else {
            Log::info("Rollback d'une transaction PDO imbriquée");
        }
    }
}
