<?php

class Fournisseur extends PolarObject {
    public static $table = 'polar_fournisseurs';
    protected static $attrs = array(
        'Nom' => T_STR,
        'Description' => T_STR,
        'DateSupprime' => T_STR);
    protected static $nulls = array('DateSupprime');

    public function getContacts($supprimes = false) {
        $q = FournisseurContact::select()->where('Fournisseur = ?', $this);
        if (!$supprimes)
            $q->where('DateSupprime IS NULL');
        return $q;
    }

    public function getTickets($supprimes = false) {
        $q = FournisseurTicket::select()->where('Fournisseur = ?', $this)
            ->order('Date DESC');
        if (!$supprimes)
            $q->where('DateSupprime IS NULL');
        return $q;
    }

    public function nouveauTicket() {
        return new FournisseurTicket(array('Fournisseur' => $this));
    }

    public function nouveauContact() {
        return new FournisseurContact(array('Fournisseur' => $this));
    }
}