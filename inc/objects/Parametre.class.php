<?php

require_once 'PolarObject.class.php';

class Parametre extends PolarObject {
    public static $table = 'polar_parametres';
    private static $cache = array();

    public static function get($nom) {
        if (in_array($nom, self::$cache)) {
            return self::$cache[$nom];
        } else {
            $r = Parametre::select('Valeur')
                ->where('Nom LIKE ?', $nom)
                ->rawExecute();
            $value = $r->fetchColumn();
            $r->closeCursor();

            if ($value != False)
                self::$cache[$nom] = $value;

            return $value;
        } 
    }

    public static function set($nom, $value) {
        self::$cache[$nom] = $value;
        Parametre::update()
            ->set_value('Valeur', $value)
            ->where('Nom LIKE ?', $nom)
            ->execute()
            ->closeCursor();
    }
}