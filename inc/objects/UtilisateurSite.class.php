<?php

define('LOGIN_METHOD_NO', 0);
define('LOGIN_METHOD_PASSWORD', 1);
define('LOGIN_METHOD_CAS', 2);

class UtilisateurSite extends Utilisateur {
    public $auth_method = LOGIN_METHOD_NO;

    public function log_with($method) {
        if ($method == LOGIN_METHOD_NO ||
            $method == LOGIN_METHOD_CAS ||
            $method == LOGIN_METHOD_PASSWORD) {
            $this->auth_method = $method;
        }
        return $this;
    }

    /**
     * À n'utiliser que lors de la connexion
     * (pas à chaque chargement de page)
     */
    public function log_with_cas() {
        $this->reset_login();
        $this->auth_method = LOGIN_METHOD_CAS;
        return $this;
    }

    // idem log_with_cas
    public function log_with_password() {
        $this->reset_login();
        $this->auth_method = LOGIN_METHOD_PASSWORD;
        return $this;
    }

    public function reset_login() {
        Payutc::reset();
        $_SESSION['con-lastPrivateActive'] = time();
    }

    public function is_logged_with_cas() {
        return $this->auth_method == LOGIN_METHOD_CAS;
    }

    public function is_logged_with_password() {
        return $this->auth_method == LOGIN_METHOD_PASSWORD;
    }

    public function is_logged() {
        return $this->auth_method != LOGIN_METHOD_NO;
    }

    /**
     * Charge un utilisateur depuis les informations stockées en session
     * ou renvoye un utilisateur non connecté
     */
    static public function load_from_session() {
        if (isset($_SESSION['con-connecte'], $_SESSION['con-id']) && $_SESSION['con-connecte'] == 'oui') {
            //TimeOut : 2 heures
            if(time()-intval($_SESSION['con-derniereactivite'])>2*3600){
                unset($_SESSION['con-connecte']);
                unset($_SESSION['con-id']);
                unset($_SESSION['con-derniereactivite']);
                unset($_SESSION['con-lastPrivateActive']);
                unset($_SESSION['con-method']);
                return new UtilisateurSite();
            }
            else {
                $_SESSION['con-derniereactivite'] = time();
                try {
                    return UtilisateurSite::getById($_SESSION['con-id'])->log_with($_SESSION['con-method']);
                } catch (UnknownObject $e) {
                    Log::critical("Utilisateur inexistant (" . $_SESSION['con-id'] . ") enregistré dans une session");
                    unset($_SESSION['con-connecte']);
                    unset($_SESSION['con-id']);
                    ajouterErreur("C'est quoi ce micmac ? Ton identifiant d'utilisateur est invalide !");
                    return new UtilisateurSite();
                }
            }
        } else {
            return new UtilisateurSite();
        }
    }

    public function save_to_session() {
        if ($this->is_logged()) { 
            $_SESSION['con-connecte'] = 'oui';
            $_SESSION['con-id'] = $this->get_id();
            $_SESSION['con-derniereactivite'] = time();
            $_SESSION['con-method'] = $this->auth_method;
        }
    }

    /*
     * Renvoie le vendeur par défaut quand personne n'est loggé
     * pour les ventes par internet
     * implémentation = euh
     */
    public function getVendeurDefaut() {
        $u = new UtilisateurSite();
        $u->set_id(1);
        return $u;
    }
}