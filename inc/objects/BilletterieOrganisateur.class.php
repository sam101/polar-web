<?php

require_once 'PolarObject.class.php';

class BilletterieOrganisateur extends PolarObject {
    public static $table = 'polar_billetterie_organisateurs';
    protected static $attrs = array(
        'Billetterie' => 'Billetterie',
        'User' => 'Utilisateur');
    protected static $nulls = array();

    public static function userIsOrganisateur($id, $user) {
        $q = BilletterieOrganisateur::select('count(*)')
            ->where('BilletterieOrganisateur.Billetterie = ? AND BilletterieOrganisateur.User = ?', $id, $user)
            ->rawExecute();
        return $q->fetchColumn() > 0;
    }
}