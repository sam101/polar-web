<?php

require_once 'PolarObject.class.php';

class CategorieWiki extends PolarObject {
    public static $table = 'polar_wiki_categories';
    protected static $attrs = array(
        'Nom' => T_STR);
    protected static $nulls = array();

}
