<?php

/**
 * A 'string' class representing a statement parameter which have to be
 * transmited when the statement is prepared counter to usual parameters
 * Generally a SQL function such as NOW()
 * /!\ Do not construct from a user input under any circumstances
 */
class RawString {
    private $string;

    public function __construct($str) {
        $this->string = $str;
    }

    public function __toString() {
        return $this->string;
    }
}

/**
 * Create a RawString
 * You can use it as a compact alias to new RawString($str)
 */
function raw($str) {
    return new RawString($str);
}