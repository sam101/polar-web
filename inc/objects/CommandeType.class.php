<?php

require_once 'PolarObject.class.php';

class CommandeType extends PolarObject {
    public static $table = 'polar_commandes_types';
    protected static $attrs = array(
        'Nom' => T_STR,
        'CodeProduit' => T_INT,
        'Actif' => T_BOOL,
        'RequireCommande' => T_BOOL);
    protected static $nulls = array();

    /**
     * Renvoie True si la date de retrait des commandes de ce type
     * correspond à une date de retrait souhaitée
     */
    public function dateRetraitSouhaitee() {
        return in_array($this->Nom,
                        array('Poster', 'Badges', 'Plastification'));
    }

}
?>
