<?php

require_once 'PolarObject.class.php';

class Billetterie extends PolarObject {
    public static $table = 'polar_billetterie';
    protected static $attrs = array(
        'Titre' => T_STR,
        'Date' => T_STR,
        'Asso' => 'Asso',
        'Places' => T_INT,
        'Preinscription' => T_BOOL,
        'Contact' => T_STR,
        'InfosPreinscription' => T_STR,
        'CodesBarres' => T_STR,
        'CBOrientation' => array('p', 'l'),
        'CBType' => T_STR,
        'CBWidth' => T_INT,
        'CBHeight' => T_INT);
    protected static $nulls = array(
         'Asso', 'Places', 'Contact',
         'InfosPreinscription', 'CodesBarres',
         'CBOrientation', 'CBType',
         'CBWidth', 'CBHeight');

    private $organisateurs;
}
?>
