<?php

require_once 'PolarObject.class.php';

class Secteur extends PolarObject {
    public static $primary_key = 'Code';
    public static $table = 'polar_caisse_secteurs';
    protected static $attrs = array(
        'Secteur' => T_STR,
        'Payutc' => T_INT);
    protected static $nulls = array('Payutc');
    protected static $secteurs = NULL;

    public static function load() {
        foreach(self::select() as $secteur) {
            self::$secteurs[$secteur->get_id()] = $secteur;
        }
    }

    public function getSecteurs() {
        if(self::$secteurs == NULL)
            self::load();
        return self::$secteurs;
    }

    public function getPayutcId($secteur) {
        if(self::$secteurs == NULL)
            self::load();
        if (!isset(self::$secteurs[$secteur]))
            throw new UnknownObject("La catégorie $secteur n'existe pas");
        return self::$secteurs[$secteur]->Payutc;
    }
}
?>
