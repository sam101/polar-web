<?php

class AutorisationCaisse extends PolarObject {
    public static $table = 'polar_caisse_autorisations';
    protected static $attrs = array(
        'Token' => T_STR,
        'Nom' => T_STR,
        'User' => 'Utilisateur',
        'DateCreation' => T_STR,
        'DateSuppression' => T_STR);
    protected static $nulls = array(
        'DateSuppression');

    public static function getActives() {
        return self::select()->where('DateSuppression IS NULL');
    }

    public static function isValide($token) {
        return self::select('COUNT(*)')
            ->where('DateSuppression IS NULL')
            ->where('Token = ?', $token)
            ->rawExecute()->fetchColumn() > 0;
    }

    public static function desactiver($id) {
        return self::update()->set_value('DateSuppression', raw('NOW()'))
            ->where('ID = ?', (int) $id)
            ->execute()->rowCount() > 0;
    }

}