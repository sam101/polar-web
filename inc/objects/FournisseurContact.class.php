<?php

class FournisseurContact extends PolarObject {
    public static $table = 'polar_fournisseurs_contacts';
    protected static $attrs = array(
        'Fournisseur' => 'Fournisseur',
        'Info' => T_STR,
        'Nom' => T_STR,
        'DateSupprime' => T_STR);
    protected static $nulls = array('DateSupprime');
}