<?php

/**
 * Basis of the ORM :
 *
 * PolarSaveable and PolarObject.
 *
 * PolarException.
 *
 * Helper methods like format_attr() and type_is_object().
 *
 * @author Florent Thévenet
 */

const T_STR = 'string';
const T_INT = 'integer';
const T_FLOAT = 'double';
const T_BOOL = 'boolean';
$STATIC_TYPES = array(T_STR, T_INT, T_FLOAT, T_BOOL);

/**
 * Classe de base pour les exceptions de l'ORM
 */
class PolarException extends Exception { }
class InvalidAttribute extends PolarException { }
class InvalidType extends PolarException {
    function __construct($attr, $obj_type, $expected) {
        parent::__construct("Expected '$expected' but got '$obj_type' for attribute '$attr'");
    }}
class NotNullable extends PolarException { }
class InvalidValue extends PolarException { }
class InvalidModel extends PolarException { }
class AttributesNotComplete extends PolarException { }
class UnknownObject extends PolarException { }

/**
 * Renvoie True si $type représente un objet
 * (ni un type simple ni un array en fait)
 * @param $type Chaine représentant le type à tester
 */
function type_is_object($type) {
    return !($type == T_STR or $type == T_INT or
             $type == T_FLOAT or $type == T_BOOL or
             is_array($type));
}

/**
 * Formate une valeur en fonction de son type
 * pour qu'elle puisse être utilisée dans une requête SQL.
 * @todo utiliser PDO::quote() pour les chaines de caractère
 * @param $value Valeur à formater
 * @return Valeur formatée
 */
function format_attr($value) {
    if ($value === NULL)
        return 'NULL';

    switch (gettype($value)) {
    case 'string':
        // un peu violent mais facilite la vie
        if ($value === 'NOW()')
            return $value;
        else
            return PolarObject::$db->quote($value);
    case 'integer':
    case 'double':
        return (string) $value;
    case 'boolean':
        return ($value ? "1" : "0");
    case 'object':
        return (string) $value->get_id();
    default:
        return "NULL";
    }
}

/*
Interface pour les objets sauvegardable dans la base de données:

Les fonctions get_necessaires() et get_dependants() doivent renvoyer respectivement
les objets dont l'id est nécessaire à la sauvegarde de l'objet actuel et les objets
dont l'ID de l'objet actuel est nécessaires à la sauvegarde.

La fonction save peut renvoyer une requête sql sous forme de chaine de caractères
ou un array de de requêtes qui seront exécutées en une transaction.

Lors de la sauvegarde les fonctions sont exécutées dans cet ordre
get_necessaires()
save()
get_dependants()

Voir le code de PolarDB::save pour plus de détails
*/
interface PolarSaveable
{
    public function save();
    public function get_necessaires();
    public function get_dependants();
}

/**
 * An object in the Polar DB.
 *
 * Représente un objet dans la base de donnée, 
 * c'est à dire toute ligne qui possède un champ ID primaire.
 *
 * Pour chaque table avec un champ ID primaire,
 * il faut créer une classe dérivée en modifiant les attributs
 * $attrs, $nulls et $table.
 *
 * On peut aussi définir des fonctions relatives à l'objet représenté
 * qui permettent de manipuler plus facilement cet objet.
 * 
 * Il est aussi possible d'ajuster l'attribut $primary_key pour les
 * tables qui ont une clé primaire simple mais qui ne s'appele pas 'ID'.
 *
 * Il est obligatoire de définir l'attribute statique PolarObject::$db
 * avant de créer une instance de PolarObject.
*/
abstract class PolarObject implements PolarSaveable {
    private $id = NULL;
    public static $db = NULL; /**< PolarDB avec laquelle on va faire les requêtes */
    protected static $attrs = array(); /**< Liste des champ dans la table */
    protected static $nulls = array(); /**< Liste des champs NULL */
    public static $primary_key = 'ID'; /**< Nom de la clé primaire */
    protected $values = array();
    protected $modified = array();
    protected $additional_values = array();
    protected static $table;


    /**
     * Construit un PolarObject
     * à partir d'un array clé => valeur.
     *
     * La validité des clés et des valeurs est vérifiée et une exception sera renvoyé
     * en cas de problème.
     *
     * L'identifiant de l'objet est NULL, il faut sauvegarder l'objet en utilisant
     * save pour l'enregistrer et lui attribuer un ID.
    */
    public function __construct($data=array()) {
        if ($this::$db === NULL)
            throw new Exception('You must set PolarObject::$db to instanciate PolarObjects');

        // Spécial pour le chargement depuis la db, permet d'éviter
        // une boucle en plus
        if ($data == false)
            return;

        $used_attributes = array();
        // on ajoute les attributs
        foreach ($this::$attrs as $key => $type) {
            if (array_key_exists($key, $data)) {
                // Si l'attribut est défini dans $data on le stocke
                // et on note qu'on a utilisé cet attribut
                $used_attributes[] = $key;

                if (type_is_object($type) and $data[$key] !== NULL and !($data[$key] instanceof $type)) {
                    $this->__set($key, intval($data[$key]));
                } else {
                    $this->__set($key, $data[$key]);
                }
            } else {
                // sinon on met une valeur par défaut
                if (in_array($key, $this::$nulls)) {
                    $this->values[$key] = NULL;
                } else {
                    if ($type == T_STR)
                        $this->values[$key] = '';
                    else if($type == T_BOOL)
                        $this->values[$key] = false;
                    else if(is_array($type))
                        $this->values[$key] = $type[0];
                    else // int, float, objects
                        $this->values[$key] = 0;
                }
            }
        }

        // Il ne devrait pas rester d'attributs inutilisés mais on vérifie quand
        // même pour voir si l'utilisateur ne s'est pas trompé sur un nom
        // d'attribut
        $attributes_left = array_diff(array_keys($data), $used_attributes);
        if (count($attributes_left) > 0) {
            throw new InvalidAttribute("Attributs non utilisés lors de la création d'un object ".get_called_class()." : " . implode(', ', $attributes_left));
        }
    }

    /**
     * Hydrate l'objet.
     *
     * L'array attendu est le résulat d'une requête select PDO.
     *
     * Si tous les champs attendus ne sont pas présents dans $data une exception sera levée.
     *
     * Si des valeur additionelles sont présentes elles seront sauvegardées et accessible en lecture
     * comme une valeur classique (mais pas en écriture).
     *
     * Les objets liés ne sont pas automatiquement chargés.
     * @param Array clé => valeur
    */
    public function hydrate(array $data) {
        // On vérifie que les attributs nécessaires sont bien présent
        // et on assigne les valeurs
        foreach ($this::$attrs as $key => $type) {
            if (!array_key_exists($key, $data))
                throw new InvalidModel("Key '$key' expected but not present in the database");
            if (type_is_object($type) and $data[$key] !== NULL and $data[$key] != 0) {
                $this->values[$key] = intval($data[$key]);
            } else {
                $this->__set($key, $data[$key]);
            }
            unset($data[$key]);
        }
        $this->additional_values = $data;
        unset($this->additional_values[$this::$primary_key]);

        // On va ensuite ajouter les valeurs supplémentaires
        // obtenus par jointure par exemple
        //TODO

        $this->id = (int) $data[$this::$primary_key];
    }

    public function __get($attr) {
        list($val, $type) = $this->get_stored_attribute($attr);

        if (type_is_object($type) and is_int($val)) {
            $val = $type::getById($val);
            $this->values[$attr] = $val;
        }
        return $val;
    }

    /**
     * On renvoie l'attribut attendu tel que stocké actuellement et son type
     * ou un InvalidAttribut si l'attribut n'existe pas
     * Dans le cas d'un attribut supplémentaire on renvoie le type T_STR
     */
    private function get_stored_attribute($attr) {
        if (!array_key_exists($attr, $this::$attrs)) {
            if (array_key_exists($attr, $this->additional_values))
                return array($this->additional_values[$attr], T_STR);
            else
                throw new InvalidAttribute($attr);
        }

        if (!array_key_exists($attr, $this->values))
            return NULL;

        return array($this->values[$attr], $this::$attrs[$attr]);
    }

    public function __set($attr, $value) {
        if (!array_key_exists($attr, $this::$attrs))
            throw new InvalidAttribute($attr);

        $expected = $this::$attrs[$attr];
        // 1er cas : si la value est null
        // et qu'on a pas le droit : on renvoie une exception
        if ($value === NULL) {
            if (in_array($attr, $this::$nulls))
                $this->values[$attr] = $value;
            else
                throw new NotNullable($attr);
        }
        // Deuxième cas : si on a un enum
        // On teste si la valeur est autorisée sinon exception
        else if (is_array($expected)) {
            if (in_array($value, $expected))
                $this->values[$attr] = $value;
            else
                throw new InvalidValue("$value not in allowed values for $attr");
        }
        // 3eme cas : on attend un objet,
        // Si on reçoie un objet de la bonne classe c'est cool
        // Si c'est un int on vérifie que la ligne existe bien dans la base
        // Sinon exception
        else if (type_is_object($expected)) {
            if (is_object($value) and $value instanceof $expected)
                $this->values[$attr] = $value;
            else if (is_int($value)) {
                if($this::$db->validObject($expected, $value))
                    $this->values[$attr] = $value;
                else
                    throw new InvalidValue('No object \''.$expected
                                           .'\' has id '.$value);
            }
            else
                throw new InvalidType($attr, gettype($value), $expected);
        }
        // Finalement on va tenter de convertir la valeur reçue vers la type attendu
        // En cas d'échec on balance une exception
        else {
            // petite exception pour les RawString : il ne faut pas les convertir en chaine immédiatement
            if ($value instanceof RawString) {
                $this->values[$attr] = $value;
            } else {
                $val_type = gettype($value);
                if (settype($value, $expected))
                    $this->values[$attr] = $value;
                else
                    throw new InvalidValue("Could not convert $value from $val_type to $expected");
            }
        }
        if ($this->id !== NULL)
            $this->modified[$attr] = 1;
    }

    /**
     *
     */
    public function get_object_id($attr) {
        list($val, $type) = $this->get_stored_attribute($attr);
        if (!type_is_object($type))
            throw new InvalidAttribute("$attr ne désigne pas un object");
        else {
            if ($val instanceof PolarObject)
                return $val->get_id();
            else
                return $val;
        }
    }

    /**
     * Renvoie la clé primaire de l'objet
     * @return string or NULL
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * Définie la clé primaire de l'objet
     *
     * Si l'objet à déjà une clé primaire ne fait rien
     */
    public function set_id($id) {
        if ($this->id == NULL)
            $this->id = $id;
    }

    /**
     * Renvoie la liste des objets qui doivent être enregistrés avant
     * de pouvoir enregistrer l'objet en cours
     * @return Array of PolarSaveable
     */
    public function get_necessaires() {
        $objs = array();

        foreach ($this->values as $obj) {
            if ($obj instanceof PolarSaveable)
                $objs[] = $obj;
        }
        return $objs;
    }

    /**
     * Renvoie la liste des objets qui doivent être enregistrés après
     * l'objet en cours
     * @return Array of PolarSaveable
     */
    public function get_dependants() {
        return array();
    }

    /**
     * Enregistre l'objet en cours.
     *
     * Si il n'est pas encore dans la base, fait un insert et récupère la clé primaire
     *
     * Sinon fait un update sur les champs modifiés
     * @todo sauvegarder les objets nécessaires
     */
    public function save() {
        if ($this->id === NULL)
            return $this->initial_insert();

        if (!empty($this->modified)) {
            $q = $this::$db->q_update(get_class($this));
            $q->where($this::$primary_key.'=?', (int)$this->id);

            foreach ($this->modified as $attr => $thing) {
                $q->set_value($attr, $this->values[$attr]);
            }

            $q->rawExecute();
            $this->modified = array();
        }
    }

    private function initial_insert() {
        if ($this->id !== NULL)
            return "";

        $fields = array();
        $values = array();
        $q = $this::$db->q_insert(get_class($this));
        foreach ($this::$attrs as $key => $value) {
            $q->set_value($key, @$this->values[$key]);
        }

        $q->rawExecute();
        $this->set_id($this::$db->lastInsertId());
    }

    /**
     * Supprime l'objet de la base
     */
    public function delete() {
        if ($this->id != NULL)
            $this::$db->q_delete(get_class($this))->where($this::$primary_key.'=?', $this->id)->rawExecute();
    }

    /**
     * Remplace les formes {{NomAttribut}} par la valeur de l'attribut dans $text
     * @param $text Texte à formater
     * @return Texte formaté
     */
    public function format_from_attributes($text) {
      $patterns = array('/\{\{'.$this::$primary_key.'\}\}/');
      $replacements = array($this->get_id());
      foreach ($this::$attrs as $key => $type) {
        $patterns[] = "/\{\{$key\}\}/";
        $replacements[] = $this->values[$key];
      }
      return preg_replace($patterns, $replacements, $text);
    }

    /**
     * Teste l'existance d'un attribut supplémentaire
     *
     * @param $attr nom de l'attribut
     * @return true ou false
     */
    public function hasAdditionnalAttr($attr) {
        return array_key_exists($attr, $this->additional_values);
    }

    /**
     * @return Le PolarObject désigné par $id
     * ou False si aucun objet ne correspond.
     */
    public static function getById($id) {
        $class = get_called_class(); // Late static Binding :-)
        $obj = PolarObject::$db->q_select($class)
                               ->where($class::$primary_key.'=?', $id)
                               ->getOne();

        if (is_null($obj))
            throw new UnknownObject($class.' : '.$id);

        return $obj;
    }

    /**
     * Pour modifier un champ rapidement sans avoir à charger l'objet entier.
     *
     * @return une PolarQuery en mode UPDATE sur la classe actuelle
     */
    public static function update() {
        return static::$db->q_update(get_called_class());
    }

    /**
     * Pour sélectionner un objet de la classe actuelle autrement
     * que par sa clé primaire.
     *
     * ATTENTION : Si vous faites des select supplémentaire il faut penser à sélectionner
     * tous les champs de la classe actuelle sous peine d'échec de l'hydration.
     * ( $q->select('<NomClasseActuelle>.*'); )
     * @param $what champ en plus à sélectionner ou NULL
     * @param $alias alias pour ce champ ou NULL
     * @see PolarQuery::select();
     * @return une PolarQuery en mode SELECT sur la classe actuelle.
     */
    public static function select($what=NULL, $alias=NULL) {
        $q = static::$db->q_select(get_called_class());
        if ($what != NULL)
            $q->select($what, $alias);
        return $q;
    }

    final public function __toString() {
        return (string) $this->get_id();
    }
}
?>
