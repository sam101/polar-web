<?php

class FournisseurTicket extends PolarObject {
    public static $table = 'polar_fournisseurs_tickets';
    protected static $attrs = array(
        'Fournisseur' => 'Fournisseur',
        'Contact' => 'FournisseurContact',
        'Nom' => T_STR,
        'Contenu' => T_STR,
        'NomFichier' => T_STR,
        'Fichier' => T_STR,
        'User' => 'Utilisateur',
        'Date' => T_STR,
        'DateSupprime' => T_STR);
    protected static $nulls = array(
        'Contact', 'DateSupprime', 'Fichier', 'NomFichier');
}