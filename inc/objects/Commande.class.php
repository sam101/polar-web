<?php

require_once 'PolarObject.class.php';

class Commande extends PolarObject {
    public static $table = 'polar_commandes';
    protected static $attrs = array(
        'Type' => 'TypeCommande',
        'Nom' => T_STR,
        'Prenom' => T_STR,
        'Mail' => T_STR,
        'Asso' => 'Asso',
        'DateCommande' => T_STR,
        'IPCommande' => T_STR,
        'DatePaiement' => T_STR,
        'IDVente' => T_INT,
        'DatePrete' => T_STR,
        'IDPreparateur' => 'User',
        'DateRetrait' => T_STR,
        'IDRetrait' => 'Utilisateur',
        'DateRetour' => T_STR,
        'IDRetour' => 'Utilisateur',
        'Panier' => 'PanierWeb',
        'Termine' => T_BOOL);
    protected static $nulls = array(
        'Nom', 'Prenom', 'Mail', 'Asso',
        'DateCommande', 'IPCommande',
        'DatePaiement', 'IDVente',
        'DatePrete', 'IDPreparateur',
        'DateRetrait', 'IDRetrait',
        'DateRetour', 'IDRetour',
        'Panier');
    private $details;

    public static $DatePreValidation = "2000-01-01 00:00:00";

    function set_payee($idvente) {
        $this->DatePaiement = raw('NOW()');
        $this->IDVente = $idvente;
    }
        
    function set_prete($preparateur) {
        $this->IDPreparateur = $preparateur;
        $this->DatePrete = raw('NOW()');
    }
    
    function set_retiree($preparateur, $termine=0) {
        $this->IDRetrait = $preparateur;
        $this->DateRetrait = raw('NOW()');
    }
    
    function set_retour($preparateur, $termine=1) {
        $this->IDRetour = $preparateur;
        $this->DateRetour = raw('NOW()');
    }

    function get_details() {
        return $this->details;
    }

    function add_detail($type, $qte=NULL) {

    }

    function get_barcode() {
        if ($this->hasAdditionnalAttr('CodeCaisse')) {
            return sprintf('%04d%06d', $this->CodeCaisse, $this->get_id());
        }
    }

    
    public static function validerBilletterie($id) {
        $b = Commande::select('TarifBilletterie.Evenement')
            ->join('TarifBilletterie', 'TarifBilletterie.TypeCommande = Commande.Type')
            ->where('Commande.ID = ?', intval($id))
            ->groupBy('TarifBilletterie.Evenement');

        $r = Billetterie::select('Places - COUNT(*)')
            ->join('TarifBilletterie', 'TarifBilletterie.Evenement = Billetterie.ID')
            ->join('Commande', 'Commande.Type = TarifBilletterie.TypeCommande')
            ->addSubquery($b)
            ->where('Billetterie.ID = ('.$b->get_sql().')')
            ->where('Commande.Termine = 0 AND Commande.DatePaiement IS NOT NULL')
            ->rawExecute();
        return $r->fetchColumn(0) > 0;
    }

    /**
     * Essaye de déterminer la branche de l'étudiant qui a commandé à partir de
     * la date de retrait enregistrée
     */
    public function getBranche() {
        if ($this->DateRetrait === null)
            return false;

        switch ($this->DateRetrait) {
        case "2014-02-20 01:00:00":
            return "GM";
            break;
        case "2014-02-20 02:00:00":
            return "GSM";
            break;
        case "2014-02-20 03:00:00":
            return "GSU";
            break;
        case "2014-02-20 04:00:00":
            return "MPI";
            break;
        case "2014-02-20 05:00:00":
            return "GI";
            break;
        case "2014-02-20 05:00:00":
            return "GB";
            break;
        case "2014-02-20 07:00:00":
            return "GP";
            break;
        default:
            return False;
        }
    }

    public function estPayee() {
        return $this->DatePaiement !== null && $this->DatePaiement != $this->DatePreValidation;
    }

    /*
     * Retourne une instance de CommandeContenu non sauvegardé
     * avec le champ IDCommande pré-rempli
     */
    public function creerContenu() {
        return new CommandeContenu(array('IDCommande' => $this));
    }
}

class CommandeProjecteur extends Commande {
    public function __construct($data=NULL, $db=NULL, $type='vp-etu') {

    }
}
?>
