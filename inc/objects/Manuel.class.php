<?php

require_once 'PolarObject.class.php';

/**
 * Manuel fonctionne un peu différemment des autres objets
 * Il faut utiliser load_from_barcode et create_from_barcode
 * On ne peut changer que l'Etat
 */
class Manuel extends PolarObject {
    public static $table = 'polar_manuels_flotte';
    protected static $attrs = array(
        'Manuel' => 'TypeManuel',
        'Serie' => T_INT,
        'Numero' => T_INT,
        'Etat' => array('stock', 'location',
                         'rendu', 'perdu'));
    protected static $nulls = array();
    private $constructing;
    private $saved;

    public function hydrate($data) {
        $data['ID'] = 1;
        $this->constructing = true;
        parent::hydrate($data);
        $this->constructing = false;
    }

    public function __set($key, $value) {
        if ($key == "Etat" or $this->constructing)
            parent::__set($key, $value);
        else
            throw new Exception("Impossible de modifier '$key'");
    }

    public function get_barcode() {
        return sprintf("9%03d%03d%03d", $this->values['Manuel'], $this->Serie, $this->Numero);
    }

    public static function create($manuel, $serie, $numero) {
        $this->constructing = true;
        return new Manuel(array('Manuel' => $manuel,
                                'Serie' => $serie,
                                'Numero' => $numero,
                                'Etat' => 'Stock'));
        $this->constructing = false;
    }

    public static function load_from_barcode($barcode) {
        if (strlen($barcode) != 10)
            throw new Exception("Invalide barcode '$barcode', must be 10 characters long.");
        $manuel = intval(substr($barcode, 1, 3));
        $serie = intval(substr($barcode, 4, 3));
        $numero = intval(substr($barcode, 7, 3));
        $obj = PolarObject::$db->q_select('Manuel')->where('Manuel=? AND Serie=? AND Numero=?', $manuel, $serie, $numero)->execute();
        return ($obj instanceof Manuel) ? $obj : Manuel::create($manuel, $serie, $numero);
    }

    public function emprunter($user, $num_commande, $nom, $prenom, $mail) {
        if ($this->Etat != "location") {
            $commande = $this::$db->fetchOne("Commande", $num_commande);
            if ($commande != NULL) {
                $this->Etat = "location";
                $commande->Mail = $mail;
                $commande->Nom = $nom;
                $commande->Prenom = $prenom;
                $commande->DateRetrait = raw('NOW()');
                if ($isStaff)
                    $commande->DatePaiement = raw('NOW()');
                $commande->IDRetrait = $user;
                $commande->contenu->Detail = $this->get_barcode();
                sendMail();
            } else {
                ajouterErreur("Bon de commande invalide");
            }
        }
    }

    public function rendre($user) {

    }

    public function stock() {

    }

    public function encaisser_caution($user, $banque, $numCheque) {

    }

    public function save() {
        if ($this->get_id() == NULL) { // On crée le livre
            $this::$db->q_insert('Manuel')->set_value('Manuel', $this->Manuel)
                                          ->set_value('Serie', $this->values['Serie'])
                                          ->set_value('Numero', $this->Numero)
                                          ->set_value('Etat', $this->Etat)
                                          ->rawExecute();
            $this->set_id(1);
        } else {
            if (isset($this->modified['Etat'])) { // Un seul champ à sauvegarder ^^
                $this::$db->q_update('Manuel')->where('Manuel=? AND Serie=? AND Numero=?', $this->values['Manuel'], $this->Serie, $this->Numero)->set_value('Etat', $this->Etat)->rawExecute();
                $this->modified = array();
        }
    }
}
?>
