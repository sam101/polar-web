<?php

require_once 'PolarObject.class.php';

class DetailsCaisse extends PolarObject {
  public static $table = 'polar_caisse_details';
  protected static $attrs = array(
      'c1' => T_INT,
      'c2' => T_INT,
      'c5' => T_INT,
      'c10' => T_INT,
      'c20' => T_INT,
      'c50' => T_INT,
      'e1' => T_INT,
      'e2' => T_INT,
      'e5' => T_INT,
      'e10' => T_INT,
      'e20' => T_INT,
      'e50' => T_INT,
      'e100' => T_INT);
  protected static $nulls = array();

  public function getTotal() {
      return $this->e100 * 100 + $this->e50 * 50 + $this->e20 * 20 +
          $this->e10 * 10 + $this->e5 * 5 + $this->e2 * 2 + $this->e1 +
          $this->c50 * 0.5 + $this->c20 * 0.2 + $this->c10 * 0.1 +
          $this->c5 * 0.05 + $this->c2 * 0.02 + $this->c1 * 0.01;
  }

}