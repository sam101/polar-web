<?php

/**
 * Classe gérant tous les mails qui peuvent être envoyés à propos d'une commande
 * Archivage d'un panier, validation d'une commande, paiement d'un panier,
 * réalisation d'une commande...
 */
class CommandesMailer {
    public static $format_commande =  "<table>
            <tr><td>Article</td><td>%nom%</td></tr>
            <tr><td>Identifiant</td><td>%id%</td></tr>
            <tr><td>%nom_qte%</td><td>%qte%</td></tr>
            %more%
            <tr><td>Prix TTC</td><td>%prix% &euro;</td></tr>
            </table>";

    /**
     * Envoie un mail annonçant que le panier a bien été archivé
     */
    public static function archiverPanier($panier) {
        if ($panier->User === null) {
            Log::info("Impossible d'envoyer le mail d'archivage, pas d'utilisateur enregistré pour le panier ".$panier);
            return false;
        }

        $intro = "
      <p>Nous venons d'archiver votre panier.<br/>
      Pour continuer vos achats, suivez simplement le lien suivant : <a href=\"".self::urlAbsolue(urlTo('commander', 'panier_reprendre', 'panier='.$panier))."\">continuer la commande</a>
      </p>";

        $listing = "<h3>Contenu de votre panier</h3>" . self::createListing($panier->getCommandes());

        $texte = self::createHtml("Archivage d'un panier",
                                  $intro, $listing);

        self::sendMail($texte, "Le Polar - Votre panier", $panier->Mail);

        return true;
    }

    /**
     * Envoie un mail annonçant que le panier est bien enregistré et
     * l'id de commande pour venir le payer au Polar
     */
    public static function validerPanier($panier) {
        if ($panier->Mail === null) {
            Log::error("Impossible d'envoyer le mail de validation, pas de mail enregistré pour le panier ".$panier);
            return false;
        }

        $intro = "
      <p>Nous vous remercions de votre commande.<br/>
      Votre commande n'est pas encore payée, pour la payer vous pouvez passer au Polar muni du numéro suivant : <b>".$panier->CommandeGroupe."</b><br/>";

        if ($panier->User !== null) {
            $intro .= "<br/>
      Vous pouvez également <a href=\"".self::urlAbsolue(urlTo('commander', 'panier_reprendre', 'panier='.$panier))."\">vous connecter sur le site du Polar</a> pour payer votre commande en ligne, grâce à payutc.";
        }

        $intro .= "</p>";

        $listing = "<h3>Récapitulatif de votre commande</h3>" . self::createListing($panier->getCommandes());

        $texte = self::createHtml("Validation de votre commande",
                                  $intro, $listing);

        self::sendMail($texte, "Le Polar - Validation de votre commande", $panier->Mail);

        return true;
    }

    /**
     * Envoie un mail annonçant que la commande est bien enregistrée et
     * l'id de commande pour venir la payer au Polar
     */
    public static function validerCommande($commande) {
        $intro = "
      <p>Nous vous remercions de votre commande.<br/>
      Votre commande n'est pas encore payée, pour la payer vous pouvez passer au Polar muni du numéro suivant : <b>".$commande."</b><br/></p>";

        // on récupère l'object commande avec toutes les infos nécessaires
        $commandes = PanierWeb::prepareQuery()
            ->where('Commande.ID = ?', $commande);

        $listing = "<h3>Récapitulatif de votre commande</h3>" . self::createListing($commandes);

        $texte = self::createHtml("Validation de votre commande",
                                  $intro, $listing);

        self::sendMail($texte, "Le Polar - Validation de votre commande", $commande->Mail);

        return true;
    }

    /**
     * Envoie un mail récapitulatif de la commande
     */
    public static function confirmerPaiementWeb($panier) {
        if ($panier->Mail === null) {
            Log::error("Impossible d'envoyer le mail de validation, pas de mail enregistré pour le panier ".$panier);
            return false;
        }

        $intro = "
      <p>Nous vous remercions de votre commande.<br/>
      Votre commande est payée, elle sera prête d'ici 72 heures en fonction des articles commandés.<br/>
      Vous recevrez un mail de confirmation lors de la réalisation de vos commandes.</p>";
        $listing = "<h3>Récapitulatif de votre commande</h3>" . self::createListing($panier->getCommandes());

        $texte = self::createHtml("Validation de votre commande",
                                  $intro, $listing);

        return self::sendMail($texte, "Le Polar - Validation de votre commande", $panier->Mail);

        return true;
    }
    /**
     * Envoie un mail récapitulatif de la transaction + facture
     */
    public static function confirmerPaiementAsso($mail, $commandes) {
        $intro = "
      <p>Nous vous remercions de votre commande.<br/>
      Votre paiement a été enregistré sur le compte asso, elle sera prête d'ici 72 heures en fonction des articles commandés.<br/>
      Vous recevrez un mail de confirmation lors de la réalisation de vos commandes.</p>";
        $listing = "<h3>Récapitulatif de votre commande</h3>" . self::createListing($commandes);

        $texte = self::createHtml("Validation de votre commande",
                                  $intro, $listing);

        return self::sendMail($texte, "Le Polar - Validation de votre commande", $mail);

        return true;
    }

    /**
     * Envoie un mail de rappel précisant que la commande n'est pas encore payée
     * @throw MailerException
     */
    public static function rappelPaiementCommande($commande) {
        if ($commande->DatePaiement !== null)
            throw new MailerException("Commande déjà payée");

        $intro = "
      <p>Vous avez effectué une commande sur le site du Polar.<br/>
      Nous vous rappelons que les commandes ne sont réalisées que si elles si payées.<br/>";
        if ($commande->Panier !== null) {
            $intro .= "Vous pouvez payer en ligne par Payutc ou par Carte Bleue en suivant le lien suivant <a href=\"".urlTo('commander', 'panier_reprendre', 'panier='.$commande->Panier)."\">Réactiver votre panier</a>.<br/>";
        } else {
            // on demande au mec de nous contacter si il veut payer en ligne
            $intro .= "Si vous souhaitez payer en ligne merci de répondre à ce mail en demandant à ce que l'on vous crée un panier.<br/>";
        }
        $intro .= "Vous pouvez également payer au Polar par Carte Bleue, Chèque ou Espèces.</p>";

        $texte = self::createHtml("Paiement de votre commande", $intro);

        return self::sendMail($texte, "Le Polar - Commande $commande non payée", $commande->Mail);

        return true;
    }

    /**
     * Formatte le mail à partir des infos passées à la fonction et
     * du modèle html
     */
    public static function createHtml($titre, $intro, $fin="", $listing=null) {
        global $CONF;
        ob_start();
        include 'modules/commander/_modele_mail.php';
        return ob_get_clean();
    }

    /**
     * Génère le balisage html pour le listing des commandes
     * @param commandes PolarObjectsArray ou PolarQuery de Commande
     * les objects commandes doivent avoir certains attributs additionels
     * cf PanierWeb::prepareQuery()
     */
    public static function createListing($commandes) {
        $listing = '';
        $prix = 0;

        foreach($commandes as $commande) {
            $res= self::formatCommande($commande);
            $listing .= $res[0] . "<hr/>";
            $prix += $res[1];
        }
        $listing .= "<p><b>Total</b> : ".formatPrix($prix)." &euro;</p>";

        return $listing;
    }

    public static function formatCommande($commande) {
        $article = Article::getById($commande->Article);
        $prix = $article->calculerPrix($commande->Quantite, false);

        if ($commande->NomType == 'Billet')
            $nom = $commande->NomBilletterie . ' : ' .$commande->NomTarif;
        else
            $nom = $commande->NomArticle;

        $nom_qte = "Quantité";
        $more = "";
        if ($commande->NomType == 'Annale') {
            require_once('inc/annales.php');

            $uvs = array();
            foreach(explode(' ', $commande->Contenu) as $uv) {
                $uvs[] = Annales::parseCode($uv);
            }
            $more = "<tr><td>UVs</td><td>".implode('; ', $uvs)."</td></tr>";

            $nom_qte = "Nombre de pages";
        } else if ($commande->NomType == 'Videoprojecteur') {
            $more = "<tr><td>Retrait</td><td><b>Vous pourrez retirer le vidéo-projecteur le jour de la réservation dès 12h30</b></td></tr>";
        }

        return array(str_replace(array('%nom%', '%id%',
                                       '%nom_qte%', '%qte%',
                                       '%more%', '%prix%'),
                                 array($nom, $commande->get_id(),
                                       $nom_qte, $commande->Quantite,
                                       $more, formatPrix($prix)),
                                 self::$format_commande),
                     $prix);
    }

    public static function sendMail($html, $titre, $destinataire) {
        sendMail('polar@assos.utc.fr', 'Le Polar', array($destinataire),
                 $titre, $html);
    }

    public static function urlAbsolue($url) {
        global $CONF, $racine;
        return substr($CONF['polar_url'], 0, -strlen($racine)) . $url;
    }
}
