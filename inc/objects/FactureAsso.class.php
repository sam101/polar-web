<?php

require_once 'PolarObject.class.php';

class FactureAsso extends PolarObject {
    public static $table = 'polar_assos_factures';
    protected static $attrs = array(
        'Asso' => 'Asso',
        'Date' => T_STR,
        'Montant' => T_FLOAT,
        'Encaisse' => T_BOOL,
        'Cheque' => T_INT);
    protected static $nulls = array();

    public function getNumero() {
        return 'A'.$this->get_id().'.'.date('dmY', strtotime($this->Date));
    }

    public function genererPDF($duplicata = False) {
        $ventes = OldVente::select('OldVente.*')
            ->select('Article.Nom, Article.PrixVente, Article.PrixVenteAsso, Article.TVA, CompteAsso.Nom as Compte')
            ->join('Article', 'Article.ID = OldVente.Article')
            ->join('CompteAsso', 'OldVente.Asso = CompteAsso.ID')
            ->where('OldVente.Facture = ?', $this)
            ->order('OldVente.Date ASC');

        require_once('inc/tcpdf.php');

        if($duplicata)
            $pdf = new PolarPDF("DUPLICATA DE FACTURE");
        else
            $pdf = new PolarPDF("FACTURE");

        $pdf->SetFontSize(12);
        $output = '<p align="right">'."Facture N°".$this->getNumero();
        $output .= ' du '.date("d/m/Y", strtotime($this->Date)).'<br>';
        $output .= 'Client : '.stripslashes(html_entity_decode($this->Asso->Asso, ENT_COMPAT, 'UTF-8')).'</p>';

        $pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
        $pdf->Ln(10);

        $pdf->SetFontSize(10);
        $output = '<table border="1" cellpadding="4">
	<tr>
		<th width="315"><b>Désignation</b></th>
		<th width="85"><b>Date</b></th>
		<th width="70"><b>PU TTC</b></th>
		<th width="45"><b>Qté</b></th>
		<th width="55"><b>TVA</b></th>
		<th width="75"><b>Total TTC</b></th>
	</tr>';

        $detail_comptes = array();
        $montant_comptes = array();

        $montant = 0;
        $tva = 0;
        $tvas = array();
        // Données
        foreach($ventes as $vente) {
            if($vente->Tarif == 'asso' && $vente->PrixVenteAsso != 0)
                $prixVente = $vente->PrixVenteAsso;
            else
                $prixVente = $vente->PrixVente;

            $ligne = '<tr>';
            $ligne .= '<td width="315">'.$vente->Nom;
            if(!is_null($vente->Client))
                $ligne .= '<br>  '.stripslashes($vente->Client);
            $ligne .= '</td>';

            $ligne .= '<td width="85">'.stripslashes($vente->Date).'</td>';
            $ligne .= '<td width="70" align="right">'.number_format($prixVente, 2, ',', ' ').' €</td>';
            $ligne .= '<td width="45" align="right">'.$vente->Quantite.'</td>';
            $ligne .= '<td width="55" align="right">'.number_format($vente->TVA*100, 1, ',', ' ').'%</td>';
            $ligne .= '<td width="75" align="right">'.number_format($vente->PrixFacture, 2, ',', ' ').' &euro;</td>';
            $ligne .= '</tr>';

            if (isset($detail_comptes[$vente->Compte])) {
                $detail_comptes[$vente->Compte] .= $ligne;
                $montant_comptes[$vente->Compte] += $vente->PrixFacture;
            }
            else {
                $detail_comptes[$vente->Compte] = $ligne;
                $montant_comptes[$vente->Compte] = $vente->PrixFacture;
            }
            $output .= $ligne;

            $montant += $vente->PrixFacture;
            $tva += $vente->MontantTVA;

            if (!isset($tvas[$vente->TVA])) {
                $tvas[$vente->TVA] = array("Taux" => $vente->TVA * 100,
                                           "Total" => 0,
                                           "TotalTVA" => 0);
            }
            $tvas[$vente->TVA]["Total"] += $vente->PrixFacture;
            $tvas[$vente->TVA]["TotalTVA"] += $vente->MontantTVA;
        }

        $output .= '</table>';

        $pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
        $pdf->Ln(10);

        $output = '<p align="right"><b>Montant HT : '.number_format($montant - $tva, 2, ',', ' ').' €<br>';
        $output .= 'Montant TTC : '.number_format($montant, 2, ',', ' ').' €</b></p>';
        $pdf->SetFontSize(12);
        $pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');


        $output = '<br/><br/>Détails de la TVA<br/>';
        $pdf->SetFontSize(10);
        $output .= '<table border="1" cellpadding="4" width="50%">';
        $output .= '<tr><th>Montant HT</th><th>Taux</th><th>Montant TVA</th></tr>';

        foreach($tvas as $t => $tva) {
            $output .= '<tr><td>' . formatPrix($tva["Total"] - $tva["TotalTVA"]) . '€</td><td>' . number_format($tva["Taux"], 1, ',', '') . '%</td><td>' . formatPrix($tva["TotalTVA"]) . '€</td></tr>';
        };
        $output .= "</table>";

        $output .= "<p><br/><br/>";

        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->Date);
        $date->add(new DateInterval('P2M')); // lol 2 mois, merci PHP...
        $output .= "<b>À régler au plus tard le " . $date->format('d/m/Y') . "</b><br/>";
        $output .= "<br/>Pas d'escompte en cas de paiement anticipé.";

        $pdf->Image('upload/signature_prez.jpg',135, '', 60, '', 'JPG', '', '', true, 150);

        $pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');

        if (count($detail_comptes) > 1) {
            $pdf->SetFontSize(10);
            foreach($detail_comptes as $compte => $tableau) {
                $pdf->AddPage();
                $output = "<h3>$compte</h3>";
                $output .= '<table border="1" cellpadding="4">
	<tr>
		<th width="315"><b>Désignation</b></th>
		<th width="85"><b>Date</b></th>
		<th width="70"><b>PU TTC</b></th>
		<th width="45"><b>Qté</b></th>
		<th width="55"><b>TVA</b></th>
		<th width="75"><b>Total TTC</b></th>
	</tr>'.$tableau.'</table>';
                $output .= '<p align="right">Total : '.number_format($montant_comptes[$compte], 2, ',', ' ').' €</b></p>';
                $pdf->writeHTML($output, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
            }

        }

        $file = 'Documents/facture_'.$this->getNumero().'_'.date('HmidmY').'.pdf';
        $pdf->Output($file, 'F');

        return $file;
    }

    public function encaisserCheque($num_cheque, $banque) {
        $cheque = new Cheque(array('Date' => raw('NOW()'),
                                   'Numero' => $num_cheque,
                                   'Banque' => $banque,
                                   'Montant' => $this->Montant,
                                   'Emetteur' => $this->Asso->Asso,
                                   'Ordre' => 'Le Polar',
                                   'PEC' => 1)
                             );
        $cheque->save();

        // La facture est payée
        $this->Encaisse=true;
        $this->Cheque=$num_cheque;

        // Envoi d'un mail de confirmation
        $msg = "Bonjour,<br /><br />
	Nous vous confirmons la réception du paiement de la facture ".$this->getNumero()." par chèque $banque n°$num_cheque, d'un montant de ".$this->Montant." euros.<br /><br />
	Cordialement,<br />L'&eacute;quipe du Polar";
        $this->Asso->sendMail('polar@assos.utc.fr', 'Le Polar', "[Le Polar] Réception du paiement de votre facture", $msg);

        $this->save();
    }

    public function encaisserVirement() {
        // La facture est payée
        $this->Encaisse = true;

        // Envoi d'un mail de confirmation
        $msg = "Bonjour,<br /><br />
	Nous vous confirmons la réception du paiement de la facture ".$this->getNumero()." par virement, d'un montant de ".$this->Montant." euros.<br /><br />
	Cordialement,<br />L'&eacute;quipe du Polar";
        $this->Asso->sendMail('polar@assos.utc.fr', 'Le Polar', "[Le Polar] Réception du paiement de votre facture", $msg);

        $this->save();
    }
}
?>
