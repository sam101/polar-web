<?php

class PanierWeb extends PolarObject {
    public static $table = 'polar_commandes_paniers_web';
    protected static $attrs = array(
        'TransactionID' => T_INT,
        'DateCreation' => T_STR,
        'DateValidation' => T_STR,
        'Etat' => array('filling', 'saved', 'validated', 'sent', 'paid', 'failed'),
        /* Détails des états:
         * filling : l'utilisateur est en train de faire sa commande (il -rempli- son panier
         * saved : l'utilisateur a sauvegardé son panier pour le payer au polar
         * sent : l'utilisateur a été envoyé vers le service de paiement, à partir de ce moment TransactionId n'est plus NULL
         * paid : transaction validée par le servic de paiement
         * failed : transaction échouée, erreur consignée par mail et dans les logs
         */
        'User' => 'Utilisateur',
        'Mail' => T_STR,
        'IDVente' => T_STR,
        'CommandeGroupe' => 'Commande');
    protected static $nulls = array(
        'TransactionID', 'IDVente',
        'User', 'DateValidation',
        'Mail', 'CommandeGroupe');

    private static $etatsAffichables = array('filling' => "En cours",
                                      'saved' => "Sauvegardé",
                                      'validated' => "Validé (à payer au Polar)",
                                      'sent' => "En attente de la confirmation de paiement",
                                      'paid' => "Payé",
                                      'failed' => "Échec de paiement");

    public static function getForUser($user) {
        return self::select()->where('User = ?', $user);
    }

    public function getEtatAffichable() {
        return self::$etatsAffichables[$this->Etat];
    }

    public function reactivable() {
        return in_array($this->Etat, array('filling', 'saved', 'validated', 'failed'));
    }

    public static function getForTransaction($tr_id) {
        return self::select()
            ->where('TransactionID = ?', $tr_id)
            ->getOne();
    }

    /*
     * Renvoie une instance de PolarQuery pour récupérer les infos
     * associées aux commandes
     * la fonction appelante doit remplir le champs where elle même
     */
    public static function prepareQuery() {
        $query = Commande::select('Commande.*, Article.Nom as NomArticle, Article.CodeCaisse, TarifBilletterie.Intitule as NomTarif, Billetterie.Titre as NomBilletterie, CommandeType.Nom as NomType, COALESCE(SUM(CommandeContenu.Quantite), 1) as Quantite, COALESCE(GROUP_CONCAT(CommandeContenu.Detail SEPARATOR " "), "") as Contenu, Article.ID as Article')
            ->where('Article.EnVente = 1 AND Article.Actif = 1')
            ->where('Commande.Termine = 0') // 1 = commande annulée
            ->leftJoin('CommandeType', 'CommandeType.ID = Commande.Type')
            ->leftJoin('CommandeContenu', 'CommandeContenu.IDCommande = Commande.ID')
            ->leftJoin('Article', 'Article.CodeCaisse = CommandeType.CodeProduit')
            ->leftJoin('TarifBilletterie', 'TarifBilletterie.TypeCommande = Commande.Type')
            ->leftJoin('Billetterie', 'Billetterie.ID = TarifBilletterie.Evenement')
            ->groupBy('CommandeContenu.IDCommande, Commande.ID');

        return $query;
    }

    /*
     * Renvoie une PolarQuery correspondant à toutes
     * les commandes du Panier
     */
    public function getCommandes() {
        $q = $this::prepareQuery();
        $q->where('Commande.Panier = ?', $this);
        return $q;
    }
}