<?php

class Permanence extends PolarObject {
    public static $table = 'polar_perms';
    public static $primary_key = 'id';
    protected static $attrs = array(
        'Jour' => T_INT,
        'Creneau' => T_INT,
        'Permanencier' => 'Utilisateur');
    protected static $nulls = array();
}