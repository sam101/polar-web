<?php

class Utilisateur extends PasswordProtected {
    public static $table = 'polar_utilisateurs';
    protected static $attrs = array(
        'IPClient' => T_STR,
        'DateCreation' => T_STR,
        'Login' => T_STR,
        'MotDePasse' => T_STR,
        'MotDePasseSecurise' => T_BOOL,
        'Email' => T_STR,
        'Staff' => T_BOOL,
        'Nom' => T_STR,
        'Bureau' => T_BOOL,
        'Ancien' => T_BOOL,
        'Responsable' => T_INT,
        'Poste' => T_STR,
        'Presentation' => T_STR,
        'Prenom' => T_STR,
        'Sexe' => array('m', 'f'),
        'Telephone' => T_STR,
        'Newsletter' => T_STR);
    protected static $nulls = array(
        'Responsable', 'Poste',
        'Sexe', 'Telephone');

    /**
     * Crée un utilisateur Polar à partir de son login en se basant
     * sur les informations fournis par Ginger (outils cotisant du simde)
     *
     * Si l'utlisateur avec ce login existe déjà dans la base cette fonction renvoie
     * l'instance Utilisateur correspondante
     * Sinon elle renvoie une instance d'Utilisateur crée à partir
     * des infos dans récupérée depuis Ginger
     * L'utilisateur de la fonction doit appeler save() sur l'objet récupéré
     * si il souhaite sauvegarder cet utilisateur dans la base
     * Cela permet de différencier un nouvel utilisateur d'un utilisateur
     * déjà existant
     * En cas d'échec de récupération des infos depuis Ginger lance une ApiException
     */
    public static function create_from_ginger($ginger, $login) {
        $class = get_called_class();

        // on vérifie si l'utilisateur avec ce login existe déjà
        $utilisateur = $class::select()->where('Login LIKE ?', $login)
            ->getOne();

        if ($utilisateur === null) {
            // On crée un nouvel Utilisateur avec les valeurs
            // par défaut et celles obtenues depuis ginger
            $user = $ginger->getUser($login); // throw ApiException

            $utilisateur =  new $class(array('IPClient' => get_ip(),
                                             'DateCreation' => raw('NOW()'),
                                             'Login' => $login,
                                             'MotDePasse' => createHash(genererAleatoire(8)),
                                             'Email' => $user->mail,
                                             'Presentation' => 'Ce membre n&rsquo;a pas encore de pr&eacute;sentation.',
                                             'Nom' => $user->nom,
                                             'Prenom' => $user->prenom,
                                             'Newsletter' => 1));
        }

        return $utilisateur;
    }

    public function sendMail($from, $from_nom, $sujet, $html, $pj=array()) {
        sendMail($from, $from_nom, array($this->Email), $sujet, $html, $pj);
    }

    public function getPhoto() {
        if(is_file('upload/profil/'.$this->get_id().'_mid.jpg'))
            return 'upload/profil/'.$this->get_id().'_mid.jpg';
        else
            return 'styles/0/icones/inconnu.jpg';
    }

    public function nomComplet() {
        return $this->Prenom . ' ' . $this->Nom;
    }
}
