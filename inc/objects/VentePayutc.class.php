<?php

class VentePayutc extends PolarObject {
    public static $table = 'polar_caisse_ventes_payutc';
    public static $primary_key = 'IDVente';
    protected static $attrs = array(
        'IDTransaction' => T_INT,
        'DateVersement' => T_STR,
        'DateAnnule' => T_STR);
    protected static $nulls = array('DateVersement', 'DateAnnule');
    private $saved = null;

    public function __construct($transaction_id=null, $idVente=null) {
        if (!is_null($transaction_id)) {
            $this->set_id($idVente);
            $this->IDTransaction = $transaction_id;
            $this->DateVersement = null;
            $this->DateAnnule = null;
            $this->saved = false;
        }
    }

    public function hydrate(array $data) {
        parent::hydrate($data);
        $saved = true;
    }

    public function save() {
        if (is_null($this->saved))
            return;
        else if ($this->saved) // mise à jour
            parent::save();
        else { // creation
            $fields = array();
            $values = array();
            $q = $this::$db->q_insert(get_class($this));
            $q->set_value(self::$primary_key, $this->get_id());
            foreach ($this::$attrs as $key => $value) {
                $q->set_value($key, $this->values[$key]);
            }

            $q->rawExecute();
            $this->saved = true;
        }
    }
}