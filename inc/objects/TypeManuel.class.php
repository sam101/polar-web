<?php

require_once 'PolarObject.class.php';

class TypeManuel extends PolarObject {
    public static $table = 'polar_manuels';
    protected static $attrs = array(
        'UV' => T_STR,
        'Photo' => T_STR,
        'Actif' => T_BOOL,
        'CD' => T_BOOL);
    protected static $nulls = array();

    private $organisateurs;
}
?>
