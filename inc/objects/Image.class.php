<?php

/**
 * Classe de gestion des images
 * 
 * Requière librarie GD et EXIF
 *
 * @author Antoine GAVEL, Florent THEVENET
 */

class Image
{
	private $image,
			$width,
			$height;

	private static $logo = 'upload/logo.png';

	const TOP = 1,
		  BOTTOM = 2,
		  LEFT = 3,
		  RIGHT = 4,
		  MIDDLE = 5;

	public function __construct($file)
	{
		$this->load($file);
	}

	public function __destruct()
	{
		if ($this->image)
			imagedestroy($this->image);
	}

	/**
	 * Setter width
	 */
	public function setWidth($width)
	{
		$this->width = (int) $width;
	}

	/**
	 * Setter height
	 */
	public function setHeight($height)
	{
		$this->height = (int) $height;
	}

	/**
	 * Get width
	 */
	public function width()
	{
		return $this->width;
	}

	/**
	 * Get height
	 */
	public function height()
	{
		return $this->height;
	}

	/**
	 * Chargement d'une image
	 *
	 * @param $file : chemin vers l'image JPG, PNG, GIF
	 * @param $transparency bool : pour générer un fond transparent au chargement
	 * @return void
	 * @author Antoine GAVEL
	 */
	public function load($file)
	{
		$this->image = $this->createImage($file);

		// Récupération des informations sur l'image
		$this->setWidth(imagesx($this->image));
		$this->setHeight(imagesy($this->image));
	}

	/**
	 * Fonction retournant l'identifiant d'une image en fonction de son type : png, gif, jpg
	 *
	 * @param $file : chemin vers l'image JPG, PNG, GIF
	 * @return identifiant de l'image (library GD)
	 * @author Antoine GAVEL
	 */
	public function createImage($file)
	{
		// Détermination du type d'image
		switch(exif_imagetype($file))
		{
			case IMAGETYPE_GIF:
				return imagecreatefromgif($file);
				break;

			case IMAGETYPE_JPEG:
				return imagecreatefromjpeg($file);
				break;

			case IMAGETYPE_PNG:
				return imagecreatefrompng($file);
				break;

			default:
				throw new Exception('Incorrect image format, use PNG, JPG or GIF');
				break;
		}
	}

	/**
	 * Redimmensionnement d'une image
	 *
	 * @param $height int
	 * @param $width int
	 * @param $transparency bool : permet de rester fidèle aux dimensions demandées
	 * @return void
	 * @author Antoine GAVEL, Florent THEVENET
	 */
	public function resize($height, $width, $transparency = false)
	{
		if (!is_int($height) || !is_int($width))
			throw new Exception('Height and width must be an integer !');

		// Calcul du ratio
		$ratio = $this->width / $this->height;

		if ($ratio <= 0)
			throw new Exception('An error occured, please check your parameters (width : '.$width.' and height : '.$height.') to resize an image.');

		// Calcul de la nouvelle taille
		if ($width / $height > $ratio)
		{
   			$nWidth = (int) $height * $ratio;
   			$nHeight = $height;
   		}
		else
		{
   			$nHeight = (int) $width / $ratio;
   			$nWidth = $width;
   		}

   		// Calcul de l'espace à combler
		$posH = ($nHeight < $height && $transparency) ? (int) ($height - $nHeight) / 2  : 0;
		$posW = ($nWidth < $width && $transparency) ? (int) ($width - $nWidth) / 2 : 0;

   		// Redimensionnement
   		$targetImage = imagecreatetruecolor($width, $height);
   		imagealphablending( $targetImage, false );
        $color = imagecolorallocatealpha($targetImage, 255, 0, 255, 127);
        imagefill($targetImage, 0, 0, $color);
        imagesavealpha($targetImage, true );
   		imagecopyresampled($targetImage, $this->image, $posW, $posH, 0, 0, $nWidth, $nHeight, $this->width, $this->height);
   		$this->image = $targetImage;		

		// Image redimensionnée, on set les nouvelles dimensions
		$this->setWidth($width);
		$this->setHeight($height);
	}

	/**
	 * Taguer l'image avec le logo
	 *
	 * /!\ Ne fonctionne pas avec des images transparentes
	 *
	 * @param $x : LEFT, RIGHT, MIDDLE (cste)
	 * @param $y : TOP, BOTTOM, MIDDLE (cste)
	 * @param $size : pourcentage, taille du logo par rapport à l'image ou le logo doit être placé
	 * @param $transparency : transparence du logo -> 0: transparent, 100: non-transparent
	 * @return void
	 * @author Antoine GAVEL
	 */
	public function tag($x, $y, $size = 30, $transparency = 90)
	{
		if (!is_int($size) || !is_int($transparency))
			throw new Exception('Size and transparency must be an integer !');

		// Chargement du logo
		$logo = new Image(self::getLogo());

		// Calcul de la taille du logo
		$height = ($this->height * $size) / 100;
		$width = ($this->width * $size) / 100;

		$logo->resize((int)$height, (int)$width);

		// Détermination de l'emplacement du tag
		switch($x)
		{
			case self::LEFT:
				$posX = 0;
				break;

			case self::RIGHT:
				$posX = $this->width - $logo->width();
				break;

			case self::MIDDLE:
				$posX = ($this->width / 2) - ($logo->width() / 2);
				break;

			default:
				throw new Exception('Invalid argument x, function tag. Use LEFT, RIGHT OR MIDDLE');
				break;
		}

		switch($y)
		{
			case self::TOP:
				$posY = 0;
				break;

			case self::BOTTOM:
				$posY = $this->height - $logo->height();
				break;

			case self::MIDDLE:
				$posY = ($this->height / 2) - ($logo->height() / 2);
				break;

			default:
				throw new Exception('Invalid argument y, function tag. Use TOP, BOTTOM OR MIDDLE');
				break;
		}

		imagecopymerge($this->image, $logo->getImage(), $posX, $posY, 0, 0, $logo->width(), $logo->height(), $transparency);
	}

	/**
	 * Enregistrement de l'image en PNG
	 *
	 * @param $directory string : répertoire d'enregistrement
	 * @param $name string : nom de l'image
	 * @return void
	 * @author Antoine GAVEL
	 */
	public function saveToPng($directory, $name)
	{
		imagepng($this->image, $directory.$name.'.png');
		imagedestroy($this->image);
	}

	/**
	 * Enregistrement de l'image en JPG
	 *
	 * @param $directory string : répertoire d'enregistrement
	 * @param $name string : nom de l'image
	 * @return void
	 * @author Antoine GAVEL
	 */
	public function saveToJpg($directory, $name)
	{
		imagejpeg($this->image, $directory.$name.'.jpg');
		imagedestroy($this->image);
	}

	/**
	 * Enregistrement de l'image en GIF
	 *
	 * @param $directory string : répertoire d'enregistrement
	 * @param $name string : nom de l'image
	 * @return void
	 * @author Antoine GAVEL
	 */
	public function saveToGif($directory, $name)
	{
		imagegif($this->image, $directory.$name.'.gif');
		imagedestroy($this->image);
	}

	/**
	 * Renvoie l'image
	 *
	 * @return instance de l'image en cours de modification
	 * @author Antoine GAVEL
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * Retourne le logo de l'asso
	 */
	public static function getLogo()
	{
		return __DIR__ .'/../../'. self::$logo;
	}
}