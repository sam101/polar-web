<?php

/**
 * PolarQuery Implementation
 *
 * @author Florent Thévenet
 */

require_once 'RawString.class.php';

define('QUERY_SELECT', 0);
define('QUERY_INSERT', 1);
define('QUERY_DELETE', 2);
define('QUERY_UPDATE', 3);

class WrongValuesNumber extends PolarException {};

/**
 * A SQL query builder.
 *
 * Suport basic SQL operations (SELECT, INSERT, UPDATE, DELETE)
 * Statement are prepared to avoid SQL injections,
 * You can use SQL functions (e.g. NOW()) in your parameters with RawString
 *
 * Support joins, order, group and limit
 *
 * Support 'intelligent' subqueries @see select()
 *
 * Automatically hydrate and return an object of the right type with execute() in SELECT Mode
 */
class PolarQuery implements IteratorAggregate {
    private $db;
    protected $type;
    protected $class;
    public $wheres = array();
    public $selects = array(); // alias.nom => as ...
    public $joins = array(); // 
    public $fields = array();
    public $values = array();
    public $orders = array();
    public $groups = array();
    public $inputs = array();
    public $limit = NULL;
    public $offset = NULL;
    public $result = NULL; // Utilisé pour itérer sur les résulats sans avoir à appeler execute
    public $parent = NULL; // pour les sous requêtes
    public $id; // ID utilisé pour construire le nom des paramètres de façon unique
    public $subqueries = array(); // On stocke les sous requêtes pour pouvoir récupérer leurs inputs

    private $tables = array();

    /**
     * Create a PolarQuery.
     * @param $db PolarDB object to access the db
     * @param $type Query type in (QUERY_SELECT, QUERY_INSERT, QUERY_UPDATE, QUERY_DELETE)
     * @param $class Main class which will be queried, an hydrated object of this class will
     * be returned by execute() if in SELECT Mode
     */
    public function __construct($db, $type, $class) {
        $this->db = $db;

        if (!in_array($type, array(QUERY_SELECT, QUERY_INSERT, QUERY_DELETE, QUERY_UPDATE)))
            throw new InvalidTruc();

        $this->type = $type;

        $this->class = $class;
        $alias = $this->addClass($class);
        $this->id = $this->db->makeId($alias);
        // Nos paramètres seront de la forme :{$id}{db supplied int}_{position}
        // @see addInput
    }

    /**
     * Add a class to the list of needed tables and return its alias
     */
    public function addClass($class) {
        $alias = make_alias($class::$table);
        $i = '';
        while (isset($this->table[$alias.$i]) && $this->table[$alias . $i] != $class)
            $i++;
        $this->tables[$alias . $i] = $class;
        return $alias . $i;
    }

    public function getType() {
        return $this->type;
    }

    public function getClass() {
        return $this->class;
    }

    /**
     * Mark a PolarQuery as a subquery of this one
     * You can (and must) then use it, either by passing it to select
     * or by using the generated SQL (@see get_sql) somewhere in the query
     * @param $req the PolarQuery
     * @return current PolarQuery instance
     */
    public function addSubquery($req) {
        if ($req instanceof PolarQuery && !in_array($req, $this->subqueries)) {
            $this->subqueries[] = $req;
            $req->parent = $this;
        }
        return $this;
    }

    /**
     * Add a field to SELECT.
     *
     * The string $req will not be checked for validity so you can do
     * complex selects like CASE, Subqueries...
     *
     * If you pass a PolarQuery object and an alias, the subquery sql will be
     * automatically included in the query
     *
     * Can be called multiple times
     * @param $req string or PolarQuery
     * @param $alias string or NULL
     * @return current PolarQuery instance
     */
    public function select($req, $alias=NULL) {
        if ($this->type != QUERY_SELECT)
            throw new InvalidQueryType();

        if ($req instanceof PolarQuery) { //subqueries
            if ($alias == NULL)
                throw new Exception('Must give array for subquery');
            else
                $this->selects[] = array($req, $alias);
        } else { // normal field select
            if ($alias == NULL) {
                $this->selects[] = array($req, NULL);
            } else { 
                $this->selects[] = array($req, $alias);
            } 
        }

        return $this;
    }

    /**
     * Add a AND condition in the WHERE clause.
     *
     * You can use a ? to mark values that needs to be escaped and added in the condition.
     * There has to be as many ? as values provided.
     *
     * Can be called multiple times
     * @param $where the condition (ex 'ID=?', '(A=1 OR B=?)')
     * @param ... Values to be escaped and included in the query
     * @return current PolarQuery instance
     */
    public function where($where) {
        $values = func_get_args();
        array_shift($values);
        $pieces = explode('?', $where);
        if ((count($pieces) - 1) != count($values)) {
            throw new WrongValuesNumber(sprintf("expected %d but got %d", (count($pieces) - 1), count($values)));
        } else {
            $real_where = current($pieces);
            foreach ($values as $val) {
                $real_where .= $this->addInput($val) . next($pieces);
            }
            $this->wheres[] = $real_where;
        }
        return $this;
    }

    /**
     * Add an input parameter
     * These parameters will be passed when the prepared statement is executed
     * It avoids crappy string escaping and SQL injections
     * @param $value the value to store
     * @return the name of the parameter to use on the query
     */
    public function addInput($value) {
        if ($value instanceof RawString) // raw strings must not be passed as input parameters
            return $value->__toString();

        $id = ':' . $this->id . '_' . count($this->inputs);
        if ($value instanceof PolarObject)
            $this->inputs[$id] = $value->get_id();
        else
            $this->inputs[$id] = $value;
        return $id;
    }

    /**
     * Add a field to ORDER BY.
     *
     * Can be called multiple times
     * @param $ordre un ou plusieurs champs ('A', 'B DESC, C')
     * @return current PolarQuery instance
     */
    public function order($ordre) {
        if ($this->type != QUERY_SELECT)
            throw new InvalidQueryType();

        $this->orders[] = $ordre;

        return $this;
    }

    /**
     * Add a field to GROUP BY.
     *
     * Can be called multiple times
     * @param $group un ou plusieurs champs ('A', 'B, C')
     * @return current PolarQuery instance
     */
    public function groupBy($group) {
        if ($this->type != QUERY_SELECT)
            throw new InvalidQueryType();

        $this->groups[] = $group;

        return $this;
    }

    /**
     * Define the value of a field for INSERT and UPDATE.
     *
     * Can be called multiple times
     * @param $field field name
     * @param $value correctly escaped value ("'Aujourd\'hui'", "")
     * @return current PolarQuery instance
     */
    public function set_value($field, $value) {
        $this->fields[] = $field;
        $this->values[] = $this->addInput($value);
        return $this;
    }

    /**
     * Define a JOIN.
     *
     * Can be called multiple times
     * @param $class Name of the class to join
     * @param $condition string : join condition (ON ...)
     * @return current PolarQuery instance
     */
    public function join($class, $condition) { // + objets à sélectionner
        $alias = $this->addClass($class);
        $this->joins[] = array($class, $alias, $condition);
        return $this;
    }

    /**
     * Define a LEFT JOIN.
     *
     * Can be called multiple times
     * @param $class Name of the class to join
     * @param $condition string : join condition (ON ...)
     * @return current PolarQuery instance
     */
    public function leftJoin($class, $condition) { // + objets à sélectionner
        $alias = $this->addClass($class);
        $this->joins[] = array($class, $alias, $condition, ' LEFT');
        return $this;
    }

    /**
     * Define LIMIT and OFFSET.
     *
     * if called multiple times the new values replace the older ones
     * @param $class integer maximum number of rows to retrieve
     * @param $offset integer offset or NULL for no offset
     * @return current PolarQuery instance
     */
    public function limit($limit, $offset=NULL) {
        $this->limit = $limit;
        $this->offset = $offset;
        return $this;
    }

    /**
     * Build and return the sql query string.
     * @return A string, containing the sql query, no validity check is done
     */
    public function get_sql() {
        $sql = '';
        switch($this->type) {
        case QUERY_SELECT:
            $sql = $this->get_sql_select();
            break;
        case QUERY_UPDATE:
            $sql = $this->get_sql_update();
            break;
        case QUERY_DELETE:
            $sql = $this->get_sql_delete();
            break;
        case QUERY_INSERT:
            $sql = $this->get_sql_insert();
            break;
        }
        return $this->replace_classname_by_alias($sql);
    }

    // Select
    protected function get_sql_select() {
        $req = 'SELECT %selects% FROM %main_table% %joins% WHERE %wheres% %group% %order% %limit%';
        $class = reset($this->tables);
        return str_replace(array('%selects%',
                                 '%main_table%',
                                 '%joins%',
                                 '%wheres%',
                                 '%group%',
                                 '%order%',
                                 '%limit%'),
                           array($this->format_selects(),
                                 $class::$table . ' ' . key($this->tables),
                                 $this->format_joins(),
                                 $this->format_wheres(),
                                 $this->format_groups(),
                                 $this->format_order(),
                                 $this->format_limit()),
                           $req);
    }

    public function format_selects() {
        reset($this->tables);
        if (empty($this->selects))
            return key($this->tables).'.*';

        $selects = '';
        foreach($this->selects as $select) {
            if ($select[0] instanceof PolarQuery) {
                $field = '('.$select[0]->get_sql().')';
                $this->addSubquery($select[0]);
            } else {
                $field = $select[0];
            }
            if ($select[1] == NULL) {
                $selects .= $field . ', ';
            } else {
                $selects .= $field . ' AS ' . $select[1] . ', ';
            }
        }
        return substr($selects, 0, -2);
    }

    public function format_wheres() {
        if (empty($this->wheres))
            return '1';

        $wheres = '';
        foreach($this->wheres as $where) {
            $wheres .= $where . ' AND ';
        }
        return substr($wheres, 0, -5);
    }

    public function format_order() {
        if (empty($this->orders))
            return '';
        else
            return ' ORDER BY ' . implode(',', $this->orders);
    }

    public function format_groups() {
        if (empty($this->groups))
            return '';
        else
            return ' GROUP BY ' . implode(',', $this->groups);
    }

    public function format_limit() {
        if ($this->limit == NULL) {
            return '';
        } else {
            if ($this->offset == NULL) {
                return ' LIMIT '.$this->limit;
            } else {
                return ' LIMIT '.$this->offset.','.$this->limit;
            }
        }
    }

    protected function replace_classname_by_alias($string) {
        $search = array();
        $replace = array();
        $what = array();
        $order = array();
        foreach ($this->tables as $alias => $class) {
            $search[] = '/([^\w])' . $class . '\./';
            $replace[] = '$1' . $alias . '.';
        }
        return preg_replace($search, $replace, $string);
    }

    public function format_joins() {
        $joins = '';

        foreach($this->joins as $join) {
            $class = $join[0];
            if (isset($join[3]))
                $joins .= $join[3];
            $joins .= ' JOIN '.$class::$table.' '.$join[1].' ON '.$join[2];
        }
        return $joins;
    }

    // Update
    protected function get_sql_update() {
        $req = 'UPDATE %table% SET %sets% WHERE %wheres%';
        $class = reset($this->tables);
        return str_replace(array('%table%',
                                 '%sets%',
                                 '%wheres%'),
                           array($class::$table .' '. key($this->tables),
                                 $this->format_sets(),
                                 $this->format_wheres()),
                           $req);
    }

    public function format_sets() {
        if (count($this->fields) != count($this->values))
            throw new WrongValuesNumber('Use set_value !!!');

        $sets = '';
        foreach($this->fields as $index => $field) {
            $sets .= '`'.$field . '`='.$this->values[$index].', ';
        }
        return substr($sets, 0, -2);
    }

    // Delete
    protected function get_sql_delete() {
        $req = 'DELETE FROM %table% WHERE %wheres%';
        $class = reset($this->tables);
        return str_replace(array('%table%',
                                 '%wheres%'),
                           array($class::$table,
                                 $this->format_wheres()),
                           $req);
    }

    // Insert
    protected function get_sql_insert() {
        $req = 'INSERT INTO %table% (%fields%) VALUES(%values%)';
        $class = reset($this->tables);
        return str_replace(array('%table%',
                                 '%fields%',
                                 '%values%'),
                           array($class::$table,
                                 implode($this->fields, ','),
                                 implode($this->values, ',')),
                           $req);
    }

    /**
     * Execute la requête crée.
     *
     * En mode SELECT tente d'hydrater des objets de type $class
     *
     * @return un objet PolarObject correctement hydraté
     * ou un PolarObjectsArray contenant tous les objets
     * retournés par la requête ou un objet PDOStatement
     * lors de l'échec de l'hydratation ou en mode non SELECT
     */
    public function execute($group=false) {
        $req = $this->rawExecute();

        if ($this->type != QUERY_SELECT)
            return $req;

        $objects = new PolarObjectsArray($this->class);
        foreach ($req as $ligne) {
            $obj = new $this->class(false);
            $obj->hydrate($ligne);
            $objects[] = $obj;
        }

        $req->closeCursor();

        if (count($objects) == 1 && !$group)
            return $objects[0];
        else
            return $objects;
    }

    /**
     * Fetch the input parameters of the subqueries
     * and return them with the current query parameters
     * @return array : parameter_name => value
     */
    public function getInputs() {
        $inputs = $this->inputs;
        foreach($this->subqueries as $subq)
            $inputs = array_merge($inputs, $subq->getInputs());
        return $inputs;
    }

    /**
     * Execute la requête crée.
     * @return l'objet PDOStatement résulant de la requête
     */
    public function rawExecute() {
        try {
            $req = $this->db->prepare($this->get_sql());

            $req->execute($this->getInputs());
        } catch (PDOException $e) {
            Log::warning("Erreur lors d'une requete sql", array('exception' => $e));
            throw $e;
        }
        $req->setFetchMode(PDO::FETCH_ASSOC);
        return $req;
    }

    /**
     * Retourne la première ligne du résulat ou NULL si vide
     */
    public function getOne() {
        $this->limit(1);
        $res = $this->execute(true);
        if (count($res) > 0)
            return $res[0];
        else
            return NULL;
    }

    /* Ci dessous la fonction pour itérer sur les résultats */
    public function getIterator() {
        return $this->execute(true);
    }
}

/**
 * Create a short alias from a table name
 * polar_caisse_ventes will result in pcv for example
 */
function make_alias($table_name) {
    $alias = '';
    foreach (explode('_', $table_name) as $mot)
        $alias .= $mot[0];
    return $alias;
}