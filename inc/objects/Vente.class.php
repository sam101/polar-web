<?php

require_once 'PolarObject.class.php';

class MoyenInvalide extends PolarException { }

class Vente extends PolarObject {
    public static $table = 'polar_caisse_ventes';
    protected static $attrs = array(
        'IDVente' => T_INT,
        'Article' => 'Article',
        'Date' => T_STR,
        'Tarif' => array('asso', 'normal'),
        'Finalise' => T_BOOL,
        'Asso' => 'CompteAsso',
        'Client' => T_STR,
        'Facture' => T_INT,
        'PrixFacture' => T_FLOAT,
        'MontantTVA' => T_FLOAT,
        'MoyenPaiement' => array('cb', 'moneo', 'cheque', 'especes', 'assos', 'payutc', 'interne', 'virement', 'utc', 'web'),
        'Quantite' => T_INT,
        'Permanencier' => 'Utilisateur');
    protected static $nulls = array(
        'Asso');

    public function __construct($code=false, $qte=1, $paiement='cb', $permanencier='2',
                                $tarif='normal', $asso=NULL, $client='',
                                $idvente=NULL) {
        if ($code == false)
            return;

        if ($idvente==NULL) $idvente = self::generate_idvente();

        if ($qte <= 0)
            throw new InvalidValue('Quantite <= 0');

        $article = Article::getActifByCode(intval($code));
        if (is_null($article))
            throw new InvalidValue('Article '.intval($code).' introuvable');

        $attrs = array('IDVente' => $idvente,
                       'Article' => $article,
                       'Date' => raw('NOW()'),
                       'Finalise' => $asso ? 0 : 1,
                       'Asso' => $asso,
                       'Client' => $client,
                       'Facture' => 0,
                       'Tarif' => $tarif,
                       'MoyenPaiement' => $paiement,
                       'Quantite' => $qte,
                       'Permanencier' => $permanencier);
        parent::__construct($attrs);
    }

    public function __set($attr, $value) {
        if ($attr === 'Quantite' and intval($value) != 0) {
            $this->values[$attr] = intval($value);
            $prix = $this->calcule_prix();
            $this->PrixFacture = $prix[0];
            $this->MontantTVA = $prix[1];
        } else {
            parent::__set($attr, $value);
        }
    }

    private function calcule_prix() {
        // On gère la remise sur quantité
        // et les tarifs asso
        $a = $this->Article;
        $q = $this->Quantite;
        if ($this->Tarif === 'normal') {
            $prix = $q * $a->PrixVente;
            if ($q >= $a->Palier1) {
                if ($a->Palier2 != NULL and $a->Palier2 > 0 and $q >= $a->Palier2)
                    $prix *= (1-$a->Remise2);
                else
                    $prix *= (1-$a->Remise1);
            }
        }
        else {
            if ($a->PrixVenteAsso > 0)
                $prix = $q * $a->PrixVenteAsso;
            else
                $prix = $q * $a->PrixVente;
        }
        // Attention on travaille en prix TTC !
        // Montant TVA = Prix HT * Taux TVA
        // or Prix TTC = Prix HT + Montant TVA
        // Donc Montant TVA = (Prix TTC * Taux TVA) / (1 + Taux TVA)
        $tva = ((float) $prix * (float) $a->TVA) / (1 + (float) $a->TVA);
        return array($prix, $tva);
    }

    public function creerSimilaire($code, $qte) {
        return new Vente($code, $qte, $this->MoyenPaiement,
                         $this->Permanencier, $this->Tarif, $this->Asso,
                         $this->Client, $this->IDVente);
    }

    function generate_idvente() {
        if ($this::$db == NULL)
            throw new PolarException("PolarObject::$db must be set to use generate_idvente");

        $result = $this::$db->query("SELECT GREATEST(pcvg.MaxIDVente, pcv.MaxIDVente) + 1 AS MaxIDVente FROM
(SELECT COALESCE(MAX(IDVente), 0) AS MaxIDVente FROM polar_caisse_ventes_global) pcvg,
(SELECT COALESCE(MAX(IDVente), 0) AS MaxIDVente FROM polar_caisse_ventes) pcv");
        return $result->fetchColumn();
    }

    /**
     * Renvoie un array [obj_id, qte, reduction] pour payutc
     */
    public function getPayutcArray() {
        if ($this->MoyenPaiement != 'payutc')
            throw new MoyenInvalide("Impossible de générer un objet payutc pour une vente non payutc !");

        $p = ArticlePayutc::getByArticle($this->Article);
        if (is_null($p)) {
            Log::warning("Article non synchronisé pour payutc, id : ".$this->Article);
        }
        if ($this->Article->Palier2 > 0 && $this->Quantite >= $this->Article->Palier2) {
            $remise = $this->Article->Remise2;
        } else if ($this->Article->Palier1 > 0 && $this->Quantite >= $this->Article->Palier1) {
            $remise = $this->Article->Remise1;
        }
        else {
            $remise = null;
        }

        return array($p->IDPayutc, $this->Quantite, $remise);
    }

}

?>
