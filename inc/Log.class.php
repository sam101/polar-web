<?php

/* Interface de log
 * On fait des appels statiques sur la classe
 * Passe les appels au logger sous-jacent en incluant la page en cours dans le contexte
 */
class Log {
    private static $logger = NULL;

    public function __construct() { }

    public static function setLogger($logger) {
        self::$logger = $logger;
    }

    public static function log($level, $message, array $context=array()) {
        global $user;
        if (!is_null(self::$logger)) {
            $context['module'] = Request::$module;
            $context['section'] = Request::$section;
            if (isset($user) && $user instanceof UtilisateurSite && $user->is_logged()) {
                $context['user'] = $user->get_id();
            }
            $context['ip'] = get_ip();
            self::$logger->log($level, $message, $context);
        }
    }
    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function emergency($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function alert($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function critical($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function error($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function warning($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function notice($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function info($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public static function debug($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::DEBUG, $message, $context);
    }
}