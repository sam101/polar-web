<div id="menu">
            <table id="liens-menu">
              <tr>
                <td><a <?php if($module == 'news' && $section == 'index') echo 'id="lien-actif" '; ?>href="<?php echo $racine; ?>" title="Toutes les news du polar sont à l'accueil !">ACCUEIL</a></td>
                <td><a <?php if($module == 'static' && $section == 'presentation') echo 'id="lien-actif" '; ?>href="<?php echo $racine; ?>static/presentation" title="Le Polar... kézako ? La réponse à toutes vos questions sont dans cette rubrique !">LE POLAR ?</a></td>
				<td><a <?php if($module == 'membres' && $section == 'horaires') echo 'id="lien-actif" '; ?>href="<?php echo $racine; ?>membres/horaires" title="Horaires d'ouverture !">HORAIRES</a></td>
                <td><a <?php if($module == 'membres' && $section == 'liste') echo 'id="lien-actif" '; ?>href="<?php echo $racine; ?>membres/liste" title="Découvrez tous ceux qui se cachent derrière le Polar !">LE STAFF</a></td>
                <td><a <?php if($module == 'static' && $section == 'contributeurs') echo 'id="lien-actif" '; ?>href="<?php echo $racine; ?>static/contributeurs" title="Voir les personnes qui ont travaill&eacute; sur le site !">CONTRIBUTEURS</a></td>
				<td><a <?php if($module == 'hotline') echo 'id="lien-actif" '; ?>href="<?php echo $racine; ?>hotline" title="Contactez nous !">CONTACT</a></td>
              </tr>
            </table>
          </div>
