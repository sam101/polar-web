<?php
$estmembre = ($user->is_logged()) ? 1 : 0;
$isStaff = $estmembre ? ($user->Staff ? 1 : 0) : 0;
$pages = Page::select()->leftJoin('Droits', 'Droits.ID = Page.ID')
    ->where("((Page.Acces LIKE 'member' AND $estmembre = 1) OR (Page.Acces LIKE 'staff' AND $isStaff = 1) OR (Page.Acces LIKE 'private' AND Droits.User=?) OR (Page.Acces LIKE 'public'))", $user)
    ->where('Menu IS NOT NULL')
    ->order('Module, Menu')
    ->groupBy('Module, Menu ASC');
$module_en_cours = NULL;
?>
<div class="navbar">
  <div class="navbar-inner navbar-secondaire">
    <ul class="nav pull-right">
<?php
foreach ($pages as $page) {
    if ($module_en_cours != $page->Module) {
        if($module_en_cours != NULL)
            echo "</ul>\n</li>";
        echo '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'.ucfirst($page->Module).' <b class="caret"></b></a>';
        echo "<ul class=\"dropdown-menu\">";
    }
    $module_en_cours = $page->Module;
    echo '<li><a href="'.urlTo($page->Module, $page->Section).'" title="'.$page->Titre.'">'.$page->Menu.'</a></li>';    
}
    ?>
    </ul>
  </div>
</div>