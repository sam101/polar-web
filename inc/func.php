<?php

require_once('passwordHash.php');

require_once "lib/ginger-client/Ginger.class.php";
$ginger = new Ginger($CONF['ginger_key'], $CONF['ginger_url']);

/**
 * Une exception qui peut être affichée à l'utilisateur sans risque
 */
class PolarUserError extends Exception { }

/**
 * Effectuer une requête SQL en comptant le nombre de requêtes
 *
 * @param string $sql Requête SQL
 * @return mysql_result
 */
function query($sql) {
    $req = mysql_query($sql);

    // on log et on tue le site si une requête à échoué
    if ($req === false) {
        Log::warning("Erreur de requête sql", array('erreur' => mysql_error()));
        if(defined('DEBUG') && DEBUG) {
            die('Erreur SQL : <pre>'.mysql_error().'</pre>');
        }
        else {
            die("<b>Une erreur fatale s'est produite, veuillez nous excuser du dérangement</b>");
        }
    }

	// On incrémente le compteur
    Request::$nb_requetes++;

	// On retourne le résultat de la requête
	return $req;
}

/**
 * Ajouter une erreur à la session
 *
 * @param string $texte Texte de l'erreur, sans majuscule
 * @return void
 * @author Arthur Puyou
 */
function ajouterErreur($texte){
	if(empty($_SESSION['ep-erreur']))
		$_SESSION['ep-erreur'] = $texte;
	else
		$_SESSION['ep-erreur'] .= "\n".$texte;
}

/**
 * Afficher les erreurs sauvées dans la session avec mise en page
 *
 * @return void
 * @author Arthur Puyou
 */
function afficherErreurs(){
	if(isset($_SESSION['ep-erreur'])) {
		echo '<p class="alert alert-error"><strong>Erreur !</strong><br/>',nl2br($_SESSION['ep-erreur']),'</p>';
		unset($_SESSION['ep-erreur']);
	}
}

/**
 * Renvoie true si des erreurs ont été enregistrées
 *
 * @return bool
 * @author Arthur Puyou
 */
function hasErreurs(){
	return isset($_SESSION['ep-erreur']);
}

/**
 * Enregistre les éléments fournis en paramètre pour un réaffichage ultérieur
 *
 * @param string $post $_POST à sauvegarder
 * @return void
 * @author Arthur Puyou
 */
function saveFormData($post){
	$_SESSION['sentFormData'] = $post;
}

/**
 * Renvoie la valeur déjà saisie dans un champ de formulaire
 *
 * @param string $name Nom du champ
 * @return void
 * @author Arthur Puyou
 */
function getFormData($name){
	if(isset($_SESSION['sentFormData'][$name])){
		$data = $_SESSION['sentFormData'][$name];
		unset($_SESSION['sentFormData'][$name]);
		return $data;
	}
	return false;
}

/**
 * Sécuriser du texte pour une insertion SQL
 *
 * @param string $text Texte à sécuriser
 * @return string Texte sécurisé
 * @author Arthur Puyou
 */
function mysqlSecureText($text){
	return htmlentities(mysql_real_escape_string(stripslashes(html_entity_decode($text, ENT_COMPAT, 'UTF-8'))), ENT_COMPAT, 'UTF-8');
}

function shtml($text) {
    return htmlspecialchars($text);
}

function entities($text) {
	return htmlentities(html_entity_decode($text, ENT_COMPAT, 'UTF-8'), ENT_COMPAT, 'UTF-8');
}

/**
 * Décode du texte avec des entités HTML dans l'encodage courant
 *
 * @param string $text Texte avec entités
 * @return string $text Texte en UTF8
 * @author Arthur Puyou
 */
function unhtmlentities($text){
	return html_entity_decode($text, ENT_COMPAT, 'UTF-8');
}

/**
 * Affiche un montant à la française
 *
 * @param float $prix
 * @return string Le nombre formaté
 * @author Arthur Puyou
 */
function formatPrix($prix){
	return number_format($prix, 2, ',', ' ');
}

/**
 * Retire les accents d'une chaîne de caractères
 *
 * @param string $string
 * @return string
 */
function stripAccents($string){
	return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
		'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

/**
 * Retirer des articles du stock
 *
 * @param string $idvente Vente comprenant les articles à retirer
 * @return void
 * @author Arthur Puyou
 */
function MAJStock($idvente){
	$req = query("SELECT Article, Nom, Quantite, Stock, SeuilAlerte, Secteur FROM polar_caisse_ventes
		LEFT JOIN polar_caisse_articles ON polar_caisse_articles.ID = polar_caisse_ventes.Article
		WHERE polar_caisse_ventes.IDVente = $idvente AND Stock IS NOT NULL");
	while($data = mysql_fetch_assoc($req)){
		$article = $data['Article'];
		$nb = $data['Quantite'];
		query("UPDATE polar_caisse_articles SET Stock = Stock-$nb WHERE ID = $article");

		// Si l'article vient de passer en rupture, on mail
		if($data['SeuilAlerte'] !== NULL && $data['Stock'] - $nb <= $data['SeuilAlerte'] && $data['SeuilAlerte'] < $data['Stock']){
			$secteur = $data['Secteur'];
			$produit = unhtmlentities($data['Nom']);
			$seuil = $data['SeuilAlerte'];
			$req = query("SELECT Email FROM polar_utilisateurs
				WHERE Responsable = $secteur");
			$emails = array("polar@assos.utc.fr");
			while($resp = mysql_fetch_assoc($req))
				$emails[] = $resp['Email'];

			sendMail('polar@assos.utc.fr', 'Le Polar', $emails, "[Le Polar] Alerte de stock", "Bonjour,<br /><br />Le produit $produit vient de passer en dessous de son seuil d'alerte de $seuil unités ! Pensez à réapprovisionner !<br /><br />Cordialement,<br />Le super logiciel de caisse");
		}
	}
}

/**
 * Rajouter des articles au stock
 *
 * @param string $idvente Vente comprenant les articles à rajouter
 * @return void
 * @author Arthur Puyou
 */
function MAJStockSupprimer($idvente){
	//Cette fonction met à jour le stock pour la vente passée en argument
	$req = query("SELECT Article, Quantite FROM polar_caisse_ventes
		LEFT JOIN polar_caisse_articles ON polar_caisse_articles.ID = polar_caisse_ventes.Article
		WHERE polar_caisse_ventes.IDVente = $idvente AND Stock IS NOT NULL");
	while($data = mysql_fetch_assoc($req)){
		//On met à jour le stock
		$article = $data['Article'];
		$nb = $data['Quantite'];
		query("UPDATE polar_caisse_articles SET Stock = Stock+$nb WHERE ID = $article");
	}
}

/**
 * Renvoie true si le navigateur actuel à le droit d'accèder à la caisse
 */
function CaisseAutorisee() {
    return isset($_COOKIE['private_browser_token']) &&
        AutorisationCaisse::isValide($_COOKIE['private_browser_token']);
}

/**
 * Vérifie si un login UTC est valide ou non
 *
 * @param string $login
 * @return bool
 * @author Arthur Puyou, Florent Thévenet
 */
function verifLoginUTC($login){
  global $ginger;

  try {
    $user = $ginger->getUser($login);
    return $user->type === 'etu' OR $user->type == 'escom';
  } catch(ApiException $e) {
    return False;
  }
}

/**
 * Vérifie si un étudiant est cotisant au BDE
 *
 * @param string $login Login de l'étudiant
 * @return bool
 * @author Arthur Puyou, Florent Thévenet
 */
function isCotisant($login){
  global $ginger;

  $user = $ginger->getUser($login);
  if (!isset($user->error)) {
    return $user->is_cotisant;
  } else {
    return False;
  }
}

/**
 * Renvoie le poste d'un membre du Polar
 *
 * @param int $user Id de l'utilisateur sur le site du Polar
 * @return string Le poste lisible en français
 * @author Arthur Puyou
 */
function getPoste($user){
	$req = query('SELECT Staff, Poste FROM polar_utilisateurs WHERE ID='.intval($user));
	$data = mysql_fetch_array($req);
	empty($data['Poste']) ? (intval($data['Staff'])==1 ? $poste='Permanencier' : $poste='Aucun') : $poste=$data['Poste'];
	return $poste;
}

/**
 * Renvoie les premiers codes d'article disponibles dans
 * chaque secteur.
 *
 * @return array
 * @author Geoffrey Thiesset
 */
function getPremierDisponible(){
	$result = Array();
	for($i=0;$i<10;$i++){
		$result[$i] = $i*100;
		$req = query('SELECT CodeCaisse FROM polar_caisse_articles WHERE CodeCaisse>='.intval(100*$i).' AND CodeCaisse<'.intval(100*($i+1)).'');
		$data = mysql_fetch_assoc($req);
		$valeur = $data['CodeCaisse'];
		while($data=mysql_fetch_assoc($req)) {
			if($data['CodeCaisse']-$valeur>1) {
				$result[$i]=$valeur+1;
				break;
				}
			$valeur = $data['CodeCaisse'];
		}
	}
	return $result;
}

/**
 * Vérifie qu'une adresse mail est valide et du domaine utc/escom
 *
 * @param string $email
 * @return bool
 * @author Arthur Puyou
 */
function verifMailUTC($email) {
	$banned = array("ent.", "edu.");
	$r = preg_match('#^[a-z0-9._%+-]+@([a-z0-9.-]*\.)?(utc|escom)\.fr$#i', $email, $match);
	if(!empty($match[1]))
		return ($r && !in_array($match[1], $banned));

	return $r;
}

/**
 * Vérifie qu'une adresse mail est valide
 *
 * @param string $email
 * @return bool
 * @author Arthur Puyou
 */
function verifMail($email) {
	return preg_match('#^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$#i', $email);
}

/**
 * Transforme les <br> en \r\n (inverse de nl2br)
 *
 * @param string $string
 * @return string
 * @author Arthur Puyou
 */
function br2nl($string){
	// On vire les retours à la ligne existants
	$string = str_replace("\r", '', $string);
	$string = str_replace("\n", '', $string);
	// On transforme les br en retours à la ligne
	$string = str_replace('<br >',"\r\n", $string);
	$string = str_replace('<br />',"\r\n", $string);
	return str_replace('<br/>',"\r\n", $string);
}

/**
 *
 * Envoyer un e-mail, avec éventuellement des PJ.
 *
 * @param string $from Adresse de l'expéditeur
 * @param string $from_nom Nom de l'expédieteur
 * @param array $to Destinataires
 * @param string $sujet Sujet du message
 * @param string $html Contenu du message en html
 * @param array $pj Pièces-jointes (facultatif)
 * @return bool
 * @author Geoffrey Thiesset & Arthur Puyou
 */
function sendMail($from, $from_nom, $to, $sujet, $html, $pj = array()){
	require_once('Rmail.php');

	// Tests sur les adresses
	foreach($to as $courriel) {
		if(!verifMail($courriel)) {
			return false;
		}
	}

	if(!verifMail($from))
		return false;

	// Conversion du texte en ISO-8859-1
	if(mb_detect_encoding($sujet) == 'UTF-8')
		$sujet = utf8_decode($sujet);
	if(mb_detect_encoding($html) == 'UTF-8')
		$html = utf8_decode($html);

	// Construction et envoi du mail
	$mail = new Rmail();
	$mail->setFrom($from_nom.' <'.$from.'>');
	$mail->setSubject($sujet);
	$mail->setText(stripslashes(br2nl($html)));
	$mail->setHTML($html);
	foreach($pj as $piece)
		$mail->addAttachment(new fileAttachment($piece));

	return $mail->send($to);
}

/**
 * Générer une chaîne alphanumérique
 *
 * @param string $car Nombres de caractères de la chaine
 * @return string La chaîne aléatoire
 * @author Geoffrey Thiesset
 */
function genererAleatoire($car) {
	$string = "";
	$chaine = "abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
	srand((double)microtime()*1000000);
	for($i=0; $i<$car; $i++)
		$string .= $chaine[rand()%strlen($chaine)];
	return $string;
}

/**
 * Ajoute un en-tête à la page qui va sortir.
 * À appeler avant require("header.php");
 *
 * @param string $headers Chaîne à ajouter
 * @return void
 * @author Arthur Puyou
 */
require_once("headers.php");
function addHeaders($headers){
	global $extraHeaders, $racine;
	if(empty($extraHeaders))
		$extraHeaders = $headers;
	else
		$extraHeaders .= $headers;
}

/**
 * Ajoute un script en pied de page
 * A appeler avant require('footer.php');
 *
 * @param string $script Chaîne à ajouter
 * @return void
 * @author Florent Thévenet
 */
function addFooter($footer){
	global $extraFooter, $racine;
	if(empty($extraFooter))
		$extraFooter = $footer;
	else
		$extraFooter .= $footer;
}

/**
 * Renvoie l'URL publique vers la photo d'un membre
 *
 * @param string $id ID sur le site du membre dont on veut la photo
 * @return string URL de la photo
 * @author Arthur Puyou
 */
function photoUrl($id = 0){
	global $racine;

	if($id == 0 && !empty($_SESSION['con-id']))
		$id = $_SESSION['con-id'];

	if(is_file('upload/profil/'.$id.'_mid.jpg'))
		return $racine.'upload/profil/'.$id.'_mid.jpg';
	else
 		return $racine.'styles/0/icones/inconnu.jpg';
}

/**
 * Renvoie un ID de Vente
 *
 * @return int IDVente
 */
function genererIDVente() {
    $result = query("SELECT GREATEST(pcvg.MaxIDVente, pcv.MaxIDVente) + 1 AS MaxIDVente FROM
(SELECT COALESCE(MAX(IDVente), 0) AS MaxIDVente FROM polar_caisse_ventes_global) pcvg,
(SELECT COALESCE(MAX(IDVente), 0) AS MaxIDVente FROM polar_caisse_ventes) pcv");
    $max = mysql_fetch_row($result);
    return $max[0];
}

function getSemestre($time = 0){
  if(empty($time))
    $time = time();

  if(date('m', $time) == 1)
  	return 'A'.(date('y', $time)-1);
  elseif(date('m', $time) >= 2 AND date('m', $time) <= 7)
  	return 'P'.date('y', $time);
  elseif(date('m', $time) >= 8)
  	return 'A'.date('y', $time);
  else
  	return 'WTF ?';
}

/**
 * Check if a string starts with a specific substring.
 *
 * @param string $haystack The string to search in.
 * @param string $needle The specific substring.
 * @param bool $case Use case-sensitive mode, default is true.
 * @return bool
 */
function startsWith($haystack,$needle,$case=true)
{
   if($case)
       return strpos($haystack, $needle, 0) === 0;

   return stripos($haystack, $needle, 0) === 0;
}

/**
 * Check if a string ends with a specific substring.
 *
 * @param string $haystack The string to search in.
 * @param string $needle The specific substring.
 * @param bool $case Use case-sensitive mode, default is true.
 * @return bool
 */
function endsWith($haystack,$needle,$case=true)
{
  $expectedPosition = strlen($haystack) - strlen($needle);

  if($case)
      return strrpos($haystack, $needle, 0) === $expectedPosition;

  return strripos($haystack, $needle, 0) === $expectedPosition;
}

/**
 * Makes directory if directory doesn't exist.
 *
 * @param type $pathname Directory path
 * @param type $mode mkdir mode
 * @param type $recursive Allows the creation of nested directories specified in the pathname. Default is true.
 * @return boolean true on success or the directory already exists, false on failure.
 */
function mkrndir($pathname, $mode = 0777, $recursive = true) {
    if (!is_dir($pathname)) {
        if (!mkdir($pathname, $mode, $recursive)) {
            return false;
        }
    }
    return true;
}

/**
 * Charge les informations de l'utilisateur depuis $_SESSION
 * et les sauvegarde dans $_SESSION
 *
 * @param Utilisateur : Utilisateut déjà chargé ou NULL
 * @return Utilisateur ou NULL
 * @author Florent Thévenet
 **/
function load_user_for_session($user) {
    if ($user == NULL) { // On cherche dans la session
        return UtilisateurSite::load_from_session();
    } else { // Déjà chargé, on le met dans la session
        $user->save_to_session();
        return $user;
    }
}

function checkMotDePasseBureau($mdp) {
    global $user;
    return $user->Bureau && $user->checkMotDePasse($mdp);
}

function checkMotDePasse($id, $mdp, $table, $cond = '') {
    $mdp = stripslashes($mdp);
    if (empty($mdp)) {
        return false;
    }
    $req = query("SELECT MotDePasse, MotDePasseSecurise FROM $table WHERE ID = $id $cond");
    if (mysql_num_rows($req) === 0) {
        return false;
    }
    $data = mysql_fetch_assoc($req);
    $hashMdp = $data['MotDePasse'];
    $mdpSecurise = $data['MotDePasseSecurise'] === '1';
    if ($mdpSecurise) {
        return validatePassword($mdp, $hashMdp);
    }

    if (md5($mdp) === $hashMdp) {
        createMotDePasseSecurise($id, $mdp, $table);
        return true;
    }
    return false;
}

function createMotDePasseSecurise($id, $mdp, $table) {
    $hash = createHash($mdp);
    query("UPDATE $table
        SET MotDePasse = '$hash',
            MotDePasseSecurise = 1
        WHERE ID = $id");
}

// --- méthodes de génération d'url

/**
 * Renvoie l'url vers une section, un fichier
 * ou vers la racine si aucun argument n'est donné
 *
 * Si $section est null, on considère $module comme le chemin
 * vers un fichier, et on retourne l'url pour y accèder
 * Sinon on retourne l'url pour accèder à la section
 * @see urlSection
 *
 * @return chemin vers la section ou la fichier
 * @author Florent Thévenet
 **/
function urlTo($module=NULL, $section=NULL, $args=NULL) {
    global $racine;
    if ($module == NULL && $section == NULL)
        return $racine;
    if ($section == NULL)
        return $racine . $module;
    return urlSection($args, false, $module, $section);
}

/**
 * Retourne l'url d'une ressources dans le dossier styles
 *
 * @param $file nom de la ressource
 * @return 
 * @author Florent Thévenet
 **/
function urlStyle($file) {
    return urlTo('styles/0/'.$file);
}


/**
 * 
 *
 * @param $args arguments GETS à ajouter à l'url sous forme de chaine de caractères ou array
 * @param $control Génère une url vers ${section}_control si true
 * @param $module_ Nom du module ou null pour le module actuel
 * @param $section Nom de la section ou null pour la section actuelle
 * @return l'url demandée
 * @author Florent Thévenet
 **/
function urlSection($args=NULL, $control=false, $module_=NULL, $section_=NULL) {
    global $racine, $module, $section;

    // Par défault on est sur le module courant
    if (!$module_)
        $module_ = Request::$module;
    if (!$section_)
        $section_ = Request::$section;
    $url = $racine . $module_ . '/' . $section_;

    // On veut la page de control ?
    if ($control)
        $url .= '_control';

    // Arguements sous forme de tableau ou de chaine
    if ($args) {
        $args_ = '';
        if (is_array($args)) {
            foreach($args as $key => $value)
                $args_ .= $key.'='.$value.'&';
            $args_ = substr($args, 0, -1);
        } else {
            $args_ = $args;
        }
        $url .= '?' . $args_;
    }

    return $url;
}

/**
 * Appelle urlSection avec $control = true
 *
 * @see urlSection
 * @author Florent Thévenet
 **/
function urlControl($args='', $module_=NULL, $section_=NULL) {
    return urlSection($args, true, $module_, $section_);
}

/**
 * Redirige l'utilisateur vers une section avec ou san erreur
 *
 * @param $error L'erreur à renvoyer ou null pour aucune erreur
 * @see urlSection pour les autres paramètres
 * @return 
 * @author Florent Thévenet
 **/
function redirectWithErrors($args='', $error = NULL, $module_=NULL, $section_=NULL) {
    if ($error)
        ajouterErreur($error);

    header('Location: '. urlSection($args, false, $module_, $section_));
    die();
}

/**
 * Redirige l'utilisateur vers une section
 * Il est possible de préciser un message qui sera affiché sous forme
 * d'une alert javascript
 *
 * @param $message message à afficher ou null pour pas de message, dans ce cas l'utilisateur sera directement redirigé vers la section demandée
 * @see urlSection pour les autres paramètres
 * @author Florent Thévenet
 **/
function redirectOk($args='', $message=NULL, $module_=NULL, $section_=NULL) {
    if ($message) {
        echo '<script>alert('.json_encode($message).');document.location.href="'.urlSection($args, false, $module_, $section_).'";</script>'; // json_encode permet de rendre la chaine safe pour le js
        die();
    } else {
        redirectWithErrors($args, NULL, $module_, $section_); // il n'y a pas d'erreur donc la fonction fait exactement ce qu'on veut
    }
}

?>
