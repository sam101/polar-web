<?php

require 'Log.class.php';

class OneLineFormatter extends \Monolog\Formatter\LineFormatter {
    public function format(array $record) {
        return str_replace("\n", " \\n ", substr(parent::format($record), 0, -1)) . "\n";
    }
}

// configuration de monolog
$logger = new Monolog\Logger('web');

// Handler fichier basique pour tous les logs
$fhandler = new \Monolog\Handler\RotatingFileHandler('logs/polar-web.log', 60);
$fhandler->setFormatter(new OneLineFormatter());
$fhandler->pushProcessor(new \Monolog\Processor\WebProcessor());
$logger->pushHandler($fhandler);

// Handler mail pour les erreurs à partir de critical
$mhandler = new  \Monolog\Handler\NativeMailerHandler('polar-devel@assos.utc.fr', '[Polar-web] Logging - Erreur critique', 'polar-devel@assos.utc.fr', \Monolog\Logger::CRITICAL);
$logger->pushHandler($mhandler);

Log::setLogger($logger);
