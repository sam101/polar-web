<?php
/**
 * Paramètres pour la sauvegarde.
 * Le fichier backup.dist.php doit être rempli et renommé
 * en backup.php
 *
 * @author Arthur Puyou
 */
// -- Paramètres pour la sauvegarde --
$ftphost = '';
$ftpuser = '';
$ftppass = '';
$ftpproxy = 'proxyftp.utc.fr:3128';
$clecryptage = ''; // Doit faire 256bits (32 caractères)
$dossier = 'Documents/Backups/'; // Avec / final s'il vous plaît
// -- Fin des paramètres --
?>
