<?php
require_once('lib/tcpdf/config/lang/fra.php');
require_once('lib/tcpdf/tcpdf.php');

class PolarPDF extends TCPDF {
	public function __construct($titre){
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$this->SetCreator('PolarSite');
		$this->SetAuthor('Polar UTC');
		$this->SetTitle($titre);

		// set default monospaced font
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// Pas d'en tête
		$this->setPrintHeader(false);

		//set margins
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// ---------------------------------------------------------
		// set font
		$this->SetFontSubsetting(true);
//		$this->SetFont('dejavusans', '', 8);
		$this->SetFontSize(8);

		// add a page
		$this->AddPage();

		// En-tête
    	$this->Image('upload/polar.png', 30, 10, 25, '', 'PNG', '', 'T', true, 150, '', false, false, 0, false, false, false);
		$adresse = "<b>Association LE POLAR</b><br>Université de Technologie de Compiègne<br>Rue Roger Couttolenc<br>60200 COMPIEGNE<br>Tél. 03 44 23 43 73<br>E-mail : polar@assos.utc.fr";
		$this->MultiCell(0, 5, $adresse, $border=0, $align='C', $fill=false, $ln=1, $x=PDF_MARGIN_LEFT, $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=0, $valign='T', $fitcell=false);
		$this->Image('upload/utc.png', 210-PDF_MARGIN_RIGHT-45, 15, 45, '', 'PNG', '', 'T', true, 150, '', false, false, 0, false, false, false);
		$this->Ln();
		$this->Line(10, $this->y, 200, $this->y);
		$this->MultiCell(0, 5, "<h1>$titre</h1>", $border=0, $align='C', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=true, $autopadding=true, $maxh=0, $valign='T', $fitcell=false);
		$this->Line(10, $this->y, 200, $this->y);
		$this->Ln();
		// set font
		$this->SetFontSize(10);
	}

	public function Signature($gauche, $droite=0){
		$this->Ln(30);
		if($gauche = "prez"){
			$sql = query("SELECT Valeur AS president FROM polar_parametres WHERE NOM like 'prez'");
			$prez = mysql_fetch_assoc($sql);
			$html = '<p>Pour le Polar,<br />'.$prez['president'].', Président<br /><img src="upload/signature_prez.jpg"  alt="Signature du président" width="45mm" border="0" /></p>';
		}
		else
			$html = '<p>'.$gauche.'</p>';
		$this->writeHTMLCell($w=60, $h=5, $x=$this->GetX(), $y=$this->GetY(), $html, $border=0, $ln=0, $fill=false, $reseth=true, $align='', $autopadding=true);

		if(!empty($droite)){
			$html = '<p>'.$droite.'</p>';
			$this->writeHTMLCell($w=0, $h=5, $x=$this->GetX(), $y=$this->GetY(), $html, $border=0, $ln=1, $fill=false, $reseth=true, $align='R', $autopadding=true);
		}
		else
			$this->Ln();
	}

	// Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-PDF_MARGIN_BOTTOM+5);
		$this->SetX(0);

        // Page number
        $this->Cell(0, 0, $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}
