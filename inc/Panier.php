<?php

/**
 * Classe pour contrôler le panier de la session en cours
 */
// à noter : tous les attributs et fonctions sont public pour les units tets
class Panier {
    public $db;
    public $commandes = null;
    public $panier = null;
    public $user = null;
    public $session = null;

    public function __construct($db, &$session) {
        $this->db = $db;
        $this->session = &$session;
    }

    /**
     * Est-ce qu'il y a un panier actif pour la session actuelle ?
     * @return true ou false
     */
    public function actif() {
        return isset($this->session['panier_id']) or isset($this->session['commande_id']);
    }

    /**
     * Supprime les variables de session relatives au panier
     * Et les commandes effectuées ? TODO
     * Le panier sera perdu à moins qu'il soit sauvegardé dans la base
     */
    public function nettoyer() {
        unset($this->session['panier_id']);
        unset($this->session['panier_commandes']);
        unset($this->session['commande_id']);
        unset($this->session['panier_user_id']);
        unset($this->session['panier_nom']);
        unset($this->session['panier_prenom']);
        unset($this->session['panier_mail']);
        unset($this->session['commande_supprimee']);
        unset($this->session['date_commande_supprimee']);
        $this->user = null;
        $this->commandes = null;
        $this->panier = null;
    }

    /**
     * Archive le panier
     * ou le supprime si aucun utilisateur n'est enregistré
     * puisqu'il ne pourra pas le récupérer de toute façon
     */
    public function archiver() {
        $panier = $this->getPanier();
        if ($panier == null) {
            if ($this->user->is_logged()) {
                if (isset($this->session['commande_id']))
                    // dans ce cas on va créer un panier
                    $this->creer();
            } else {
                // aucun utilisateur loggé, on nettoie et on se casse
                $this->nettoyer();
                return;
            }
        }

        $this->panier->Etat = 'saved';
        $this->panier->save();

        CommandesMailer::archiverPanier($this->panier);

        $this->nettoyer();
    }

    /**
     * Marque le panier comme validé (= à payer au polar)
     * puis envoie un mail résumé + les détails pour le paiement à l'utilisateur
     */
    public function payLater() {
        /* Si il n'y a qu'une seule commande on envoie juste un mail récapitualif
         * si il a plusieurs commande on passe le panier en saved
         * puis on crée une commande spéciale qui pourra être utilisée par la caisse pour insérer toutes les commandes du panier
         */
        if (isset($this->session['panier_id'])) {
            $type = TypeCommande::select()->where("Nom = 'Panier'")->getOne();
            $article = Article::select()->where("CodeCaisse = 1")->getOne();
            if ($type === null || $article == null) {
                Log::critical("Le type de commande 'Panier' et/ou l'article n'existe pas");
                throw new PolarUserError("Impossible de sauvegarder ce panier, merci de réessayer prochainement");
            }

            $this->db->beginTransaction();
            $commandes = $this->getCommandes();
            if (count($commandes) == 0) {
                Log::error("Tentative de sauvegarde d'un panier vide : ".$this->session['panier_id']);
                throw new PolarUserError("Impossible de sauvegarder ce panier, veuillez nous excuser pour ce dérangement");
            }
            $c1 = $commandes[0];

            // on récupère les infos de la première commande du panier
            // pour remplir la "commande-panier"
            $cgroup = new Commande();
            $cgroup->Type = $type;
            $cgroup->Mail = $c1->Mail;
            $cgroup->Nom = $c1->Nom;
            $cgroup->Prenom = $c1->Prenom;
            $cgroup->Asso = $c1->Asso;
            $cgroup->DateCommande = raw('NOW()');
            $cgroup->IPCommande = get_ip();
            $cgroup->Termine = 0;
            $cgroup->save();

            // on stocke juste l'id du panier dans le contenu de la commande
            $contenu = $cgroup->creerContenu();
            $contenu->Detail = (string) $this->getPanier();
            $contenu->Quantite = 1;
            $contenu->Article = $article;
            $contenu->PrixTTC = 1;
            $contenu->save();

            $this->panier->Etat = 'validated';
            $this->panier->CommandeGroupe = $cgroup;
            $this->panier->save();

            CommandesMailer::validerPanier($this->panier);
            $this->db->commit();

            return $cgroup->get_id();
        } else if (isset($this->session['commande_id'])) {
            CommandesMailer::validerCommande(Commande::getById($this->session['commande_id']));
            return $this->session['commande_id'];
        } else {
            return false;
        }
    }

    /**
     * Met le panier passé en argument comme panier en cours
     * seulement si il est en etat filling, saved ou failed
     * sinon ne fait rien
     */
    public function activer($panier) {
        if ($panier instanceof PanierWeb) {
            if ($panier->reactivable()){
                if ($this->getPanier() !== NULL) {
                    $this->panier->Etat = 'saved';
                    $this->panier->save();
                }

                $this->panier = $panier;
                $this->panier->Etat = 'filling';
                $this->panier->save();

                // on stocke en session l'id du panier
                // et l'id des commandes
                $this->session['panier_id'] = $panier->get_id();
                $this->session['panier_commandes'] = Commande::select('ID')
                    ->where('Panier = ?', $panier)
                    ->where('Termine = 0')
                    ->rawExecute()
                    ->fetchAll(PDO::FETCH_COLUMN | PDO::FETCH_UNIQUE, 0);
            } else {
                throw new PanierWebNonReactivable();
            }
        }
    }

    /**
     * Créé un panier si besoin :
     * si il y a une commande en cours, essaye de l'ajouter au panier
     * si cette commande n'existe en fait pas ne crée pas de panier
     * et return false
     * @return une instance de PanierWeb, déjà sauvegardé dans la base
     */
    public function creer() {
        // si l'utilisateur n'est pas connecté on insère null dans la base
        if ($this->user instanceof UtilisateurSite &&
            !$this->user->is_logged())
            $user = NULL;
        else
            $user = $this->user;

        // on regarde si il n'y a pas une commande solitaire en session
        // si oui on la récupère pour lui voler son adresse mail
        if (isset($this->session['commande_id'])) {
            try {
                $commande = Commande::getById($this->session['commande_id']);

                $mail = $commande->Mail;
                $this->session['panier_commandes'] = array($commande->get_id());

                unset($this->session['commande_id']);
            } catch (UnknownObject $e) {
                // par contre si l'id de la commande n'est pas bon
                // on retourne false pour que la fonction appelante puisse
                // gérer ça comme elle le veut
                Log::info("Transformation d'une commande en panier : commande initiale ".$this->session['commande_id']." n'existe pas dans la base");
                return false;
            }
        } else {
            // si on a pas de commande on met le mail à null
            // on le définira dès qu'un utilisateur se connecte
            // ou dès qu'on ajoute une commande
            $mail = null;
        }

        $this->panier = new PanierWeb(array('DateCreation' => raw('NOW()'),
                                            'Etat' => 'filling',
                                            'User' => $user,
                                            'Mail' => $mail));
        $this->panier->save();

        // si on avait une commande solitaire on change son id de panier
        if (isset($commande)) {
            $commande->Panier = $this->panier;
            $commande->save();
        }

        $this->session['panier_id'] = $this->panier->get_id();

        if (!isset($this->session['panier_commandes']))
            $this->session['panier_commandes'] = array();

        return $this->panier;
    }

    /**
     * Charge le panier à partir de l'id stocké en session
     */
    public function chargerPanier() {
        if (isset($this->session['panier_id'])) {
            try {
                $this->panier = PanierWeb::getById($this->session['panier_id']);
            } catch (UnknownObject $e) {
                Log::info('Tentative de charger un panier inexistant '.$this->session['panier_id']);
                return false;
            }
        } else
            return false;
    }

    public function getPanier() {
        if ($this->panier === null)
            $this->chargerPanier();
        return $this->panier;
    }

    /**
     * Retourne une instance de PanierWeb
     * même si il n'y a qu'une commande
     * si il n'y a pas de commande renvoie false
     */
    public function getPanierForWebsale() {
        if ($this->panier === null) {
            $res = $this->chargerPanier();

            if ($res === false) {
                if ($this->creer() === false) {
                    return false;
                }
            }
        }
        return $this->panier;
    }

    /**
     * Charge les commandes de ce panier
     * Panier::$commandes sera un PolarObjectsArray de Commande
     * Les instances de Commandes auront les champs additionnels suivants :
     * NomType : type
     */
    public function loadCommandes() {
        $query = PanierWeb::prepareQuery();

        if (isset($this->session['panier_commandes'])) {
            if (count($this->session['panier_commandes']) == 0) {
                return new PolarObjectsArray('Commande');
            }
            $query->where('Commande.ID IN ('.implode(', ', $this->session['panier_commandes']).')');
        } else if (isset($this->session['commande_id'])) {
            $query->where('Commande.ID = ?', $this->session['commande_id']);
        } else
            return;

        $this->commandes = $query->execute(true);
    }

    /**
     * Renvoie la liste des commandes dans le panier
     * @return PolarObjectsArray of Commande
     */
    public function getCommandes() {
        if ($this->commandes === null) {
            $this->loadCommandes();
        }
        return $this->commandes;
    }

    /**
     * Renvoie le nombre de commandes actives
     */
    public function getNbCommandes() {
        if (isset($this->session['panier_commandes']))
            return count($this->session['panier_commandes']);
        else if (isset($this->session['commande_id']))
            return 1;
        else
            return 0;
    }

    /**
     * @param commande, une instance de la classe Commande
     * le champ Panier de la commande sera modifié en fonction du panier actuel
     * et les modifications seront sauvegardées
     * @param user l'utilisateur pour lequel on enregistre cette commande, peut est null
     */
    public function addCommande($commande) {
        $nb = $this->getNbCommandes();
        if ($nb == 0) {
            // on ne crée pas de panier si il n'y a qu'une seule commande
            // à moins qu'il y ai déjà un panier en cours
            if (isset($this->session['panier_id']))
                $nb = 1; // triche pour ajouter la commade au panier en cours
            else
                $this->session['commande_id'] = $commande->get_id();

            $this->saveUserInfos($commande);
        }
        else if ($nb == 1 && isset($this->session['commande_id'])) {
            // dès qu'on ajoute une deuxième commande on crée un panier
            if ($this->creer() === false) {
                $this->session['commande_id'] = $commande->get_id();
                // on triche pour ne pas tomber dans la condition suivante
                $nb = 0;
            }
        }
        if ($nb >= 1) {
            $commande->Panier = $this->getPanier();
            $commande->save();
            Log::info('Commande '.$commande.' ajoutée au panier '.$this->panier);

            $this->session['panier_commandes'][] = $commande->get_id();
        }
    }

    /**
     * Redirige l'utilisateur vers la page de gestion du panier
     * @param commande instance de la classe Commande représentant la commande
     *   qui vient d'être ajoutée ou NULL
     */
    public function redirectTo($commande=NULL) {
        if ($commande !== NULL) {
            redirectOk('added='.$commande->get_id(), null, 'commander', 'panier');
        } else {
            // si il y a une commande supprimée on l'indique à la page de commande
            if (isset($this->session['commande_supprimee'])) {
                if (time() - $this->session['date_commande_supprimee'] < 60 * 2) {
                    redirectOk('deleted='.$this->session['commande_supprimee'], null, 'commander', 'panier');
                } else {
                    unset($this->session['commande_supprimee']);
                    unset($this->session['date_commande_supprimee']);
                    redirectOk('', null, 'commander', 'panier');
                }
            } else {
                redirectOk('', null, 'commander', 'panier');
            }
        }
    }

    /**
     * Défini l'utilisateur pour le panier actuel
     */
    public function setUser($user) {
        $this->user = $user;
        if ($user->is_logged()) {
            if (!isset($this->session['panier_user_id']) || $this->session['panier_user_id'] != $user->get_id()) {
                if ($this->getPanier() !== null) {
                    $this->panier->User = $user;
                    if ($this->panier->Mail === null)
                        $this->panier->Mail = $user->Email;
                    $this->panier->save();

                    $this->session['panier_user_id'] = $user->get_id();
                }
            }
        }
    }

    /**
     * Marque une commande comme annulée
     */
    public function supprimerCommande($commande) {
        $this->db->beginTransaction();
        if ($this->removeCommande($commande)) {
            $commande->Termine = true;
            $commande->save();
            $this->db->commit();

            $this->session['commande_supprimee'] = $commande->get_id();
            $this->session['date_commande_supprimee'] = time();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    /**
     * Annuler la dernière suppression de commande
     */
    public function annulerSuppression() {
        if (isset($this->session['commande_supprimee'])) {
            if (time() - $this->session['date_commande_supprimee'] < 60 * 2) {
                $this->db->beginTransaction();

                try {
                    $commande = Commande::getById($this->session['commande_supprimee']);
                } catch (UnknownObject $e) {
                    Log::info("Impossible d'annuler la suppression : commande ".$this->session['commande_supprimee']." introuvable");
                    $this->db->rollBack();
                    return false;
                }
                $this->addCommande($commande);
                $commande->Termine = false;
                $commande->save();

                $this->db->commit();

                unset($this->session['commande_supprimee']);
                unset($this->session['date_commande_supprimee']);
                return true;
            }
        }
        return false;
    }

    /**
     * Enlève une commande du panier
     * Si il n'y a qu'un commande efface aussi le panier
     */
    public function removeCommande($commande) {
        if (isset($this->session['commande_id'])) {
            if ($this->session['commande_id'] == $commande->get_id()) {
                Log::info("Supression de la comande $commande (dans aucun panier)");

                if ($this->commandes !== null) {
                    $this->commandes->removeObject($commande);
                }

                unset($this->session['commande_id']);
                return true;
            }
        } else if (isset($this->session['panier_id'])) {
            if (($key = array_search($commande->get_id(), $this->session['panier_commandes'])) !== false) {
                Log::info("Supression de la comande $commande du panier ".$this->session['panier_id']);
                unset($this->session['panier_commandes'][$key]);

                // si les commandes sont chargées on enlève la commande du résultat
                if ($this->commandes !== null) {
                    $this->commandes->removeObject($commande);
                }

                $commande->Panier = null;
                $commande->save();
                return true;
            }
        }
        return false;
    }

    /*
     * Renvoie le nom / prénom / mail défini lors de la première commande
     * Ces infos ne sont utiles que si l'utilisateur n'est pas connecté
     * Si il est connecté on utilisera les infos stockées dans la base
     */
    public function getUserInfos() {
        if (isset($this->session['panier_nom'], $this->session['panier_prenom'], $this->session['panier_mail'])) {
            return array('nom' => $this->session['panier_nom'],
                         'prenom' => $this->session['panier_prenom'],
                         'mail' => $this->session['panier_mail']);
        } else {
            return false;
        }
    }

    /*
     * Sauvegarde le nom / prénom / mail à partir d'une commande
     * pour qu'il puisse être récupéré facilement par la suite
     */
    public function saveUserInfos($commande) {
        $this->session['panier_nom'] = $commande->Nom;
        $this->session['panier_prenom'] = $commande->Prenom;
        $this->session['panier_mail'] = $commande->Mail;
    }
}