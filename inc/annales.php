<?php
class AnnalesException extends Exception { }

class Annales {
    public static $PAGES_OFFERTES_PAR_SUJET = 15;
    public static $PAGES_OFFERTES_PAR_CORRECTION = 30;
    public static $ANNALES_SUBJECT_TYPES = array('t' => 'Tests', 'u' => 'Tests 2', 'v' => 'Tests 3', 'p' => 'Partiels', 'q' => 'Partiels 2', 'm' => 'Médians', 'f' => 'Finaux');
    public static $ANNALES_SUBJECT_TYPES_CODES = array('t', 'u', 'v', 'p', 'q', 'm', 'f');
    public static $ANNALES_PAGES_TYPES_DESCRIPTIONS = array('p' => 'Partiels', 'm' => 'Médians', 'f' => 'Finaux');
    public static $ANNALES_PAGES_SUBJECT_TYPES = array('p' => array('t', 'u', 'v', 'p', 'q'), 'm' => array('m'), 'f' => array('f'));
    public static $ANNALES_SUBJECT_PAGES_TYPES = array('t' => 'p', 'u' => 'p', 'v' => 'p', 'p' => 'p', 'q' => 'p', 'm' => 'm', 'f' => 'f');
    public static $IMPRESSION_CSS = '<style type="text/css">
.uv {
	font-family: impact;
	text-align: center;
}

.titre  {
	margin-top: 80px;
	font-weight: bold;
	font-size: 90px;
}

.type {
	text-decoration: underline;
	font-size: 50px;
}

.message_bas {
	font-size: 24px;
	font-family: impact;
	text-align: center;
}
.intercalaire {
  width: 99%;
  text-align: center;
  font-family: impact;
  font-size: 80pt;
}
.uvpdg {
  width: 99%;
  text-align: center;
  font-family: impact;
  font-size: 100pt;
}
.typespdg {
  width: 99%;
  text-align: center;
  font-family: impact;
  font-size: 40pt;
}

</style>';

    public static function fabriquerAnnale($code, array $pagesTypes){
        require_once('lib/tcpdf/tcpdf.php');
        require_once('lib/fpdi/fpdi.php');

        $pagesTypesKey = array_keys($pagesTypes);
        $pagesTypesStr = implode('', $pagesTypesKey);
        $pagesTypesDescr = self::getTypesDescription($pagesTypesStr, self::$ANNALES_SUBJECT_TYPES);
        $sujetTypes = self::getSujetTypesFromPagesTypes($pagesTypesKey);
        $sujetTypesStr = implode('', $sujetTypes);


        $requete = query("SELECT sujet_uv, sujet_type, sujet_semestre, sujet_annee, sujet_corrige, sujet_commentaire
                        FROM polar_annales_sujets
                        WHERE sujet_uv LIKE '$code' AND sujet_ecarte = 0
                        AND sujet_type RLIKE '[$sujetTypesStr]'
                        ORDER BY sujet_uv, sujet_type, sujet_annee, sujet_semestre DESC, sujet_corrige");
        $type = '';

        $files = array();
        $sommaire = self::$IMPRESSION_CSS;

        // Ajout de la phrase de pub pour la boîte à médians
        $sommaire .= '
    <div class="uv">
        <span class="titre">Annales '.$code.'</span><br />Version '.getSemestre().'<br />';

        while($donnees = mysql_fetch_assoc($requete)) {
            if($donnees['sujet_type'] != $type) {
                if(in_array($type, self::$ANNALES_SUBJECT_TYPES_CODES))
                    $sommaire .= '</p>';

                $type = $donnees['sujet_type'];
                if(in_array($type, self::$ANNALES_SUBJECT_TYPES_CODES))
                    $sommaire .= '<p class="colonnes">';
                else
                    $sommaire .= '<p>';

                $sommaire .= '<span class="type">'.self::$ANNALES_SUBJECT_TYPES[$donnees['sujet_type']].'</span><br />';
            }
            $sommaire .= $donnees['sujet_semestre'].($donnees['sujet_annee'] < 10 ? '0'.$donnees['sujet_annee'] : $donnees['sujet_annee']);
            if($donnees['sujet_corrige'] == 1)
                $sommaire .= ' + Corrigé';

            if(!empty($donnees['sujet_info']))
                $sommaire .= '<span style="margin-left: 30px;">'.$donnees['sujet_info'].'</span>';

            $sommaire .= '<br />';

            // On ajoute le fichier dans la liste des fichiers à inclure
            $files[] = array($type, strtoupper(substr($code, 0, 1).'/'.$code.'_'.$donnees['sujet_type'].$donnees['sujet_semestre'].sprintf("%02d", $donnees['sujet_annee'])));
        }

        $sommaire .= '</div>';
        $sommaire .= '<div class="message_bas">Les annales sont disponibles grâce à TOI !<br />
    Après les examens, dépose ton sujet dans la boîte à médians située au Polar !</div>';

        $pdf = new FPDI();
        $pagetotal = 0;

        $pdf->SetFontSubsetting(true);

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // Ajout de la page de garde
        $pagecount = $pdf->setSourceFile("upload/annales/pdg.pdf");
        $tplidx = $pdf->ImportPage(1);
        $s = $pdf->getTemplatesize($tplidx);
        $pdf->AddPage('P', array($s['w'], $s['h']));
        $pdf->useTemplate($tplidx);
        $pdf->writeHTML(self::$IMPRESSION_CSS."<br /><div class=\"uvpdg\"><br /><br /><br />$code</div><div class=\"typespdg\">$pagesTypesDescr</div>", $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
        $pagetotal++;

        // Ajout du sommaire
        $pdf->AddPage('P');
        $pdf->writeHTML($sommaire, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
        $pagetotal++;

        // Import de l'annale
        $typeactuel = '';
        foreach($files as $file){
            if($file[0] != $typeactuel){
                // Ajout de l'intercalaire
                $pdf->AddPage('P');
                $intercalaire = self::$IMPRESSION_CSS.'<div class="intercalaire"><br /><br /><br />'.self::$ANNALES_SUBJECT_TYPES[$file[0]].'</div>';
                $pdf->writeHTML($intercalaire, $ln=true, $fill=false, $reseth=false, $cell=false, $align='');
                $pagetotal++;
                $typeactuel = $file[0];
            }
            $pagecount = $pdf->setSourceFile("upload/annales/sujets/{$file[1]}.pdf");
            for ($i = 1; $i <= $pagecount; $i++) {
                $tplidx = $pdf->ImportPage($i);
                $s = $pdf->getTemplatesize($tplidx);
                $pdf->AddPage('P', array($s['w'], $s['h']));
                $pdf->useTemplate($tplidx);
            }
            $pagetotal += $pagecount;
        }

        // Ajout du print automatique
        $pdf->IncludeJS("print(false, true);");

        // On sauvegarde le fichier
        $pdf->Output("upload/annales/{$code}_{$pagesTypesStr}.pdf", 'F');

        unset($pdf);

        $update = query("INSERT INTO polar_annales_pages (pages_code, pages_types, pages_nbrPages)
        VALUES ('$code', '$pagesTypesStr', $pagetotal)
        ON DUPLICATE KEY UPDATE pages_nbrPages = $pagetotal");
        return $pagetotal;
    }

    public static function getSujetTypesFromPagesTypes($pagesTypes) {
        $retArray = array();
        foreach($pagesTypes as $p) {
            $retArray = array_merge($retArray, self::$ANNALES_PAGES_SUBJECT_TYPES[$p]);
        }
        return $retArray;
    }

    public static function getPagesTypesFromSujetTypes($sujetTypes) {

        $retArray = array();
        foreach ($sujetTypes as $s) {
            $pageType = self::$ANNALES_SUBJECT_PAGES_TYPES[$s];
            if (isset($retArray[$pageType])) {
                $retArray[$pageType][] = $s;
            } else {
                $retArray[$pageType] = array($s);
            }
        }
        return $retArray;
    }

    public static function getSujetTypesForAnnales($code) {
        $req = query("SELECT sujet_type
FROM polar_annales_sujets
WHERE sujet_uv = '$code'
AND sujet_ecarte = 0
GROUP BY sujet_type");
        $retArr = array();
        while ($data = mysql_fetch_array($req)) {
            $retArr[] = $data[0];
        }
        return $retArr;
    }


    public static function array_power_set($array) {
        // initialize by adding the empty set
        $results = array(array());

        foreach ($array as $element) {
            foreach ($results as $combination) {
                $results[] = array_merge($combination, array($element));
            }
        }
        array_shift($results);
        return $results;
    }

    public static function getPagesTypesCombinations(array $pagesTypes) {
        $retArray = array();
        while (count($pagesTypes) !== 0) {
            $retArray[] = $pagesTypes;
            array_shift($pagesTypes);
        }
        return $retArray;
    }

    public static function getSujetPagesTypesCombinations(array $sujetTypes) {
        $pagesTypes = self::getPagesTypesFromSujetTypes($sujetTypes);
        return self::getPagesTypesCombinations($pagesTypes);
    }

    public static function getTypesDescription($types, $typesDescriptions) {
        if(!is_array($types)) {
            $typesExploded = str_split($types);
        } else {
            $typesExploded = $types;
        }

        $retStr = '';
        $notFirst = false;
        foreach ($typesExploded as $type) {
            if ($notFirst) {
                $retStr .= ' + ';
            }
            $retStr .= $typesDescriptions[$type];
            $notFirst = true;
        }

        return $retStr;
    }

    public static function retrieveTypesPages($onlyActives = true, $reduit = false) {
        $query = 'SELECT pau.uv_code AS Code, pap.pages_types AS Types, pap.pages_nbrPages AS NbPages
FROM polar_annales_pages pap
INNER JOIN polar_annales_uvs pau ON pap.pages_code = pau.uv_code';
        if ($onlyActives) {
            $query .= '
WHERE pau.uv_enVente = 1 AND pap.pages_nbrPages > 0';
        }
        $query .= '
ORDER BY Code, NbPages DESC, Types DESC';
        $req = query($query);

        $retArr = array();

        while($data = mysql_fetch_assoc($req)) {
            $code = $data['Code'];
            $types = $data['Types'];
            $nbPages = intval($data['NbPages']);

            if (isset($retArr[$code])) {
                if($reduit)
                    $retArr[$code][$types] = array(strtoupper($types), $nbPages);
                else
                    $retArr[$code][$types] = array(Annales::getTypesDescription($types, self::$ANNALES_PAGES_TYPES_DESCRIPTIONS), $nbPages);
            } else {
                if($reduit)
                    $retArr[$code] = array($types => array(strtoupper($types), $nbPages));
                else
                    $retArr[$code] = array($types => array(Annales::getTypesDescription($types, self::$ANNALES_PAGES_TYPES_DESCRIPTIONS), $nbPages));
            }
        }

        return $retArr;
    }

    /**
     * Enregistre une commande d'annale
     *
     * @param sring $login Login de l'étudiant
     * @author Florent Thévenet
     */
    function commander_annales($login) {
        global $ginger;
        require 'modules/annales/_constantes.php';
        require 'modules/annales/_fonctions.php';

        try {
            $user = $ginger->getUser($login);
            $nom = mysqlSecureText($user->nom);
            $prenom = mysqlSecureText($user->prenom);
            $mail = $user->mail;
        } catch (ApiException $e) {
            throw new AnnalesException("ce login n'existe pas");
        }

        if(!isset($_POST['uv0']) OR empty($_POST["uv0"]))
            throw new AnnalesException("aucune annale n'a été commandée");

        $ip = get_ip();

        //OK, on ajoute la commande
        query("INSERT INTO polar_commandes (
	        Type,
	        Nom,
	        Prenom,
	        Mail,
	        DateCommande,
	        IPCommande
        ) VALUES (
	        (SELECT ID FROM polar_commandes_types WHERE Nom LIKE 'Annale'),
	        '$nom',
	        '$prenom',
	        '$mail',
	        NOW(),
	        '$ip'
        )");

        $idCommande = mysql_insert_id();

        $listeUV = "";

        $req_contenu = "INSERT INTO polar_commandes_contenu (
	IDCommande,
	Detail,
	Quantite
	) VALUES ";

        $i = 0;
        while (isset($_POST["uv$i"])) {
            if(!empty($_POST["uv$i"])) {
                $uv = mysqlSecureText($_POST["uv$i"]);
                $types = mysqlSecureText($_POST["types$i"]);

                if ($i > 0) {
                    $req_contenu .= ', ';
                    $listeUV .= ', ';
                }

                $uv = mysqlSecureText(substr($_POST["uv$i"], 0, 4));
                $req_contenu .= "($idCommande, '{$uv}_{$types}', (SELECT pages_nbrPages FROM polar_annales_pages WHERE pages_code = '$uv' AND pages_types = '$types'))";
                $listeUV .= $uv . ' - ' . self::getTypesDescription($types, self::$ANNALES_PAGES_TYPES_DESCRIPTIONS);
            }

            $i += 1;
        }

        // On vire la virgule finale
        query($req_contenu);
        
        // Utilisation de la réduction éventuelle
        $aPayer = true;
        $reductionUtilisee = false;

        $req = query("SELECT SUM(Pages) AS PagesOffertes FROM polar_annales_reductions WHERE Login = '$login' AND DateUtilisation IS NULL");
        $data = mysql_fetch_assoc($req);
        $pagesOffertes = intval($data['PagesOffertes']);
        
        if($pagesOffertes > 0) {
            $req = query("SELECT SUM(Quantite) AS TotalPages FROM polar_commandes_contenu WHERE IDCommande = $idCommande");
            $data = mysql_fetch_assoc($req);
            $totalPages = intval($data['TotalPages']);
            
            if($pagesOffertes < $totalPages) {
                // Il reste des pages à payer.
                // =>  On marque les Réductions comme utilisées dans polar_annales_reductions
                // =>  On ajoute une ligne "Reduction" dans le détail de la commande
                $pagesUtilisees = $pagesOffertes;
                query("INSERT INTO polar_commandes_contenu(IDCommande, Detail, Quantite) VALUES($idCommande, 'Reduction', (-$pagesUtilisees))");
                query("UPDATE polar_annales_reductions SET DateUtilisation = NOW() WHERE Login = '$login' AND DateUtilisation IS NULL");
            }
            elseif($pagesOffertes >= $totalPages) {
                // La commande intégralement offerte
                // =>  On marque les Réductions comme utilisées
                // =>  On ajoute une ligne de Réductions contenant le nombre de pages offertes non utilisées et User (offrant) = NULL
                // =>  On ajoute une ligne "Reduction" dans le détail de la commande
                $pagesUtilisees = $totalPages;
                $pagesRestantes = $pagesOffertes - $totalPages;

                query("INSERT INTO polar_commandes_contenu(IDCommande, Detail, Quantite) VALUES($idCommande, 'Reduction', (-$pagesUtilisees))");
                query("UPDATE polar_annales_reductions SET DateUtilisation = NOW() WHERE Login = '$login' AND DateUtilisation IS NULL");
                
                if($pagesRestantes > 0)
                    query("INSERT INTO polar_annales_reductions(Login, Date, Pages) VALUES('$login', NOW(), $pagesRestantes)");
                
                query("UPDATE polar_commandes SET DatePaiement = NOW(), IDVente = NULL WHERE ID = $idCommande LIMIT 1");
                
                $aPayer = false;
            }
            
            $reductionUtilisee = true;
        }

        return $idCommande;
    }

    /**
     * Transforme un code de la forme PS90_mf
     * en une chaine lisible (ici PS90 Médians, Finaux)
     */
    public static function parseCode($code) {
        $parts = explode('_', $code);
        if (count($parts) != 2)
            return false;

        $types = array();
        $length = strlen($parts[1]);
        for ($i=0; $i < $length; $i++)
            $types[] = self::$ANNALES_PAGES_TYPES_DESCRIPTIONS[$parts[1][$i]];

        return $parts[0] . ' ' . implode(', ', $types);
    }
}