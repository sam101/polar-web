<?php

class Request {
    public static $module = NULL;
    public static $section = NULL;
    public static $filename = NULL;
    public static $tps_ini;
    public static $nb_requetes;

    // Request est l'endroit magique depuis lequel on devrait pouvoir accèder à tout
    public static $panier = NULL;

    public static function start() {
        self::$tps_ini = getTime();
        self::$nb_requetes = 0;
    }

    public static function getDureeGeneration() {
        return round((getTime() - self::$tps_ini), 4);
    }

    public static function getNbRequetes() {
        global $db;
        return self::$nb_requetes + $db->get_nb_queries();
    }

    public static function includeSection($module_=NULL, $filename_=NULL) {
        // le but est de réduire la liste des globals au fur et à mesure
        global $racine, $user, $conid, $db, $CONF, $extraHeaders, $extraFooter, $ginger;

        $design = 0; // à dégager mais énormement de pages l'utilisent
        // pour rester comptabible avec les vieilles pages on fixe $module, $section et $filename
        $module = Request::$module;
        $section = Request::$section;
        $filename = Request::$filename;

        if($module_ == NULL)
            $module_ = self::$module;
        if($filename_ == NULL)
            $filename_ = self::$filename;

        try {
            require('modules/'.$module_.'/'.$filename_.'.php');
        } catch(PolarUserError $e) {
            require_once('inc/header.php');
            echo '<h1>Erreur</h1>';
            echo '<pre>' . $e->getMessage() . '</pre>';
            require_once('inc/footer.php');
        }
    }

    public static function setPage($module, $section) {
        self::$module = $module;
        self::$section = $section;
        self::processFilename();
    }

    public static function load() {
        // Module/section sélectionné
        if (self::$module == NULL)
            if(!empty($_GET['module']))
                self::$module = mysqlSecureText($_GET['module']);
            else
                self::$module = 'news';

        if (self::$section == NULL)
            if(!empty($_GET['section']))
                self::$section = mysqlSecureText($_GET['section']);
            else
                self::$section = 'index';

        self::processFilename();
    }

    private static function processFilename() {
        // Détection de la validation d'un formulaire
        self::$filename = self::$section;
        if(substr(self::$section, -8) == '_control'){
            self::$section = substr(self::$section, 0, -8);
            self::$filename = self::$section."_control";
        }
    }

    /**
     * sauvegarde $module, $section, $_GET et $_POST en session
     * puis redirige vers l'url demandée
     */
    public static function saveAndRedirect($url) {
        self::save();
        header('Location: '.$url);
    }

    public static function save() {
        global $racine;

        $qs = "?";
        foreach ($_GET as $key => $value){
            if($key != "module" && $key != "section")
                $qs .= "$key=$value&";
        }
        $qs = substr($qs, 0, -1); // Enlève au choix le ? ou le dernier &

        $_SESSION['ReAuthTarget'] = $racine . self::$module . '/' . self::$filename . $qs;

        if (isset($_POST) && !isset($_SESSION['SavedPost']))
            $_SESSION['SavedPost'] = $_POST;
    }

    public static function inject() {
        if(isset($_SESSION['SavedPost'])){
            $_POST = $_SESSION['SavedPost'];
            unset($_SESSION['SavedPost']);
        }
        unset($_SESSION['ReAuthTarget']);
    }

    // Si on a une page en attente on redirige vers là bas
    public static function redirectToTarget() {
        if (isset($_SESSION['ReAuthTarget'])) {
            header('Location: '.$_SESSION['ReAuthTarget']);
            unset($_SESSION['ReAuthTarget']);
            die();
        }
    }

    // sauvegarde les paramètres et demande un réauthentification de l'utilisateur en cours
    public static function handlePrivate() {
        Request::save();
        self::includeSection('static', 'private');
    }

    public static function handleAuth() {
        Request::save();
        self::includeSection('static', 'auth');
    }

    public static function handle404() {
        self::includeSection('static', '404');
    }
}