<?php
function menu_element($module_, $section_, $texte) {
    global $module, $section, $racine;
    if ($section_ == $section && $module_ == $module)
        $a = ' class="active"';
    else
        $a = '';
    return '<li'.$a.'><a href="'.urlTo($module_, $section_).'">'.$texte.'</a></li>';
} ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
  <head>
	<?php
		if(empty($titrePage))
			echo "    <title>Le Polar</title>";
		else
			echo "    <title>Le Polar - $titrePage</title>";
	?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="icon" href="<?php echo urlStyle('icones/favicon.ico') ?>" type="image/ico"/>
    <link href="<?php echo urlStyle('flatstrap.css') ?>" rel="stylesheet" media="screen" />
    <link href="<?php echo urlStyle('bootstrap-responsive.min.css') ?>" rel="stylesheet" media="screen" />
    <link href="<?php echo urlStyle('polar.css') ?>" rel="stylesheet" media="screen" />
    <?php
	if(!empty($extraHeaders))
		echo $extraHeaders;
	?>
  </head>

  <body>
    <div id="wrap">
      <div class="container main-navbar">
        <div class="navbar navbar-inverse" style="margin-bottom:0">
          <a href="<?php echo urlTo() ?>" id="logo">
          <img src="<?php echo urlStyle('textures/head.png') ?>" alt="Le Polar" />
        </a>
          <div class="navbar-inner">
          <a class="brand" href="<?php echo urlTo() ?>">Le Polar</a>
            <div class="pull-right">
              <ul class="nav">
<?php
echo menu_element('news', 'index', 'Accueil');
echo menu_element('static', 'presentation', 'À propos');
echo menu_element('membres', 'horaires', 'Horaires');
echo menu_element('membres', 'liste', 'L\'équipe');
echo menu_element('hotline', 'index', 'Contact');
?>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Produits <b class="caret"></b></a>
<ul class="dropdown-menu pull-right">
  <?php
    echo menu_element('secteurs', 'impressions', 'Impressions');
    echo menu_element('secteurs', 'fournitures', 'Fournitures');
    echo menu_element('secteurs', 'informatique', 'Informatique');
    echo menu_element('secteurs', 'services', 'Services');
  ?>
</ul>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a>
<ul class="dropdown-menu pull-right">
  <?php
    echo menu_element('commander', 'posters', 'Posters');
    echo menu_element('annales', 'commander', 'Annales');
    echo menu_element('billetterie', 'preinscription', 'Billetterie');
    echo menu_element('commander', 'badges', 'Badges');
    echo menu_element('videoprojecteur', '', 'Vidéo-projecteur');
    echo menu_element('assos', 'reactiver', 'Compte asso');
  ?>
</ul>
</li>
<?php
  // on affiche le menu panier si besoin
if (Request::$panier->actif()): ?>
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-shopping-cart"></i> <?php echo Request::$panier->getNbCommandes() ?> &nbsp;<b class="caret"></b></a>
  <ul class="dropdown-menu pull-right">
    <?php
        echo menu_element('commander', 'panier', 'Détails');
        echo menu_element('commander', 'paiement', 'Payer en ligne');
        echo menu_element('commander', 'panier_control?pay_later', 'Payer au Polar');
    ?>
  </ul>
</li>
<?php endif;
/* On va afficher soit les boutons de connexion
soit le nom de l'utilisateur avec le menu d'options */
if ($user->is_logged()): ?>
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-user"></i> <?php echo $user->Prenom ?> <?php echo $user->Nom ?> &nbsp;<b class="caret"></b></a>
  <ul class="dropdown-menu pull-right">
    <li><a href="<?php echo urlTo('membres', 'profil') ?>"><img src="<?php echo $racine . $user->getPhoto() ?>" /><br/>Mon profil</a></li>
    <?php if($user->Staff): ?>
    <li><a href="<?php echo urlTo('membres', 'perms') ?>">Mes permanences</a></li>
    <?php endif; ?>
    <li><a href="<?php echo urlTo('membres', 'assos') ?>">Mes assos</a></li>
    <li><a href="<?php echo urlTo('commander', 'panier') ?>">Mon panier</a></li>
    <li class="divider"></li>
    <li><a href="<?php echo urlTo('membres', 'deconnexion') ?>">Déconnexion</a></li>
  </ul>
</li>
<?php else: ?>
                <li><a href="<?php echo $CONF["cas_login"] ?>"><i class="icon-white icon-user"></i> Connexion</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                  <li><a href="<?php echo $CONF["cas_login"] ?>">Utilisateurs UTC (CAS)</a></li>
                  <li><a href="<?php echo urlTo() ?>membres/connexion">Extérieurs</a></li>
                </ul>
              </li>
<?php
    endif; // fin du menu de connexion
 ?>
              </ul>
            </div><!--/.pull-right -->
          </div>
        </div>
        <?php if ($user->is_logged()): ?>
        <?php require_once('inc/menu_permanencier.php'); ?>
<?php endif; ?>
      </div>
<div class="container">&nbsp;</div>
      <div class="container">
