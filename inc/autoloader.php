<?php

spl_autoload_register(function ($class) {
        $parts = explode('\\', $class);
        if (count($parts) == 1) // on suppose que c'est un object de l'ORM
            include_once 'inc/objects/'. $class . '.class.php';
        else { // on suppose que c'est un module dans lib/
            $module = strtolower($parts[0]);
            include_once 'lib/'.$module.'/src/'.implode('/', $parts).'.php';
        }
    });