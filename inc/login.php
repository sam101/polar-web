<?php

$user = NULL;
/* juste pour que la variable $user existe,
   elle sera modifiée plus tard et est toujours une instance de la classe UtilisateurSite
*/

// Connexion utilisateur
// On regarde si on reçoit un ticket du CAS ou une authentification extérieure
if (isset($_GET['ticket'])) {
    // On valide le ticket CAS à moins qu'il soit destiné à payutc
    if (isset($_GET['module'], $_GET['section']) && $_GET['module'] == 'payutc' && $_GET['section'] == 'payutc') {
        Payutc::saveTicket($_GET['ticket']);
        Request::redirectToTarget(); // on renvoie vers la page initiale, les $_GET et $_POST sont intacts et seront injectés sur cette page
    }

    $ch = curl_init($CONF["cas_validate"] . $_GET['ticket']);
    curl_setopt_array($ch, array(
                                 CURLOPT_USERAGENT => "polar-web/0.1",
                                 CURLOPT_RETURNTRANSFER => true
                                 ));
    $reponse = curl_exec($ch);

    // On récupère l'identifiant de l'utilisateur
    $xml = simplexml_load_string($reponse);
    if ($xml !== False) {
        $users = $xml->xpath('//cas:serviceResponse//cas:authenticationSuccess//cas:user');
        if ($users != FALSE) {
            $user = null;
            try {
                $user = UtilisateurSite::create_from_ginger($ginger, (string)$users[0]);

                if ($user->get_id() === null) {
                    $user->save();
                    Log::info('Utilisateur '.$user.' créé');
                    // On affiche la page de bienvenue
                    Request::setPage('membres', 'welcome');
                }
                $user->log_with_cas();

            } catch (ApiException $e) {
                Log::warning("Erreur Ginger lors de la création d'un utilisateur :" . (string)$users[0], array('exception' => $e));

                unset($_SESSION['ReAuthTarget']);
                redirectWithErrors('', "Désolé, une erreur est survenue pendant la récupération de ton profil, merci de contacter le Simde (simde@assos.utc.fr) en précisant ton login : " . (string)$users[0], "news", "index");

            }
        } else {
            Log::warning("Pas de login dans la réponse du cas : " . $reponse);
            unset($_SESSION['ReAuthTarget']);
            redirectWithErrors('', "Erreur d'authentification CAS", "news", "index");
        }
    } else {
        Log::warning('XML du cas impossible à parser : '.$reponse);
        unset($_SESSION['ReAuthTarget']);
        redirectWithErrors('', "Le CAS a renvoyé une réponse invalide, merci de réessayer dans quelques minutes", "news", "index");
    }
} else if (isset($_POST['ext-mail'], $_POST['ext-mdp'])) {
    // Authentification par mot de passe

    $user = UtilisateurSite::select()->where('Email LIKE ?', $_POST['ext-mail'])->getOne();
    if (is_null($user) || !$user->checkMotDePasse($_POST['ext-mdp'])) {
        header('Location: '.$racine.'membres/connexion?erreur_login');
        die();
    } else {
        $user->log_with_password();
    }
} else {
    unset($_SESSION['ReAuthTarget']);
}