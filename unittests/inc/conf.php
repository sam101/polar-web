<?php

$CONF = array(
	"polar_url" => "http://localhost".$racine,
	"payutc_url" => "http://localhost/server/web",
    "ginger_key" => "blablabla",
    "ginger_url" => "http://localhost/ginger/web/v1/",
    "cas_url" => "http://localhost/cas/",
    "payutc_caisse_key" => "",
    "payutc_manager_key" => "",
    "payutc_web_key" => "",
    "payutc_caisse_app_id" => 3,
    "payutc_manager_app_id" => 5,
    "payutc_web_app_id" => 4,
    "proxy" => NULL,
    "payutc_fundation" => 3,
    // Est-ce qu'on active payutc ?
    "use_payutc" => true,
 	);

$CONF["cas_login"] = $CONF["cas_url"] . 'login?service=' . $CONF["polar_url"];
$CONF["cas_logout"] = $CONF["cas_url"] . 'logout?url=' . $CONF["polar_url"];
$CONF["cas_validate"] = $CONF["cas_url"] . 'serviceValidate?service=' . $CONF["polar_url"] . "&pgtUrl=" . $CONF["polar_url"] . "&ticket=";

$CONF["polar_url_for_payutc"] = $CONF["polar_url"] . 'payutc/payutc';
$CONF["payutc_cas_login"] = $CONF["cas_url"] . 'login?service=' . $CONF["polar_url_for_payutc"];
