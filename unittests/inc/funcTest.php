<?php

require_once dirname(__FILE__) . '/../../inc/objects/PolarObject.class.php';
require_once dirname(__FILE__) . '/../../inc/objects/PolarDB.class.php';
require_once dirname(__FILE__) . '/../../lib/ginger-client/Ginger.class.php';
require_once dirname(__FILE__) . '/bdd_racine.php';
require_once dirname(__FILE__) . '/conf.php';
require_once dirname(__FILE__) . '/../../inc/essentials.php';
require_once dirname(__FILE__) . '/../../inc/Request.class.php';
require_once dirname(__FILE__) . '/../../inc/func.php';

/**
 * Test class for func.
 */
class funcTest extends PHPUnit_Framework_TestCase {

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
    }



    public function testStartsWith() {
        $str = "fooBarBaz";
        $this->assertTrue(startsWith($str, "foo"));
        $this->assertTrue(startsWith($str, "fooBarBaz"));
        $this->assertTrue(startsWith($str, "f"));
        $this->assertTrue(startsWith($str, "foo", false));
        $this->assertTrue(startsWith($str, "fooba", false));


        $this->assertFalse(startsWith($str, "fop"));
        $this->assertFalse(startsWith($str, "fooBaz", false));
        $this->assertFalse(startsWith($str, "foobar"));

    }

    public function testEndsWith() {
        $str = "fooBarBaz";
        $this->assertTrue(endsWith($str, "Baz"));
        $this->assertTrue(endsWith($str, "fooBarBaz"));
        $this->assertTrue(endsWith($str, "z"));
        $this->assertTrue(endsWith($str, "baz", false));
        $this->assertTrue(endsWith($str, "barbaz", false));


        $this->assertFalse(endsWith($str, "Bar"));
        $this->assertFalse(endsWith($str, "BazBaz", false));
        $this->assertFalse(endsWith($str, "barbaz"));
    }

    public function testMkrndir() {
        $this->assertTrue(@mkrndir('.'));
        $this->assertTrue(@mkrndir('/tmp/php_mkrndir_test'));

        if (is_dir('/tmp/php_mkrndir_test/rec/cre/test')) {
            @rmdir('/tmp/php_mkrndir_test/rec/cre/test');
            @rmdir('/tmp/php_mkrndir_test/rec/cre');
            @rmdir('/tmp/php_mkrndir_test/rec');
            @rmdir('/tmp/php_mkrndir_test');
        }

        $this->assertFalse(@mkrndir('/tmp/php_mkrndir_test/rec/cre/test', 0777, false));
        $this->assertTrue(@mkrndir('/tmp/php_mkrndir_test/rec/cre/test'));
        $this->assertFalse(@mkrndir('/dev/null/t'));
    }

    public function testGenererIDVente() {
        $venteFunctMax = genererIDVente();

        $manualSimpleQuery1 = "SELECT MAX(IDVente) AS MaxIDVente FROM polar_caisse_ventes_global";
        $manualSimpleQuery2 = "SELECT MAX(IDVente) AS MaxIDVente FROM polar_caisse_ventes";

        $manualResult1 = query($manualSimpleQuery1);
        $manualResult2 = query($manualSimpleQuery2);

        $manualTab1 = mysql_fetch_row($manualResult1);
        $manualTab2 = mysql_fetch_row($manualResult2);

        $manualMax = max($manualTab1[0], $manualTab2[0]) + 1;
        $this->assertEquals($manualMax, $venteFunctMax);
    }


}

?>
