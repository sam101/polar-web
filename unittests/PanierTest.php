<?php

require_once 'inc/objects/PolarDB.class.php';
require_once 'inc/objects/PolarObjectsArray.class.php';
require_once 'inc/objects/PolarQuery.class.php';
require_once 'inc/objects/Commande.class.php';
require_once 'inc/objects/CommandeContenu.class.php';
require_once 'inc/objects/PasswordProtected.class.php';
require_once 'inc/objects/Utilisateur.class.php';
require_once 'inc/objects/UtilisateurSite.class.php';
require_once 'inc/objects/PanierWeb.class.php';
require_once 'inc/objects/PanierWebNonReactivable.class.php';
require_once 'inc/objects/Article.class.php';
require_once 'inc/objects/ArticlePayutc.class.php';
require_once 'inc/objects/TarifBilletterie.class.php';
require_once 'inc/objects/Billetterie.class.php';
require_once 'inc/objects/CommandeType.class.php';
require_once 'inc/objects/TypeCommande.class.php';
require_once 'inc/Panier.php';
require_once 'lib/psr/src/Psr/Log/LogLevel.php';
require_once 'unittests/Log.php';

function get_ip() {
    return "127.0.0.1";
}

/**
 * Faux CommandesMailer
 */
class CommandesMailer {
    public static function archiverPanier($panier) {
        return true;
    }
    public static function validerPanier($panier) {
        return true;
    }
    public static function validerCommande($commande) {
        return true;
    }
    public static function confirmerPaiementWeb($panier) {
        return true;
    }
    public static function confirmerPaiementAsso($mail, $commandes) {
        return true;
    }
}

/**
 * Permet de tester si un PolarObject a un attribut supplémentaire particulier
 */
class Constraint_HasAdditionalAttr extends PHPUnit_Framework_Constraint
{
    public function __construct($attr) {
        $this->attr = $attr;
    }

    public function matches($objet)
    {
        return $objet->hasAdditionnalAttr($this->attr);
    }

    public function toString()
    {
        return 'has additional attribute "'.$this->attr.'"';
    }
}

/**
 * Phpunit appelle setUp à chaque fois donc on doit avoir
 * un object où on stocke les variables qui persistent pour émuler
 * la session et la persistence de la base de données
 */
class S {
    public static $db;
    public static $session;
    public static $test;
    public static $redirectTo = array('arg' => null,
                                      'message' => null,
                                      'module' => null,
                                      'section' => null);

    public static function setUp() {
        self::$db = new PolarDB("localhost", "polar", "root", "root");
        self::$db->beginTransaction();
        PolarObject::$db = self::$db;
        self::$session = array();
    }

    public static function setTest($test) {
        self::$test = $test;
    }

    public static function resetSession() {
        self::$session = array();
    }

    public static function setRedirectTo($arg, $message, $module, $section) {
        self::$redirectTo = compact('arg', 'message', 'module', 'section');
    }
}
S::setup();

function redirectOk($arg, $message, $module, $section) {
    S::$test->assertEquals(S::$redirectTo, compact('arg', 'message', 'module', 'section'));
}


class PanierTest extends PHPUnit_Framework_TestCase
{
    public function assertHasAdditionalAttribute($attr, $objet) {
        S::setTest($this);
        $this->assertThat($objet, new Constraint_HasAdditionalAttr($attr));
    }

    public function setUp() {
        $this->panier = new Panier(S::$db, S::$session);
    }

    public function testInit() {
        $this->assertEquals(S::$db, $this->panier->db);
        $this->assertEquals(S::$session, $this->panier->session);
        $this->assertEstNettoye();
    }

    public function assertEstNettoye() {
        $this->assertCount(0, S::$session);
        $this->assertNull($this->panier->user);
        $this->assertNull($this->panier->commandes);
        $this->assertNull($this->panier->panier);
        $this->assertFalse($this->panier->getUserInfos());
        $this->assertFalse($this->panier->actif());
    }

    public function testActif() {
        S::$session['commande_id'] = 3;
        $this->assertTrue($this->panier->actif());
        unset(S::$session['commande_id']);
        S::$session['panier_id'] = 3;
        $this->assertTrue($this->panier->actif());
        unset(S::$session['panier_id']);
    }

    public function testGetpanier() {
        S::resetSession();
        $this->setUp();

        $this->assertNull($this->panier->getPanier());

        S::$session['panier_id'] = 0;
        $this->assertNull($this->panier->getPanier());

        $panier = new PanierWeb(array('DateCreation' => raw('NOW()'),
                                      'Etat' => 'filling'));
        $panier->save();

        S::$session['panier_id'] = $panier->get_id();
        $this->assertEquals($panier->get_id(), $this->panier->getPanier()->get_id());
    }

    public function testGetNbCommandes() {
        // pas de session => pas de panier
        $this->assertEquals(0, $this->panier->getNbCommandes());

        // si il y a commande_id il y a une seule commande
        S::$session['commande_id'] = 3;
        $this->assertEquals(1, $this->panier->getNbCommandes());
        unset(S::$session['commande_id']);

        // si il y a panier_commandes c'est la taille de l'array
        S::$session['panier_commandes'] = array(1, 2);
        $this->assertEquals(2, $this->panier->getNbCommandes());

        // si il y a panier_commandes on ne tient pas compte de commande_id
        S::$session['commande_id'] = 3;
        $this->assertEquals(2, $this->panier->getNbCommandes());
        unset(S::$session['commande_id']);

        unset(S::$session['panier_id']);
        unset(S::$session['panier_commandes']);
    }

    public function testAddCommandes() {
        $commande = Commande::getById(17190);

        // premier cas : on ajoute la toute première commande :
        // ça ne crée pas de panier
        // et ça ajoute les infos de l'utilisateur en session
        $this->panier->addCommande($commande);
        $this->assertEquals($commande->get_id(), S::$session['commande_id']);
        $this->assertEquals($commande->Nom, S::$session['panier_nom']);
        $this->assertEquals($commande->Prenom, S::$session['panier_prenom']);
        $this->assertEquals($commande->Mail, S::$session['panier_mail']);
        $this->assertEquals($this->panier->getUserInfos(),
                            array('nom' => $commande->Nom,
                                  'prenom' => $commande->Prenom,
                                  'mail' => $commande->Mail));

        // Deuxième cas : on ajoute une deuxième commande
        // ça crée un panier et définie les attributs panier sur les deux commandes
        $commande2 = Commande::getById(17206);
        $this->panier->addCommande($commande2);

        $this->assertFalse(isset(S::$session['commande_id']));
        $this->assertCount(2, S::$session['panier_commandes']);

        $this->assertTrue(isset(S::$session['panier_id']));
        $panier = $this->panier->getPanier();
        $this->assertEquals($panier->get_id(), S::$session['panier_id']);

        // on regarde si c'est toujours les infos user de la première commande
        $this->assertEquals($this->panier->getUserInfos(),
                            array('nom' => $commande->Nom,
                                  'prenom' => $commande->Prenom,
                                  'mail' => $commande->Mail));

        // tests sur le nouveau panier
        $this->assertEquals('filling', $panier->Etat);
        $this->assertNull($panier->TransactionID);
        $this->assertNull($panier->User);
        $this->assertEquals($commande->Mail, $panier->Mail);
        $this->assertNull($panier->IDVente);
        $this->assertNull($panier->CommandeGroupe);

        // test sur les commandes
        $commande = Commande::getById(17190);
        $commande2 = Commande::getById(17206);
        $this->assertEquals($panier->get_id(), $commande->get_object_id('Panier'));
        $this->assertEquals($panier->get_id(), $commande2->get_object_id('Panier'));
    }

    /**
     * @depends testAddCommandes
     */
    public function testSetUser() {
        // on essaye d'abord avec un utilisateur non loggé
        $user = UtilisateurSite::getById(913);
        $this->panier->panier = null;
        $this->panier->setUser($user);
        $this->assertEquals($user, $this->panier->user);
        $panier = $this->panier->getPanier();
        $this->assertNull($panier->User);
        $this->assertFalse(isset(S::$session['panier_user_id']));

        $user->auth_method = LOGIN_METHOD_CAS;
        $this->panier->setUser($user);
        $this->panier->panier = null;
        $panier = $this->panier->getPanier();
        $this->assertEquals(913, $panier->get_object_id('User'));
        $this->assertEquals(913, S::$session['panier_user_id']);
    }

    /*
     * @depends testAddCommandes
     */
    public function testGetCommandes() {
        $commandes = $this->panier->getCommandes();

        $this->assertInstanceOf('PolarObjectsArray', $commandes);
        $this->assertCount(2, $commandes);

        $commande = $commandes[0];
        $this->assertEquals(S::$session['panier_id'], $commande->get_object_id('Panier'));

        $this->assertHasAdditionalAttribute('Article', $commande);
        $this->assertHasAdditionalAttribute('NomArticle', $commande);
        $this->assertHasAdditionalAttribute('CodeCaisse', $commande);
        $this->assertHasAdditionalAttribute('NomBilletterie', $commande);
        $this->assertHasAdditionalAttribute('NomTarif', $commande);
        $this->assertHasAdditionalAttribute('Contenu', $commande);
        $this->assertHasAdditionalAttribute('NomType', $commande);
    }

    /*
     * @depends testAddCommandes
     */
    public function testAddMoreCommandes() {
        /* on va tester le cas (improbable) où la commande unique dans
         * le panier a été supprimée de la db
         * et on ajoute une nouvelle commande
         */
        S::resetSession();

        $commande = Commande::getById(17192);
        $this->panier->addCommande($commande);
        $commande->delete();

        $commande2 = Commande::getById(17193);
        $this->panier->addCommande($commande2);

        $this->assertEquals($commande2->get_id(), S::$session['commande_id']);
    }

    /**
     * @depends testAddCommandes
     */
    public function testRemoveCommande() {
        // on ajoute une commande, comme ça il y en a deux
        $commande = Commande::getById(17194);
        $this->panier->addCommande($commande);

        $commandeNonPresente = Commande::getById(17195);
        $this->assertFalse($this->panier->removeCommande($commandeNonPresente));

        $size = count($this->panier->getCommandes());
        $this->assertTrue($this->panier->removeCommande($commande));
        $commandes2 = $this->panier->getCommandes();
        $this->assertCount($size - 1, $commandes2);

        // on regarde si on l'a bien enlevé de la liste
        $found = false;
        foreach($commandes2 as $c) {
            if ($c->get_id() == $commande->get_id()) {
                $found = true;
                break;
            }
        }
        $this->assertFalse($found);

        // on essaye d'enlever la commande si il n'y en a qu'une seule
        S::resetSession();
        $this->setUp();
        $this->panier->addCommande($commande);
        // on recrée un panier comme si on allait sur une autre page
        $this->setUp();
        $this->assertFalse($this->panier->removeCommande($commandeNonPresente));
        $this->panier->getCommandes();
        $this->assertTrue($this->panier->removeCommande($commande));
        $this->assertFalse(isset(S::$session['commande_id']));
        $this->assertCount(0, $this->panier->getCommandes());

        // on peut même esayer quand il n'y a plus rien
        $this->assertFalse($this->panier->removeCommande($commandeNonPresente));
    }

    /**
     * @depends testAddCommandes
     * @depends testRemoveCommande
     */
    public function testSupprimerCommande() {
        S::resetSession();
        $this->setUp();

        $commande = Commande::getById(17194);
        $this->panier->addCommande($commande);
        $this->assertTrue($this->panier->supprimerCommande($commande));
        $this->assertEquals(0, $this->panier->getNbCommandes());

        $commande = Commande::getById($commande->get_id());
        $this->assertNull($commande->Panier);
        $this->assertTrue($commande->Termine);

        $this->assertTrue(isset(S::$session['commande_supprimee']));
        $this->assertEquals($commande->get_id(), S::$session['commande_supprimee']);
        $this->assertTrue(isset(S::$session['date_commande_supprimee']));

        // on regarde si on est redirigé au bon endroit
        S::setRedirectTo('deleted=17194', null, 'commander', 'panier');
        $this->panier->redirectTo();

        // on annule la suppression
        // d'abord pour de faux
        S::$session['commande_supprimee'] = 0;
        $this->assertFalse($this->panier->annulerSuppression());
        S::$session['commande_supprimee'] = $commande->get_id();

        // puis pour de vrai
        $this->assertTrue($this->panier->annulerSuppression());
        $this->assertFalse(isset(S::$session['date_commande_supprimee']));
        $this->assertFalse(isset(S::$session['commande_supprimee']));

        // on regarde si la suppression foire bien quand il faut
        $this->assertFalse($this->panier->supprimerCommande(Commande::getById(17195)));

        // on simule la redirection 2 (fausses) minutes plus tard
        $this->assertTrue($this->panier->supprimerCommande($commande));
        S::$session['date_commande_supprimee'] -= 123;
        $this->setUp();
        S::setRedirectTo('', null, 'commander', 'panier');
        $this->panier->redirectTo();
        $this->assertFalse(isset(S::$session['date_commande_supprimee']));
        $this->assertFalse(isset(S::$session['commande_supprimee']));

        // le final
        $this->assertFalse($this->panier->annulerSuppression());
    }

    public function testNettoyer() {
        $this->panier->nettoyer();
        $this->assertEstNettoye();
    }

    public function testPayLater() {
        $this->assertFalse($this->panier->payLater());

        // on essaye d'abord avec une comande simple
        $commande = Commande::getById(17194);
        $this->panier->addCommande($commande);
        $this->assertEquals($commande->get_id(), $this->panier->payLater());

        // maintenant on tente avec un panier
        $this->setUp();
        $this->panier->addCommande($commande);
        $commande2 = Commande::getById(17193);
        $this->panier->addCommande($commande2);

        $panier = $this->panier->getPanier();
        $id = $this->panier->payLater();
        $this->assertInstanceOf('Commande', $panier->CommandeGroupe);
        $this->assertEquals($panier->CommandeGroupe->get_id(), $id);
    }

    public function testArchiver() {
        S::resetSession();
        $this->setUp();

        $user = UtilisateurSite::getById(913);
        $user->auth_method = LOGIN_METHOD_CAS;
        $this->panier->setUser($user);

        // même avec une seule commande la fonction d'archivage doit créer un panier si un utilisateur est loggé
        $commande = Commande::getById(17195);
        $this->panier->addCommande($commande);

        $this->panier->archiver();
        $this->assertEstNettoye();

        $commande1 = Commande::getById(17195);
        $this->assertInstanceOf('PanierWeb', $commande1->Panier);
        $panier = $commande1->Panier;
        $this->assertEquals('saved', $panier->Etat);

        // par contre si personne n'est loggé ça nettoye simplement la session
        $user->auth_method = LOGIN_METHOD_NO;
        $this->panier->setUser($user);
        $commande = Commande::getById(17196);
        $this->panier->addCommande($commande);
        $this->panier->archiver();
        $this->assertEstNettoye();

        $commande = Commande::getById(17196);
        $this->assertNull($commande->Panier);

        return array($panier, $commande1);
    }

    /**
     * @depends testArchiver
     */
    public function testActiver($truc) {
        list($panier, $commande) = $truc;
        $this->panier->activer($panier);

        // le test sur l'utilisateur doit être fait par l'appelant
        // actuellement on n'a qu'un appelant mais si on a d'autre
        // appelants plus tard il faudra revoir si c'est pertinent
        // pour rappel on a sauvegardé la comande
        $panier = PanierWeb::getById($panier->get_id());
        $this->assertEquals($panier->get_id(), S::$session['panier_id']);
        $this->assertEquals('filling', $panier->Etat);
        $this->assertCount(1, S::$session['panier_commandes']);
        $this->assertContains($commande->get_id(), S::$session['panier_commandes']);

        // si on active un panier alors qu'un autre est déjà actif ça doit sauvegarder le premier
        $panier2 = new PanierWeb(array('DateCreation' => raw('NOW()'),
                                      'Etat' => 'failed'));
        $this->panier->activer($panier2);

        $panier = PanierWeb::getById($panier->get_id());
        $this->assertEquals('saved', $panier->Etat);

        // le nouveau panier est vide
        $this->assertEquals($panier2->get_id(), S::$session['panier_id']);
        $this->assertEquals('filling', $panier2->Etat);
        $this->assertCount(0, S::$session['panier_commandes']);

        return $panier;
    }

    /**
     * @depends testActiver
     * @expectedException PanierWebNonReactivable
     */
    public function testActiverSent($panier) {
        $panier->Etat = 'sent';
        $this->panier->activer($panier);
    }

    /**
     * @depends testActiver
     * @expectedException PanierWebNonReactivable
     */
    public function testActiverpaid($panier) {
        $panier->Etat = 'paid';
        $this->panier->activer($panier);
    }

    public function testRedirectTo() {
        S::setRedirectTo('', null, 'commander', 'panier');
        $this->panier->redirectTo();

        S::setRedirectTo('added=17191', null, 'commander', 'panier');
        $this->panier->redirectTo(Commande::getById(17191));
    }
}