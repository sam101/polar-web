<?php

require_once 'inc/objects/PolarDB.class.php';
require_once 'inc/objects/CommandesMailer.class.php';
require_once 'inc/objects/Commande.class.php';
require_once 'inc/objects/CommandeContenu.class.php';
require_once 'inc/objects/PasswordProtected.class.php';
require_once 'inc/objects/Utilisateur.class.php';
require_once 'inc/objects/PanierWeb.class.php';
require_once 'inc/objects/Article.class.php';
require_once 'inc/objects/ArticlePayutc.class.php';
require_once 'inc/objects/TarifBilletterie.class.php';
require_once 'inc/objects/Billetterie.class.php';
require_once 'inc/objects/CommandeType.class.php';
require_once 'lib/psr/src/Psr/Log/LogLevel.php';
require_once 'unittests/Log.php';

$test_php_unit_panier = null;
function sendMail($from, $from_nom, $to, $sujet, $html, $pj=array()) {
    global $test_php_unit_panier;
    if ($test_php_unit_panier instanceof PHPUnit_Framework_TestCase) {
        $test_php_unit_panier->assertEquals('polar@assos.utc.fr', $from);
        $test_php_unit_panier->assertEquals('Le Polar', $from_nom);
        $test_php_unit_panier->assertEquals(array('florent@fthevenet.fr'), $to);
        $test_php_unit_panier->assertEquals('Bonjour', $sujet);
        $test_php_unit_panier->assertEquals('<html></html>', $html);
        $test_php_unit_panier->assertEquals(array(), $pj);
    }
}

$racine = "/polar/";
$CONF = array("polar_url" => "http://assos.utc.fr".$racine);

class CommandesMailerTest extends PHPUnit_Framework_TestCase
{
    public function setUp() {
        $this->db = new PolarDB("localhost", "polar", "root", "root");
        $this->db->beginTransaction();
        PolarObject::$db = $this->db;
    }

    public function tearDown() {
        $this->db->rollback();
    }

    public function testSendMail() {
        global $test_php_unit_panier;
        $test_php_unit_panier = $this;
        CommandesMailer::sendMail('<html></html>', 'Bonjour', 'florent@fthevenet.fr');
    }

    public function testUrlAbsolue() {
        $this->assertEquals('http://assos.utc.fr/polar/news/index', CommandesMailer::urlAbsolue("/polar/news/index"));
    }
}
?>