<?php
require_once 'inc/objects/Page.class.php';
require_once 'inc/objects/PasswordProtected.class.php';
require_once 'inc/objects/Utilisateur.class.php';
require_once 'inc/objects/PolarDB.class.php';
require_once 'inc/objects/Article.class.php';
require_once 'inc/objects/Vente.class.php';
require_once 'inc/objects/PolarAssociation.class.php';

/**
 * TODO
 *
 * polarobject
 * - hydrate
 * - get
 * - set
 * - get_stored_attribute
 * - get_object_id
 * - get_id
 * - set_id
 * - format_from_attributes
 * - hasAdditionnalAttr
 * - __toString
 *
 * polarquery
 * - addSubquery
 * - addInput
 * - order
 * - groupBy
 * - join
 * - leftJoin
 * - replace_classname_by_alias
 * - getIterator
 */

class TestObject extends PolarObject {
    public static $primary_key = 'id';
    public static $table = 'tests';
    protected static $attrs = array(
        'texte' => T_STR,
        'nombre' => T_INT,
        'liste' => array('a', 'b', 'c', 'd'),
        'flottant' => T_FLOAT,
        'booleen' => T_BOOL,
        'objet' => 'TestObject');
    protected static $nulls = array('objet');
}

class PolarORMTest extends PHPUnit_Framework_TestCase
{
    public function setUp() {
        $this->db = new PolarDB("localhost", "polar_test", "root", "root");
        $this->db->beginTransaction();
        PolarObject::$db = $this->db;
    }

    public function tearDown() {
        $this->db->rollback();
    }

    public function testMakeId() {
        $this->assertEquals('abc0', $this->db->makeId('abc'));
        $this->assertEquals('def1', $this->db->makeId('def'));
        $this->assertEquals('t2', $this->db->makeId('t'));
    }

    public function testTypeDetection() {
        $this->assertFalse(type_is_object(T_STR));
        $this->assertFalse(type_is_object(T_INT));
        $this->assertFalse(type_is_object(T_BOOL));
        $this->assertFalse(type_is_object(T_FLOAT));
        $this->assertFalse(type_is_object(array('member', 'private', 'public')));
        $this->assertTrue(type_is_object('Utilisateur'));
        $this->assertTrue(type_is_object('Page'));
    }

    /**
     * @expectedException Exception
     */
    public function testObjectWithoutDb() {
        PolarObject::$db = NULL;
        $o = new TestObject();
    }

    public function testPolarObject() {
        $o = new TestObject(array("texte" => "Bonjour",
                                  "nombre" => 42,
                                  "liste" => "b",
                                  "booleen" => True,
                                  "flottant" => 9.456,
                                  "objet" => NULL));
        $this->assertSame("Bonjour", $o->texte);
        $this->assertSame(42, $o->nombre);
        $this->assertSame("b", $o->liste);
        $this->assertSame(9.456, $o->flottant);
        $this->assertTrue($o->booleen);
        $o->flottant = "coucou";
        $this->assertSame(0.0, $o->flottant);
        $o->nombre = "truc";
        $this->assertSame(0, $o->nombre);
        $o->booleen = "faux";
        $this->assertSame(True, $o->booleen);

        $p = new TestObject(array("texte" => "A",
                                  "nombre" => 0,
                                  "liste" => "a",
                                  "flottant" => 9,
                                  "booleen" => False,
                                  "objet" => $o));
        $this->assertFalse($p->booleen);
        $this->assertSame($o, $p->objet);
        return $p;
    }

    /**
     * @expectedException InvalidValue
     * @depends testPolarObject
     */
    public function testInvalidValueListe($o) {
        $o->liste = "x";
    }

    public function testMakeAlias() {
        $this->assertEquals('t', make_alias('tests'));
        $this->assertEquals('pca', make_alias('polar_caisse_articles'));
        $this->assertEquals('pcvg', make_alias('polar_caisse_ventes_global'));
    }

    public function testCreateQuery() {
        $q = new PolarQuery($this->db, QUERY_SELECT, 'TestObject');
        $this->assertEquals(QUERY_SELECT, $q->getType());
        $this->assertEquals('TestObject', $q->getClass());

        $q = TestObject::update();
        $this->assertEquals(QUERY_UPDATE, $q->getType());
        $this->assertEquals('TestObject', $q->getClass());

        $q = TestObject::select();
        $this->assertEquals(QUERY_SELECT, $q->getType());
        $this->assertEquals('TestObject', $q->getClass());
    }

    /**
     * @depends testMakeId
     */
    public function testPolarQuerySelect() {
        $q = new PolarQuery($this->db, QUERY_SELECT, 'TestObject');
        
        $this->assertEquals('t.*', $q->format_selects());
        $this->assertEquals($q, $q->select('t.id'));
        $this->assertEquals('t.id', $q->format_selects());
        $q->select('nom');
        $this->assertEquals('t.id, nom', $q->format_selects());

        $this->assertEquals('1', $q->format_wheres());
        $this->assertEquals($q, $q->where('t.id = ?', 3));
        $this->assertEquals('t.id = :t0_0', $q->format_wheres());
        $q->where('t.texte = ?', 'THEVENET');
        $this->assertEquals('t.id = :t0_0 AND t.texte = :t0_1', $q->format_wheres());
    }

    public function testReplaceClassnameByAlias() {

    }

    public function testRawString() {
        $r = raw('Bonjour');
        $this->assertTrue($r instanceof RawString);
        $this->assertEquals('Bonjour', $r);
    }

    public function testPolarQueryInsert() {
        $q = new PolarQuery($this->db, QUERY_INSERT, 'Page');

        $q->set_value('Titre', "Coucou lélé'");
        $this->assertEquals('`Titre`=:psp0_0', $q->format_sets());
        $q->set_value('Acces', raw("'public'"));
        $this->assertEquals('`Titre`=:psp0_0, `Acces`=\'public\'', $q->format_sets());
        $q->set_value('Module', 'Coucou');
        $this->assertEquals('`Titre`=:psp0_0, `Acces`=\'public\', `Module`=:psp0_1', $q->format_sets());
        $q->set_value('Section', 'l"l"');
        $this->assertEquals('`Titre`=:psp0_0, `Acces`=\'public\', `Module`=:psp0_1, `Section`=:psp0_2', $q->format_sets());

        $in = $q->getInputs();
        $this->assertEquals("Coucou lélé'", $in[':psp0_0']);
        $this->assertEquals("Coucou", $in[':psp0_1']);
        $this->assertEquals('l"l"', $in[':psp0_2']);

        $q->execute();
    }

    public function testUtilisateur() {
        $u = new Utilisateur(array('Nom' => 'Thévenet'));
        $this->assertEquals('Thévenet', $u->Nom);
        return $u;
    }

    /**
     * @expectedException InvalidValue
     * @depends testUtilisateur
     */
    public function testInvalidVal($u) {
        $u->Sexe = 'b';
    }

    /**
     * @expectedException InvalidAttribute
     * @depends testUtilisateur
     */
    public function testInvalidAttr($u) {
        $u->Bonjour = NULL;
    }

    /**
     * @expectedException NotNullable
     * @depends testUtilisateur
     */
    public function testNotNullable($u) {
        $u->Email = NULL;
    }

    /**
     * @expectedException PDOException
     */
    public function testConnexion() {
        $a = new PolarDB("localhost", "polar", "choux", "fleur");
    }

    /**
     * @depends testTypeDetection
     */
    public function testHydrateObject() {
        
    }

    /**
     * @depends testTypeDetection
     */
    public function testLoadFromDB() {
        $u = Utilisateur::select()->where('Telephone LIKE ?', "0663469246")->execute();
        $this->assertEquals($u->get_id(), 913);
        $this->assertEquals($u->Login, 'fthevene');
        $this->assertEquals($u->Email, 'fthevene@etu.utc.fr');
        $this->assertTrue($u->Staff);
        $this->assertTrue($u->Bureau);
        $this->assertEquals($u->Nom, 'THEVENET');
        $this->assertEquals($u->Sexe, 'm');
        $this->assertEquals($u->Telephone, '0663469246');
        $this->assertCount(0, $u->get_dependants());
        $this->assertCount(0, $u->get_necessaires());

        $u->Nom = "Moimoi";
        $this->assertEquals("Moimoi", $u->Nom);
        $u->Sexe = "f";
        $this->assertEquals("f", $u->Sexe);
        $u->Staff = False;
        $this->assertFalse($u->Staff);
        $u->save();
        $this->assertEquals(1, Utilisateur::select('COUNT(*)')->where("Nom=? AND Sexe=? AND Staff=? AND ID=?", 'Moimoi', 'f', 0, 913)->rawExecute()->fetchColumn());

        $bureau = Utilisateur::select()->where('Bureau=?', 1)->execute();
        $this->assertCount(2, $bureau);
        return $u;
    }

    /**
     * @depends testLoadFromDB
     */
    public function testVente($u) {
        $article1 = Article::getById(557);
        $this->assertEquals(913, $article1->Auteur->get_id());
        $article2 = Article::getById(802);
        $v = array();
        $v[] = new Vente($article1, 2, 'cb', $u);
        $this->assertTrue(in_array($u, $v[0]->get_necessaires()));
        $this->assertCount(0, $v[0]->get_dependants());
        $this->assertEquals(2, $v[0]->Quantite);
        $this->assertEquals('cb', $v[0]->MoyenPaiement);
        $this->assertEquals('normal', $v[0]->Tarif);
        $this->assertEquals(NULL, $v[0]->Asso);
        $this->assertEquals(10, $v[0]->PrixFacture);
        $this->assertEquals(10*0.196, $v[0]->MontantTVA);
        $v[] = $v[0]->create_similaire($article1, 12);
        $this->assertEquals(12, $v[1]->Quantite);
        $this->assertEquals('cb', $v[1]->MoyenPaiement);
        $this->assertEquals('normal', $v[1]->Tarif);
        $this->assertEquals(NULL, $v[1]->Asso);
        $this->assertEquals(54, $v[1]->PrixFacture);
        $this->assertEquals(54*0.196, $v[1]->MontantTVA);
        $v[] = $v[0]->create_similaire($article2, 30);
        $v[] = $v[0]->create_similaire($article2, 150);
        $v[] = $v[0]->create_similaire($article2, 300);
    }

    /**
     * @depends testLoadFromDB
     */
    public function testPage() {
        $membres = Page::getById(7);
        $manuels = Page::getById(13);
        $this->assertEquals('public', $membres->Acces);
        $this->assertEquals('private', $manuels->Acces);
    }

    public function testInitialSave() {
        $u = new Utilisateur(array(
                                'IPClient' => '127.0.0.1',
                                'DateCreation' => 'NOW()',
                                'Login' => 'abcdefgh',
                                'MotDePasse' => '******',
                                'Email' => 'abcdefgh@etu.utc.fr',
                                'Staff' => 1,
                                'Bureau' => 0,
                                'Ancien' => 0,
                                'Responsable' => NULL,
                                'Poste' => NULL,
                                'Presentation' => '',
                                'Nom' => 'Super',
                                'Prenom' => 'Man',
                                'Sexe' => 'm',
                                'Telephone' => NULL,
                                'Newsletter' => 0));
        $u->save();
        $this->assertNotNull($u->get_id());
        return $u;
    }

    /**
     * @depends testInitialSave
     */
    public function testIncrementalSave($u) {
        $u->Telephone = "0663469246";
        $u->save();
        $this->assertEquals(1, Utilisateur::select('COUNT(*)')
                            ->where('ID = ? AND Telephone = ?', 913, "0663469246")
                            ->rawExecute()->fetchColumn());

        return $u;
    }

    public function testValidObject() {
        $this->assertTrue($this->db->validObject('Utilisateur', 913));
        $this->assertFalse($this->db->validObject('Utilisateur', 0));
    }

    /**
     * @depends testIncrementalSave
     * @depends testValidObject
     */
    function testDelete($u) {
        $id = $u->get_id();
        $u->delete();
        $this->assertFalse($this->db->validObject('Utilisateur', $id));
    }

}
?>
