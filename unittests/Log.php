<?php
// Logger de test
// là il il ne fait rien mais on pourrait tester que les fonctions loggent
// ce qu'il faut
class Log {
    public function __construct() { }

    public static function log($level, $message, array $context=array()) {
    }

    public static function emergency($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::EMERGENCY, $message, $context);
    }

    public static function alert($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::ALERT, $message, $context);
    }

    public static function critical($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::CRITICAL, $message, $context);
    }

    public static function error($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::ERROR, $message, $context);
    }

    public static function warning($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::WARNING, $message, $context);
    }

    public static function notice($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::NOTICE, $message, $context);
    }

    public static function info($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::INFO, $message, $context);
    }

    public static function debug($message, array $context = array()) {
        self::log(Psr\Log\LogLevel::DEBUG, $message, $context);
    }
}